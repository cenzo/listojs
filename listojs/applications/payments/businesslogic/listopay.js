"use strict";

function getTimeStamp() {
    let d = new Date();
    const date = d.getDate();
    const time = d.getTime();
    const ms = d.getMilliseconds();
    return date + time + ms;
}

function createQRText(productID, productName, productDescription, price, currency, qty, customText, ticketUid) {
    if (!productID) {
        document.getElementById("qrCode").innerText = "Error";
        console.error(getTextById(267));
    }
    document.getElementById("selectedProductID").innerText = productID;
    document.getElementById("selectedProduct").innerText = productName;
    document.getElementById("descOfselectedProduct").innerText = productDescription;
    document.getElementById("priceOfselectedProduct").innerText = price;
    document.getElementById("currency").innerText = currency;
    document.getElementById("quantity").value = qty;
    document.getElementById("customText").innerText = customText;
    document.getElementById("ticketUid").innerText = ticketUid;
    const ticketIdent = ticketUid + "|" + uuidv4() + "|" + getTimeStamp();
    document.getElementById("ticketUid").innerText = ticketUid;
    document.getElementById("transactionIdentifier").innerText = ticketIdent;
    const seller = document.getElementById("shopName").innerText;
    const firstName = document.getElementById("firstName").value;
    const lastName = document.getElementById("lastName").value;
    let qrText = "#" + qty + " [" + productID + "|" + productName + "(" + productDescription + ")][" + price + currency + "][" + firstName
        + " " + lastName + "][id:  " + ticketIdent + "][" + seller + "] " + customText;
    document.getElementById("qrText").innerText = qrText;
    return qrText;
}

function makeCode(text, targetDiv) {
    try {
        document.getElementById(targetDiv).innerText = "";
        let qrcode = new QRCode(document.getElementById(targetDiv), {
            width: 100,
            height: 100
        });
        qrcode.makeCode(text);
    } catch (err) {
        throw new Error(err.toString());
    }
}

jQuery(document).on('click', '.listoPayCreateTicket', function () {
    const currentUrl = window.location.href;
    sessionStorage.setItem("payment_callingPage", currentUrl);
    const productID = $(this).attr('data-productID');
    const productName = $(this).attr('data-productName');
    const productDescription = $(this).attr('data-productDescription');
    const price = $(this).attr('data-price') / 100;
    const currency = $(this).attr('data-currency');
    const qty = document.getElementById("quantity").value;
    const customText = $(this).attr('data-customText');
    const uid = $(this).attr('data-ticketUid');
    let qrText = createQRText(productID, productName, productDescription, price, currency, qty, customText, uid);
    try {
        makeCode(qrText, "qrcode");
        document.getElementById("qrcode").scrollIntoView();
    } catch (error) {
        console.error(error);
        // Errors occur often because of text overflow. So let's try with a shorter text:
        qrText = createQRText(productID, "n.n.", "n.n.", price, currency, qty, "", uid);
        try {
            makeCode(qrText, "qrcode");
            document.getElementById("qrcode").scrollIntoView();
        } catch (error2) {
            console.error(error2);
            document.getElementById("qrcode").innerText = getTextById(499);
        }
    }
});

$("#firstName").on("blur", function () {
    const productID = document.getElementById("selectedProductID").innerText;
    const productName = document.getElementById("selectedProduct").innerText;
    const productDescription = document.getElementById("descOfselectedProduct").innerText;
    const price = document.getElementById("priceOfselectedProduct").innerText;
    const currency = document.getElementById("currency").innerText;
    const qty = document.getElementById("quantity").value;
    const customText = document.getElementById("customText").innerText;
    const ticketUid = document.getElementById("ticketUid").innerText;
    let qrText = createQRText(productID, productName, productDescription, price, currency, qty, customText, ticketUid);
    try {
        makeCode(qrText, "qrcode");
    } catch (err) {
        console.error(err);
        qrText = createQRText(productID, "n.n.", "n.n.", price, currency, qty, "", ticketUid);
        try {
            makeCode(qrText, "qrcode");
            document.getElementById("qrcode").scrollIntoView();
        } catch (error2) {
            console.error(error2);
            document.getElementById("qrcode").innerText = getTextById(499);
        }
    }
}).on("keydown", function (e) {
    if (e.keyCode == 13) {
        const productID = document.getElementById("selectedProductID").innerText;
        const productName = document.getElementById("selectedProduct").innerText;
        const productDescription = document.getElementById("descOfselectedProduct").innerText;
        const price = document.getElementById("priceOfselectedProduct").innerText;
        const currency = document.getElementById("currency").innerText;
        const qty = document.getElementById("quantity").value;
        const customText = document.getElementById("customText").innerText;
        const ticketUid = document.getElementById("ticketUid").innerText;
        let qrText = createQRText(productID, productName, productDescription, price, currency, qty, customText, ticketUid);
        try {
            makeCode(qrText, "qrcode");
        } catch (err) {
            console.error(err);
            qrText = createQRText(productID, "", "", price, currency, qty, "", ticketUid);
            try {
                makeCode(qrText, "qrcode");
                document.getElementById("qrcode").scrollIntoView();
            } catch (error2) {
                console.error(error2);
                document.getElementById("qrcode").innerText = getTextById(499);
            }
        }
    }
});
$("#lastName").on("blur", function () {
    const productID = document.getElementById("selectedProductID").innerText;
    const productName = document.getElementById("selectedProduct").innerText;
    const productDescription = document.getElementById("descOfselectedProduct").innerText;
    const price = document.getElementById("priceOfselectedProduct").innerText;
    const currency = document.getElementById("currency").innerText;
    const qty = document.getElementById("quantity").value;
    const customText = document.getElementById("customText").innerText;
    const ticketUid = document.getElementById("ticketUid").innerText;
    let qrText = createQRText(productID, productName, productDescription, price, currency, qty, customText, ticketUid);
    try {
        makeCode(qrText, "qrcode");
    } catch (err) {
        console.error(err);
        qrText = createQRText(productID, "n.n.", "n.n.", price, currency, qty, "", ticketUid);
        try {
            makeCode(qrText, "qrcode");
            document.getElementById("qrcode").scrollIntoView();
        } catch (error2) {
            console.error(error2);
            document.getElementById("qrcode").innerText = getTextById(499);
        }
    }
}).on("keydown", function (e) {
    if (e.keyCode == 13) {
        const productID = document.getElementById("selectedProductID").innerText;
        const productName = document.getElementById("selectedProduct").innerText;
        const productDescription = document.getElementById("descOfselectedProduct").innerText;
        const price = document.getElementById("priceOfselectedProduct").innerText;
        const currency = document.getElementById("currency").innerText;
        const qty = document.getElementById("quantity").value;
        const customText = document.getElementById("customText").innerText;
        const ticketUid = document.getElementById("ticketUid").innerText;
        createQRText(productID, productName, productDescription, price, currency, qty, customText, ticketUid);
        let qrText = getCurrenQRText();
        try {
            makeCode(qrText, "qrcode");
        } catch (err) {
            console.error(err);
            qrText = createQRText(productID, "n.n.", "n.n.", price, currency, qty, "", ticketUid);
            try {
                makeCode(qrText, "qrcode");
                document.getElementById("qrcode").scrollIntoView();
            } catch (error2) {
                console.error(error2);
                document.getElementById("qrcode").innerText = getTextById(499);
            }
        }
    }
});


jQuery(document).on('click', '.listoPayPrintTicket', function () {
    let firstName = document.getElementById("firstName")
    let lastName = document.getElementById("lastName")
    let labelFirstName = document.getElementById("labelFirstName")
    let labelLastName = document.getElementById("labelLastName");
    const qty = document.getElementById("quantity").value;
    if (parseInt("" + qty) < 1) {
        document.getElementById("qrcode").innerText = getTextById(300);
        return;
    }
    if (!firstName.value) {
        labelFirstName.innerText = getTextById(725);
        firstName.focus();
    }
    if (!lastName.value) {
        labelLastName.innerText = getTextById(725)
        lastName.focus();
        return;
    }

    labelFirstName.innerText = "";
    labelLastName.innerText = "";
    // stackoverflow.com/questions/2255291/print-the-contents-of-a-div
    // stackoverflow.com/questions/10541079/print-in-the-background-with-javascript-i-e-without-document-pop-up
    const mywindow = window.open('', 'PRINT', 'height=350,width=550');
    mywindow.document.getElementsByTagName("body").item(0).innerHTML = "";
    mywindow.document.write(document.getElementById("printTicket").innerHTML);
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/
    mywindow.print();
    mywindow.close();
    return true;
});

jQuery(document).on('click', '.listoPayCheckout', function () {
    debug2("listoypay", "Trying to check out customer...", RELEASE, [""]);
    let firstName = document.getElementById("firstName")
    let lastName = document.getElementById("lastName")
    let labelFirstName = document.getElementById("labelFirstName")
    let labelLastName = document.getElementById("labelLastName")
    const qty = document.getElementById("quantity").value;
    const stripe_account = getStripeAccountID(function(stripe_acc_id){
        console.log("Found stripe_account_id for this customer is: "+stripe_acc_id);
    });
    const productID = document.getElementById("selectedProductID").innerText;
    const price = document.getElementById("priceOfselectedProduct").innerText;
    const productName = document.getElementById("selectedProduct").innerText;
    const productDescription = document.getElementById("descOfselectedProduct").innerText + "[id:" + document.getElementById("transactionIdentifier").innerText + "]";
    const currency = document.getElementById("currency").innerText;
    if(!productID){
        document.getElementById("qrcode").innerText = getTextById(760);
        debug2("listoypay", "no product was selected", RELEASE, qty);
        return;
    }
    if (parseInt("" + qty) < 1) {
        document.getElementById("qrcode").innerText = getTextById(300);
        debug2("listoypay", "invalid quantity for product", RELEASE, qty);
        return;
    }
    if (!firstName.value) {
        labelFirstName.innerText = getTextById(725);
        debug2("listoypay", "First name of customer is missing", RELEASE, [firstName]);
        firstName.focus();
    }
    if (!lastName.value) {
        labelLastName.innerText = getTextById(725);
        debug2("listoypay", "Last name of customer is missing", RELEASE, [lastName]);
        lastName.focus();
        return;
    }
    labelFirstName.innerText = "";
    labelLastName.innerText = "";


    var customerID=getCustomerId();
    console.log("customerID="+customerID);
    console.log("Checkout clicked:\nStripe-account:" + stripe_account + "\n productID:" + productID + ",\n price:" + price + ",\n product: " + productName + ",\nDescription: " +
        productDescription + ",\n" + currency + ",\nqty: " + qty);
    const priceWDigits = price * 100;
    const receiptEmail = sessionStorage.getItem("userEmail");
    console.log("receiptEmail=" + receiptEmail);
    stripeConnectPayment(stripe_account, 1, productID, priceWDigits, productName, productDescription, currency, qty,receiptEmail,"card",function (data) {
        console.log("Data returned from stripe: " + JSON.stringify(data));
        const stripe_session_id = data.id;
        if (!stripe_session_id) {
            console.error("NO STRIPE SESSION ID FOUND: " + JSON.stringify(data));
            //showHttpErrorMessage("main-content", data);
        }
        console.log("Returned session id:" + stripe_session_id);
        connectCheckout("" + stripe_session_id, "" + stripe_account).then(function (data) {
            console.log("checkout completed");
            console.log("Data returned from session: " + JSON.stringify(data));
        }, function (err) {
            console.error("checkout failed: " + err);
            document.getElementById("qrcode").innerText = getTextById(499);
        });
    },function (err) {
        console.error(err);
        showHttpErrorMessage("qrcode", err);
    });

});

