module = "admin_category.js";


//  SHOW CATEGORIES TABLE
function showcategories() {
    const action1 = ["create-editcategory-form", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
    const action2 = ["delete-category", actionDeleteStyle.elements[0] + getTextById(106) + actionDeleteStyle.elements[1]];
    const dataColumnNames1 = ["category", "idproductcategory", "prodcatdesc", "prodcatname",
        "prodcatimage", "level", "position", "hierarchy_1", "hierarchy_2", "hierarchy_3", "prodcatname_locale", "idcatstyle"];
    const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12];
    const dataColumnNames2 = ["idproductcategory"];
    const dataColumnIndices2 = [1];
    const filterArray = [];
    apiCall_listorante_public_categories(function (data) {
        const tData = data;
        for (let i = 0; i < tData.count; i++) {
            if (jQuery.inArray(tData.rows[i].s[1], filterArray) !== -1) {
                delete data.rows[i];
            } else {
                filterArray.push(tData.rows[i].s[1]);
            }
        }
        const admin = {
            IDENTIFIER: "admin-edit-category",
            jsonRMLData: data,
            header: [getTextById(229), getTextById(228), "ID", getTextById(227), getTextById(230), getTextById(310), "", ""],
            columnIndices: [11, 3, 1, 2, 4, 10],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (colIndex === 4) {
                        if (!data) data = images.missing_image_txt;
                        return TDopen + "<img src='" + getImagePath() + "/" + data
                            + "' " + ThumbStyle.default + ">" + TDclose;
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(admin, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    })

};


function setParentCategory(parent_ID) {
    document.getElementById("parentCategoryId").innerHTML = parent_ID;
    debug('checked parent:', 0, parent_ID);
};

function unSetParentCategory(IDcategory) {
    document.getElementById('parentCategoryId').innerHTML = '';
    debug('un-checked parent:', 0, IDcategory);
};


function linkcategory(category, parent_category) {
    apiCall_listorante_admin_linkcategory(category, parent_category, function (data) {
        if (data._rc > 0) {
            alert(getTextById(260));
            debug("Could not assign category", release, data);
        } else {
            debug("linkcategory", release, data);
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
        debug("Error: could not assign category", 0, err);
        alert(getTextById(260));
    });
}


// 1. Create editable form with "add-category"-event
$(document)
    .on(
        'click',
        '.create-addcategory-form',
        function () {
            const idproductcategory = [FormSelectBox, 53, "category_add_idproductcategory", "rows='1' required ", ""];
            const prodcatdesc_locale = [FormTextArea, 227, "category_add_prodcatdesc_locale", "rows=2 required ", ""];
            const prodcatname = [formTextInput, 228, "category_add_prodcatname", "rows=\"1\" required style=\"text-transform:uppercase\"", ""];
            const prodcatname_locale = [formTextInput, 229, "category_add_prodcatname_locale", "rows=\"1\" required ", ""];
            const prodcatimage = [formImage, 230, "file_imagefield", "required", "", ""];
            const submitButton = [FormButton, 215, "add_category", "", "add-category"];
            const form1 = [prodcatname_locale, prodcatname, idproductcategory, prodcatdesc_locale, prodcatimage, submitButton];
            listoHTML.createForm("#main-content", 8, form1, "add-category-form");
            const categoriesData = [];
            const dupArray = [];
            apiCall_listorante_public_categories(function (response) {
                // sessionStorage.setItem('uniqueID', '');
                const tree_root = {
                    id: "root_0",
                    parent: "#",
                    text: "*",
                    state: {
                        "opened": true, "checked": false, "selected": false
                    }
                }
                categoriesData.push(tree_root);
                for (let i = 0; i < response.count; i++) {
                    const billdata = {};

                    /*if (sessionStorage.getItem('uniqueID') == response.rows[i].s[1]) {
                        if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                            billdata.id = i + 'rep_' + response.rows[i].s[1];
                            dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                        } else {
                            billdata.id = 'rep_' + response.rows[i].s[1];
                        }
                        dupArray.push(response.rows[i].s[1]);
                    } else {
                        billdata.id = 'cat_' + response.rows[i].s[1];
                    }*/
                    billdata.id = 'cat_' + response.rows[i].s[1];
                    if (response.rows[i].s[0] == 0) {
                        billdata.parent = 'root_0';
                    } else {
                        if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                            billdata.parent = 'rep_' + response.rows[i].s[0];
                        } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                            billdata.parent = i + 'rep_' + response.rows[i].s[0];
                        } else {
                            billdata.parent = 'cat_' + response.rows[i].s[0];
                        }
                    }
                    billdata.text = response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
                    debug("response.rows[" + i + "].s: " + response.rows[i].s);
                    billdata.state = {
                        "opened": true, "checked": false, "selected": false
                    };
                    categoriesData.push(billdata);
                    //sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
                }
                $('#jstree').jstree({
                    'core': {
                        'multiple': false,
                        'data': categoriesData,
                        'check_callback': false
                    }, types: {
                        "root": {
                            "icon": "glyphicon glyphicon-record"
                        },
                        "child": {
                            "icon": "glyphicon glyphicon-minus"
                        },
                        "default": {
                            "icon": "glyphicon glyphicon-cutlery"
                        }
                    },
                    "checkbox": {
                        'deselect_all': true,
                        "three_state": false
                    },
                    plugins: ["types", "data", "checkbox"]
                }).on('open_node.jstree', function (e, data) {
                    data.instance.set_icon(data.node, "glyphicon glyphicon-minus");
                }).on('close_node.jstree', function (e, data) {
                    data.instance.set_icon(data.node, "glyphicon glyphicon-cutlery");
                }).on('loaded.jstree', function () {
                    $("#jstree").jstree().select_node('cat_0');
                    $("#jstree").jstree().check_node('cat_0');
                    $('#product_category').val('0');
                }).on("select_node.jstree unselect_node.jstree", function (e, data) {
                    const cNode = "" + data.node.id;
                    const underlineIdx = cNode.indexOf("_") + 1;
                    const idcategory = cNode.substring(underlineIdx, cNode.length);
                    $('#product_category').val(idcategory);
                    (data.node.state.selected ? setParentCategory(idcategory) : unSetParentCategory(idcategory))
                }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
                    const cNode = "" + data.node.id;
                    const underlineIdx = cNode.indexOf("_") + 1;
                    const idcategory = cNode.substring(underlineIdx, cNode.length);
                    $('#product_category').val(idcategory);
                    debug('data.node.state:', 0, data.node.state);
                    (data.node.state.checked ? setParentCategory(idcategory) : unSetParentCategory(idcategory));
                });
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        });

// 2. Add new category by submitting the form
$(document).on('click', '.add-category', function () {
    if ($("#add-category-form").valid()) {
        const idproductcategory = document.getElementById('parentCategoryId').innerHTML;
        if (parseInt(idproductcategory, 10) >= 0) {
            const prodcatdesc_locale = $('#category_add_prodcatdesc_locale').val();
            const prodcatname = $('#category_add_prodcatname').val().toUpperCase().replace(/[^A-Z0-9]/gmi, "_");
            const prodcatname_locale = $('#category_add_prodcatname_locale').val();
            const custID = getCustomerId();
            if (document.getElementById('file_imagefield').value) {
                upload(custID, 'add-category-form', function (data) {
                    debug("upload-data:", release, data);
                    const imageNew = "" + data.rows[0].s[2];
                    const image_thumbNew = imageNew;
                    alert(getTextById(255) + document.getElementById('file_imagefield').value +
                        getTextById(256) + imageNew + "'");
                    apiCall_listorante_admin_newcategory('0', '0', '0', '0',
                        '0', idproductcategory, -1, -1,
                        prodcatdesc_locale, prodcatdesc_locale, imageNew, prodcatname, prodcatname_locale, function (response) {
                            debug('response from new category:', 0, response);
                            showcategories();
                        }, function (err) {
                            showHttpErrorMessage("main-content", err);
                        });
                }, function (error) {
                    alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
                });
            } else {
                const noImage = images.missing_image_txt;
                apiCall_listorante_admin_newcategory('0', '0', '0', '0', '0',
                    idproductcategory, -1, -1, prodcatdesc_locale,
                    prodcatdesc_locale, noImage, prodcatname, prodcatname_locale, function (response) {
                        debug('response from new category:', 0, response);
                    }, function (err) {
                        showHttpErrorMessage("main-content", err);
                    });
            }
            document.getElementById('parentCategoryId').innerHTML = '';
        } else {
            alert("Please select a parent category.");
            return;
        }
    } else {
        debug('Not Valid');
        return;
    }

});

/*
 * Admin-Menu / Edit Category
 *
 *
 */
// 1. Select categories-data and create table
$(document).on(
    'click',
    '.edit-category',
    function () {
        showcategories();
    }
);

// 2. Create editable form to change one single category
$(document).on(
    'click',
    '.create-editcategory-form',
    function () {
        release = RELEASE;
        debug("this.data:", release, $(this).data());
        const idCatStyle = $(this).data("idcatstyle");
        const category = $(this).data("category");
        const idproductcategory = [FormSelectBox, 53, "category_add_idcategory", "rows='1' required ", category];
        const idproductcategoryID = $(this).data("idproductcategory");
        const prodcatdesc = $(this).data("prodcatdesc");

        debug("create Edit category form", release, category, idproductcategoryID);
        if ($(this).data("prodcatimage")) {
            var prodcatimage = [formImage, 230, "file_imagefield", "required", $(this).data("prodcatimage"),
                $(this).data("prodcatimage")];
        } else {
            var prodcatimage = [formImage, 230, "file_imagefield", "required", "", ""];
        }
        const uploadButton = [FormButton, 17, "file_imagefield", "", "file_imagefield"];
        const prodcatdesc_locale = [FormTextArea, 227, "category_add_prodcatdesc_locale", "rows=2 required ", prodcatdesc];
        const prodcatname = [formTextInput, 228, "category_add_prodcatname", 'rows="1" required style="text-transform: uppercase"',
            $(this).data("prodcatname")];
        const idStyle = [formNumericInput, 262, "product_edit_style", "required step='1'", idCatStyle];
        //const idStyle = formNumericInput(262,"product_edit_style", "required step='1'",idCatStyle );

        const prodcatname_locale = [formTextInput, 229, "category_add_prodcatname_locale", "rows=\"1\" required",
            $(this).data("prodcatname_locale")];
        const submitButton = [FormButton, 215, "update_category", "", "update-category"];
        const hidden_category_id = [formHiddenInput, "", idproductcategoryID, "id_of_edited_category"];

        const productsDiv = [formRowDiw, "productsBox", 10];

        debug("productsDiv", 0, productsDiv);


        const form1 = [prodcatname_locale, prodcatname, idStyle, idproductcategory, prodcatdesc_locale, prodcatimage,
            hidden_category_id, submitButton, productsDiv];
        const productListOfCategory = [];
        apiCall_listorante_public_product(idproductcategoryID, function (data) {
            for (let i = 0; i < data.count; i++) {
                productListOfCategory.push(data.rows[i].s[0]);
            }
            debug("productListOfCategory", 0, productListOfCategory);
            apiCall_listorante_public_products(function (products) {
                const action_edit = ["edit_element_product", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
                const dataColumnNames1 = ["idproduct", "idlabel", "prodname_en", "image_thumb", "prodname", "price",
                    "image",
                    "prodinfo_en", "proddesc_en", "position",
                    "qty_in_stock", "qty_unit", "threshold_qty"];
                const dataColumnIndices1 = [0, 12, 5, 4, 1, 2, 3, 6, 7, 8, 9, 10, 11];

                const products_of_category = {
                    IDENTIFIER: "admin-edit-products_of_category",
                    jsonRMLData: products,
                    header: [getTextById(71), getTextById(733), getTextById(205), getTextById(61), "", ""],
                    columnIndices: [12, 5, 4],
                    actions: [action_edit],
                    dataColumnIndices: [dataColumnIndices1],
                    dataColumnNames: [dataColumnNames1],
                    rowFormat: function (rowIndex, dataArray) {
                        return dataArray;
                    },
                    cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                        {
                            if (colIndex === 0) {
                                if (productListOfCategory.includes(products.rows[rowIndex].s[0])) {
                                    return TDopen + "*" + TDclose + TDopen + FormCheckBox(105, "checkProd" + products.rows[rowIndex].s[0], "class=checkProduct data-product='" + products.rows[rowIndex].s[0] + "' data-category='" + idproductcategoryID + "'", true) + TDclose + TDclose + TDopen + data + TDclose;
                                } else
                                    return TDopen + "" + TDclose + TDopen + FormCheckBox(105, "checkProd" + products.rows[rowIndex].s[0], "class=checkProduct data-product='" + products.rows[rowIndex].s[0] + "' data-category='" + idproductcategoryID + "'", false) + TDclose + TDclose + TDopen + data + TDclose
                            }
                            if (colIndex === 2) return TDopen + "<img src='" + getImagePath() + "/" + data + "' width='35'>" + TDclose;
                            else return TDopen + data + TDclose
                        }
                    }
                };
                listoHTML.createTable.call(products_of_category, "productsBox", false);
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });

        listoHTML.createForm("#main-content", 8, form1, 'category-edit-form');
        const categoriesData = [];
        const dupArray = [];
        apiCall_listorante_public_categories(function (response) {
            // sessionStorage.setItem('uniqueID', '');
            const tree_root = {
                id: "0",
                parent: "#",
                text: "",
                icon:"glyphicon glyphicon-cutlery",
                state: {
                    opened: false, checked: false, selected: false
                }
            }
            categoriesData.push(tree_root);
            for (let i = 0; i < response.count; i++) {
                const billdata = {};
                /* if (sessionStorage.getItem('uniqueID') == response.rows[i].s[3]) {
                     if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                         billdata.id = i + 'rep_' + response.rows[i].s[1];
                         dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                     } else {
                         billdata.id = 'rep_' + response.rows[i].s[1];
                     }
                     dupArray.push(response.rows[i].s[1]);
                 } else {
                     billdata.id = 'cat_' + response.rows[i].s[1];
                 }*/
                //billdata.id = 'cat_' + response.rows[i].s[1];
                billdata.id = response.rows[i].s[1];
                billdata.parent=response.rows[i].s[0];
                /*if (response.rows[i].s[0] == 0) {
                    billdata.parent = 'root_0';
                } else {
                    if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = 'rep_' + response.rows[i].s[0];
                    } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = i + 'rep_' + response.rows[i].s[0];
                    } else {
                        billdata.parent = 'cat_' + response.rows[i].s[0];
                    }
                }*/
                billdata.text = response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
                //if (billdata.id === 'cat_' + idproductcategoryID || billdata.id === 'rep_' + idproductcategoryID || billdata.id === i + 'rep_' + idproductcategoryID)
               //  {
                     if (parseInt(response.rows[i].s[1], 10) === parseInt(idproductcategoryID, 10)) {

                         const release = RELEASE;
                    const nodeState = {"disabled": true, "opened": true, "checked": false, "selected": false};
                    billdata.state = nodeState;
                    //$("#"+billdata.id+"_anchor >a").css("color","red");
                    //console.log("Try to set color of " + "#" + billdata.id + "_anchor to red");
                  //  console.log("parent of category " + billdata.id + "=" + billdata.parent);
                    debug2(module, ">>>>> disable category ", release, [idproductcategoryID, billdata.parent, billdata]);
                }
                /*if (category === billdata.parent) {
                    // I think this never occurs...
                    const nodeState = {"opened": true, "checked": true, "selected": true};
                    billdata.state = nodeState;
                    console.log(" ---- WHERE ARE WE??? ");
                    debug2(module, " ---- WHERE ARE WE??? ", release, [category, response.rows[i].s[1], billdata]);
                }*/
                else if (parseInt(response.rows[i].s[1], 10) === parseInt(category,10)) {
                    const nodeState = {"opened": true, "checked": true, "selected": true};
                    document.getElementById("parentCategoryId").innerText = response.rows[i].s[1];
                    billdata.state = nodeState;
                    billdata.text="***** "+billdata.text+" ***** ";
                    billdata.icon="glyphicon glyphicon-flag";
                    debug2(module, ">>>>> Set checkbox for parent of category", release, [idproductcategoryID, response.rows[i].s[1], billdata]);
                } else {
                    try {
                        const nodeState = {"opened": false, "checked": false, "selected": false};
                        billdata.data.state = nodeState;
                     //   debug2(module, "Nodestates", release, [billdata, billdata.state]);
                    } catch (err) {
                        //  debug("Error while trying to get nodeState",release,err);
                    }
                }
                categoriesData.push(billdata);
                //sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
            }
            //for (let n=0;n<categoriesData.length;n++){
             //   categoriesData[n].state={opened:false};
           // }
            $('#jstree').jstree({
                'core': {
                    'multiple': false,
                    'data': categoriesData,
                    'check_callback': false
                }, types: {
                    "root": {
                        "icon": "glyphicon glyphicon-cutlery"
                    },
                    "child": {
                        "icon": "glyphicon glyphicon-record"
                    },
                    "default": {
                        "icon": "glyphicon glyphicon-minus"
                    }
                },
                "checkbox": {
                    "deselect_all": true,
                    "three_state": false
                },
                plugins: ["types", "data", "checkbox", "state"]
            }).on('open_node.jstree', function (e, data) {
               if(data.node.id!="0") data.instance.set_icon(data.node, "glyphicon glyphicon-record");
               console.log("open:\n"+JSON.stringify(data.node));
            }).on('close_node.jstree', function (e, data) {
                if(data.node.id!="0") data.instance.set_icon(data.node, "glyphicon glyphicon-asterisk");
            }).on("select_node.jstree unselect_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                //$('#product_category').val(idcategory);
                (data.node.state.selected ? setParentCategory(idcategory) : unSetParentCategory(idcategory));
            }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                // debug("underlineIdx,idcategory/cNode/node.id:",release,underlineIdx,idcategory,cNode,data.node.id);
                (data.node.state.checked ? setParentCategory(idcategory) : unSetParentCategory(idcategory))
            });
            console.log(JSON.stringify(categoriesData));
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
    });

// Edit product from edit category-form:
$(document).on(
    'click',
    '.open-editproduct-form',
    function () {
        const prod = $(this).data();
        debug("open product edit for:" + prod, 0, prod);
    });

// 3 EDIT CATEGORY FORM SUBMISSION
$(document).on('click', '.update-category', function () {
    const idproductcategory = $('#product_category').val();
    const prodcatdesc_locale = $('#category_add_prodcatdesc_locale').val();
    const category_idchildcategory = $('#id_of_edited_category').val();
    const prodcatname = $('#category_add_prodcatname').val().toUpperCase().replace(/[^A-Z0-9]/gmi, "_");
    const prodcatname_locale = $('#category_add_prodcatname_locale').val();
    const prodcatimage = $('#product_file_imageField').val();
    const idStyle = $('#product_edit_style').val();
    debug('prodcatname--' + prodcatname, 0);
    debug("update-category: idproductcategory/childcategory", 0, idproductcategory, category_idchildcategory);
    const custID = getCustomerId();
    if ($("#category-edit-form").valid()) {
        if (document.getElementById('file_imagefield').value) {
            upload(custID, 'category-edit-form', function (data) {
                const imageNew = "" + data.rows[0].s[2];
                debug(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'", "" + release);
                apiCall_listorante_admin_editcategory('0', '0', '0', '0', '0',
                    category_idchildcategory, idStyle, '0', prodcatdesc_locale, prodcatdesc_locale, imageNew, prodcatname, prodcatname_locale,
                    function (response) {
                        let parentCategory =
                            parseInt(document.getElementById('parentCategoryId').innerHTML, 10);
                        linkcategory(category_idchildcategory, parentCategory);
                        showcategories();
                    }, function (err) {
                        showHttpErrorMessage("main-content", err);
                    });
            }, function (error) {
                alert(getTextById(257) +
                    document.getElementById('file_imagefield').value
                    + getTextById(258) + error.status);
            });
        } else {
            let catImage;
            if (prodcatimage) catImage = prodcatimage;
            else catImage = images.missing_image_txt;
            apiCall_listorante_admin_editcategory('0', '0', '0', '0', '0',
                category_idchildcategory, idStyle, 0, prodcatdesc_locale,
                prodcatdesc_locale, catImage, prodcatname, prodcatname_locale,
                function (response) {
                    if (response._rc > 0) alert("Edit failed");
                    else {
                        let parentCategory =
                            parseInt(document.getElementById('parentCategoryId').innerHTML, 10);
                        linkcategory(category_idchildcategory, parentCategory);
                        showcategories();
                    }
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });
        }
    }
});


// 4. Assign a product to a category
$(document).on(
    'click',
    '.checkProduct',
    function () {
        const product = $(this).data("product");
        const category = $(this).data("category");
        debug("check Product", 0, product, category);
        let checkBox = document.getElementById("checkProd" + product);
        // checkBox.checked=!checkBox.checked;
        if (checkBox.checked) {
            apiCall_listorante_admin_setproductcategory(category, product, function (data) {
                debug("Assigned product " + product + " to category " + category);
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        } else {
            apiCall_listorante_admin_unsetproductcategory(category, product, function (data) {
                debug("De-Assigned product " + product + " from category " + category);
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        }
    });

// 5. Delete a category directly from within the editable table
$(document).on('click', '.delete-category', function () {
    const idcategory = $(this).data("idproductcategory");
    if (confirm('Delete this category?'))
        apiCall_listorante_admin_deletecategory(idcategory, function (data) {
            if (data._rc > 0) {
                alert(getTextById(259));
            } else {
                debug("Delete " + idcategory, 0, data);
                showcategories();
            }
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
});


// End of: Admin-Menu/ Function 4 / Edit Category

function pleaseOpen(thisNode) {
    if (typeof thisNode == "undefined") return;
    if (thisNode.hasClass("jstree-leaf") || thisNode.hasClass("jstree-open")) return;
    $('#jstree').open_node(thisNode, function () {
        ;
    }, true);
};
