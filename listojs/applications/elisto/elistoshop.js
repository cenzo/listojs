/* elistoshop.js  
28.10.2020 21:19:38,62  */ 
/*-----------------------------------*/ 
/* localization.js */ 
// ------- Text-IDs for localization of ISO 'en' -----------

var texts_en = {
  "text1" : "Submit Order",
  "text2" : "Delete",
  "text3" : "Kitchen",
  "text4" : "Currently occupied tables",
  "text5" : "Current  bills",
  "text6" : "Overview",
  "text7" : "Current customer status",
  "text8" : "Overview tables",
  "text9" : "Overview bills",
  "text10" : "Current status",
  "text11" : "Connected",
  "text12" : "Ordered items",
  "text13" : "Product Management",
  "text14" : "Add Category",
  "text15" : "Add Product",
  "text16" : "Edit Category",
  "text17" : "Edit Product",
  "text18" : "Staff and customers",
  "text19" : "Add new Person",
  "text20" : "Edit Profiles",
  "text21" : "Reservations",
  "text22" : "Layout-Management",
  "text23" : "Layout-Structure",
  "text24" : "Layout-Colors",
  "text25" : "Layout-Images",
  "text26" : "Sign Out",
  "text27" : "Product Info",
  "text28" : "Table",
  "text29" : "Number of open bills",
  "text30" : "Add new order",
  "text31" : "Number of persons",
  "text32" : "Enter text...:",
  "text33" : "Bill Number",
  "text34" : "Creation time",
  "text35" : "Bill status",
  "text36" : "Remarks",
  "text37" : "Delete this item?",
  "text38" : "Product-quantity=",
  "text39" : "Tables",
  "text40" : "New table",
  "text41" : "Edit table",
  "text42" : "Short name",
  "text43" : "Name of table",
  "text44" : "Is a single Seat",
  "text45" : "Number of places",
  "text46" : "Delete table",
  "text47" : "Change language",
  "text48" : "Product List",
  "text49" : "Delete Product",
  "text50" : "Product Name",
  "text51" : "Product Price",
  "text52" : "Quantity in Stock ",
  "text53" : "Product Category",
  "text54" : "Product Information",
  "text55" : "Product Description",
  "text56" : "Product Image",
  "text57" : "Product Image Thumb",
  "text58" : "Product Position",
  "text59" : "Product Qty in Unit",
  "text60" : "Threshold Qty",
  "text61" : "Product Name",
  "text62" : "Delete Category",
  "text63" : "Total Bills",
  "text64" : "Utilisation in %",
  "text65" : "Turnover waiting",
  "text66" : "Items being prepared",
  "text67" : "Items in delivery",
  "text68" : "Bill Id",
  "text69" : "Elapsed time in minutes",
  "text70" : "Waiter",
  "text71" : "Status",
  "text72" : "Table Number",
  "text73" : "Table Place Number",
  "text74" : "Cook",
  "text75" : "Email",
  "text76" : "First Name",
  "text77" : "Is Active?",
  "text78" : "Is Staff?",
  "text79" : "Is SuperUser",
  "text80" : "Last Name",
  "text81" : "Your Password",
  "text82" : "User Name",
  "text83" : "Add User",
  "text84" : "No",
  "text85" : "User Image",
  "text86" : "Date Joined",
  "text87" : "Last Login",
  "text88" : "Edit User",
  "text89" : "Delete User",
  "text90" : "Assign Role",
  "text91" : "Waiter ID",
  "text92" : "Waiter Name",
  "text93" : "Table Name",
  "text94" : "Customer Table ID",
  "text95" : "TS creation",
  "text96" : "ID Bill Status",
  "text97" : "Guest Count",
  "text98" : "Minutes",
  "text99" : "Overview waiter",
  "text100" : "Manager",
  "text101" : "WEB-Administrator",
  "text102" : "Customer",
  "text103" : "Assign Role",
  "text104" : "Layout-Fonts",
  "text105" : "Edit",
  "text106" : "Delete",
  "text107" : "Sub-Categories",
  "text201" : "Element Name",
  "text202" : "Value",
  "text203" : "Default value",
  "text204" : "Layout Text",
  "text205" : "Id",
  "text206" : "Element name",
  "text207" : "Element type id",
  "text208" : "Value",
  "text209" : "Default value",
  "text210" : "Edit Structure",
  "text211" : "Edit Color",
  "text212" : "Edit Font",
  "text213" : "Edit Image",
  "text214" : "Edit Text",
  "text215" : "Save",
  "text216" : "Reset Default",
  "text217" : "Reset to Default Value?",
  "text221" : "Preparation waiting",
  "text222" : "Preparation started",
  "text223" : "Preparation completed",
  "text224" : "Messages",
  "text225" : "More info",
  "text226" : "[Category Description]",
  "text227" : "Category Description",
  "text228" : "[International Short Name]",
  "text229" : "Category Name",
  "text230" : "Category Image",
  "text231" : "Position",
  "text232" : "Level",
  "text233" : "ID",
  "text250" : "Change password",
  "text251" : "Please fill the blanks.",
  "text252" : "Please type a correct number.",
  "text253" : "Error: rc=",
  "text254" : "Table id is not defined.",
  "text255" : "File ",
  "text256" : " is being uploaded and will be saved with following name: '",
  "text257" : "Error: File ",
  "text258" : " could not be  uploaded.  Status=",
  "text259" : "Unable to delete category. Please retry.",
  "text260" : "Could not assign category to Parent category. Please retry.",
  "text261" : "Could not de-assign category from Parent category. Please retry.",
  "text262" : "Layout-ID ",
  "text263" : " id is not defined.",
  "text264" : "Selected file: ",
  "text265" : "Could not assign product to category. Please retry.",
  "text266" : "Could not de-assign product from category. Please retry.",
  "text267" : "Product id is not defined.",
  "text268" : "Please insert a valid Email Address!",
  "text269" : "User Id is not assigned! Please check again!",
  "text270" : "User updated succesfully",
  "text271" : "An unexpected event occurred",
  "text272" : "Print Orders",
  "text273" : "Check out",
  "text274" : "Archive",
  "text275" : "There are still unprocessed listed items.",
  "text300" : "Please type in a positive number",
  "text310" : "Parent category",
  "text311" : "unit",
  "text312" : "liter",
  "text313" : "gram",
  "text314" : "kilogram",
  "text315" : "can",
  "text316" : "bottle",
  "text317" : "other",
  "text318" : "Product Gallery",
  "text319" : "Upload Image",
  "text320" : "Attributes",
  "text321" : "Product  Gallery Image",
  "text322" : "Open Attribute Definitions",
  "text323" : "Attribute Name",
  "text324" : "Attribute Value",
  "text325" : "No Attributes Found.",
  "text326" : "Set as Product Attribute",
  "text327" : "Attribute Mnemonic",
  "text328" : "Define New Attribute",
  "text329" : "Deassign Attribute",
  "text330" : "Support",
  "text331" : "Ticket Number",
  "text332" : "Ticket Post Date",
  "text333" : "Ticket Status",
  "text334" : "Select Ticket",
  "text335" : "Message ID",
  "text336" : "Customer Text",
  "text337" : "Attached File",
  "text338" : "Date",
  "text339" : "Submit Ticket",
  "text400" : "HTTP 400: Bad request",
  "text401" : "HTTP 401: Sign-in required",
  "text403" : "HTTP 403: No authorization to access this content",
  "text404" : "HTTP 404: No such page",
  "text405" : "HTTP 405: Unallowed http-method",
  "text422" : "HTTP 422: Sorry...: the sent data contained some errors. Please retry.",
  "text499" : "Sorry...! An unexpected error occured.",
  "text500" : "HTTP 500: Internal Server-Error",
  "text533" : "HTTP 533: server error",
  "text534" : "HTTP 534: server error",
  "text535" : "HTTP 535: server error",
  "text600" : "Refresh Preview",
  "text601" :"---------------",
  "text602" :"Return",
  "text603" :"Close this order",
  "text604" :"Pay",
  "text605" :"Re-open order",
  "text606" :"Archive this order",
  "text607" :"Order has been archived",
  "text608" : "Passwords do not match",
  "text609" : "Password was changed",
  "text610" : "Value was saved",
  "text611" : "Image was uploaded",
  "text612" : "Delivered items",
  "text613" : "Average elapsed time in minutes",
  "text614" : "Sign up",
  "text615" : "Login",
  "text616" : "Product-site",
  "text701" : "Registration",
  "text702" : "Thank you for your registration! We just sent you an Email with a link, which will allow you to confirm your request. If you do not find any Email from listorante.com, kindly check your spam-folder. You can send us also, at any time, an Email to support@listorante.com.",
  "text703" : "Request a listorante-application for your company",
  "text704" : "This Email-adress is already registered",
  "text705" : "This domain-name is not available",
  "text706" : "Register as a sub-domain",
  "text707" : "Are you the owner of this domain, and do you want to install your listorante-application on your own web-space?",
  "text708" : "Are you the owner of this domain, and do you want to install your listorante-application on a listorante-web-space?",
  "text709" : "Do you prefer stopping this registration and receive more help and directions via Email beforehand?",
  "text710" : "Check this box to agree with our business terms.",
  "text711" : "Read our Business terms.",
  "text712" : "Send request",
  "text713" : "Access your application",
  "text714" : "Confirm",
  "text715" : "Select the app you want to access:",
  "text716" : "I already have a listorante-accountnumber",
  "text717" : "The requested domain-name is available",
  "text718" : "Send now?",
  "text719" : "YES",
  "text720" : "NO",
  "text721" : "This domain-name is not available",
  "text722" : "Do you want to try another domain-name?",
  "text723" : "Create your application",
  "text724" : "Name of your company",
  "text725" : "This field is mandatory",
  "text726" : "Your listorante company ID:",
  "text727" : "You don't have a listorante company account-number, yet?",
  "text728" : "Request it here",
  "text729" : "Account number",
  "text730" : "Your listorante-application is now ready to be used. The initial password has been sent to you via Email.",
  "text731" : "Sorry, unfortunately the creation of your listorante-application has failed.",
  "text732" : "Our Email, asking for your confirmation, was sent to:",
  "text733": "Add",
  "text734": "Back",
  "text735": "Take a tour",
  "text736": "Edit Business Terms Text",
  "text737": "Edit Privacy Text",
  "text738": "Login failed: wrong password",
  "text739": "Order not yet opened. Please insert your table number first.",
  "text740": "product added to list!",
  "text741": "Is the order correct?",
  "text742": "Currently available amount of credits:",
  "text743": "Last updated:",
  "text744": "Credits",
  "text745": "Many thanks for your purchase!",
  "text746": "credits were added to your account",
  "text747": "Sorry... an unexpected error occurred.",
  "text748": "Buy credits",
  "text749": "Subscriptions",
  "text750": "End subscription",
  "text751": "Credit card",
  "text752": "PayPal",
  "text753": "Bank transfer",
  "text754": "Please retry",
  "text755": "Connect with Stripe",
  "text756": "Stripe Login",
  "text757": " ",
  "text758": "Registration (or connection) was successful",
  "text759": "Payment was cancelled",
  "text760": "Please select a product",
  "text761": "Type of attribute (1=numeric, 2=false/true, 3=text)",
  "text762": "Save in image-database",





  "text0" : "ERROR: NLV_en_no_id"
}

//------- Text-IDs for localization of ISO 'de'  -----------

var texts_de = {
  "text1" : "Bestellen",
  "text2" : "Löschen",
  "text3" : "Küche",
  "text4" : "Aktuell besetzte Tische",
  "text5" : "Aktuelle Rechnungen",
  "text6" : "Übersicht",
  "text7" : "Kunden im Lokal",
  "text8" : "Tisch-Übersicht",
  "text9" : "Rechnungs-Übersicht",
  "text10" : "Aktualisierte Übersicht",
  "text11" : "Verbunden",
  "text12" : "Bestellungen",
  "text13" : "Produktverwaltung",
  "text14" : "Kategorie hinzufügen",
  "text15" : "Produkt hinzufügen",
  "text16" : "Kategorie bearbeiten",
  "text17" : "Produkt bearbeiten",
  "text18" : "Personal und Kunden",
  "text19" : "Person hinzufügen",
  "text20" : "Profile bearbeiten",
  "text21" : "Reservierungen",
  "text22" : "Layout-Verwaltung",
  "text23" : "Layout-Struktur",
  "text24" : "Layout-Farben",
  "text25" : "Layout-Bilder",
  "text26" : "Ausloggen",
  "text27" : "Produktinfo",
  "text28" : "Tisch",
  "text29" : "Anzahl offener Rechnungen",
  "text30" : "Neue Bestellung",
  "text31" : "Anzahl Personen",
  "text32" : "Text einfügen...",
  "text33" : "Rechnungs-Nummer",
  "text34" : "Erstellungs-Uhrzeit",
  "text35" : "Rechnungs-Status",
  "text36" : "Anmerkungen",
  "text37" : "Bestelltes Produkt löschen?",
  "text38" : "Produkt-Anzahl=",
  "text39" : "Tische",
  "text40" : "Tisch hinzufügen",
  "text41" : "Tisch-Daten bearbeiten",
  "text42" : "Kurzbezeichnung",
  "text43" : "Bezeichnung",
  "text44" : "Einzelplatz",
  "text45" : "Anzahl Plätze",
  "text46" : "Tisch entfernen",
  "text47" : "Sprache wechseln",
  "text48" : "Produktliste",
  "text49" : "Produkt entfernen",
  "text50" : "Produkt",
  "text51" : "Produktpreis",
  "text52" : "Vorhandene Menge",
  "text53" : "Produkt-Kategorie",
  "text54" : "Produkt-Information",
  "text55" : "Produktbeschreibung",
  "text56" : "Produkt-Bild",
  "text57" : "Produkt-Miniaturbild",
  "text58" : "Produkt-Position",
  "text59" : "Produkt-Messgrösse",
  "text60" : "Produkt-Warnmenge",
  "text61" : "ProduktName",
  "text62" : "Kategorie entfernen",
  "text63" : "Anzahl Rechnungen",
  "text64" : "Auslastung in %",
  "text65" : "Ausstehende Zahlungen",
  "text66" : "Produkte in Zubereitung",
  "text67" : "Produkte in Lieferung",
  "text68" : "Rechnungs-ID",
  "text69" : "Verstrichene Zeit in Minuten",
  "text70" : "Kellner",
  "text71" : "Status",
  "text72" : "Tisch-Nummer",
  "text73" : "Tischplatz-Nummer",
  "text74" : "Koch",
  "text75" : "Email",
  "text76" : "Vorname",
  "text77" : "Aktiviert?",
  "text78" : "Gehört zum Personal?",
  "text79" : "Ist SuperUser",
  "text80" : "Nachname",
  "text81" : "Passwort",
  "text82" : "Nutzername",
  "text83" : "Benutzer hinzufügen",
  "text84" : "Nr",
  "text85" : "Benutzer-Bild",
  "text86" : "Datum hinzugefügt",
  "text87" : "Letzter Login",
  "text88" : "Benutzer bearbeiten",
  "text89" : "Benutzer löschen",
  "text90" : "Rolle zuweisen",
  "text91" : "Kellner-ID",
  "text92" : "Kellner-Name",
  "text93" : "Tisch-Name",
  "text94" : "ID Tisch",
  "text95" : "Erstellungs-Uhrzeit",
  "text96" : "ID Rechnungsstatus",
  "text97" : "Anzahl Gäste",
  "text98" : "Minuten",
  "text99" : "Übersicht Kellner",
  "text100" : "Verwaltung",
  "text101" : "Web-Administrator",
  "text102" : "Kunde",
  "text103" : "Rolle zuweisen",
  "text104" : "Layout-Fonts",
  "text105" : "Bearbeiten",
  "text106" : "Entfernen",
  "text107" : "Unter-Kategorien",
  "text201" : "Element-Name",
  "text202" : "Wert",
  "text203" : "Default-Wert",
  "text204" : "Layout-Text",
  "text205" : "Id",
  "text206" : "Element-Name",
  "text207" : "Id des Elementtyps",
  "text208" : "Wert",
  "text209" : "Default Wert",
  "text210" : "Struktur bearbeiten",
  "text211" : "Farbe bearbeiten",
  "text212" : "Font bearbeiten",
  "text213" : "Anderes Bild wählen",
  "text214" : "Text bearbeiten",
  "text215" : "Speichern",
  "text216" : "Wert auf Default zurücksetzen",
  "text217" : "Default-Wert wieder speichern?",
  "text221" : "Zubereitung wartet",
  "text222" : "Zubereitung begonnen",
  "text223" : "Zubereitung beendet",
  "text224" : "Nachrichten",
  "text225" : "Mehr Infos",
  "text226" : "[Kategorie-Beschreibung]",
  "text227" : "Beschreibung",
  "text228" : "[Kürzel - international]",
  "text229" : "Kategoriename ",
  "text230" : "Bild",
  "text231" : "Position",
  "text232" : "Level",
  "text233" : "ID",
  "text250" : "Passwort ändern",
  "text251" : "Bitte alle Leerstellen ausfüllen.",
  "text252" : "Ungültige Zahl eingegeben.",
  "text253" : "Fehler: rc=",
  "text254" : "Unbekannte id",
  "text255" : "Datei  ",
  "text256" : " wird hochgeladen und unter folgendem Namen abgespeichert: '",
  "text257" : "Fehler: Datei ",
  "text258" : " wurde nicht hochgeladen.  Status=",
  "text259" : "Kategorie kann nicht gelöscht werden. Bitte nochmal versuchen.",
  "text260" : "Kategorie konnte  der übergeordneten Kategorie nicht zugeordnet werden. Bitte nochmal versuchen.",
  "text261" : "Kategorie konnte  von der übergeordneten Kategorie nicht entfernt werden.  Bitte nochmal versuchen.",
  "text262" : "Layout-ID ",
  "text263" : " id ist unbekannt.",
  "text264" : "Ausgewählte Datei: ",
  "text265" : "Produkt konnte  der Kategorie nicht zugeordnet werden.  Bitte nochmal versuchen.",
  "text266" : "Produkt konnte von der Kategorie nicht entfernt werden.  Bitte nochmal versuchen.",
  "text267" : "Unbekannte Produkt-id.",
  "text268" : "Bitte eine gültige Email-Adresse eingeben!",
  "text269" : "Benutzer-Id existiert nicht! Bitte nochmal prüfen!",
  "text270" : "Benutzer-Profil wurde erfolgreich geändert ",
  "text271" : "Ein unbekanntes Ereignis ist eingetreten",
  "text272" : "Drucken ",
  "text273" : "Checkout",
  "text274" : "Archiv",
  "text275" : "Es sind noch unverarbeitete Bestellungen vorhanden.",
  "text300" : "Bitte eine positive Zahl eingeben ",
  "text310" : "Übergeordnete Kategorie",
  "text311" : "Einheit",
  "text312" : "Liter",
  "text313" : "Gramm",
  "text314" : "Kilo",
  "text315" : "Dose",
  "text316" : "Flasche",
  "text317" : "Sonstige",
  "text318" : "Gallerie",
  "text319" : "Bild hochladen",
  "text320" : "Eigenschaften",
  "text321" : "Gallerie-Bild",
  "text322" : "Neue Eigenschaft hinzu",
  "text323" : "Name der Eigenschaft",
  "text324" : "Wert",
  "text325" : "Keine Eigenschaften gefunden",
  "text326" : "Als Produkt-Eigenschaft setzen",
  "text327" : "Attribut-Mnemonic",
  "text328" : "Neues Attribut definieren",
  "text329" : "Attributzuweisung löschen",
  "text330" : "Support",
  "text331" : "Ticket Nummer",
  "text332" : "Ticket-Datum",
  "text333" : "Ticket Status",
  "text334" : "Ticket wählen",
  "text335" : "Nachrichten ID",
  "text336" : "Kunden-Text",
  "text337" : "Angehängte Datei",
  "text338" : "Datum",
  "text339" : "Ticket abschicken",
  "text400" : "HTTP 400: Die Anfrage ist fehlerhaft aufgebaut.",
  "text401" : "HTTP 401: Einloggen erforderlich.",
  "text403" : "HTTP 403: Zugang verweigert.",
  "text404" : "HTTP 404: Sorry...: Die Seite wurde nicht gefunden",
  "text405" : "HTTP 405: Unzulässige HTTP-Methode",
  "text422" : "HTTP 422: Sorry...: Es gab Fehler in der Verarbeitung der Daten. Bitte nochmal versuchen.",
  "text499" : "Sorry...! Ein unerwarteter Fehler ist eingetreten.",
  "text500" : "HTTP 500: Interner Server-Fehler",
  "text533" : "HTTP 533: Server-Fehler",
  "text534" : "HTTP 534: Server-Fehler",
  "text535" : "HTTP 535: Server-Fehler",
  "text600" : "Vorschau",
  "text601" : "---------------",
  "text602" : "Abbrechen",
  "text603" : "Bestellung schliessen",
  "text604" : "Bezahlen",
  "text605" : "Weiter bestellen",
  "text606" : "Archivieren",
  "text607" : "Bestellung wurde archiviert",
  "text608" : "Passwörter stimmen nicht überein",
  "text609" : "Passwort wurde geändert",
  "text610" : "Wert wurde gespeichert",
  "text611" : "Bild wurde hochgeladen",
  "text612" : "Ausgelieferte Produkte",
  "text613" : "Durchschn. Wartezeit min.",
  "text614" : "Registrieren",
  "text615" : "Anmelden",
  "text616" : "Produkt-Seite",
  "text701" : "Registrierung",
  "text702" : "Vielen Dank für Ihre Registrierung! Wir haben Ihnen eine Email mit einem Link gesendet, der Ihnen zur Bestätigung Ihrer Anfrage dient. Falls Sie keine Email von listorante.com finden können, prüfen Sie bitte Ihren Spam-Ordner. Wir sind außerdem via Email unter support@listorante.com erreichbar.",
  "text703" : "Bestellen Sie eine listorante-Anwendung für Ihr Unternehmen",
  "text704" : "Diese Email-Adresse ist bereits registriert",
  "text705" : "Der Name dieser Domain ist nicht verfügbar",
  "text706" : "Als Sub-Domain registrieren",
  "text707" : "Sind Sie der Inhaber dieser Domain, und möchten Sie Ihre Listorante-Applikation auf Ihrem eigenen Web-Space installieren?",
  "text708" : "Sind Sie der Inhaber dieser Domain, und möchten Sie Ihre Listorante-Applikation auf einem Web-Space von Listorante installieren?",
  "text709" : "Möchten Sie lieber abbrechen, und vor der Registrierung  weitere Hilfe und Hinweise via Email erhalten?",
  "text710" : "Klicken Sie hier um unseren AGBs zuzustimmen",
  "text711" : "Unsere AGBs",
  "text712" : "Anfrage senden",
  "text713" : "Zu Ihrer Anwendung",
  "text714" : "Bestätigen",
  "text715" : "Wählen Sie die App, in die Sie sich einloggen möchten:",
  "text716" : "Ich habe bereits eine listorante-Kundennummer",
  "text717" : "Der gewünschte Domain-Name ist verfügbar",
  "text718" : "Absenden?",
  "text719" : "JA",
  "text720" : "NEIN",
  "text721" : "Dieser  Domain-Name ist nicht verfügbar",
  "text722" : "Möchten Sie es mit einem anderen Domain-Namen versuchen?",
  "text723" : "Erstellen Sie Ihre Anwendung",
  "text724" : "Name Ihres Betriebs",
  "text725" : "Bitte füllen Sie dieses Feld aus",
  "text726" : "Ihre Listorante Company-ID:",
  "text727" : "Sie haben noch keine Listorante Company-ID?",
  "text728" : "Weiter zur Erstellung",
  "text729" : "Kundennummer",
  "text730" : "Ihre listorante-Anwendung ist nun bereit. Das Initial-Passwort wurde Ihnen per Email zugesandt.",
  "text731" : "Sorry, Ihre listorante-Anwendung konnte leider nicht erstellt werden.",
  "text732" : "Unsere Email wurde an folgende Adresse gesendet:",
  "text733": "Hinzu",
  "text734": "Zurück",
  "text735": "Tour starten",
  "text736": "AGB-Text bearbeiten",
  "text737": "Privacy-Text bearbeiten",
  "text738": "Login gescheitert: falsches Passwort?",
  "text739": "Bestellung noch nicht begonnen.",
  "text740": "Produkt hinzugefügt!",
  "text741": "Ist Ihre Bestellung korrekt?",
  "text742": "Verfügbare credits:",
  "text743": "Zuletzt aktualisiert:",
  "text744": "Credits",
  "text745": "Vielen Dank für Ihren Einkauf.",
  "text746": "Credits wurden Ihrem Konto hinzugefügt",
  "text747": "Sorry... ein unerwarteter Fehler ist eingetreten.",
  "text748": "Credits kaufen",
  "text749": "Abonnements",
  "text750": "Abonnement beenden",
  "text751": "Kreditkarte",
  "text752": "PayPal",
  "text753": "Überweisung",
  "text754": "Bitte nochmal versuchen",
  "text755": "Mit Stripe verknüpfen",
  "text756": "Stripe Login",
  "text757": " ",
  "text758": "Registrierung (oder Verknüpfung) war erfolgreich",
  "text759": "Zahlung wurde abgebrochen",
  "text760": "Bitte ein Produkt auswählen",
  "text761": "Art der Eigenschaft (1=Numerisch, 2=Wahr/Falsch, 3=Text)",
  "text762": "In Bilder-Datenbank speichern",



  "text0" : "ERROR: NLV_de_no_id"
}


// ------- Text-IDs for localization of ISO 'it' -----------

var texts_it = {
  "text1" : "Invia Ordine",
  "text2" : "Rimuovi",
  "text3" : "Cucina",
  "text4" : "Tavoli occupati",
  "text5" : "Conti attuali",
  "text6" : "Panoramica",
  "text7" : "Clienti attualmente presenti",
  "text8" : "Panoramica tavoli",
  "text9" : "Panoramica conti",
  "text10" : "Situazione corrente",
  "text11" : "Connesso",
  "text12" : "Ordini",
  "text13" : "Gestione Prodotti",
  "text14" : "Aggiungi Categoria",
  "text15" : "Aggiungi Prodotto",
  "text16" : "Modifica Categorie",
  "text17" : "Modifica Prodotti",
  "text18" : "Personale e clienti",
  "text19" : "Aggiungere Persona",
  "text20" : "Gestione Profili",
  "text21" : "Prenotazioni",
  "text22" : "Gestione Layout",
  "text23" : "Struttura del Layout",
  "text24" : "Colori del Layout",
  "text25" : "Immagini del Layout",
  "text26" : "Esci",
  "text27" : "Informazioni sui prodotti",
  "text28" : "Tavolo",
  "text29" : "Conti aperti",
  "text30" : "Nuovo ordinativo",
  "text31" : "Numero persone",
  "text32" : "Inserisci testo...",
  "text33" : "Conto ",
  "text34" : "Orario di creazione",
  "text35" : "Situazione",
  "text36" : "Note",
  "text37" : "Cancellare il prodotto ordinato?",
  "text38" : "Quantità prodotti",
  "text39" : "Tavoli",
  "text40" : "Nuovo tavolo",
  "text41" : "Modifica dati tavolo",
  "text42" : "Nome abbreviato",
  "text43" : "Descrizione",
  "text44" : "Posto singolo",
  "text45" : "Numero posti",
  "text46" : "Rimouvere tavolo",
  "text47" : "Cambia lingua",
  "text48" : "Lista Prodotti",
  "text49" : "Cancella prodotto",
  "text50" : "Nome del prodotto",
  "text51" : "Prezzo ",
  "text52" : "Quantità disponibile",
  "text53" : "Categoria prodotto",
  "text54" : "Informazione prodotto",
  "text55" : "Descrizione prodotto",
  "text56" : "Immagine del prodotto",
  "text57" : "Immagine miniaturizzata del prodotto",
  "text58" : "Posizione prodotto",
  "text59" : "Misura unitaria ",
  "text60" : "Limite di quantità",
  "text61" : "Nome",
  "text62" : "Cancella categoria",
  "text63" : "Totale Conti",
  "text64" : "Utilizzazione in %",
  "text65" : "Pagamenti attesi",
  "text66" : "Prodotti ordinati",
  "text67" : "Prodotti in consegna",
  "text68" : "Identificativo conto",
  "text69" : "Tempo trascorso (minuti)",
  "text70" : "Cameriere",
  "text71" : "Status",
  "text72" : "Numero del tavolo",
  "text73" : "Numero posto",
  "text74" : "Cuoco",
  "text75" : "Email",
  "text76" : "Nome",
  "text77" : "Attivato?",
  "text78" : "Fa parte del personale?",
  "text79" : "È utente superUser?",
  "text80" : "Cognome",
  "text81" : "La tua password",
  "text82" : "Nome utente",
  "text83" : "Aggiungi utente",
  "text84" : "Nr.",
  "text85" : "Immagine Profilo",
  "text86" : "Data di inserimento",
  "text87" : "Ultimo Login",
  "text88" : "Modifica dati profilo",
  "text89" : "Cancella profilo",
  "text90" : "Assegna ruolo",
  "text91" : "Identificativo cameriere",
  "text92" : "Nome del cameriere",
  "text93" : "Nome del tavolo",
  "text94" : "ID del tavolo",
  "text95" : "Momento di creazione",
  "text96" : "ID dello status del conto",
  "text97" : "Numero clienti",
  "text98" : "Minuti",
  "text99" : "Panoramica cameriere",
  "text100" : "Amministratore",
  "text101" : "Amministratore Web",
  "text102" : "Cliente",
  "text103" : "Assegna ruolo",
  "text104" : "Fonts del layout",
  "text105" : "Modifica",
  "text106" : "Rimuovi",
  "text107" : "Sotto-categorie",
  "text201" : "Nome dell'elemento",
  "text202" : "Valore",
  "text203" : "Valore Default",
  "text204" : "Testo",
  "text205" : "Identificatore",
  "text206" : "Nome elemento",
  "text207" : "tipologia dell'elemento",
  "text208" : "valore",
  "text209" : "Valore Default",
  "text210" : "Modifica struttura",
  "text211" : "Modifica colore",
  "text212" : "Modifica font",
  "text213" : "Scegli altra immagine",
  "text214" : "Modifica testo",
  "text215" : "Salva",
  "text216" : "Reimposta il valore default",
  "text217" : "Reimpostare il valore default?",
  "text221" : "preparazione in attesa",
  "text222" : "preparazione in corso",
  "text223" : "preparazione completata",
  "text224" : "Messaggi",
  "text225" : "Dettagli",
  "text226" : "[Descrizione]",
  "text227" : "Descrizione",
  "text228" : "[Nome breve internazionalizzato]",
  "text229" : "Nome",
  "text230" : "Immagine",
  "text231" : "Posizione",
  "text232" : "Livello",
  "text233" : "Numero identificativo",
  "text250" : "Cambia password",
  "text251" : "Completare i campi mancanti.",
  "text252" : "Numero non valido",
  "text253" : "Errore: rc=",
  "text254" : "L'dentificatore non é definito.",
  "text255" : "Il file ",
  "text256" : " sta per essere caricato e verrà salvato col nome seguente: '",
  "text257" : "Errore: Il file ",
  "text258" : " non é stato caricato.  Status=",
  "text259" : "Impossibile cancellare la categoria. Riprova.",
  "text260" : "Impossibile assegnare la categoria a quella selezionata soprastante. Riprova.",
  "text261" : "Impossibile sganciare la categoria da quella selezionata soprastante. Riprova.",
  "text262" : "ID del Layout ",
  "text263" : "  non é definito.",
  "text264" : "File selezionato: ",
  "text265" : "Impossibile assegnare il prodotto alla categoria selezionata. Riprova.",
  "text266" : "Impossibile sganciare il prodotto dalla categoria selezionata. Riprova",
  "text267" : "L'identificatore del prodotto non é definito.",
  "text268" : "Inserire una Email valida!",
  "text269" : "L'identificatore utente non è stato trovato! Ripetere la verifica.",
  "text270" : "Il profilo utente é stato modificato.",
  "text271" : "Si é verficato un evento inatteso ",
  "text272" : "Stampa",
  "text273" : "Checkout",
  "text274" : "Ordini archiviati",
  "text275" : "Esistono ancora ordini inevasi da cancellare o inviare.",
  "text300" : "Inserire un numero positivo",
  "text310" : "Categoria sovrastante",
  "text311" : "unitá",
  "text312" : "litro",
  "text313" : "grammi",
  "text314" : "kilogrammi",
  "text315" : "barattolo",
  "text316" : "bottiglia",
  "text317" : "altro",
  "text318" : "Galleria immagini",
  "text319" : "Carica immagine",
  "text320" : "Proprietá",
  "text321" : "Immagine di galleria",
  "text322" : "Aggiungere proprietá",
  "text323" : "Nome della proprietá",
  "text324" : "Valore",
  "text325" : "Non ci sono proprietá",
  "text326" : "Assegna come proprietá prodotto",
  "text327" : "Codice mnemonico della proprietá",
  "text328" : "Definisci nuova proprietá",
  "text329" : "Cancella proprietá",
  "text330" : "Support",
  "text331" : "Numero della richiesta",
  "text332" : "Data di richiesta",
  "text333" : "Status della richiesta",
  "text334" : "Seleziona richiesta",
  "text335" : "ID messaggio",
  "text336" : "Testo",
  "text337" : "File incluso",
  "text338" : "Data",
  "text339" : "Invia richiesta",
  "text400" : "HTTP 400: Errore di sintassi. La richiesta non può essere soddisfatta.",
  "text401" : "HTTP 401: É necessario ri-effettuare il login ",
  "text403" : "HTTP 403: Autorizzazione negata",
  "text404" : "HTTP 404: Pagina inesistente",
  "text405" : "HTTP 405: Si é tentato di utilizzare un metodo http inappropriato",
  "text422" : "HTTP 422: Spiacenti...: i dati inviati contenevano errori. Ritentare, per favore.",
  "text499" : "Spiacenti...! Si é verificato un errore inatteso.",
  "text500" : "HTTP 500: Errore interno del server",
  "text533" : "HTTP 533: Errore server ",
  "text534" : "HTTP 534: Errore server ",
  "text535" : "HTTP 535: Errore server ",
  "text600" : "Aggiorna",
  "text601" : "---------------",
  "text602" : "Ritorna",
  "text603" : "Chiudere ordinativo",
  "text604" : "Pagamento",
  "text605" : "Riaprire ordinativo",
  "text606" : "Archiviare",
  "text607" : "L'ordine é stato archiviato",
  "text608" : "Le due password non sono identiche",
  "text609" : "La password è stata cambiata",
  "text610" : "Il valore é stato salvato",
  "text611" : "L'immagine é stata caricata",
  "text612" : "Prodotti consegnati",
  "text613" : "Tempo medio di attesa in minuti",
  "text614" : "Iscriviti",
  "text615" : "Accedi",
  "text616" : "Link del prodotto",
  "text701" : "Registrazione",
  "text702" : "Grazie per la tua registrazione! Ti abbiamo inviato una mail contenente un apposito link per confermare la tua richiesta. Se non trovi nessuna mail di listorante.com, gentilmente verifica se la mail é finita nello spam. In ogni caso, puoi sempre inviarci una mail a support@listorante.com.",
  "text703" : "Richiedi un'applicazione listorante per la tua azienda",
  "text704" : "Questo indirizzo Email é stato giá registrato",
  "text705" : "Il nome di questa domain non é disponibile",
  "text706" : "Registra come sub-domain",
  "text707" : "Sei il proprietario di questa domain, e vuoi installare la tua applicazione listorante sul tuo spazio web?",
  "text708" : "Sei il proprietario di questa domain, e vuoi installare l'applicazione sullo spazio web di listorante.com?",
  "text709" : "Preferisci fermare un attimo qui la registrazione e ricevere piú informazioni ed aiuto via Email?",
  "text710" : "Clicca qui per acconsentire alle nostre condizioni contrattuali",
  "text711" : "Le nostre condizioni contrattuali.",
  "text712" : "Invia richiesta",
  "text713" : "Accedi alla tua applicazione",
  "text714" : "Conferma",
  "text715" : "Seleziona l'applicazione che vuoi accedere:",
  "text716" : "Ho giá un codice listorante",
  "text717" : "Il nome del dominio desiderato é diponibile",
  "text718" : "Inviare?",
  "text719" : "SI",
  "text720" : "NO",
  "text721" : "Il nome del dominio desiderato non é diponibile",
  "text722" : "Vuoi tentare con un altro nome di dominio?",
  "text723" : "Crea la tua applicazione",
  "text724" : "Nome della tua azienda",
  "text725" : "Questo campo é obbligatorio",
  "text726" : "Il tuo Codice identificativo listorante:",
  "text727" : "Non hai ancora un codice aziendale listorante?",
  "text728" : "Richiedilo qui",
  "text729" : "Codice cliente",
  "text730" : "La tua applicazione listorante è pronta per essere utilizzata. La password iniziale ti é stata inviata via Email.",
  "text731" : "La creazione della tua applicazione listorante purtroppo non é riuscita.",
  "text732" : "La nostra mail per la richiesta di conferma é stata inviata a questo indirizzo:",
  "text733": "Aggiungi",
  "text734": "Ritorna",
  "text735": "Esplora",
  "text736": "Modifica testo condizioni contrattuali",
  "text737": "Modifica testo privacy",
  "text738": "Login non riuscito: password errata?",
  "text739": "Inserire il numero del tavolo, per favore",
  "text740": "prodotto aggiunto in lista!",
  "text741": "É corretto l'ordine ?",
  "text742": "Crediti disponbili:",
  "text743": "Ultimo aggiornamento:",
  "text744": "Credits",
  "text745": "Grazie per l'acquisto!",
  "text746": "credits sono stati aggiunti.",
  "text747": "Siamo spiacenti... si é verificato un'errore inatteso.",
  "text748": "Ricariche",
  "text749": "Abbonamenti",
  "text750": "Termina Abbonamento",
  "text751": "Carta di credito",
  "text752": "PayPal",
  "text753": "Bonifico",
  "text754": "Riprova, per favore",
  "text755": "Connetti Stripe",
  "text756": "Login Stripe",
  "text757": " ",
  "text758": "La registrazione (o connessione) ha avuto successo",
  "text759": "Il pagamento é stato annullato",
  "text760": "Scegliere un prodotto",
  "text761": "Tipo di proprietà (1=numerica, 2=vero/falso, 3=Testo)",
  "text762": "Salva immagine in banca-dati",



  "text0" : "ERROR: NLV_it_no_id"
};
/*-----------------------------------*/ 
/* localization_functions.js */ 
release = "d1a6f_5";
const langDebug = true;


function setElemValue(id) {
    try {
        document.getElementById('text' + id).value = getTextById(id);
    } catch (err) {
        if (langDebug) console.warn("Could not set text" + id);
    }
}

function setElemTextIgnoreErrors(id) {
    //if (langDebug) console.log('Setting text for element with id "text' + id + '" ');
    try {
        document.getElementById('text' + id).innerText = getTextById(id);
        if (langDebug) console.log("... text" + id + " was set.");
    } catch (err) {
        if (langDebug) console.log("--- Could not set text" + id);
    }
}

function setElemText(id) {
    let elem;
    try {
        elem = document.getElementById('text' + id);
    } catch (error) {
        if (langDebug) console.error(error);
    }
    if (elem) {
        try {
            elem.innerText = getTextById(id);
        } catch (err2) {
            try {
                if (langDebug) console.error("Inner text for id 'text" + id + "'+could not be set. Try to set text-value for element with  id 'text" + id
                    + "': " + JSON.stringify(err2));
                setElemValue(id);
            } catch (err3) {
                if (langDebug) console.error("Could not set text for id 'text" + id + "': " + JSON.stringify(err3));
            }
        }
    } else {
        if (langDebug) console.error("Localization-ERROR: id 'text" + id
            + "' does not exist in this document.");
    }
}


$(document)
    .on(
        'click',
        '.set-language',
        function () {
            var rowDiv = languageSelect.pre;

            $
                .each(
                    languageSelect.languages,
                    function (key, lang) {
                        rowDiv += `<a href="javascript:void(0)" name="language" class="selectTheLanguage ${languageSelect.styleClass}" data-lang="${lang.id}" data-callback="${languageSelect.callback}" >&nbsp;${lang.name}</a>&nbsp;`;
                    });
            rowDiv += languageSelect.suf;
            $('#' + languageSelect.targetDiv).html(rowDiv);
        });

/*$(document).on('change', '#langSelect', function () {
    const radios = document.getElementsByName('language');

    let i = 0;
    const length = radios.length;
    for (; i < length; i++) {
        if (radios[i].checked) {
            // do whatever you want with the checked radio
            const href = new URL(location.href);

            href.searchParams.set('lang', radios[i].id);
            location.href = href.toString();

            // only one radio can be logically checked, don't check the rest
            break;
        }
    }
});*/

function getUrlVars() {
    const vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function languageIsAvailable(lan) {
    let found = false;
    for (let i = 0; i < languageSelect.languages.length; i++) {
        if (languageSelect.languages[i].id === lan) {
            found = true;
            break;
        }
    }
    if (found) return true;
    else return false;
}

function getLocale() {
    const theLanguage = getUrlVars()["lang"];
    if ((theLanguage) && (theLanguage.length > 1) && languageIsAvailable(theLanguage)) {
        locale = theLanguage;
        sessionStorage.setItem("currentLanguage", locale);
        if (langDebug) console.log("Switched locale via url parameter to '" + locale + "'");
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        //return locale;
    }
    if (locale && languageIsAvailable(locale)) {
        sessionStorage.setItem("currentLanguage", locale);
        if (langDebug) console.log("Locale is already set to: '" + locale + "'");
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        //return locale;
    }
    if (navigator.languages !== undefined) {
        locale = navigator.languages[0].substring(0, 2);
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        if (langDebug) console.log("Default locale selected from first element in browser-list: '" + locale + "'");
    } else if (navigator.language !== undefined) {
        if (langDebug) console.log("Default browser-locale selected: '" + locale + "'");
        locale = navigator.language.substring(0, 2);
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
    }
    if (!languageIsAvailable(locale)) //return locale;
    //else
        {
        console
            .log("Locale '"
                + locale + "' is not available for this application. Returning default-locale: 'en'");
        locale = "en";
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        //return locale;
    }
    console.log("Detected locale: "+locale);
    return locale;
}

var locale = getLocale();

function getCurrentLanguage() {
    const uriLanguage = getUrlVars()["lang"];
    const sessionLanguage = sessionStorage.getItem("currentLanguage");
    if (uriLanguage && languageIsAvailable(uriLanguage)) {
        if (langDebug) console.log("Got current language from uri: " + uriLanguage);
        return uriLanguage;
    } else if (sessionLanguage && languageIsAvailable(sessionLanguage)) {
        if (langDebug) console.log("Got current language from session: " + sessionLanguage);
        return sessionLanguage;
    } else
        return locale;
}


function getCurrency() {
    switch (locale) {
        case ("de"):
            return " €";
        case ("en"):
            return " €";
        case ("it"):
            return " €";
        case ("us"):
            return " $";
        case ("uk"):
            return " £";
        default:
            return "  ";
    }
}

function getTextById(id) {
    const theText = getLocaleTextById(id);
    if (theText === null)
        return "ERROR_NLV_" + locale + "_id_" + id;
    return theText;
}

function getLocaleTextById(id) {
    const lang = getCurrentLanguage();
    switch (lang) {
        case ("de"):
            if (typeof texts_de !== 'undefined')
                return texts_de["text" + id];
            else
                return null;
            break;
        case ("it"):
            if (typeof texts_it !== 'undefined')
                return texts_it["text" + id];
            else
                return null;
            break;
        case ("us"):
            if (typeof texts_us !== 'undefined')
                return texts_us["text" + id];
            else
                return null;
            break;
        case ("en"):
            if (typeof texts_en !== 'undefined')
                return texts_en["text" + id];
            else
                return null;
            break;
        case ("uk"):
            if (typeof texts_uk !== 'undefined')
                return texts_uk["text" + id];
            else
                return null;
            break;
        default:
            //if (typeof texts_en !== 'undefined')
            //{
            //    console.error("No language "+lang+" is defined.")
            //    return texts_en["text" + id];
            //}
            //else
            return "ERROR: NOLOC_" + lang;
    }

}

function getText(mnemonic) {
    const theText = getLocaleTextByMnemonic(mnemonic);
    if (theText === null)
        return "ERROR_NLV_" + locale + "_m_" + mnemonic;
    return theText;
}

function getLocaleTextByMnemonic(mnemonic) {
    const lang = getCurrentLanguage();
    switch (lang) {
        case ("de"):
            if (typeof mnemonics_de !== 'undefined')
                return mnemonics_de[mnemonic];
            else
                return null;
            break;
        case ("it"):
            if (typeof mnemonics_it !== 'undefined')
                return mnemonics_it[mnemonic];
            else
                return null;
            break;
        case ("us"):
            if (typeof mnemonics_us !== 'undefined')
                return mnemonics_us[mnemonic];
            else
                return null;
            break;
        case ("en"):
            if (typeof mnemonics_en !== 'undefined')
                return mnemonics_en[mnemonic];
            else
                return null;
            break;
        case ("uk"):
            if (typeof mnemonics_uk !== 'undefined')
                return mnemonics_uk[mnemonic];
            else
                return null;
            break;
        default:
            return "ERROR: NOLOC_" + locale;
    }
};

function setLanguage(id) {
    sessionStorage.setItem("currentLanguage", id);
    apiCall_elisto_public_assignedtexts(getLayoutId(), function (data) {
        for (let k = 0; k < data.count; k++) {
            let languageElement = document.getElementById(data.rows[k].s[0]);
            if (languageElement) languageElement.innerText = data.rows[k].s[3];
        }
        if (languageSelect.preCallback) {
            languageSelect.preCallback();
        }
        if (getUrlVars()['lang']) {
            const href = new URL(location.href);
            href.searchParams.set('lang', id);
            location.href = href.toString();
        }
        setTexts();
        if (languageSelect.postCallback) {
            languageSelect.postCallback();
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

jQuery(document)
    .on(
        'click',
        '.selectTheLanguage',
        function () {
            const id = $(this).attr('data-lang');
            const func = $(this).attr('data-languageCallBackFunction');
            setLanguage(id, func);
        });


/*-----------------------------------*/ 
/* apiFunctions_global_public.js */ 
/*-----------------------------------*/ 
/* apiFunctions_elisto_admin.js */ 
/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Admin'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Admin.apiCall1
	Create a new product
	***************************************/
function apiCall_elisto_admin_newproduct
	(price,
	prod_desc,
	prod_image,
	prod_info,
	prod_label,
	prod_name,
	qty_in_stock,
	qty_threshold,
	unit,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"price" : price,
	"prod_desc" : prod_desc,
	"prod_image" : prod_image,
	"prod_info" : prod_info,
	"prod_label" : prod_label,
	"prod_name" : prod_name,
	"qty_in_stock" : qty_in_stock,
	"qty_threshold" : qty_threshold,
	"unit" : unit
	};
	return apiCall("elisto.Admin.apiCall1","/4/admin/newproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall2
	Create a new product-category underneath an existing parent category.
		('idproductcategory' here is the parent-id. The newly created category will be assigned a new id). 
		 
	***************************************/
function apiCall_elisto_admin_newcategory
	(hierarchy_1,
	hierarchy_2,
	hierarchy_3,
	hierarchy_4,
	hierarchy_5,
	idproductcategory,
	level,
	position,
	prodcatdesc,
	prodcatdesc_locale,
	prodcatimage,
	prodcatname,
	prodcatname_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"hierarchy_1" : hierarchy_1,
	"hierarchy_2" : hierarchy_2,
	"hierarchy_3" : hierarchy_3,
	"hierarchy_4" : hierarchy_4,
	"hierarchy_5" : hierarchy_5,
	"idproductcategory" : idproductcategory,
	"level" : level,
	"position" : position,
	"prodcatdesc" : prodcatdesc,
	"prodcatdesc_locale" : prodcatdesc_locale,
	"prodcatimage" : prodcatimage,
	"prodcatname" : prodcatname,
	"prodcatname_locale" : prodcatname_locale
	};
	return apiCall("elisto.Admin.apiCall2","/4/admin/newcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall3
	Create new staff-person
	***************************************/
function apiCall_elisto_admin_newstaff
	(email,
	first_name,
	is_active,
	is_superuser,
	last_name,
	password,
	username,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"is_active" : is_active,
	"is_superuser" : is_superuser,
	"last_name" : last_name,
	"password" : password,
	"username" : username
	};
	return apiCall("elisto.Admin.apiCall3","/4/admin/newstaff", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall4
	Delete a staff-person
	***************************************/
function apiCall_elisto_admin_deletestaff
	(USERIDVALUE,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"USERIDVALUE" : USERIDVALUE
	};
	return apiCall("elisto.Admin.apiCall4","/4/admin/deletestaff", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall5
	Assign role to staff person
	***************************************/
function apiCall_elisto_admin_assignrole
	(roleid,
	userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid,
	"userid" : userid
	};
	return apiCall("elisto.Admin.apiCall5","/4/admin/assignrole", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall6
	Revoke role from staff person
	***************************************/
function apiCall_elisto_admin_revokerole
	(roleid,
	userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid,
	"userid" : userid
	};
	return apiCall("elisto.Admin.apiCall6","/4/admin/revokerole", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall7
	select a layout
	***************************************/
function apiCall_elisto_admin_setlayoutid
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall7","/4/admin/setlayoutid", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall8
	select all possible structures of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutstructures
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall8","/4/admin/layoutstructures", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall9
	select all possible images of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutimages
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall9","/4/admin/layoutimages", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall10
	select all possible colors of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutcolors
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall10","/4/admin/layoutcolors", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall11
	Update a layout-text. Works currently only for texts. This call nNeeds to be amende for superadmin
	***************************************/
function apiCall_elisto_admin_updatelayout
	(layoutelementname,
	layoutid,
	value,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutelementname" : layoutelementname,
	"layoutid" : layoutid,
	"value" : value
	};
	return apiCall("elisto.Admin.apiCall11","/4/admin/updatelayout", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall13
	edit a department
	***************************************/
function apiCall_elisto_admin_editdepartment
	(cnt_participants,
	description,
	description_locale,
	iddepartment,
	isonlyone,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"cnt_participants" : cnt_participants,
	"description" : description,
	"description_locale" : description_locale,
	"iddepartment" : iddepartment,
	"isonlyone" : isonlyone
	};
	return apiCall("elisto.Admin.apiCall13","/4/admin/editdepartment", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall14
	create a new department
	***************************************/
function apiCall_elisto_admin_createdepartment
	(cnt_participants,
	description,
	description_locale,
	isonlyone,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"cnt_participants" : cnt_participants,
	"description" : description,
	"description_locale" : description_locale,
	"isonlyone" : isonlyone
	};
	return apiCall("elisto.Admin.apiCall14","/4/admin/createdepartment", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall15
	Delete a department
	***************************************/
function apiCall_elisto_admin_deletedepartment
	(iddepartment,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iddepartment" : iddepartment
	};
	return apiCall("elisto.Admin.apiCall15","/4/admin/deletedepartment", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall16
	Update profile data
	***************************************/
function apiCall_elisto_admin_editprofile
	(email,
	first_name,
	iduser,
	image,
	is_active,
	is_staff,
	last_name,
	username,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"iduser" : iduser,
	"image" : image,
	"is_active" : is_active,
	"is_staff" : is_staff,
	"last_name" : last_name,
	"username" : username
	};
	return apiCall("elisto.Admin.apiCall16","/4/admin/editprofile", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall17
	Edit a product-category
	***************************************/
function apiCall_elisto_admin_editcategory
	(hierarchy_1,
	hierarchy_2,
	hierarchy_3,
	hierarchy_4,
	hierarchy_5,
	idproductcategory,
	idstyle,
	position,
	prodcatdesc,
	prodcatdesc_locale,
	prodcatimage,
	prodcatname,
	prodcatname_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"hierarchy_1" : hierarchy_1,
	"hierarchy_2" : hierarchy_2,
	"hierarchy_3" : hierarchy_3,
	"hierarchy_4" : hierarchy_4,
	"hierarchy_5" : hierarchy_5,
	"idproductcategory" : idproductcategory,
	"idstyle" : idstyle,
	"position" : position,
	"prodcatdesc" : prodcatdesc,
	"prodcatdesc_locale" : prodcatdesc_locale,
	"prodcatimage" : prodcatimage,
	"prodcatname" : prodcatname,
	"prodcatname_locale" : prodcatname_locale
	};
	return apiCall("elisto.Admin.apiCall17","/4/admin/editcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall18
	Edit a product
	***************************************/
function apiCall_elisto_admin_editproduct
	(idlabel,
	idprodstyle,
	idproduct,
	image,
	image_thumb,
	position,
	price,
	proddesc_locale,
	prodinfo_locale,
	prodname,
	prodname_locale,
	qty_in_stock,
	qty_unit,
	threshold_qty,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlabel" : idlabel,
	"idprodstyle" : idprodstyle,
	"idproduct" : idproduct,
	"image" : image,
	"image_thumb" : image_thumb,
	"position" : position,
	"price" : price,
	"proddesc_locale" : proddesc_locale,
	"prodinfo_locale" : prodinfo_locale,
	"prodname" : prodname,
	"prodname_locale" : prodname_locale,
	"qty_in_stock" : qty_in_stock,
	"qty_unit" : qty_unit,
	"threshold_qty" : threshold_qty
	};
	return apiCall("elisto.Admin.apiCall18","/4/admin/editproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall19
	Assign a product to a category
	***************************************/
function apiCall_elisto_admin_setproductcategory
	(ididhierarchy,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ididhierarchy" : ididhierarchy,
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall19","/4/admin/setproductcategory", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall20
	Remove a product from a category
	***************************************/
function apiCall_elisto_admin_unsetproductcategory
	(ididhierarchy,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ididhierarchy" : ididhierarchy,
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall20","/4/admin/unsetproductcategory", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall21
	Admin  Dashboard 
	***************************************/
function apiCall_elisto_admin_admindash
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall21","/4/admin/admindash", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall22
	select all items currently in pipeline
	***************************************/
function apiCall_elisto_admin_orders
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall22","/4/admin/orders", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall23
	Define a new product attribute
	***************************************/
function apiCall_elisto_admin_defineattribute
	(attribute_type,
	attributemnemonic,
	productattributename_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attribute_type" : attribute_type,
	"attributemnemonic" : attributemnemonic,
	"productattributename_locale" : productattributename_locale
	};
	return apiCall("elisto.Admin.apiCall23","/4/admin/defineattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall24
	Assign a  product attribute
	***************************************/
function apiCall_elisto_admin_setattribute
	(idattribute,
	idproduct,
	productattributevalue_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct,
	"productattributevalue_locale" : productattributevalue_locale
	};
	return apiCall("elisto.Admin.apiCall24","/4/admin/setattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall25
	De-assign a  product attribute
	***************************************/
function apiCall_elisto_admin_unsetattribute
	(idattribute,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall25","/4/admin/unsetattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall26
	Delete an attribute-definition
	***************************************/
function apiCall_elisto_admin_deleteattributedefinition
	(idproductattributename,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproductattributename" : idproductattributename
	};
	return apiCall("elisto.Admin.apiCall26","/4/admin/deleteattributedefinition", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall27
	Insert a new image for a product
	***************************************/
function apiCall_elisto_admin_insertproductimage
	(idproduct,
	imagepath,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct,
	"imagepath" : imagepath
	};
	return apiCall("elisto.Admin.apiCall27","/4/admin/insertproductimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall28
	Delete an image of a product
	***************************************/
function apiCall_elisto_admin_deleteproductimage
	(idlnk_product_image,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlnk_product_image" : idlnk_product_image
	};
	return apiCall("elisto.Admin.apiCall28","/4/admin/deleteproductimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall29
	Define a new localized text 
	***************************************/
function apiCall_elisto_admin_newText
	(id,
	mnemonic,
	text_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id,
	"mnemonic" : mnemonic,
	"text_locale" : text_locale
	};
	return apiCall("elisto.Admin.apiCall29","/4/admin/newText", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall30
	Update a localized text 
	***************************************/
function apiCall_elisto_admin_editText
	(id,
	mnemonic,
	text_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id,
	"mnemonic" : mnemonic,
	"text_locale" : text_locale
	};
	return apiCall("elisto.Admin.apiCall30","/4/admin/editText", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall31
	select all user profiles 
	***************************************/
function apiCall_elisto_admin_userprofiles
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall31","/4/admin/userprofiles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall32
	select all roles
	***************************************/
function apiCall_elisto_admin_userroles
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall32","/4/admin/userroles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall33
	select roles of a given user
	***************************************/
function apiCall_elisto_admin_getuserroles
	(userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"userid" : userid
	};
	return apiCall("elisto.Admin.apiCall33","/4/admin/getuserroles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall34
	select owners of a given role
	***************************************/
function apiCall_elisto_admin_getroleowners
	(roleid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid
	};
	return apiCall("elisto.Admin.apiCall34","/4/admin/getroleowners", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall35
	select all possible fonts of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutfonts
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall35","/4/admin/layoutfonts", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall36
	select all possible texts of the selected layout
	***************************************/
function apiCall_elisto_admin_layouttexts
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall36","/4/admin/layouttexts", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall37
	delete a product
	***************************************/
function apiCall_elisto_admin_deleteproduct
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall37","/4/admin/deleteproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall38
	select all css-elements of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutelements
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall38","/4/admin/layoutelements", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall39
	move an existing category under a new parent category
	***************************************/
function apiCall_elisto_admin_linkcategory
	(idcategory,
	idparentcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory,
	"idparentcategory" : idparentcategory
	};
	return apiCall("elisto.Admin.apiCall39","/4/admin/linkcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall40
	delete a category
	***************************************/
function apiCall_elisto_admin_deletecategory
	(idcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory
	};
	return apiCall("elisto.Admin.apiCall40","/4/admin/deletecategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall41
	Edit a  product attribute value
	***************************************/
function apiCall_elisto_admin_editattribute
	(idattribute,
	idproduct,
	value,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct,
	"value" : value
	};
	return apiCall("elisto.Admin.apiCall41","/4/admin/editattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall42
	Edit a  product attribute name
	***************************************/
function apiCall_elisto_admin_editattributename
	(attributename,
	idproductattributename,
	mnemonic,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attributename" : attributename,
	"idproductattributename" : idproductattributename,
	"mnemonic" : mnemonic
	};
	return apiCall("elisto.Admin.apiCall42","/4/admin/editattributename", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall43
	Update a css-file with the saved element-types
	***************************************/
function apiCall_elisto_admin_updatecss
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall43","/4/admin/updatecss", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall44
	Get the css for a specific layoutid
	***************************************/
function apiCall_elisto_admin_getcss
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall44","/4/admin/getcss", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall45
	Show all open tickets
	***************************************/
function apiCall_elisto_admin_tickets
	(customerid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid
	};
	return apiCall("elisto.Admin.apiCall45","/4/admin/tickets", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall46
	Create a new support-ticket
	***************************************/
function apiCall_elisto_admin_createticket
	(customerid,
	ticketdefid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid,
	"ticketdefid" : ticketdefid
	};
	return apiCall("elisto.Admin.apiCall46","/4/admin/createticket", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall47
	Upload text and or data for a support-ticket
	***************************************/
function apiCall_elisto_admin_addticketdata
	(attachedfile,
	customertext,
	ticketid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attachedfile" : attachedfile,
	"customertext" : customertext,
	"ticketid" : ticketid
	};
	return apiCall("elisto.Admin.apiCall47","/4/admin/addticketdata", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall48
	Get all data of a support-ticket
	***************************************/
function apiCall_elisto_admin_ticketdata
	(ticketid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ticketid" : ticketid
	};
	return apiCall("elisto.Admin.apiCall48","/4/admin/ticketdata", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall49
	Show which services can be requested
	***************************************/
function apiCall_elisto_admin_ticketdef
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall49","/4/admin/ticketdef", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall50
	Show shopping cart of a customer 
	***************************************/
function apiCall_elisto_admin_showcart
	(billnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"billnumber" : billnumber
	};
	return apiCall("elisto.Admin.apiCall50","/4/admin/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall51
	get customer-ID
	***************************************/
function apiCall_elisto_admin_customerid
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall51","/4/admin/customerid", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall52
	change user password
	***************************************/
function apiCall_elisto_admin_changeuserpassword
	(iduser,
	password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iduser" : iduser,
	"password" : password
	};
	return apiCall("elisto.Admin.apiCall52","/4/admin/changeuserpassword", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall53
	store a new string for a certain context
	***************************************/
function apiCall_elisto_admin_storetext
	(idsemantic,
	stringvalue,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idsemantic" : idsemantic,
	"stringvalue" : stringvalue
	};
	return apiCall("elisto.Admin.apiCall53","/4/admin/storetext", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall54
	update a string for a certain context
	***************************************/
function apiCall_elisto_admin_updatetext
	(idsemantic,
	stringvalue,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idsemantic" : idsemantic,
	"stringvalue" : stringvalue
	};
	return apiCall("elisto.Admin.apiCall54","/4/admin/updatetext", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall55
	update a settings-value
	***************************************/
function apiCall_elisto_admin_setcustomerpassword
	(setting,
	val,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"setting" : setting,
	"val" : val
	};
	return apiCall("elisto.Admin.apiCall55","/4/admin/setcustomerpassword", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall56
	Assign an employee-id to a department 
	***************************************/
function apiCall_elisto_admin_setcustomerdepartment
	(iddepartment,
	iduser,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iddepartment" : iddepartment,
	"iduser" : iduser
	};
	return apiCall("elisto.Admin.apiCall56","/4/admin/setcustomerdepartment", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall57
	Show the current amount of available credits
	***************************************/
function apiCall_elisto_admin_showCredits
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall57","/4/admin/showCredits", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall58
	Select the user-data of the currently logged-in Administrator
	***************************************/
function apiCall_elisto_admin_adminuserdata
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall58","/4/admin/adminuserdata", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall59
	Insert the stripe account id
	***************************************/
function apiCall_elisto_admin_setstripeid
	(stripe_user_account,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"stripe_user_account" : stripe_user_account
	};
	return apiCall("elisto.Admin.apiCall59","/4/admin/setstripeid", dataObject, callBack, httpErrorCallBack,"post");
}


/*-----------------------------------*/ 
/* apiFunctions_elisto_customer.js */ 
/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Customer'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Customer.apiCall1
	Add a product-item to the order-list.
	***************************************/
function apiCall_elisto_customer_addtoproductlist
	(departmentnumber,
	executivenumber,
	idinvoicenumber,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber,
	"executivenumber" : executivenumber,
	"idinvoicenumber" : idinvoicenumber,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("elisto.Customer.apiCall1","/4/customer/addtoproductlist", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall2
	Delete a product from the order-list
	***************************************/
function apiCall_elisto_customer_delitem
	(idorder,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder
	};
	return apiCall("elisto.Customer.apiCall2","/4/customer/delitem", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall3
	Set remark on an ordered item 
	***************************************/
function apiCall_elisto_customer_itemremark
	(idorder,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder
	};
	return apiCall("elisto.Customer.apiCall3","/4/customer/itemremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall4
	Show shopping cart related to an invoice 
	***************************************/
function apiCall_elisto_customer_showcart
	(invoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"invoicenumber" : invoicenumber
	};
	return apiCall("elisto.Customer.apiCall4","/4/customer/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall5
	Show the status of the ordered items of an invoice 
	***************************************/
function apiCall_elisto_customer_deliverystatus
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Customer.apiCall5","/4/customer/deliverystatus", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall6
	Send the list of ordered items to the warehouse
	***************************************/
function apiCall_elisto_customer_sendorder
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Customer.apiCall6","/4/customer/sendorder", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall7
	Send a message to an executive
	***************************************/
function apiCall_elisto_customer_sendnotification
	(idexecutive,
	idinvoice,
	text,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idexecutive" : idexecutive,
	"idinvoice" : idinvoice,
	"text" : text
	};
	return apiCall("elisto.Customer.apiCall7","/4/customer/sendnotification", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall8
	Get meta-information about the selected invoice
	***************************************/
function apiCall_elisto_customer_invoiceinfo
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Customer.apiCall8","/4/customer/invoiceinfo", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall9
	Initiate a new invoice; it will be assigned to the chief-executive (chief-executive has idexecutive=1)
	***************************************/
function apiCall_elisto_customer_createinvoice
	(departmentnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber
	};
	return apiCall("elisto.Customer.apiCall9","/4/customer/createinvoice", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall10
	Get order-information per user of the selected invoice
	***************************************/
function apiCall_elisto_customer_orderuserinfo
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Customer.apiCall10","/4/customer/orderuserinfo", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall11
	Get the department-id of the logged-in customer
	***************************************/
function apiCall_elisto_customer_getcustomerdepartment
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Customer.apiCall11","/4/customer/getcustomerdepartment", dataObject, callBack, httpErrorCallBack,"post");
}


/*-----------------------------------*/ 
/* apiFunctions_elisto_kitchen.js */ 
/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Warehouse'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Warehouse.apiCall1
	select all items currently being procured from the warehouse
	***************************************/
function apiCall_elisto_warehouse_items
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Warehouse.apiCall1","/4/warehouse/items", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Warehouse.apiCall2
	Update delivery status of an ordered item
	***************************************/
function apiCall_elisto_warehouse_setprodstatus
	(idorder,
	status,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"status" : status
	};
	return apiCall("elisto.Warehouse.apiCall2","/4/warehouse/setprodstatus", dataObject, callBack, httpErrorCallBack,"put");
}


/*-----------------------------------*/ 
/* apiFunctions_elisto_public.js */ 
/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Public'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Public.apiCall1
	Show Products
	***************************************/
function apiCall_elisto_public_products
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall1","/4/public/products", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall2
	Show Categories
	***************************************/
function apiCall_elisto_public_categories
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall2","/4/public/categories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall3
	Show to which categories a given product belongs to
	***************************************/
function apiCall_elisto_public_productcategories
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Public.apiCall3","/4/public/productcategories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall4
	Show child-categories of a category
	***************************************/
function apiCall_elisto_public_category
	(category,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"category" : category
	};
	return apiCall("elisto.Public.apiCall4","/4/public/category", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall5
	Show which products belong to the subtree of a given category
	***************************************/
function apiCall_elisto_public_product
	(idcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory
	};
	return apiCall("elisto.Public.apiCall5","/4/public/product", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall6
	Get a Text by id
	***************************************/
function apiCall_elisto_public_textbyid
	(id,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id
	};
	return apiCall("elisto.Public.apiCall6","/4/public/textbyid", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall7
	Get a Text by mnemonic
	***************************************/
function apiCall_elisto_public_textbymnemonic
	(mnemonic,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"mnemonic" : mnemonic
	};
	return apiCall("elisto.Public.apiCall7","/4/public/textbymnemonic", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall8
	[Under development] New client registration
	***************************************/
function apiCall_elisto_public_register
	(domainName,
	eMail,
	firstName,
	lastName,
	passWord,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName,
	"eMail" : eMail,
	"firstName" : firstName,
	"lastName" : lastName,
	"passWord" : passWord
	};
	return apiCall("elisto.Public.apiCall8","/4/public/register", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Public.apiCall9
	[This call is deprecated]: Check  a domain-name
	***************************************/
function apiCall_elisto_public_domaincheck
	(domainName,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName
	};
	return apiCall("elisto.Public.apiCall9","/4/public/domaincheck", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall10
	Show available layouts
	***************************************/
function apiCall_elisto_public_layouts
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall10","/4/public/layouts", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall11
	Show attributes of a product
	***************************************/
function apiCall_elisto_public_getproductattributes
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Public.apiCall11","/4/public/getproductattributes", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall12
	Show all attribute-definitions 
	***************************************/
function apiCall_elisto_public_allproductattributes
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall12","/4/public/allproductattributes", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall13
	Select images of a product
	***************************************/
function apiCall_elisto_public_productimages
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Public.apiCall13","/4/public/productimages", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall14
	[This call is deprecated]: show all departments
	***************************************/
function apiCall_elisto_public_showdepartments
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall14","/4/public/showdepartments", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall15
	Show parent-categories of a category
	***************************************/
function apiCall_elisto_public_parentcategories
	(category,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"category" : category
	};
	return apiCall("elisto.Public.apiCall15","/4/public/parentcategories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall16
	Select id of a product by its label 
	***************************************/
function apiCall_elisto_public_idproductbylabel
	(idlabel,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlabel" : idlabel
	};
	return apiCall("elisto.Public.apiCall16","/4/public/idproductbylabel", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall17
	Select all variable texts of a layout
	***************************************/
function apiCall_elisto_public_assignedtexts
	(layout,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layout" : layout
	};
	return apiCall("elisto.Public.apiCall17","/4/public/assignedtexts", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall18
	Show id of currently selected  layout
	***************************************/
function apiCall_elisto_public_layoutid
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall18","/4/public/layoutid", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall19
	Select a long paragraph as html or text via the contextid
	***************************************/
function apiCall_elisto_public_longtext
	(idcontext,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcontext" : idcontext
	};
	return apiCall("elisto.Public.apiCall19","/4/public/longtext", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall20
	Select a long paragraph as html or text by its contextname
	***************************************/
function apiCall_elisto_public_longtextbystr
	(contextname,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"contextname" : contextname
	};
	return apiCall("elisto.Public.apiCall20","/4/public/longtextbystr", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall21
	Get the value of a setting
	***************************************/
function apiCall_elisto_public_settings
	(setting,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"setting" : setting
	};
	return apiCall("elisto.Public.apiCall21","/4/public/settings", dataObject, callBack, httpErrorCallBack,"post");
}


/*-----------------------------------*/ 
/* apiFunctions_elisto_executive.js */ 
/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Executive'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Executive.apiCall1
	Create a new order-list and initiate invoice
	***************************************/
function apiCall_elisto_executive_createinvoice
	(departmentnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber
	};
	return apiCall("elisto.Executive.apiCall1","/4/executive/createinvoice", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall2
	Add a product-item to the order-list
	***************************************/
function apiCall_elisto_executive_addtoproductlist
	(departmentnumber,
	idinvoicenumber,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber,
	"idinvoicenumber" : idinvoicenumber,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("elisto.Executive.apiCall2","/4/executive/addtoproductlist", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall3
	Delete a product from the order-list
	***************************************/
function apiCall_elisto_executive_delitem
	(idorder,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("elisto.Executive.apiCall3","/4/executive/delitem", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall4
	Set notice on invoice 
	***************************************/
function apiCall_elisto_executive_invoiceremark
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall4","/4/executive/invoiceremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall5
	Set remark on an ordered item 
	***************************************/
function apiCall_elisto_executive_itemremark
	(idorder,
	remark,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"remark" : remark
	};
	return apiCall("elisto.Executive.apiCall5","/4/executive/itemremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall6
	Show shopping cart of a department 
	***************************************/
function apiCall_elisto_executive_showcart
	(departmentnumber,
	invoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber,
	"invoicenumber" : invoicenumber
	};
	return apiCall("elisto.Executive.apiCall6","/4/executive/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall8
	executive Dashboard 
	***************************************/
function apiCall_elisto_executive_executivedash
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Executive.apiCall8","/4/executive/executivedash", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall10
	Show invoices of a department 
	***************************************/
function apiCall_elisto_executive_departmentinvoices
	(departmentnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber
	};
	return apiCall("elisto.Executive.apiCall10","/4/executive/departmentinvoices", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall11
	Show status of ordered items of a invoice 
	***************************************/
function apiCall_elisto_executive_deliverystatus
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Executive.apiCall11","/4/executive/deliverystatus", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall12
	Send listed items as order to warehouse
	***************************************/
function apiCall_elisto_executive_sendorder
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Executive.apiCall12","/4/executive/sendorder", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall13
	Get notifications of customers for the currently
			logged in executive
	***************************************/
function apiCall_elisto_executive_getnotifications
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Executive.apiCall13","/4/executive/getnotifications", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall14
	Delete a customer-notification for the currently
			logged in executive
	***************************************/
function apiCall_elisto_executive_deletenotification
	(idnotification,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idnotification" : idnotification
	};
	return apiCall("elisto.Executive.apiCall14","/4/executive/deletenotification", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall15
	Set number of customers for an invoice
	***************************************/
function apiCall_elisto_executive_setcustomercount
	(customercount,
	idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customercount" : customercount,
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall15","/4/executive/setcustomercount", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall16
	Mark a invoice with current status
	***************************************/
function apiCall_elisto_executive_setinvoicestatus
	(idinvoice,
	invoicestatus,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice,
	"invoicestatus" : invoicestatus
	};
	return apiCall("elisto.Executive.apiCall16","/4/executive/setinvoicestatus", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall17
	Archiviation of an invoice
	***************************************/
function apiCall_elisto_executive_archiveinvoice
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall17","/4/executive/archiveinvoice", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall18
	Select executive profile data
	***************************************/
function apiCall_elisto_executive_getexecutiveprofile
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Executive.apiCall18","/4/executive/getexecutiveprofile", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall19
	Update profile data
	***************************************/
function apiCall_elisto_executive_editexecutiveprofile
	(email,
	executivename,
	first_name,
	last_name,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"executivename" : executivename,
	"first_name" : first_name,
	"last_name" : last_name
	};
	return apiCall("elisto.Executive.apiCall19","/4/executive/editexecutiveprofile", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall20
	Change executive password
	***************************************/
function apiCall_elisto_executive_changeexecutivepassword
	(password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"password" : password
	};
	return apiCall("elisto.Executive.apiCall20","/4/executive/changeexecutivepassword", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall21
	Change executive profile Image
	***************************************/
function apiCall_elisto_executive_changeexecutiveimage
	(image,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"image" : image
	};
	return apiCall("elisto.Executive.apiCall21","/4/executive/changeexecutiveimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall22
	Show orders of all registered users for an invoice
	***************************************/
function apiCall_elisto_executive_userorders
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall22","/4/executive/userorders", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall23
	Re-Assign orders of a registered user to a different
			invoice
	***************************************/
function apiCall_elisto_executive_reassignuserorders
	(idfrominvoice,
	idtoinvoice,
	iduser,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idfrominvoice" : idfrominvoice,
	"idtoinvoice" : idtoinvoice,
	"iduser" : iduser
	};
	return apiCall("elisto.Executive.apiCall23","/4/executive/reassignuserorders", dataObject, callBack, httpErrorCallBack,"put");
}


/*-----------------------------------*/ 
/* main.js */ 
"use strict";
release = RELEASE;
console.log(" 1 --- Customer ID before call of main.js: " + customerID);
var customerIDChanged = false;


function setCustomerId() {
    let oldCustomerID;
    if (!customerID) {
        oldCustomerID = null;
    } else {
        oldCustomerID = customerID;
    }
    const cid = getCustomerId();
    if (!isNaN(cid)) {
        customerID = cid;
        console.log("customerID has been set to: " + cid);
    } else {
        console.warn("customerID is NaN");
    }
    if (cid && oldCustomerID != customerID) {
        customerIDChanged = true;
    }
};

const getCookie = function (cname) {
    debug2("main.js", "get cookie", release, [cname]);
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    //debug("cookie:", release, decodedCookie);
    const ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};


setCustomerId();

console.log(" 2 --- Customer ID after call of main.js: " + customerID);

role = getUserRole();
let idbill = 0;

function debug(message, loggedRelease, param1, param2, param3, param4, param5) {
    if (loggedRelease === RELEASE) {
        console.log("Module last loaded: " + module + ", logged release-code:" + loggedRelease + "/ current release:" + RELEASE);
        console.log(message + "\n--------------");
        if (param1)
            console.log(param1.toString() + ": " + JSON.stringify(param1)
                + "\n--------------");
        if (param2)
            console.log(param2.valueOf() + ": " + JSON.stringify(param2)
                + "\n--------------");
        if (param3)
            console.log(param3.isPrototypeOf(String) + ": " + JSON.stringify(param3)
                + "\n--------------");
        if (param4)
            console.log(param4 + ": " + JSON.stringify(param4)
                + "\n--------------");
        if (param5)
            console.log(typeof param5 + " Debug-value5: " + JSON.stringify(param5)
                + "\n--------------");
    }

}

function debug2(module, message, loggedRelease, loggedValues) {
    if (loggedRelease === RELEASE) {
        console.log("::::: --- Debug module '" + module + "'. Message: " + message + " ---------- :::::");
        for (let m = 0; m < loggedValues.length; m++) {
            console.log(":::\tLogged value " + m + ": " + "(" + typeof loggedValues[m] + ") " + JSON.stringify(loggedValues[m]));
        }
        console.log("::::: ------------- :::::");
    }
}

function getThemeName(themeID) {
//TODO
}

function showRelease(currmod) {
    if (currmod) debug("\tCurrent module:" + currmod + "\tLast loaded module: " + module + "\tlogged release: " + release + "\tGlobal RELEASE: " + RELEASE, "" + release);
    else debug("\tLast loaded module: " + module + "\tlogged release: " + release + "\tGlobal RELEASE: " + RELEASE, "" + release);
}

//showRelease();


let labelAlertObjectID = "messageLabelId";


function isListoNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}


function apiCall(apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method) {
    if (!bearerCookie || bearerCookie === -1) {
        const key = "listo_" + customerID + "_" + applicationID + "_" + role;
        debug2("main.js", "calling api with new key", release, [apiUrl, apiParams, key]);
        bearerCookie = sessionStorage.getItem(key);
        //console.log("Calling api " + apiID + "and tried to retrieve bearerCookie with key " + key + ", bearerCookie=" + bearerCookie);
        //console.log(JSON.stringify(sessionStorage));
        bearerCookie = getCookie(key);
        //console.log("Auth-cookie read as: " + bearerCookie + " with key " + key);
        //debug2("main.js", "get auth token from session storage", release, [key, customerID, applicationID, bearerCookie, sessionStorage]);
    }
    if (!customerIDChanged && selectedServer) {
        // selectedServer is set, and also  stored in sessionStorage
        debug2("main.js", "calling api: USECASE A", release, [apiUrl, apiParams, bearerCookie]);
        doApiCall(selectedServer, apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method);
    } else if (!customerIDChanged && sessionStorage.getItem("selectedServer")) {
        // selectedServer is not set, but it was stored in sessionStorage
        debug2("main.js", "calling api: USECASE B", release, [apiUrl, apiParams, bearerCookie]);
        doApiCall(sessionStorage.getItem("selectedServer"), apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method);
        console.log("\n\n\t:::Selected server from session in MAIN.js: " + sessionStorage.getItem("selectedServer"));
    } else {
        // selectedServer is not set, and not stored in sessionStorage
        // or, so far, no customerID has been set
        // or the customerID has just been set to a new value
        const cid = getCustomerId();
        const app = getApplication();
        $.ajax({
            url: `https://backend.listorante.com${backendVersion}server/${cid}/${app}`,
            type: "get",
            data: {}
        }).done(
            function (data) {
                debug2("main.js", "calling api: USECASE C", release, [apiUrl, apiParams, bearerCookie, data]);
                const newSelectedServer = `https://backend${data.nodeid}.listorante.com${backendVersion}`;
                console.log("\n\n\t:::NEW selected server in MAIN.js during call '" + apiUrl + "': " + newSelectedServer + "(node " + data.nodeid + ")");
                selectedServer = newSelectedServer;
                sessionStorage.setItem("selectedServer", newSelectedServer);
                doApiCall(newSelectedServer, apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method);
                customerIDChanged = false;
            }).fail(
            function (error) {
                const err = readErrors(error);
                debug(`HTTP-ERROR in method ${apiUrl}, status=${error.status}`,
                    release, err, apiParams);
                httpErrorCallBack(err);
            });
    }
}

function doApiCall(server, apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method) {
    //const paramLen = apiParams.length;
    let url;
    url = "" + server + customerID + apiUrl + "?LOC=" + getCurrentLanguage();
    //debug(method + "-APICALL: ID=" + apiID, release);
    //debug("         URL=" + url, release);
    //debug("         token=" + bearerCookie, release);
    //debug("dataObject before:", release, apiParams);
    for (let key in apiParams) {
        let encStr = apiParams[key];
        if (encStr && !isListoNumeric(encStr)) {
            try {
                let str = decodeURIComponent(encStr);
                str = str.replace(/\\/g, "&#92;").replace(/\//g, "&#47;").replace(/'/g, "&#39;").replace(/</g, "&#10094;").replace(/>/g, "&#10095;").replace(/"/g, "&#34;")
                    .replace(/\u276E/g, "&#10094;").replace(/\u276F/g, "&#10095;");
                apiParams[key] = str;
            } catch (err) {
                apiParams[key] = encStr.replace(/\\/g, "&#92;").replace(/\//g, "&#47;").replace(/'/g, "&#39;").replace(/</g, "&#10094;").replace(/>/g, "&#10095;").replace(/"/g, "&#34;")
                    .replace(/\u276E/g, "&#10094;").replace(/\u276F/g, "&#10095;");
            }
        }
    }
    //debug("dataObject after decoding:", release, apiParams);

    if (method === "undefined") {
        console.warn("api-call '" + apiID + "' with empty  HTTP-method");
        $.get(url, {}).done(
            function (data) {
                if (data._rc > 0)
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                callBack(data);
            }).fail(
            function (error) {
                const err = readErrors(error);
                debug("HTTP-ERROR in " + method + "-API-CALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
            });
    } else if (method === "get") {
        $.ajax({
            url: url,
            type: method,
            data: apiParams
        }).done(
            function (data) {
                if (data._rc && data._rc > 0) {
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                    httpErrorCallBack(readErrors(data));
                } else {
                    callBack(data);
                }
            }).fail(
            function (error) {
                const err = readErrors(error);
                debug("HTTP-ERROR in " + method + "-API-CALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
            });
    } else if (method === "post" || method === "put" || method === "patch") {
        $.ajax({
            url: url,
            type: method,
            data: apiParams,
            headers: {
                "Authorization": "Bearer " + bearerCookie,
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
            }
        }).done(
            function (data) {
                if (data._rc && data._rc > 0) {
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                    httpErrorCallBack(readErrors(data));
                } else {
                    callBack(data);
                }
            }).fail(
            function (error) {
                const err = readErrors(error);
                debug("HTTP-ERROR in " + method + "-API-CALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
            });
    } else if (method === "delete") {
        $.ajax({
            url: url,
            type: method
        }).done(
            function (data) {
                if (data._rc && data._rc > 0) {
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                    httpErrorCallBack(readErrors(data));
                } else {
                    callBack(data);
                }
            }).fail(
            function (error) {
                const err = readErrors(error);
                debug("HTTP-ERROR in " + method + "-APIC-ALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
            });
    }
}

function readErrors(errorData) {
    let err = {};
    if (errorData._rc) err._rc = errorData._rc;
    if (errorData._rcA) err._rcA = errorData._rcA;
    if (errorData.Exception) err.Exception = errorData.Exception;
    if (errorData.status) err.status = errorData.status;
    if (errorData.responseText) err.responseText = errorData.responseText;
    if (errorData.readyState) err.readyState = errorData.readyState;
    err.ErrorData = JSON.stringify(errorData);
    return err;
}

/*
function doErrorHandling(div, data) {
    if (div && data._rc > 0) showHttpErrorMessage(div, data._rc);
    else if (div && data._rcA > 0) showHttpErrorMessage(div, data.rcA);
    else if (data._rc > 0)
        showHttpErrorMessage('main-content', data._rc);
    else if (data.rcA > 0)
        showHttpErrorMessage('main-content', data.rcA);
}*/


function upload(customerIdentifier, form, callBack, httpErrorCallBack) {
    const formData = document.querySelector('#' + form);
    const data = new FormData(formData);
    const upUrl = `${getAPIServerPath()}${customerIdentifier}/${applicationID}/upload/`;

    debug2('main.js', 'Upload file: ' + upUrl, release, [""]);
    $.ajax({
        url: upUrl,
        method: 'POST',
        cache: true,
        data: data,
        crossDomain: true,
        processData: false,
        contentType: false,
        timeout: 600000,
        headers: {
            'Authorization': 'Bearer ' + bearerCookie
        }
    }).done(function (data) {
        if (data._rc && data._rc > 0) {
            debug("ERROR IN UPLOAD-API-CALL, rc=" + data._rc, release, data._rc);
            httpErrorCallBack(readErrors(data));
        } else {
            debug('Done: ', release, data._rc);
            callBack(data);
        }
    }).fail(function (error) {
        console.error(error);
        const err = readErrors(error);
        debug('ERROR IN upload-function, status=' + error.status, "" + release, error);
        httpErrorCallBack(err);
    });
}

function uploadBlob(customerIdentifier, form, callBack, httpErrorCallBack) {
    const formData = document.querySelector('#' + form);
    const data = new FormData(formData);
    console.log("FormData: " + data);
    const upUrl = `${getAPIServerPath()}${customerIdentifier}/${applicationID}/loadblob/`;
    try {
        document.getElementById('imageLoaderID').innerHTML = images.loading;
    } catch (excDone) {
        console.error(excDone);
    }
    debug2('main.js', 'Upload file: ' + upUrl, release, [""]);
    $.ajax({
        url: upUrl,
        method: 'POST',
        cache: false,
        data: data,
        crossDomain: true,
        processData: false,
        contentType: false,
        timeout: 600000,
        headers: {
            'Authorization': 'Bearer ' + bearerCookie
        }
    }).done(function (data) {
        try {
            document.getElementById('imageLoaderID').innerHTML = "";
        } catch (excDone) {
            console.error(excDone);
        }
        if (data._rc && data._rc > 0) {
            debug("ERROR IN loadBLOB-API-CALL, rc=" + data._rc, release, data._rc);
            httpErrorCallBack(readErrors(data));
        } else {
            debug('Done: ', release, data._rc);
            callBack(data);
        }

    }).fail(function (error) {
        debug('ERROR IN loadBLOB-function, status=' + error.status, "" + release, error);
        let err;
        try {
            err = readErrors(error);
            console.error(err);
        } catch (excErr) {
            console.error(excErr);
        }
        try {
            document.getElementById('imageLoaderID').innerHTML = "";
        } catch (imgErr) {
            console.error(imgErr);
        }
        httpErrorCallBack(err);
    });
}


function showHttpErrorMessage(div, error) {
    let errorID;
    if (error.status) {
        errorID = error.status;
    } else {
        errorID = 499;
    }
    try {
        let msg = "";
        for (let key in error) {
            msg += key + ": " + error[key] + "\n";
        }
        let displayMsg = getTextById(errorID);
        if (error._rc) {
            displayMsg += `<br>rc: ${error._rc}`;
        }
        if (error._rcA) {
            displayMsg += `<br>application-rc: ${error._rcA}`;
        }
        const icon = `<small><i class="glyphicon glyphicon-remove-sign show-ErrorTextArea" title="ERROR"></i></small><br><br>`;
        const ta = `<textarea id="errorTextArea" rows="5" cols="50" readonly style="display:none;">${msg}</textarea>`;
        $('#' + div).html(icon + '<b><small>' + displayMsg + '</small></b><br><br><br><br>' + ta);
    } catch (err) {
        debug("Error in error-handling:", release + "", err.message, error);
        const icon = `<small><i class="glyphicon glyphicon-remove-sign show-ErrorTextArea" title="ERROR"></i></small><br><br>`;
        const ta = `<textarea id="errorTextArea" rows="5" cols="50" readonly style="display:none;">${JSON.stringify(error)}</textarea>`;
        $('#' + div).html(icon + `<b><small> Unknown ERROR</small></b><br><br><br><small>${JSON.stringify(err)}</small><br><br>${ta}`);
        //if (error.status && error.status > 0) $('#' + div).html('<b>ERROR ' + error.status + ' ' + getTextById(error.status) + '</b>');
        //else if (error.status === 0) $('#' + div).html('<b>  ERROR (status=0: network or other connection problem)</b>');
        //else $('#' + div).html('<b> Unknown ERROR</b>');
    }
    //debug("Error is:", ""+release, error, error.message);
}

$(document).on('click', '.show-ErrorTextArea', function () {
    let ta = document.getElementById("errorTextArea");
    ta.hidden = false;
    ta.setAttribute("style", "background-color: lightgray;overflow-x: none;")
});


function getIndex(data, variableName) {
    for (let vindex = 0; vindex < data._VariableLabels.length; vindex++) {
        if (data._VariableLabels[vindex] === variableName)
            return vindex;
    }
}

function get(data, rowIndex, variableName) {
    const idx = getIndex(data, variableName);
    return data.rows[rowIndex].s[idx];
}

// TODO This function is deprecated; calls to it need to be replaced with
// listoHTML.createTable
/*function populateBox(elem, jsonRMLData, indexArray, rowTag, columnTag,
                     callbackCell, callBackRow) {
    for (let i = 0; i < jsonRMLData.count; i++) {
        const row = document.createElement(rowTag);
        const rowData = jsonRMLData.rows[i];
        for (let j = 0; j < indexArray.length; j++) {
            const cell = document.createElement(columnTag);
            callbackCell(j, cell);
        }
        callBackRow(row, rowData);
        elem.appendChild(row);
    }
};
*/
const listoHTML = {

    IDENTIFIER: 'self',
    jsonRMLData: null,
    columnIndices: null,
    header: null,
    actions: null,
    dataColumnNames: null,
    dataColumnIndices: null,
    rowFormat: function (rowIndex, sDataArray) {
    },
    cellFormat: function (columnIndex, rowIndex, TDopen, data, TDclose) {
    },

    createTableTest:
    // just test if 'native' datatable is more performant with ajax
        function (targetDiv) {
            $('#' + targetDiv).html('<table id="datatableID"><table>');
            $("#datatableID").DataTable({
                ajax: {
                    url: 'https://backend0.listorante.com/RMLRest2-0.9.1-SNAPSHOT/1/1/public/categories?LOC=de',
                    dataSrc: 'rows'
                },
                columns: [
                    {data: 'rn'},
                    {data: 's.0'},
                    {data: 's.1'},
                    {data: 's.2'},
                    {data: 's.3'},
                    {data: 's.4'},
                    {data: 's.5'},
                    {data: 's.6'},
                    {data: 's.7'},
                    {data: 's.8'},
                    {data: 's.9'},
                    {data: 's.10'},
                    {data: 's.11'},
                    {data: 's.12'},
                    {data: 's.13'}
                ]
            });
        },
    createTable:
        function (targetDiv, isNoDataTable) {
            let localUrl;
            'use strict';
            // debug('create table: ',0,this.IDENTIFIER,targetDiv,sandGlass.image);
            let error = 0;
            if (this.jsonRMLData == null) {
                error = 5;
                debug('ERROR [CRHTMLTABLE' + error + ']: in calling object "'
                    + this.IDENTIFIER + '": missing jsonRMLData.', 0);
                $('#' + targetDiv).html(
                    '<b>ERROR [CRHTMLTABLE' + error + ']: in calling object "' + IDENTIFIER
                    + '": missing jsonRMLData.</b>');
            } else if (this.columnIndices == null || this.columnIndices.length === 0) {
                error = 6;
                debug('ERROR [CRHTMLTABLE' + error + ']: in calling object "'
                    + this.IDENTIFIER + '": missing column-Indices.', 0);
                $('#' + targetDiv).html(
                    '<b>ERROR [CRHTMLTABLE' + error + ']: in calling object "'
                    + this.IDENTIFIER + '": missing column-Indices.</b>');
            }

            let rowDiv = TableStyle.tableHeadOpen;
            const headerLength = this.header.length;
            for (let h = 0; h < headerLength; h++) {
                rowDiv += TableStyle.thOpen + this.header[h] + TableStyle.thClose;
            }
            //rowDiv += TableStyle.thOpen + TableStyle.thClose;
            rowDiv += TableStyle.tableHeadClose;
            const rowCount = this.jsonRMLData.count;
            const columnCount = this.columnIndices.length;
            const actionCount = this.actions.length;
            const dataCount = this.dataColumnNames.length;
            if (dataCount !== this.dataColumnIndices.length) {
                error = 1;
                debug('ERROR [CRHTMLTABLE' + error
                    + ']: Wrong number of data-items for input and action.', 0,
                    this.dataColumnNames, this.dataColumnIndices);
                $('#' + targetDiv)
                    .html(
                        '<b>ERROR [CRHTMLTABLE'
                        + error
                        + ']: in calling object "'
                        + this.IDENTIFIER
                        + '" of "createHTMLTable": Wrong number of arrays for input and action.</b>');
            } else if (actionCount !== dataCount) {
                error = 2;
                debug('ERROR[CRHTMLTABLE' + error
                    + ']:  Wrong number of array-items for input and action.', 0,
                    this.actions, this.dataColumnNames, this.dataColumnIndices);
                $('#' + targetDiv)
                    .html(
                        '<b>ERROR [CRHTMLTABLE'
                        + error
                        + ']: in calling object "'
                        + this.IDENTIFIER
                        + '" of "createHTMLTable": Wrong number of arrays for input and action.</b>');
            }
            for (let i = 0; i < rowCount; i++) {
                if (error > 0)
                    break;
                const rowData = this.jsonRMLData.rows[i];
                if (rowData !== undefined) {
                    rowDiv += TableStyle.trOpen;
                    let theRow = [];
                    if (this.rowFormat) {
                        theRow = this.rowFormat(i, rowData.s);
                    } else {
                        theRow = rowData.s;
                    }
                    for (let c = 0; c < columnCount; c++) {
                        if (theRow) {
                            const theCell = theRow[this.columnIndices[c]];
                            if (this.cellFormat) {
                                rowDiv += this.cellFormat(c, i, TableStyle.tdOpen, theCell,
                                    TableStyle.tdClose);
                            } else {
                                rowDiv += TableStyle.tdOpen + theCell + TableStyle.tdClose;
                            }
                        }
                    }
                    for (let ca = 0; ca < actionCount; ca++) {
                        rowDiv += TableStyle.tdOpen;
                        if (this.actions[ca].length !== 2) {
                            error = 3;
                            debug('ERROR[CRHTMLTABLE' + error
                                + ']:  array actions must have 2 values: name and label', 0,
                                this.actions);
                            $('#' + targetDiv)
                                .html(
                                    '<b>ERROR [CRHTMLTABLE'
                                    + error
                                    + ']: in caller object "'
                                    + this.IDENTIFIER
                                    + '" of "createHTMLTable": array actions must have 2 values: name and label.</b>');
                            break;
                        }
                        rowDiv += TableStyle.aOpen + ' class="' + this.actions[ca][0] + '" ';
                        const caDataNameCount = this.dataColumnNames[ca].length;
                        const caDataInputCount = this.dataColumnIndices[ca].length;
                        if (caDataNameCount !== caDataInputCount) {
                            error = 4;
                            debug('ERROR [CRHTMLTABLE' + error + ']:  caDataNameCount='
                                + caDataNameCount + ',caDataInputCount=' + caDataInputCount, 0,
                                caDataNameCount, caDataInputCount);
                            $('#' + targetDiv)
                                .html(
                                    '<b>ERROR [CRHTMLTABLE'
                                    + error
                                    + ']: in caller object "'
                                    + this.IDENTIFIER
                                    + '" of "createHTMLTable": wrong number of name and data parameters.</b>');
                            break;
                        }
                        for (let caIdx = 0; caIdx < caDataNameCount; caIdx++) {
                            rowDiv += ' data-' + this.dataColumnNames[ca][caIdx] + '="'
                                + rowData.s[this.dataColumnIndices[ca][caIdx]];
                            rowDiv += '"';
                        }
                        rowDiv += TableStyle.aAttributes + this.actions[ca][1]
                            + TableStyle.aClose;
                        rowDiv += TableStyle.tdClose;
                    }
                    rowDiv += TableStyle.trClose;
                }
            }
            rowDiv += TableStyle.bottom;
            if (error === 0) {
                $('#' + targetDiv).html(rowDiv);
            }
            if (isNoDataTable)
                return;
            if (locale === 'en') {
                localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json';
            } else if (locale === 'it') {
                localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json';
            } else if (locale === 'de') {
                localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json';
            }

            $(".dataTable").DataTable({
                retrieve: true,
                //"ajax": this.jsonRMLData,
                //"deferRender": true,
                'pageLength': 10,
                'language': {
                    'url': localUrl,
                }
            });
        },
    buildForm:
        function (div, width, array, id) {
            const pre = FormStyle.pre1 + width + FormStyle.pre2 + id + FormStyle.pre3;
            const suf = FormStyle.suf;
            $(div).html('');
            let rowDiv = pre;
            for (let i = 0; i < array.length; i++) {
                rowDiv += array[i];
            }
            rowDiv += suf;
            $(div).html(rowDiv);
        },
    createForm:
        function (div, width, array, id) {
            const pre = FormStyle.pre1 + width + FormStyle.pre2 + id + FormStyle.pre3;
            const suf = FormStyle.suf;
            $(div).html('');
            let rowDiv = pre;
            for (let i = 0; i < array.length; i++) {
                const funcLen = array[i].length;
                const objType = array[i][0];
                switch (funcLen) {
                    case (2):
                        rowDiv += objType(array[i][1]);
                        break;
                    case (3):
                        rowDiv += objType(array[i][1], array[i][2]);
                        break;
                    case (4):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3]);
                        break;
                    case (5):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4]);
                        break;
                    case (6):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5]);
                        break;
                    case (7):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6]);
                        break;
                    case (8):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8]);
                        break;
                    case (9):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8], array[i][9]);
                        break;
                    case (10):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8], array[i][9], array[i][10]);
                        break;
                    case (11):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8], array[i][9], array[i][10], array[i][11]);
                        break;
                    default:
                }
            }
            rowDiv += suf;
            $(div).html(rowDiv);
        },
    createTour:
        function (targetDiv, stepshtml, effect, orientation) {
            $(targetDiv).html(stepshtml);
            $(targetDiv).steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: effect, /*slideLeft*/
                autoFocus: true,
                stepsOrientation: orientation /*"vertical"*/
            });
        }

};

// Email Validation Function
function validateEmail(email) {
    const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

// FORM Validation
$(document).ready(function () {
    $('form').validate();
});


if (locale === "en") {

} else if (locale === "it") {
    $.extend($.validator.messages, {
        required: "Campo obbligatorio",
        remote: "Controlla questo campo",
        email: "Inserisci un indirizzo email valido",
        url: "Inserisci un indirizzo web valido",
        date: "Inserisci una data valida",
        dateISO: "Inserisci una data valida (ISO)",
        number: "Inserisci un numero valido",
        digits: "Inserisci solo numeri",
        creditcard: "Inserisci un numero di carta di credito valido",
        equalTo: "Il valore non corrisponde",
        extension: "Inserisci un valore con un&apos;estensione valida",
        maxlength: $.validator.format("Non inserire pi&ugrave; di {0} caratteri"),
        minlength: $.validator.format("Inserisci almeno {0} caratteri"),
        rangelength: $.validator.format("Inserisci un valore compreso tra {0} e {1} caratteri"),
        range: $.validator.format("Inserisci un valore compreso tra {0} e {1}"),
        max: $.validator.format("Inserisci un valore minore o uguale a {0}"),
        min: $.validator.format("Inserisci un valore maggiore o uguale a {0}"),
        nifES: "Inserisci un NIF valido",
        nieES: "Inserisci un NIE valido",
        cifES: "Inserisci un CIF valido",
        currency: "Inserisci una valuta valida"
    });
} else if (locale === "de") {
    $.extend($.validator.messages, {
        required: "Dieses Feld ist ein Pflichtfeld.",
        maxlength: $.validator.format("Geben Sie bitte maximal {0} Zeichen ein."),
        minlength: $.validator.format("Geben Sie bitte mindestens {0} Zeichen ein."),
        rangelength: $.validator.format("Geben Sie bitte mindestens {0} und maximal {1} Zeichen ein."),
        email: "Geben Sie bitte eine gültige E-Mail Adresse ein.",
        url: "Geben Sie bitte eine gültige URL ein.",
        date: "Bitte geben Sie ein gültiges Datum ein.",
        number: "Geben Sie bitte eine Nummer ein.",
        digits: "Geben Sie bitte nur Ziffern ein.",
        equalTo: "Bitte denselben Wert wiederholen.",
        range: $.validator.format("Geben Sie bitte einen Wert zwischen {0} und {1} ein."),
        max: $.validator.format("Geben Sie bitte einen Wert kleiner oder gleich {0} ein."),
        min: $.validator.format("Geben Sie bitte einen Wert größer oder gleich {0} ein."),
        creditcard: "Geben Sie bitte eine gültige Kreditkarten-Nummer ein."
    });
}


function makeid(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getLabelAlertObjectId() {
    return labelAlertObjectID;
}

function setLabelAlertObject(labelDivId) {
    labelAlertObjectID = labelDivId;
};

function labelAlert(msg) {
    const labelId = getLabelAlertObjectId();
    document.getElementById(labelId).innerText = msg;
}

function logout() {
    const logoutPath = getAPIServerPath() + customerID + "/1/logout";
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("token=" + bearerCookie);
    xhttp.onreadystatechange = function () {
        debug("logoutPath=" + logoutPath, 0);
        debug("token=" + bearerCookie, 0);
        if (this.readyState == 4) {
            sessionStorage.removeItem("selectedServer");
            window.open("access.html?cust=" + customerID, "_self");
            if (this.status >= 400) {
                debug("ERROR: logout-status: " + this.status, 0);
            }
        }
    };
}

$(document).on("click", ".logout", function () {
    logout();
});

function requestNewListoranteInstance(newCustomerID, callBack, httpErrorCallBack) {
    const params = {"database": 1, "instance": newCustomerID, "replicatedInstances": 1, "environment": environment};
    return apiCall("global.listoranteRequest", "/0/customer/listorequest", params, callBack, httpErrorCallBack, "post");
}


function createNewListoranteInstance(newCustomerID, email, activationToken, callBack, httpErrorCallBack) {
    const params = {
        "database": 1,
        "instance": newCustomerID,
        "email": email,
        "activationToken": activationToken,
        "replicatedInstances": 1
    };
    return apiCall("global.listoranteCreate", "/0/customer/listocreate", params, callBack, httpErrorCallBack, "post");
}

function getStripeAccountID(callback) {
    if (stripe_customer_id) {
        callback(stripe_customer_id);
        return stripe_customer_id;
    }
    apiCall_elisto_public_settings("STRIPE_ACCOUNT_ID", function (data) {
        if (data.count > 0) {
            stripe_customer_id = data.rows[0].s[0];
            is_stripe_registered = true;
            callback(stripe_customer_id);
            return stripe_customer_id;
        }
    }, function (error) {
        showHttpErrorMessage("main-content", error);
    });
}

function getNode(cb, errCb) {
    $.ajax({
        url: getAPIServerPath() + "/server/nodeid",
        type: "post",
        data: {},
        headers: {
            "Authorization": "Bearer " + bearerCookie,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
    }).done(
        function (data) {
            cb(data);
        }).fail(
        function (error) {
            errCb(error);
        });
}

function stripePayment(application, productID, price, productName, productDescription, currency, quantity, callBack, httpErrorCallBack) {
    /*
    *
    *  @FormParam("productID") int productID,
			@FormParam("price") int price, @FormParam("product") String productName,
			@FormParam("productDescription") String productDescription, @FormParam("currency") String currency,
			@FormParam("quantity") int quantity
    **/

    const params = {
        "productID": productID,
        "price": price,
        "product": productName,
        "productDescription": productDescription,
        "currency": currency,
        "quantity": quantity
    };
    return apiCall("payment.stripe", "/" + application + "/stripe", params, callBack, httpErrorCallBack, "post");
}

function sendMail(application, sender, recipient, mailSubject, mailBody, senderKey, callBack, httpErrorCallBack) {
    debug2("main", "Sending Email", RELEASE, ["Sender:" + sender, "Recipient:" + recipient]);
    const params = {
        "sender": sender,
        "recipient": recipient,
        "subject": mailSubject,
        "body": mailBody,
        "senderKey": senderKey
    };
    return apiCall("sendMail", "/" + application + "/sendmail", params, callBack, httpErrorCallBack, "post");
}


function stripeConnectPayment(stripeAccountID, application, productID, price, productName, productDescription,
                              currency, quantity, customerEmail, paymentMethod, callBack, httpErrorCallBack) {

    const params = {
        "sellerStripeID": stripeAccountID,
        "productID": productID,
        "price": price,
        "product": productName,
        "productDescription": productDescription,
        "currency": currency,
        "quantity": quantity,
        "customerEmail": customerEmail,
        "paymentMethod": paymentMethod
    };
    return apiCall("payment.stripe", "/" + application + "/stripeconnect", params, callBack, httpErrorCallBack, "post");
}

function updateCredits(application, sessionid, callBack, httpErrorCallback) {
    const params = {};
    return apiCall("update.credits", "/" + application + "/updatecredits/" + sessionid, params, callBack, httpErrorCallback, "post");
}

function getCreditCharges(callBack, httpErrorCallback) {
    const params = {};
    return apiCall("credit.showcharges", "/" + applicationID + "/showcreditcharges/", params, callBack, httpErrorCallback, "post");
}

function getCustomerCredits(callback, errorcallback) {
    $.ajax({
        url: getAPIServerPath() + getCustomerId() + "/" + applicationID + "/showcredits",
        type: "post",
        data: {},
        headers: {
            "Authorization": "Bearer " + bearerCookie,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
    }).done(
        function (data) {
            callback(data);
        }).fail(
        function (error) {
            errorcallback(error);
        });
}


function uuidv4() {
    const uid = ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
    return uid.replace("-", "s");
}

function createNewStripeAccount(stripeAuthCode, callBack, httpErrorCallback) {
    const params = {"stripe_auth_code": stripeAuthCode};
    return apiCall("createNewStripeAccount", "/" + applicationID + "/createstripeaccount", params, callBack, httpErrorCallback, "post");
}

function selectUploadFiles(evt) {
    const filesToUpload = evt.target.files; // FileList object

    // Read the files in a loop
    for (let i = 0, f; f = filesToUpload[i]; i++) {

        // restrict images
        if (!f.type.match('image.*')) {
            continue;
        }

        let reader = new FileReader();

        reader.onload = (function(theFile) {
            return function(e) {
                // create Thumbnails.
                let preView = document.createElement('img');
                preView.className = 'thumb';
                preView.src   = e.target.result;
                preView.title = theFile.name;
                document.getElementById('list').insertBefore(preView, null);
            };
        })(f);

        // Read images as Data URL:
        reader.readAsDataURL(f);
    }
}
// React on new selection and, in case, perform selectUploadFiles again.
//document.getElementById('filesUploadElement').addEventListener('change', selectUploadFiles, false);

(function (global) {

    if(typeof (global) === "undefined") {
        throw new Error("window is undefined");
    }

    const _hash = "!";
    const noBackPlease = function () {
        global.location.href += "#";

        // making sure we have the fruit available for juice (^__^)
        global.setTimeout(function () {
            global.location.href += "!";
        }, 50);
    };

    global.onhashchange = function () {
        if (global.location.hash !== _hash) {
            global.location.hash = _hash;
        }
    };

    global.onload = function () {
        noBackPlease();

        // disables backspace on page except on input fields and textarea..
        document.body.onkeydown = function (e) {
            const elm = e.target.nodeName.toLowerCase();
            if (e.which === 8 && (elm !== 'input' && elm  !== 'textarea')) {
                e.preventDefault();
            }
            // stopping event bubbling up the DOM tree..
            e.stopPropagation();
        };
    }

})(window);
/*-----------------------------------*/ 
/* utils.js */ 
let layoutTexts = {mappings: []};
let layoutTextsLoaded = false;
let layoutChanged = true;
let layoutID;
getLayoutId();


function getLayoutId() {
    if (layoutID && !layoutChanged) {
        debug("Layout is already set to: " + layoutID, release);
        return layoutID;
    }
    apiCall_elisto_public_layoutid(function (data) {
        layoutID = data.rows[0].s[0];
        layoutChanged = false;
        layoutTextsLoaded = false;
        debug("Layout was selected and is now set to: " + layoutID, release, layoutID, layoutChanged, layoutTextsLoaded);
        assignLayoutTexts();
        return layoutID;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

function assignLayoutTexts() {
    if (layoutTextsLoaded) {
        for (let i = 0; i < layoutTexts.count; i++) {
            debug("TODO: re-assign loaded text  " + i, release);
            document.getElementById(layoutTexts.getKey(i)).innerText = layoutTexts.getValue(i);
        }
        return;
    }
    if (layoutID) {
        apiCall_elisto_public_assignedtexts(layoutID, function (data) {
            for (let i = 0; i < data.count; i++) {
                let map = {};
                map.text_id = data.rows[i].s[0];
                map.text_value = data.rows[i].s[3];
                //debug("text-map " + i + ":", 0, map);
                layoutTexts.mappings.push(map);
                let textElement = document.getElementById(map.text_id);
                if (textElement) textElement.innerText = map.text_value;
                else debug("Element with id '" + map.text_id + "' does not exist in this document", release);
            }
            //debug("layout-Texts:", 0, layoutTexts);
            layoutTextsLoaded = true;
        }, function (err) {
            showHttpErrorMessage("main-content", err.status);
        });
    } else {
        debug("Could not assign texts, layoutID is undefined ", release, layoutID, layoutChanged, layoutTextsLoaded);
    }
}

stripe_customer_id = getStripeAccountID(function(found_stripe_id){
    if (found_stripe_id) {
        console.log("Customer has following stripe-account: " + found_stripe_id);
        is_stripe_registered = true;
    } else {
        console.log("No stripe-account found for this customer ");
    }
});

/* login.js */ 
module = "login.js";
//applicationID = 1;
var userID;
release = RELEASE;

/**
 * Set texts
 **/
function setLoginTexts() {
    const passW = getTextById(81);
    const uName = getTextById(82);
    const cID = getTextById(729);

    setElemText(615);
    setElemText(713);
    setElemText(726);
    setElemText(727);
    setElemText(728);
    let roleAssigned = "";
    let user = document.getElementById("user");
    let password = document.getElementById("password");
    let inputCustomerID = document.getElementById("inputCustomerID")
    if (user) {
        user.placeholder = uName;
    }
    if (password) {
        password.placeholder = passW;
    }
    if (inputCustomerID) {
        inputCustomerID.placeholder = cID;
    }
    /*
    if (!isNaN(role)) {
        document.getElementById("selectRole").innerHTML = "";
    }
    */
    if (role === 0) {
        roleAssigned = getTextById(100); //Manager
    } else if (role === 1) {
        roleAssigned = getTextById(70);//Waiter
    } else if (role === 2) {
        roleAssigned = getTextById(3);//Cook
    } else if (role === 3) {
        roleAssigned = getTextById(102); // client
    } else {
        roleAssigned = getTextById(715); //select - message
    }
    let roleElement = document.getElementById("role");
    if (roleElement) {
        roleElement.innerText = roleAssigned;
    }
    if (customerID) {
        let selectCustomer = document.getElementById("selectCustomer");
        if (selectCustomer) {
            selectCustomer.innerHTML = "";
        }
        let text726 = document.getElementById("text726");
        if (text726) {
            text726.innerHTML = "";
        }
        let text727 = document.getElementById("text727");
        if (text727) {
            text727.innerHTML = "";
        }
        let text728 = document.getElementById("text728");
        if (text728) {
            text728.innerHTML = "";
        }
    }
}

//setTexts();


// Using local storage is bad practice: the values are never deleted from browser!
// Better clear all values. "So what" for other applications which should use the local storage
localStorage.clear();


function userLogout() {
    /*
     * loginState:
     * 0/NaN : user is not logged in
     * 1 : user is logged in
     * 2 : there are items in the list
   * */
    debug2("shop", "loginState in userLogout()", release, [loginState]);
    sessionStorage.removeItem("theCustomerID");
    const logoutPath = getAPIServerPath() + customerID + "/1/logout";
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.onreadystatechange = function () {
        debug2("shop", "logoutPath,xhttp,loginState,authCookie", release, [logoutPath, xhttp, loginState, bearerCookie]);
        if (xhttp.readyState == 4) {
            if (xhttp.status >= 400) {
                debug2("shop", "ERROR: logout-status ", "" + release, [xhttp, loginState]);
            } else {
                setLoginState(0);
                const loginButton = document.getElementById("loginButton");
                loginButton.innerText = getTextById(615);
                loginButton.setAttribute("class", "btn btn-singin login");
                document.getElementById("username").hidden = false;
                document.getElementById("userpass").hidden = false;
            }
        }
    };
    xhttp.send("Authorization=Bearer " + bearerCookie + "&token=" + bearerCookie);
}

function setLoginState(state) {
    /*
    * loginState:
    * 0/NaN : user is not logged in
    * 1 : user is logged in
    * 2 : there are items in the list
  * */
    sessionStorage.setItem("customerLoginCheck", state);
    loginState = parseInt(sessionStorage.getItem("customerLoginCheck"), 10);
}

function login(user, pass, customerTheme) {
    console.log("LOGIN START");
    document.getElementById("statusIndicator").innerHTML=images.loading;
    if (!customerID || customerID === 9) {
        console.error("Invalid customerID for login: " + customerID);
    } else if (!selectedServer) {
        console.error("No server was selected, yet");
    } else {
        const xhttp = new XMLHttpRequest();
        const path = `${selectedServer}server/${customerID}/${applicationID}`;
        xhttp.open("GET", path, true);
        xhttp.onreadystatechange = function () {
            console.log("path to select customer server=" + path);
            if (xhttp.readyState === 4) {
                if (xhttp.status === 200) {
                    const customerServerData = JSON.parse(xhttp.responseText);
                    selectedServer = `https://backend${customerServerData.nodeid}.listorante.com${backendVersion}`;
                    console.log(":::: Selected customer server: " + selectedServer);
                    sessionStorage.setItem("selectedServer", selectedServer);
                    doLogin(selectedServer + customerID + "/" + applicationID + "/login", user, pass, customerTheme);
                }else {
                    document.getElementById("statusIndicator").innerHTML=" ";
                    showLoginFailure(getTextById(499)+" (HTTP"+xhttp.status+")");
                }
            }else {
                //document.getElementById("statusIndicator").innerHTML=" ";
                showLoginFailure(getTextById(499)+" (ready state: "+xhttp.readyState+")");
            }
        };
        xhttp.send();
    }
    console.log("LOGIN END");
    //document.getElementById("statusIndicator").innerHTML="";
}




function doLogin(loginPath, user, pass, customerTheme) {

    debug("customerID is: " + customerID, "xxxxxxxxxxx");
    if (customerTheme) {
        role = 3;
    }

    //const loginPath = getAPIServerPath() + customerID + "/1/login";
    //bearerCookieName = "listoranteToken" + customerID + user;
    showLoginFailure("");

    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", loginPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + user + "&password=" + pass);
    xhttp.onreadystatechange = function () {
        console.log("loginPath=" + loginPath);
        let statusString = "Start";

        statusString += "_" + xhttp.readyState + ": " + xhttp.status + " (" + xhttp.statusText + " " + xhttp.getAllResponseHeaders() + ")";
        //document.getElementById("statusIndicator").innerHTML = statusString;
        document.getElementById("statusIndicator").innerHTML=images.loading2;
        if (xhttp.readyState === 4) {
            if (xhttp.status === 200) {
                setLoginState(0);
                const token = xhttp.responseText.split(";")[0];
                // we need userEmail for payment checkout:
                const userEmail = xhttp.responseText.split(";")[1];
                sessionStorage.setItem("userEmail", userEmail);
                setCookie("listo_" + customerID + "_" + applicationID + "_" + role, token, 1);
                sessionStorage.setItem("listo_" + customerID + "_" + applicationID + "_" + role, token);
                console.log("responseText=" + xhttp.responseText);

                if (role === 0) {
                    window.open("admin.html?cust=" + customerID, "_self");
                } else if (role === 1) {
                    sessionStorage.setItem("listo_" + customerID + "_" + applicationID, xhttp.responseText.split(";")[0]);
                    window.open("executive.html?cust=" + customerID, "_self");
                } else if (role === 2) {
                    window.open("warehouse.html?cust=" + customerID, "_self");
                } else if (customerTheme) {
                    debug2("login", "no custom actions defined in main/login.js for this theme", release, [customerTheme]);
                    /*const loginButton = document.getElementById("loginButton");
                    loginButton.innerText = getTextById(26);
                    loginButton.setAttribute("class", "btn btn-singin userLogout");
                    document.getElementById("username").hidden = true;
                    document.getElementById("userpass").hidden = true;
                    setLoginState("1");*/
                    //window.open("/applications/shop/layout/" + customerTheme + "/?cust=" + customerID, "_self");
                } else {
                    showLoginFailure(getTextById(JSON.stringify(xhttp.responseText)));
                }
            } else if (xhttp.status >= 400) {
                document.getElementById("statusIndicator").innerHTML=" ";
                if (xhttp.status === 403 || xhttp.status === 401) {
                    showLoginFailure(getTextById(738));// message will be "Login failed: wrong password?"
                } else if (xhttp.status === 404) {
                    showLoginFailure(getTextById(404));
                } else  {
                    showLoginFailure(getTextById(499)+" (HTTP-status: "+xhttp.status  +")");
                }
                /*
                 * window.open("error.html?err=" + this.status + " (state=" +
                 * this.readyState + ")", "_self");
                 */
            } else {
                document.getElementById("statusIndicator").innerHTML=" ";
                console.log("status: " + xhttp.status);
                showLoginFailure(getTextById(499)+" (HTTP-status: "+xhttp.status +")");
            }
            //return xhttp.status;
            debug("statusIndicator", release, statusString);
        }
    };
}

function setCookie(cname, cvalue, exdays) {
    debug("cookie before login:" + document.cookie);
    const allCookieArray = document.cookie.split(';');
    for (let i = 0; i < allCookieArray.length; i++) {
        debug("cookie" + i + ":" + allCookieArray[i], 0);
        allCookieArray[i] = "cookiename= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    }
    //document.cookie = "cookiename= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function showLoginFailure(message) {
    const loginMsg = document.getElementById("login-msg");
    if (loginMsg) loginMsg.innerHTML = message;
}


$(document).on('click', '.login', function () {
    try {
        const customerInputField = document.getElementById("inputCustomerID");
        if (customerInputField) {
            customerID = customerInputField.value;
        }
    } catch (err) {
    }
    const nameValue = document.getElementById("user").value;
    const passValue = document.getElementById("password").value;
    const themeName = document.getElementById("themeName").innerText;
    debug2("shop.login", "login clicked", RELEASE, [nameValue, themeName]);
    if (isNaN(role)) {
        //role = parseInt(role = document.getElementById("inputSelectRole").value, 10);
        console.error("No role has been set!");
    }
    //setServerPath(customerID, 1);
    login(nameValue, passValue, themeName);
})

$(document).on('click', '.userLogout', function () {
    userLogout();
    //scrollToCustomerBox("login-msg");
    //window.open("/applications/shop/layout/" + customerTheme + "/?cust=" + customerID, "_self");
})

/*-----------------------------------*/ 
/*---------------END OF elistoshop.js--------------------*/ 
