"use strict";
module = "listocreate.js";
release = "d1a6f_5";
const instance = getUrlVars()["inst"];
const customerMail = getUrlVars()["email"];
const activation = getUrlVars()["activation"];


document.getElementById("CID").innerText = instance;
document.getElementById("userEmail").innerText = customerMail;
//setServerPath(0, 0);
setElemText(82);
setElemText(715);
setElemText(726);
document.getElementById("loginRoleList").hidden = true;
document.getElementById("text715").hidden = true;
const clockImage = images.loading;
document.getElementById("clockImage").innerHTML = clockImage;
customerID = 0;
createNewListoranteInstance(instance, customerMail, activation, function (data) {
    debug("listorante creation, data:", release, data);
    setElemText(3);
    setElemText(70);
    setElemText(100);
    document.getElementById("text731").innerText = "";
    document.getElementById("loginRoleList").hidden = false;
    setElemText(730);
    document.getElementById("text715").hidden = false;
    debug("New instance for customer id " + instance + " was created.", release);
    document.getElementById("clockImage").hidden = true;
}, function (data) {
    document.getElementById("clockImage").hidden = true;
    const err500 = getTextById(500);
    const errText = getTextById(271);
    debug("errror data:", release, data);
    setElemText(731);
    document.getElementById("loginRoleList").innerHTML = "";
    document.getElementById("text730").innerText = errText + ".";
    document.getElementById("text715").innerText = err500;
    document.getElementById("text715").hidden = false;
    debug("Could not create new instance for customer id " + instance, release);
    document.getElementById("clockImage").hidden = true;
});