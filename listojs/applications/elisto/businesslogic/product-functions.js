
module = "product-functions.js";

/*
 * Admin-Menu/ Function 4 / PRODUCTS MODULE
 *
 *
 *
 *
 *
 *
 */
// SHOW PRODUCTS LIST - event
$(document).on('click', '.product-list', function () {
    showProducts();
});


const showProducts = function () {
    apiCall_elisto_public_products(function (data) {
        const action1 = [
            "edit_element_product",
            actionModifyStyle.elements[0] + getTextById(105)
            + actionModifyStyle.elements[1]];
        const action2 = [
            "delete_element_product",
            actionDeleteStyle.elements[0] + getTextById(106)
            + actionDeleteStyle.elements[1]];
        const action3 = ["product_gallery", actionModifyStyle.elements[0] + getTextById(318) + actionModifyStyle.elements[1]];
        const action4 = ["product_attributes", actionModifyStyle.elements[0] + getTextById(320) + actionModifyStyle.elements[1]];
        const dataColumnNames1 = ["idproduct", "prodname", "price",
            "image", "image_thumb", "prodname_en",
            "prodinfo_en", "proddesc_en", "position",
            "qty_in_stock", "qty_unit", "threshold_qty", "idlabel"];
        const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12];
        const dataColumnNames2 = ["idproduct"];
        const dataColumnIndices2 = [0];
        const ProductObject = {
            IDENTIFIER: "admin-showProducts",
            jsonRMLData: data,
            header: [getTextById(61),"ID1","ID2", getTextById(51),
                getTextById(57), getTextById(52),"",""],
            columnIndices: [5,0, 12, 2, 4, 9],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (colIndex === 4) {
                        return TDopen + "<img src='" + getImagePath() + "/" + data
                            + "' " + ThumbStyle.default + ">" + TDclose;
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(ProductObject, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
};



// 1) Add Product Form
$(document).on(
    'click',
    '.create-addproduct-form',
    function () {
        const idcategory = [FormSelectBox, 53, "product_add_idcategory",
            "rows='1' required ", ''];
        const proddesc_locale = [FormTextArea, 55,
            "product_add_proddesc_en", "rows='3' required ", ''];
        const prodinfo_locale = [FormTextArea, 54,
            "product_add_prodinfo_en", "rows='2' required ", ''];
        const prodlabel = [formTextInput, 233,
            "product_add_prodlabel", "rows='1' required ", ''];
        const prodname_locale = [formTextInput, 61,
            "product_add_prodname_en", "rows='2' required ", ''];
        const image = [formImage, 230, "file_imagefield", "required", "", "fileImage_id"];
        const qty_unit = [formSelectBox, 59, "product_edit_qty_unit",
            "product_add_qty_unit", getTextById(311), getTextById(312), getTextById(313),
            getTextById(314), getTextById(315), getTextById(316), getTextById(317),];
        const threshold_qty = [formNumericInput, 60, "product_add_threshold_qty", "required", ""];
        const price = [formNumericInput, 51, "product_add_price", "required step=.01", ""];
        const qty_in_stock = [formNumericInput, 52, "product_add_qty_in_stock", "required step=1"];
        const submitButton = [FormButton, 215, "add_product", "",
            "add-product"];
        const form1 = [prodname_locale, prodlabel, idcategory,
            image,
            price,
            prodinfo_locale, proddesc_locale,
            qty_in_stock, qty_unit, threshold_qty,
            submitButton];
        listoHTML.createForm("#main-content", 6, form1, 'product-edit-form');
        const categoriesData = [];
        const dupArray = [];
        const prodCatData = [];
        apiCall_elisto_public_categories(function (response) {
            sessionStorage.setItem('uniqueID', '');
            for (let i = 0; i < response.count; i++) {
                const billdata = {};
                if (sessionStorage.getItem('uniqueID') == response.rows[i].s[1]) {
                    if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                        billdata.id = i + 'rep_' + response.rows[i].s[1];
                        dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                    } else {
                        billdata.id = 'rep_' + response.rows[i].s[1];
                    }
                    dupArray.push(response.rows[i].s[1]);
                } else {
                    billdata.id = 'cat_' + response.rows[i].s[1];
                }
                if (response.rows[i].s[0] == 0) {
                    billdata.parent = '#';
                } else {
                    if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = 'rep_' + response.rows[i].s[0];
                    } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = i + 'rep_' + response.rows[i].s[0];
                    } else {
                        billdata.parent = 'cat_' + response.rows[i].s[0];
                    }
                }
                billdata.text =  response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
                for (let pCat = 0; pCat < prodCatData.length; pCat++) {
                    debug("prodcatData: ", release, prodCatData[pCat], billdata.id);
                    if (prodCatData[pCat] == response.rows[i].s[1]) {
                        const nodeState = {"opened": true, "checked": true};
                        billdata.state = nodeState;
                    }
                }
                categoriesData.push(billdata);
                sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
            }
            $('#jstree').jstree({
                'core': {
                    'multiple': true,
                    'data': categoriesData,
                    'check_callback': false
                }, types: {
                    "root": {
                        "icon": "glyphicon glyphicon-record"
                    },
                    "child": {
                        "icon": "glyphicon glyphicon-minus"
                    },
                    "default": {
                        "icon": "glyphicon glyphicon-cutlery"
                    }
                },
                "checkbox": {
                    "three_state": false,
                    "cascade": "up",
                    "tie_selection": false
                },
                plugins: ["types", "data", "checkbox"]
            }).on('open_node.jstree', function (e, data) {
                data.instance.set_icon(data.node, "glyphicon glyphicon-minus");
            }).on('close_node.jstree', function (e, data) {
                data.instance.set_icon(data.node, "glyphicon glyphicon-cutlery");
            }).on("select_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                //$('#product_category').val(idcategory);
            }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                //$('#product_category').val(idcategory);
                (data.node.state.checked ?
                    select_category_of_new_product(idcategory)
                    :
                    unselect_category_of_new_product(idcategory))
            });

        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
    });

let categories_of_new_product = [];

function select_category_of_new_product(selectedCategory) {
    debug("Add product, selected category:", release, selectedCategory);
    categories_of_new_product.push(selectedCategory);
}

function unselect_category_of_new_product(unSelected_category) {
    debug("Add product, unselect category:", release, unSelected_category);
    //indexof
    //categories_of_new_product.slice(idx,1,unSelected category_);
}

// SUBMIT ADD PRODUCT FORM

$(document).on(
    'click',
    '#add_product',
    function () {
        const uuid = getUuidv4();
        debug(uuid);
        if ($("#product-edit-form").valid()) {
            const idcategory = $("#product_add_idcategory").val();
            if (categories_of_new_product.length === 0) {
                alert("Please select at least one category for the product");
                return;
            }
            const proddesc_locale = $("#product_add_proddesc_en").val();
            const prodname_locale = $("#product_add_prodname_en").val();
            const prodlabel = $("#product_add_prodlabel").val();
            const prodinfo_locale = $("#product_add_prodinfo_en").val();
            const qty_unit = $("#product_add_qty_unit").val();
            const threshold_qty = $("#product_add_threshold_qty").val();
            const price = $("#product_add_price").val();
            const qty_in_stock = $("#product_add_qty_in_stock").val();
            const custID = getCustomerId();
            debug("Product label:", 0, prodlabel);
            if (document.getElementById('file_imagefield').value) {
                upload(custID, 'product-edit-form', function (data) {
                    debug("upload-data:", release, data);
                    const imageNew = "" + data.rows[0].s[2];
                    //alert(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'");
                    apiCall_elisto_admin_newproduct(price, proddesc_locale, imageNew, prodinfo_locale, prodlabel, prodname_locale,
                        qty_in_stock, threshold_qty, qty_unit, function (data) {
                            if (data._rc > 0) {
                                alert(getTextById(253) + data._rc);
                            } else {
                                showProducts();
                                debug("Add-product", 0, data);
                            }
                        }, function err() {
                            showHttpErrorMessage("main-content", err);
                        });
                }, function (error) {
                    alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
                });
            } else {
                const noImage = images.missing_image_txt;
                apiCall_elisto_admin_newproduct(price, proddesc_locale, noImage, prodinfo_locale, prodlabel, prodname_locale,
                    qty_in_stock, threshold_qty, qty_unit, function (data) {
                        if (data._rc > 0) {
                            alert(getTextById(253) + data._rc);
                        } else {
                            showProducts();
                            debug("Add-product", 0, data);
                        }
                    }, function err() {
                        showHttpErrorMessage("main-content", err);
                    });
            }
            const numberOfNewProducts = categories_of_new_product.length;
            for (let k = 0; k < numberOfNewProducts; k++) {
                debug("Add new pcategory for product id " + uuid, 0, categories_of_new_product[k]);
            }
        } else {
            debug("Invalid form: missing fields detected in #add-category-form", 0);
        }
    });

function getUuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}

// 2. Create form with editable Product data
$(document).on(
    'click',
    '.edit_element_product',
    function () {
        sessionStorage.setItem('imagePath', '');
        const product_id = $(this).data('idproduct');
        const selectedCategory = $(this).data('idcategory');
        const idlabel = [formTextInput, 233, 'idlabel', 'required', $(this).data('idlabel')];
        const idcategory = [FormSelectBox, 53, 'product_edit_idcategory',
            'rows="1" required ', selectedCategory];
        const imageSrc = $(this).data('image');
        sessionStorage.setItem('image', imageSrc);
        // TODO: use layout-identifier for specific product-visualization
        //const idStyle = [formNumericInput, 262, "product_edit_style", $(this).data('idprodstyle')];
        const proddesc_locale = [FormTextArea, 55,
            "product_edit_proddesc_en", "rows='1' required ",
            $(this).data("proddesc_en")];
        const prodinfo_locale = [FormTextArea, 54,
            "product_edit_prodinfo_en", "rows='1' required ",
            $(this).data("prodinfo_en")];
        const displayedProdname = $(this).data("prodname_en");
        const prodname_locale = [formTextInput, 61,
            "product_edit_prodname_en", "rows=2 required ",
            displayedProdname];
        const shownImage = [formDisplayImage, 56, "shownImage", "longdesc=\"" + imageSrc + "\"", getImagePath() + "/" + imageSrc];
        const image = [formImage, 56, "file_imagefield", "",
            "", $(this).data("image")];
        const image_thumb = [formThumb, 57, "product_file_thumb",
            "required ", "", $(this).data("image_thumb")];
        const qty_unit = [formSelectBox, 59, "",
            "product_edit_qty_unit", getTextById(311), getTextById(312), getTextById(313),
            getTextById(314), getTextById(315), getTextById(316), getTextById(317),];
        const threshold_qty = [formNumericInput, 60, "product_edit_threshold_qty", "required step=1", $(this).data("threshold_qty")];
        const price = [formNumericInput, 51, "product_edit_price",
            "required step=.01", $(this).data("price")];
        const qty_in_stock = [formNumericInput, 52, "product_edit_qty_in_stock",
            "required step=1", $(this).data("qty_in_stock")];
        const listPosition = [formNumericInput, 58, "product_edit_position_inlist",
            "required step=1", $(this).data("position")];
        const external_link = [formTextInput, 616, "product_edit_external_link",
            "", $(this).data("extLink")];
        const submitButton = [FormButton, 215, "edit_product",
            "data-idproduct=\"" + product_id + "\"", "edit_product"];
        const galleryButton = [FormButton, 318, "product_gallery", "data-idproduct=\"" + product_id + "\"", "product_gallery"];
        const attributeButton = [FormButton, 320, "product_attributes", "data-idproduct=\"" + product_id + "\"", "product_attributes"];
        const cancelButton = [FormButton,602, "backtoAllProductList", "", "cancel-button"];
        const form1 = [prodname_locale, idlabel, idcategory, shownImage, image, image_thumb, price,
            prodinfo_locale, proddesc_locale,
            qty_in_stock, qty_unit, threshold_qty,listPosition,external_link,
            submitButton, galleryButton, attributeButton, cancelButton];
        listoHTML.createForm("#main-content", 6, form1, 'product-edit-form');

        const prodCatData = [];
        const parentCatData = [];
        apiCall_elisto_public_productcategories(product_id, function (data) {
            if (data.count === 0){
                createProductTree(product_id,[],[]);
            }
            for (let prodcatIdx = 0; prodcatIdx < data.count; prodcatIdx++) {
                prodCatData.push(data.rows[prodcatIdx].s[3]);
            }
            let category;
            for (let prodcatIdx = 0; prodcatIdx < data.count; prodcatIdx++) {

                category=data.rows[prodcatIdx].s[3];
                apiCall_elisto_public_parentcategories(category, function (parentData) {
                    for (let i = 0; i < parentData.count; i++) {
                        if (!parentCatData.includes(parentData.rows[i].s[0])) parentCatData.push(parentData.rows[i].s[0]);
                    }
                    if (prodcatIdx === data.count - 1 ) {
                        debug("prodCatData:", 0, prodCatData);
                        debug("parentCatData:", 0, parentCatData);
                        createProductTree(product_id,prodCatData,parentCatData);
                    }
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                })
            }

        }, function (err) {
            debug("Cannot find category for product-id:", 0, product_id, err);
        });


    });

function createProductTree(product_id,prodCatData,parentCatData){
    const categoriesData = [];
    const dupArray = [];
    apiCall_elisto_public_categories(function (response) {
        sessionStorage.setItem('uniqueID', '');
        for (let i = 0; i < response.count; i++) {
            const billdata = {};
            if (sessionStorage.getItem('uniqueID') == response.rows[i].s[1]) {
                if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                    billdata.id = i + 'rep_' + response.rows[i].s[1];
                    dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                } else {
                    billdata.id = 'rep_' + response.rows[i].s[1];
                }
                dupArray.push(response.rows[i].s[1]);
            } else {
                billdata.id = 'cat_' + response.rows[i].s[1];
            }
            //billdata.id=response.rows[i].s[0] ;
            if (response.rows[i].s[0] == 0) {
                billdata.parent = '#';
            }
            else {
                //billdata.parent = response.rows[i].s[0];
                if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                    billdata.parent = 'rep_' + response.rows[i].s[0];
                } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                    billdata.parent = i + 'rep_' + response.rows[i].s[0];
                } else {
                    billdata.parent = 'cat_' + response.rows[i].s[0];
                }
            }
            billdata.text = response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
            let nodeState = {};
            if (prodCatData.includes(response.rows[i].s[1])) {
                nodeState = {"opened": true, "checked": true};
            } else if (parentCatData.includes(response.rows[i].s[1])) {
                nodeState = {"opened": true, "checked": false};
            } else nodeState = {"opened": false, "checked": false};
            billdata.state = nodeState;
            categoriesData.push(billdata);
            sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
        }
        //debug("Check tree-data", 0, categoriesData);
        $('#jstree').jstree({
            'core': {
                'data': categoriesData,
                'check_callback': false
            }, types: {
                "root": {
                    "icon": "glyphicon glyphicon-record"
                },
                "child": {
                    "icon": "glyphicon glyphicon-minus"
                },
                "default": {
                    "icon": "glyphicon glyphicon-cutlery"
                }
            },
            "checkbox": {
                "three_state": false,
                "whole_node": false,
                //"cascade": "undetermined",
                "tie_selection": false
            },
            plugins: ["types", "changed", "data", "checkbox"]
        }).on('open_node.jstree', function (e, data) {
            data.instance.set_icon(data.node, "glyphicon glyphicon-minus");
        }).on('close_node.jstree', function (e, data) {
            data.instance.set_icon(data.node, "glyphicon glyphicon-cutlery");
        }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
            const cNode = "" + data.node.id;
            const underlineIdx = cNode.indexOf("_") + 1;
            const idcategory = cNode.substring(underlineIdx, cNode.length);
            (data.node.state.checked ?
                setProductCategory(idcategory, product_id, data)
                :
                unSetProductCategory(idcategory, product_id, data))
        });
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

function setProductCategory(idcategory, idproduct, treeData) {
    apiCall_elisto_admin_setproductcategory(idcategory, idproduct, function (data) {
        debug('setProductCategory', 0, idproduct + '->' + idcategory);
        if (data._rc > 0) {
            alert(getTextById(265));
            treeData.node.state.checked = false;
        }
    }, function (err) {
        treeData.node.state.checked = false;
        showHttpErrorMessage("main-content", err);
        alert(getTextById(265));
    });
};


function unSetProductCategory(idcategory, idproduct, treeData) {
    apiCall_elisto_admin_unsetproductcategory(idcategory, idproduct, function (data) {
        debug('unSetProductCategory', 0, idproduct + '->' + idcategory);
        if (data._rc > 0) {
            alert(getTextById(266));
            treeData.node.state.checked = true;
        }
    }, function (err) {
        treeData.node.state.checked = true;
        showHttpErrorMessage("main-content", err);
        alert(getTextById(266));
    });
}


// 3. Edit product-data with the form and submit
$(document)
    .on(
        'click',
        '.edit_product',
        function () {

            if ($("#product-edit-form").valid()) {
                // language=HTML
                let image;
                if (document.getElementById("file_imageField"))
                    image = document.getElementById("file_imageField").value;
                const proddesc_locale = document
                    .getElementById("product_edit_proddesc_en").value;
                debug("image in valid-form:" + image);
                const idproduct = $(this).data("idproduct");
                const productLabel = document
                    .getElementById("idlabel").value;

                const prodinfo_locale = document
                    .getElementById("product_edit_prodinfo_en").value;

                const prodname_locale = document
                    .getElementById("product_edit_prodname_en").value;
                const price = document.getElementById("product_edit_price").value;
                // TODO: use own style for every product, by considering the style-id
                //const style = document.getElementById("product_edit_style").value;
                const qty_in_stock = document
                    .getElementById("product_edit_qty_in_stock").value;
                const listPosition= document
                    .getElementById("product_edit_position_inlist").value;
                debug("listPosition="+listPosition);
                sessionStorage.getItem("image");

                const qty_unit_element = document
                    .getElementById("product_edit_qty_unit");
                const qty_unit = qty_unit_element.options[qty_unit_element.selectedIndex].text;
                const threshold_qty = document
                    .getElementById("product_edit_threshold_qty").value;
                const custID = getCustomerId();
                if (document.getElementById('file_imagefield').value) {
                    upload(custID, 'product-edit-form', function (data) {
                        debug("upload-data:", release, data);
                        const imageNew = "" + data.rows[0].s[2];
                        const image_thumbNew = imageNew;
                        debug(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'", 0);
                        apiCall_elisto_admin_editproduct(productLabel, -1, idproduct,
                            imageNew, image_thumbNew, listPosition, price, proddesc_locale,
                            prodinfo_locale, prodname_locale, prodname_locale,
                            qty_in_stock, qty_unit, threshold_qty, function (data2) {
                                if (data2._rc > 0) {
                                    alert(getTextById(253) + data2._rc);
                                } else {
                                    showProducts();
                                }
                            }, function (err) {
                                showHttpErrorMessage("main-content", err);
                            });
                    }, function (error) {
                        alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
                    });
                } else {
                    image = document.getElementById("shownImage").getAttribute("longdesc");
                    debug("image in edit_product:", 0, image);
                    if (!image)
                        image = images.missing_image_txt;
                    apiCall_elisto_admin_editproduct(productLabel, -1, idproduct,
                        image, image, listPosition, price, proddesc_locale,
                        prodinfo_locale, prodname_locale, prodname_locale,
                        qty_in_stock, qty_unit, threshold_qty, function (data) {
                            if (data._rc > 0) {
                                alert(getTextById(253) + data._rc);
                            } else {
                                showProducts();
                            }
                        }, function (err) {
                            showHttpErrorMessage("main-content", err);
                        });
                }
            } else {
                debug("Invalid Form: Missing fields detected in 'edit-product'", 0);
            }
        });


/* FUNCTION FOR DELETING THE PRODUCT */

$(document)
    .on(
        'click',
        '.delete_element_product',
        function () {
            const productId = $(this).data("idproduct");
            apiCall_elisto_admin_deleteproduct(productId, function (data) {
                if (data._rc > 0) {
                    alert(getTextById(253) + data._rc);
                } else {
                    showProducts();
                }

            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        }
    );


// BACK TO ALL PRODUCTS LIST
$(document).on('click', '#backtoAllProductList', function(){
        showProducts();
});