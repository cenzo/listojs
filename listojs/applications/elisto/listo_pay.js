/* listo_pay.js  
28.10.2020 21:19:38,89 */ 
/*-----------------------------------*/ 
/* payments.js */ 
/*
const callingPage = sessionStorage.getItem("payment_callingPage");
const backlink = document.getElementById("backlinkid");
const backlinkText = document.getElementById("backlinktextid");
backlink.setAttribute("href", callingPage);
backlinkText.innerText = getTextById(734);

function setPaymentTexts() {
    for (let txtID = 742; txtID < 754; txtID++) {
        setElemText(txtID);
    }
}

async function connectCheckout(CHECKOUT_SESSION_ID, CONNECTED_STRIPE_ACCOUNT_ID) {
    // Initialize Stripe.js with the same connected account ID used when creating
    // the Checkout Session.
    const stripe = Stripe('pk_test_WWbnKC93qOMARiC4lbirfDrg00CuCspHmW', {
        stripeAccount: CONNECTED_STRIPE_ACCOUNT_ID
    });
    //const elements = stripe.elements();
    const {error} = await stripe.redirectToCheckout({
        // Make the id field from the Checkout Session creation API response
        // available to this file, so you can provide it as parameter here
        // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
        sessionId: CHECKOUT_SESSION_ID
    }).onerror(console.error(error));
// If `redirectToCheckout` fails due to a browser or network
// error, display the localized error message to your customer
// using `error.message`.
}


async function checkout(CHECKOUT_SESSION_ID) {
    const stripe = Stripe('pk_test_WWbnKC93qOMARiC4lbirfDrg00CuCspHmW');
    //const elements = stripe.elements();
    const {error} = await stripe.redirectToCheckout({
        sessionId: CHECKOUT_SESSION_ID
    }).onerror(console.error(error));
}


$(document).on('click', '.checkoutLinkBankTransfer', function () {
    const productID = $(this).data("productid");
    const productName = $(this).data("productname");
    const productDescription = $(this).data("productdescription");
    const currency = $(this).data("currency");
    const qty = Number(document.getElementById("creditqty").innerText).toFixed(0);
    const price = Number(document.getElementById("selectedprice").innerText).toFixed(0);
    const newDesc = productDescription + " (= " + Number(qty * 1000) + " clicks)";
    console.log("Checkout with bank-transfer clicked:\n productID:" + productID + ",\n price:" + price + ",\n product: " + productName + ",\nDescription: " +
        newDesc + ",\n" + currency + ",\nqty: " + qty);
    updateCredits(1, 'noSessionID', function () {

    }, function (err) {
        showHttpErrorMessage("main-content", err);
    })
});

$(document).on('click', '.checkout-stripe', function () {

    const productID = $(this).data("productid");
    const productName = $(this).data("productname");
    const productDescription = $(this).data("productdescription");
    const currency = $(this).data("currency");
    const qty = Number(document.getElementById("creditqty").innerText).toFixed(0);
    const price = Number(document.getElementById("selectedprice").innerText).toFixed(0);
    const newDesc = productDescription + " (= " + Number(qty * 1000) + " clicks)";
    console.log("Checkout clicked:\n productID:" + productID + ",\n price:" + price + ",\n product: " + productName + ",\nDescription: " +
        newDesc + ",\n" + currency + ",\nqty: " + qty);
    stripePayment(1, productID, price, productName, newDesc, currency, qty, function (data) {
        console.log("Data returned from stripe: " + JSON.stringify(data));
        const stripe_session_id = data.id;
        if (!stripe_session_id) {
            console.error("NO STRIPE SESSION ID FOUND: " + JSON.stringify(data));
            showHttpErrorMessage("main-content", data);
        }
        console.log("Returned session id:" + stripe_session_id);
        checkout("" + stripe_session_id).then(function (data) {
            console.log("checkout completed");
            console.log("Data returned from session: " + JSON.stringify(data));
        }, function (err) {
            console.error("checkout failed: " + err);
        });
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});


$(document).on('click', '.checkout-stripeconnect', function () {
    const productID = 1;
    const productName = getTextById(33);
    const productDescription = getTextById(745);
    const currency = "eur";
    const qty = 1;
    const price = 10000;
    const newDesc = productDescription + " :-) ";
    console.log("Checkout clicked:\n productID:" + productID + ",\n price:" + price + ",\n product: " + productName + ",\nDescription: " +
        newDesc + ",\n" + currency + ",\nqty: " + qty);
    const stripe_account = getStripeAccountID();
    const receiptEmail = sessionStorage.getItem("userEmail");
    console.log("receiptEmail=" + receiptEmail);
    stripeConnectPayment(stripe_account, 1, productID, price, productName, newDesc, currency, receiptEmail, "card", qty, function (data) {
        console.log("Data returned from stripe: " + JSON.stringify(data));
        const stripe_session_id = data.id;
        if (!stripe_session_id) {
            console.error("NO STRIPE SESSION ID FOUND: " + JSON.stringify(data));
            showHttpErrorMessage("main-content", data);
        }
        console.log("Returned session id:" + stripe_session_id);
        connectCheckout("" + stripe_session_id, "" + stripe_account).then(function (data) {
            console.log("checkout completed");
            console.log("Data returned from session: " + JSON.stringify(data));
            sendMail(1, "NOREPLY", receiptEmail, "Payment succeeded", JSON.stringify(data), "key", function () {
                console.log("Payment and mail sending succeeded");
            }, function () {
                console.log("Payment suceeded and mail sending failed?");
            });
        }, function (err) {
            console.error("checkout failed: " + err);
            sendMail(1, "NOREPLY", receiptEmail, "Payment failed", JSON.stringify(data), "key", function () {
                console.log("Payment failed and mail sending succeeded");
            }, function () {
                console.log("Payment and mail sending failed");
            });
        });
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});

*/