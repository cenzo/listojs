/* elisto_executive.js  
28.10.2020 21:19:38,83 */ 
/* executive.js */ 
module = "waiter.js";
//applicationID = 1;
const bill = getUrlVars()['bill'];
const table = getUrlVars()['table'];
let infoLabel = document.getElementById('infoLabel');
//let waiterName = getUrlVars()['waiterName'];
let nrOnlyListedItems = 0;
const products = [];
$('#infoLabelFocus').fadeOut(25);
release = "d1a6f_2";
//const version = '1.2.7';
softwareProduct = 'Listorante Waiter-HandHeld'
let action = 'add-product';
let previousCatid;
let box1Quantities = [];
let box1QuantityIndices = [];
const currentUrl = window.location.href;
sessionStorage.setItem("payment_callingPage", currentUrl);
setCustomerId();

function printHtml(content, styleSheet) {
    const output = document.getElementById("printIframe").contentWindow;
    output.document.open();
    if (styleSheet !== undefined) {
        output.document.write('<link href="' + styleSheet
            + '" rel="stylesheet" type="text/css" />');
    }
    output.document.write(content);
    output.document.close();
    debug("print output", '696d31e2', content);
    output.focus();
    output.print();
    const receiptEmail = sessionStorage.getItem("userEmail");
    sendMail(1, "NOREPLY", receiptEmail, "Payment succeeded", content, "key", function () {
        console.log("Test  mail sending succeeded");
    }, function () {
        console.log("Test mail sending failed?");
    });
}

function setTexts() {
    for (let txtID = 1; txtID < 754; txtID++) {
        //true-flag means "ignore-errors"
        setElemTextIgnoreErrors(txtID);
    }
}

function setFocus(objectId) {
    document.getElementById(objectId).focus();
};

let printOrderForKitchen = function (divId, printedTable, printedBill, printedTime) {
    // stackoverflow.com/questions/2255291/print-the-contents-of-a-div
    // stackoverflow.com/questions/10541079/print-in-the-background-with-javascript-i-e-without-document-pop-up
    let html = '<html><head><title>' + printedTime + 'Print</title>';
    html += '</head><body >';
    html += '<h4>' + printedTime + '</h4>';
    html += '<h4>' + getTextById(28) + '&nbsp' + printedTable + '</h4>';
    html += '<h4>' + getTextById(33) + '&nbsp' + printedBill + '</h4>';
    html += document.getElementById(divId).innerHTML;
    html += '</body></html>';
    printHtml(html);
};
apiCall_elisto_waiter_getwaiterprofile(function (data) {
    waiterName = data.rows[0].s[1];
    //debug('profile: ', 0, data);
    document.getElementById('waiterID').innerText = waiterName;
    document.getElementById('waitername').innerText = data.rows[0].s[3]
        + '&nbsp;&nbsp;' + data.rows[0].s[4];
}, function (err) {
    showHttpErrorMessage('main-content', err);
});


//showNotifications();
$(document)
    .on(
        'click',
        '.set-Settings',
        function () {
            apiCall_elisto_waiter_getwaiterprofile(function (data) {
                const image = ThumbImage(85,
                    `${getImagePath()}/${data.rows[0].s[2]}`);
                const profileImageFile = formImage(213, "file_imagefield", "", "", "profileImage_id");
                const uploadImageButton = FormButton(319, 'uploadImageButtonID', '',
                    'upload-ProfilePicture');
                const username = formTextInput(82, 'waiter_edit_username',
                    'rows=\'1\' required', data.rows[0].s[1]);
                const first_name = formTextInput(76, 'waiter_edit_first_name',
                    'rows=\'1\' required', data.rows[0].s[3]);
                const last_name = formTextInput(80, 'waiter_edit_last_name',
                    'rows=\'1\' required', data.rows[0].s[4]);
                const email = formTextInput(75, 'waiter_edit_email',
                    'rows=\'1\' required', data.rows[0].s[5]);
                const submitButton = FormButton(105, 'edit_waiter_profile', '',
                    'edit-waiter-profile');
                const changePasswordButton = FormButton(250, 'setPassword', '',
                    'set-Password');
                const messageLabel = formLabel('', 'profileMsgLabelD')
                const form = [image, profileImageFile, uploadImageButton, username, first_name, last_name, email,
                    submitButton, changePasswordButton, messageLabel];
                listoHTML.buildForm('#main-content', 6, form, 'profile_form');
            }, function (err) {
                showHttpErrorMessage('main-content', err);
            });
        });

$(document).on(
    'click',
    '.upload-ProfilePicture', function () {
        debug("upload profile picture", 0);
        upload(customerID, 'profile_form', function (data) {
            const profileImage = data.rows[0].s[2];
            apiCall_elisto_waiter_changewaiterimage(profileImage, function (data) {
                if (data._rc > 0) showHttpErrorMessage('main-content', data._rc);
            }, function (err) {
                showHttpErrorMessage('main-content', err);
            });
            document.getElementById('profileMsgLabelD').innerText = getTextById(611);
        }, function (err) {
            showHttpErrorMessage('main-content', err);
        });
    });

$(document).on(
    'click',
    '.set-Password',
    function () {
        const password = formPassWord(81, 'waiter_edit_password',
            'rows="1" required ', '');
        const passwordConfirm = formPassWord(81, 'waiter_confirm_password',
            'rows="1" required ', '');
        const submitButton = FormButton(105, 'change_password_button', '',
            'change_password');
        const messageLabel = formLabel('', 'messageLabelID');
        const form = [password, passwordConfirm,
            submitButton, messageLabel];
        listoHTML.buildForm('#main-content', 6, form, 'change_password');

    });
$(document).on(
    'click',
    '.change_password',
    function () {
        const password = $('#waiter_edit_password').val();
        const passwordConfirm = $('#waiter_confirm_password').val();
        if ($("#change_password").valid()) {
            if (password === passwordConfirm) {
                apiCall_elisto_waiter_changewaiterpassword(password, function (data) {
                    document.getElementById('messageLabelID').innerText = getTextById(609);
                }, function (err) {
                    showHttpErrorMessage('main-content', err);
                });
            } else {
                document.getElementById('messageLabelID').innerText = getTextById(608);
            }
        }
    });

$(document).on(
    'click',
    '#edit_waiter_profile',
    function () {
        const waiter_name = $('#waiter_edit_username').val();
        const first_name = $('#waiter_edit_first_name').val();
        const last_name = $('#waiter_edit_last_name').val();
        const email = $('#waiter_edit_email').val();
        document.getElementById('profileMsgLabelD').innerText = '';
        apiCall_elisto_waiter_editwaiterprofile(email, first_name,
            last_name, waiter_name, function (data) {
                debug("edit_waiter_profile", 0, data);
                document.getElementById('profileMsgLabelD').innerText = getTextById(610);
            }, function (err) {
                showHttpErrorMessage('main-content', err);
            });
    });


function showSubCategories(category, catParent) {
    // CALL-ID: listorante.Public.4
    apiCall_elisto_public_category(category, function (data) {
        update0_Categories(category, data, catParent);
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}

function searchProduct() {
    document.getElementById('box0').innerHTML = `<a class="btn btn-sm  get-products" style="padding: 10px; border-radius: 25px;" data-category="0">
<i class="fa fa-sitemap" ></i></a>`;
    let subData = {
        count: 0,
        _rc: 0,
        rows: []
    };
    apiCall_elisto_public_products(function (data) {
        subData.count = data.count;
        subData._rc = data._rc;
        for (let i = 0; i < data.count; i++) {
            let s2 = {
                rn: i,
                s: []
            };
            s2.s[0] = data.rows[i].s[0];
            s2.s[1] = data.rows[i].s[8];
            s2.s[2] = data.rows[i].s[5];
            s2.s[3] = data.rows[i].s[2];
            s2.s[4] = data.rows[i].s[2];
            s2.s[5] = "0";
            s2.s[6] = data.rows[i].s[12];
            subData.rows.push(s2);
        }
        update1_ProductList(subData);
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
};

function showProductsOfCategory(categoryId, catParent) {
    if (parseInt(categoryId) === 0) {
        document.getElementById('box1').innerHTML = '';
    } else {
        apiCall_elisto_public_product(categoryId, function (data) {
            update1_ProductList(data);
        }, function (err) {
            showHttpErrorMessage('main-content', err);
        });
    }
}

function addProduct(idBillNumber, prodId, tableNumber) {
    //let count=0;
    apiCall_elisto_waiter_addtoproductlist(idBillNumber, prodId, tableNumber,
        function (data) {
            const qty = data[4].rows[0].s[1];
            if (qty < 3) {
                infoLabel.innerHTML = getTextById(38) + qty;
                $('#infoLabelFocus').fadeIn(25);
                setFocus('infoLabelFocus');
                $('#infoLabelFocus').fadeOut(25);
            }
            // document.getElementById('addProductButton'+prodId).setAttribute("style=","margin:0 0 3px 3px;");
            showCart(idBillNumber, tableNumber);
            showDeliveryStatus(idBillNumber);
        }, function (err) {
            showHttpErrorMessage('main-content', err);
        });

    // TODO
    /*
     *
     * const clickedIconDiv = document.createElement('div');
    clickedIconDiv.setAttribute('class', ' check' + prodid);
    const clickedIcon = document.createElement('i');
    clickedIcon.setAttribute('class', 'fa fa-check');
    clickedIconDiv.appendChild(clickedIcon);
    count++;
    if (count > 1)
      clickedIcon.innerHtml = ' X ' + count;
    document.getElementById('addProductButton' + prodid).appendChild(
      clickedIconDiv);*/
}

jQuery(document).on('click', '.itemRemark', function () {
    const productName = $(this).attr('data-productName');
    const idOrder = $(this).attr('data-idorder');
    const remark = $(this).attr('data-remark');
    const remarkLabel = formLabel(productName, '');
    const remarkInputText = formTextInput(601, 'remark_text_id', 'required', remark);
    const submitRemark = FormButton(215, '', `data-idOrder="${idOrder}" data-productName="${productName}" 
			data-remark="${remark}"`, "set-Remark");
    const formArray = [remarkLabel, remarkInputText, submitRemark];
    listoHTML.buildForm("#main-content", 6, formArray, "set-Remark-Form");
});

$(document).on('click', '.del-item', function () {
    const idOrder = $(this).attr('data-idOrder');
    const idProduct = $(this).attr('data-idProduct');
    const billStatus = $(this).attr('data-billStatus');
    const idBillNumber = document.getElementById('billNr').innerHTML;
    const nameOfTable = document.getElementById('tableName').innerHTML;
    const tableNumber = document.getElementById('idTable').innerHTML;
    const nGuests = document.getElementById('guestCount').innerHTML;
    apiCall_elisto_waiter_delitem(idOrder, idProduct, function (data) {
        if (data._rc > 0) showHttpErrorMessage(data._rc);
        else {
            showCart(idBillNumber, tableNumber);
            showDeliveryStatus(idBillNumber);
            openBill(idBillNumber, tableNumber, nameOfTable, nGuests, billStatus);
        }
    }, function errHandling(err) {
        showHttpErrorMessage('main-content', err.status);
    });
});

$(document).on('click', '.cancel-del', function () {
    const billStatus = $(this).attr('data-billStatus');
    const idBillNumber = document.getElementById('billNr').innerHTML;
    const nameOfTable = document.getElementById('tableName').innerHTML;
    const tableNumber = document.getElementById('idTable').innerHTML;
    const nGuests = document.getElementById('guestCount').innerHTML;
    openBill(idBillNumber, tableNumber, nameOfTable, nGuests, billStatus);
});

function deleteOrderFromList(idOrder, idProduct, productName, billStatus) {
    const focusDummy = [formTextInput, 601, 'tmpFocusID', '', ''];
    const deleteLabel = [formLabel, `${getTextById(37)} (${productName})`, ''];
    const submitDeletion = [FormButton, 2, 'deleteButton_ID' + idOrder, `data-idOrder="${idOrder}"
        data-billStatus="${billStatus}" data-idProduct="${idProduct}"
        data-idOrder="${idOrder}"`, "del-item"];
    const cancelDeletion = [FormButton, 602, '', '', "cancel-del"];
    const formArray = [deleteLabel, focusDummy, submitDeletion, cancelDeletion];
    listoHTML.createForm("#main-content", 6, formArray, "delete_Product");
    setFocus('tmpFocusID');
    $("#tmpFocusID").fadeOut(20);
}

function startBills() {
    // CALL-ID: listorante.Waiter.8
    apiCall_elisto_waiter_waiterdash(function (data) {
        createBillsOverview(data);
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}

function createBillsOverview(jsonRMLData, onlyArchive) {

    const resultLength = jsonRMLData.length;

    //showNotifications();
    let archivedBillsObject;
    document.getElementById('reOpenOrderButton').style.visibility = 'hidden';
    document.getElementById('printOrderButton').style.visibility = 'hidden';
    document.getElementById('checkOutButton').style.visibility = 'hidden';
    document.getElementById('archiveButton').style.visibility = 'visible';
    document.getElementById('tableName').innerHTML = '';
    document.getElementById('idTable').innerHTML = '';
    document.getElementById('billNr').innerHTML = '';
    document.getElementById('guestCount').innerHTML = '';
    document.getElementById('main-content').innerHTML = '';


    for (let r = 0; r < resultLength; r++) {
        const myObj = jsonRMLData[r];
        if (r == 2) {
            const waiterID = myObj.rows[0].s[0];
            const waitername = myObj.rows[0].s[1];
            document.getElementById('waiterID').innerHTML = waiterID;
            document.getElementById('waitername2').innerHTML = waitername;
            document.getElementById('waitername').innerHTML = myObj.rows[0].s[2] + '&nbsp;' + myObj.rows[0].s[3];
            document.getElementById('user-photo1').setAttribute('src', getImagePath() + '/' + myObj.rows[0].s[4]);
            document.getElementById('user-photo2').setAttribute('src', getImagePath() + '/' + myObj.rows[0].s[4]);
            document.getElementById('user-photo3').setAttribute('src', getImagePath() + '/' + myObj.rows[0].s[4]);
            continue;
        } else if (r == 1) {

            document.getElementById('main-content').innerHTML = '';
            const data = jsonRMLData[r];
            let subData = {
                count: data.count,
                rows: []
            };
            for (let j = 0; j < data.count; j++) {
                let rowArr = data.rows[j];

                let minutesDiff = rowArr.s[9];

                let billSpan, span2, span3, span4, span5, span6, anchorTag, secondAnchorTag, folderIcon, boldTag,
                    insTag, insTagClose, spanClose, anchorClose, boldTagClose, billSpanHtml, span2Html, span3Html,
                    span4Html, span5Html, span6Html;

                insTag = startInsTag('', '', '');
                insTagClose = endInsTag();
                boldTag = startBold('', '', '');
                boldTagClose = endBold();
                spanClose = endSpan();
                anchorClose = endAnchor();


                anchorTag = startAnchor('btn btn-app open-Order', '', ' data-table="' + rowArr.s[1] + '" data-tableName="' + rowArr.s[0] + '" data-bill="' + rowArr.s[2] + '" data-guestNumber="' + rowArr.s[8] + '" data-billstatus="' + rowArr.s[7] + '"');

                secondAnchorTag = startAnchor('btn btn-app open-Order', '', ' data-table="' + rowArr.s[1] + '" data-tableName="' + rowArr.s[0] + '" data-bill="' + rowArr.s[2] + '" data-guestNumber="' + rowArr.s[8] + '" data-billstatus="' + rowArr.s[7] + '"');

                if (rowArr.s[8] == 0 || rowArr.s[7] == 2) {
                    billSpan = startSpan('badge bg-orange', '', '');
                    span3 = startSpan('badge bg-orange', '', '');

                } else {
                    billSpan = startSpan('badge bg-light-blue', '', '');
                    span3 = startSpan('badge bg-light-green', '', '');
                }

                span2 = startSpan('btn  btn-xs bg-navy', '', '');


                if (minutesDiff > 60) {
                    span4 = startSpan('badge bg bg-red', '', '');
                } else {
                    span4 = startSpan('badge bg bg-light-green', '', '');
                }


                if (rowArr.s[7] == 1) {
                    span5 = startSpan('badge bg-olive', '', '');
                    span5Html = `${span5}${spanClose}`;
                } else if (rowArr.s[7] == 2) {
                    span5 = startSpan('badge bg-red', '', '');
                    span5Html = `${span5}${insTag}${boldTag} * ${boldTagClose}${insTagClose} ${rowArr.s[4]}${spanClose}`;
                } else if (rowArr.s[7] > 2) {
                    span5 = startSpan('badge bg-purple', '', '');
                    span5Html = `${span5} ${rowArr.s[4]} ${spanClose}`;
                }

                span6 = startSpan('badge bg-light-green', '', '');

                if (rowArr.s[6] === null && typeof rowArr.s[6] === "object") {
                    span6Html = ``;// comment
                } else {
                    span6Html = `${span6} ${rowArr.s[6]} ${spanClose}`;// comment
                }

                if (rowArr.s[8] == 0 || rowArr.s[7] == 2) {
                    billSpanHtml = `${billSpan}${insTag}${boldTag} * ${boldTagClose}${insTagClose} # ${rowArr.s[2]}${spanClose}`;
                } else {
                    billSpanHtml = `${billSpan} # ${rowArr.s[2]}${spanClose}`;
                }

                folderIcon = startSpan('fa fa-folder-open-o', '', '');

                span2Html = `${span2} ${rowArr.s[0]} ${spanClose}`;

                span3Html = `${span3} ${rowArr.s[8]} ${spanClose}`;
                span4Html = `${span4} ${minutesDiff} min ${spanClose}`;


                let persNrIcon = startIcon('fa fa-user-times', '', '') + rowArr.s[8] + endIcon();


                let timeIcon = startIcon('fa fa-clock-o', '', '') + endIcon();

                //span6Html = `${span6} ${rowArr.s[6]} ${spanClose}`;// comment

                let firstdataHTML = `${anchorTag}${folderIcon}${spanClose}${billSpanHtml}${span2Html}`;
                let seconddataHTML = `${secondAnchorTag}${persNrIcon}${span4Html}${timeIcon}`;
                let thirddataHTML = `${span5Html}${span6Html}`;


                let dataArray = {
                    rn: j,
                    s: [firstdataHTML, seconddataHTML, thirddataHTML]
                };
                subData.rows.push(dataArray);
            }


            const tableOverviewTableObject = {
                actions: [],
                dataColumnNames: [],
                dataColumnIndices: [],
                IDENTIFIER: 'waiter-Products-Box1',
                jsonRMLData: subData,
                header: ['', '', ''],
                columnIndices: [0, 1, 2],
                rowFormat: function (rowIdx, rowArr) {
                    return rowArr;
                },
                cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
                    return `${TDo}${cellData}${TDc}`;
                }
            };
            listoHTML.createTable.call(tableOverviewTableObject, "main-content", false);
        } else if (r == 0) {
            const openBillAction = ['open-Order', actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
            const dcn = ['bill', 'table', 'tableName', 'guestnumber', 'billstatus'];
            const dci = [2, 1, 0, 8, 7];
            let arr2 = [];
            archivedBillsObject = {
                actions: [openBillAction],
                dataColumnNames: [dcn],
                dataColumnIndices: [dci],
                IDENTIFIER: 'waiter-archivedBills-table',
                jsonRMLData: jsonRMLData[r],
                header: ['', '', ''],
                columnIndices: [0, 1, 2, 4, 5, 6],
                rowFormat: function (r, arr) {
                    arr2[0] = arr[0] + " / #" + arr[2];
                    arr2[1] = arr[4] + " / " + arr[5];
                    return arr2;
                },
                cellFormat: function (c, r, tdo, cell, tdc) {
                    if (c === 0) return `${tdo}${colouredValue('purple', cell)}</span>${tdc}`;
                    else if (c === 1) return `${tdo}${colouredValue('orange', cell)}</span>${tdc}`;
                    else return ``;
                }
            };
        }
    }
}

function showDeliveryStatus(idBill) {
    apiCall_elisto_waiter_deliverystatus(idBill, function (data) {
        nrOnlyListedItems =
            update3_DeliveryStatus(data);
        if (nrOnlyListedItems > 0) document.getElementById("nr_listed_orders").innerText = "" + nrOnlyListedItems;
        else document.getElementById("nr_listed_orders").innerText = "0";
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}

function showCart(bill, table) {
    apiCall_elisto_waiter_showcart(bill, table, function (data) {
        update2_Cart(data);
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}

function submitOrder(bill) {
    apiCall_elisto_waiter_sendorder(bill, function (data) {
        if (data._rc > 0) showHttpErrorMessage(data._rc);
        else {
            showDeliveryStatus(bill);
        }
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}


function showNotifications() {
    apiCall_elisto_waiter_getnotifications(function (data) {
        updateNotifications(data);
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}

function update1_ProductList(jsonData) {
    let subData = {
        count: jsonData.count,
        rows: []
    };
    for (let j = 0; j < jsonData.count; j++) {

        let rowArr = jsonData.rows[j].s;
        let price = Number(rowArr[4]).toFixed(2);

        let spanCreatefirst = startSpan('badge bg-purple', 'box1ProdCount' + rowArr[0], '');
        let spanClose = endSpan();
        let iconStart = startIcon(action + ' fa fa-cart-arrow-down', 'addProductButton' + rowArr[0], 'style="border:1px dotted darkgray; padding:5px; border-radius:5px;" data-product="' + rowArr[0] + '" data-productname="' + rowArr[2] + '"  ');
        let iTagClose = endIcon();
        let spanCreatesecond = startSpan('badge bg-purple', '', 'style="margin:0 5px;"');
        let boldTag = startBold('', '', 'style="margin:0 5px;"');
        let boldTagClose = endBold();
        let strongTag = startStrong('', '', 'style="margin:0 5px;"');
        let strongTagClose = endStrong();

        let createAnchor = startAnchor('btn btn-app btn-sm ' + action, '', 'data-product="' + rowArr[0] + '" data-productname="' + rowArr[2] + '" ');
        let IconTag2 = startIcon('fa fa-plus', '', '');
        let anchorClose = endAnchor();

        let sArr0 = `${spanCreatefirst}${spanClose}${iconStart}${spanCreatesecond}${rowArr[6]}${spanClose}${boldTag}${rowArr[2]}${boldTagClose}${strongTag}${price} ${getCurrency()}${strongTagClose}`;

        let sArr1 = `${createAnchor}${IconTag2}${iTagClose}${anchorClose}`;


        let rj = {
            rn: j,
            s: [sArr0, sArr1]
        };
        subData.rows.push(rj);
    }
    const prodObject = {
        actions: [],
        dataColumnNames: [],
        dataColumnIndices: [],
        IDENTIFIER: 'waiter-Products-Box1',
        jsonRMLData: subData,
        header: ['', ''],
        columnIndices: [0, 1],
        rowFormat: function (rowIdx, rowArr) {
            return rowArr;
        },
        cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
            return `${TDo}${cellData}${TDc}`;
        }
    };
    listoHTML.createTable.call(prodObject, 'box1', false);

    /*for (let b1 = 0; b1 < box1Quantities.length; b1++) {
        debug("box1end: "+b1,0);
        document.getElementById('box1ProdCount' + box1QuantityIndices[b1]).innerHTML = " x " +
            Number(box1Quantities[b1]).toFixed(0);
    }*/
    debug("END of update1", 0);

}


jQuery(document).on('click', '.set-Remark', function () {
    const idOrder = $(this).attr('data-idOrder');
    const idBill = document.getElementById('billNr').innerHTML;
    const idTable = document.getElementById('idTable').innerHTML;
    const nGuests = document.getElementById('guestCount').innerHTML;
    const billStatus = document.getElementById('billStatus_ID').innerText;
    const tableName = document.getElementById('tableName').innerHTML;
    const newRemark = document.getElementById('remark_text_id').value;
    apiCall_elisto_waiter_itemremark(idOrder, escape(newRemark), function (data) {
        if (data._rc > 0) showHttpErrorMessage('main-content', data._rc);
        else openBill(idBill, idTable, tableName, nGuests, billStatus);
    }, function (err) {
        showHttpErrorMessage('main-content', err.status);
    })

});


jQuery(document).on('click', '.itemRemark', function () {
    const productName = $(this).attr('data-productName');
    const idOrder = $(this).attr('data-idorder');
    const remark = $(this).attr('data-remark');
    const remarkLabel = [formLabel, productName, ''];
    const remarkInputText = [formTextInput, 601, 'remark_text_id', 'required', remark];
    const submitRemark = [FormButton, 215, '', `data-idOrder="${idOrder}" data-productName="${productName}"
                          data-remark="${remark}"`, "set-Remark"];
    const formArray = [remarkLabel, remarkInputText, submitRemark];
    listoHTML.createForm("#main-content", 6, formArray, "set-Remark-Form");
    setFocus('remark_text_id');
});


function update3_DeliveryStatus(data) {
    let newOrderedItems = 0;
    let sArr2 = [];
    const billStatus = document.getElementById('billStatus_ID').innerText;
    const orderStatus0_maxDuration = document.getElementById('orderStatus0_MaxTime').innerText;
    const orderStatus1_maxDuration = document.getElementById('orderStatus1_MaxTime').innerText;
    const orderStatus2_maxDuration = document.getElementById('orderStatus2_MaxTime').innerText;
    const cartObject = {
        actions: [],
        dataColumns: [],
        dataColumnNames: [],
        dataColumnIndices: [],
        IDENTIFIER: 'waiter-deliveryStatus-table',
        jsonRMLData: data,
        header: [''],
        columnIndices: [2, 3, 4, 7],
        rowFormat: function (rIdx, sArr) {
            sArr2 = sArr;
            let prodName = sArr[2];
            let remark = sArr[7];
            let column1;
            let idStatus = sArr[5];
            if (
                parseInt(idStatus, 10) === 0
                || (parseInt(idStatus, 10) === 1 && parseInt(sArr[3], 10) > parseInt(orderStatus0_maxDuration, 10))
                || (parseInt(idStatus, 10) === 2 && parseInt(sArr[3], 10) > parseInt(orderStatus1_maxDuration, 10))
                || (parseInt(idStatus, 10) === 3 && parseInt(sArr[3], 10) > parseInt(orderStatus2_maxDuration, 10))
            ) {

                let icon1 = startIcon('fa fa-delete', '', '') + endIcon();
                let span1 = startSpan('badge bg-navy', '', '') + sArr[2] + endSpan();
                let span2 = startSpan('badge bg-red', '', '') + startSupTag('', '', '') + sArr[3] + ' min.' + endSupTag() + endSpan();
                let icon2 = startIcon('fa fa-hourglass-half', '', '') + endIcon();
                let span3 = startSpan('badge bg-red', '', '') + '!' + endSpan();

                column1 = `${icon1}${span1}${span2}${icon2}${span3}`;

            } else {

                let icon1 = startIcon('fa fa-delete', '', '') + endIcon();
                let span1 = startSpan('badge bg-navy', '', '') + sArr[2] + endSpan();
                let span2 = startSpan('badge bg-olive', '', '') + startSupTag('', '', '') + sArr[3] + ' min.' + endSupTag() + endSpan();

                column1 = `${icon1}${span1}${span2}`;
            }


            if (parseInt(idStatus, 10) === 0) {
                let span4 = startSpan('badge bg-maroon', '', '') + sArr[4] + endSpan();
                sArr2[3] = `${span4}`;
                newOrderedItems++;
                nrOnlyListedItems = newOrderedItems;
            } else if (parseInt(sArr[5], 10) === 1) {
                let span4 = startSpan('badge bg-orange', '', '') + sArr[4] + endSpan();
                sArr2[3] = `${span4}`;
            } else if (parseInt(sArr[5], 10) === 2) {
                let span4 = startSpan('badge bg-purple', '', '') + sArr[4] + endSpan();
                sArr2[3] = `${span4}`;
            } else if (parseInt(sArr[5], 10) === 3) {
                let span4 = startSpan('badge bg-olive', '', '') + sArr[4] + endSpan();
                sArr2[3] = `${span4}`;
            }


            let breaktag = breakTag();
            let endanchor = endAnchor();
            let boldtag = startBold('', '', '');
            let endbold = endBold();

            if (parseInt(billStatus, 10) < 3) {
                let anchor1 = startAnchor('btn btn-app delete-product', '', 'data-order="' + sArr[0] + '" data-productId="' + sArr[1] + '"	 data-productName="' + prodName + '" data-billStatus="' + idStatus + '"');
                let icon3 = startIcon('fa fa-trash-o', '', '') + endIcon();

                column1 += `${breaktag}${anchor1}${icon3}${endanchor}`;
            }
            sArr2[2] = column1;
            sArr2[4] = '';
            sArr2[7] = '';

            if (parseInt(billStatus, 10) < 3) {
                let anchor2 = startAnchor('btn btn-app itemRemark', '', 'data-idorder="' + sArr[0] + '" data-remark="' + remark + '" data-productName="' + prodName + '"');
                let icon4 = startIcon('fa fa-sticky-note', '', '') + endIcon();

                sArr2[3] += `${anchor2}${boldtag}${remark}${endbold}${icon4}${endanchor}`;
            } else {
                let icon4 = startIcon('fa fa-sticky-note', '', '') + endIcon();
                sArr2[3] += `${boldtag}${remark}${endbold}${icon4}`;
            }
            return sArr2;
        },
        cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
            if (parseInt(rowIdx, 10) === data.count - 1 && newOrderedItems === 0) nrOnlyListedItems = 0;
            if ((parseInt(billStatus, 10) < 3) && newOrderedItems > 0 && parseInt(rowIdx, 10) === data.count - 1 && colIdx === 3)
                return `${TDo}${cellData}${TDc}${TableStyle.trOpen}${TDo}${startAnchor('submit-order btn btn-app', '', '')}${startIcon('fa fa-send', '', '')}${endIcon()}${endAnchor()}${TDc}${TableStyle.trClose}`
            else return `${TDo}${cellData}${TDc}`;
        }
    };

    listoHTML.createTable.call(cartObject, 'box3', false);
    return nrOnlyListedItems;
}

function updateNotifications(data) {
    //TODO
    /*
    if (data.count == 0)
        document.getElementById('table-notificationCount').innerHTML = '';
    else
        document.getElementById('table-notificationCount').innerHTML = data.count;
    const ul = document.getElementById('table-notifications');
    ul.innerHTML = '';
    for (let n = 0; n < data.count; n++) {
        const li = document.createElement('li');
        const a = document.createElement('a');
        a.innerHTML = '<a class="del-notification" data-notification="'
            + data.rows[n].s[0] + '"><i class="fa fa-remove bg bg-orange"></i></a>'
            + '  <b><span class="text-orange">' + data.rows[n].s[5] + ' (#'
            + data.rows[n].s[2] + '):' + data.rows[n].s[4] + '</span></b>';
        li.appendChild(a);
        ul.appendChild(li);
    }*/
}

function newOrder(table) {
    apiCall_elisto_waiter_createbill(table, function () {
        startBills();
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
}

const setBillGuestNumber = function (idOfSelectedBill, nameOfSelectedTable) {
    const title = formTitle(`${getTextById(33)}: ${idOfSelectedBill}`);
    const title2 = formTitle(`${getTextById(28)}: ${nameOfSelectedTable}`,
        'nameOfTable_GuestCount');
    const numericInput = formNumericInput(97, 'set_guestCountInputField', 'required',
        '');
    const button = FormButton(105, 'openNewOrderButton', '',
        'open-Order');
    const messageLabel = formLabel('', 'guestNumberValidation');
    // TODO show orderLabel for customer
    //const orderLabel = formLabel( orderCode, 'guestOrderCode');
    //const orderCode=makeid(5);
    //const form = [messageLabel, title, title2, orderLabel, numericInput, button];
    const form = [messageLabel, title, title2, numericInput, button];
    listoHTML.buildForm('#main-content', 4, form, 'set-BillGuestNumber');
    setFocus('set_guestCountInputField');
};

const openBill = function (idBill, idtable, nameOfTableToOpen, nGuests,
                           billStatus) {
    box1QuantityIndices = [];
    box1Quantities = [];
    if (billStatus < 1) billStatus = 1;
    document.getElementById('infoLabel').innerText = '';
    document.getElementById('reOpenOrderButton').style.visibility = 'visible';
    document.getElementById('printOrderButton').style.visibility = 'visible';
    document.getElementById('checkOutButton').style.visibility = 'visible';
    document.getElementById('archiveButton').style.visibility = 'hidden';
    document.getElementById('tableName').innerHTML = nameOfTableToOpen;
    if (document.getElementById('nameOfTable_GuestCount')) {
        document.getElementById('nameOfTable_GuestCount').innerHTML = nameOfTableToOpen;
    }
    document.getElementById('billNr').innerHTML = idBill;
    document.getElementById('idTable').innerHTML = idtable;
    document.getElementById('guestCount').innerHTML = nGuests;
    document.getElementById('billStatus_ID').innerText = "" + billStatus;
    document.getElementById('main-content').innerHTML = images.loading;
    if (idBill === 0) {
        document.getElementById('billNr').innerHTML = '??';
    }
    if (parseInt(nGuests, 10) === 0) {
        setBillGuestNumber(idBill, nameOfTableToOpen);
    } else {
        document.getElementById('guestCount').innerHTML = nGuests;

        let box0 = startDiv('box', 'box0', '');
        let box1 = startDiv('box', 'box1', '');
        let box2 = startDiv('box', 'box2', '');
        let box3 = startDiv('box', 'box3', '');
        let topRow = startDiv('row', '', '');
        let bottomRow = startDiv('row', '', '');
        let leftTop = startDiv('col-md-4', '', '');
        let rightTop = startDiv('col-md-8', '', '');
        let enddivtag = endDiv();

        let topRowHtml = `${topRow}${leftTop}${box0}${enddivtag}${enddivtag}${rightTop}${box1}${enddivtag}${enddivtag}${enddivtag}`;

        let bottomRowHtml = `${bottomRow}${box2}${enddivtag}${box3}${enddivtag}${enddivtag}`;

        $('#main-content').html(topRowHtml + bottomRowHtml);
        if (billStatus < 3) {
            showSubCategories(0, 0);
            showProductsOfCategory(0, 0);
        }
        showCart(idBill, idtable);
        showDeliveryStatus(idBill);
    }
};
const createNewOrderForm = function (newOrderIdtable, newOrderTableName) {
    const title = [formTitle, newOrderTableName];
    const button = [FormButton, 30, '', `data-table="${newOrderIdtable}"`,
        'new-Order'];
    const form = [title, button];
    listoHTML.createForm('#main-content', 4, form, 'new-Order');
};

function createTablesOverview(jsonRMLData) {
    document.getElementById('reOpenOrderButton').style.visibility = 'hidden';
    document.getElementById('printOrderButton').style.visibility = 'hidden';
    document.getElementById('checkOutButton').style.visibility = 'hidden';
    document.getElementById('archiveButton').style.visibility = 'hidden';
    document.getElementById('tableName').innerHTML = '';
    document.getElementById('billNr').innerHTML = '';
    document.getElementById('guestCount').innerHTML = '';
    document.getElementById('main-content').innerHTML = '';

    const data = jsonRMLData[3];
    let subData = {
        count: data.count,
        rows: []
    };
    for (let j = 0; j < data.count; j++) {

        let rowArr = data.rows[j];

        let tableSpan, span2, anchorTag, spanClose, anchorClose, span2Html, tableSpanHtml;

        spanClose = endSpan();
        anchorClose = endAnchor();


        anchorTag = startAnchor('new-OrderForm', '', ' data-table="' + rowArr.s[1] + '" data-tableName="' + rowArr.s[0] + '"');
        tableSpan = startSpan('btn  btn-xs bg-light-blue', '', '');
        span2 = startSpan('badge bg-light-green', '', '');

        let firstdataHTML = `${anchorTag}${tableSpan}${rowArr.s[0]}${spanClose}${anchorClose}`;
        let seconddataHTML = `${span2}${rowArr.s[7]} / ${rowArr.s[2]} ${spanClose}`;


        let dataArray = {
            rn: j,
            s: [firstdataHTML, seconddataHTML]
        };
        subData.rows.push(dataArray);
    }

    const txt72 = getTextById(72);
    const txt29 = getTextById(29)
    const tableOverviewTableObject = {
        actions: [],
        dataColumnNames: [],
        dataColumnIndices: [],
        IDENTIFIER: 'waiter-Products-Box1',
        jsonRMLData: subData,
        header: [txt72, txt29],
        columnIndices: [0, 1],
        rowFormat: function (rowIdx, rowArr) {
            return rowArr;
        },
        cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
            return `${TDo}${cellData}${TDc}`;
        }
    };
    listoHTML.createTable.call(tableOverviewTableObject, "main-content", false);
}

jQuery(document).on('click', '.new-OrderForm', function () {
    const idTable = $(this).attr('data-table');
    const nameOfSelectedTable = $(this).attr('data-tableName');
    createNewOrderForm(idTable, nameOfSelectedTable);
});
jQuery(document).on('click', '.new-Order', function () {
    const idtable = $(this).attr('data-table');
    newOrder(idtable);
});
jQuery(document).on('click', '.submit-order', function () {
    const selectedBill = document.getElementById('billNr').innerText;
    submitOrder(selectedBill);
    infoLabel.innerHTML = '';
});
jQuery(document).on('click', '.get-products', function () {
    const cat = $(this).attr('data-category');
    const catParent = $(this).attr('data-idparent');
    showSubCategories(cat, catParent);
    showProductsOfCategory(cat, catParent);
});
jQuery(document).on('click', '.search-products', function () {
    searchProduct();
});
jQuery(document).on('click', '.waiter-table-overview', function () {
    apiCall_elisto_waiter_waiterdash(function (data) {
        createTablesOverview(data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});
jQuery(document).on('click', '.bill-overview', function () {
    apiCall_elisto_waiter_waiterdash(function (data) {
        createBillsOverview(data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});
jQuery(document).on('click', '.openArchive', function () {
    apiCall_elisto_waiter_waiterdash(function (data) {
        createBillsOverview(data, true);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});
jQuery(document).on(
    'click',
    '.add-product',
    function () {
        const prod = $(this).attr('data-product');
        const table = document.getElementById('idTable').innerText;
        const bill = document.getElementById('billNr').innerText;
        const productName = $(this).attr('data-productname');
        infoLabel.innerHTML = productName + '   ' + startIcon('fa fa-check-circle', '', '') + endIcon();
        ;
        addProduct(bill, prod, table);
    });

jQuery(document).on('click', '.delete-product', function () {
    const idOrder = $(this).attr('data-order');
    const idProduct = $(this).attr('data-productId');
    const productName = $(this).attr('data-productName');
    const billStatus = $(this).attr('data-billStatus');
    deleteOrderFromList(idOrder, idProduct, productName, billStatus);
});

jQuery(document).on('click', '.open-Order', function () {
    action = 'add-product';
    let theTable = $(this).attr('data-table');
    let theTableName = $(this).attr('data-tableName');
    let theBill = $(this).attr('data-bill');
    let theBillStatus = $(this).attr('data-billstatus');
    let theGuestCount = $(this).attr('data-guestnumber');
    if (!theTable) theTable = document.getElementById('idTable').innerHTML;
    if (!theTableName) theTableName = document.getElementById('tableName').innerHTML;
    if (!theBill) theBill = document.getElementById('billNr').innerHTML;
    if (!theBillStatus) theBillStatus = 1;
    if (document.getElementById('set-BillGuestNumber')) {
        //if (!theGuestCount) {
        if ($("#set-BillGuestNumber").valid()) {
            theGuestCount = document.getElementById('set_guestCountInputField').value;
            apiCall_elisto_waiter_setguestcount(theGuestCount, theBill, function (
                data) {
                if (data._rc > 0)
                    showHttpErrorMessage(data._rc);
                else openBill(theBill, theTable, theTableName, theGuestCount, theBillStatus);
            }, function (err) {
                showHttpErrorMessage('main-content', err);
            });
        }
    } else openBill(theBill, theTable, theTableName, theGuestCount, theBillStatus);
});
jQuery(document).on(
    'click',
    '.reOpen-Order',
    function () {
        const currentTable = document.getElementById('idTable').innerText;
        const currentTableName = document.getElementById('tableName').innerText;
        const currentBill = document.getElementById('billNr').innerText;
        const currentBillStatus = document.getElementById('billStatus_ID').innerText;
        const currentGuestCount = document.getElementById('guestCount').innerText;
        action = 'add-product';
        openBill(currentBill, currentTable, currentTableName, currentGuestCount,
            currentBillStatus);
    });
jQuery(document).on('click', '.print-Order', function () {
    const printTableName = document.getElementById('tableName').innerText;
    const printBillNumber = document.getElementById('billNr').innerText;
    let d = new Date().toLocaleString();
    printOrderForKitchen('box3', printTableName, printBillNumber, d);
});

jQuery(document).on('click', '.checkOut', function () {
    doCheckOut();
});

function beforeCheckOut(bill) {
    apiCall_elisto_waiter_setbillstatus(2, bill, function (data) {
        if (data._rc > 0) showHttpErrorMessage(data._rc);
        else {
            document.getElementById('billNr').innerText = bill;
            document.getElementById('billStatus_ID').innerText = "2";
        }
    }, function (err) {
        showHttpErrorMessage(err.status);
    });
    const checkoutFocus = formTextInput(601, 'checkoutFocus', '', '');
    const warnLabel = formLabel(getTextById(275), '');
    const reOpenButton = FormButton(105, 'before_checkout_Button_id', '', 'reOpen-Order');
    const formArray = [warnLabel, checkoutFocus, reOpenButton];
    listoHTML.buildForm("#main-content", 6, formArray, 'before_checkOut_form');
    setFocus('checkoutFocus');
    debug("beforeCheckOut", 0);
    $("#checkoutFocus").fadeOut(25);
};

function doCheckOut() {
    const theBill = document.getElementById('billNr').innerText;
    const tmpFocusInput = formTextInput(601, 'tmpFocusID', '', '');
    const status = parseInt(document.getElementById('billStatus_ID').innerText, 10);
    document.getElementById('box0').innerHTML = '';
    document.getElementById('box1').innerHTML = '';
    document.getElementById('box3').innerHTML = '';
    let checkOutButton = [], reOpenButton = [], formArray = [], icon = [];
    debug("doCheckout", '696d31e2', theBill, status);
    if (status === 1) {
        icon = formIcon('', 'fa fa-money', 'billStatus_icon_id');
        checkOutButton = FormButton(603, 'checkOutFormButton_ID',
            `data-bill="${theBill}" data-billStatus="2"`, 'process-Order');
        formArray = [icon, tmpFocusInput, checkOutButton];
    } else if (status === 2) {
        const stripeID = getStripeAccountID(function (stripeAccount) {
            console.log("Is this the Stripe account-id? ==>" + stripeAccount);
        });
        icon = formIcon('', 'fa fa-unlock', 'billStatus_icon_id');
        checkOutButton = FormButton(604, 'checkOutFormButton_ID', `data-bill="${theBill}"
        data-billStatus="3" data-stripeID="${stripeID}" href="javascript:void(0);"`, `checkout-stripeconnectCustomer`);
        reOpenButton = FormButton(605, '', `data-bill="${theBill}" data-billStatus="2"`,
            'continue-Order');
        formArray = [icon, tmpFocusInput, checkOutButton, reOpenButton];
    } else if (status === 3) {
        icon = formIcon('', 'fa fa-lock', 'billStatus_icon_id');
        checkOutButton = FormButton(606, 'checkOutFormButton_ID', `data-bill="${theBill}"
        data-billStatus="4"`, 'archive-Order');
        reOpenButton = FormButton(605, '', `data-bill="${theBill}" data-billStatus="2"`,
            'continue-Order');
        formArray = [icon, tmpFocusInput, checkOutButton, reOpenButton];
    }
    listoHTML.buildForm('#box0', 6, formArray, 'checkout_form');
    //setFocus('tmpFocusID');
    $("#tmpFocusID").fadeOut(25);
};

jQuery(document).on('click', '.process-Order', function () {
    const idBill = $(this).attr('data-bill');
    const idBillStatus = $(this).attr('data-billStatus');
    const nr_listed_orders = document.getElementById('nr_listed_orders').innerText;
    if (parseInt(nr_listed_orders, 10) > 0) {
        beforeCheckOut(idBill);
    } else {
        apiCall_elisto_waiter_setbillstatus(idBillStatus, idBill, function (data) {
            if (data._rc > 0) showHttpErrorMessage("box0", data);
            else {
                document.getElementById('billNr').innerText = idBill;
                document.getElementById('billStatus_ID').innerText = idBillStatus;
                doCheckOut();
            }
        }, function (err) {
            showHttpErrorMessage("box0", err);
        });
    }
});

jQuery(document).on('click', '.checkout-stripeconnectCustomer', function (data) {
    console.log("Trying to check out customer...");
    const productID = 1;
    const productName = getTextById(33) + $(this).attr('data-bill');
    const receiptEmail = sessionStorage.getItem("userEmail");
    const productDescription = getTextById(745) + " " + getTextById(732) + receiptEmail;
    const currency = "EUR";
    const qty = 1;
    const price = sessionStorage.getItem("currentCustomerBillAmount");
    const stripe_account = $(this).attr('data-stripeID');
    console.log("Checkout clicked:\nStripe-account:" + stripe_account + "\n productID:" + productID + ",\n price:"
        + price + ",\n" + currency
        + ",\n product: " + productName + ",\nDescription: " +
        productDescription + ",\nqty: " + qty);
    console.log("receiptEmail=" + receiptEmail);
    stripeConnectPayment(stripe_account, 1, productID, price, productName, productDescription, currency, qty,
        receiptEmail, "card", function (data) {
            console.log("Data returned from stripe: " + JSON.stringify(data));
            const stripe_session_id = data.id;
            if (!stripe_session_id) {
                console.error("NO STRIPE SESSION ID FOUND: " + JSON.stringify(data));
                showHttpErrorMessage("main-content", data);
            }
            console.log("Returned session id:" + stripe_session_id);
            connectCheckout("" + stripe_session_id, "" + stripe_account).then(function (data) {
                console.log("checkout completed");
                console.log("Data returned from session: " + JSON.stringify(data));
                sendMail(1, "NOREPLY", receiptEmail, productName + "(" + price/100 + currency + ")", productDescription,
                    "dummyKey", function () {
                        console.log("Success-Mail was sent.");
                    }, function (err) {
                        console.error(err);
                    });
            }, function (err) {
                console.error("checkout failed: " + err);
                sendMail(1, "NOREPLY", receiptEmail, productName + "(" + price/100 + currency + ")", productDescription,
                    "dummyKey", function () {
                        console.log("Failure-Mail was sent.");
                    }, function (err) {
                        console.error(err);
                    });
            });
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });

    /* console.log(`------------`);
     const stripe_session_id = 4711;
     connectCheckout(stripe_session_id, stripeAccount).then(function () {
         console.log("checkout successful");
     }, function (err) {
         console.error(`checkout failed:  ${err}`);
     });*/
});


jQuery(document).on('click', '.archive-Order', function () {
    const idBill = $(this).attr('data-bill');
    const idBillStatus = $(this).attr('data-billStatus');
    const nr_listed_orders = document.getElementById('nr_listed_orders').innerText;
    if (parseInt(nr_listed_orders, 10) > 0) {
        beforeCheckOut(idBill);
    } else {
        apiCall_elisto_waiter_setbillstatus(idBillStatus, idBill, function (data) {
            if (data._rc > 0) showHttpErrorMessage(data._rc);
            else apiCall_elisto_waiter_archivebill(idBill, function (data1) {
                if (data1._rc > 0) showHttpErrorMessage(data1._rc);
                else {
                    infoLabel.innerText = getTextById(607);
                    document.getElementById('box0').innerHTML = '';
                    document.getElementById('box1').innerHTML = '';
                    document.getElementById('box2').innerHTML = '';
                    document.getElementById('box3').innerHTML = '';
                    document.getElementById('guestCount').innerHTML = '';
                    document.getElementById('tableName').innerHTML = '';
                    document.getElementById('billNr').innerHTML = '';
                    $('#infoLabelFocus').fadeIn(25);
                    setFocus('infoLabelFocus');
                    $('#infoLabelFocus').fadeOut(25);
                }
            }, function (err0) {
                showHttpErrorMessage(err0.status);
            });
        }, function (err) {
            showHttpErrorMessage(err.status);
        });
    }
});

jQuery(document).on('click', '.continue-Order', function () {
    const idBill = $(this).attr('data-bill');
    const idBillStatus = $(this).attr('data-billStatus');
    apiCall_elisto_waiter_setbillstatus(idBillStatus, idBill, function (data) {
        if (data._rc > 0) showHttpErrorMessage(data._rc);
        else startBills();
    }, function (err) {
        showHttpErrorMessage(err.status);
    })
});

jQuery(document).on('click', '.del-notification', function () {
    const idnote = $(this).attr('data-notification');
    apiCall_elisto_waiter_deletenotification(idnote, function (data) {
        showNotifications();
    }, function (err) {
        showHttpErrorMessage('main-content', err);
    });
});

/* Product Overview Function*/
jQuery(document).on('click', '.product-overview', function () {
    displayCategoryProducts();
});

function displayCategoryProducts() {

    let box0, box1, topRow, leftTop, rightTop, divClose, mainHTML;

    box0 = startDiv('box', 'box0', '');
    box1 = startDiv('box', 'box1', '');
    topRow = startDiv('row', '', '');
    leftTop = startDiv('col-md-4', '', '');
    rightTop = startDiv('col-md-8', '', '');
    bottomRow = startDiv('row', '', '');
    box2 = startDiv('box', 'box2', '');
    divClose = endDiv();


    // Append Elements

    mainHTML = `${topRow}${leftTop}${box0}${divClose}${divClose}${rightTop}${box1}${divClose}${divClose}${divClose}${bottomRow}${box2}${divClose}${divClose}`


    // Append Elements to the Main Content
    $('#main-content').html(mainHTML);

    // Show Cateogries
    showSubCategories(0, 0);

    // Action For Product Item
    action = "view-product";
    // Show Products
    showProductsOfCategory(0, 0);
}

jQuery(document).on('click', '.show-Info', function () {
    document.getElementById('main-content').innerHTML = showInfo();
});

// Click Function Production Item
/*jQuery(document).on('click', '.view-product', function () {
	const prodcutId = $(this).data('product');
	const displayProductName = $(this).data('productname');
	let boxdiv, boxDivHTML, h4Tag;
	apiCall_elisto_public_getproductattributes(prodcutId, function (data) {
		boxDiv = startDiv('box-body table-responsive no-padding','','');
		document.getElementById("box2").innerHTML = "";
		const table = document.createElement("table");
		table.setAttribute("class", "table table-striped");
		const boxHead = document.createElement("thead");
		const boxBody = document.createElement("tbody");
		table.append(boxHead);
		table.append(boxBody);
		const title = document.createElement("h4");
		title.innerHTML = displayProductName;
		const trh = document.createElement("tr");
		const th1 = document.createElement("th");
		th1.innerHTML = "__________________";
		const th2 = document.createElement("th");
		th2.innerHTML = "__________________";
		trh.append(th1);
		trh.append(th2);
		boxHead.append(title);
		boxHead.append(trh);
		let subData = {
				count: data.rows.length,
				rows: []
			};
		for (let i = 0; i < data.rows.length; i++) {
			const tr = document.createElement("tr");
			const td1 = document.createElement("td");
			td1.innerHTML = data.rows[i].s[4];
			const td2 = document.createElement("td");
			td2.innerHTML = data.rows[i].s[5];
			tr.append(td1);
			tr.append(td2);
			boxBody.append(tr);
		}
		boxDiv.append(table);
		document.getElementById("box2").append(boxDiv);
	}, function (error) {
		showHttpErrorMessage('main-content', err);
	});
});*/

let update2_Cart = function (data) {
    let rowCount = 0;
    let TOTAL;
    let sArr2;
    sArr2 = [];
    const cartObject = {
        actions: [],
        dataColumns: [],
        dataColumnNames: [],
        dataColumnIndices: [],
        IDENTIFIER: 'waiter-Box2-table',
        jsonRMLData: data,
        header: ['', '', ''],
        columnIndices: [0, 1, 2, 3, 4],
        rowFormat: function (rIdx, sArr) {
            rowCount++;
            if (sArr[1] !== 'TOTAL') {
                if (rowCount == parseInt(data.count, 10)) {
                    sArr2[0] = cartCol1(sArr);
                    sArr2[1] = cartCol2(sArr);
                    sArr2[2] = cartLastRow(sArr, TOTAL);
                } else {
                    sArr2[0] = cartCol1(sArr);
                    sArr2[1] = cartCol2(sArr);
                    sArr2[2] = cartCol3(sArr);
                }
                if (document.getElementById('box1ProdCount' + sArr[2])) {
                    $("#box1ProdCount" + sArr[2]).html(parseInt(sArr[0]) + " x");
                }
                return sArr2;
            } else if (sArr[1] === 'TOTAL') {
                TOTAL = sArr;
                sessionStorage.setItem("currentCustomerBillAmount", "" + parseInt("" + parseFloat(sArr[4]).toFixed(2) * 100));
            }
        },
        cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
            if (colIdx < 3) {
                return `${TDo}${cellData}${TDc}`;
            } else {
                return ``;
            }
        }
    };
    listoHTML.createTable.call(cartObject, 'box2', false);
};


function update0_Categories(catid, jsonData, catParent) {
    if (document.getElementById('box0'))
        document.getElementById('box0').innerHTML = '';

    let boxContainer = $('#box0');

    let boxBody = startDiv('box-body table-responsive no-padding', '', '');

    let arrayLength = jsonData.count;

    let br = breakTag();

    let searchTag = startAnchor('search-products btn btn-app', '', ' style="margin:0 0 1px 1px;" ');

    let anchorClose = endAnchor();
    let spanClose = endSpan();
    let divClose = endDiv();
    let strongClose = endStrong();

    let searchIcon = startIcon('fa fa-search', '', '') + endIcon();

    let searchTagHTML = `${searchTag}${searchIcon}${anchorClose}`;

    let homeTagHTML, topTagHTML, anchor2HTML, boxHTML, anchor2, homeTag, topTag;
    boxHTML = searchTagHTML;
    // boxBody.appendChild(searchTag);
    console.log('CATID---' + catid);

    if (catid != 0) {
        topTagHTML = '';
        //topTag  = startAnchor('get-products btn btn-app', '', 'style="margin:0 0 1px 1px;" data-category="'+catid+'" ');
        apiCall_elisto_public_parentcategories(catid, function (parentData) {
            let lev = 0;
            if (parentData.count < 2) previousCatid = 0;
            else {
                for (let levIdx = 0; levIdx < parentData.count; levIdx++) {
                    if (parseInt(parentData.rows[levIdx].s[3], 10) > lev) {
                        previousCatid = parentData.rows[levIdx].s[0];
                        lev = parseInt(parentData.rows[levIdx].s[3], 10);

                    }
                }
            }


            console.log('TOP TAG12--' + topTag);
            // topTag.setAttribute('', previousCatid);
        }, function err() {
            previousCatid = 0;
        });


        topTag = startAnchor('get-products btn btn-app', '', 'style="margin:0 0 1px 1px;" data-category="' + catParent + '" data-parent="' + catParent + '" ');
        //console.log('TOP TAG--'+topTag);

        homeTag = startAnchor('get-products btn btn-app previous-button', '', 'style="margin:0 0 1px 1px;" data-category="0" ');


        let topSpan = startSpan('', '', '');
        let topStrong = startStrong('', '', '');

        let topSpanHTML = `${topSpan}${topStrong}${strongClose}${spanClose}`;

        let homeIcon = startIcon('fa fa-sitemap', '', '') + endIcon();
        let Icon2 = startIcon('fa fa-reply', '', '') + endIcon();

        topTagHTML = `${topTag}${topSpanHTML}${Icon2}${anchorClose}`;
        homeTagHTML = `${homeTag}${homeIcon}${anchorClose}`;

        boxHTML += homeTagHTML + topTagHTML + br;

    }
    let newLevel = 1;
    anchor2HTML = '';
    for (let i = 0; i < arrayLength; i++) {

        let currentLevel = parseInt(jsonData.rows[i].s[3], 10);

        if (currentLevel > newLevel) {
            newLevel = parseInt(jsonData.rows[i].s[3], 10);
            const hr = hrTag();
            // boxContainer.append(hr);
        }

        let getDataCatParent = get(jsonData, i, 'idparent');

        let getDataCat = get(jsonData, i, 'idproductcategory');

        if (currentLevel < 2) {
            if (currentLevel === 1) {
                anchor2 = startAnchor('get-products btn btn-app bg-purple', '', ' data-category="' + getDataCat + '" data-idparent="' + getDataCatParent + '" style="margin:0 0 1px 1px;" ');
            } else {
                anchor2 = startAnchor('get-products btn btn-app', '', ' data-category="' + getDataCat + '" data-idparent="' + getDataCatParent + '" style="margin:0 0 1px 1px;" ');
            }

            let span3 = startSpan('', '', '');
            let strong3 = startStrong('', '', '');
            let strongHTML = get(jsonData, i, 'prodcatname');

            anchor2HTML += `${anchor2}${span3}${strong3}${strongHTML}${strongClose}${spanClose}${anchorClose}`;

        } else {

            let anchor2 = startAnchor('get-products btn btn-app previous-button', '', '');
            //anchor2HTML += `${anchor2}${anchorClose}`;

        }
    }

    boxHTML += anchor2HTML;


    if (document.getElementById('box0'))
        $('#box0').html(boxHTML);
    // document.getElementById('box0').appendChild(boxBody);
    //setFocus('box1');
}


$(document).ready(function () {
    $('.product-overview').hide();
    $('.openArchive').hide();

});
