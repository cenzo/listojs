"use strict";
const themeID = 0;
const customerTheme = "minimal";

function guestScroll(boxName) {
    debug2(customerTheme,"custom scrolling not implemented for this theme",release,[boxName,scrollingCustomized]);
}


function customized_ProductDetails(idproduct) {
    debug2(customerTheme,"custom product details visualization not implemented for this theme",release,[idproduct,scrollingCustomized]);
}

//  Elements of box0
function categoryHeader() {
    return "<TABLE>";
}

function categoryBottom() {
    return "</TABLE>";
}

function categoryItem(rowNum, catItemArray) {
    /*
    *   catItemArray:
    * [
    * 0: id of productcategory,
    * 1: description of category,
    * 2: parent-id of category,
    * 3: level in category tree,
    * 4: position in category tree,
    * 5: name of category,
    * 6: image of category
    * ]
    *
    * */
    let categoryList = `<tr><td><a href="javascript:void(0)" class="get-products" data-categoryText="${catItemArray[5]}" data-category="${catItemArray[0]}">${catItemArray[1]}</a></td></tr>`;
    return categoryList;
};

//  Elements of box1
function productsHeader() {
    return "<TABLE>";
}

function productsBottom() {
    return "</TABLE>";
}

function productItem(rowNum, prodItemArray) {
    /*
    * prodItemArray:
    * [
    * 0: idproduct,
    * 1: position in list,
    * 2: product-name,
    * 3: product long description,
    * 4: price,
    * 5: image,
    * 6: id of product category,
    * 7: product-label set by the restaurant (alternatively to idproduct),
    * 8: product short description
    *
    *
    * */
    let productList = "";
    productList += '<p><img src="' + getImagePath() + '/' + prodItemArray[5] + '" style="max-width: 100px;"></p>';
    productList += '<h2>' + prodItemArray[2] + '</h2>';
    productList += '<p>' + Number(prodItemArray[4]).toFixed(2) + getCurrency() + '</p>';
    productList += '<p>' + prodItemArray[3] + '</p>';
    productList += '<p><a href="javascript:void(0);" class="add-product" data-product="' + prodItemArray[0] + '">' + getTextById(733) + '</a></p>';
    return productList;
};

//  Elements of box2
function cartHeader() {
    return "<TABLE>";
}

function cartBottom() {
    return "</TABLE>";
}

function cartItem(rowNum, cartItemArray) {
    /*
    * cartItemArray:
    * [
    * 0: quantity,
    * 1: productname,
    * 2: id of product,
    * 3: price,
    * 4: pricesum (=quantity*price),
    * 5: tablenumber,
    * 6: id of bill
    * ]
    *
    *
    * */
    const row = `<tr><td>${rowNum}</td><td>${cartItemArray[0]}<td>${cartItemArray[1]}</td>
                     <td>${cartItemArray[2]}</td><td>${cartItemArray[3]}</td>
                     <td>${cartItemArray[4]}&nbsp;${getCurrency()}</td></tr>`;
    return row;
};


//  Elements of box3
function deliveryHeader() {
    return "<TABLE>";
}

function deliveryBottom() {
    return "</TABLE>";
}

function deliveryItem(rowNum, orderStatusItemArray) {
    /*
    * orderStatusItemArray:
    * [
    * 0: id of order,
    * 1: id of product,
    * 2: productname,
    * 3: minutes passed since order,
    * 4: delivery status (listed, ordered, in preparation, in delivery),
    * 5: id of delivery status,
    * 6: billnumber,
    * 7: remarks
    * ]
    *
    *
    * */
    const row = `<tr><td>${rowNum}</td><td>${orderStatusItemArray[0]}<td>${orderStatusItemArray[1]}</td>
            <td>${orderStatusItemArray[2]}</td><td>${orderStatusItemArray[3]}&nbsp;<b>mins</b>&nbsp;</td>
            <td>${orderStatusItemArray[4]}</td><td>${orderStatusItemArray[5]}</td><td>${orderStatusItemArray[7]}</td>
            <td><a href="javascript:void(0);" class="delete-product" data-product="" data-order="${orderStatusItemArray[0]}">${getTextById(106)}</a></td></tr>`;
    return row;
};

//////////////////////////////////////////////////////////////////////
const showThemeInfo = function () {
    console.log("\n\tTheme-ID: " + themeID + "\n\tTheme-Name: " + customerTheme);
};


showThemeInfo();
