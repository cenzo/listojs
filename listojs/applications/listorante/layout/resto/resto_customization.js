"use strict";
const themeID = 9;
const customerTheme = "resto";
module = customerTheme;

//  Elements of box0
function categoryHeader() {
    return ` `;
}

function categoryBottom() {
    return ``;
}

let categoryList = ``;

function categoryItem(rowNum, catItemArray, totalRows) {
    /*
    *   catItemArray:
    * [
    * 0: id of productcategory,
    * 1: description of category,
    * 2: parent-id of category,
    * 3: level in category tree,
    * 4: position in category tree,
    * 5: name of category,
    * 6: image of category
    * ]
    *
    * */
    const breakAt = 3;
    categoryList += `<div class="col-lg-4 menu-wrap"><div class="menus d-flex align-items-center">
        <div class="menu-img rounded-circle">
        <img class="img-fluid" src="${getImagePath()}/${catItemArray[6]}" alt="listorante-category-image">
        </div>
        <div class="text-wrap">
        <div class="row align-items-start">
        <div class="col-8"><a href="javascript:void(0)" class="get-products"
                                  data-categoryText="${catItemArray[5]}" data-category="${catItemArray[0]}">
        <h2>${catItemArray[5]} </h2> </a>   </div>
    <div class="col-4">
        </div>
        </div>
        <p>${catItemArray[1]}</p>
    </div>
    </div> </div>`;
    let n = parseInt("" + rowNum, 10) + 1;
    debug2(customerTheme, "check n iter", RELEASE, [n, rowNum, totalRows, catItemArray]);
    if (n % breakAt === 0 || n === parseInt(totalRows, 10)) {
        const list = categoryList;
        categoryList = ``;
        return `<div class="row">${categoryHeader()}${list}</div></div>`;
    } else {
        return ``;
    }
}

//  Elements of box1
function productsHeader() {
    return ``;
}

function productsBottom() {
    return ``;
}

let productList = ``;

function productItem(rowNum, prodItemArray, totalRows) {
    /*
    * prodItemArray:
    * [
    * 0: idproduct,
    * 1: position in list,
    * 2: product-name,
    * 3: product long description,
    * 4: price,
    * 5: image,
    * 6: id of product category,
    * 7: product-label set by the restaurant (alternatively to idproduct),
    * 8: product short description
    *
    *
    * */
    let breakAt = 3;
    productList += `<div class="col-lg-4 menu-wrap"><div class="menus d-flex align-items-center">
        <div class="menu-img rounded-circle"><a href="javascript:void(0)"   class="add-product"
                                           data-product="${prodItemArray[0]}">
        <img class="img-fluid" src="${getImagePath()}/${prodItemArray[5]}" alt="listorante-product-image"></a>
        </div>
        <div class="text-wrap">
        <div class="row align-items-start">
        <div class="col-8">
        <h3>${prodItemArray[2]} </h3></div>
    <div class="col-4">
        </div>
        </div><a href="javascript:void(0)"   class="add-product"
                                           data-product="${prodItemArray[0]}">
        <strong><h6>${Number(prodItemArray[4]).toFixed(2)}&nbsp;${getCurrency()}</h6></strong>
        <p><small>${prodItemArray[8]}</small></p>
        <p>${prodItemArray[3]}</p></a>
    </div>
    </div> </div>`;
    let n = parseInt("" + rowNum, 10) + 1;
    if (n % breakAt === 0 || n === parseInt(totalRows, 10)) {
        const list = productList;
        productList = ``;
        return `<div class="row">${productsHeader()}${list}</div></div>`;
    } else {
        return ``;
    }
};

//  Elements of box2
function cartHeader() {
    return `<table>`;
}

function cartBottom() {
    return `</table>`;
}

function cartItem(rowNum, cartItemArray) {
    /*
    * cartItemArray:
    * [
    * 0: quantity,
    * 1: productname,
    * 2: id of product,
    * 3: price,
    * 4: pricesum (=quantity*price),
    * 5: tablenumber,
    * 6: id of bill
    * ]
    *
    *
    * */
    if (cartItemArray[1] == "TOTAL") return `<tr><td><div class="fh5co-item animate-box fadeInUp animated-fast">
                                               <div class="fh5co-item">
                                                   <h3>&nbsp;${cartItemArray[1]}
                                                   <b class="fh5co-price">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                   <u>${cartItemArray[4]}&nbsp;${getCurrency()}</u></b>
                                                   </h3>
                                               </div>
                                           </div></td></tr>`;
    else return ` <tr class="fh5co-item animate-box fadeInUp animated-fast">
                  <td style="color: #fff width: 25px;"> <small>(id:${cartItemArray[2]})</small></td><td style="width: 300px;"><h3>&nbsp;${cartItemArray[1]}</h3></td>
                  </tr><tr class="fh5co-item animate-box fadeInDown animated-fast"">
                   <td style="width: 100px;"><span  class="fh5co-price">${cartItemArray[3]}${getCurrency()}</span></td><td>&nbsp;(x${cartItemArray[0]})</td>
                           <td style="width: 125px;"><span  class="fh5co-price">&nbsp;${cartItemArray[4]}&nbsp;${getCurrency()}</span></td>
                  </tr> `;
};


//  Elements of box3
function deliveryHeader() {
    return "<TABLE>";
}

function deliveryBottom() {
    return "</TABLE>";
}

function orderStatusItem(rowNum, orderStatusItemArray) {
    /*
    * orderStatusItemArray:
    * [
    * 0: id of order,
    * 1: id of product,
    * 2: productname,
    * 3: minutes passed since order,
    * 4: delivery status (listed, ordered, in preparation, in delivery),
    * 5: id of delivery status,
    * 6: billnumber,
    * 7: remarks
    * ]
    *
    *
    * */
    const row = ` <tr style="color: #fff;" class="fh5co-item animate-box fadeInLeft animated-fast">
                  <td> <small>(id:${orderStatusItemArray[1]})</small></td><td ><h3 style="color: #fff;">&nbsp;${orderStatusItemArray[2]}</h3></td>
                   <td style="color: #fff;"><span  class="fh5co-price">${orderStatusItemArray[3]} minutes</span></td><td>&nbsp;(${orderStatusItemArray[4]})</td>
                           <td><span  class="fh5co-price">&nbsp;${orderStatusItemArray[7]} </span></td>
                  </tr> `;
    return row;
};

//////////////////////////////////////////////////////////////////////
const showThemeInfo = function () {
    console.log("\n\tTheme-ID: " + themeID + "\n\tTheme-Name: " + customerTheme);
};

debug("name of waiter:", RELEASE, waiterName);
showThemeInfo();
