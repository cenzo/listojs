/**
 *
 */


function setTexts() {
    // TODO write here customized texts
    console.log("START HERE");
}

function guest0_categories(category, cattext, jsonRMLdata) {

    var elem = document.getElementById("box0");
    elem.innerHTML = "";
    //debug("jsonRMLdata, category '" + category + "'", 0, jsonRMLdata);

    if (category != '0') {
        var catDiv = document.createElement("a");
        // catDiv.setAttribute('data-nav-section', 'top');
        catDiv.setAttribute('data-category', '0');
        catDiv.setAttribute('class', 'get-products');
        catDiv.setAttribute('href', 'javascript:void(0)');
        var top = document.createElement("h3");
        top.setAttribute("class", "glyphicon glyphicon-menu-up");
        var top2 = document.createElement("h3");
        top2.setAttribute("class", "glyphicon glyphicon-cutlery");
        var topText = getTextById(734);
        top.innerText = topText;
        var br = document.createElement("br");

        catDiv.appendChild(top2);
        catDiv.appendChild(top);
        catDiv.appendChild(br);

        elem.appendChild(catDiv);
    }
    var firstLevel = 1000;
    for (var k = 0; k < jsonRMLdata.count; k++) {
        // Show only top-level of the categories:
        if (parseInt(jsonRMLdata.rows[k].s[3], 10) > firstLevel) {
            break;
        } else if (k > 0) {
            var bTag = document.createElement("b");
            bTag.innerText = " - ";

            elem.appendChild(bTag);
        }
        catDiv = document.createElement("a");
        catDiv.setAttribute('class', 'get-products');
        catDiv.setAttribute('href', 'javascript:void(0)');
        if (k === 0) {
            firstLevel = parseInt(jsonRMLdata.rows[k].s[3], 10);
        }
        catDiv.setAttribute('data-category', jsonRMLdata.rows[k].s[0]);
        catDiv.innerText = jsonRMLdata.rows[k].s[1];
        elem.appendChild(catDiv);


    }
    if (category != '0') document.getElementById("categoryDesc").innerText = cattext;
    else document.getElementById("categoryDesc").innerText = '';

}


function update2_Cart(jsonData) {


}

function guest1_product(jsonRMLdata) {

    debug("products: ", 0, jsonRMLdata);
    document.getElementById("box1").innerHTML = "";
    var ul = document.createElement("ul");
    var liDummy = document.createElement("li");

    for (var k = 0; k < jsonRMLdata.count; k++) {
        var li = document.createElement("li");

        var fooDesc = document.createElement("div");
        fooDesc.setAttribute("class", "fh5co-food-desc");
        var fig = document.createElement("figure");
        var img = document.createElement("img");
        var imgSrc = getImagePath() + "/" + jsonRMLdata.rows[k].s[5];
        img.setAttribute("class", "img-responsive");
        img.setAttribute("src", imgSrc);
        fig.appendChild(img);
        var div2 = document.createElement("div");
        var h3 = document.createElement("h3");
        h3.innerText = jsonRMLdata.rows[k].s[2];
        var p = document.createElement("p");
        p.innerText = jsonRMLdata.rows[k].s[3];
        div2.appendChild(h3);
        div2.appendChild(p);
        fooDesc.appendChild(fig);
        fooDesc.appendChild(div2);
        var fooPrice = document.createElement("div");
        var iAddProduct = document.createElement("i");
        var br = document.createElement("br");
        iAddProduct.setAttribute("class", "add-product glyphicon glyphicon-plus");
        iAddProduct.setAttribute("data-product", jsonRMLdata.rows[k].s[0]);
        fooPrice.setAttribute("class", "fh5co-food-pricing");
        fooPrice.innerText = jsonRMLdata.rows[k].s[4] + " " + getCurrency();
        fooPrice.appendChild(br);

        var pAddText = document.createElement("p");
        var prodText = getTextById(733);
        pAddText.innerText = prodText;
        pAddText.appendChild(iAddProduct);
        fooPrice.appendChild(pAddText);
        li.appendChild(fooDesc);
        li.appendChild(fooPrice);
        ul.appendChild(li);
    }
    ul.appendChild(liDummy);
    document.getElementById("box1").appendChild(ul);


}

function guest2_cart(jsonRMLdata) {
    // TODO write here the customization-code
    debug("box2:", 0, jsonRMLdata);
    update2_Cart(jsonRMLdata);
}

function guest3_orderstatus(jsonData) {

}

var LanguageSelect = {
    pre: "",
    suf: ""
};


jQuery(document).on('click', '.listojs-Cart', function () {
    //event.preventDefault();

    apiCall_listorante_guest_showcart(idbill, function (data) {
        $('.navbar-nav li').each(function () {
            $(this).find('a').removeClass('active');
        });

        update2_Cart(data);
        $('.listojs-Cart').addClass('active');
    }, function (err) {
        debug("ERROR:", release, err);
    });
});


jQuery(document).on('click', '.listojs-OrderStatus', function (event) {
    event.preventDefault();
    apiCall_listorante_guest_deliverystatus(idbill, function (data) {
        $('.navbar-nav li').each(function () {
            $(this).find('a').removeClass('active');
        });
        guest3_orderstatus(data);
        $('.listojs-OrderStatus').addClass('active');
    }, function (err) {
        debug("ERROR:", release, err);
    });
});