/**
 *
 */

let START = 0;
applicationID = 1;
const themeID=3;
const customerTheme = "foodee3";
function setTexts() {
    setElemText(615);
}


function guest0_categories(category, cattext, jsonRMLdata) {
    var elem = document.getElementById("box0");
    elem.scrollIntoView(false);
    let categoryList = "<p></p>";
    if (category != '0') {
        categoryList += "<a data-category='0' class='get-products' href='javascript:void(0)'>";
        categoryList += "<h3 class='glyphicon glyphicon-menu-up' style='color:#FFF'>" + getTextById(734) + "</h3>";
        categoryList += "<h3 class='glyphicon glyphicon-cutlery' style='color:#FFF'></h3>";
        categoryList += "</a><br>";

    }
    let catDescElEment = document.getElementById("categoryDesc");
    if (catDescElEment && category != '0') catDescElEment.innerText = cattext;
    else if (catDescElEment) catDescElEment.innerText = "";
    document.getElementById("orderButton").value = getTextById(30);


    var firstLevel = 1000;
    for (var k = 0; k < jsonRMLdata.count; k++) {
        // Show only top-level of the categories:
        if (parseInt(jsonRMLdata.rows[k].s[3], 10) > firstLevel) {
            break;
        }

        categoryList += '<br> <div class="fh5co-v-half" >';
        if (START == 0) categoryList += ' <div class="fh5co-h-row-2 to-animate">';
        else categoryList += ' <div class="fh5co-h-row-2">';

        categoryList += '<div class="fh5co-v-col-1 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_2.jpg)">';
        categoryList += '</div>';
        categoryList += '<div class="fh5co-v-col-2 fh5co-text arrow-left">';

        categoryList += '<a href="javascript:void(0)" class="get-products" data-category="' + jsonRMLdata.rows[k].s[0] + '">' + jsonRMLdata.rows[k].s[1] + '</a>';
        //categoryList += '<p><a href="javascript:void(0)" class="get-products" data-category="'+jsonRMLdata.rows[k].s[0]+'">'+jsonRMLdata.rows[k].s[1]+'</a></p>';
        //categoryList += '<p>'+jsonRMLdata.rows[k].s[1]+'</p>';

        categoryList += '</div>';
        categoryList += '</div>';
        categoryList += '</div>';
        if (k === 0) {
            firstLevel = parseInt(jsonRMLdata.rows[k].s[3], 10);
        }

    }
    elem.innerHTML = categoryList;


    START++;

}


function update2_Cart(jsonData) {


}


function guest1_product(jsonRMLdata) {
    prodStyle3(jsonRMLdata);
}

function prodStyle1(jsonRMLdata) {

    document.getElementById("box1").innerHTML = "";

    let productList = "";
    for (var k = 0; k < jsonRMLdata.count; k++) {

        productList += ' <div class="fh5co-v-half" >';
        productList += ' <div class="fh5co-h-row-2">';

        productList += '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_2.jpg)">';
        productList += '</div>';
        productList += '<div class="fh5co-v-col-2 fh5co-text arrow-left">';
        //productList += '<h2>'+jsonRMLdata.rows[k].s[1]+'</h2>';
        //productList += '<span class="pricing">'+jsonRMLdata.rows[k].s[0]+'</span>';
        productList += '<p>' + jsonRMLdata.rows[k].s[2] + '</p>';
        productList += '<p>' + jsonRMLdata.rows[k].s[3] + '</p>';
        //productList += '<p>'+jsonRMLdata.rows[k].s[5]+'</p>';
        productList += '</div>';
        productList += '</div>';
        productList += '</div>';

    }

    document.getElementById("box1").innerHTML = productList;

}

function prodStyle2(jsonRMLdata) {

    document.getElementById("box1").innerHTML = "";

    let productList = "";
    for (var k = 0; k < jsonRMLdata.count; k++) {

        productList += '<div class="fh5co-v-half" >';
        productList += '<div class="fh5co-h-row-2">';
        productList += '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_2.jpg)"></div>';
        productList += '<div class="fh5co-v-col-2 fh5co-text arrow-left">';
        productList += '<h2>' + jsonRMLdata.rows[k].s[4] + '</h2>';
        productList += '<span class="pricing">' + jsonRMLdata.rows[k].s[2] + '</span>';
        productList += '<p>' + jsonRMLdata.rows[k].s[3] + '</p>';
        productList += '</div>';
        productList += '</div>';
        if (k < jsonRMLdata.count - 1) {
            k += 1;
            productList += '<div class="fh5co-h-row-2 fh5co-reversed">';
            productList += '<div class="fh5co-h-row-2">';
            productList += '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_8.jpg)"></div>';
            productList += '<div class="fh5co-v-col-2 fh5co-text arrow-right">';
            productList += '<h2>' + jsonRMLdata.rows[k].s[4] + '</h2>';
            productList += '<span class="pricing">' + jsonRMLdata.rows[k].s[2] + '</span>';
            productList += '<p>' + jsonRMLdata.rows[k].s[3] + '</p>';
            productList += '</div>';
            productList += '</div>';
        }
        productList += '</div>';

    }

    document.getElementById("box1").innerHTML = productList;
}

function prodStyle3(jsonRMLdata) {

    /* same pattern as in prodstyle2*/

    document.getElementById("box1").innerHTML = "";

    let productList = "";

    for (var k = 0; k < jsonRMLdata.count; k++) {


        productList += '<div class="fh5co-v-half" >';
        productList += '<div class="fh5co-h-row-2">';
        productList += '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_8.jpg)">';
        //productList +=  '<div class="fh5co-v-col-2></div>';
        productList += '<img src="' + getImagePath() + '/' + jsonRMLdata.rows[k].s[5] + '" style="max-width: 200px;">';
        productList += '</div>';
        productList += '<div class="fh5co-v-col-2 fh5co-text arrow-left">';
        productList += '<h2>' + jsonRMLdata.rows[k].s[2] + '</h2>';
        productList += '<span class="pricing">' + Number(jsonRMLdata.rows[k].s[4]).toFixed(2) + getCurrency() + '</span>';
        productList += '<p>' + jsonRMLdata.rows[k].s[3] + '</p>';
        productList += '<a class="add-product glyphicon glyphicon-plus" data-product="' + jsonRMLdata.rows[k].s[0] + '">' + getTextById(733) + '</a>';
        productList += '</div>';
        productList += '</div>';
        productList += '</div>';


    }
    document.getElementById("box1").innerHTML = productList;
}

function guest2_cart(jsonRMLdata) {
    // TODO write here the customization-code 
    update2_Cart(jsonRMLdata);
}

function guest3_orderstatus(jsonData) {

}

var LanguageSelect = {
    pre: "",
    suf: ""
};


jQuery(document).on('click', '.createBill', function () {
    apiCall_listorante_guest_createbill(5, function (data) {
        debug("Create new bill for customer", 0, data);
        const newbill = data[4].rows[0].s[1];
        document.getElementById("idbill").innerText = newbill;

        apiCall_listorante_guest_billinfo(newbill, function (data) {
                debug("Info of bill " + newbill + ":", 0, data);
                const w = data.rows[0].s[0];
                waiterName = data.rows[0].s[2];
                guestCount = data.rows[0].s[3];
                tableName = data.rows[0].s[4];
                document.getElementById("idwaiter").innerText = w;
                document.getElementById("idtable").innerText = "5";
            },
            function (err) {
                showHttpErrorMessage("box0", err);
            });

    }, function (err) {
        showHttpErrorMessage("box0", err);
    });
});


jQuery(document).on('click', '.closeOrder', function () {
    logout();
    // TODO ...
});


jQuery(document).on('click', '.listojs-Cart', function () {
    //event.preventDefault();

    apiCall_listorante_guest_showcart(idbill, function (data) {
        $('.navbar-nav li').each(function () {
            $(this).find('a').removeClass('active');
        });

        update2_Cart(data);
        $('.listojs-Cart').addClass('active');
    }, function (err) {
        debug("ERROR:", release, err);
    });
});


jQuery(document).on('click', '.listojs-OrderStatus', function (event) {
    event.preventDefault();
    apiCall_listorante_guest_deliverystatus(idbill, function (data) {
        $('.navbar-nav li').each(function () {
            $(this).find('a').removeClass('active');
        });
        guest3_orderstatus(data);
        $('.listojs-OrderStatus').addClass('active');
    }, function (err) {
        debug("ERROR:", release, err);
    });
});