/**
 *
 */
const customerTheme="foodee6";
let START = 0;
function setTexts() {
	 
	setElemText(615);
}




function guest0_categories(category, cattext, jsonRMLdata) {
				document.getElementById("box0").innerHTML = "";
			document.getElementById("box1").innerHTML = "";
			document.getElementById("box2").innerHTML = "";
			document.getElementById("box3").innerHTML = "";
	var elem = document.getElementById("box0");
		elem.scrollIntoView(false);
    let categoryList = "<p></p>";
	if (category != '0') { 
		categoryList += "<a data-category='0' class='get-products' href='javascript:void(0)'>";
		categoryList += "<h3 class='glyphicon glyphicon-menu-up' style='color:#FFF'>"+getTextById(734)+"</h3>";
		categoryList += "<h3 class='glyphicon glyphicon-cutlery' style='color:#FFF'></h3>";
		categoryList += "</a><br>"; 
	  
    }
	let catDescElEment = document.getElementById("categoryDesc");
    if (catDescElEment && category != '0') catDescElEment.innerText = cattext;
    else if (catDescElEment) catDescElEment.innerText = '';
    // document.getElementById("orderButton").value = getTextById(30);
	debug("box0, category '" + category + "'", 0, elem.innerHTML);

    
    var firstLevel = 1000;
    for (var k = 0; k < jsonRMLdata.count; k++) {
        // Show only top-level of the categories:
        if (parseInt(jsonRMLdata.rows[k].s[3], 10) > firstLevel) {
            break;
        } 
		 
		categoryList +=  '<br> <div class="fh5co-v-half" >'; 
		if (START == 0) categoryList +=  ' <div class="fh5co-h-row-2 to-animate">'; 
		else categoryList +=  ' <div class="fh5co-h-row-2">'; 
		
		categoryList += '<div class="fh5co-v-col-1 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_2.jpg)">';
		categoryList += '</div>';
		categoryList += '<div class="fh5co-v-col-2 fh5co-text arrow-left">';
	 
        categoryList += '<a href="javascript:void(0)" class="get-products" data-category="'+jsonRMLdata.rows[k].s[0]+'">'+jsonRMLdata.rows[k].s[1]+'</a>';
		//categoryList += '<p><a href="javascript:void(0)" class="get-products" data-category="'+jsonRMLdata.rows[k].s[0]+'">'+jsonRMLdata.rows[k].s[1]+'</a></p>';
		//categoryList += '<p>'+jsonRMLdata.rows[k].s[1]+'</p>';
 
		categoryList += '</div>';
		categoryList += '</div>';
		categoryList += '</div>'; 
        if (k === 0) {
            firstLevel = parseInt(jsonRMLdata.rows[k].s[3], 10);
        }

    }
    elem.innerHTML = categoryList;
	
    //debug("jsonRMLdata, category '" + category + "'", 0, jsonRMLdata);

 
	START++;

}


	
function guest1_product(jsonRMLdata) {
	 
	 debug("products: ", 0, jsonRMLdata);
  
	prodStyle3(jsonRMLdata);
}

function prodStyle1(jsonRMLdata){
    
    debug("products: ", 0, jsonRMLdata);
    document.getElementById("box1").innerHTML = "";

    let productList="";		
    for (var k = 0; k < jsonRMLdata.count; k++) {

        productList +=  ' <div class="fh5co-v-half" >'; 
		productList +=  ' <div class="fh5co-h-row-2">'; 
		
		productList += '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_2.jpg)">';
		productList += '</div>';
		productList += '<div class="fh5co-v-col-2 fh5co-text arrow-left">';
		//productList += '<h2>'+jsonRMLdata.rows[k].s[1]+'</h2>';
		//productList += '<span class="pricing">'+jsonRMLdata.rows[k].s[0]+'</span>';
		productList += '<p>'+jsonRMLdata.rows[k].s[2]+'</p>';
		productList += '<p>'+jsonRMLdata.rows[k].s[3]+'</p>';
		//productList += '<p>'+jsonRMLdata.rows[k].s[5]+'</p>';
		productList += '</div>';
		productList += '</div>';
		productList += '</div>';
		
    }	
 	
	console.log("box1:  "+productList);
    document.getElementById("box1").innerHTML = productList;

}

function prodStyle2(jsonRMLdata){
    debug("products: ", 0, jsonRMLdata);
    document.getElementById("box1").innerHTML = "";

    let productList="";		
    for (var k = 0; k < jsonRMLdata.count; k++) {

        productList +=  '<div class="fh5co-v-half" >'; 
		productList +=  '<div class="fh5co-h-row-2">'; 
		productList +=  '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_2.jpg)"></div>';
		productList +=  '<div class="fh5co-v-col-2 fh5co-text arrow-left">';
		productList += '<h2>'+jsonRMLdata.rows[k].s[4]+'</h2>';
		productList += '<span class="pricing">'+jsonRMLdata.rows[k].s[2]+'</span>';
		productList += '<p>'+jsonRMLdata.rows[k].s[3]+'</p>';
		productList += '</div>';
		productList += '</div>';
		if (k < jsonRMLdata.count-1){
			k += 1;		
			productList +=  '<div class="fh5co-h-row-2 fh5co-reversed">'; 
			productList +=  '<div class="fh5co-h-row-2">'; 
			productList +=  '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_8.jpg)"></div>';
			productList +=  '<div class="fh5co-v-col-2 fh5co-text arrow-right">';
			productList += '<h2>'+jsonRMLdata.rows[k].s[4]+'</h2>';
			productList += '<span class="pricing">'+jsonRMLdata.rows[k].s[2]+'</span>';
			productList += '<p>'+jsonRMLdata.rows[k].s[3]+'</p>';
			productList += '</div>';
			productList += '</div>';
		}
		productList += '</div>';
	 		
    }
	console.log("box1:  "+productList);
    document.getElementById("box1").innerHTML = productList;
}
function prodStyle3(jsonRMLdata){
	
	/* same pattern as in prodstyle2*/
	  debug("products: ", 0, jsonRMLdata);
    document.getElementById("box1").innerHTML = "";

    let productList="";		
 
    for (var k = 0; k < jsonRMLdata.count; k++) {
		 

        productList +=  '<div class="fh5co-v-half" >'; 
		productList +=  '<div class="fh5co-h-row-2">'; 
		productList +=  '<div class="fh5co-v-col-2 fh5co-bg-img" style="background-image: url(http://www.listorante.com/thirdparty/foodee/images/res_img_8.jpg)">';
		//productList +=  '<div class="fh5co-v-col-2></div>';
		productList +=  '<img src="'+getImagePath()+'/'+jsonRMLdata.rows[k].s[5]+'" style="max-width: 200px;">';
		productList +=  '</div>';
		 productList +=  '<div class="fh5co-v-col-2 fh5co-text arrow-left">'; 
		productList += '<h2>'+jsonRMLdata.rows[k].s[2]+'</h2>';
		productList += '<span class="pricing">'+Number(jsonRMLdata.rows[k].s[4]).toFixed(2)+getCurrency()+'</span>';
		productList += '<p>'+jsonRMLdata.rows[k].s[3]+'</p>';
		productList += '<a class="add-product glyphicon glyphicon-plus" data-product="'+jsonRMLdata.rows[k].s[0]+'">'+getTextById(733)+'</a>';
		productList += '</div>';
		productList += '</div>';
		productList += '</div>';
		
		  
	 		
    }
	console.log("box1:  "+productList);
    document.getElementById("box1").innerHTML = productList;
}





jQuery(document).on('click', '.showGuestcart', function () {
    var idbill = parseInt(document.getElementById('idbill').innerText);
    showCart(idbill);
});


jQuery(document).on('click', '.deliveryStatus', function () {
    var idbill = parseInt(document.getElementById('idbill').innerText);
            showDeliveryStatus(idbill);
});


jQuery(document).on('click', '.showcategories', function () {
    showSubCategories(0,'');
});


jQuery(document).on('click', '.orderButton', function (event) {
    const userName = $('#username').val();
    const restaurantID = parseInt(getUrlVars()['cust'], 10);

    apiCall_listorante_public_settings('GUESTPASS', function (data){
        const passwordString = data.rows[0].s[0];
        const pass = userName+passwordString;
        login(userName, pass, 'foodee6');


    }, function (err) {
        debug("ERROR:", release, err);
    });

});


function guest2_cart(jsonData)
	{
		// TODO write here the customization-code
		debug("box2:", release, jsonData);
		 var prodArray= [];
		//apiCall_listorante_guest_deliverystatus(idbill, function(data){
			var d = jsonData;
			var prodCount = d.count;
			for(var j=0; j<prodCount; j++){
				prodArray.push(d.rows[j].s[4]);
			}
			//console.log(JSON.stringify(prodArray));
			document.getElementById("box0").innerHTML = "";
			document.getElementById("box1").innerHTML = "";
			document.getElementById("box2").innerHTML = "";
			document.getElementById("box3").innerHTML = "";
			var boxDiv = document.createElement("div");
			boxDiv.setAttribute("class", "box-body");
			var h1 = document.createElement('h1');
			h1.innerHTML = "My Order"
			boxDiv.appendChild(h1);
			var table = document.createElement("table");
			table.setAttribute("class", "table");
			var boxBody = document.createElement("tbody");
			var boxHead = document.createElement("thead");
			var trh = document.createElement("tr");
			var th1 = document.createElement("th");
			var th2 = document.createElement("th");
		//	var th3 = document.createElement("th");
			var th4 = document.createElement("th");
			var th5 = document.createElement("th");
			var StatusValue = "";
			th1.innerHTML = "Quantity";
			th2.innerHTML = "Product Name";
			//th3.innerHTML = "Status";
			th4.innerHTML = "Unit Price";
			th5.innerHTML = "Price";
			trh.appendChild(th1);
			trh.appendChild(th2);
		//	trh.appendChild(th3);
			trh.appendChild(th4);
			trh.appendChild(th5);
			boxHead.appendChild(trh);
			var arrayLength = jsonData.count;
			for (var i = 0; i < arrayLength; i++) {
				var qty = jsonData.rows[i].s[0];
				var prodname = jsonData.rows[i].s[1];
				var idproduct = jsonData.rows[i].s[2];
				var price = jsonData.rows[i].s[3];
				var pricesum = jsonData.rows[i].s[4];
				var id = document.createElement("span");
				// var con = parseInt(i) + 1; 
				if (i < prodCount){
					var StatusValue = prodArray[i];
				}
				
				id.setAttribute("class", "label label-success bg-purple");
				id.innerHTML = "id: " + idproduct;

				var tr = document.createElement("tr");
				var td1 = document.createElement("td");
				var td2 = document.createElement("td");
				var prod = document.createElement("span");
				//var td3 = document.createElement("td");
				var td4 = document.createElement("td");
				var td5 = document.createElement("td");
				prod.setAttribute("class", "badge bg bg-navy");
				prod.innerHTML = prodname;
				td1.innerHTML = qty;
				//td3.innerHTML = StatusValue;
				td2.appendChild(prod);
				td4.innerHTML = price + "&nbsp;" + getCurrency();
				td5.innerHTML = "<b>" + pricesum + getCurrency() + "</b>";
				if (qty > 1) {

					var qtyLabel = document.createElement("span");
					qtyLabel.setAttribute("class", "label label-success bg-purple");
					qtyLabel.innerHTML = "<b> x " + qty + "</b>";

					td2.appendChild(qtyLabel);
				}
				tr.appendChild(td1);
				tr.appendChild(td2);
			//	tr.appendChild(td3);
				tr.appendChild(td4);
				tr.appendChild(td5);
				if (idproduct > 0)
					boxBody.appendChild(tr);
				else {
					var total = document.createElement("tr");
					total.innerHTML = "<tr><td></td><td></td><td>Total </td><td>" + "<ins><b>"
							+ pricesum + getCurrency() + "</b></ins>"
							+ "</td></tr>"
				}
			}
			if (total)
				boxBody.appendChild(total);
			table.appendChild(boxHead);
			table.appendChild(boxBody);
			boxDiv.appendChild(table);

			document.getElementById("box2").appendChild(boxDiv);
		//});

		
	}
 
function guest3_orderstatus(jsonData){
			var prodArray= [];
		
			console.log(JSON.stringify(jsonData));
						document.getElementById("box0").innerHTML = "";
			document.getElementById("box1").innerHTML = "";
			document.getElementById("box2").innerHTML = "";
			document.getElementById("box3").innerHTML = "";
			var boxDiv = document.createElement("div");
			boxDiv.setAttribute("class", "");
			var h1 = document.createElement('h1');
			h1.innerHTML = "Items Delivery Status"
			boxDiv.appendChild(h1);
			var table = document.createElement("table");
			table.setAttribute("class", "table");
			var boxBody = document.createElement("tbody");
			var boxHead = document.createElement("thead");
			var trh = document.createElement("tr");
			var th1 = document.createElement("th");
			var th2 = document.createElement("th");
			var th3 = document.createElement("th");
			var th4 = document.createElement("th");
			var th5 = document.createElement("th");
			th1.innerHTML = "Item Name";
			th2.innerHTML = "Item Ordered";
			th3.innerHTML = "Item Status ";
			th4.innerHTML = "Bill Number";
			th5.innerHTML = "Remarks";
			trh.appendChild(th1);
			trh.appendChild(th2);
			trh.appendChild(th3);
			trh.appendChild(th4);
			trh.appendChild(th5);
			boxHead.appendChild(trh);
			var arrayLength = jsonData.count;
			for (var i = 0; i < arrayLength; i++) {
				var prodname = jsonData.rows[i].s[2];
				var min = jsonData.rows[i].s[3];
				var status = jsonData.rows[i].s[4];
				var billnumber = jsonData.rows[i].s[6];
				var Remarks = jsonData.rows[i].s[7];

				var tr = document.createElement("tr");
				var td1 = document.createElement("td");
				var td2 = document.createElement("td");
				var td3 = document.createElement("td");
				var td4 = document.createElement("td");
				var td5 = document.createElement("td");
				td1.innerHTML = prodname;
				td2.innerHTML = min+' mins ago';
				td3.innerHTML = status;
				td4.innerHTML = billnumber
				td5.innerHTML = Remarks;
				tr.appendChild(td1);
				tr.appendChild(td2);
				tr.appendChild(td3);
				tr.appendChild(td4);
				tr.appendChild(td5);
				boxBody.appendChild(tr);
			}

			table.appendChild(boxHead);
			table.appendChild(boxBody);

			boxDiv.appendChild(table);

			document.getElementById("box3").appendChild(boxDiv);
	}
