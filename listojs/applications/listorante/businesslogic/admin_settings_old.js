release = "d1a6f_4";
module = "admin_settings.js";
let layOutData;
let currentView;
let selectedLayoutId;

function checkCustomError(data) {
    if (data && data._rc && parseInt(data._rc, 10) > 0) {
        showHttpErrorMessage("main-content", data._rc);
    } else {
        debug("No return code found in data: ", release, data);
    }
}

function showLayoutTextAdmin() {
    initDataTableForLayoutElemsAdmin(apiCall_listorante_admin_layouttexts, getLayoutId(), "texts");
}

function initDataTableForLayoutElemsAdmin(_DataFunction, layoutId, layoutType) {
    currentView = layoutType;
    _DataFunction(layoutId, function (successData) {
        showListForLayoutAdmin(layoutType, successData);
    }, function (errorData) {
        showHttpErrorMessage("main-content", errorData);
    });
}

/*
* // TODO insert to new function following approach for adding customer-accounts
			//const newPassValue=makeid(6);
			//const newPass = formTextInput( 250,
			//  "table_edit_newPass",
			//"rows=1 required ",
			//newPassValue);
* */

function showListForLayoutAdmin(type, data) {

    let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
            const tableDiv = formRowDiw("tableDivID", 12);
            layOutData = successData;
            let layouts = {};
            for (let opt = 0; opt < successData.count; opt++) {
                layouts[opt] = successData.rows[opt].s[6];
                if (parseInt(successData.rows[opt].s[9], 10) === 1) {
                    //Set selected theme
                    selectedTheme = successData.rows[opt].s[6];
                    //debug("selected theme = " + opt + "(" + selectedTheme + ")", release);
                }
            }
            //debug("layouts", release, layouts, successData);
            const listBox = formSelectBoxArray(262, "get-Theme", "listBoxIDAdmin", layouts);
            //TODO add event-listener for click of refresh-Button (point nr 7 of specification)
            const privacyButton = FormButton(737, "privacyButton", "", "refresh-preview");
            const BusinessTerms = FormButton(736, "BusinessTerms", "", "refresh-preview");
            const action1 = ["create-edit" + type + "-formAdmin", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
            //const action2 = ["set-default-" + type + "Admin", actionDeleteStyle.elements[0] + getTextById(216) + actionDeleteStyle.elements[1]];
            const dataColumnNames1 = ["id", "layoutid", "elementlabel", "value", "cssrootelement", "name", "typeid", "alt_text", "defaultvalue"];
            const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8];
            //const dataColumnNames2 = ["id"];
            //const dataColumnIndices2 = [0];
            const admin = {
                IDENTIFIER: "admin-edit-layout-" + type,
                jsonRMLData: data,
                //header: [getTextById(201), getTextById(205), getTextById(202), getTextById(203),'',''],
                header: [getTextById(205), getTextById(202), ""],
                //columnIndices: [2, 3, 4, 8],
                columnIndices: [3, 2],
                //actions: [action1, action2],
                actions: [action1],
                //dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
                //dataColumnNames: [dataColumnNames1, dataColumnNames2],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1],
                rowFormat: function (rowIndex, dataArray) {
                    return dataArray;
                },
                cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                    {
                        if (type == "images" && colIndex === 2) {
                            return TDopen + "<img src='" + getImagePath() + "/" + data
                                + "' " + ThumbStyle.default + ">" + TDclose;
                        } else {
                            return TDopen + data + TDclose;
                        }
                    }
                }
            };
            const theForm = [listBox, tableDiv, privacyButton, BusinessTerms];
            listoHTML.buildForm("#main-content", 12, theForm, "layoutEdit");
            // listoHTML.createForm("#main-content", 12, theForm, "layoutEdit");
            listoHTML.createTable.call(admin, "tableDivID");
            //const lpT = layoutPageTemplate("150%", "500px", selectedTheme,"layoutFrame");
            // $("#iFrameDivID").html(lpT);
            setDefaultSelectedLayoutValueAdmin();
        }, function err(data) {
            showHttpErrorMessage(data);
        }
    );
};


function setDefaultSelectedLayoutValueAdmin() {
    if (!layOutData) {
        return;
    }
    const layoutIdArray = [];
    const layoutNameArray = [];
    for (let i = 0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutName = layoutNameArray[layoutIdArray.indexOf(getLayoutId())];
    $("#listBoxIDAdmin").val(_selectedLayoutName);
}


function setSelectedLayoutIdAdmin(_selectedLayoutId) {
    selectedLayoutId = _selectedLayoutId;
}


function initLayoutFunctionAdmin(_type) {
    switch (_type) {
        case "texts" :
            initDataTableForLayoutElemsAdmin(apiCall_listorante_admin_layouttexts, getLayoutId(), "texts");
            break;
        case "colors":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutcolors, getLayoutId(), "colors");
            break;
        case "fonts":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutfonts, getLayoutId(), "fonts");
            break;
        case "structures":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutstructures, getLayoutId(), "structures");
            break;
        case "images":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutimages, getLayoutId(), "images");
            break;
        default:
            break;
    }

}


function generateFormAdmin(that, layoutType) {
    currentView = layoutType;
    //const iFrameDiv = formRowDiw("iFrameDivID", 12);
    //const refreshButton = FormButton(600, "refreshEditButtonId", "", "refresh-edit-preview");
    debug("that data #####", release, $(that).data());
    const backButton = FormButton(734, "backButtonLayout", "", "back-button");
    const elementLabel = $(that).data("elementlabel");
    //const id = $(that).data("id");
    const elementid = $(that).data("id");
    const id = $(that).data("value");

    const cssVariable = $(that).data("cssrootelement");
    //const value = $(that).data("value");
    const value = $(that).data("elementlabel");
    //const defaultValue = $(that).data("defaultvalue");
    const alt_Text = $(that).data("alt_text");
    debug("create edit " + layoutType + " form", release, elementLabel, id);
    //const layoutElementName = formTextInput(201, "layout_" + layoutType + "_element_name", 'rows="1" required disabled ' +
    //    'alt="' + alt_Text + '" title="' + alt_Text + '"',
    //    elementLabel);
    const layoutElementName = formTextInput(201, "layout_" + layoutType + "_element_name", 'rows="1" required disabled ' +
        'alt="' + alt_Text + '" title="' + alt_Text + '"',
        id);
    let layoutValue = null, layoutDefaultValue = null;
    //const imageSrc = $(this).data('layout_images_default_value');

    if (layoutType === "texts") {
        layoutValue = FormTextArea(202, "layout_" + layoutType + "_value", "rows=2 required maxlength='250'", value);
        //layoutDefaultValue = FormTextArea(203, "layout_" + layoutType + "_default_value", "rows=2 disabled required ", defaultValue);
    } else if (layoutType === 'images') {
        layoutValue = formImage(202, "layout_" + layoutType + "_value", "", "", "Choose Image");
        //layoutDefaultValue = formDisplayImage(203, "layout_" + layoutType + "_default_value", "longdesc=\"" + value + "\"", getImagePath() + "/" + value,"Default Image");

    } else {
        layoutValue = formTextInput(202, "layout_" + layoutType + "_value", 'rows="1" required  ' +
            'alt="' + alt_Text + '" title="' + alt_Text + '"',
            value);
        //layoutDefaultValue = formTextInput(203, "layout_" + layoutType + "_default_value", 'rows="1" required disabled ', defaultValue);
    }
    const cssField = formTextInput(205, "layout_" + layoutType + "_cssrootelement", 'rows="1" required disabled',
        cssVariable);

    //const hidden_layout_id = formHiddenInput("", id, "id_of_edited_layout_" + layoutType + "");
    const hidden_layout_id = formHiddenInput("", elementid, "id_of_edited_layout_" + layoutType + "");
    const submitButton = FormButton(215, "update-layout-" + layoutType + "", "data-idtext='" + id + "'", "update-layout-" + layoutType + "");
    //const form1 = [layoutElementName, cssField, layoutValue, layoutDefaultValue, hidden_layout_id, submitButton, backButton];
    const form1 = [layoutElementName, layoutValue, hidden_layout_id, submitButton, backButton];
    listoHTML.buildForm("#main-content", 12, form1, "layout-" + layoutType + "-edit-form");
    // listoHTML.createForm("#main-content", 12, form1, "layout-" + layoutType + "-edit-form");
    /*let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
         //selectedTheme = successData.rows[2].s[7];
         selectedTheme = getLayoutId();
         const lpT = layoutPageTemplate("150%", "500px", selectedTheme, "layoutFrame");
         //$("#iFrameDivID").html(lpT);
         refreshIframeDataBySelectedLayoutData(getLayoutId());
     }, function err(data) {
         showHttpErrorMessage(data._rc);
     });*/
}


$(document).on("click", ".set-Settings", function () {
    showLayoutTextAdmin();
});

$(document).on("click", "#backButtonLayout", function () {
    showLayoutTextAdmin();
});

$(document).on("click", "#cancelBusinessTerms", function () {
    showLayoutTextAdmin();
});


$(document).on("click", "#cancelPrivacy", function () {
    showLayoutTextAdmin();
});


$(document).on("click", "#submitTerms", function () {
    let longText = document.getElementById("businessTerms").value;
    if (longText.length > 16000) {
        longText = longText.substring(0, 16000);
    }
    apiCall_listorante_admin_updatetext(2, longText, function (data) {
        debug("Terms updated", release, data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
        alert("ERROR, status=" + err.status + "(" + getTextById(err.status) + ")");
    });
    showLayoutTextAdmin();
});


$(document).on("click", "#submitPrivacy", function () {
    let longText = document.getElementById("privateTerms").value;
    if (longText.length > 16000) {
        longText = longText.substring(0, 16000);
    }
    apiCall_listorante_admin_updatetext(1, longText, function (data) {
        debug("Privacy updated", release, data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
        alert("ERROR, status=" + err.status + "(" + getTextById(err.status) + ")");
    });

    showLayoutTextAdmin();
});

$(document).on("click", ".create-edittexts-formAdmin", function () {
    generateFormAdmin(this, "texts");
});

$(document).on("click", "#privacyButton", function () {
    let longText = "";
    apiCall_listorante_public_longtext(3, function (data) {
        for (let i = 0; i < data.rows.length; i++) longText += data.rows[i].s[0];
        //debug("privacy longText", release, longText);
        document.getElementById("privateTerms").value = longText;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
    const textAreaField = FormTextArea(737, "privateTerms", "rows=15 ", longText);
    const submitButton = FormButton(215, "submitPrivacy", "", "submit-button");
    const cancelButton = FormButton(602, "cancelPrivacy", "", "cancel-button");

    const theForm = [textAreaField, submitButton, cancelButton];
    listoHTML.buildForm("#main-content", 12, theForm, "privateTermsForm");
});


$(document).on("click", "#BusinessTerms", function () {
    let longText = "";
    apiCall_listorante_public_longtext(2, function (data) {
        for (let i = 0; i < data.rows.length; i++) longText += data.rows[i].s[0];
        debug("Business Terms longText", release, longText);
        document.getElementById("businessTerms").value = longText;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
    const textAreaField = FormTextArea(736, "businessTerms", "rows=15 ", longText);
    const submitButton = FormButton(215, "submitTerms", "", "submit-button");
    const cancelButton = FormButton(602, "cancelBusinessTerms", "", "cancel-button");

    const theForm = [textAreaField, submitButton, cancelButton];
    listoHTML.buildForm("#main-content", 12, theForm, "businessTermsForm");
});

$(document).on("click", ".update-layout-texts", function () {
    debug("ButtonData:", release, $(this).data());
    const textID = $(this).data("idtext");
    const textValue = $.trim($("#layout_texts_value").val());

    apiCall_listorante_admin_updatelayout(textID, getLayoutId(), textValue, function (data) {
        debug("setting value of id " + textID + " to '" + textValue + "'", release, data);
    }, function (error) {
        showHttpErrorMessage("main-content", error);
    });
    showLayoutTextAdmin();
});

$(document).on("change", "#listBoxIDAdmin", function () {
    const selectdLayoutName = $(this).val();
    const layoutIdArray = [];
    const layoutNameArray = [];
    for (let i = 0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutId = layoutIdArray[layoutNameArray.indexOf(selectdLayoutName)];
    layoutChanged = true;
    //refreshIframeDataByFromApiSelectedLayoutDataAdmin(_selectedLayoutId);
    apiCall_listorante_admin_setlayoutid(_selectedLayoutId, function (successData) {
        //debug("set new layout id to '"+_selectedLayoutId+"'",release,successData,layoutIdArray,layoutNameArray,selectdLayoutName);
        sessionStorage.setItem("selectedLayoutId", _selectedLayoutId);
        setSelectedLayoutIdAdmin(_selectedLayoutId);
        initLayoutFunctionAdmin(currentView);
        apiCall_listorante_admin_layouttexts(_selectedLayoutId, function (data) {
            showLayoutTextAdmin();
            checkCustomError(data);
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        })
    }, function err(data) {
        showHttpErrorMessage("main-content", data);
    });
});
