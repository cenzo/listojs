module = "login.js";
applicationID = 1;

release=RELEASE;

/**
 * Set texts
 **/
function setLoginTexts() {
    const passW = getTextById(81);
    const uName = getTextById(82);
    const cID = getTextById(729);

    setElemText(615);
    setElemText(713);
    setElemText(726);
    setElemText(727);
    setElemText(728);
    let roleAssigned = "";
    let user = document.getElementById("user");
    let password = document.getElementById("password");
    let inputCustomerID = document.getElementById("inputCustomerID")
    if (user) {
        user.placeholder = uName;
    }
    if (password) {
        password.placeholder = passW;
    }
    if (inputCustomerID) {
        inputCustomerID.placeholder = cID;
    }
    /*
    if (!isNaN(role)) {
        document.getElementById("selectRole").innerHTML = "";
    }
    */
    if (role === 0) {
        roleAssigned = getTextById(100); //Manager
    } else if (role === 1) {
        roleAssigned = getTextById(70);//Waiter
    } else if (role === 2) {
        roleAssigned = getTextById(3);//Cook
    } else if (role === 3) {
        roleAssigned = getTextById(102); // client
    } else {
        roleAssigned = getTextById(715); //select - message
    }
    let roleElement = document.getElementById("role");
    if (roleElement) {
        roleElement.innerText = roleAssigned;
    }
    if (customerID) {
        let selectCustomer = document.getElementById("selectCustomer");
        if (selectCustomer) {
            selectCustomer.innerHTML = "";
        }
        let text726 = document.getElementById("text726");
        if (text726) {
            text726.innerHTML = "";
        }
        let text727 = document.getElementById("text727");
        if (text727) {
            text727.innerHTML = "";
        }
        let text728 = document.getElementById("text728");
        if (text728) {
            text728.innerHTML = "";
        }
    }
}

//setTexts();


// Using local storage is bad practice: the values are never deleted from browser!
// Better clear all values. "So what" for other applications which should use the local storage
localStorage.clear();

/*function old_login(user, pass, customerTheme) {
    console.log("LOGIN START");
    if (customerID && customerID !== 9 && selectedServer) {
        doLogin(selectedServer + customerID + "/1/login", user, pass, customerTheme);
    } else {
        if (!customerID || customerID === 9) {
            console.error("Invalid customerID: " + customerID);
        }
        if (!selectedServer) {
            console.error("No server was selected, yet");
        }
    }
    console.log("LOGIN END");
}*/

function userLogout() {
    /*
     * loginState:
     * 0/NaN : user is not logged in
     * 1 : user is logged in
     * 2 : there are items in the list
   * */
    debug2("shop", "loginState in userLogout()", release, [loginState]);
    sessionStorage.removeItem("theCustomerID");
    const logoutPath = getAPIServerPath() + customerID + "/1/logout";
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    const authCookie=getCookie(bearerCookieName);
    xhttp.onreadystatechange = function () {
        debug2("shop", "logoutPath,xhttp,loginState,authCookie", release, [logoutPath, xhttp, loginState, bearerCookieName, authCookie]);
        if (xhttp.readyState == 4) {
            if (xhttp.status >= 400) {
                debug2("shop", "ERROR: logout-status ", "" + release, [xhttp, loginState]);
            } else {
                setLoginState(0);
                const loginButton = document.getElementById("loginButton");
                loginButton.innerText = getTextById(615);
                loginButton.setAttribute("class", "btn btn-singin login");
                document.getElementById("username").hidden = false;
                document.getElementById("userpass").hidden = false;
            }
        }
    };
    xhttp.send("Authorization=Bearer " + authCookie + "&token=" + authCookie);
}

function login(user, pass, customerTheme) {
    console.log("LOGIN START");
    if (!customerID || customerID === 9) {
        console.error("Invalid customerID for login: " + customerID);
    } else if (!selectedServer) {
        console.error("No server was selected, yet");
    } else {
        const xhttp = new XMLHttpRequest();
        const path = `${selectedServer}server/${customerID}/1`;
        xhttp.open("GET", path, true);
        xhttp.onreadystatechange = function () {
            console.log("path to select customer server=" + path);
            if (xhttp.readyState === 4) {
                if (xhttp.status === 200) {
                    const customerServerData = JSON.parse(xhttp.responseText);
                    selectedServer = `https://backend${customerServerData.nodeid}.listorante.com${backendVersion}`;
                    console.log(":::: Selected customer server: " + selectedServer);
                    sessionStorage.setItem("selectedServer", selectedServer);
                    doLogin(selectedServer + customerID + "/1/login", user, pass, customerTheme);
                }
            }
        };
        xhttp.send();
    }
    console.log("LOGIN END");
}

function doLogin(loginPath, user, pass, customerTheme) {
    debug("customerID is: " + customerID, "xxxxxxxxxxx");

    //const loginPath = getAPIServerPath() + customerID + "/1/login";
    bearerCookieName = "listoranteToken" + customerID + user;
    showLoginFailure("");

    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", loginPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + user + "&password=" + pass);
    xhttp.onreadystatechange = function () {
        console.log("loginPath=" + loginPath);
        let statusString = "Start";

        statusString += "_" + xhttp.readyState + ": " + xhttp.status + " (" + xhttp.statusText + " " + xhttp.getAllResponseHeaders() + ")";
        //document.getElementById("statusIndicator").innerHTML = statusString;

        if (xhttp.readyState === 4) {
            if (xhttp.status === 200) {
                setCookie(bearerCookieName, xhttp.responseText, 1);
                bearerCookie=getCookie(bearerCookieName);
                if (role === 0) {
                    window.open("admin_adminLTE_customization.html?cust=" + customerID, "_self");
                } else if (role === 1) {
                    window.open("wdash_adminLTE_customization.html?cust=" + customerID, "_self");
                } else if (role === 2) {
                    window.open("kitchen_adminLTE_customization.html?cust=" + customerID, "_self");
                } else if (customerTheme) {
                    const loginButton = document.getElementById("loginButton");
                    loginButton.innerText = getTextById(26);
                    loginButton.setAttribute("class", "btn btn-singin userLogout");
                    document.getElementById("username").hidden = true;
                    document.getElementById("userpass").hidden = true;
                    setLoginState("1");
                    //window.open("/applications/shop/layout/" + customerTheme + "/?cust=" + customerID, "_self");
                } else {
                    showLoginFailure(getTextById(JSON.stringify(xhttp.responseText)));
                }
            } else if (xhttp.status >= 400) {
                if (xhttp.status == 403 || xhttp.status == 401) {
                    showLoginFailure(getTextById(738));// message will be "Login failed: wrong password?"
                } else if (xhttp.status == 404) {
                    showLoginFailure(getTextById(404));
                } else if (xhttp.status > 404) {
                    showLoginFailure(getTextById(xhttp.status));
                }
                /*
                 * window.open("error.html?err=" + this.status + " (state=" +
                 * this.readyState + ")", "_self");
                 */
            } else {
                console.log("status: " + xhttp.status);
            }
            //return xhttp.status;
            debug("statusIndicator", release, statusString);
        }
    };
}

function setCookie(cname, cvalue, exdays) {
    debug("cookie before login:" + document.cookie);
    const allCookieArray = document.cookie.split(';');
    for (let i = 0; i < allCookieArray.length; i++) {
        debug("cookie" + i + ":" + allCookieArray[i], 0);
        allCookieArray[i] = "cookiename= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    }
    //document.cookie = "cookiename= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function showLoginFailure(message) {
    const loginMsg = document.getElementById("login-msg");
    if (loginMsg) loginMsg.innerHTML = message;
}


$(document).on('click', '.login', function () {
    const nameValue = document.getElementById("username").value;
    const passValue = document.getElementById("userpass").value;
    const themeName = document.getElementById("themeName").innerText;
    debug2("shop.login", "login clicked", RELEASE, [nameValue, themeName]);
    if (isNaN(role)) {
        //role = parseInt(role = document.getElementById("inputSelectRole").value, 10);
        console.error("No role has been set!");
    }
    //setServerPath(customerID, 1);
    login(nameValue, passValue, themeName);
})

$(document).on('click', '.userLogout', function () {
    userLogout();
    //scrollToCustomerBox("login-msg");
    //window.open("/applications/shop/layout/" + customerTheme + "/?cust=" + customerID, "_self");
})

