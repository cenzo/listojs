release = RELEASE;
module = "shopevents.js";
showRelease(module);

document.getElementById("text1").hidden = true;
document.getElementById("text741").hidden = true;
jQuery(document).on('click', '.submit-order', function () {
    const bill = document.getElementById("idbill").innerText;
    const loginState = getLoginState();
    document.getElementById("tableName").innerText = "//Delete this text. loginState = " + loginState;
    if (loginState && loginState === 2) {
        submitorder(bill);
        setLoginState("1");
        document.getElementById("tableName").innerText = "//Change this text. 'Ordered items were submitted'. loginState = " + loginState;
        scrollToCustomerBox("box0");
        document.getElementById("text1").hidden = true;
        document.getElementById("text741").hidden = true;
    } else if (loginState && loginState === 1) {
        document.getElementById("tableName").innerText = "//Change this text. 'No items in list'. loginState = " + loginState;
        scrollToCustomerBox("box0");
    } else {
        document.getElementById("username").hidden = false;
        document.getElementById("userpass").hidden = false;
        scrollToCustomerBox("username");
        document.getElementById("tableName").innerText = "//TODO: change text. Order is not active yet. Please click on 'start order'. loginState = " + loginState;
    }
});

jQuery(document).on('click', '.get-products', function () {
    const cat = $(this).attr('data-category');
    const cattext = $(this).attr('data-categoryText');
    debug("Category " + cat + " clicked (" + cattext + ")", release);
    showSubCategories(cat, cattext);
    showProductsOfCategory(cat);
    scrollToCustomerBox("box0");
});


jQuery(document).on('click', '.add-product', function () {
    const prod = $(this).attr('data-product');
    const bill = document.getElementById("idbill").innerText;
    let _idwaiter = document.getElementById("idwaiter").innerText;
    let _idtable = document.getElementById("idtable").innerText;
    if (!_idwaiter) {
        _idwaiter = 1;
    }
    if (!_idtable) {
        _idtable = 1;
    }
    const tableObject = {
        "tablenumber": _idtable
    };
    const ls = getLoginState();
    document.getElementById("tableName").innerText = "//TODO delete this text. loginState = " + ls;
    debug2(module, "add-product: bearerCookie, loginState", release, [bearerCookie, ls]);
    /*
      * loginState:
      * NaN : user is not logged in
      * 0 : user comes from login
      * 1 : user is logged in
      * 2 : there are items in the list
    * */
    if (ls === 1 || ls === 2) {
        debug2("shopevents", "Adding product-id " + prod + " to bill " + bill, release, [prod, bill])
        addproduct(bill, prod, _idtable, _idwaiter);
        if (ls === 1) {
            setLoginState("2");
        }
        $("#h3ID").remove();
        const h3Tag = document.createElement("h3");
        h3Tag.setAttribute("id", "h3ID");
        const txt = document.getElementById("addProductText").innerHTML;
        h3Tag.innerHTML = txt;
        $(this).append(h3Tag);
        $("#h3ID").fadeOut(2500);
        document.getElementById("text1").hidden = false;
        document.getElementById("text741").hidden = false;
        // } else if (ls === 0 || ls === NaN || ls === null || ls === -1) {
    } else {
        apiCall("createBill", "/1/guest/createbill", tableObject, function (data) {
            const newBill = data[4].rows[0].s[1];
            document.getElementById("idbill").innerText = newBill;
            debug2("shopevents", "Adding product with new bill " + newBill + " id " + prod + " virtual waiter " + _idwaiter + " and virtual idtable " + _idtable, release, [prod, _idwaiter, _idtable]);
            addproduct(newBill, prod, _idtable, _idwaiter);
            setLoginState("2");
            $("#h3ID").remove();
            const h3Tag = document.createElement("h3");
            h3Tag.setAttribute("id", "h3ID");
            const txt = document.getElementById("addProductText").innerHTML;
            h3Tag.innerHTML = txt;
            $(this).append(h3Tag);
            $("#h3ID").fadeOut(2500);
            document.getElementById("text1").hidden = false;
            document.getElementById("text741").hidden = false;
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        }, 'post');
    }
});

jQuery(document).on('click', '.delete-product', function () {
    // TODO change loginState to "1" if all products were ordered
    const idorder = $(this).attr('data-order');
    const bill = document.getElementById("idbill").innerText;
    debug2("shopevents", "delete item", release, [idorder, bill]);
    deleteOrderFromList(idorder, bill);
});


jQuery(document)
    .on(
        'click',
        '.product-overview',
        function () {
            const idprod = $(this).attr('data-product');
            showProductDetails(idprod);
            document.getElementById("main-content").innerText = "Create Product Overview";
        });

jQuery(document)
    .on(
        'click',
        '.show-cart',
        function () {
            const idbill = document.getElementById("idbill").innerText;
            showCart(idbill);
            scrollToCustomerBox("box2");
        });

jQuery(document)
    .on(
        'click',
        '.show-deliverystatus',
        function () {
            const idbill = document.getElementById("idbill").innerText;
            showDeliveryStatus(idbill);
            scrollToCustomerBox("box3");
        });


jQuery(document).on(
    "click",
    ".send-notification",
    function () {
        const idnote = $(this).attr('data-notification');
        const idwaiter = document.getElementById("idwaiter").innerText;
        apiCall_listorante_guest_sendnotification(idnote, idwaiter,
            function (data) {
                showNotifications();
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
    });


jQuery(document).on('click', '.start-order', function (event) {
    /*
      * loginState:
      * 0/NaN : user is not logged in
      * 1 : user is logged in
      * 2 : there are items in the list
    * */
    const userName = $('#username').val();
    const pass = $('#userpass').val();
    const loginState = loginState;
    debug2("shopevents", "start new order", RELEASE, ["userName:\n\t" + userName, "event:\n\t" + event.valueOf(), "loginState: " + loginState]);
    if (loginState && loginState === 2) {
        document.getElementById("tableName").innerText = "TODO change text. There are still un-confirmed items. loginState = " + loginState;
        scrollToCustomerBox("orderForm");
    } else if (loginState && loginState === 1) {
        document.getElementById("tableName").innerText = "TODO change text. Order is already open. loginState = " + loginState;
        scrollToCustomerBox("box0");
    } else {
        scrollToCustomerBox("username");
        document.getElementById("username").hidden = false;
        document.getElementById("userpass").hidden = false;
        if (userName && pass && userName.length > 1 && pass.length > 1) {
            login(userName, pass, customerTheme);
            initializeOrders();
        } else {
            document.getElementById("tableName").innerText = "TODO change text. Login failed . LoginState = " + loginState;
        }
    }
});

jQuery(document).on('click', '.close-order', function (event) {
    /*
         * loginState:
         * NaN : user is not logged in
         * 0 : this value should never occur here
         * 1 : user is logged in
         * 2 : there are items in the list
       * */
    const loginState = getLoginState();
    debug2("shopevents", "close order", RELEASE, ["event:\n\t" + event.valueOf(), "loginState: " + loginState]);
    if (loginState && loginState === 2) {
        scrollToCustomerBox("orderForm");
        const tblName = document.getElementById("tableName");
        tblName.innerText = "TODO: change Text There are still items in list." + loginState;
        debug2("shopevents", "close order", RELEASE, ["event:\n\t" + event.valueOf(), "loginState: " + loginState, tblName, tblName.innerText]);
    } else if (loginState && loginState === 1) {
        setLoginState("");
        userLogout();
    } else if (!loginState) {
        document.getElementById("username").hidden = false;
        document.getElementById("userpass").hidden = false;
        scrollToCustomerBox("username");
        document.getElementById("tableName").innerText = "TODO: change text. Order is already closed." + loginState;
    } else {
        document.getElementById("tableName").innerText = "TODO: delete text. Unexpected state:" + loginState;
    }
});

jQuery(document).on('click', '.shop-login', function () {
    const userObject = document.getElementById("username");
    const passObject = document.getElementById("userpass");
    const loginButton = document.getElementById("loginButton");
    const loginButtonClass = loginButton.getAttribute("class").replace("shop-login", "");
    const user = userObject.value;
    const pass = passObject.value;
    shopLogin(user, pass, function (status, responseText) {
        const lrc = parseInt("" + status, 0);
        if (lrc === 200) {
            userObject.setAttribute("style", "display:none;")
            passObject.setAttribute("style", "display:none;")
            loginButton.innerText = getTextById(26);
            loginButton.setAttribute("class", `${loginButtonClass} shop-logout`);
            setLoginState(0);
            const token = responseText.split(";")[0];
            // we need userEmail for payment checkout:
            const userEmail = responseText.split(";")[1];
            sessionStorage.setItem("userEmail", userEmail);
            setCookie("listo_" + customerID + "_" + applicationID + "_" + role, token, 1);
            sessionStorage.setItem("listo_" + customerID + "_" + applicationID + "_" + role, token);
            //initializeOrders();
        } else if (lrc === 0) {
            document.getElementById("login-msg").innerText = getTextById(499);
        } else {
            document.getElementById("login-msg").innerText = getTextById(lrc);
        }
    });

});

jQuery(document).on('click', '.shop-logout', function () {
    const userObject = document.getElementById("username");
    const passObject = document.getElementById("userpass");
    const loginButton = document.getElementById("loginButton");
    const loginButtonClass = loginButton.getAttribute("class").replace("shop-logout", "");
    shopLogout();
    userObject.removeAttribute("style");
    passObject.removeAttribute("style");
    loginButton.innerText = getTextById(615);
    loginButton.setAttribute("class", `${loginButtonClass} shop-login`);
});

// Display this only as soon as the user tries to order an item
// ??
//TODO check
//document.getElementById("userpass").style.visibility = "hidden";

//initializeOrders();
showSubCategories(0, "");
const ls = getLoginState();
debug2(module, "loginState: ", release, [ls]);
if (getLoginState() >= 0) {
    const userObject = document.getElementById("username");
    const passObject = document.getElementById("userpass");
    userObject.setAttribute("style", "display:none;")
    passObject.setAttribute("style", "display:none;")
    loginButton.innerText = getTextById(26);
    const loginButtonClass = loginButton.getAttribute("class").replace("shop-login", "");
    loginButton.setAttribute("class", `${loginButtonClass} shop-logout`);
}
