release = "xx";
module = "admin_staff.js";
// upload of user-image
let doImageUpload = false;

function uploadProfileImage(booleanValue) {
    const selectedImage = document.getElementById('staff_image_id').value;
    debug("upload profile picture", release, selectedImage);
    doImageUpload = booleanValue;
};

/*
 * Admin-Menu/ Function 5 / Staff Management
 *
 *
 */

// 1. Add new Staff
$(document)
    .on(
        'click',
        '.add-user',
        function () {
            // Create Staff Form
            const email = [FormTextArea, 75, "staff_add_email", "rows='1' required ", ''];
            const first_name = [FormTextArea, 76, "staff_add_first_name", "rows='1' required ", ''];
            const last_name = [FormTextArea, 80, "staff_add_last_name", "rows='1' required ", ''];
            const user_name = [FormTextArea, 82, "staff_add_user_name", "rows='1' required ", ''];
            const password = [FormTextArea, 81, "staff_add_password", "rows='1' required ", ''];
            const is_active = [FormCheckBox, 77, "staff_add_is_active", ""];
            const is_staff = [FormCheckBox, 78, "staff_add_is_staff", ""];
            //const is_superuser = [FormCheckBox, 79, "staff_add_is_superuser", ""];
            const submitButton = [FormButton, 83, "add_user", "", "add-user-btn"];
            const labelForUIMessages = getLabelAlertObjectId();
            const messageLabel = [formLabel, '', labelForUIMessages];
            // Form Object of Array
            const form = [email, first_name, last_name, user_name,
                password, is_active, is_staff,
                //is_superuser,
                submitButton, messageLabel];
            debug("add-user-form", 0, form);
            listoHTML.createForm("#main-content", 6, form, "add-user-form-id");
        })

// Add user with API when Click Add User button in the form
$(document).on(
    'click',
    '.add-user-btn',
    function () {
        const email = $("#staff_add_email").val();
        const first_name = $("#staff_add_first_name").val();
        const last_name = $("#staff_add_last_name").val();
        const user_name = $("#staff_add_user_name").val();
        const password = $("#staff_add_password").val();
        const is_active = $("#staff_add_is_active").prop('checked');
        const is_staff = $("#staff_add_is_staff").prop('checked');
        // Email Validation
        if (!validateEmail(email)) {
            labelAlert(getTextById(268));
            return false;
        } else {
            labelAlert("");
        }
        if ($("#add-user-form-id").valid()) {
            apiCall_listorante_admin_newstaff(email, first_name, is_active,
                is_staff, last_name, password, user_name, function (data) {
                    debug("CHECK: ", release, data);
                    showUsers();
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });
        }
    });

// 2. Display User List when click Edit User

// Function to show Users to be reusable for staff
function showUsers() {
    apiCall_listorante_admin_userprofiles(
        function (data) {

            const action1 = [
                "edit-user",
                actionModifyStyle.elements[0] + getTextById(105)
                + actionModifyStyle.elements[1]];
            const action2 = [
                "delete-user",
                actionDeleteStyle.elements[0] + getTextById(106)
                + actionDeleteStyle.elements[1]];
            const action3 = [
                "assign-role-to-user",
                actionModifyStyle.elements[0] + getTextById(90)
                + actionModifyStyle.elements[1]];

            const dataColumnNames1 = ["id", "image", "first_name",
                "last_name", "email", "username", "password",
                "is_staff", "is_active", "date_joined",
                "last-login"];
            const dataColumnIndices1 = [0, 5, 6, 7, 8, 4, 1, 3, 9, 10, 2];
            const dataColumnNames2 = ["id"];
            const dataColumnIndices2 = [0];
            const dataColumnNames3 = ["id", "image", "username"];
            const dataColumnIndices3 = [0, 5, 4];
            const userTable = {
                IDENTIFIER: "admin-user-table",
                jsonRMLData: data[0],
                header: [getTextById(85), getTextById(76),
                    getTextById(80), getTextById(82),
                    getTextById(87), "", "", ""],
                columnIndices: [5, 6, 7, 4, 10],
                actions: [action1, action2, action3],
                dataColumnIndices: [dataColumnIndices1,
                    dataColumnIndices2, dataColumnIndices3],
                dataColumnNames: [dataColumnNames1, dataColumnNames2,
                    dataColumnNames3],
                cellFormat: function (colIndex, rowIndex, tdOpen, cell, tdClose) {
                    const img = getImagePath() + "/" + cell;
                    if (colIndex === 0) return `${tdOpen}${ThumbImage(601, img)}${tdClose}`;
                    else return `${tdOpen}${cell}${tdClose}`;
                }
            };
            listoHTML.createTable.call(userTable, "main-content");
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
}

// Show Users when click edit user list item
$(document).on('click', '.edit-userprofile', function () {
    showUsers();
});

// 3. Delete User When click Delete User Button
$(document).on('click', '.delete-user', function () {
    const userId = $(this).data("id");
    apiCall_listorante_admin_deletestaff(userId, function (data) {
        showUsers();
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    })
});

// This is the main change function

// 4. Edit User Form Display when click Edit User Button
$(document).on(
    'click',
    '.edit-user',
    function () {
        const userId = $(this).data("id");
        const email = formTextInput(75, "staff_edit_email", "rows='1' required ", $(this).data("email"));
        const first_name = formTextInput(76, "staff_edit_first_name", "rows='1' required ", $(this).data("first_name"));
        const last_name = formTextInput(80, "staff_edit_last_name", "rows='1' required ", $(this).data("last_name"));
        const user_name = formTextInput(82, "staff_edit_user_name", "rows='1' required ", $(this).data("username"));
        // const password = formTextInput(81, "staff_edit_password", "rows='1' required ", $(this).data("password"));
        const is_active = FormCheckBox(77, "staff_edit_is_active", "",
            $(this).data("is_active"));
        const is_staff = FormCheckBox(78, "staff_edit_is_staff", "", $(this).data("is_staff"));
        const submitButton = FormButton(215, "edit_user_btn", "data-userid=\"" + userId + "\"", "edit-user-btn");
        const changePasswordButton = FormButton(250, 'setPassword', "data-userid=\"" + userId + "\"", 'set-Password');
        const labelForUIMessages = getLabelAlertObjectId();
        const messageLabel = formLabel('', labelForUIMessages);
        const select_new_profile_image = simpleFormImage("uploadProfileImage(true)", "staff_image_id");
        const existing_image = formDisplayImage(85, "profile_image_id", 'name="' + $(this).data("image") + '"', getImagePath() + "/" + $(this).data("image"));
        const label_select_image = formLabel(getTextById(213), "profileimageselect_label_id");
        // Form Object of Array
        const form = [email, first_name, last_name, user_name, existing_image, label_select_image, select_new_profile_image,
            is_active, is_staff, submitButton, changePasswordButton, messageLabel];

        listoHTML.buildForm("#main-content", 6, form, "edit-user-form-id");
    })
// 5. Edit User with API when click edit user button in the form
$(document).on(
    'click',
    '.edit-user-btn',
    function () {
        const userId = $(this).data("userid");
        const email = $("#staff_edit_email").val();
        const first_name = $("#staff_edit_first_name").val();
        const last_name = $("#staff_edit_last_name").val();
        const user_name = $("#staff_edit_user_name").val();
        const image = document.getElementById("profile_image_id").getAttribute("name");
        const is_active = $("#staff_edit_is_active").prop('checked');
        const is_staff = $("#staff_edit_is_staff").prop('checked');

        // Check if assgined correct user with id
        if (!userId) {
            labelAlert(getTextById(269));
            return false;
        }
        // Email Validation
        if (!validateEmail(email)) {
            labelAlert(getTextById(268));
            return false;
        } else {
            labelAlert("");
        }
        debug("doImageUpload=" + doImageUpload, "d1a6f_5", doImageUpload);
        if ($("#edit-user-form-id").valid()) {
            // API Call to the backend with edit form Data
            if (doImageUpload === true) {
                upload(customerID, 'edit-user-form-id', function (data) {
                    const profileImage = data.rows[0].s[2];
                    apiCall_listorante_admin_editprofile(email, first_name, userId,
                        profileImage, is_active, is_staff, last_name, user_name,
                        function (data) { // If success
                            if (data._rc == 0) {
                                labelAlert(getTextById(270));
                                showUsers();
                                uploadProfileImage(false);
                            } else {
                                labelAlert(getTextById(271));
                                return false;
                            }
                        }, function (err) { // If failed
                            showHttpErrorMessage("main-content", err);
                            return false;
                        });
                }, function (err) {
                    showHttpErrorMessage('main-content', err);
                });
            } else {
                apiCall_listorante_admin_editprofile(email, first_name, userId,
                    image, is_active, is_staff, last_name, user_name,
                    function (data) { // If success
                        if (data._rc == 0) {
                            labelAlert(getTextById(270));
                            showUsers();
                        } else {
                            labelAlert(getTextById(271));
                            return false;
                        }
                    }, function (err) { // If failed
                        showHttpErrorMessage("main-content", err);
                        return false;
                    });
            }
        }
    });

$(document).on(
    "click",
    ".assign-role-to-user",
    function () {
        const userId = $(this).data("id");
        const userName = $(this).data("username");
        const userImage = formThumb(103, "", "", "", $(this).data("image"));
        const title = formTitle(getTextById(82) + ": " + userName);
        const formElements = [];
        formElements.push(title);
        formElements.push(userImage);
        apiCall_listorante_admin_userroles(function (allRolesData) {
            //debug("Check roles data:", release, allRolesData);
            apiCall_listorante_admin_getuserroles(userId,
                function (userRolesData) {
                    for (let j = 0; j < allRolesData.count; j++) {
                        let roleAssigned = false;
                        let roleTocheck = parseInt(allRolesData.rows[j].s[0]);
                        for (let k = 0; k < userRolesData.count; k++) {
                            let userRole = parseInt(userRolesData.rows[k].s[1]);
                            if (userRole === roleTocheck) {
                                roleAssigned = true;
                            }
                        }
                        let roleid = parseInt(allRolesData.rows[j].s[0]);
                        let textid;
                        switch (roleid) {
                            case 1:
                                textid = 100; //manager
                                break;
                            case 3:
                                textid = 70; //waiter
                                break;
                            case 4:
                                textid = 3; // kitchen
                                break;
                            case 5:
                                textid = 102; //guest
                                break;
                            default:
                                textid = 263; // unknown (this is an error)
                        }
                        if (roleAssigned) formElements.push(FormCheckBox(textid, "checkbox_role" + allRolesData.rows[j].s[0], "data-roleid=\"" + allRolesData.rows[j].s[0] + "\"", true));
                        else formElements.push(FormCheckBox(textid, "checkbox_role" + allRolesData.rows[j].s[0], "data-roleid=\"" + allRolesData.rows[j].s[0] + "\"", false));
                    }

                    const submitButton = FormButton(103, "assign_role", "data-userid=\"" + userId + "\"", "assign-role-btn");

                    formElements.push(submitButton);
                    listoHTML.buildForm("#main-content", 6, formElements, "role_form");
                    //listoHTML.createForm("#main-content", 6, role);
                    debug("Check role:", release, role);
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });

        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });

    });

$(document)
    .on(
        "click",
        "#assign_role",
        function () {
            const userId = $(this).data("userid");

            //const release = 0;
            apiCall_listorante_admin_userroles(function (allRoles) {
                // Checkboxes in the form
                const checkboxes = [];
                for (let r = 0; r < allRoles.count; r++) {
                    checkboxes.push(allRoles.rows[r].s[0]);
                }
                //debug("checkboxes for roles to assign/revoke ", release, checkboxes);
                apiCall_listorante_admin_getuserroles(
                    userId,
                    function (data) {
                        let flagged = false;
                        for (let i = 0; i < checkboxes.length; i++) {
                            //debug("CHECK flagged:", release, flagged);
                            let roleID = parseInt(checkboxes[i]);
                            flagged = false;
                            let chk = document.getElementById("checkbox_role" + roleID).checked;
                            for (let j = 0; j < data.rows.length; j++) {
                                //let roleID = parseInt($(checkboxes[i]).prop("id"));
                                let userRoleID = parseInt(data.rows[j].s[1]);
                                //debug("roles", release, roleID, userRoleID);
                                if (roleID === userRoleID) {
                                    debug("chk=" + chk, release, chk, roleID);
                                    if (chk != true) {
                                        apiCall_listorante_admin_revokerole(roleID, userId,
                                            function (data) {
                                                //debug("CHECK roles-data:", release, data);
                                            },
                                            function (err) {
                                                showHttpErrorMessage("main-content", err);
                                            });
                                    }
                                    flagged = true;
                                    break;
                                }
                            }
                            //debug("CHECK flag in assign-roles:", release, flagged);
                            debug("chk", release, flagged, chk, roleID);
                            if (flagged === false && chk === true) {
                                apiCall_listorante_admin_assignrole(roleID, userId, function (data) {
                                    //debug("CHECK roles-data:", release, data);
                                }, function (err) {
                                    showHttpErrorMessage("main-content", err);
                                });
                            }
                        }
                        showUsers();
                    }, function (err) {
                        showHttpErrorMessage("main-content", err);
                    });
            })
        });


$(document).on(
    'click',
    '.set-Password',
    function () {
        const password = formTextInput(81, "staff_edit_password", "rows='1' required ", '');
        const passwordConfirm = formTextInput(81, "staff_confirm_password", "rows='1' required ", '');
        const hidden_user_id = formHiddenInput("", $(this).data('userid'), 'id_of_edited_user');

        const submitButton = FormButton(105, 'change_password_button', '', 'change_password');
        const messageLabel = formLabel('', 'messageLabelID');
        const form = [password, passwordConfirm, hidden_user_id, submitButton, messageLabel];
        listoHTML.buildForm('#main-content', 6, form, 'change_password');

    });


$(document).on(
    'click',
    '.change_password',
    function () {
        const password = $('#staff_edit_password').val();
        const passwordConfirm = $('#staff_confirm_password').val();
        const userID = $('#id_of_edited_user').val();
        if ($("#change_password").valid()) {
            if (password === passwordConfirm) {
                apiCall_listorante_admin_changeuserpassword(userID, password, function (data) {
                    document.getElementById('messageLabelID').innerText = getTextById(609);
                }, function (err) {
                    showHttpErrorMessage('main-content', err);
                });
            } else {
                document.getElementById('messageLabelID').innerText = getTextById(608);
            }
        }
    });