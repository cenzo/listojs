release = "xxd16af_5";
module = "guestevents.js";
showRelease(module);

document.getElementById("text1").hidden = true;
document.getElementById("text741").hidden = true;
jQuery(document).on('click', '.submit-order', function () {
    const bill = document.getElementById("idbill").innerText;
    document.getElementById("tableName").innerText = "//Delete this text. loginState = " + loginState;
    if (loginState && loginState === 2) {
        submitorder(bill);
        setLoginState("1");
        document.getElementById("tableName").innerText = "//Change this text. 'Ordered items were submitted'. loginState = " + loginState;
        scrollToCustomerBox("box0");
        document.getElementById("text1").hidden = true;
        document.getElementById("text741").hidden = true;
    } else if (loginState && loginState === 1) {
        document.getElementById("tableName").innerText = "//Change this text. 'No items in list'. loginState = " + loginState;
        scrollToCustomerBox("box0");
    } else {
        document.getElementById("username").hidden = false;
        scrollToCustomerBox("username");
        document.getElementById("tableName").innerText = "//TODO: change text. Order is not active yet. Please click on 'start order'. loginState = " + loginState;
    }
});

jQuery(document).on('click', '.get-products', function () {
    const cat = $(this).attr('data-category');
    const cattext = $(this).attr('data-categoryText');
    debug("Category " + cat + " clicked (" + cattext + ")", release);
    document.getElementById("tableName").innerText = "//TODO delete this text. loginState = " + loginState;
    showSubCategories(cat, cattext);
    showProductsOfCategory(cat);
    scrollToCustomerBox("box0");
});


jQuery(document).on('click', '.add-product', function () {
    const prod = $(this).attr('data-product');
    const bill = document.getElementById("idbill").innerText;
    const _idwaiter = document.getElementById("idwaiter").innerText;
    const _idtable = document.getElementById("idtable").innerText;
    debug('loginState: ' + loginState, release);
    debug('idbill: ' + bill, release);
    document.getElementById("tableName").innerText = "//TODO delete this text. loginState = " + loginState;
    /*
      * loginState:
      * NaN : user is not logged in
      * 0 : user comes from login, this should never occur here
      * 1 : user is logged in
      * 2 : there are items in the list
    * */
    if (loginState && (loginState === 1 || loginState === 2)) {
        debug("Adding product with id " + prod + " and waiter " + _idwaiter + " and idtable " + _idtable, release, prod, _idwaiter, _idtable);
        addproduct(bill, prod, _idtable, _idwaiter);
        if (loginState === 1) {
            setLoginState("2");
        }
        $("#h3ID").remove();
        const h3Tag = document.createElement("h3");
        h3Tag.setAttribute("id", "h3ID");
        const txt = document.getElementById("addProductText").innerHTML;
        h3Tag.innerHTML = txt;
        $(this).append(h3Tag);
        $("#h3ID").fadeOut(2500);
        document.getElementById("text1").hidden = false;
        document.getElementById("text741").hidden = false;
    } else {
        const infoText = getTextById(739);// Text is "Order not yet opened. Please insert your table number first."
        document.getElementById("tableName").innerText = infoText + " [TODO delete this text part. LoginState = " + loginState + "]";
        document.getElementById("username").hidden = false;
        scrollToCustomerBox("username");
    }
});

jQuery(document).on('click', '.delete-product', function () {
    // TODO change loginState to "1" if all products were ordered
    const idorder = $(this).attr('data-order');
    const bill = document.getElementById("idbill").innerText;
    debug2("guestevent", "delete item", release, [idorder, bill]);
    deleteOrderFromList(idorder, bill);
});


jQuery(document)
    .on(
        'click',
        '.product-overview',
        function () {
            const idprod = $(this).attr('data-product');
            showProductDetails(idprod);
            document.getElementById("main-content").innerText = "Create Product Overview";
        });

jQuery(document)
    .on(
        'click',
        '.show-cart',
        function () {
            const idbill = document.getElementById("idbill").innerText;
            showCart(idbill);
            scrollToCustomerBox("box2");
        });

jQuery(document)
    .on(
        'click',
        '.show-deliverystatus',
        function () {
            const idbill = document.getElementById("idbill").innerText;
            showDeliveryStatus(idbill);
            scrollToCustomerBox("box3");
        });


jQuery(document).on(
    "click",
    ".send-notification",
    function () {
        const idnote = $(this).attr('data-notification');
        const idwaiter = document.getElementById("idwaiter").innerText;
        apiCall_listorante_guest_sendnotification(idnote, idwaiter,
            function (data) {
                showNotifications();
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
    });


jQuery(document).on('click', '.start-order', function (event) {
    /*
      * loginState:
      * 0/NaN : user is not logged in
      * 1 : user is logged in
      * 2 : there are items in the list
    * */
    const userName = $('#username').val();
    debug2("guestevents", "start new order", RELEASE, ["userName:\n\t" + userName, "event:\n\t" +  event.valueOf(), "loginState: " + loginState]);
    if (loginState && loginState === 2) {
        document.getElementById("tableName").innerText = "TODO change text. There are still un-confirmed items. loginState = " + loginState;
        scrollToCustomerBox("orderForm");
    } else if (loginState && loginState === 1) {
        document.getElementById("tableName").innerText = "TODO change text. Order is already open. loginState = " + loginState;
        scrollToCustomerBox("box0");
    } else {
        scrollToCustomerBox("username");
        document.getElementById("username").hidden = false;
        setLoginState("");
        if (userName && userName.length > 1) {
            apiCall_listorante_public_settings('GUESTPASS', function (data) {
                const passwordString = data.rows[0].s[0];
                const pass = userName + passwordString;
                // login() will redirect to index.html with loginState 0
                // initializeOrder sets the value to 1
                if (passwordString) {
                    debug2("guestevents", "start new order", RELEASE, "passwordString was created",passwordString);
                    setLoginState("0");
                    login(userName, pass, customerTheme);
                    //after login, page will be redirected to index.html
                } else {
                    document.getElementById("tableName").innerText = "TODO delete text. Login failed. LoginState = " + loginState + " pass=" + pass;
                    debug2("guestevents", "start new order", RELEASE, "No passwordString could be created");
                }
            }, function (err) {
                debug("ERROR:", release, err);
            });
        } else {
            document.getElementById("tableName").innerText = "TODO change text. Please insert a  table-number. LoginState = " + loginState;
        }
    }
});

jQuery(document).on('click', '.close-order', function (event) {
    /*
         * loginState:
         * NaN : user is not logged in
         * 0 : this value should never occur here
         * 1 : user is logged in
         * 2 : there are items in the list
       * */
    debug2("guestevents", "close order", RELEASE, ["event:\n\t" + event.valueOf(), "loginState: " + loginState]);
    if (loginState && loginState === 2) {
        scrollToCustomerBox("orderForm");
        const tblName=document.getElementById("tableName");
        tblName.innerText = "TODO: change Text There are still items in list." + loginState;
        debug2("guestevents", "close order", RELEASE, ["event:\n\t" + event.valueOf(), "loginState: " + loginState,tblName,tblName.innerText]);
    } else if (loginState && loginState === 1) {
        setLoginState("");
        // userLogout() will redirect to index.html
        userLogout();
    } else if (!loginState) {
        document.getElementById("username").hidden = false;
        scrollToCustomerBox("username");
        document.getElementById("tableName").innerText = "TODO: change text. Order is already closed." + loginState;
    } else {
        document.getElementById("tableName").innerText = "TODO: delete text. Unexpected state:" + loginState;
    }
});
