module = "guest.js";
release = "none";
// functional parameters:
let idtable = 0;
let idwaiter = 0;
let guestCount = 0;
let loginState = parseInt(sessionStorage.getItem("customerLoginCheck"), 10);
const mandatoryIds = ["box0", "box1", "box2", "box3", "orderForm", "main-content", "languageDiv", "idbill", "idwaiter", "idtable",
    "username", "userpass", "addProductText", "inputSelectRole", "waiterName", "tableName", "customerLoginCheck", "text1",
    "text28", "text30", "text33", "text47", "text72", "text92", "text603", "text714", "text741", "closeOrder", "confirmOrder",
    "startOrder", "openMenu", "showCart", "showStatus"];

// visual parameters:
let scrollingCustomized = false;
let productDetailsCustomized = false;
let waiterName;
let tableName;
let scrollingDuration = 250;
let shownCategoryLevel = 1;

/*
Include this for language-selection into a <script>-tag at top of every customer-theme:

const updateGuestTexts = function () {
    showSubCategories(0, "");
    showCart();
    showDeliveryStatus();
    setGuestTexts();
};

const languageSelect = {
    pre: "<div><span><p>",
    suf: "</p></span></div>",
    callback: updateGuestTexts,
    styleClass:"",
    targetDiv:"main-content",
    languages: [{
        id: "en",
        name: "English"
    }, {
        id: "de",
        name: "Deutsch"
    }, {
        id: "it",
        name: "Italiano"
    }]
};

*/

function checkLayoutPermission() {
    apiCall_listorante_public_layouts(function (data) {
        let allowed = false;
        console.log("Check permission of use for theme-Id '" + themeID + "':");
        for (let i = 0; i < data.count; i++) {
            let checkedID = parseInt(data.rows[i].s[0], 10);
            console.log((1 + i) + ": Customer has permission to use theme '" + checkedID + "': " + JSON.stringify(data.rows[i].s));
            if (checkedID === parseInt(themeID, 10)) {
                console.log("Permission for theme '" + themeID + "' found.");
                allowed = true;
                //break;
            }
        }
        if (!allowed) {
            const txt = "Sorry! The customer with CID '" + customerID + "' is not allowed to use this theme...";
            document.getElementsByTagName("body")[0].innerHTML = "<h1>" + txt + "</h1>";
            console.error(txt);
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    })
}

checkLayoutPermission();

function verifyMandatoryClass(id, className) {
    try {
        const classNameAttribute = document.getElementById(id).getAttribute("class");
        if (!classNameAttribute) {
            console.error("Element with mandatory id '" + id + "' has no class-attribute (Requires '" + className + "')");
            return false;
        }
        const coIdx = classNameAttribute.indexOf(className);
        if (coIdx < 0) {
            console.error("Element with mandatory id '" + id + "' is missing the mandatory class-attribute '" + className + "'");
            return false;
        }
    } catch (err) {
        console.error("Element with mandatory id '" + id + "' is missing or not properly configured: " + err.message);
        return false;
    }
    return true;
}

function verifyTheme() {
    debug("Checking theme...", release);
    showRelease(module);
    let check;
    let check2;
    let check3;
    let ok = true;
    for (let id = 0; id < mandatoryIds.length; id++) {
        try {
            check = document.getElementById(mandatoryIds[id]).innerText;
            check2 = document.getElementById(mandatoryIds[id]).innerHTML;
            check3 = document.getElementById(mandatoryIds[id]).value;
            debug2(module, "Check-results", release, [mandatoryIds[id], check, check2, check3]);

            if (mandatoryIds[id] === "inputSelectRole") {
                if (parseInt(check, 10) !== 3) {
                    console.error("ERROR: Mandatory element with id '" + mandatoryIds[id] + "' must have value 3.");
                    ok = false;
                }
            }
        } catch (err) {
            ok = false;
            if (!check) {
                console.error("ERROR: Mandatory element with id '" + mandatoryIds[id] + "' is missing in index.html." +
                    JSON.stringify(err));
            } else {
                console.error("Unexpected error: " + JSON.stringify(err));
                console.error("Mandatory element with id '" + mandatoryIds[id] + "' is probably missing in index.html.");
            }
        }
    }
    ok = ok && verifyMandatoryClass("closeOrder", "close-order") &&
        verifyMandatoryClass("confirmOrder", "submit-order") &&
        verifyMandatoryClass("startOrder", "start-order") &&
        verifyMandatoryClass("openMenu", "get-products") &&
        verifyMandatoryClass("showCart", "show-cart") &&
        verifyMandatoryClass("showStatus", "show-deliverystatus");
    if (!ok) {
        console.error("Errors in this theme were detected. It is not properly configured and must not be used.");
    }
}


verifyTheme();

function setLoginState(state) {
    /*
    * loginState:
    * 0/NaN : user is not logged in
    * 1 : user is logged in
    * 2 : there are items in the list
  * */
    sessionStorage.setItem("customerLoginCheck", state);
    loginState = parseInt(sessionStorage.getItem("customerLoginCheck"), 10);
}

function scrollToCustomerBox(divBoxName) {
    if (scrollingCustomized && typeof guestScroll === "function") {
        guestScroll(divBoxName);
    } else {
        standardScroll(divBoxName);
    }
};

function standardScroll(boxName) {
    $('html, body').animate({
        scrollTop: $("#" + boxName).offset().top
    }, scrollingDuration);

};

function setGuestTexts() {
    debug("Setting texts...", release);
    setElemText(1);
    setElemText(6);
    setElemText(12);
    setElemText(30);
    setElemText(33);
    setElemText(35);
    setElemText(47);
    setElemText(72);
    setElemText(92);
    setElemText(603);
    setElemText(714);
    setElemText(741);

    apiCall_listorante_public_settings("ADD_PRODUCT_TEXT_" + locale, function (data) {
        const addHtml = document.getElementById("addProductText").innerHTML;
        if (addHtml) {
            $("#addProductText").html(addHtml);
            debug("addProductText=" + addHtml.toString(), release);
        } else {
            const addTxt = document.getElementById("addProductText").innerText;
            if (addTxt > 3) {
                $("#addProductText").text(addTxt);
                debug("addProductText=" + addTxt, release);
            } else {
                try {
                    $("#addProductText").text(data.rows[0].s[0]);
                } catch (err) {
                    try {
                        $("#addProductText").text(getTextById(740));
                    } catch (err2) {
                        try {
                            $("#addProductText").text("+++");
                        } catch (err3) {
                            debug("#addProductText not found.", release, err, err2, err3);
                        }
                    }
                }
            }
        }
    }, function (err) {
        debug("Setting 'ADD_PRODUCT_TEXT_" + locale + "' could not be retrieved", release, err);
        $("#addProductText").text("+");
    });
}

apiCall_listorante_public_settings("SHOWN_CATEGORY_LEVEL", function (data) {
    shownCategoryLevel = data.rows[0].s[0];
    debug2(module, "shown category level", RELEASE, [shownCategoryLevel]);

}, function (err) {
    debug2(module, "could not retrieve setting 'SHOWCATLEV'", [err]);
});

setGuestTexts();
document.getElementById("username").hidden = true;

function guest0_categories(category, categoryText, data) {
    let box0 = categoryHeader();
    let errArray = [];
    let levelCount = 0;
    for (let i = 0; i < data.count; i++) {
        if (parseInt(data.rows[i].s[3], 10) <= parseInt(shownCategoryLevel, 10)) {
            levelCount++;
        }
    }
    for (let i = 0; i < data.count; i++) {
        try {
            if (parseInt(data.rows[i].s[3], 10) <= parseInt(shownCategoryLevel, 10)) {
                box0 += categoryItem(i, data.rows[i].s, levelCount);
            }
        } catch (err) {
            debug2("guest", "Erroneous value in row " + i + " of category-data.", release, [data.rows[i].s]);
            for (let j = 0; j < data.rows[i].s.length; j++) {
                try {
                    errArray[j] = data.rows[i].s[j];
                } catch (err2) {
                    errArray[j] = "";
                }
            }
            try {
                box0 += categoryItem(i, errArray, levelCount);
            } catch (err3) {
                debug2("guest", "Erroneous value in row " + i + " could not be replaced.", release, [err3]);
            }
        }
    }
    box0 += categoryBottom();
    return box0;
}
;

function guest1_product(data) {
    let box1 = productsHeader();
    let errArray = [];
    for (let i = 0; i < data.count; i++) {
        try {
            box1 += productItem(i, data.rows[i].s, data.count);
        } catch (err) {
            debug2("guest", "Erroneous value in row " + i + " of product-data.", release, [data.rows[i].s]);
            for (let j = 0; j < data.rows[i].s.length; j++) {
                try {
                    errArray[j] = data.rows[i].s[j];
                } catch (err2) {
                    errArray[j] = "";
                }
            }
            try {
                box1 += productItem(i, errArray, data.count);
            } catch (err3) {
                debug2("guest", "Erroneous value in row " + i + " could not be replaced.", release, [err3]);
            }
        }
    }
    box1 += productsBottom();
    return box1;
};

function guest2_cart(data) {
    let total = "";
    let box2 = cartHeader();
    let errArray = [];
    for (let i = 0; i < data.count; i++) {
        try {
            if (data.rows[i].s[1].trim().startsWith("TOTAL")) {
                total = data.rows[i].s;
            } else {
                box2 += cartItem(i, data.rows[i].s);
            }
        } catch (err) {
            debug2("guest", "Erroneous value in row " + i + " of cart-data.", release, [data.rows[i].s]);
            for (let j = 0; j < data.rows[i].s.length; j++) {
                try {
                    errArray[j] = data.rows[i].s[j];
                } catch (err2) {
                    errArray[j] = "";
                }
            }
            try {
                box2 += cartItem(i, errArray);
            } catch (err3) {
                debug2("guest", "Erroneous value in row " + i + " could not be replaced.", release, [err3]);
            }
        }
    }
    box2 += cartItem("", total);
    box2 += cartBottom();
    return box2;
};

function guest3_orderstatus(data) {
    let box3 = deliveryHeader();
    let errArray = [];
    for (let i = 0; i < data.count; i++) {
        try {
            box3 += deliveryItem(i, data.rows[i].s);
        } catch (err) {
            debug2("guest", "Erroneous value in row " + i + " of delivery-status-data.", release, [data.rows[i].s]);
            for (let j = 0; j < data.rows[i].s.length; j++) {
                try {
                    errArray[j] = data.rows[i].s[j];
                } catch (err2) {
                    errArray[j] = "";
                }
            }
            try {
                box3 += orderStatusItem(i, errArray);
            } catch (err3) {
                debug2("guest", "Erroneous value in row " + i + " could not be replaced.", release, [err3]);
            }
        }
    }
    box3 += deliveryBottom();
    return box3;
};

function showSubCategories(category, categoryText) {
    // CALL-ID: listorante.Public.4
    apiCall_listorante_public_category(category, function (data) {
        document.getElementById("box0").innerHTML = guest0_categories(category, categoryText, data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}


function showProductsOfCategory(catid) {
    if (catid == 0)
        document.getElementById("box1").innerHTML = "";
    //else
    document.getElementById("box0").hidden = false;
    document.getElementById("box1").hidden = false;
    apiCall_listorante_public_product(catid, function (data) {
        document.getElementById("box1").innerHTML = guest1_product(data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

function addproduct(idbillnumber, prodid, tablenumber, waiternumber) {
    apiCall_listorante_guest_addtoproductlist(idbillnumber, prodid, tablenumber,
        waiternumber, function (data) {
            debug("add to product list, data:", release, data);
            const qty = data[4].rows[0].s[1];
            if (qty < 3) {
                $("#h3ID").remove();
                const h3Tag = document.createElement("h3");
                h3Tag.setAttribute("id", "h3ID");
                const txt = getTextById(38) + qty;
                h3Tag.innerHTML = "<strong>" + txt + "</strong>";
                $(this).append(h3Tag);
                $("#h3ID").fadeOut(7500);
            }
        }, function (err) {
            debug("addproduct debug: idbillnumber,idproduct,idtable,idwaiter", release, idbillnumber, prodid, tablenumber, waiternumber);
            showHttpErrorMessage("box1", err);
            scrollToCustomerBox("box1");
        });

}

function showProductDetails(idproduct) {
    if (productDetailsCustomized && typeof customized_ProductDetails === "function"
    ) {
        customized_ProductDetails(idproduct);
    } else {
        standard_productDetails(idproduct, "box1");
    }
}

function standard_productDetails(idproduct, div) {
    apiCall_listorante_public_getproductattributes(idproduct, function (data) {
        let details = "<div>";
        for (let i = 0; i < data.count; i++) {
            if (i == 0) {
                details += "<h3>" + data.rows[i].s[1] + "</h3>";
                details += "<p>" + data.rows[i].s[4] + ":&nbsp;&nbsp;" + data.rows[i].s[5] + "</p>";
            } else {
                details += "<p>" + data.rows[i].s[4] + ":&nbsp;&nbsp;" + data.rows[i].s[5] + "</p>";
            }
        }
        details += "</div>";
        document.getElementById(div).innerHTML = details;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
};

function deleteOrderFromList(idorder, idbillnumber) {
    apiCall_listorante_guest_delitem(idorder, function (data) {
        debug2("guest", "delete item", release, [idorder, idbillnumber, data]);
        showCart(idbillnumber);
        showDeliveryStatus(idbillnumber);
    }, function errHandling(err) {
        showHttpErrorMessage("box3", err);
    });
    scrollToCustomerBox("box3");
}

function showDeliveryStatus(idbill) {
    document.getElementById("box0").hidden = true;
    document.getElementById("box1").hidden = true;
    document.getElementById("box2").hidden = true;
    document.getElementById("box3").hidden = false;
    apiCall_listorante_guest_deliverystatus(idbill, function (data) {
        guest3_orderstatus(data);
        document.getElementById("box3").innerHTML = guest3_orderstatus(data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
};

function showCart(bill) {
    document.getElementById("box0").hidden = true;
    document.getElementById("box1").hidden = true;
    document.getElementById("box3").hidden = true;
    document.getElementById("box2").hidden = false;
    apiCall_listorante_guest_showcart(bill, function (data) {
        guest2_cart(data);
        document.getElementById("box2").innerHTML = guest2_cart(data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
};

function submitorder(bill) {
    apiCall_listorante_guest_sendorder(bill, function (data) {
        debug2("guest", "submit Order", "d16af_5", [data]);
        showDeliveryStatus(bill);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
};


function initializeOrders() {
    /*
     * loginState:
     * NaN : user is not logged in
     * 0 : user comes from login right now
     * 1 : user is logged in
     * 2 : there are items in the list
   * */
    if (loginState === 0) {
        // Create a new bill number:
        apiCall_listorante_guest_getguesttable(function (data) {
            const thetableID = data.rows[0].s[0];
            apiCall_listorante_guest_createbill(thetableID, function (data) {
                debug("Create new bill for customer", release, data);
                const newbill = data[4].rows[0].s[1];
                document.getElementById("idbill").innerText = newbill;
                apiCall_listorante_guest_billinfo(newbill, function (data) {
                        const release = RELEASE;
                        debug("Info of bill " + newbill + ":", release, data);
                        // Set loginState=1, so if page is refreshed, we still keep the
                        // assigned values for idbill, waitername etc.
                        setLoginState("1");
                        // Initialize values to be displayed:
                        idwaiter = data.rows[0].s[0];
                        idtable = data.rows[0].s[1];
                        waiterName = data.rows[0].s[2];
                        guestCount = data.rows[0].s[3];
                        tableName = data.rows[0].s[4];
                        debug2(module, "intitialization", RELEASE, ["new bill-number: " + newbill, data]);
                        //Store the values:
                        sessionStorage.setItem("idwaiter", idwaiter);
                        sessionStorage.setItem("idtable", idtable);
                        sessionStorage.setItem("waitername", waiterName);
                        sessionStorage.setItem("tableName", tableName);
                        sessionStorage.setItem("idbill", newbill);
                        if (parseInt("" + customerID, 10) > 0) {
                            sessionStorage.setItem("theCustomerID", customerID);
                        }
                        document.getElementById("idwaiter").innerText = idwaiter;
                        document.getElementById("idtable").innerText = idtable;
                        document.getElementById("waiterName").innerText = waiterName;
                        document.getElementById("tableName").innerText = tableName;
                        document.getElementById("idbill").innerText = newbill;
                        document.getElementById("username").hidden = true;
                        debug2(module, "set element's texts", RELEASE, [idwaiter, idtable, waiterName, tableName, newbill,
                            document.getElementById("tableName").innerText,
                            document.getElementById("waiterName").innerText]);
                    },
                    function (err) {
                        setLoginState("")
                        showHttpErrorMessage("box0", err);
                    });
            }, function (err) {
                setLoginState("")
                showHttpErrorMessage("box0", err);
            });
        }, function (err) {
            setLoginState("")
            debug("Seems an error but this is OK: guest is not logged in yet", release, err);
        });
    } else if (loginState > 0) {
        idwaiter = sessionStorage.getItem("idwaiter");
        document.getElementById('idwaiter').innerText = idwaiter;
        idtable = sessionStorage.getItem("idtable");
        document.getElementById('idtable').innerText = idtable;
        waiterName = sessionStorage.getItem("waiterName");
        document.getElementById('waiterName').innerText = waiterName;
        tableName = sessionStorage.getItem("tableName");
        document.getElementById('tableName').innerText = tableName;
        idbill = sessionStorage.getItem("idbill");
        document.getElementById('idbill').innerText = idbill;
        debug2(module, "intitialization, loginstate>0", RELEASE, "loginState=" + loginState, idwaiter, idtable, waiterName, tableName, idbill);
    } else {//customer has been logged out
        debug2(module, "intitialization, other loginstate", RELEASE, "loginState=" + loginState, idwaiter, idtable, waiterName, tableName, idbill);
        document.getElementById("idwaiter").innerText = "";
        document.getElementById("idtable").innerText = "";
        document.getElementById("waiterName").innerText = "";
        document.getElementById("tableName").innerText = "";
        document.getElementById("idbill").innerText = "";
        setLoginState("");
    }
}


function userLogout() {
    /*
     * loginState:
     * 0/NaN : user is not logged in
     * 1 : user is logged in
     * 2 : there are items in the list
   * */
    debug2("guest", "loginState in userLogout()", "d16af_5", [loginState]);
    sessionStorage.removeItem("theCustomerID");
    const logoutPath = getAPIServerPath() + customerID + "/1/logout";
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("token=" + bearerCookie);
    xhttp.onreadystatechange = function () {
        debug2("guest", "logoutPath,xhttp,loginState,bearerCookie", "" + release, [logoutPath, xhttp, loginState, bearerCookie]);
        if (xhttp.readyState == 4) {
            if (xhttp.status >= 400) {
                debug2("guest", "ERROR: logout-status ", "" + release, [xhttp, loginState]);
            } else {
                window.open("../" + customerTheme + "/index.html?cust=" + customerID, "_self");
            }
        }
    };
}


// Display this only as soon as the user tries to order an item
document.getElementById("userpass").style.visibility = "hidden";

initializeOrders();
showSubCategories(0, "");
