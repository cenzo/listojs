applicationID = 1;
release = '55710cc3d5f57fd63cc2ca6a9b98b3cc7a85744b';
module = "kitchen.js";

function setTexts() {
    setElemText(6);
    setElemText(26);
    onLoadItems();
}

const cookUserName = getUrlVars()['user'];
softwareProduct = 'Listorante Kitchen-Overview';
document.getElementById('loading').innerHTML = images.loading;
document.getElementById('logo').innerHTML = images.logosmall;
const subsetData = [];

const kitchenData = [];
const isSubset = false;

let nItems;

//onLoadItems(); place this call into kitchen.html!


function onLoadItems() {
    apiCall_elisto_warehouse_items(function (data) {
        const action1 = ["changeUp btn btn-app", "<i class='fa fa-angle-double-up'></i>"];
        const action2 = ["changeDown btn btn-app", "<i class='fa fa-angle-double-down'></i>"];
        const dataColumnNames1 = ["idorder", "idstatus"];
        const dataColumnIndices1 = [0, 9];
        const dataColumnNames2 = ["idorder", "idstatus"];
        const dataColumnIndices2 = [0, 9];
        const warehouseObject = {
            IDENTIFIER: "warehouse-items",
            jsonRMLData: data,
            actions: [action1, action2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
            header: [getTextById(33), getTextById(69), getTextById(36), getTextById(61), getTextById(70), getTextById(71), getTextById(72), getTextById(74), "", ""],
            columnIndices: [1, 2, 4, 5, 6, 8, 10, 12],
            //changes VP 2019-03-23: always use Table-functions for customized formattting
            rowFormat: function (rowIndex, sArray) {
                const status = sArray[8];
                if (sArray[9] === "1") {
                    sArray[8] = DataStyle.statusOrderedStartTag + " id='orderStatus" + sArray[0] + "'>"
                        + status
                        + DataStyle.statusOrderedEndTag;
                } else if (sArray[9] === "2") {
                    sArray[8] = DataStyle.statusPreparationOpenTag + " id='orderStatus" + sArray[0] + "'>"
                        + status
                        + DataStyle.statusPreparationEndTag;
                } else if (sArray[9] === "3") {
                    sArray[8] = DataStyle.statusDeliveredStartTag + " id='orderStatus" + sArray[0] + "'>"
                        + status
                        + DataStyle.statusDeliveredEndTag;
                }
                return sArray;
            },
        };
        listoHTML.createTable.call(warehouseObject, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });

}


$(document).on('click', '.changeUp', function () {
    let status = 0;
    const idorder = $(this).attr('data-idorder');
    const orderStatus = sessionStorage.getItem("status" + idorder);
    if (orderStatus) status = parseInt(orderStatus) + 1;
    else status = parseInt($(this).attr('data-idstatus')) + 1;
    if (status === 4) alert("Cannot push upwards this status any more");
    else {
        changeUp(idorder, status);
        sessionStorage.setItem("status" + idorder, status);
    }
    debug("changeUp orderStatus=" + status, release);
});

$(document).on('click', '.changeDown', function () {
    let status = 0;
    const idorder = $(this).attr('data-idorder');
    const orderStatus = sessionStorage.getItem("status" + idorder);
    if (orderStatus) status = parseInt(orderStatus) - 1;
    else status = parseInt($(this).attr('data-idstatus')) - 1;
    if (status === 0) alert("Cannot push downwards this status any more");
    else {
        changeDown(idorder, status);
        sessionStorage.setItem("status" + idorder, status);
    }
    debug("changeDown orderStatus=" + status, release);
});


function changeUp(idorder, status) {
    apiCall_elisto_warehouse_setprodstatus(idorder, status, function (data) {
        const oldStatus = document.getElementById('orderStatus' + idorder);
        if (status === 2) {
            oldStatus.setAttribute("class", DataStyle.statusPreparationClass);
            oldStatus.innerHTML = getTextById(222);
        } else if (status === 3) {
            oldStatus.setAttribute("class", DataStyle.statusDeliveredClass);
            oldStatus.innerHTML = getTextById(223);
        }
    }, function (err) {
        debug("ERROR", release, err);
    });
}

function changeDown(idorder, status) {
    apiCall_elisto_warehouse_setprodstatus(idorder, status, function (data) {
        const oldStatus = document.getElementById('orderStatus' + idorder);
        if (status === 2) {
            oldStatus.setAttribute("class", DataStyle.statusPreparationClass);
            oldStatus.innerHTML = getTextById(222);
        } else if (status === 1) {
            oldStatus.setAttribute("class", DataStyle.statusOrderedClass);
            oldStatus.innerHTML = getTextById(221);
        }
    }, function (err) {
        debug("ERROR", release, err);
    });
}

jQuery(document).on('click', '.show-Info', function () {
    document.getElementById('main-content').innerHTML = showInfo();
});