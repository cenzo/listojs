module = "product-attributes.js";

/*
 * Admin-Menu/ Function 4 / PRODUCTS ATTRIBUTE MODULE
 *
 *
 *
 *
 *
 *
 */
function showGallery(idProduct) {

    const image = [formImage, 56, "file_imagefield", "required", "", "fileImage_id"];
    const submitButton = [FormButton, 319, "add_image", "", "add-image"];
    const hiddenField = [formHiddenInput, "hiddenProductID", idProduct, "productID_" + idProduct];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const form1 = [image, hiddenField, submitButton, tableDiv];

    listoHTML.createForm("#main-content", 8, form1, 'product-gallery-form');


    apiCall_elisto_public_productimages(idProduct, function (data) {
        //debug(data);
        if (data.count > 0) {
            const action1 = ["deleteImageIcon", actionModifyStyle.elements[0] + getTextById(106) + actionModifyStyle.elements[1]];

            const dataColumnNames1 = ["idlnk_product_image", "idproduct", "imagepath"];
            const dataColumnIndices1 = [0, 1, 2];
            const ProductObject = {
                IDENTIFIER: "admin-showProductGallery",
                jsonRMLData: data,
                header: [getTextById(321)],
                columnIndices: [2],
                actions: [action1],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1],
                rowFormat: function (rowIndex, dataArray) {
                    return dataArray;
                },
                cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                    if (colIndex === 0) {
                        return TDopen + "<img src='" + getImagePath() + "/" + data + "' width='200' >" + TDclose;
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            };
            listoHTML.createTable.call(ProductObject, "tableDiv", true);
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });

}

// PRODUCT GALLERY - event
$(document).on('click', '.product_gallery', function () {
    const idProduct = $(this).data('idproduct');
    showGallery(idProduct);
});


$(document).on('click', '.deleteImageIcon', function () {
    const idlnk_product_image = $(this).data('idlnk_product_image');
    const idProduct = $(this).data('idproduct');
    //debug(idlnk_product_image);
    apiCall_elisto_admin_deleteproductimage(idlnk_product_image, function (data) {
        showGallery(idProduct);
    }, function (error) {
        alert("Not Able to Delete The Image from Product Gallery");
    });
});


$(document).on('click', '#add_image', function () {
    const idProduct = $('.hiddenProductID').val();
    const custID = getCustomerId();
    if (document.getElementById('file_imagefield').value) {
        debug('ok');
        upload(custID, 'product-gallery-form', function (data) {
            debug("upload-data:", release, data);
            const imageNew = "" + data.rows[0].s[2];
            const image_thumbNew = imageNew;
            alert(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'");
            apiCall_elisto_admin_insertproductimage(idProduct, imageNew, function (data) {
                if (data._rc > 0) {
                    alert(getTextById(253) + data._rc);
                } else {
                    showGallery(idProduct);
                }
            }, function err() {
                showHttpErrorMessage("main-content", err);
            });
        }, function (error) {
            alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
        });
    }
});


// PRODUCT Attributes - event
$(document).on('click', '.product_attributes', function () {
    const idProduct = $(this).data('idproduct');
    showproductattributes(idProduct);
});


function showproductattributes(idProduct) {
    const newattributeButton = [FormButton, 322, "add_attribute", "", "add-attribute"];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const hiddenField = [formHiddenInput, "hiddenProductID", idProduct, "productID_" + idProduct];
    const form1 = [hiddenField, newattributeButton, tableDiv];
    listoHTML.createForm("#main-content", 12, form1, 'add-attribute-form-button');


    apiCall_elisto_public_getproductattributes(idProduct, function (data) {
        debug(data);
        const action1 = ["editattribute", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
        const action2 = ["deassignattribute", actionDeleteStyle.elements[0] + getTextById(329) + actionDeleteStyle.elements[1]];
        const countRecords = data.count;
        const dataColumnNames1 = ["idproduct", "prodname", "idattribute", "attributemnemonic", "productattributename_" + locale, "productattributevalue_" + locale];
        const dataColumnIndices1 = [0, 1, 2, 3, 4, 5];
        const ProductObject = {
            IDENTIFIER: "admin-showProductAttributes",
            jsonRMLData: data,
            header: [getTextById(50), getTextById(323), getTextById(324)],
            columnIndices: [1, 4, 5],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices1],
            dataColumnNames: [dataColumnNames1, dataColumnNames1],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (countRecords < 1) {
                        return "<td colspan='4'>" + getTextById(325) + "</td>";
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(ProductObject, "tableDiv", true);
    }, function (err) {

    });
}


$(document).on('click', '.deleteattribute', function () {
    const idproductattributename = $(this).data('idproductattributename');
    const productID = $('.hiddenProductID').val();
    apiCall_elisto_admin_deleteattributedefinition(idproductattributename, function (data) {
        listallattributestable(productID);
    }, function (err) {

    });
});

// FUNCTION FOR LISTING ATTRIBUTES AND NEW ATTRIBUTE BUTTON

function listallattributestable(productID) {

    const newattributeButton = [FormButton, 328, "add_new_attribute", "", "add-new-attribute"];
    const cancelButton = [FormButton, 602, "backtoAttributeList", "", "cancel-button"];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const hiddenField = [formHiddenInput, "hiddenProductID", productID, "productID_" + productID];
    const form1 = [hiddenField, newattributeButton, cancelButton, tableDiv];
    listoHTML.createForm("#main-content", 12, form1, 'add-new-attribute-form-button');


    apiCall_elisto_public_allproductattributes(function (data) {
        debug(data);
        const action1 = ["editattribute", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
        const action2 = ["deleteattribute", actionDeleteStyle.elements[0] + getTextById(106) + actionDeleteStyle.elements[1]];
        const action3 = ["selectattribute", actionModifyStyle.elements[0] + getTextById(326) + actionModifyStyle.elements[1]];
        const countRecords = data.count;
        const dataColumnNames1 = ["idproductattributename", "attributemnemonic", "productattributename_" + locale];
        const dataColumnIndices1 = [0, 1, 2];
        const ProductObject = {
            IDENTIFIER: "admin-setProductAttributes",
            jsonRMLData: data,
            header: [getTextById(327), getTextById(323) + '_' + locale],
            columnIndices: [1, 2],
            actions: [action3, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices1],
            dataColumnNames: [dataColumnNames1, dataColumnNames1],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (countRecords < 1) {
                        return "<td colspan='3'>" + getTextById(325) + "</td>";
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(ProductObject, "tableDiv");
    }, function (err) {

    });
}


// ADD ATTRIBUTE BUTTON CLICK EVENT

$(document).on('click', '.add-attribute', function () {
    const productID = $('.hiddenProductID').val();
    listallattributestable(productID);
});


// BACK TO ALL ATTRIBUTE LIST
$(document).on('click', '#backtoAllAttributeList', function () {
    const productID = $('.hiddenProductID').val();
    listallattributestable(productID);
});


// DEFINE NEW ATTRIBUTE BUTTON 

$(document).on('click', '#add_new_attribute', function () {
    const productID = $('.hiddenProductID').val();
    const attributemnemonic = [formTextInput, 327, "attributemnemonic", " required ", ''];
    const productattributename_locale = [formTextInput, 323, "productattributename_locale", " required ", ''];
    const cancelButton = [FormButton, 602, "backtoAllAttributeList", "", "cancel-button"];
    const newattributeButton = [FormButton, 215, "save_new_attribute", "", "save-new-attribute"];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const hiddenField = [formHiddenInput, "hiddenProductID", productID, "productID_" + productID];
    const form1 = [attributemnemonic, productattributename_locale, hiddenField, newattributeButton, cancelButton, tableDiv];
    listoHTML.createForm("#main-content", 8, form1, 'add-new-attribute-form-button');
});


$(document).on('click', '#save_new_attribute', function () {
    const productID = $('.hiddenProductID').val();
    const attributemnemonic = $('#attributemnemonic').val();
    const productattributename_locale = $('#productattributename_locale').val();
    apiCall_elisto_admin_defineattribute(attributemnemonic, productattributename_locale, function (data) {
        debug(data);
        listallattributestable(productID);
    }, function (err) {

    });
});

// SELECT ATTRIBUTE FOR PRODUCT 

$(document).on('click', '.selectattribute', function () {
    const productID = $('.hiddenProductID').val();
    const idproductattributeID = $(this).data('idproductattributename');
    const attributemnemonic = [formTextInput, 327, "attributemnemonic", " required ", $(this).data('attributemnemonic')];
    const productattributename_locale = [formTextInput, 323, "productattributename_locale", " required ", $(this).data('productattributename_' + locale)];
    const productattributevalue = [formTextInput, 324, "productattributevalue", " required ", ''];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const newattributeButton = [FormButton, 215, "set_new_attribute", "", "set-new-attribute"];
    const hiddenField = [formHiddenInput, "hiddenProductID", productID, "productID_" + productID];
    const hiddenAttributeID = [formHiddenInput, "hiddenAttributeID", idproductattributeID, "attributeID_" + idproductattributeID];
    const form1 = [attributemnemonic, productattributename_locale, productattributevalue, hiddenAttributeID, hiddenField, newattributeButton, tableDiv];
    listoHTML.createForm("#main-content", 8, form1, 'update-attribute-form-button');


});

// SUBMITTING SET ATTRIBUTE FORM

$(document).on('click', '.set-new-attribute', function () {
    const idproduct = $('.hiddenProductID').val();
    const idattribute = $('.hiddenAttributeID').val();
    const attributemnemonic = $('#attributemnemonic').val();
    const productattributename_locale = $('#productattributename_locale').val();
    const productattributevalue_locale = $('#productattributevalue').val();
    apiCall_elisto_admin_setattribute(idattribute, idproduct, productattributevalue_locale, function (data) {
        debug(data);
        showproductattributes(idproduct);
    }, function (err) {

    });
});


// EDIT ATTRIBUTE VALUE

$(document).on('click', '.editattribute', function () {
    debug('Clicked');
    const idproduct = $('.hiddenProductID').val();
    const idattribute = $(this).data('idattribute');

    const attributemnemonic = [formTextInput, 327, "attributemnemonic", " required readonly ", $(this).data('attributemnemonic')];
    const productattributename_locale = [formTextInput, 323, "productattributename_locale", " required readonly ", $(this).data('productattributename_' + locale)];
    const productattributevalue = [formTextInput, 324, "productattributevalue", " required ", $(this).data('productattributevalue_' + locale)];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const newattributeButton = [FormButton, 215, "update_attribute_value", "", "update-attribute-value"];
    const hiddenField = [formHiddenInput, "hiddenProductID", idproduct, "productID_" + idproduct];
    const hiddenAttributeID = [formHiddenInput, "hiddenAttributeID", idattribute, "attributeID_" + idattribute];
    const cancelButton = [FormButton, 602, "backtoAttributeList", "", "cancel-button"];
    const form1 = [attributemnemonic, productattributename_locale, productattributevalue, hiddenAttributeID, hiddenField, newattributeButton, cancelButton, tableDiv];
    listoHTML.createForm("#main-content", 8, form1, 'update-attribute-form-button');
});

/* BACK TO LIST */
$(document).on('click', '#backtoAttributeList', function () {
    const productID = $('.hiddenProductID').val();
    showproductattributes(productID);
});

// SUBMITTING EDIT ATTRIBUTE VALUE FORM

$(document).on('click', '.update-attribute-value', function () {
    const idproduct = $('.hiddenProductID').val();
    const idattribute = $('.hiddenAttributeID').val();
    const attributemnemonic = $('#attributemnemonic').val();
    const productattributename_locale = $('#productattributename_locale').val();
    const productattributevalue_locale = $('#productattributevalue').val();
    apiCall_elisto_admin_editattribute(idattribute, idproduct, productattributevalue_locale, function (data) {
        debug(data);
        showproductattributes(idproduct);
    }, function (err) {

    });
});

// DEASSIGN ATTRIBUTE FROM PRODUCT

$(document).on('click', '.deassignattribute', function () {
    const idproduct = $(this).data('idproduct');
    const idattribute = $(this).data('idattribute');
    apiCall_elisto_admin_unsetattribute(idattribute, idproduct, function (data) {
        debug(data);
        showproductattributes(idproduct);
    }, function (err) {

    });
});