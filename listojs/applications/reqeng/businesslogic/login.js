module = "login.js";
applicationID = 1;

release=RELEASE;

/**
 * Set texts
 **/
function setLoginTexts() {
    const passW = getTextById(81);
    const uName = getTextById(82);
    const cID = getTextById(729);

    setElemText(615);
    setElemText(713);
    setElemText(726);
    setElemText(727);
    setElemText(728);
    let roleAssigned = "";
    let user = document.getElementById("user");
    let password = document.getElementById("password");
    let inputCustomerID = document.getElementById("inputCustomerID")
    if (user) {
        user.placeholder = uName;
    }
    if (password) {
        password.placeholder = passW;
    }
    if (inputCustomerID) {
        inputCustomerID.placeholder = cID;
    }
    /*
    if (!isNaN(role)) {
        document.getElementById("selectRole").innerHTML = "";
    }
    */
    if (role === 0) {
        roleAssigned = getTextById(100); //Manager
    } else if (role === 1) {
        roleAssigned = getTextById(70);//Waiter
    } else if (role === 2) {
        roleAssigned = getTextById(3);//Cook
    } else if (role === 3) {
        roleAssigned = getTextById(102); // client
    } else {
        roleAssigned = getTextById(715); //select - message
    }
    let roleElement = document.getElementById("role");
    if (roleElement) {
        roleElement.innerText = roleAssigned;
    }
    if (customerID) {
        let selectCustomer = document.getElementById("selectCustomer");
        if (selectCustomer) {
            selectCustomer.innerHTML = "";
        }
        let text726 = document.getElementById("text726");
        if (text726) {
            text726.innerHTML = "";
        }
        let text727 = document.getElementById("text727");
        if (text727) {
            text727.innerHTML = "";
        }
        let text728 = document.getElementById("text728");
        if (text728) {
            text728.innerHTML = "";
        }
    }
}

//setTexts();


// Using local storage is bad practice: the values are never deleted from browser!
// Better clear all values. "So what" for other applications which should use the local storage
localStorage.clear();


function login(user, pass, customerTheme) {
    console.log("LOGIN START");
    if (!customerID || customerID === 9) {
        console.error("Invalid customerID for login: " + customerID);
    } else if (!selectedServer) {
        console.error("No server was selected, yet");
    } else {
        const xhttp = new XMLHttpRequest();
        const path = `${selectedServer}server/${customerID}/1`;
        xhttp.open("GET", path, true);
        xhttp.onreadystatechange = function () {
            console.log("path to select customer server=" + path);
            if (xhttp.readyState === 4) {
                if (xhttp.status === 200) {
                    const customerServerData = JSON.parse(xhttp.responseText);
                    debug2("login", "check customerServerData", RELEASE, [customerServerData]);
                    selectedServer = `https://backend${customerServerData.nodeid}.listorante.com${backendVersion}`;
                    console.log(":::: Selected customer server: " + selectedServer);
                    sessionStorage.setItem("selectedServer", selectedServer);
                    doLogin(selectedServer + customerID + "/1/login", user, pass, customerTheme);
                }
            }
        };
        xhttp.send();
    }
    console.log("LOGIN END");
}

function doLogin(loginPath, user, pass, customerTheme) {
    debug("customerID is: " + customerID, "d16af_5");

    //const loginPath = getAPIServerPath() + customerID + "/1/login";
    bearerCookieName = "listoranteToken" + customerID + user;
    showLoginFailure("");

    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", loginPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + user + "&password=" + pass);
    xhttp.onreadystatechange = function () {
        console.log("loginPath=" + loginPath);
        let statusString = "Start";

        statusString += "_" + xhttp.readyState + ": " + xhttp.status + " (" + xhttp.statusText + " " + xhttp.getAllResponseHeaders() + ")";
        //document.getElementById("statusIndicator").innerHTML = statusString;

        if (xhttp.readyState === 4) {
            if (xhttp.status === 200) {
                setCookie(bearerCookieName, xhttp.responseText, 1);
                bearerCookie=getCookie(bearerCookieName);
                sessionStorage.setItem("userEmail", xhttp.responseText.split(";")[1]);
                debug2("login", "xhttp.responseText", RELEASE, [xhttp.responseText]);

                if (role === 0) {
                    window.open("admin_adminLTE_customization.html?cust=" + customerID, "_self");
                } else if (role === 1) {
                    window.open("wdash_adminLTE_customization.html?cust=" + customerID, "_self");
                } else if (role === 2) {
                    window.open("kitchen_adminLTE_customization.html?cust=" + customerID, "_self");
                } else if (customerTheme) {
                    window.open("../" + customerTheme + "/index.html?cust=" + customerID, "_self");
                } else {
                    showLoginFailure(getTextById(499));
                }
            } else if (xhttp.status >= 400) {
                if (xhttp.status == 403 || xhttp.status == 401) {
                    showLoginFailure(getTextById(738));// message will be "Login failed: wrong password?"
                } else if (xhttp.status == 404) {
                    showLoginFailure(getTextById(404));
                } else if (xhttp.status > 404) {
                    showLoginFailure(getTextById(xhttp.status));
                }
                /*
                 * window.open("error.html?err=" + this.status + " (state=" +
                 * this.readyState + ")", "_self");
                 */
            } else {
                console.error("status: " + xhttp.status);
                showLoginFailure(getTextById(400));
            }
            //return xhttp.status;
            debug("statusIndicator", release, statusString);
        }
    };
}

function setCookie(cname, cvalue, exdays) {
    debug("cookie before login:" + document.cookie);
    const allCookieArray = document.cookie.split(';');
    for (let i = 0; i < allCookieArray.length; i++) {
        debug2("login", "cookie" + i + ":" + allCookieArray[i], RELEASE, [""]);
        allCookieArray[i] = "cookiename= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    }
    //document.cookie = "cookiename= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function showLoginFailure(message) {
    const loginMsg = document.getElementById("login-msg");
    if (loginMsg) loginMsg.innerHTML = message;
}


$(document).on('click', '.login', function () {
    const nameValue = document.getElementById("user").value;
    const passValue = document.getElementById("password").value;
    const themeName = document.getElementById("themeName").value;
    if (isNaN(customerID)) {
        customerID = parseInt(document.getElementById("inputCustomerID").value, 10);
    }
    if (isNaN(role)) {
        //role = parseInt(role = document.getElementById("inputSelectRole").value, 10);
        console.error("No role has been set!");
    }
    //setServerPath(customerID, 1);
    login(nameValue, passValue, themeName);
})

