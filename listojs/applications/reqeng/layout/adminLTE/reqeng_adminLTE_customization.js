/**
 *
 */
let showInfo = function () {
    let custId = "-";
    if (customerID) {
        custId = customerID;
    }
    return `<div class="col-md-12"><img src="${getImagePath()}/listorante_logo_gimp.png"><br/>
        <h4><strong><i class="fa fa-language"></i> ${getCurrentLanguage()}</strong></h4><br/>
        <b>CID: ${custId}</b><br/>
        <b>${softwareProduct}</b><br/>
        Version: ${version}<br/>
        release: ${RELEASE}<br/>
        frontend:${WEBSERVER}<br/>
        backend: ${getAPIServerPath()}<br/>
        <b>Listojs</b><br>
        Copyright © 2019  Vincenzo Parrilla<br/>
        Copyright © 2019  Enpasoft GmbH<br/>
        <b>AdminLTE</b><br/>
        Copyright © 2014-2019 Almsaeed Studio<br/></div>`;
};

const FormStyle = {
    pre1: '<div class="row"><div class="col-md-',
    pre2: '"><div class="box box-primary"><form role="form" id=',
    pre3: ' enctype="multipart/form-data"><div class="box-body">',
    suf: '</div></form></div></div></div>',
};


const actionModifyStyle = {
    elements: ["<span class=\"badge bg-purple\">", "</span>"]
};
const actionDeleteStyle = {
    elements: ["<span class=\"badge bg-red\">", "</span>"]
};

const FormButtonStyle = {
    elements: [`<div class="form-group"><a class="btn btn-primary `, `" id="`, `"`, `>`, `</a></div>`]
};

const HorizontalFormButtonStyle = {
    elements: ['<div class="col-lg-3"><a class="btn btn-primary ',
        '" id="', '" ', '>', '</a></div>']
};

const actionSupportStyle = {
    elements: ["<i class='fa fa-headphones'></i>"]
};

const createTicketStyle = {
    elements: ['<i class="fa fa-ticket" title="Create Ticket" aria-hidden="true"></i>']
};

const viewTicketStyle = {
    elements: ['<i class="fa fa-file-text" title="View details" aria-hidden="true"></i>']
};


/**
 * @param textid
 * @param actionid
 * @param options
 * @param value
 * @returns {jQuery|HTMLElement}
 * @constructor
 */
function HorizontalFormImageUploader(textid, actionid, options, value) {
    var theText = getTextById(textid);
    var templateElemTemplate = HorizontalFormImageUploaderStyleTemplate.elementTemplate;
    var searchRegexId = new RegExp('{elem_Id}', 'g');
    var searchRegexSrc = new RegExp('{elem_src}', 'g');
    templateElemTemplate = templateElemTemplate.replace(searchRegexId, actionid);
    templateElemTemplate = templateElemTemplate.replace(searchRegexSrc, value === undefined || value === "undefiend" ? '' : value);
    return templateElemTemplate;
}

function layoutPageTemplate(iFrameWidth, iFrameHeight, src, iFrameID) {
    return `<div class="col-md-12">
    <div class="box">
    <div class="box-header with-border">
    <h3 class="box-title" id="text262"></h3>
    </div>
    <div class="col-md-8">
    <iframe style="width: ${iFrameWidth};height: ${iFrameHeight};" src="${src}" id="${iFrameID}"></iframe>
    </div>`;
}

function formIcon(theText, theClass, theId) {
    let str = '<i class="' + theClass + '"';
    if (theId) {
        str += 'id="' + theId + '">';
    } else {
        str += '>';
    }
    if (theText) {
        str += theText + '</i>';
    } else {
        str += '</i>'
    }
    return str;
}

const TitleStyle = {
    start: '<h4>',
    end: '</h4>'
};

const LabelStyle = {
    start: '<label id="',
    mid: '">',
    end: '</label>'
};

const FormTextAreaStyle = {
    elements: [
        '<div class="form-group"><label for="',
        '">',
        '</label><textarea style="border: 1px solid #E27849;box-shadow: #e3e3e3; border-radius: 0.75rem" type="text" class="form-control" id="',
        '" name="',
        '" placeholder="',
        '" ',
        ' >',
        '</textarea></div>'
    ]
};
const FormNumericInputStyle = {
    elements: [
        '<div class="form-group has-warnings"><label for="',
        '">',
        '</label><input style="border: 1px solid #E27849;box-shadow: #e3e3e3; border-radius: 0.75rem" type="number" class="form-control" id="',
        '" name="',
        '"  ',
        ' value="',
        '"></div>'
    ]
};


const FormTextInputStyle = {
    elements: [
        '<div class="form-group has-warning"><label for="',
        '">',
        '</label><input style="border: 1px solid #E27849;box-shadow: #e3e3e3; border-radius: 0.75rem" type="text" class="form-control" id="',
        '" name="',
        '" placeholder="',
        '" ',
        ' value="',
        '"></div>'
    ]
};
const FormHiddenInputStyle = {
    pre: '<input type="hidden" class="',
    mid1: '" id="',
    mid2: '" value="',
    suf: '">'
};
const FormImageInputStyle = {
    pre: '<input type="file" name="file" onchange="',
    mid1: '" id="',
    suf: '">'
};
const FormPassWordStyle = {
    elements: ['<div class="form-group"><label for="', '">',
        '</label><input type="password" class="form-control" id="',
        '" placeholder="', '"  ', ' value="', '"></div>']
};

const HorizontalFormTextAreaStyle = {
    elements: ['<div class=\'col-lg-4\'><textarea class="form-control" id="',
        '" placeholder="', '" ', ' >', '</textarea></div>'],
};
const HorizontalFormTextInputStyle = {
    elements: ['<div class=\'col-lg-4\'><input type="text" class="form-control" id="',
        '" placeholder="', '" value="', '" ', ' /></div>'],
};


var HorizontalFormImageUploaderStyleTemplate = {
    elementTemplate:
        "<div class='col-lg-4 horizontal-image-uploader' data-section-id='{elem_Id}-section' data-id='{elem_Id}' data-src='{elem_src}'>" +
        "<input type='hidden' name='saved_image_path' class='saved_image_path' id='{elem_Id}_input_path'>" +
        "<div class='select-group' style='display: none;'>" +
        "<input type='file' class='image-input' id='{elem_Id}_input'>" +
        "<img src='../icons/add-image.png' class='image-input-placeholder'>" +
        "</div>" +
        "<div class='image-preview-group' style='display: none;'>" +
        "<button type='button' class='remove_image_button' data-id='{elem_Id}' id='{elem_Id}_remove_image_button'></button>" +
        "<img src='{elem_src}' class='image_thumb_button' id='{elem_Id}_image_tag'>" +
        "</div>" +
        "</div>"
};

const FormCheckBoxStyle = {
    elements: ['<div class="checkbox"><label><input type="checkbox" id="',
        '>', '</label></div>', ' checked="true">'],
};
const FormSelectBoxStyle = {
    pre: `<div class="form-group has-warnings"><label>`,
    mid1: `</label><select style="border: 1px solid #E27849;box-shadow: #e3e3e3; border-radius: 0.75rem" class="form-control `,
    mid2: `" id="`,
    mid3: `">`,
    suf: `</select></div>`
};

const images = {
    logo: `<img src="${getImagePath()}/listorante_logo_gimp.png">`,
    logosmall: `<img src="${getImagePath()}/listorante_logo_small.png">`,
    loading: `<img src="${getImagePath()}/loading_2d_balls_red.png" width="75">`,
    loading2: `<img src="${getImagePath()}/clockwait.gif" width="75">`,
    loading3: `<img src="${getImagePath()}/loading_3d_blue.gif" width="75">`,
    missing_image: `<img src="${getImagePath()}/missing_image.png">`,
    missing_image_txt: `missing_image.png`

};


const DataStyle = {
    evidence1Open: '<span class="badge bg-navy">',
    evidence1Close: '</span>',
    evidence2Open: '<span class="badge bg-red">',
    evidence2Close: '</span>',
    evidence3Open: '<span class="label label-warning">',
    evidence3Close: '</span>',
    statusOrderedStartTag: '<span class="bg bg-orange" ',
    statusOrderedEndTag: '</span>',
    statusPreparationOpenTag: '<span class="badge bg-purple" ',
    statusPreparationEndTag: '</span>',
    statusDeliveredStartTag: '<span class="badge bg-olive" ',
    statusDeliveredEndTag: '</span>',
    statusOrderedClass: 'bg bg-orange',
    statusPreparationClass: 'badge bg-purple',
    statusDeliveredClass: 'badge bg-olive',
};

const TableStyle = {
    // tableHeadOpen: '<div class="row"><div class="col-md-12"><div class="box" style="overflow-x: scroll;"><table class="table table-bordered table-striped dataTable"><thead><tr>',
    tableHeadOpen: '<div class="row"><div class="col-md-12"><div class="box" style="overflow-x: scroll;"><table class="dataTable"><thead><tr>',
    tableHeadClose: '</tr></thead><tbody>',
    thOpen: '<th>',
    thClose: '</th>',
    trOpen: '<tr>',
    trClose: '</tr>',
    tdOpen: '<td>',
    tdClose: '</td>',
    aOpen: '<a',
    aAttributes: '>',
    aClose: '</a>',
    bottom: '</tbody></table></div></div></div>',
    elements: [],
};

const ThumbStyle = {
    default: "width='60'",
};
/*
let LanguageSelect = {
    pre: '<div class="row"><div class="col-md-4"><div class="box" style="overflow: hidden; height: 300px;">'
        + '<form id="langForm"><h1>. . . . . .&nbsp;<i class="fa fa-language"></i></h1><!--select name="lang" id="langSelect"--><fieldset name="lang" id="langSelect">',
    suf: '</fieldset><!--/select--></form></div></div></div>',
};*/

const FormActionInputStyleNumeric = {
    pre: `<div class="input-group margin">
            <input type="number" class="form-control">
            <span class="input-group-btn">
                <button type="button" class="btn btn-info btn-flat">`,
    suf: `          </button>
            </span>
         </div>`
};


function ThumbImage(textId, theImage) {
    const theText = getTextById(textId);
    return `<div class="form-group"><label for="${theText}">
                ${theText} </label></div><div class="form-group"><img src="${theImage}"
                width="60"></div>`;

}

function simpleFormImage(onChange, idImage) {
    return `${FormImageInputStyle.pre}${onChange}${FormImageInputStyle.mid1}${idImage}${FormImageInputStyle.suf}`;
}

function formImage(textid, actionid, options, value, theImage) {
    const theText = getTextById(textid);
    return `<div class="form-group"><label for=" ${theText}" >
                  ${theText} </label><input type="file" name="file"
                   onchange="myUploadOnChangeFunction()"
                   class="form-control" id="${actionid}" /></div><div class="form-group">
                <input type="hidden" value="${value}" id="product_file_imageField"/></div>`;
}

function formDisplayImage(textid, actionid, options, theImage) {
    const theText = getTextById(textid);
    return `<div class="form-group"><img src="${theImage}" alt="${theText}"
                   width="300" max-height="200" id="${actionid}" ${options}></div>`;
}

function FormImage(textid, actionid, options, value, theImage) {
    var theText = getTextById(textid);

    return '<div class="form-group">'
        + '<label for="'
        + theText
        + '">'
        + theText
        + '</label>'
        + '<input type="file" name="file" required onchange="myUploadOnChangeFunction('
        + ')" class="form-control" id="' + actionid
        + '" /></div><div class="form-group"><img src="' + theImage
        + '" class="image_' + textid + '" width="350" max-height="175"/><input type="hidden" value="' + value + '" id="product_file_imageField" /></div>';
}

function formThumb(textid, actionid, options, value, theImage) {
    const theText = getTextById(textid);
    return `<div class="form-group"><label for=" + ${theText}">
         ${theText}</label></div><div class="form-group"><img alt="${theText}" 
         longdesc=${theImage} src=${getImagePath()}/${theImage}
         class="image_ ${textid}" width="60" max-height="30" id="shown_thumb_in_${actionid}"/>
         <input type="hidden" value="${value}" id="product_file_thumb"/></div>`;
}

function formRowDiw(rowId, rowWidth) {
    return `<div class="row"><div class="col-md-${rowWidth}" id="${rowId}"></div></div>`;
}

function HorizontalFormButton(textid, actionid, options, actionClass) {
    const theText = getTextById(textid);
    return HorizontalFormButtonStyle.elements[0] + actionClass
        + HorizontalFormButtonStyle.elements[1] + actionid
        + HorizontalFormButtonStyle.elements[2] + options
        + HorizontalFormButtonStyle.elements[3] + theText
        + HorizontalFormButtonStyle.elements[4];
}

function FormButton(textid, actionid, options, actionClass) {
    const theText = getTextById(textid);
    return FormButtonStyle.elements[0] + actionClass
        + FormButtonStyle.elements[1] + actionid
        + FormButtonStyle.elements[2] + options
        + FormButtonStyle.elements[3] + theText
        + FormButtonStyle.elements[4];
}

function formButtonWithIconAndBadge(textid, id, badgecolor, badgeText, icon, actionClass) {
    const theText = getTextById(textid);
    return `<div class="form-group"><a class="${actionClass}" id="${id}">
                <span class="badge ${badgecolor}">${badgeText}</span>
                <h4><i class="${icon}"></i></h4><b>${theText}</b>
              </a></div>`;
}

let breakLines = function (lines) {
    let breakLine = "<br>";
    for (let n = 0; n < lines; n++) breakLine += "<br>";
    return breakLine;
}

function formButtonWithIcon(textid, id, icon, actionClass) {
    const theText = getTextById(textid);
    return `<div class="form-group"><a class="${actionClass}" id="${id}">
                <h4><i class="${icon}"></i></h4><b>${theText}</b>
              </a></div>`;
}

function formNumericInput(textid, actionid, options, value) {
    const theText = getTextById(textid);
    let theValue = '';
    if (value) theValue = value;
    return FormNumericInputStyle.elements[0] + theText
        + FormNumericInputStyle.elements[1] + theText
        + FormNumericInputStyle.elements[2] + actionid
        + FormNumericInputStyle.elements[3] + actionid
        + FormNumericInputStyle.elements[4] + options
        + FormNumericInputStyle.elements[5] + theValue
        + FormNumericInputStyle.elements[6];
}

/*function formNumericInput(textID, actionClassName, options, buttonID) {
    const buttonText = getTextById(textID);
    return `${FormNumericInputStyle.pre}${FormNumericInputStyle.mid1}${buttonID}${FormNumericInputStyle.mid2}${actionClassName}${FormNumericInputStyle.mid3}
    ${buttonText}${FormNumericInputStyle.suf}`;
};*/

function formSelectBox(textID, actionClassName, objectID, opt1, opt2, opt3, opt4, opt5, opt6, opt7, opt8) {
    const labelText = getTextById(textID);
    let html = `${FormSelectBoxStyle.pre}${labelText}${FormSelectBoxStyle.mid1}${actionClassName}${FormSelectBoxStyle.mid2}${objectID}${FormSelectBoxStyle.mid3}
           <option>${opt1}</option>`;
    if (opt2) html += `<option>${opt2}</option>`;
    if (opt3) html += `<option>${opt3}</option>`;
    if (opt4) html += `<option>${opt4}</option>`;
    if (opt5) html += `<option>${opt5}</option>`;
    if (opt6) html += `<option>${opt6}</option>`;
    if (opt7) html += `<option>${opt7}</option>`;
    if (opt8) html += `<option>${opt8}</option>`;
    html += `${FormSelectBoxStyle.suf}`;
    return html;
}

function formSelectBoxArray(textID, actionClassName, objectID, options) {
    const labelText = getTextById(textID);
    let html = `${FormSelectBoxStyle.pre}${labelText}${FormSelectBoxStyle.mid1}${actionClassName}${FormSelectBoxStyle.mid2}${objectID}${FormSelectBoxStyle.mid3}`;
    for (let key in options) {
        //debug("options["+key+"]="+options[key],release);
        html += "<option>" + options[key] + "</option>";
    }
    html += `${FormSelectBoxStyle.suf}`;
    return html;
}

function formHiddenInput(actionClassName, value, ID) {
    return `${FormHiddenInputStyle.pre}${actionClassName}${FormHiddenInputStyle.mid1}${ID}${FormHiddenInputStyle.mid2}${value}${FormHiddenInputStyle.suf}`;
}

function formTitle(titleText) {
    return `${TitleStyle.start}${titleText}${TitleStyle.end}`;
}

function formLabel(labelText, labelID) {
    return `${LabelStyle.start}${labelID}${LabelStyle.mid}${labelText}${LabelStyle.end}`;
}

function FormCheckBox(textid, actionid, options, value) {
    const theText = getTextById(textid);
    if (value === 'true' || value === true) {
        return FormCheckBoxStyle.elements[0] + actionid + '"'
            + options + FormCheckBoxStyle.elements[3]
            + theText
            + FormCheckBoxStyle.elements[2];
    } else {
        return FormCheckBoxStyle.elements[0] + actionid + '"'
            + options
            + FormCheckBoxStyle.elements[1] + theText
            + FormCheckBoxStyle.elements[2];
    }
}

function FormRadioButton(textid, actionid, name, value, options) {
    var theText = getTextById(textid);
    return '<div class="radio"><label><input type="radio" id="' + actionid
        + '" name="' + name + '" value="' + value + ' "' + options
        + '>' + theText + '</label></div>';

}

function FormTextArea(textid, actionid, options, value) {
    const theText = getTextById(textid);
    return FormTextAreaStyle.elements[0] + theText
        + FormTextAreaStyle.elements[1] + theText
        + FormTextAreaStyle.elements[2] + actionid
        + FormTextAreaStyle.elements[3] + actionid
        + FormTextAreaStyle.elements[4] + theText
        + FormTextAreaStyle.elements[5] + options
        + FormTextAreaStyle.elements[6] + value
        + FormTextAreaStyle.elements[7];
}

function formTextInput(textid, actionid, options, value) {
    const theText = getTextById(textid);
    return FormTextInputStyle.elements[0] + theText
        + FormTextInputStyle.elements[1] + theText
        + FormTextInputStyle.elements[2] + actionid
        + FormTextInputStyle.elements[3] + actionid
        + FormTextInputStyle.elements[4] + theText
        + FormTextInputStyle.elements[5] + options
        + FormTextInputStyle.elements[6] + value
        + FormTextInputStyle.elements[7];
}

function formPassWord(textid, actionid, options, value) {
    const theText = getTextById(textid);
    return FormPassWordStyle.elements[0] + theText
        + FormPassWordStyle.elements[1] + theText
        + FormPassWordStyle.elements[2] + actionid
        + FormPassWordStyle.elements[3] + theText
        + FormPassWordStyle.elements[4] + options
        + FormPassWordStyle.elements[5] + value
        + FormPassWordStyle.elements[6];
}

/**
 * const HorizontalFormTextAreaStyle = { elements : [ '<div class=\'col-lg-4\'><textarea
 * class="form-control" id="', '" placeholder="', '" ', ' >', '</textarea></div>' ] };
 * const HorizontalFormTextInputStyle = { elements : [ '<div
 * class=\'col-lg-4\'><input type="text" class="form-control" id="', '"
 * placeholder="', '" value="' ,'" ', ' ',' >', '</div>' ] };
 *
 * @param textid
 * @param actionid
 * @param options
 * @param value
 * @returns {string};
 * @constructor
 */
function HorizontalFormTextArea(textid, actionid, options, value) {
    const theText = getTextById(textid);
    return HorizontalFormTextAreaStyle.elements[0] + actionid
        + HorizontalFormTextAreaStyle.elements[1] + theText
        + HorizontalFormTextAreaStyle.elements[2] + options
        + HorizontalFormTextAreaStyle.elements[3] + value
        + HorizontalFormTextAreaStyle.elements[4];
}

function HorizontalFormTextInput(textid, actionid, options, value) {
    const theText = getTextById(textid);
    return HorizontalFormTextInputStyle.elements[0] + actionid
        + HorizontalFormTextInputStyle.elements[1] + theText
        + HorizontalFormTextInputStyle.elements[2] + value
        + HorizontalFormTextInputStyle.elements[3] + options
        + HorizontalFormTextInputStyle.elements[4];
}

function FormSelectBox(textid, actionid, options, value) {
    const theText = getTextById(textid);
    return '<div class="form-group">' + '<label for="' + theText + '">'
        + theText + '</label>' + '<div id="jstree"></div>'
        + '<input type="hidden" id="product_category" value="' + value + '" /></div>';
}

function cartCol1(arr) {
    return `<span class="badge bg bg-purple">id: ${arr[7]}</span>&nbsp;<span class="badge bg bg-navy">${arr[1]}</span>
              <span class="badge bg bg-purple" id="cartProductCount"${arr[2]}>x ${Number(arr[0]).toFixed(0)}</span>`;
}

function cartCol2(arr) {
    return `<span>${Number(arr[3]).toFixed(2)}&nbsp;${getCurrency()}</span>`;
}

function cartCol3(arr) {
    return `<span><b>${Number(arr[4]).toFixed(2)}&nbsp;${getCurrency()}</b></span>`;
}

function cartLastRow(arr, TOTAL) {
    return `<b>${Number(arr[4]).toFixed(2)} ${getCurrency()}</b><tr><td><h4><b>${TOTAL[1]}</b></h4></td><td><h4><b>
                            <u></u></b></h4></td><td><h4><b>${Number(TOTAL[4]).toFixed(2)} ${getCurrency()}</b></h4></td></tr>`;
}

function myUploadOnChangeFunction() {
    debug(getTextById(264) + document.getElementById('file_imagefield').value, 0);

}

function colouredValue(color, cell) {
    return `<span class="label bg-${color}">${cell}</span>`;
}


function startDiv(classes, id, attributes) {
    if (attributes === '') {
        return `<div class="${classes}" id="${id}">`;
    } else {
        return `<div class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endDiv() {
    return `</div>`;
}


function startSpan(classes, id, attributes) {
    if (attributes === '') {
        return `<span class="${classes}" id="${id}">`;
    } else {
        return `<span class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endSpan() {
    return `</span>`;
}

function startAnchor(classes, id, attributes) {
    if (attributes === '') {
        return `<a href="javascript:void(0);" class="${classes}" id="${id}">`;
    } else {
        return `<a href="javascript:void(0);" class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endAnchor() {
    return `</a>`;
}

function startParagraph(classes, id, attributes) {
    if (attributes === '') {
        return `<p class="${classes}" id="${id}">`;
    } else {
        return `<p class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endParagraph() {
    return `</p>`;
}


function startIcon(classes, id, attributes) {
    if (attributes === '') {
        return `<i class="${classes}" id="${id}">`;
    } else {
        return `<i class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endIcon() {
    return `</i>`;
}

function startStrong(classes, id, attributes) {
    if (attributes === '') {
        return `<strong class="${classes}" id="${id}">`;
    } else {
        return `<strong class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endStrong() {
    return `</strong>`;
}

function startBold(classes, id, attributes) {
    if (attributes === '') {
        return `<b class="${classes}" id="${id}">`;
    } else {
        return `<b class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endBold() {
    return `</b>`;
}

function startInsTag(classes, id, attributes) {
    if (attributes === '') {
        return `<ins class="${classes}" id="${id}">`;
    } else {
        return `<ins class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endInsTag() {
    return `</ins>`;
}

function startSupTag(classes, id, attributes) {
    if (attributes === '') {
        return `<sup class="${classes}" id="${id}">`;
    } else {
        return `<sup class="${classes}" id="${id}" ${attributes}>`;
    }
}

function endSupTag() {
    return `</sup>`;
}

function breakTag() {
    return `<br />`;
}

function hrTag() {
    return `<hr>`;
}


function startHeadingTag(tagNumber, classes, id, attributes) {
    return `<h${tagNumber} class="${classes}" id="${id}" ${attributes}>`;
}

function endHeadingTag(tagNumber) {
    return `</h${tagNumber}>`;
}
