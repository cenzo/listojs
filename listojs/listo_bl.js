/* listo_bl.js  
22.12.2020 13:12:35,46 */ 
/*-----------------------------------*/ 
/* admin.js */ 
let softwareProduct = "Listorante Admin";

module = "admin.js";
release = "_d1a6f";
applicationID = 1;
//setTexts();
getCustomerId();


function setTexts() {

    //setElemText(5);
    setElemText(8);
    setElemText(9);
    setElemText(10);
    setElemText(11);
    setElemText(12);
    setElemText(13);
    setElemText(14);
    setElemText(15);
    setElemText(16);
    setElemText(17);
    setElemText(18);
    setElemText(19);
    setElemText(20);
    /*  setElemText(21);
      setElemText(22);
      setElemText(23);
      setElemText(24);
      setElemText(25);*/
    setElemText(26);
    setElemText(39);
    setElemText(40);
    setElemText(41);
    // setElemText(104);

    setElemText(748);
    const moreInfoText = getTextById(225);
    assignInnerText("moreinfoText_Orders", moreInfoText);
    assignInnerText("moreinfoText_Bills", moreInfoText);
    assignInnerText("moreinfoText_Messages", moreInfoText);
    assignInnerText("moreinfoText_Guests", moreInfoText);
    assignInnerText("ordersText", getTextById(12));
    assignInnerText("billsText", getTextById(9));
    assignInnerText("messagesText", getTextById(13));
    assignInnerText("guestsText", getTextById(8));
    document.getElementById('loading').innerHTML = "";
    console.log("Set load-image to: " + images.logosmall);
}

function assignInnerText(elementName, text) {
    try {
        const element = document.getElementById(elementName);
        if (element) {
            element.innerText = text;
        }
    } catch (err) {
        console.warn(err);
    }
}

function showShoppingPage() {
    getCustomerCredits(function (data) {
        const chargeTable = formRowDiw("chargesTableID", 6);
        getCreditCharges(function (data) {
            const chargesTableObject = {
                IDENTIFIER: "charges-data",
                jsonRMLData: data,
                header: ['#', getTextById(743)],
                columnIndices: [0, 1],
                actions: [],
                dataColumnIndices: [],
                dataColumnNames: []
            };
            listoHTML.createTable.call(chargesTableObject, "chargesTableID", false);
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
        let creditsObj = {};
        creditsObj.amount = "n.n.";
        creditsObj.lastupdate = "n.n.";
        creditsObj.amount = data.rows[0].s[0];
        creditsObj.lastupdate = data.rows[0].s[1];
        const creditsAmount = formLabel(creditsObj.amount, "");
        console.log("Credits retrieved successfully: " + JSON.stringify(data));
        try {
            const buyButton = formButtonWithIconAndBadge(744, 'badgebutton', 'bg-green',
                creditsObj.amount, 'fa fa-battery-3', 'btn btn-app buyCredits');
            const breaks = breakLines(2);
            const creditsLastUpDated = formLabel(creditsObj.lastupdate + ")", "");
            const labelCreditAmount = formLabel(getTextById(742) + "&nbsp;&nbsp;&nbsp;&nbsp;", "");
            const labelCreditLastUpdated = formLabel("&nbsp;&nbsp;&nbsp;(" + getTextById(743) + "&nbsp;&nbsp;&nbsp;", "");
            const buyForm = [labelCreditAmount, creditsAmount, labelCreditLastUpdated, creditsLastUpDated, breaks, buyButton, chargeTable];
            // TODO tbd for suscriptions in next version(s), DO NOT DELETE!
            // const subscriptions = formButtonWithIcon(749, "subscriptionButtonID", "glyphicon glyphicon-certificate", "buySubscription");
            // const endSubscription = formButtonWithIcon(750, "endSubscriptionButtonID", "glyphicon glyphicon-off", "endSubscription");
            // const buyForm = [labelCreditAmount, creditsAmount, labelCreditLastUpdated, creditsLastUpDated, breaks, buyButton, subscriptions, endSubscription, chargeTable];
            listoHTML.buildForm("#main-content", 8, buyForm);
        } catch (err) {
            console.error(JSON.stringify(err));
        }
    }, function (error) {
        const errJsonized = JSON.stringify(error);
        console.error("Credits could not be retrieved : " + errJsonized);
        showHttpErrorMessage("main-content", error);
    });
}

/*
 * 
 * 
 * Admin-Menu/ Function 2/ Common function for all sub-functions: Show existing
 * tables
 */

function showTables() {
    apiCall_listorante_public_showtables(function (data) {

        const action1 = [
            "edit_element_table",
            actionModifyStyle.elements[0] + getTextById(105)
            + actionModifyStyle.elements[1]];
        const action2 = [
            "delete_element_table",
            actionDeleteStyle.elements[0] + getTextById(106)
            + actionDeleteStyle.elements[1]];
        // End of TODO
        const dataColumnNames1 = ["idcustomertable", "places", "isseat",
            "description", "description_locale"];
        const dataColumnIndices1 = [0, 1, 2, 3, 4];
        const dataColumnNames2 = ["idcustomertable"];
        const dataColumnIndices2 = [0];
        const tableObject = {
            IDENTIFIER: "admin-showTables",
            jsonRMLData: data,
            header: [getTextById(43), getTextById(42),
                getTextById(45), "", ""],
            columnIndices: [4, 3, 1],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1,
                dataColumnIndices2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            rowFormat: function (rowIndex, sArray) {
                const transform = [];
                // transform = sArray;

                if (sArray[1] < 3) {
                    for (let l = 0; l < sArray.length; l++)
                        transform[l] = DataStyle.evidence2Open
                            + sArray[l]
                            + DataStyle.evidence2Close;
                    //debug("sArray[1]<3:", release, sArray,
                    //  transform);
                    return transform;
                } else if (rowIndex === 0) {
                    for (let l = 0; l < sArray.length; l++)
                        transform[l] = DataStyle.evidence3Open
                            + sArray[l]
                            + DataStyle.evidence3Close;
                    //debug("rowindex 0 or 1:", release, sArray,
                    //transform);
                    return transform;
                } else {
                    return sArray;
                }
            },
            /*cellFormat: function (columnIndex, rowIndex, TDopen, data,
                                  TDclose) {
                if (columnIndex === 2 && data > 0) {
                    return TDopen + DataStyle.evidence1Open + data
                        + DataStyle.evidence1Close + TDclose;
                } else {
                    return TDopen + data + TDclose;
                }
            },*/
        };
        listoHTML.createTable.call(tableObject, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

$(document).on("click", ".table-list", function () {
    showTables();
})

/*
 * Admin-Menu/ Function 1 / Dashboard
 * 
 * 
 */

// 1. Table Overview for dashboard
$(document)
    .on(
        'click',
        '.table-overview',
        function () {
            apiCall_listorante_admin_admindash(function (data) { // If success
                const tableOverviewTableObject = {
                    IDENTIFIER: "admin-dash-overview-table",
                    jsonRMLData: data[2],
                    header: [getTextById(28), getTextById(63),
                        getTextById(97), getTextById(64),
                        getTextById(65), getTextById(66), getTextById(67),
                        getTextById(612), getTextById(613)],
                    columnIndices: [1, 3, 4, 5, 7, 10, 11, 12, 13],
                    actions: [],
                    dataColumnIndices: [],
                    dataColumnNames: [],
                    cellFormat: function (c, r, o, cell, cl) {
                        let currency;
                        if (c === 4) currency = getCurrency();
                        else currency = "";
                        if (c === 0) return `${o}${cell}${cl}`;
                        else if (c === 3 || c === 4 || c === 8) return `${o}${Number(cell).toFixed(2)}${currency}${cl}`;
                        else return `${o}${parseInt(cell, 10)}${cl}`;
                    }
                };
                listoHTML.createTable.call(
                    tableOverviewTableObject, "main-content");
            }, function (err) { // If failed
                showHttpErrorMessage("main-content", err);
            });
        });

// 2. Kitchen Overview for preparing items
$(document)
    .on(
        'click',
        '.kitchen-overview',
        function () {
            $("#main-content").html("");
            $("#main-content").html(images.loading);
            apiCall_listorante_admin_orders(
                function (data) { // If success to get data
                    // Create table Object
                    const tableObject = {
                        actions: [],
                        IDENTIFIER: "admin-itemtable",
                        jsonRMLData: data,
                        header: [getTextById(68),
                            getTextById(69), getTextById(36),
                            getTextById(50), getTextById(70),
                            getTextById(71), getTextById(72),
                            getTextById(73), getTextById(74)],
                        columnIndices: [1, 2, 4, 5, 6, 8, 10, 11,
                            12],
                        dataColumnIndices: [],
                        dataColumnNames: [],
                        rowFormat: function (rowIndex, sArray) {
                            const status = sArray[8];
                            if (sArray[9] === "1") {
                                sArray[8] = DataStyle.statusOrderedStartTag
                                    + " id='orderStatus"
                                    + sArray[0]
                                    + "'>"
                                    + status
                                    + DataStyle.statusOrderedEndTag;
                            } else if (sArray[9] === "2") {
                                sArray[8] = DataStyle.statusPreparationOpenTag
                                    + " id='orderStatus"
                                    + sArray[0]
                                    + "'>"
                                    + status
                                    + DataStyle.statusPreparationEndTag;
                            } else if (sArray[9] === "3") {
                                sArray[8] = DataStyle.statusDeliveredStartTag
                                    + " id='orderStatus"
                                    + sArray[0]
                                    + "'>"
                                    + status
                                    + DataStyle.statusDeliveredEndTag;
                            }
                            return sArray;
                        }
                    };
                    listoHTML.createTable.call(tableObject,
                        "main-content");

                }, function (err) { // If failed to get data
                    showHttpErrorMessage("main-content", err);
                });
        });

// 3. Bills Overview for preparing items
$(document).on(
    'click',
    '.bills-overview',
    function () {
        apiCall_listorante_admin_admindash(function (data) { // If success to
            // get data
            // Create new action Objects
            const action1 = ["viewCart", '<i class="fa fa-search"></i>'];
            //console.log(data);
            // Create table Object
            const dataColumnNames1 = ["idcustomertable", "description_en", "totalbills", "minbillstatus", "maxbillstatus", "mintimestamp", "maxtimestamp", "idbill"];
            const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 2];

            const billsTableObject = {
                IDENTIFIER: "admin-dash-bills-table",
                jsonRMLData: data[0],
                header: [getTextById(68), getTextById(93),
                    getTextById(95), getTextById(35),
                    getTextById(70), getTextById(36),
                    getTextById(97), getTextById(98), ""],
                columnIndices: [2, 0, 3, 4, 5, 6, 8, 9],
                actions: [action1],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1]
            };

            listoHTML.createTable.call(billsTableObject,
                "main-content");

        }, function (err) { // If failed to get data
            showHttpErrorMessage("main-content", err);
        });
    })

/* SHOW BILL OVERVIEW ITEMS */

$(document).on('click', '.viewCart', function () {

    const billnumber = $(this).data('idbill');
    apiCall_listorante_admin_showcart(billnumber, function (data) {
        //console.log(data);
        update2_Cart_Admin(data);
    }, function (err) {
        showHttpErrorMessage(err);
    });

});


let update2_Cart_Admin = function (data) {
    let rowCount = 0;
    let TOTAL;
    let sArr2;
    sArr2 = [];
    const cartObject = {
        actions: [],
        dataColumns: [],
        dataColumnNames: [],
        dataColumnIndices: [],
        IDENTIFIER: 'waiter-Box2-table',
        jsonRMLData: data,
        header: ['', '', ''],
        columnIndices: [0, 1, 2, 3, 4],
        rowFormat: function (rIdx, sArr) {
            rowCount++;
            if (sArr[1] !== 'TOTAL') {
                if (rowCount == parseInt(data.count, 10)) {
                    sArr2[0] = cartCol1(sArr);
                    sArr2[1] = cartCol2(sArr);
                    sArr2[2] = cartLastRow(sArr, TOTAL);
                } else {
                    sArr2[0] = cartCol1(sArr);
                    sArr2[1] = cartCol2(sArr);
                    sArr2[2] = cartCol3(sArr);
                }
                if (document.getElementById('box1ProdCount' + sArr[2])) {
                    $("#box1ProdCount" + sArr[2]).html(parseInt(sArr[0]) + " x");
                }
                return sArr2;
            } else if (sArr[1] === 'TOTAL') {
                TOTAL = sArr;
            }
        },
        cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
            if (colIdx < 3) {
                return `${TDo}${cellData}${TDc}`;
            } else {
                return ``;
            }
        }
    };
    listoHTML.createTable.call(cartObject, 'main-content', true);
};


/*
 * Admin-Menu/ Function 2 / Add new restaurant-table
 * 
 * 
 */
// 1. Create form with editable data
$(document)
    .on(
        'click',
        '.add-new-table',
        function () {
            //const description = [FormTextArea, 42,
            //  "table_add_description", "rows='1' required "];
            const description = formTextInput(42, "table_add_description", "rows='1' required", "");
            //const description_locale = [FormTextArea, 43,
            //  "table_add_description_locale", "rows=2 required "];
            const description_locale = formTextInput(43, "table_add_description_locale", "rows='2' required", "");
            const places = formNumericInput(45, "table_add_places", "rows='1' required", "");
            //const places = [FormTextArea, 45, "table_add_places",
            //  'cols="4" rows="1"'];
            //const isseat = [FormCheckBox, 44, "table_add_isseat", ""];
            //const submitButton = [FormButton, 40, "add_table", "",
            //  "add-table"];
            //const isseat = FormCheckBox(44, "table_add_isseat","","");
            const submitButton = FormButton(40, "add_table", "", "add-table");
            // forms: array of objects
            const form1 = [description, description_locale, places,
                //isseat,
                submitButton];
            listoHTML.buildForm("#main-content", 6, form1);
        })

// 2. Submit the form with the data of the new restaurant table
$(document).on(
    'click',
    '#add_table',
    function () {
        // TODOs 1: never use String literals! Use getTextById(<n>) or
        // getText(<mnemonic>) instead!
        // If no suitable textid exists, create one in localizedText.js
        // for your
        // language

        // TODOs 2: Do never use alert, but implement a modal instead

        // TODOs 3: Use best-practice equality comparator (In this case,
        // probably '===')
        // or implement a best-practice validation-approach
        const description = $("#table_add_description").val();
        const description_locale = $("#table_add_description_locale").val();
        let places = $("#table_add_places").val();

        if (description == "" || description_locale == "" || places == "") {
            alert(getTextById(251));
            return false;
        }

        if (!$.isNumeric(places)) {
            alert(getTextById(252));
            return false;
        }else if(places>50){
            alert(getTextById(252)+"(>50)");
            places=50;
        }

        // let isseat = 0;
        // if ($("#table_add_isseat").is(":checked")) {
        //     isseat = 1;
        // }
        apiCall_listorante_admin_tableinsert(description,
            description_locale, 0, places, function (data) {
                const release = RELEASE;
                if (data._rc > 0) {
                    //alert(getTextById(253) + data._rc);
                    document.getElementById("main-content").innerText = getTextById(253);
                } else {
                    for (let i = 1; i <= parseInt(places); i++) {
                        let username = description_locale.replace(/[^a-zA-Z]/g, "") + i;
                        debug("Try to create user '" + username + "'...:", release);
                        apiCall_listorante_admin_newstaff("nomail@here", "" + getTextById(76), 1, 0, "" + getTextById(102),
                            "public", username, function (data) {
                                debug2(module, "data of new staff", release, [data]);
                                if (data._rc > 0) {
                                    const respTxt = "Anonymous customer " + i + " for table '" + description + "' could not be created. ";
                                    debug("" + respTxt, "" + release);
                                    data.status = data._rc;
                                    data.responseText = respTxt;
                                    showHttpErrorMessage("main-content", data);
                                } else {
                                    debug("Anonymous customer " + i + " for table '" + description + "' was created. Username: " + username, release);
                                    let userId;
                                    try {
                                        userId = data[1].rows[0].s[0];
                                    } catch (err) {
                                        if (!userId) {
                                            debug("" + getTextById(254), "" + release);
                                            let errData = {};
                                            errData.status = 1400;
                                            errData.responseText = getTextById(254);
                                            showHttpErrorMessage("main-content", errData);
                                        } else {
                                            showHttpErrorMessage("main-content", err);
                                        }
                                        return false;
                                    }
                                    apiCall_listorante_admin_editprofile("nomail@here", "" + getTextById(76), userId,
                                        images.missing_image, 1, 0, "" + getTextById(102), username, function (data) {
                                            if (data._rc > 0) {
                                                {
                                                    const respTxt = "Anonymous customer " + i + " for table '" + description + "' was created, but could not be edited. ";
                                                    debug("" + respTxt, "" + release);
                                                    data.status = data._rc;
                                                    data.responseText = respTxt;
                                                    showHttpErrorMessage("main-content", data);
                                                }
                                            } else {
                                                debug("Anonymous customer " + i + " for table '" + description + "' was edited. Username: " + username, release);
                                                apiCall_listorante_admin_assignrole(5, userId, function (data) {
                                                    if (data._rc > 0) {
                                                        {
                                                            const respTxt = "Anonymous customer " + i + " for table '" + description + "' was created and edited, but role could not be assigned. ";
                                                            debug("" + respTxt, "" + release);
                                                            data.status = data._rc;
                                                            data.responseText = respTxt;
                                                            showHttpErrorMessage("main-content", data);
                                                        }
                                                    }
                                                }, function (err) {
                                                    showHttpErrorMessage("main-content", err);
                                                });
                                            }
                                        }, function (err) {
                                            showHttpErrorMessage("main-content", err);
                                        });
                                }
                            }, function (err) {
                                showHttpErrorMessage("main-content", err);
                            });
                    }
                    showTables();
                }
            }, function err() {
                showHttpErrorMessage("main-content", err);
            });
    });

// End of Admin-Menu/ Function 2 / Add new restaurant-table

/*
 * Admin-Menu/ Function 2 / Edit data of restaurant-table
 * 
 * 
 */
// 1. Create form with editable data
$(document)
    .on(
        'click',
        '.edit_element_table',
        function () {
            debug("Edit elements:  description="
                + $(this).data("description"), release);
            const table_id = $(this).data("idcustomertable");
            const description = FormTextArea(42,
                "table_edit_description", "rows='1' required ",
                $(this).data("description"));
            const description_locale = formTextInput(43,
                "table_edit_description_locale",
                "rows=1 required ",
                $(this).data("description_locale"));
            const places = formNumericInput(45, "table_edit_places",
                'cols="4" rows="1" required', $(this).data("places"));
            //const isSeat = FormCheckBox(44, "table_edit_isseat", "",
            //  $(this).data("isseat"));
            const submitButton = FormButton(215, "",
                "data-idcustomertable=\"" + table_id + "\"",
                "edit_table");
            const form1 = [description, description_locale, places,
                submitButton];
            // TODO following approach can be used later for adding customer-accounts
            //const newPassValue=makeid(6);
            //const newPass = formTextInput( 250,
            //  "table_edit_newPass",
            //"rows=1 required ",
            //newPassValue);
            //const form1 = [description, description_locale, places,
            //  isSeat, newPass, submitButton];
            listoHTML.buildForm("#main-content", 6, form1);
        })

// 2. Delete a restaurant-table directly from within the shown table-list
$(document).on('click', '.delete_element_table', function () {

    const idcustomertable = $(this).data("idcustomertable");
    if (idcustomertable == "") {
        debug("" + getTextById(254), "" + release);
        let errData = {};
        errData.status = 1400;
        errData.responseText = getTextById(254);
        showHttpErrorMessage("main-content", errData);
        return false;
    }
    apiCall_listorante_admin_tabledelete(idcustomertable, function (data) {
        const release = RELEASE;
        if (data._rc > 0) {
            {
                const respTxt = "Table with id '" + idcustomertable + "' could not be deleted. ";
                debug("" + respTxt, "" + release);
                data.status = data._rc;
                data.responseText = respTxt;
                showHttpErrorMessage("main-content", data);
            }
        } else {
            showTables();
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});

// 3. Edit restaurant-table-data with the form and submit
$(document)
    .on(
        'click',
        '.edit_table',
        function () {
            const release = RELEASE;
            //console.log(".edit-table");
            const idcustomertable = $(this).data("idcustomertable");
            const description = document
                .getElementById("table_edit_description").value;
            const description_locale = document
                .getElementById("table_edit_description_locale").value;
            let places = document.getElementById("table_edit_places").value;
            if (idcustomertable == "") {
                alert(getTextById(254));
                return false;
            }

            if (description == "" || description_locale == ""
                || places == "") {
                alert(getTextById(251));
                return false;
            }
            if (!$.isNumeric(places)) {
                alert(getTextById(252));
                return false;
            }
            let isseat = 0;
            if ($("#table_edit_isseat").is(":checked")) {
                isseat = 1;
                places = 1;
                $("#table_edit_places").value = 1;
            }
            debug("CHECK", release, description, description_locale,
                idcustomertable, isseat, places);
            apiCall_listorante_admin_tableedit(description,
                description_locale, idcustomertable, isseat, places,
                function (data) {
                    if (data._rc > 0) {
                        {
                            const respTxt = "Data of  table '" + description + "' could not be edited. ";
                            debug("" + respTxt, "" + release);
                            data.status = data._rc;
                            data.responseText = respTxt;
                            showHttpErrorMessage("main-content", data);
                        }
                    }
                    showTables();
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });
        });

$(document).on('click', '.shopping', function () {
    showShoppingPage();
});


$(document).on('click', '.buyCredits', function () {
    const currentUrl = window.location.href;
    sessionStorage.setItem("payment_callingPage", currentUrl);
    setCustomerId();
    window.open("buy.html?cust=" + customerID, "_self");
});

$(document).on('click', '.show-Info', function () {
    var softwareInfo = showInfo();
    //const supportButton = [FormButton, 330, "support_button", "", "support-button"];
    const form1 = [];
    listoHTML.createForm("#main-content", 6, form1, "supportButtonForm");
    $('#main-content .box-primary').prepend(softwareInfo);
});

$(document).on('click', '.fadeIn-languageList', function languageListFadeIn() {
    $("#languageList").fadeIn(1000);
});



/*-----------------------------------*/ 
/* admin_category.js */ 
module = "admin_category.js";


//  SHOW CATEGORIES TABLE
function showcategories() {
    const action1 = ["create-editcategory-form", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
    const action2 = ["delete-category", actionDeleteStyle.elements[0] + getTextById(106) + actionDeleteStyle.elements[1]];
    const dataColumnNames1 = ["category", "idproductcategory", "prodcatdesc", "prodcatname",
        "prodcatimage", "level", "position", "hierarchy_1", "hierarchy_2", "hierarchy_3", "prodcatname_locale", "idcatstyle"];
    const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12];
    const dataColumnNames2 = ["idproductcategory"];
    const dataColumnIndices2 = [1];
    const filterArray = [];
    apiCall_listorante_public_categories(function (data) {
        const tData = data;
        for (let i = 0; i < tData.count; i++) {
            if (jQuery.inArray(tData.rows[i].s[1], filterArray) !== -1) {
                delete data.rows[i];
            } else {
                filterArray.push(tData.rows[i].s[1]);
            }
        }
        const admin = {
            IDENTIFIER: "admin-edit-category",
            jsonRMLData: data,
            header: [getTextById(229), getTextById(228), "ID", getTextById(227), getTextById(230), getTextById(310), "", ""],
            columnIndices: [11, 3, 1, 2, 4, 10],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (colIndex === 4) {
                        if (!data) data = images.missing_image_txt;
                        return TDopen + "<img src='" + getImagePath() + "/" + data
                            + "' " + ThumbStyle.default + ">" + TDclose;
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(admin, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    })

};


function setParentCategory(parent_ID) {
    document.getElementById("parentCategoryId").innerHTML = parent_ID;
    debug('checked parent:', 0, parent_ID);
};

function unSetParentCategory(IDcategory) {
    document.getElementById('parentCategoryId').innerHTML = '';
    debug('un-checked parent:', 0, IDcategory);
};


function linkcategory(category, parent_category) {
    apiCall_listorante_admin_linkcategory(category, parent_category, function (data) {
        if (data._rc > 0) {
            alert(getTextById(260));
            debug("Could not assign category", release, data);
        } else {
            debug("linkcategory", release, data);
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
        debug("Error: could not assign category", 0, err);
        alert(getTextById(260));
    });
}


// 1. Create editable form with "add-category"-event
$(document)
    .on(
        'click',
        '.create-addcategory-form',
        function () {
            const idproductcategory = [FormSelectBox, 53, "category_add_idproductcategory", "rows='1' required ", ""];
            const prodcatdesc_locale = [FormTextArea, 227, "category_add_prodcatdesc_locale", "rows=2 required ", ""];
            const prodcatname = [formTextInput, 228, "category_add_prodcatname", "rows=\"1\" required style=\"text-transform:uppercase\"", ""];
            const prodcatname_locale = [formTextInput, 229, "category_add_prodcatname_locale", "rows=\"1\" required ", ""];
            const prodcatimage = [formImage, 230, "file_imagefield", "required", "", ""];
            const submitButton = [FormButton, 215, "add_category", "", "add-category"];
            const form1 = [prodcatname_locale, prodcatname, idproductcategory, prodcatdesc_locale, prodcatimage, submitButton];
            listoHTML.createForm("#main-content", 8, form1, "add-category-form");
            const categoriesData = [];
            const dupArray = [];
            apiCall_listorante_public_categories(function (response) {
                // sessionStorage.setItem('uniqueID', '');
                const tree_root = {
                    id: "root_0",
                    parent: "#",
                    text: "*",
                    state: {
                        "opened": true, "checked": false, "selected": false
                    }
                }
                categoriesData.push(tree_root);
                for (let i = 0; i < response.count; i++) {
                    const billdata = {};

                    /*if (sessionStorage.getItem('uniqueID') == response.rows[i].s[1]) {
                        if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                            billdata.id = i + 'rep_' + response.rows[i].s[1];
                            dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                        } else {
                            billdata.id = 'rep_' + response.rows[i].s[1];
                        }
                        dupArray.push(response.rows[i].s[1]);
                    } else {
                        billdata.id = 'cat_' + response.rows[i].s[1];
                    }*/
                    billdata.id = 'cat_' + response.rows[i].s[1];
                    if (response.rows[i].s[0] == 0) {
                        billdata.parent = 'root_0';
                    } else {
                        if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                            billdata.parent = 'rep_' + response.rows[i].s[0];
                        } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                            billdata.parent = i + 'rep_' + response.rows[i].s[0];
                        } else {
                            billdata.parent = 'cat_' + response.rows[i].s[0];
                        }
                    }
                    billdata.text = response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
                    debug("response.rows[" + i + "].s: " + response.rows[i].s);
                    billdata.state = {
                        "opened": true, "checked": false, "selected": false
                    };
                    categoriesData.push(billdata);
                    //sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
                }
                $('#jstree').jstree({
                    'core': {
                        'multiple': false,
                        'data': categoriesData,
                        'check_callback': false
                    }, types: {
                        "root": {
                            "icon": "glyphicon glyphicon-record"
                        },
                        "child": {
                            "icon": "glyphicon glyphicon-minus"
                        },
                        "default": {
                            "icon": "glyphicon glyphicon-cutlery"
                        }
                    },
                    "checkbox": {
                        'deselect_all': true,
                        "three_state": false
                    },
                    plugins: ["types", "data", "checkbox"]
                }).on('open_node.jstree', function (e, data) {
                    data.instance.set_icon(data.node, "glyphicon glyphicon-minus");
                }).on('close_node.jstree', function (e, data) {
                    data.instance.set_icon(data.node, "glyphicon glyphicon-cutlery");
                }).on('loaded.jstree', function () {
                    $("#jstree").jstree().select_node('cat_0');
                    $("#jstree").jstree().check_node('cat_0');
                    $('#product_category').val('0');
                }).on("select_node.jstree unselect_node.jstree", function (e, data) {
                    const cNode = "" + data.node.id;
                    const underlineIdx = cNode.indexOf("_") + 1;
                    const idcategory = cNode.substring(underlineIdx, cNode.length);
                    $('#product_category').val(idcategory);
                    (data.node.state.selected ? setParentCategory(idcategory) : unSetParentCategory(idcategory))
                }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
                    const cNode = "" + data.node.id;
                    const underlineIdx = cNode.indexOf("_") + 1;
                    const idcategory = cNode.substring(underlineIdx, cNode.length);
                    $('#product_category').val(idcategory);
                    debug('data.node.state:', 0, data.node.state);
                    (data.node.state.checked ? setParentCategory(idcategory) : unSetParentCategory(idcategory));
                });
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        });

// 2. Add new category by submitting the form
$(document).on('click', '.add-category', function () {
    if ($("#add-category-form").valid()) {
        const idproductcategory = document.getElementById('parentCategoryId').innerHTML;
        if (parseInt(idproductcategory, 10) >= 0) {
            const prodcatdesc_locale = $('#category_add_prodcatdesc_locale').val();
            const prodcatname = $('#category_add_prodcatname').val().toUpperCase().replace(/[^A-Z0-9]/gmi, "_");
            const prodcatname_locale = $('#category_add_prodcatname_locale').val();
            const custID = getCustomerId();
            if (document.getElementById('file_imagefield').value) {
                upload(custID, 'add-category-form', function (data) {
                    debug("upload-data:", release, data);
                    const imageNew = "" + data.rows[0].s[2];
                    const image_thumbNew = imageNew;
                    alert(getTextById(255) + document.getElementById('file_imagefield').value +
                        getTextById(256) + imageNew + "'");
                    apiCall_listorante_admin_newcategory('0', '0', '0', '0',
                        '0', idproductcategory, -1, -1,
                        prodcatdesc_locale, prodcatdesc_locale, imageNew, prodcatname, prodcatname_locale, function (response) {
                            debug('response from new category:', 0, response);
                            showcategories();
                        }, function (err) {
                            showHttpErrorMessage("main-content", err);
                        });
                }, function (error) {
                    alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
                });
            } else {
                const noImage = images.missing_image_txt;
                apiCall_listorante_admin_newcategory('0', '0', '0', '0', '0',
                    idproductcategory, -1, -1, prodcatdesc_locale,
                    prodcatdesc_locale, noImage, prodcatname, prodcatname_locale, function (response) {
                        debug('response from new category:', 0, response);
                    }, function (err) {
                        showHttpErrorMessage("main-content", err);
                    });
            }
            document.getElementById('parentCategoryId').innerHTML = '';
        } else {
            alert("Please select a parent category.");
            return;
        }
    } else {
        debug('Not Valid');
        return;
    }

});

/*
 * Admin-Menu / Edit Category
 *
 *
 */
// 1. Select categories-data and create table
$(document).on(
    'click',
    '.edit-category',
    function () {
        showcategories();
    }
);

// 2. Create editable form to change one single category
$(document).on(
    'click',
    '.create-editcategory-form',
    function () {
        release = RELEASE;
        debug("this.data:", release, $(this).data());
        const idCatStyle = $(this).data("idcatstyle");
        const category = $(this).data("category");
        const idproductcategory = [FormSelectBox, 53, "category_add_idcategory", "rows='1' required ", category];
        const idproductcategoryID = $(this).data("idproductcategory");
        const prodcatdesc = $(this).data("prodcatdesc");

        debug("create Edit category form", release, category, idproductcategoryID);
        if ($(this).data("prodcatimage")) {
            var prodcatimage = [formImage, 230, "file_imagefield", "required", $(this).data("prodcatimage"),
                $(this).data("prodcatimage")];
        } else {
            var prodcatimage = [formImage, 230, "file_imagefield", "required", "", ""];
        }
        const uploadButton = [FormButton, 17, "file_imagefield", "", "file_imagefield"];
        const prodcatdesc_locale = [FormTextArea, 227, "category_add_prodcatdesc_locale", "rows=2 required ", prodcatdesc];
        const prodcatname = [formTextInput, 228, "category_add_prodcatname", 'rows="1" required style="text-transform: uppercase"',
            $(this).data("prodcatname")];
        const idStyle = [formNumericInput, 262, "product_edit_style", "required step='1'", idCatStyle];
        //const idStyle = formNumericInput(262,"product_edit_style", "required step='1'",idCatStyle );

        const prodcatname_locale = [formTextInput, 229, "category_add_prodcatname_locale", "rows=\"1\" required",
            $(this).data("prodcatname_locale")];
        const submitButton = [FormButton, 215, "update_category", "", "update-category"];
        const hidden_category_id = [formHiddenInput, "", idproductcategoryID, "id_of_edited_category"];

        const productsDiv = [formRowDiw, "productsBox", 10];

        debug("productsDiv", 0, productsDiv);


        const form1 = [prodcatname_locale, prodcatname, idStyle, idproductcategory, prodcatdesc_locale, prodcatimage,
            hidden_category_id, submitButton, productsDiv];
        const productListOfCategory = [];
        apiCall_listorante_public_product(idproductcategoryID, function (data) {
            for (let i = 0; i < data.count; i++) {
                productListOfCategory.push(data.rows[i].s[0]);
            }
            debug("productListOfCategory", 0, productListOfCategory);
            apiCall_listorante_public_products(function (products) {
                const action_edit = ["edit_element_product", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
                const dataColumnNames1 = ["idproduct", "idlabel", "prodname_en", "image_thumb", "prodname", "price",
                    "image",
                    "prodinfo_en", "proddesc_en", "position",
                    "qty_in_stock", "qty_unit", "threshold_qty"];
                const dataColumnIndices1 = [0, 12, 5, 4, 1, 2, 3, 6, 7, 8, 9, 10, 11];

                const products_of_category = {
                    IDENTIFIER: "admin-edit-products_of_category",
                    jsonRMLData: products,
                    header: [getTextById(71), getTextById(733), getTextById(205), getTextById(61), "", ""],
                    columnIndices: [12, 5, 4],
                    actions: [action_edit],
                    dataColumnIndices: [dataColumnIndices1],
                    dataColumnNames: [dataColumnNames1],
                    rowFormat: function (rowIndex, dataArray) {
                        return dataArray;
                    },
                    cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                        {
                            if (colIndex === 0) {
                                if (productListOfCategory.includes(products.rows[rowIndex].s[0])) {
                                    return TDopen + "*" + TDclose + TDopen + FormCheckBox(105, "checkProd" + products.rows[rowIndex].s[0], "class=checkProduct data-product='" + products.rows[rowIndex].s[0] + "' data-category='" + idproductcategoryID + "'", true) + TDclose + TDclose + TDopen + data + TDclose;
                                } else
                                    return TDopen + "" + TDclose + TDopen + FormCheckBox(105, "checkProd" + products.rows[rowIndex].s[0], "class=checkProduct data-product='" + products.rows[rowIndex].s[0] + "' data-category='" + idproductcategoryID + "'", false) + TDclose + TDclose + TDopen + data + TDclose
                            }
                            if (colIndex === 2) return TDopen + "<img src='" + getImagePath() + "/" + data + "' width='35'>" + TDclose;
                            else return TDopen + data + TDclose
                        }
                    }
                };
                listoHTML.createTable.call(products_of_category, "productsBox", false);
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });

        listoHTML.createForm("#main-content", 8, form1, 'category-edit-form');
        const categoriesData = [];
        const dupArray = [];
        apiCall_listorante_public_categories(function (response) {
            // sessionStorage.setItem('uniqueID', '');
            const tree_root = {
                id: "0",
                parent: "#",
                text: "",
                icon:"glyphicon glyphicon-cutlery",
                state: {
                    opened: false, checked: false, selected: false
                }
            }
            categoriesData.push(tree_root);
            for (let i = 0; i < response.count; i++) {
                const billdata = {};
                /* if (sessionStorage.getItem('uniqueID') == response.rows[i].s[3]) {
                     if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                         billdata.id = i + 'rep_' + response.rows[i].s[1];
                         dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                     } else {
                         billdata.id = 'rep_' + response.rows[i].s[1];
                     }
                     dupArray.push(response.rows[i].s[1]);
                 } else {
                     billdata.id = 'cat_' + response.rows[i].s[1];
                 }*/
                //billdata.id = 'cat_' + response.rows[i].s[1];
                billdata.id = response.rows[i].s[1];
                billdata.parent=response.rows[i].s[0];
                /*if (response.rows[i].s[0] == 0) {
                    billdata.parent = 'root_0';
                } else {
                    if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = 'rep_' + response.rows[i].s[0];
                    } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = i + 'rep_' + response.rows[i].s[0];
                    } else {
                        billdata.parent = 'cat_' + response.rows[i].s[0];
                    }
                }*/
                billdata.text = response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
                //if (billdata.id === 'cat_' + idproductcategoryID || billdata.id === 'rep_' + idproductcategoryID || billdata.id === i + 'rep_' + idproductcategoryID)
               //  {
                     if (parseInt(response.rows[i].s[1], 10) === parseInt(idproductcategoryID, 10)) {

                         const release = RELEASE;
                    const nodeState = {"disabled": true, "opened": true, "checked": false, "selected": false};
                    billdata.state = nodeState;
                    //$("#"+billdata.id+"_anchor >a").css("color","red");
                    //console.log("Try to set color of " + "#" + billdata.id + "_anchor to red");
                  //  console.log("parent of category " + billdata.id + "=" + billdata.parent);
                    debug2(module, ">>>>> disable category ", release, [idproductcategoryID, billdata.parent, billdata]);
                }
                /*if (category === billdata.parent) {
                    // I think this never occurs...
                    const nodeState = {"opened": true, "checked": true, "selected": true};
                    billdata.state = nodeState;
                    console.log(" ---- WHERE ARE WE??? ");
                    debug2(module, " ---- WHERE ARE WE??? ", release, [category, response.rows[i].s[1], billdata]);
                }*/
                else if (parseInt(response.rows[i].s[1], 10) === parseInt(category,10)) {
                    const nodeState = {"opened": true, "checked": true, "selected": true};
                    document.getElementById("parentCategoryId").innerText = response.rows[i].s[1];
                    billdata.state = nodeState;
                    billdata.text="***** "+billdata.text+" ***** ";
                    billdata.icon="glyphicon glyphicon-flag";
                    debug2(module, ">>>>> Set checkbox for parent of category", release, [idproductcategoryID, response.rows[i].s[1], billdata]);
                } else {
                    try {
                        const nodeState = {"opened": false, "checked": false, "selected": false};
                        billdata.data.state = nodeState;
                     //   debug2(module, "Nodestates", release, [billdata, billdata.state]);
                    } catch (err) {
                        //  debug("Error while trying to get nodeState",release,err);
                    }
                }
                categoriesData.push(billdata);
                //sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
            }
            //for (let n=0;n<categoriesData.length;n++){
             //   categoriesData[n].state={opened:false};
           // }
            $('#jstree').jstree({
                'core': {
                    'multiple': false,
                    'data': categoriesData,
                    'check_callback': false
                }, types: {
                    "root": {
                        "icon": "glyphicon glyphicon-cutlery"
                    },
                    "child": {
                        "icon": "glyphicon glyphicon-record"
                    },
                    "default": {
                        "icon": "glyphicon glyphicon-minus"
                    }
                },
                "checkbox": {
                    "deselect_all": true,
                    "three_state": false
                },
                plugins: ["types", "data", "checkbox", "state"]
            }).on('open_node.jstree', function (e, data) {
               if(data.node.id!="0") data.instance.set_icon(data.node, "glyphicon glyphicon-record");
               console.log("open:\n"+JSON.stringify(data.node));
            }).on('close_node.jstree', function (e, data) {
                if(data.node.id!="0") data.instance.set_icon(data.node, "glyphicon glyphicon-asterisk");
            }).on("select_node.jstree unselect_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                //$('#product_category').val(idcategory);
                (data.node.state.selected ? setParentCategory(idcategory) : unSetParentCategory(idcategory));
            }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                // debug("underlineIdx,idcategory/cNode/node.id:",release,underlineIdx,idcategory,cNode,data.node.id);
                (data.node.state.checked ? setParentCategory(idcategory) : unSetParentCategory(idcategory))
            });
            console.log(JSON.stringify(categoriesData));
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
    });

// Edit product from edit category-form:
$(document).on(
    'click',
    '.open-editproduct-form',
    function () {
        const prod = $(this).data();
        debug("open product edit for:" + prod, 0, prod);
    });

// 3 EDIT CATEGORY FORM SUBMISSION
$(document).on('click', '.update-category', function () {
    const idproductcategory = $('#product_category').val();
    const prodcatdesc_locale = $('#category_add_prodcatdesc_locale').val();
    const category_idchildcategory = $('#id_of_edited_category').val();
    const prodcatname = $('#category_add_prodcatname').val().toUpperCase().replace(/[^A-Z0-9]/gmi, "_");
    const prodcatname_locale = $('#category_add_prodcatname_locale').val();
    const prodcatimage = $('#product_file_imageField').val();
    const idStyle = $('#product_edit_style').val();
    debug('prodcatname--' + prodcatname, 0);
    debug("update-category: idproductcategory/childcategory", 0, idproductcategory, category_idchildcategory);
    const custID = getCustomerId();
    if ($("#category-edit-form").valid()) {
        if (document.getElementById('file_imagefield').value) {
            upload(custID, 'category-edit-form', function (data) {
                const imageNew = "" + data.rows[0].s[2];
                debug(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'", "" + release);
                apiCall_listorante_admin_editcategory('0', '0', '0', '0', '0',
                    category_idchildcategory, idStyle, '0', prodcatdesc_locale, prodcatdesc_locale, imageNew, prodcatname, prodcatname_locale,
                    function (response) {
                        let parentCategory =
                            parseInt(document.getElementById('parentCategoryId').innerHTML, 10);
                        linkcategory(category_idchildcategory, parentCategory);
                        showcategories();
                    }, function (err) {
                        showHttpErrorMessage("main-content", err);
                    });
            }, function (error) {
                alert(getTextById(257) +
                    document.getElementById('file_imagefield').value
                    + getTextById(258) + error.status);
            });
        } else {
            let catImage;
            if (prodcatimage) catImage = prodcatimage;
            else catImage = images.missing_image_txt;
            apiCall_listorante_admin_editcategory('0', '0', '0', '0', '0',
                category_idchildcategory, idStyle, 0, prodcatdesc_locale,
                prodcatdesc_locale, catImage, prodcatname, prodcatname_locale,
                function (response) {
                    if (response._rc > 0) alert("Edit failed");
                    else {
                        let parentCategory =
                            parseInt(document.getElementById('parentCategoryId').innerHTML, 10);
                        linkcategory(category_idchildcategory, parentCategory);
                        showcategories();
                    }
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });
        }
    }
});


// 4. Assign a product to a category
$(document).on(
    'click',
    '.checkProduct',
    function () {
        const product = $(this).data("product");
        const category = $(this).data("category");
        debug("check Product", 0, product, category);
        let checkBox = document.getElementById("checkProd" + product);
        // checkBox.checked=!checkBox.checked;
        if (checkBox.checked) {
            apiCall_listorante_admin_setproductcategory(category, product, function (data) {
                debug("Assigned product " + product + " to category " + category);
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        } else {
            apiCall_listorante_admin_unsetproductcategory(category, product, function (data) {
                debug("De-Assigned product " + product + " from category " + category);
            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        }
    });

// 5. Delete a category directly from within the editable table
$(document).on('click', '.delete-category', function () {
    const idcategory = $(this).data("idproductcategory");
    if (confirm('Delete this category?'))
        apiCall_listorante_admin_deletecategory(idcategory, function (data) {
            if (data._rc > 0) {
                alert(getTextById(259));
            } else {
                debug("Delete " + idcategory, 0, data);
                showcategories();
            }
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
});


// End of: Admin-Menu/ Function 4 / Edit Category

function pleaseOpen(thisNode) {
    if (typeof thisNode == "undefined") return;
    if (thisNode.hasClass("jstree-leaf") || thisNode.hasClass("jstree-open")) return;
    $('#jstree').open_node(thisNode, function () {
        ;
    }, true);
};
/*-----------------------------------*/ 
/* admin_functionlayout.js */ 
/*const LAYOUT_SUB_TYPE = {
    STRUCTURE: "structure",
    COLOR: "color",
    IMAGE: "image",
    FONT: "font",
    TEXT: "text"
};
let layOutData;
setLayoutTexts();
let currentView = null;

function setLayoutTexts() {
    setElemText(200);
    setElemText(201);
    setElemText(202);
    setElemText(203);
    setElemText(204);
    setElemText(262);
}

let selectedLayoutId = null;

function getSelectedLayoutId() {
    return selectedLayoutId ? selectedLayoutId : (sessionStorage.getItem("selectedLayoutId") ? sessionStorage.getItem("selectedLayoutId") : "2");
}
function setSelectedLayoutId(_selectedLayoutId) {
    selectedLayoutId = _selectedLayoutId;
}

/**
 * common code for show listing of layout element
 * @param type
 * @param successData
 * /
function showListForLayout(type, data) {
    let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
            const tableDiv = [formRowDiw, "tableDivID", 12];
            const iFrameDiv = [formRowDiw, "iFrameDivID", 12];
            layOutData =successData;
            //TODO add event-listener for onChange of listbox (point nr 4 of specification)
            // just for example I select here hardcoded the second theme/layout
            selectedTheme = successData.rows[2].s[7];
            const listBox = [formSelectBox, 262, "get-Theme", "listBoxID", successData.rows[0].s[6], successData.rows[1].s[6],successData.rows[2].s[6]];
            //TODO add event-listener for click of refresh-Button (point nr 7 of specification)
            const refreshButton = [FormButton, 600, "refreshButtonId", "", "refresh-preview",];
            const action1 = ["create-edit" + type + "-form", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
            const action2 = ["set-default-" + type + "", actionDeleteStyle.elements[0] + getTextById(216) + actionDeleteStyle.elements[1]];
            const dataColumnNames1 = ["id", "layoutid", "elementlabel", "cssrootelement", "value", "name", "typeid", "alt_text", "defaultvalue"];
            const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8];
            const dataColumnNames2 = ["id"];
            const dataColumnIndices2 = [0];
            const admin = {
                IDENTIFIER: "admin-edit-layout-" + type,
                jsonRMLData: data,
                header: [getTextById(201), getTextById(205), getTextById(202), getTextById(203),'',''],
                columnIndices: [2, 3, 4, 8],
                actions: [action1, action2],
                dataColumnIndices: [dataColumnIndices1, dataColumnIndices1],
                dataColumnNames: [dataColumnNames1, dataColumnNames1],
                rowFormat: function (rowIndex, dataArray) {
                    return dataArray;
                },
                cellFormat : function(colIndex,rowIndex,TDopen,data,TDclose){
                    {
                        if (type == "images" && colIndex === 2 ) {
                            return TDopen + "<img src='"+getImagePath()+"/" + data
                                + "' "+ThumbStyle.default +">" + TDclose;
                        } else {
                            return TDopen + data + TDclose;
                        }
                    }
                }
            };
            const theForm = [listBox, tableDiv, refreshButton, iFrameDiv];
            listoHTML.createForm("#main-content", 12, theForm, "layoutEdit");
            listoHTML.createTable.call(admin, "tableDivID");
            const lpT = layoutPageTemplate("150%", "500px", selectedTheme,"layoutFrame");
            $("#iFrameDivID").html(lpT);
            setDefaultSelectedLayoutValue();
        }, function err(data) {
            showHttpErrorMessage(data._rc);
        }
    );
};

function setDefaultSelectedLayoutValue(){
    if(!layOutData) {return;}
    const layoutIdArray = [];
    const layoutNameArray = [];
    for(let i=0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutName = layoutNameArray[layoutIdArray.indexOf(getSelectedLayoutId())];
    $("#listBoxID").val(_selectedLayoutName);
}

/**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
function initDataTableForLayoutElems(_DataFunction, layoutId, layoutType) {
    currentView = layoutType;
    _DataFunction(layoutId, function (successData) {
        showListForLayout(layoutType, successData);
    }, function (errorData) {
        showHttpErrorMessage("main-content", errorData);
    });
}

function initLayoutFunction(_type) {
    switch (_type) {
        case "texts" :
            initDataTableForLayoutElems(apiCall_listorante_admin_layouttexts, getSelectedLayoutId(), "texts");
            break;
        case "colors":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutcolors, getSelectedLayoutId(), "colors");
            break;
        case "fonts":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutfonts, getSelectedLayoutId(), "fonts");
            break;
        case "structures":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutstructures, getSelectedLayoutId(), "structures");
            break;
        case "images":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutimages, getSelectedLayoutId(), "images");
            break;
        default:
            break;
    }
}

/**
 * show layout structure
 * /
function showLayoutStructures() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutstructures, getSelectedLayoutId(), "structures");
}

/**
 * show layout colors
 */
function showLayoutColors() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutcolors, getSelectedLayoutId(), "colors");
}

/**
 * show layout images
 * /
 function showLayoutImages() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutimages, getSelectedLayoutId(), "images");
}

 /**
 * show layout fonts
 * /
 function showLayoutFonts() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutfonts, getSelectedLayoutId(), "fonts");
}

 /**
 * show layout text
 * /
 function showLayoutText() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layouttexts, getSelectedLayoutId(), "texts");
}

 /**
 * bind structure list events
 *
 * /
 $(document).on('click', '.layout-list', function () {
    const layoutSubType = $(this).data("type");
    switch (layoutSubType) {
        case LAYOUT_SUB_TYPE.STRUCTURE :
            showLayoutStructures();
            break;
        case LAYOUT_SUB_TYPE.COLOR :
            showLayoutColors();
            break;
        case LAYOUT_SUB_TYPE.IMAGE :
            showLayoutImages();
            break;
        case LAYOUT_SUB_TYPE.FONT :
            showLayoutFonts();
            break;
        case LAYOUT_SUB_TYPE.TEXT :
            showLayoutText();
            break;
    }

});
 const saveLayoutData = (function () {
    return function (saveData) {
        return new Promise(function (resolve, reject) {
            apiCall_listorante_admin_updatelayout(saveData.layoutelementname , saveData.layoutId, saveData.value, function (data) {
                resolve(data);
            }, function (error) {
                reject(error)
            });
        });
    }
})();

 $(document).on('click', '.create-editfonts-form', function () {
    generateForm(this, "fonts")
});
 $(document).on('click', '.create-editstructures-form', function () {
    generateForm(this, "structures")
});
 $(document).on('click', '.create-editcolors-form', function () {
    generateForm(this, "colors")
});
 $(document).on('click', '.create-edittexts-form', function () {
    generateForm(this, "texts")
});
 $(document).on('click', '.create-editimages-form', function () {
    generateForm(this, "images")
});

 $(document).on('change', '#listBoxID', function () {
    const selectdLayoutName = $(this).val();
    const layoutIdArray = [];
    const layoutNameArray = [];
    for(let i=0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutId = layoutIdArray[layoutNameArray.indexOf(selectdLayoutName)];
    refreshIframeDataByFromApiSelectedLayoutData(_selectedLayoutId);
    apiCall_listorante_admin_setlayoutid(_selectedLayoutId,function (successData) {
        sessionStorage.setItem("selectedLayoutId", _selectedLayoutId);
        setSelectedLayoutId(_selectedLayoutId);
        initLayoutFunction(currentView);
    }, function err(data) {
        showHttpErrorMessage(data._rc);
    });

});

 function generateForm(that, layoutType) {
    currentView = layoutType;
    const iFrameDiv = [formRowDiw, "iFrameDivID", 12];
    const refreshButton = [FormButton, 600, "refreshEditButtonId", "", "refresh-edit-preview"];
    const elementLabel = $(that).data("elementlabel");
    const id = $(that).data("id");
    const cssVariable=$(that).data("cssrootelement");
    const value = $(that).data("value");
    const defaultValue = $(that).data("defaultvalue");
    const alt_Text = $(that).data("alt_text");
    debug("create edit " + layoutType + " form", 0, elementLabel, id);
    const layoutElementName = [formTextInput, 201, "layout_" + layoutType + "_element_name", 'rows="1" required disabled ' +
    'alt="' + alt_Text + '" title="' + alt_Text + '"',
        elementLabel];
    let layoutValue = null, layoutDefaultValue = null;
    const imageSrc = $(this).data('layout_images_default_value');

    if (layoutType === "texts") {
        layoutValue = [FormTextArea, 202, "layout_" + layoutType + "_value", "rows=2 required ", value];
        layoutDefaultValue = [FormTextArea, 203, "layout_" + layoutType + "_default_value", "rows=2 disabled required ", defaultValue];
    } else if(layoutType === 'images'){
        layoutValue  = [formImage, 202, "layout_" + layoutType + "_value", "","","Choose Image"];
        layoutDefaultValue = [formDisplayImage, 203, "layout_" + layoutType + "_default_value", "longdesc=\"" + value + "\"", getImagePath() + "/" + value,"Default Image"];

    } else {
        layoutValue = [formTextInput, 202, "layout_" + layoutType + "_value", 'rows="1" required ' +
        'alt="' + alt_Text + '" title="' + alt_Text + '"',
            value];
        layoutDefaultValue = [formTextInput, 203, "layout_" + layoutType + "_default_value", 'rows="1" required disabled ',
            defaultValue];
    }
    const cssField = [formTextInput, 205, "layout_" + layoutType + "_cssrootelement", 'rows="1" required disabled',
        cssVariable];

    const hidden_layout_id = [formHiddenInput, "", id, "id_of_edited_layout_" + layoutType + ""];
    const submitButton = [FormButton, 215, "update-layout-" + layoutType + "", "", "update-layout-" + layoutType + ""];
    const form1 = [layoutElementName, cssField, layoutValue, layoutDefaultValue, hidden_layout_id, submitButton,refreshButton,iFrameDiv];
    listoHTML.createForm("#main-content", 12, form1, "layout-" + layoutType + "-edit-form");
    let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
        selectedTheme =successData.rows[2].s[7];
        const lpT = layoutPageTemplate("150%", "500px", selectedTheme,"layoutFrame");
        $("#iFrameDivID").html(lpT);
        refreshIframeDataBySelectedLayoutData(getSelectedLayoutId());
    }, function err(data) {
        showHttpErrorMessage(data._rc);
    });
}

 const updateLayoutButtonAction = (function () {
    return function (type, value) {
        let layoutelementname = $('#layout_' + type + '_cssrootelement').val();
        let layoutelementvalue = $('#layout_' + type + '_value').val();
        /**
 * removing first dashes
 * @type {void | string | never}
 * /
 layoutelementname = layoutelementname.replace("--", "");
 layoutelementvalue = layoutelementvalue.replace(/#|_/g, '');
 const _DataRow = {
            layoutelementname: layoutelementname,
            value: value || layoutelementvalue,
            layoutId: getSelectedLayoutId()
        };
 apiCall_listorante_admin_updatelayout(escape(_DataRow.layoutelementname), _DataRow.layoutId, escape(_DataRow.value), function (data) {
            initLayoutFunction(type);
        }, function (error) {
            showHttpErrorMessage("main-content", error);
        });
 }
 })();


 const resetLayoutButtonAction = (function () {
    return function (type, that) {
        let layoutelementname = $(that).data("cssrootelement");
        let layoutelementvalue = $(that).data("defaultvalue");
        layoutelementname = layoutelementname.replace("--", "");
        layoutelementvalue = layoutelementvalue.replace(/#|_/g, '');
        if (type == 'images') {
            // to get file name from actual default "css property values"
            const urrls = layoutelementvalue.split(",");
            const url__ = urrls[0];
            const filePath = url__.replace("url('", "").replace("')", "");
            layoutelementvalue = filePath.substring(filePath.lastIndexOf('/') + 1);
        }
        const _DataRow = {
            layoutelementname: layoutelementname,
            value: layoutelementvalue,
            layoutId: getSelectedLayoutId()
        };
        apiCall_listorante_admin_updatelayout(escape(_DataRow.layoutelementname), _DataRow.layoutId, escape(_DataRow.value), function (data) {
            initLayoutFunction(type);
        }, function (error) {
            showHttpErrorMessage("main-content", error);
        });
    }
})();

 $(document).on('click','.update-layout-structures',function () {
    updateLayoutButtonAction("structures");
});

 $(document).on('click','.update-layout-colors',function () {
    updateLayoutButtonAction("colors");
});

 $(document).on('click','.update-layout-fonts',function () {
    updateLayoutButtonAction("fonts");
});

 $(document).on('click','.update-layout-texts',function () {
    updateLayoutButtonAction("texts");
});

 $(document).on('click','.update-layout-images',function () {
    const custID = getCustomerId();
    if (document.getElementById('layout_images_value').value) {
        upload(custID, 'layout-images-edit-form', function (data) {
            debug("upload-data:", release, data);
            const imageNew = "" + data.rows[0].s[2];
            const image_thumbNew = imageNew;
            debug(getTextById(255) + document.getElementById('layout_images_value').value + getTextById(256) + imageNew + "'", 0);
            updateLayoutButtonAction("images",image_thumbNew);
        }, function (error) {
            alert(getTextById(257) + document.getElementById('layout_images_value').value + getTextById(258) + error.status);
        });
    }
});

 $(document).on('click','.set-default-structures',function () {
    resetLayoutButtonAction("structures", this);
});

 $(document).on('click','.set-default-colors',function () {
    resetLayoutButtonAction("colors", this);
});

 $(document).on('click','.set-default-fonts',function () {
    resetLayoutButtonAction("fonts", this);
});

 $(document).on('click','.set-default-texts',function () {
    resetLayoutButtonAction("texts", this);
});
 $(document).on('click','.set-default-images',function () {
    resetLayoutButtonAction("images", this);
});

 $(document).on('click','.refresh-preview',function () {
     refreshIframeDataByFromApiSelectedLayoutData(getSelectedLayoutId());
     refreshIframeTextDataByFromApiSelectedLayoutData(getSelectedLayoutId());
});
 $(document).on('click','.refresh-edit-preview',function () {
    updateEditRootElementCssStyleElement();
});

 function getLayoutDataByType(functionDefinition, layoutId) {
    return new Promise(function(resolve, reject){
        functionDefinition(layoutId, resolve, reject);
    })
}
 /**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
 function refreshIframeDataByFromApiSelectedLayoutData(layoutId, layoutType) {
    apiCall_listorante_admin_updatecss(layoutId,function (successData) {
        apiCall_listorante_admin_getcss(layoutId,function (layoutTypeData) {
            setLayoutTypeValuesForPreviewIframe(layoutTypeData);
        }, function err(data) {
            showHttpErrorMessage(data._rc);
        });
    },function err(data) {
        showHttpErrorMessage(data._rc);
    });
}
 /**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
 function refreshIframeTextDataByFromApiSelectedLayoutData(layoutId, layoutType) {
    apiCall_listorante_admin_layouttexts(layoutId,function (layoutTypeData) {
        replaceApiIframeTextFromLayout(layoutTypeData,layoutType);
     },function err(data) {
        showHttpErrorMessage(data._rc);
    });
}
 /**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
 function refreshIframeDataBySelectedLayoutData(layoutId, layoutType) {
    const allLayoutDataPromises = [
        getLayoutDataByType(apiCall_listorante_admin_layoutcolors, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layouttexts, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layoutfonts, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layoutstructures, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layoutimages, layoutId)
    ];
    Promise.all(allLayoutDataPromises).then(function(layoutTypeData){
        setLayoutTypeValuesForPreview(layoutType, layoutTypeData);
    }).catch(function (errors) {
    })
}
 var setLayoutTypeValuesForPreview = (function(){
    return function(type, layoutTypeData){
        const iFrameData = {};
        for(let i=0; i < layoutTypeData.length; i++) {
            for (let j = 0; j < layoutTypeData[i].rows.length; j++) {
                const typeKeyName = layoutTypeData[i].rows[j].s[5] + 's';
                if(!iFrameData[typeKeyName]) {
                    iFrameData[typeKeyName] = {};
                }
                iFrameData[typeKeyName][layoutTypeData[i].rows[j].s[3]] = layoutTypeData[i].rows[j].s[4];
            }
        }
        buildIFrameRootCSS(iFrameData, type);
        replaceIframeTextFromLayout(iFrameData);
    }
})();
 var setLayoutTypeValuesForPreviewIframe = (function(){
    return function(layoutTypeData){
        const styleRootElement = getRootElementCssStyleElement();
        if(styleRootElement) {
            styleRootElement.html("");
            const itemCss = [];
            for(let i=0; i < layoutTypeData.rows.length; i++) {
                itemCss.push(layoutTypeData.rows[i].s[0]);
            }
            styleRootElement.append(itemCss.concat(""));
        }
    }
})();
 const getPreviewIframeElement = (function () {
    return function (elementNodeName) {
        const iframeElement = $('#layoutFrame');
        if (iframeElement.length > 0) {
            return iframeElement.contents().find(elementNodeName)
        } else {
            return null;
        }
    }
})();

 function CssParser(){
}
 CssParser.prototype.removeComments= function(css) {
    return css.replace(/\/\*(\r|\n|.)*\*\//g,"");
};
 CssParser.prototype.parseCSSBlock = function(css) {
    const rule = {};
    const declarations = css.split(';');
    declarations.pop();
    const len = declarations.length;
    for (let i = 0; i < len; i++)
    {
        const loc = declarations[i].indexOf(':');
        const property = $.trim(declarations[i].substring(0, loc));
        const value = $.trim(declarations[i].substring(loc + 1));

        if (property != "" && value != "")
            rule[property] = value;
    }
    return rule;
};
 CssParser.prototype.parseCSS = function(css) {
    const rules = {};
    css = this.removeComments(css);
    const blocks = css.split('}');
 blocks.pop();
 const len = blocks.length;
 for (let i = 0; i < len; i++)
 {
        const pair = blocks[i].split('{');
        rules[$.trim(pair[0])] = this.parseCSSBlock(pair[1]);
    }
    return rules;
};
 var getRootElementCssStyleElement = (function(){
    return function(){
        const iframeHeadElement = getPreviewIframeElement("head");
        if (iframeHeadElement.length > 0) {
            let styleRootElement = iframeHeadElement.find("#iFrameLayoutStyle");
            if(styleRootElement.length === 0){
                styleRootElement = $("<style></style>");
                styleRootElement.prop("id", "iFrameLayoutStyle");
                styleRootElement.prop("type", "text/css");
                iframeHeadElement.append(styleRootElement)
            }
            return styleRootElement;
        } else {
            return null;
        }
    }
})();
 function updateEditRootElementCssStyleElement() {
    const layoutTypes = ["structures", "colors", "images", "fonts"];
    if(layoutTypes.indexOf(currentView) > -1) {
        const layoutelementname = $('#layout_' + currentView + '_cssrootelement').val();
        const layoutelementvalue = $('#layout_' + currentView + '_value').val();
        const styleRootElement = getRootElementCssStyleElement();
        if(styleRootElement) {
            const pastRules = styleRootElement[0].innerHTML;
            const _cssParser = new CssParser();
            const oldCssRules = _cssParser.parseCSS(pastRules);
            let rootElementVariableString = ":root {\n";
            const oldRootCssRules = oldCssRules[":root"];
            for (let key in oldRootCssRules) {
                if(currentView === "images") {
                    if(layoutelementname === key) {
                        const imageUrl = "url('../assets/images/" + layoutelementvalue + "'), url('../assets/images/" + layoutelementvalue + "')";
                        rootElementVariableString += "\t"+(key+" : "+imageUrl+"; \n");
                    } else {
                        rootElementVariableString += "\t"+(key+" : "+oldRootCssRules[key]+"; \n");
                    }
                } else {
                    if(layoutelementname === key) {
                        rootElementVariableString += "\t"+(key+" : "+layoutelementvalue+"; \n");
                    } else {
                        rootElementVariableString += "\t"+(key+" : "+oldRootCssRules[key]+"; \n");
                    }
                }
            }
            rootElementVariableString += "}";
            styleRootElement[0].innerHTML = rootElementVariableString;
        }
    } else {
        // update text here.
    }
}
 function buildIFrameRootCSS(layoutItemValues) {
    const rootElementCss = getRootElementCssVariableValues(layoutItemValues);
    const styleRootElement = getRootElementCssStyleElement();
    if(styleRootElement) {
        styleRootElement[0].innerHTML = rootElementCss;
    }
}
 function buildApiDataIFrameRootCSS(layoutItemValues) {
    const styleRootElement = getRootElementCssStyleElement();
    if(styleRootElement) {
        styleRootElement[0].innerHTML = layoutItemValues;
    }
}
 function getRootElementCssVariableValues(layoutItemValues) {
    const layoutTypes = ["structures", "colors", "images", "fonts"];
    let rootElementVariableString = ":root {\n";
    for (let i=0; i<layoutTypes.length; i++) {
        //rootElementVariableString += "\n/ * * dynamic variable started : "+layoutTypes[i]+"**/
/*
    for (let key in layoutItemValues[layoutTypes[i]]) {
        if(layoutTypes[i] === "images") {
            const imageUrl = "url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "'), url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "')";
            rootElementVariableString += "\t"+(key+" : "+imageUrl+"; \n");
        } else {
            rootElementVariableString += "\t"+(key+" : "+layoutItemValues[layoutTypes[i]][key]+"; \n");
        }
    }
    // rootElementVariableString += "\n/** dynamic variable ended : "+layoutTypes[i]+"**/
/*}
rootElementVariableString += "}";
return rootElementVariableString;
}
function getIFrameRootElementCssVariableValues(layoutItemValues) {
const layoutTypes = ["structures", "colors", "images", "fonts"];
let rootElementVariableString = ":root {\n";
for (let i=0; i<layoutTypes.length; i++) {
    for (let key in layoutItemValues[layoutTypes[i]]) {
        if(layoutTypes[i] === "images") {
            const imageUrl = "url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "'), url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "')";
            rootElementVariableString += "\t"+(key+" : "+imageUrl+"; \n");
        } else {
            rootElementVariableString += "\t"+(key+" : "+layoutItemValues[layoutTypes[i]][key]+"; \n");
        }
    }
}
rootElementVariableString += "}";
return rootElementVariableString;
}
function replaceIframeTextFromLayout(layoutItemValues) {
const iframeBodyElement = getPreviewIframeElement("body");
for (let key in layoutItemValues.texts) {
    iframeBodyElement.find("#"+key).text(layoutItemValues.texts[key])+";";
}
}
function replaceApiIframeTextFromLayout(layoutItemValues) {
const iframeBodyElement = getPreviewIframeElement("body");
for (let i=0; i<layoutItemValues.rows.length; i++) {
    iframeBodyElement.find("#"+layoutItemValues.rows[i].s[3]).text(layoutItemValues.rows[i].s[4])+";";
}
}
*//*-----------------------------------*/ 
/* admin_settings.js */ 
release = "d1a6f_4";
module = "admin_settings.js";
let layOutData;
let currentView;
let selectedLayoutId;

function checkCustomError(data) {
    if (data && data._rc && parseInt(data._rc, 10) > 0) {
        showHttpErrorMessage("main-content", data._rc);
    } else {
        debug("No return code found in data: ", release, data);
    }
}

function showLayoutTextAdmin() {
    initDataTableForLayoutElemsAdmin(apiCall_listorante_admin_layouttexts, getLayoutId(), "texts");
}

function initDataTableForLayoutElemsAdmin(_DataFunction, layoutId, layoutType) {
    currentView = layoutType;
    _DataFunction(layoutId, function (successData) {
        showListForLayoutAdmin(layoutType, successData);
    }, function (errorData) {
        showHttpErrorMessage("main-content", errorData);
    });
}


function showListForLayoutAdmin(type, data) {

    let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
            const tableDiv = formRowDiw("tableDivID", 12);
            layOutData = successData;
            let layouts = {};
            for (let opt = 0; opt < successData.count; opt++) {
                layouts[opt] = successData.rows[opt].s[6];
                if (parseInt(successData.rows[opt].s[9], 10) === 1) {
                    //Set selected theme
                    selectedTheme = successData.rows[opt].s[6];
                    //debug("selected theme = " + opt + "(" + selectedTheme + ")", release);
                }
            }
            //debug("layouts", release, layouts, successData);
            //const listBox = formSelectBoxArray(262, "get-Theme", "listBoxIDAdmin", layouts);
            const privacyButton = formButtonWithIconAndBadge(757, "privacyButton", "#28a0e5",
                getTextById(737), "fa fa-user-secret", "btn btn-primary btn-xs");
            const businessTermsButton = formButtonWithIconAndBadge(757, "businessTerms",
                "#28a0e5", getTextById(736),
                "fa fa-briefcase", "btn btn-primary btn-xs");
            let stripeRegisterButton;
            let stripeConnectButton;
            if (is_stripe_registered) {
                stripeRegisterButton = formButtonWithIconAndBadge(11, "",
                    "#28a0e5", getTextById(755), "fa fa-cc-stripe",
                    "btn btn-primary btn-xs disabled");
                stripeConnectButton = formButtonWithIconAndBadge(757, "connectWithStripe",
                    "#28a0e5", getTextById(756), "fa fa-cc-stripe",
                    "btn btn-primary btn-xs");
            } else {
                stripeRegisterButton = formButtonWithIconAndBadge(757, "registerWithStripe",
                    "#28a0e5", getTextById(755), "fa fa-cc-stripe",
                    "btn btn-primary  btn-xs ");
            }
            const action1 = ["create-edit" + type + "-formAdmin", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
            //const action2 = ["set-default-" + type + "Admin", actionDeleteStyle.elements[0] + getTextById(216) + actionDeleteStyle.elements[1]];
            const dataColumnNames1 = ["id", "layoutid", "elementlabel", "value", "cssrootelement", "name", "typeid", "alt_text", "defaultvalue"];
            const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8];
            const admin = {
                IDENTIFIER: "admin-edit-layout-" + type,
                jsonRMLData: data,
                //header: [getTextById(201), getTextById(205), getTextById(202), getTextById(203),'',''],
                header: [getTextById(205), getTextById(202), ""],
                //columnIndices: [2, 3, 4, 8],
                columnIndices: [3, 2],
                //actions: [action1, action2],
                actions: [action1],
                //dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
                //dataColumnNames: [dataColumnNames1, dataColumnNames2],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1],
                rowFormat: function (rowIndex, dataArray) {
                    return dataArray;
                },
                cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                    {
                        if (type == "images" && colIndex === 2) {
                            return TDopen + "<img src='" + getImagePath() + "/" + data
                                + "' " + ThumbStyle.default + ">" + TDclose;
                        } else {
                            return TDopen + data + TDclose;
                        }
                    }
                }
            };
            let theForm;
            if (is_stripe_registered) {
                theForm = [tableDiv, privacyButton, businessTermsButton, stripeRegisterButton, stripeConnectButton];
            } else {
                theForm = [tableDiv, privacyButton, businessTermsButton, stripeRegisterButton];
            }
            listoHTML.buildForm("#main-content", 12, theForm, "layoutEdit");
            // listoHTML.createForm("#main-content", 12, theForm, "layoutEdit");
            listoHTML.createTable.call(admin, "tableDivID");
            //const lpT = layoutPageTemplate("150%", "500px", selectedTheme,"layoutFrame");
            // $("#iFrameDivID").html(lpT);
            setDefaultSelectedLayoutValueAdmin();
        }, function err(data) {
            showHttpErrorMessage(data);
        }
    );
};


function setDefaultSelectedLayoutValueAdmin() {
    if (!layOutData) {
        return;
    }
    const layoutIdArray = [];
    const layoutNameArray = [];
    for (let i = 0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutName = layoutNameArray[layoutIdArray.indexOf(getLayoutId())];
    $("#listBoxIDAdmin").val(_selectedLayoutName);
}


function setSelectedLayoutIdAdmin(_selectedLayoutId) {
    selectedLayoutId = _selectedLayoutId;
}


function initLayoutFunctionAdmin(_type) {
    switch (_type) {
        case "texts" :
            initDataTableForLayoutElemsAdmin(apiCall_listorante_admin_layouttexts, getLayoutId(), "texts");
            break;
        case "colors":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutcolors, getLayoutId(), "colors");
            break;
        case "fonts":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutfonts, getLayoutId(), "fonts");
            break;
        case "structures":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutstructures, getLayoutId(), "structures");
            break;
        case "images":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutimages, getLayoutId(), "images");
            break;
        default:
            break;
    }

}


function generateFormAdmin(that, layoutType) {
    currentView = layoutType;
    //const iFrameDiv = formRowDiw("iFrameDivID", 12);
    //const refreshButton = FormButton(600, "refreshEditButtonId", "", "refresh-edit-preview");
    debug("that data #####", release, $(that).data());
    const backButton = FormButton(734, "backButtonLayout", "", "back-button");
    const elementLabel = $(that).data("elementlabel");
    //const id = $(that).data("id");
    const elementid = $(that).data("id");
    const id = $(that).data("value");

    const cssVariable = $(that).data("cssrootelement");
    //const value = $(that).data("value");
    const value = $(that).data("elementlabel");
    //const defaultValue = $(that).data("defaultvalue");
    const alt_Text = $(that).data("alt_text");
    debug("create edit " + layoutType + " form", release, elementLabel, id);
    //const layoutElementName = formTextInput(201, "layout_" + layoutType + "_element_name", 'rows="1" required disabled ' +
    //    'alt="' + alt_Text + '" title="' + alt_Text + '"',
    //    elementLabel);
    const layoutElementName = formTextInput(201, "layout_" + layoutType + "_element_name", 'rows="1" required disabled ' +
        'alt="' + alt_Text + '" title="' + alt_Text + '"',
        id);
    let layoutValue = null, layoutDefaultValue = null;
    //const imageSrc = $(this).data('layout_images_default_value');

    if (layoutType === "texts") {
        layoutValue = FormTextArea(202, "layout_" + layoutType + "_value", "rows=2 required maxlength='250'", value);
        //layoutDefaultValue = FormTextArea(203, "layout_" + layoutType + "_default_value", "rows=2 disabled required ", defaultValue);
    } else if (layoutType === 'images') {
        layoutValue = formImage(202, "layout_" + layoutType + "_value", "", "", "Choose Image");
        //layoutDefaultValue = formDisplayImage(203, "layout_" + layoutType + "_default_value", "longdesc=\"" + value + "\"", getImagePath() + "/" + value,"Default Image");

    } else {
        layoutValue = formTextInput(202, "layout_" + layoutType + "_value", 'rows="1" required  ' +
            'alt="' + alt_Text + '" title="' + alt_Text + '"',
            value);
        //layoutDefaultValue = formTextInput(203, "layout_" + layoutType + "_default_value", 'rows="1" required disabled ', defaultValue);
    }
    const cssField = formTextInput(205, "layout_" + layoutType + "_cssrootelement", 'rows="1" required disabled',
        cssVariable);

    //const hidden_layout_id = formHiddenInput("", id, "id_of_edited_layout_" + layoutType + "");
    const hidden_layout_id = formHiddenInput("", elementid, "id_of_edited_layout_" + layoutType + "");
    const submitButton = FormButton(215, "update-layout-" + layoutType + "", "data-idtext='" + id + "'", "update-layout-" + layoutType + "");
    //const form1 = [layoutElementName, cssField, layoutValue, layoutDefaultValue, hidden_layout_id, submitButton, backButton];
    const form1 = [layoutElementName, layoutValue, hidden_layout_id, submitButton, backButton];
    listoHTML.buildForm("#main-content", 12, form1, "layout-" + layoutType + "-edit-form");
    // listoHTML.createForm("#main-content", 12, form1, "layout-" + layoutType + "-edit-form");
    /*let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
         //selectedTheme = successData.rows[2].s[7];
         selectedTheme = getLayoutId();
         const lpT = layoutPageTemplate("150%", "500px", selectedTheme, "layoutFrame");
         //$("#iFrameDivID").html(lpT);
         refreshIframeDataBySelectedLayoutData(getLayoutId());
     }, function err(data) {
         showHttpErrorMessage(data._rc);
     });*/
}


$(document).on("click", ".set-Settings", function () {
    showLayoutTextAdmin();
});

$(document).on("click", "#backButtonLayout", function () {
    showLayoutTextAdmin();
});

$(document).on("click", "#cancelBusinessTerms", function () {
    showLayoutTextAdmin();
});


$(document).on("click", "#cancelPrivacy", function () {
    showLayoutTextAdmin();
});


$(document).on("click", "#submitTerms", function () {
    let longText = document.getElementById("businessTerms").value;
    if (longText.length > 16000) {
        longText = longText.substring(0, 16000);
    }
    apiCall_listorante_admin_updatetext(2, longText, function (data) {
        debug("Terms updated", release, data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
        alert("ERROR, status=" + err.status + "(" + getTextById(err.status) + ")");
    });
    showLayoutTextAdmin();
});


$(document).on("click", "#submitPrivacy", function () {
    let longText = document.getElementById("privateTerms").value;
    if (longText.length > 16000) {
        longText = longText.substring(0, 16000);
    }
    apiCall_listorante_admin_updatetext(1, longText, function (data) {
        debug("Privacy updated", release, data);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
        alert("ERROR, status=" + err.status + "(" + getTextById(err.status) + ")");
    });

    showLayoutTextAdmin();
});

$(document).on("click", ".create-edittexts-formAdmin", function () {
    generateFormAdmin(this, "texts");
});

$(document).on("click", "#privacyButton", function () {
    let longText = "";
    apiCall_listorante_public_longtext(3, function (data) {
        for (let i = 0; i < data.rows.length; i++) longText += data.rows[i].s[0];
        //debug("privacy longText", release, longText);
        document.getElementById("privateTerms").value = longText;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
    const textAreaField = FormTextArea(737, "privateTerms", "rows=15 ", longText);
    const submitButton = FormButton(215, "submitPrivacy", "", "submit-button");
    const cancelButton = FormButton(602, "cancelPrivacy", "", "cancel-button");

    const theForm = [textAreaField, submitButton, cancelButton];
    listoHTML.buildForm("#main-content", 12, theForm, "privateTermsForm");
});


$(document).on("click", "#businessTerms", function () {
    let longText = "";
    apiCall_listorante_public_longtext(2, function (data) {
        for (let i = 0; i < data.rows.length; i++) longText += data.rows[i].s[0];
        debug("Business Terms longText", release, longText);
        document.getElementById("businessTerms").value = longText;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
    const textAreaField = FormTextArea(736, "businessTerms", "rows=15 ", longText);
    const submitButton = FormButton(215, "submitTerms", "", "submit-button");
    const cancelButton = FormButton(602, "cancelBusinessTerms", "", "cancel-button");

    const theForm = [textAreaField, submitButton, cancelButton];
    listoHTML.buildForm("#main-content", 12, theForm, "businessTermsForm");
});

$(document).on("click", ".update-layout-texts", function () {
    debug("ButtonData:", release, $(this).data());
    const textID = $(this).data("idtext");
    const textValue = $.trim($("#layout_texts_value").val());

    apiCall_listorante_admin_updatelayout(textID, getLayoutId(), textValue, function (data) {
        debug("setting value of id " + textID + " to '" + textValue + "'", release, data);
    }, function (error) {
        showHttpErrorMessage("main-content", error);
    });
    showLayoutTextAdmin();
});

$(document).on("change", "#listBoxIDAdmin", function () {
    const selectdLayoutName = $(this).val();
    const layoutIdArray = [];
    const layoutNameArray = [];
    for (let i = 0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutId = layoutIdArray[layoutNameArray.indexOf(selectdLayoutName)];
    layoutChanged = true;
    //refreshIframeDataByFromApiSelectedLayoutDataAdmin(_selectedLayoutId);
    apiCall_listorante_admin_setlayoutid(_selectedLayoutId, function (successData) {
        //debug("set new layout id to '"+_selectedLayoutId+"'",release,successData,layoutIdArray,layoutNameArray,selectdLayoutName);
        sessionStorage.setItem("selectedLayoutId", _selectedLayoutId);
        setSelectedLayoutIdAdmin(_selectedLayoutId);
        initLayoutFunctionAdmin(currentView);
        apiCall_listorante_admin_layouttexts(_selectedLayoutId, function (data) {
            showLayoutTextAdmin();
            checkCustomError(data);
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        })
    }, function err(data) {
        showHttpErrorMessage("main-content", data);
    });
});


$(document).on("click", "#registerWithStripe", function () {
    const randomState = uuidv4();
    const stripe_client_id = STRIPE_CLID;
    console.log("Calling stripe onboaring with client-id " + stripe_client_id);
    const stripe_connect_url = 'https://connect.stripe.com/oauth/authorize?response_type=code&client_id='
        + stripe_client_id + '&scope=read_write&state=' + randomState;
    sessionStorage.setItem("stripe_new_Account_state", randomState);
    window.open(stripe_connect_url, "_self");
});

$(document).on("click", "#connectWithStripe", function () {
    const stripe_connect_url = 'https://connect.stripe.com';
    window.open(stripe_connect_url, "_self");
});

const stripe_code = getUrlVars()['code'];
const stripe_scope = getUrlVars()['scope'];
const stripe_state = getUrlVars()['state'];
const stripe_error = getUrlVars()['error'];
const stripe_errorDesc = getUrlVars()['error_description'];
const storedState = sessionStorage.getItem("stripe_new_Account_state");

if (stripe_code && stripe_scope && stripe_state) {
    if (storedState !== stripe_state) {
        const errorText = document.createElement("strong");
        errorText.innerText = getTextById(747);
        if (document.getElementById("registerWithStripe")) {
            document.getElementById("registerWithStripe").appendChild(errorText);
        }
        document.getElementById("main-content").innerHTML = errorText.outerHTML;
        console.error("Stripe status (" + stripe_state + ") and listorante-status (" + storedState + ") do not match.");
    } else {
        createNewStripeAccount(stripe_code, function (response) {
            const stripe_user_account = JSON.parse(response.stripe_account_id);
            apiCall_listorante_admin_setstripeid(stripe_user_account, function () {
                const successText = document.createElement("strong");
                successText.innerText = getTextById(758);
                if (document.getElementById("registerWithStripe")) {
                    document.getElementById("registerWithStripe").appendChild(successText);
                }
                document.getElementById("main-content").innerHTML = successText.outerHTML;
            }, function (err1) {
                showHttpErrorMessage("main-content", err1);
            });
        }, function (err) {
            showHttpErrorMessage("main-content", err)
        });
    }
} else if (stripe_state && stripe_error) {
    const errorText = document.createElement("strong");
    if (stripe_errorDesc) {
        errorText.innerText = getTextById(747) + ": (" + stripe_errorDesc + ")";
    } else {
        errorText.innerText = getTextById(747);
    }
    if (document.getElementById("registerWithStripe")) {
        document.getElementById("registerWithStripe").appendChild(errorText);
    }
    document.getElementById("main-content").innerHTML = errorText.outerHTML;
}


/*-----------------------------------*/ 
/* admin_staff.js */ 
release = "xx";
module = "admin_staff.js";
// upload of user-image
let doImageUpload = false;

function uploadProfileImage(booleanValue) {
    const selectedImage = document.getElementById('staff_image_id').value;
    debug("upload profile picture", release, selectedImage);
    doImageUpload = booleanValue;
};

/*
 * Admin-Menu/ Function 5 / Staff Management
 *
 *
 */

// 1. Add new Staff
$(document)
    .on(
        'click',
        '.add-user',
        function () {
            // Create Staff Form
            const email = [FormTextArea, 75, "staff_add_email", "rows='1' required ", ''];
            const first_name = [FormTextArea, 76, "staff_add_first_name", "rows='1' required ", ''];
            const last_name = [FormTextArea, 80, "staff_add_last_name", "rows='1' required ", ''];
            const user_name = [FormTextArea, 82, "staff_add_user_name", "rows='1' required ", ''];
            const password = [FormTextArea, 81, "staff_add_password", "rows='1' required ", ''];
            const is_active = [FormCheckBox, 77, "staff_add_is_active", ""];
            const is_staff = [FormCheckBox, 78, "staff_add_is_staff", ""];
            //const is_superuser = [FormCheckBox, 79, "staff_add_is_superuser", ""];
            const submitButton = [FormButton, 83, "add_user", "", "add-user-btn"];
            const labelForUIMessages = getLabelAlertObjectId();
            const messageLabel = [formLabel, '', labelForUIMessages];
            // Form Object of Array
            const form = [email, first_name, last_name, user_name,
                password, is_active, is_staff,
                //is_superuser,
                submitButton, messageLabel];
            debug("add-user-form", 0, form);
            listoHTML.createForm("#main-content", 6, form, "add-user-form-id");
        })

// Add user with API when Click Add User button in the form
$(document).on(
    'click',
    '.add-user-btn',
    function () {
        const email = $("#staff_add_email").val();
        const first_name = $("#staff_add_first_name").val();
        const last_name = $("#staff_add_last_name").val();
        const user_name = $("#staff_add_user_name").val();
        const password = $("#staff_add_password").val();
        const is_active = $("#staff_add_is_active").prop('checked');
        const is_staff = $("#staff_add_is_staff").prop('checked');
        // Email Validation
        if (!validateEmail(email)) {
            labelAlert(getTextById(268));
            return false;
        } else {
            labelAlert("");
        }
        if ($("#add-user-form-id").valid()) {
            apiCall_listorante_admin_newstaff(email, first_name, is_active,
                is_staff, last_name, password, user_name, function (data) {
                    debug("CHECK: ", release, data);
                    showUsers();
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });
        }
    });

// 2. Display User List when click Edit User

// Function to show Users to be reusable for staff
function showUsers() {
    apiCall_listorante_admin_userprofiles(
        function (data) {

            const action1 = [
                "edit-user",
                actionModifyStyle.elements[0] + getTextById(105)
                + actionModifyStyle.elements[1]];
            const action2 = [
                "delete-user",
                actionDeleteStyle.elements[0] + getTextById(106)
                + actionDeleteStyle.elements[1]];
            const action3 = [
                "assign-role-to-user",
                actionModifyStyle.elements[0] + getTextById(90)
                + actionModifyStyle.elements[1]];

            const dataColumnNames1 = ["id", "image", "first_name",
                "last_name", "email", "username", "password",
                "is_staff", "is_active", "date_joined",
                "last-login"];
            const dataColumnIndices1 = [0, 5, 6, 7, 8, 4, 1, 3, 9, 10, 2];
            const dataColumnNames2 = ["id"];
            const dataColumnIndices2 = [0];
            const dataColumnNames3 = ["id", "image", "username"];
            const dataColumnIndices3 = [0, 5, 4];
            const userTable = {
                IDENTIFIER: "admin-user-table",
                jsonRMLData: data[0],
                header: [getTextById(85), getTextById(76),
                    getTextById(80), getTextById(82),
                    getTextById(87), "", "", ""],
                columnIndices: [5, 6, 7, 4, 10],
                actions: [action1, action2, action3],
                dataColumnIndices: [dataColumnIndices1,
                    dataColumnIndices2, dataColumnIndices3],
                dataColumnNames: [dataColumnNames1, dataColumnNames2,
                    dataColumnNames3],
                cellFormat: function (colIndex, rowIndex, tdOpen, cell, tdClose) {
                    const img = getImagePath() + "/" + cell;
                    if (colIndex === 0) return `${tdOpen}${ThumbImage(601, img)}${tdClose}`;
                    else return `${tdOpen}${cell}${tdClose}`;
                }
            };
            listoHTML.createTable.call(userTable, "main-content");
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
}

// Show Users when click edit user list item
$(document).on('click', '.edit-userprofile', function () {
    showUsers();
});

// 3. Delete User When click Delete User Button
$(document).on('click', '.delete-user', function () {
    const userId = $(this).data("id");
    apiCall_listorante_admin_deletestaff(userId, function (data) {
        showUsers();
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    })
});

// This is the main change function

// 4. Edit User Form Display when click Edit User Button
$(document).on(
    'click',
    '.edit-user',
    function () {
        const userId = $(this).data("id");
        const email = formTextInput(75, "staff_edit_email", "rows='1' required ", $(this).data("email"));
        const first_name = formTextInput(76, "staff_edit_first_name", "rows='1' required ", $(this).data("first_name"));
        const last_name = formTextInput(80, "staff_edit_last_name", "rows='1' required ", $(this).data("last_name"));
        const user_name = formTextInput(82, "staff_edit_user_name", "rows='1' required ", $(this).data("username"));
        // const password = formTextInput(81, "staff_edit_password", "rows='1' required ", $(this).data("password"));
        const is_active = FormCheckBox(77, "staff_edit_is_active", "",
            $(this).data("is_active"));
        const is_staff = FormCheckBox(78, "staff_edit_is_staff", "", $(this).data("is_staff"));
        const submitButton = FormButton(215, "edit_user_btn", "data-userid=\"" + userId + "\"", "edit-user-btn");
        const changePasswordButton = FormButton(250, 'setPassword', "data-userid=\"" + userId + "\"", 'set-Password');
        const labelForUIMessages = getLabelAlertObjectId();
        const messageLabel = formLabel('', labelForUIMessages);
        const select_new_profile_image = simpleFormImage("uploadProfileImage(true)", "staff_image_id");
        const existing_image = formDisplayImage(85, "profile_image_id", 'name="' + $(this).data("image") + '"', getImagePath() + "/" + $(this).data("image"));
        const label_select_image = formLabel(getTextById(213), "profileimageselect_label_id");
        // Form Object of Array
        const form = [email, first_name, last_name, user_name, existing_image, label_select_image, select_new_profile_image,
            is_active, is_staff, submitButton, changePasswordButton, messageLabel];

        listoHTML.buildForm("#main-content", 6, form, "edit-user-form-id");
    })
// 5. Edit User with API when click edit user button in the form
$(document).on(
    'click',
    '.edit-user-btn',
    function () {
        const userId = $(this).data("userid");
        const email = $("#staff_edit_email").val();
        const first_name = $("#staff_edit_first_name").val();
        const last_name = $("#staff_edit_last_name").val();
        const user_name = $("#staff_edit_user_name").val();
        const image = document.getElementById("profile_image_id").getAttribute("name");
        const is_active = $("#staff_edit_is_active").prop('checked');
        const is_staff = $("#staff_edit_is_staff").prop('checked');

        // Check if assgined correct user with id
        if (!userId) {
            labelAlert(getTextById(269));
            return false;
        }
        // Email Validation
        if (!validateEmail(email)) {
            labelAlert(getTextById(268));
            return false;
        } else {
            labelAlert("");
        }
        debug("doImageUpload=" + doImageUpload, "d1a6f_5", doImageUpload);
        if ($("#edit-user-form-id").valid()) {
            // API Call to the backend with edit form Data
            if (doImageUpload === true) {
                upload(customerID, 'edit-user-form-id', function (data) {
                    const profileImage = data.rows[0].s[2];
                    apiCall_listorante_admin_editprofile(email, first_name, userId,
                        profileImage, is_active, is_staff, last_name, user_name,
                        function (data) { // If success
                            if (data._rc == 0) {
                                labelAlert(getTextById(270));
                                showUsers();
                                uploadProfileImage(false);
                            } else {
                                labelAlert(getTextById(271));
                                return false;
                            }
                        }, function (err) { // If failed
                            showHttpErrorMessage("main-content", err);
                            return false;
                        });
                }, function (err) {
                    showHttpErrorMessage('main-content', err);
                });
            } else {
                apiCall_listorante_admin_editprofile(email, first_name, userId,
                    image, is_active, is_staff, last_name, user_name,
                    function (data) { // If success
                        if (data._rc == 0) {
                            labelAlert(getTextById(270));
                            showUsers();
                        } else {
                            labelAlert(getTextById(271));
                            return false;
                        }
                    }, function (err) { // If failed
                        showHttpErrorMessage("main-content", err);
                        return false;
                    });
            }
        }
    });

$(document).on(
    "click",
    ".assign-role-to-user",
    function () {
        const userId = $(this).data("id");
        const userName = $(this).data("username");
        const userImage = formThumb(103, "", "", "", $(this).data("image"));
        const title = formTitle(getTextById(82) + ": " + userName);
        const formElements = [];
        formElements.push(title);
        formElements.push(userImage);
        apiCall_listorante_admin_userroles(function (allRolesData) {
            //debug("Check roles data:", release, allRolesData);
            apiCall_listorante_admin_getuserroles(userId,
                function (userRolesData) {
                    for (let j = 0; j < allRolesData.count; j++) {
                        let roleAssigned = false;
                        let roleTocheck = parseInt(allRolesData.rows[j].s[0]);
                        for (let k = 0; k < userRolesData.count; k++) {
                            let userRole = parseInt(userRolesData.rows[k].s[1]);
                            if (userRole === roleTocheck) {
                                roleAssigned = true;
                            }
                        }
                        let roleid = parseInt(allRolesData.rows[j].s[0]);
                        let textid;
                        switch (roleid) {
                            case 1:
                                textid = 100; //manager
                                break;
                            case 3:
                                textid = 70; //waiter
                                break;
                            case 4:
                                textid = 3; // kitchen
                                break;
                            case 5:
                                textid = 102; //guest
                                break;
                            default:
                                textid = 263; // unknown (this is an error)
                        }
                        if (roleAssigned) formElements.push(FormCheckBox(textid, "checkbox_role" + allRolesData.rows[j].s[0], "data-roleid=\"" + allRolesData.rows[j].s[0] + "\"", true));
                        else formElements.push(FormCheckBox(textid, "checkbox_role" + allRolesData.rows[j].s[0], "data-roleid=\"" + allRolesData.rows[j].s[0] + "\"", false));
                    }

                    const submitButton = FormButton(103, "assign_role", "data-userid=\"" + userId + "\"", "assign-role-btn");

                    formElements.push(submitButton);
                    listoHTML.buildForm("#main-content", 6, formElements, "role_form");
                    //listoHTML.createForm("#main-content", 6, role);
                    debug("Check role:", release, role);
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });

        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });

    });

$(document)
    .on(
        "click",
        "#assign_role",
        function () {
            const userId = $(this).data("userid");

            //const release = 0;
            apiCall_listorante_admin_userroles(function (allRoles) {
                // Checkboxes in the form
                const checkboxes = [];
                for (let r = 0; r < allRoles.count; r++) {
                    checkboxes.push(allRoles.rows[r].s[0]);
                }
                //debug("checkboxes for roles to assign/revoke ", release, checkboxes);
                apiCall_listorante_admin_getuserroles(
                    userId,
                    function (data) {
                        let flagged = false;
                        for (let i = 0; i < checkboxes.length; i++) {
                            //debug("CHECK flagged:", release, flagged);
                            let roleID = parseInt(checkboxes[i]);
                            flagged = false;
                            let chk = document.getElementById("checkbox_role" + roleID).checked;
                            for (let j = 0; j < data.rows.length; j++) {
                                //let roleID = parseInt($(checkboxes[i]).prop("id"));
                                let userRoleID = parseInt(data.rows[j].s[1]);
                                //debug("roles", release, roleID, userRoleID);
                                if (roleID === userRoleID) {
                                    debug("chk=" + chk, release, chk, roleID);
                                    if (chk != true) {
                                        apiCall_listorante_admin_revokerole(roleID, userId,
                                            function (data) {
                                                //debug("CHECK roles-data:", release, data);
                                            },
                                            function (err) {
                                                showHttpErrorMessage("main-content", err);
                                            });
                                    }
                                    flagged = true;
                                    break;
                                }
                            }
                            //debug("CHECK flag in assign-roles:", release, flagged);
                            debug("chk", release, flagged, chk, roleID);
                            if (flagged === false && chk === true) {
                                apiCall_listorante_admin_assignrole(roleID, userId, function (data) {
                                    //debug("CHECK roles-data:", release, data);
                                }, function (err) {
                                    showHttpErrorMessage("main-content", err);
                                });
                            }
                        }
                        showUsers();
                    }, function (err) {
                        showHttpErrorMessage("main-content", err);
                    });
            })
        });


$(document).on(
    'click',
    '.set-Password',
    function () {
        const password = formTextInput(81, "staff_edit_password", "rows='1' required ", '');
        const passwordConfirm = formTextInput(81, "staff_confirm_password", "rows='1' required ", '');
        const hidden_user_id = formHiddenInput("", $(this).data('userid'), 'id_of_edited_user');

        const submitButton = FormButton(105, 'change_password_button', '', 'change_password');
        const messageLabel = formLabel('', 'messageLabelID');
        const form = [password, passwordConfirm, hidden_user_id, submitButton, messageLabel];
        listoHTML.buildForm('#main-content', 6, form, 'change_password');

    });


$(document).on(
    'click',
    '.change_password',
    function () {
        const password = $('#staff_edit_password').val();
        const passwordConfirm = $('#staff_confirm_password').val();
        const userID = $('#id_of_edited_user').val();
        if ($("#change_password").valid()) {
            if (password === passwordConfirm) {
                apiCall_listorante_admin_changeuserpassword(userID, password, function (data) {
                    document.getElementById('messageLabelID').innerText = getTextById(609);
                }, function (err) {
                    showHttpErrorMessage('main-content', err);
                });
            } else {
                document.getElementById('messageLabelID').innerText = getTextById(608);
            }
        }
    });/*-----------------------------------*/ 
/* product-functions.js */ 

module = "product-functions.js";

/*
 * Admin-Menu/ Function 4 / PRODUCTS MODULE
 *
 *
 *
 *
 *
 *
 */
// SHOW PRODUCTS LIST - event
$(document).on('click', '.product-list', function () {
    showProducts();
});


const showProducts = function () {
    apiCall_listorante_public_products(function (data) {
        const action1 = [
            "edit_element_product",
            actionModifyStyle.elements[0] + getTextById(105)
            + actionModifyStyle.elements[1]];
        const action2 = [
            "delete_element_product",
            actionDeleteStyle.elements[0] + getTextById(106)
            + actionDeleteStyle.elements[1]];
        const action3 = ["product_gallery", actionModifyStyle.elements[0] + getTextById(318) + actionModifyStyle.elements[1]];
        const action4 = ["product_attributes", actionModifyStyle.elements[0] + getTextById(320) + actionModifyStyle.elements[1]];
        const dataColumnNames1 = ["idproduct", "prodname", "price",
            "image", "image_thumb", "prodname_en",
            "prodinfo_en", "proddesc_en", "position",
            "qty_in_stock", "qty_unit", "threshold_qty", "idlabel"];
        const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
            11, 12];
        const dataColumnNames2 = ["idproduct"];
        const dataColumnIndices2 = [0];
        const ProductObject = {
            IDENTIFIER: "admin-showProducts",
            jsonRMLData: data,
            header: [getTextById(61),"ID1","ID2", getTextById(51),
                getTextById(57), getTextById(52),"",""],
            columnIndices: [5,0, 12, 2, 4, 9],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (colIndex === 4) {
                        return TDopen + "<img src='" + getImagePath() + "/" + data
                            + "' " + ThumbStyle.default + ">" + TDclose;
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(ProductObject, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
};



// 1) Add Product Form
$(document).on(
    'click',
    '.create-addproduct-form',
    function () {
        const idcategory = [FormSelectBox, 53, "product_add_idcategory",
            "rows='1' required ", ''];
        const proddesc_locale = [FormTextArea, 55,
            "product_add_proddesc_en", "rows='3' required ", ''];
        const prodinfo_locale = [FormTextArea, 54,
            "product_add_prodinfo_en", "rows='2' required ", ''];
        const prodlabel = [formTextInput, 233,
            "product_add_prodlabel", "rows='1' required ", ''];
        const prodname_locale = [formTextInput, 61,
            "product_add_prodname_en", "rows='2' required ", ''];
        const image = [formImage, 230, "file_imagefield", "required", "", "fileImage_id"];
        const qty_unit = [formSelectBox, 59, "product_edit_qty_unit",
            "product_add_qty_unit", getTextById(311), getTextById(312), getTextById(313),
            getTextById(314), getTextById(315), getTextById(316), getTextById(317),];
        const threshold_qty = [formNumericInput, 60, "product_add_threshold_qty", "required", ""];
        const price = [formNumericInput, 51, "product_add_price", "required step=.01", ""];
        const qty_in_stock = [formNumericInput, 52, "product_add_qty_in_stock", "required step=1"];
        const submitButton = [FormButton, 215, "add_product", "",
            "add-product"];
        const form1 = [prodname_locale, prodlabel, idcategory,
            image,
            price,
            prodinfo_locale, proddesc_locale,
            qty_in_stock, qty_unit, threshold_qty,
            submitButton];
        listoHTML.createForm("#main-content", 6, form1, 'product-edit-form');
        const categoriesData = [];
        const dupArray = [];
        const prodCatData = [];
        apiCall_listorante_public_categories(function (response) {
            sessionStorage.setItem('uniqueID', '');
            for (let i = 0; i < response.count; i++) {
                const billdata = {};
                if (sessionStorage.getItem('uniqueID') == response.rows[i].s[1]) {
                    if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                        billdata.id = i + 'rep_' + response.rows[i].s[1];
                        dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                    } else {
                        billdata.id = 'rep_' + response.rows[i].s[1];
                    }
                    dupArray.push(response.rows[i].s[1]);
                } else {
                    billdata.id = 'cat_' + response.rows[i].s[1];
                }
                if (response.rows[i].s[0] == 0) {
                    billdata.parent = '#';
                } else {
                    if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = 'rep_' + response.rows[i].s[0];
                    } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                        billdata.parent = i + 'rep_' + response.rows[i].s[0];
                    } else {
                        billdata.parent = 'cat_' + response.rows[i].s[0];
                    }
                }
                billdata.text =  response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
                for (let pCat = 0; pCat < prodCatData.length; pCat++) {
                    debug("prodcatData: ", release, prodCatData[pCat], billdata.id);
                    if (prodCatData[pCat] == response.rows[i].s[1]) {
                        const nodeState = {"opened": true, "checked": true};
                        billdata.state = nodeState;
                    }
                }
                categoriesData.push(billdata);
                sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
            }
            $('#jstree').jstree({
                'core': {
                    'multiple': true,
                    'data': categoriesData,
                    'check_callback': false
                }, types: {
                    "root": {
                        "icon": "glyphicon glyphicon-record"
                    },
                    "child": {
                        "icon": "glyphicon glyphicon-minus"
                    },
                    "default": {
                        "icon": "glyphicon glyphicon-cutlery"
                    }
                },
                "checkbox": {
                    "three_state": false,
                    "cascade": "up",
                    "tie_selection": false
                },
                plugins: ["types", "data", "checkbox"]
            }).on('open_node.jstree', function (e, data) {
                data.instance.set_icon(data.node, "glyphicon glyphicon-minus");
            }).on('close_node.jstree', function (e, data) {
                data.instance.set_icon(data.node, "glyphicon glyphicon-cutlery");
            }).on("select_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                //$('#product_category').val(idcategory);
            }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
                const cNode = "" + data.node.id;
                const underlineIdx = cNode.indexOf("_") + 1;
                const idcategory = cNode.substring(underlineIdx, cNode.length);
                //$('#product_category').val(idcategory);
                (data.node.state.checked ?
                    select_category_of_new_product(idcategory)
                    :
                    unselect_category_of_new_product(idcategory))
            });

        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
    });

let categories_of_new_product = [];

function select_category_of_new_product(selectedCategory) {
    debug("Add product, selected category:", release, selectedCategory);
    categories_of_new_product.push(selectedCategory);
}

function unselect_category_of_new_product(unSelected_category) {
    debug("Add product, unselect category:", release, unSelected_category);
    //indexof
    //categories_of_new_product.slice(idx,1,unSelected category_);
}

// SUBMIT ADD PRODUCT FORM

$(document).on(
    'click',
    '#add_product',
    function () {
        const uuid = getUuidv4();
        debug(uuid);
        if ($("#product-edit-form").valid()) {
            const idcategory = $("#product_add_idcategory").val();
            if (categories_of_new_product.length === 0) {
                alert("Please select at least one category for the product");
                return;
            }
            const proddesc_locale = $("#product_add_proddesc_en").val();
            const prodname_locale = $("#product_add_prodname_en").val();
            const prodlabel = $("#product_add_prodlabel").val();
            const prodinfo_locale = $("#product_add_prodinfo_en").val();
            const qty_unit = $("#product_add_qty_unit").val();
            const threshold_qty = $("#product_add_threshold_qty").val();
            const price = $("#product_add_price").val();
            const qty_in_stock = $("#product_add_qty_in_stock").val();
            const custID = getCustomerId();
            debug("Product label:", 0, prodlabel);
            if (document.getElementById('file_imagefield').value) {
                upload(custID, 'product-edit-form', function (data) {
                    debug("upload-data:", release, data);
                    const imageNew = "" + data.rows[0].s[2];
                    //alert(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'");
                    apiCall_listorante_admin_newproduct(price, proddesc_locale, imageNew, prodinfo_locale, prodlabel, prodname_locale,
                        qty_in_stock, threshold_qty, qty_unit, function (data) {
                            if (data._rc > 0) {
                                alert(getTextById(253) + data._rc);
                            } else {
                                showProducts();
                                debug("Add-product", 0, data);
                            }
                        }, function err() {
                            showHttpErrorMessage("main-content", err);
                        });
                }, function (error) {
                    alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
                });
            } else {
                const noImage = images.missing_image_txt;
                apiCall_listorante_admin_newproduct(price, proddesc_locale, noImage, prodinfo_locale, prodlabel, prodname_locale,
                    qty_in_stock, threshold_qty, qty_unit, function (data) {
                        if (data._rc > 0) {
                            alert(getTextById(253) + data._rc);
                        } else {
                            showProducts();
                            debug("Add-product", 0, data);
                        }
                    }, function err() {
                        showHttpErrorMessage("main-content", err);
                    });
            }
            const numberOfNewProducts = categories_of_new_product.length;
            for (let k = 0; k < numberOfNewProducts; k++) {
                debug("Add new pcategory for product id " + uuid, 0, categories_of_new_product[k]);
            }
        } else {
            debug("Invalid form: missing fields detected in #add-category-form", 0);
        }
    });

function getUuidv4() {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
}

// 2. Create form with editable Product data
$(document).on(
    'click',
    '.edit_element_product',
    function () {
        sessionStorage.setItem('imagePath', '');
        const product_id = $(this).data('idproduct');
        const selectedCategory = $(this).data('idcategory');
        const idlabel = [formTextInput, 233, 'idlabel', 'required', $(this).data('idlabel')];
        const idcategory = [FormSelectBox, 53, 'product_edit_idcategory',
            'rows="1" required ', selectedCategory];
        const imageSrc = $(this).data('image');
        sessionStorage.setItem('image', imageSrc);
        // TODO: use layout-identifier for specific product-visualization
        //const idStyle = [formNumericInput, 262, "product_edit_style", $(this).data('idprodstyle')];
        const proddesc_locale = [FormTextArea, 55,
            "product_edit_proddesc_en", "rows='1' required ",
            $(this).data("proddesc_en")];
        const prodinfo_locale = [FormTextArea, 54,
            "product_edit_prodinfo_en", "rows='1' required ",
            $(this).data("prodinfo_en")];
        const displayedProdname = $(this).data("prodname_en");
        const prodname_locale = [formTextInput, 61,
            "product_edit_prodname_en", "rows=2 required ",
            displayedProdname];
        const shownImage = [formDisplayImage, 56, "shownImage", "longdesc=\"" + imageSrc + "\"", getImagePath() + "/" + imageSrc];
        const image = [formImage, 56, "file_imagefield", "",
            "", $(this).data("image")];
        const image_thumb = [formThumb, 57, "product_file_thumb",
            "required ", "", $(this).data("image_thumb")];
        const qty_unit = [formSelectBox, 59, "",
            "product_edit_qty_unit", getTextById(311), getTextById(312), getTextById(313),
            getTextById(314), getTextById(315), getTextById(316), getTextById(317),];
        const threshold_qty = [formNumericInput, 60, "product_edit_threshold_qty", "required step=1", $(this).data("threshold_qty")];
        const price = [formNumericInput, 51, "product_edit_price",
            "required step=.01", $(this).data("price")];
        const qty_in_stock = [formNumericInput, 52, "product_edit_qty_in_stock",
            "required step=1", $(this).data("qty_in_stock")];
        const listPosition = [formNumericInput, 58, "product_edit_position_inlist",
            "required step=1", $(this).data("position")];
        const external_link = [formTextInput, 616, "product_edit_external_link",
            "", $(this).data("extLink")];
        const submitButton = [FormButton, 215, "edit_product",
            "data-idproduct=\"" + product_id + "\"", "edit_product"];
        const galleryButton = [FormButton, 318, "product_gallery", "data-idproduct=\"" + product_id + "\"", "product_gallery"];
        const attributeButton = [FormButton, 320, "product_attributes", "data-idproduct=\"" + product_id + "\"", "product_attributes"];
        const cancelButton = [FormButton,602, "backtoAllProductList", "", "cancel-button"];
        const form1 = [prodname_locale, idlabel, idcategory, shownImage, image, image_thumb, price,
            prodinfo_locale, proddesc_locale,
            qty_in_stock, qty_unit, threshold_qty,listPosition,external_link,
            submitButton, galleryButton, attributeButton, cancelButton];
        listoHTML.createForm("#main-content", 6, form1, 'product-edit-form');

        const prodCatData = [];
        const parentCatData = [];
        apiCall_listorante_public_productcategories(product_id, function (data) {
            if (data.count === 0){
                createProductTree(product_id,[],[]);
            }
            for (let prodcatIdx = 0; prodcatIdx < data.count; prodcatIdx++) {
                prodCatData.push(data.rows[prodcatIdx].s[3]);
            }
            let category;
            for (let prodcatIdx = 0; prodcatIdx < data.count; prodcatIdx++) {

                category=data.rows[prodcatIdx].s[3];
                apiCall_listorante_public_parentcategories(category, function (parentData) {
                    for (let i = 0; i < parentData.count; i++) {
                        if (!parentCatData.includes(parentData.rows[i].s[0])) parentCatData.push(parentData.rows[i].s[0]);
                    }
                    if (prodcatIdx === data.count - 1 ) {
                        debug("prodCatData:", 0, prodCatData);
                        debug("parentCatData:", 0, parentCatData);
                        createProductTree(product_id,prodCatData,parentCatData);
                    }
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                })
            }

        }, function (err) {
            debug("Cannot find category for product-id:", 0, product_id, err);
        });


    });

function createProductTree(product_id,prodCatData,parentCatData){
    const categoriesData = [];
    const dupArray = [];
    apiCall_listorante_public_categories(function (response) {
        sessionStorage.setItem('uniqueID', '');
        for (let i = 0; i < response.count; i++) {
            const billdata = {};
            if (sessionStorage.getItem('uniqueID') == response.rows[i].s[1]) {
                if ($.inArray(response.rows[i].s[1], dupArray) !== -1) {
                    billdata.id = i + 'rep_' + response.rows[i].s[1];
                    dupArray.push(i + 'rep_' + response.rows[i].s[1]);
                } else {
                    billdata.id = 'rep_' + response.rows[i].s[1];
                }
                dupArray.push(response.rows[i].s[1]);
            } else {
                billdata.id = 'cat_' + response.rows[i].s[1];
            }
            //billdata.id=response.rows[i].s[0] ;
            if (response.rows[i].s[0] == 0) {
                billdata.parent = '#';
            }
            else {
                //billdata.parent = response.rows[i].s[0];
                if ($.inArray(response.rows[i].s[0], dupArray) !== -1) {
                    billdata.parent = 'rep_' + response.rows[i].s[0];
                } else if ($.inArray(i + 'rep_' + response.rows[i].s[0], dupArray) !== -1) {
                    billdata.parent = i + 'rep_' + response.rows[i].s[0];
                } else {
                    billdata.parent = 'cat_' + response.rows[i].s[0];
                }
            }
            billdata.text = response.rows[i].s[11] + " [" + response.rows[i].s[1] + ": " + response.rows[i].s[3] + " ]";
            let nodeState = {};
            if (prodCatData.includes(response.rows[i].s[1])) {
                nodeState = {"opened": true, "checked": true};
            } else if (parentCatData.includes(response.rows[i].s[1])) {
                nodeState = {"opened": true, "checked": false};
            } else nodeState = {"opened": false, "checked": false};
            billdata.state = nodeState;
            categoriesData.push(billdata);
            sessionStorage.setItem('uniqueID', response.rows[i].s[1]);
        }
        //debug("Check tree-data", 0, categoriesData);
        $('#jstree').jstree({
            'core': {
                'data': categoriesData,
                'check_callback': false
            }, types: {
                "root": {
                    "icon": "glyphicon glyphicon-record"
                },
                "child": {
                    "icon": "glyphicon glyphicon-minus"
                },
                "default": {
                    "icon": "glyphicon glyphicon-cutlery"
                }
            },
            "checkbox": {
                "three_state": false,
                "whole_node": false,
                //"cascade": "undetermined",
                "tie_selection": false
            },
            plugins: ["types", "changed", "data", "checkbox"]
        }).on('open_node.jstree', function (e, data) {
            data.instance.set_icon(data.node, "glyphicon glyphicon-minus");
        }).on('close_node.jstree', function (e, data) {
            data.instance.set_icon(data.node, "glyphicon glyphicon-cutlery");
        }).on("check_node.jstree uncheck_node.jstree", function (e, data) {
            const cNode = "" + data.node.id;
            const underlineIdx = cNode.indexOf("_") + 1;
            const idcategory = cNode.substring(underlineIdx, cNode.length);
            (data.node.state.checked ?
                setProductCategory(idcategory, product_id, data)
                :
                unSetProductCategory(idcategory, product_id, data))
        });
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

function setProductCategory(idcategory, idproduct, treeData) {
    apiCall_listorante_admin_setproductcategory(idcategory, idproduct, function (data) {
        debug('setProductCategory', 0, idproduct + '->' + idcategory);
        if (data._rc > 0) {
            alert(getTextById(265));
            treeData.node.state.checked = false;
        }
    }, function (err) {
        treeData.node.state.checked = false;
        showHttpErrorMessage("main-content", err);
        alert(getTextById(265));
    });
};


function unSetProductCategory(idcategory, idproduct, treeData) {
    apiCall_listorante_admin_unsetproductcategory(idcategory, idproduct, function (data) {
        debug('unSetProductCategory', 0, idproduct + '->' + idcategory);
        if (data._rc > 0) {
            alert(getTextById(266));
            treeData.node.state.checked = true;
        }
    }, function (err) {
        treeData.node.state.checked = true;
        showHttpErrorMessage("main-content", err);
        alert(getTextById(266));
    });
}


// 3. Edit product-data with the form and submit
$(document)
    .on(
        'click',
        '.edit_product',
        function () {

            if ($("#product-edit-form").valid()) {
                // language=HTML
                let image;
                if (document.getElementById("file_imageField"))
                    image = document.getElementById("file_imageField").value;
                const proddesc_locale = document
                    .getElementById("product_edit_proddesc_en").value;
                debug("image in valid-form:" + image);
                const idproduct = $(this).data("idproduct");
                const productLabel = document
                    .getElementById("idlabel").value;

                const prodinfo_locale = document
                    .getElementById("product_edit_prodinfo_en").value;

                const prodname_locale = document
                    .getElementById("product_edit_prodname_en").value;
                const price = document.getElementById("product_edit_price").value;
                // TODO: use own style for every product, by considering the style-id
                //const style = document.getElementById("product_edit_style").value;
                const qty_in_stock = document
                    .getElementById("product_edit_qty_in_stock").value;
                const listPosition= document
                    .getElementById("product_edit_position_inlist").value;
                debug("listPosition="+listPosition);
                sessionStorage.getItem("image");

                const qty_unit_element = document
                    .getElementById("product_edit_qty_unit");
                const qty_unit = qty_unit_element.options[qty_unit_element.selectedIndex].text;
                const threshold_qty = document
                    .getElementById("product_edit_threshold_qty").value;
                const custID = getCustomerId();
                if (document.getElementById('file_imagefield').value) {
                    upload(custID, 'product-edit-form', function (data) {
                        debug("upload-data:", release, data);
                        const imageNew = "" + data.rows[0].s[2];
                        const image_thumbNew = imageNew;
                        debug(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'", 0);
                        apiCall_listorante_admin_editproduct(productLabel, -1, idproduct,
                            imageNew, image_thumbNew, listPosition, price, proddesc_locale,
                            prodinfo_locale, prodname_locale, prodname_locale,
                            qty_in_stock, qty_unit, threshold_qty, function (data2) {
                                if (data2._rc > 0) {
                                    alert(getTextById(253) + data2._rc);
                                } else {
                                    showProducts();
                                }
                            }, function (err) {
                                showHttpErrorMessage("main-content", err);
                            });
                    }, function (error) {
                        alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
                    });
                } else {
                    image = document.getElementById("shownImage").getAttribute("longdesc");
                    debug("image in edit_product:", 0, image);
                    if (!image)
                        image = images.missing_image_txt;
                    apiCall_listorante_admin_editproduct(productLabel, -1, idproduct,
                        image, image, listPosition, price, proddesc_locale,
                        prodinfo_locale, prodname_locale, prodname_locale,
                        qty_in_stock, qty_unit, threshold_qty, function (data) {
                            if (data._rc > 0) {
                                alert(getTextById(253) + data._rc);
                            } else {
                                showProducts();
                            }
                        }, function (err) {
                            showHttpErrorMessage("main-content", err);
                        });
                }
            } else {
                debug("Invalid Form: Missing fields detected in 'edit-product'", 0);
            }
        });


/* FUNCTION FOR DELETING THE PRODUCT */

$(document)
    .on(
        'click',
        '.delete_element_product',
        function () {
            const productId = $(this).data("idproduct");
            apiCall_listorante_admin_deleteproduct(productId, function (data) {
                if (data._rc > 0) {
                    alert(getTextById(253) + data._rc);
                } else {
                    showProducts();
                }

            }, function (err) {
                showHttpErrorMessage("main-content", err);
            });
        }
    );


// BACK TO ALL PRODUCTS LIST
$(document).on('click', '#backtoAllProductList', function(){
        showProducts();
});/*-----------------------------------*/ 
/* product_attributes.js */ 
module = "product-attributes.js";

/*
 * Admin-Menu/ Function 4 / PRODUCTS ATTRIBUTE MODULE
 *
 *
 *
 *
 *
 *
 */
function showGallery(idProduct) {

    const image = [formImage, 56, "file_imagefield", "required", "", "fileImage_id"];
    const submitButton = [FormButton, 319, "add_image", "", "add-image"];
    const hiddenField = [formHiddenInput, "hiddenProductID", idProduct, "productID_" + idProduct];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const form1 = [image, hiddenField, submitButton, tableDiv];

    listoHTML.createForm("#main-content", 8, form1, 'product-gallery-form');


    apiCall_listorante_public_productimages(idProduct, function (data) {
        //debug(data);
        if (data.count > 0) {
            const action1 = ["deleteImageIcon", actionModifyStyle.elements[0] + getTextById(106) + actionModifyStyle.elements[1]];

            const dataColumnNames1 = ["idlnk_product_image", "idproduct", "imagepath"];
            const dataColumnIndices1 = [0, 1, 2];
            const ProductObject = {
                IDENTIFIER: "admin-showProductGallery",
                jsonRMLData: data,
                header: [getTextById(321)],
                columnIndices: [2],
                actions: [action1],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1],
                rowFormat: function (rowIndex, dataArray) {
                    return dataArray;
                },
                cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                    if (colIndex === 0) {
                        return TDopen + "<img src='" + getImagePath() + "/" + data + "' width='200' >" + TDclose;
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            };
            listoHTML.createTable.call(ProductObject, "tableDiv", true);
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });

}

// PRODUCT GALLERY - event
$(document).on('click', '.product_gallery', function () {
    const idProduct = $(this).data('idproduct');
    showGallery(idProduct);
});


$(document).on('click', '.deleteImageIcon', function () {
    const idlnk_product_image = $(this).data('idlnk_product_image');
    const idProduct = $(this).data('idproduct');
    //debug(idlnk_product_image);
    apiCall_listorante_admin_deleteproductimage(idlnk_product_image, function (data) {
        showGallery(idProduct);
    }, function (error) {
        alert("Not Able to Delete The Image from Product Gallery");
    });
});


$(document).on('click', '#add_image', function () {
    const idProduct = $('.hiddenProductID').val();
    const custID = getCustomerId();
    if (document.getElementById('file_imagefield').value) {
        debug('ok');
        upload(custID, 'product-gallery-form', function (data) {
            debug("upload-data:", release, data);
            const imageNew = "" + data.rows[0].s[2];
            const image_thumbNew = imageNew;
            alert(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'");
            apiCall_listorante_admin_insertproductimage(idProduct, imageNew, function (data) {
                if (data._rc > 0) {
                    alert(getTextById(253) + data._rc);
                } else {
                    showGallery(idProduct);
                }
            }, function err() {
                showHttpErrorMessage("main-content", err);
            });
        }, function (error) {
            alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
        });
    }
});


// PRODUCT Attributes - event
$(document).on('click', '.product_attributes', function () {
    const idProduct = $(this).data('idproduct');
    showproductattributes(idProduct);
});


function showproductattributes(idProduct) {
    const newattributeButton = [FormButton, 322, "add_attribute", "", "add-attribute"];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const hiddenField = [formHiddenInput, "hiddenProductID", idProduct, "productID_" + idProduct];
    const form1 = [hiddenField, newattributeButton, tableDiv];
    listoHTML.createForm("#main-content", 12, form1, 'add-attribute-form-button');


    apiCall_listorante_public_getproductattributes(idProduct, function (data) {
        debug(data);
        const action1 = ["editattribute", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
        const action2 = ["deassignattribute", actionDeleteStyle.elements[0] + getTextById(329) + actionDeleteStyle.elements[1]];
        const countRecords = data.count;
        const dataColumnNames1 = ["idproduct", "prodname", "idattribute", "attributemnemonic", "productattributename_" + locale, "productattributevalue_" + locale];
        const dataColumnIndices1 = [0, 1, 2, 3, 4, 5];
        const ProductObject = {
            IDENTIFIER: "admin-showProductAttributes",
            jsonRMLData: data,
            header: [getTextById(50), getTextById(323), getTextById(324)],
            columnIndices: [1, 4, 5],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices1],
            dataColumnNames: [dataColumnNames1, dataColumnNames1],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (countRecords < 1) {
                        return "<td colspan='4'>" + getTextById(325) + "</td>";
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(ProductObject, "tableDiv", true);
    }, function (err) {

    });
}


$(document).on('click', '.deleteattribute', function () {
    const idproductattributename = $(this).data('idproductattributename');
    const productID = $('.hiddenProductID').val();
    apiCall_listorante_admin_deleteattributedefinition(idproductattributename, function (data) {
        listallattributestable(productID);
    }, function (err) {

    });
});

// FUNCTION FOR LISTING ATTRIBUTES AND NEW ATTRIBUTE BUTTON

function listallattributestable(productID) {

    const newattributeButton = [FormButton, 328, "add_new_attribute", "", "add-new-attribute"];
    const cancelButton = [FormButton, 602, "backtoAttributeList", "", "cancel-button"];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const hiddenField = [formHiddenInput, "hiddenProductID", productID, "productID_" + productID];
    const form1 = [hiddenField, newattributeButton, cancelButton, tableDiv];
    listoHTML.createForm("#main-content", 12, form1, 'add-new-attribute-form-button');


    apiCall_listorante_public_allproductattributes(function (data) {
        debug(data);
        const action1 = ["editattribute", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
        const action2 = ["deleteattribute", actionDeleteStyle.elements[0] + getTextById(106) + actionDeleteStyle.elements[1]];
        const action3 = ["selectattribute", actionModifyStyle.elements[0] + getTextById(326) + actionModifyStyle.elements[1]];
        const countRecords = data.count;
        const dataColumnNames1 = ["idproductattributename", "attributemnemonic", "productattributename_" + locale];
        const dataColumnIndices1 = [0, 1, 2];
        const ProductObject = {
            IDENTIFIER: "admin-setProductAttributes",
            jsonRMLData: data,
            header: [getTextById(327), getTextById(323) + '_' + locale],
            columnIndices: [1, 2],
            actions: [action3, action2],
            dataColumnIndices: [dataColumnIndices1, dataColumnIndices1],
            dataColumnNames: [dataColumnNames1, dataColumnNames1],
            rowFormat: function (rowIndex, dataArray) {
                return dataArray;
            },
            cellFormat: function (colIndex, rowIndex, TDopen, data, TDclose) {
                {
                    if (countRecords < 1) {
                        return "<td colspan='3'>" + getTextById(325) + "</td>";
                    } else {
                        return TDopen + data + TDclose;
                    }
                }
            }
        };
        listoHTML.createTable.call(ProductObject, "tableDiv");
    }, function (err) {

    });
}


// ADD ATTRIBUTE BUTTON CLICK EVENT

$(document).on('click', '.add-attribute', function () {
    const productID = $('.hiddenProductID').val();
    listallattributestable(productID);
});


// BACK TO ALL ATTRIBUTE LIST
$(document).on('click', '#backtoAllAttributeList', function () {
    const productID = $('.hiddenProductID').val();
    listallattributestable(productID);
});


// DEFINE NEW ATTRIBUTE BUTTON 

$(document).on('click', '#add_new_attribute', function () {
    const productID = $('.hiddenProductID').val();
    const attributemnemonic = [formTextInput, 327, "attributemnemonic", " required ", ''];
    const productattributename_locale = [formTextInput, 323, "productattributename_locale", " required ", ''];
    const cancelButton = [FormButton, 602, "backtoAllAttributeList", "", "cancel-button"];
    const newattributeButton = [FormButton, 215, "save_new_attribute", "", "save-new-attribute"];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const hiddenField = [formHiddenInput, "hiddenProductID", productID, "productID_" + productID];
    const form1 = [attributemnemonic, productattributename_locale, hiddenField, newattributeButton, cancelButton, tableDiv];
    listoHTML.createForm("#main-content", 8, form1, 'add-new-attribute-form-button');
});


$(document).on('click', '#save_new_attribute', function () {
    const productID = $('.hiddenProductID').val();
    const attributemnemonic = $('#attributemnemonic').val();
    const productattributename_locale = $('#productattributename_locale').val();
    apiCall_listorante_admin_defineattribute(attributemnemonic, productattributename_locale, function (data) {
        debug(data);
        listallattributestable(productID);
    }, function (err) {

    });
});

// SELECT ATTRIBUTE FOR PRODUCT 

$(document).on('click', '.selectattribute', function () {
    const productID = $('.hiddenProductID').val();
    const idproductattributeID = $(this).data('idproductattributename');
    const attributemnemonic = [formTextInput, 327, "attributemnemonic", " required ", $(this).data('attributemnemonic')];
    const productattributename_locale = [formTextInput, 323, "productattributename_locale", " required ", $(this).data('productattributename_' + locale)];
    const productattributevalue = [formTextInput, 324, "productattributevalue", " required ", ''];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const newattributeButton = [FormButton, 215, "set_new_attribute", "", "set-new-attribute"];
    const hiddenField = [formHiddenInput, "hiddenProductID", productID, "productID_" + productID];
    const hiddenAttributeID = [formHiddenInput, "hiddenAttributeID", idproductattributeID, "attributeID_" + idproductattributeID];
    const form1 = [attributemnemonic, productattributename_locale, productattributevalue, hiddenAttributeID, hiddenField, newattributeButton, tableDiv];
    listoHTML.createForm("#main-content", 8, form1, 'update-attribute-form-button');


});

// SUBMITTING SET ATTRIBUTE FORM

$(document).on('click', '.set-new-attribute', function () {
    const idproduct = $('.hiddenProductID').val();
    const idattribute = $('.hiddenAttributeID').val();
    const attributemnemonic = $('#attributemnemonic').val();
    const productattributename_locale = $('#productattributename_locale').val();
    const productattributevalue_locale = $('#productattributevalue').val();
    apiCall_listorante_admin_setattribute(idattribute, idproduct, productattributevalue_locale, function (data) {
        debug(data);
        showproductattributes(idproduct);
    }, function (err) {

    });
});


// EDIT ATTRIBUTE VALUE

$(document).on('click', '.editattribute', function () {
    debug('Clicked');
    const idproduct = $('.hiddenProductID').val();
    const idattribute = $(this).data('idattribute');

    const attributemnemonic = [formTextInput, 327, "attributemnemonic", " required readonly ", $(this).data('attributemnemonic')];
    const productattributename_locale = [formTextInput, 323, "productattributename_locale", " required readonly ", $(this).data('productattributename_' + locale)];
    const productattributevalue = [formTextInput, 324, "productattributevalue", " required ", $(this).data('productattributevalue_' + locale)];
    const tableDiv = [formRowDiw, "tableDiv", 12];
    const newattributeButton = [FormButton, 215, "update_attribute_value", "", "update-attribute-value"];
    const hiddenField = [formHiddenInput, "hiddenProductID", idproduct, "productID_" + idproduct];
    const hiddenAttributeID = [formHiddenInput, "hiddenAttributeID", idattribute, "attributeID_" + idattribute];
    const cancelButton = [FormButton, 602, "backtoAttributeList", "", "cancel-button"];
    const form1 = [attributemnemonic, productattributename_locale, productattributevalue, hiddenAttributeID, hiddenField, newattributeButton, cancelButton, tableDiv];
    listoHTML.createForm("#main-content", 8, form1, 'update-attribute-form-button');
});

/* BACK TO LIST */
$(document).on('click', '#backtoAttributeList', function () {
    const productID = $('.hiddenProductID').val();
    showproductattributes(productID);
});

// SUBMITTING EDIT ATTRIBUTE VALUE FORM

$(document).on('click', '.update-attribute-value', function () {
    const idproduct = $('.hiddenProductID').val();
    const idattribute = $('.hiddenAttributeID').val();
    const attributemnemonic = $('#attributemnemonic').val();
    const productattributename_locale = $('#productattributename_locale').val();
    const productattributevalue_locale = $('#productattributevalue').val();
    apiCall_listorante_admin_editattribute(idattribute, idproduct, productattributevalue_locale, function (data) {
        debug(data);
        showproductattributes(idproduct);
    }, function (err) {

    });
});

// DEASSIGN ATTRIBUTE FROM PRODUCT

$(document).on('click', '.deassignattribute', function () {
    const idproduct = $(this).data('idproduct');
    const idattribute = $(this).data('idattribute');
    apiCall_listorante_admin_unsetattribute(idattribute, idproduct, function (data) {
        debug(data);
        showproductattributes(idproduct);
    }, function (err) {

    });
});/*-----------------------------------*/ 
/* support.js */ 
module = "support.js";

/**** SUPPORT MODULE ****/





function supportButtonClicked() {
        const customerID = getCustomerId();
        const tableDivCreateTicket = formRowDiw("tableDivCreate", 12);
        const tableDivSelectTicket = formRowDiw("tableDivSelect", 12);
        const form = [tableDivCreateTicket, tableDivSelectTicket];
        listoHTML.buildForm("#main-content", 12, form, 'select_or_create_ticket_form');
        apiCall_listorante_admin_tickets(customerID, function (data) {
            const action1 = ["supportIcon", actionSupportStyle.elements[0]];
            const dataColumnNames1 = ["id", "ticketdef", "date_posted", "ticketstatus"];
            const dataColumnIndices1 = [0, 1, 2, 3];
            const ticketsTableObject = {
                IDENTIFIER: "admin-support-ticket-table",
                jsonRMLData: data,
                header: [getTextById(331), getTextById(233), getTextById(332), getTextById(333), ""],
                columnIndices: [0, 1, 2, 3],
                actions: [action1],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1]
            };
            apiCall_listorante_admin_ticketdef(function (data) {
                const createTicketAction = ["createTicket", createTicketStyle.elements[0]];
                const ticketActionDataIndex = [0];
                const ticketActionDataName = ["ticketDefinition"];
                const ticketDefsTableObject = {
                    IDENTIFIER: "admin-support-ticketDef-table",
                    jsonRMLData: data,
                    header: [getTextById(233), '', '', ''],
                    columnIndices: [0, 1, 2],
                    actions: [createTicketAction],
                    dataColumnIndices: [ticketActionDataIndex],
                    dataColumnNames: [ticketActionDataName]
                };
                listoHTML.createTable.call(ticketDefsTableObject, "tableDivCreate", true);
                listoHTML.createTable.call(ticketsTableObject, "tableDivSelect", false);
            }, function (err) {
                showHttpErrorMessage((err.status));
            });
        }, function (err) { // If failed
            showHttpErrorMessage("main-content", err);
        });


}


function supportServiceClicked(ticketId) {
    const ticketText = FormTextArea(336, "ticketText", "rows='3' required ", '');
    const attachedFile = formImage(337, "file_imagefield", "required", "", "fileImage_id");
    const submitButton = FormButton(339, "submit_ticket", "", "submit-ticket");
    const cancelButton = FormButton(602, "back", "", "support-button");
    const hiddenField = formHiddenInput("hiddenticketID", ticketId, "ticketID_" + ticketId);
    const tableDiv = formRowDiw("tableDiv", 12);

    apiCall_listorante_admin_ticketdata(ticketId, function (data) {
        console.log(data);
        const action1 = ["viewMessageDetails", viewTicketStyle.elements[0]];
        const dataColumnNames1 = ["id", "ticketid", "customertext", "attachedfile", "changedate"];
        const dataColumnIndices1 = [0, 1, 2, 3, 4];
        const ticketDataTableObject = {
            IDENTIFIER: "admin-support-ticket-detail-table",
            jsonRMLData: data,
            header: [getTextById(335), getTextById(336), getTextById(337), getTextById(338), ""],
            columnIndices: [0, 2, 3, 4],
            actions: [action1],
            dataColumnIndices: [dataColumnIndices1],
            dataColumnNames: [dataColumnNames1],
            rowFormat: function (r, arr) {
                return arr;
            },
            cellFormat: function (colIdx, rowIdx, TDOpen, cell, TDClose) {
                if (colIdx == 2) {
                    debug("colIdx", 0, colIdx, cell);
                    return `${TDOpen}${formThumb(601, '', '', '', cell)}${TDClose}`;
                } else {
                    return `${TDOpen}${cell}${TDClose}`;
                }
            }
        };
        const form1 = [ticketText, attachedFile, hiddenField, submitButton, cancelButton, tableDiv];
        listoHTML.buildForm("#main-content", 10, form1, 'support-ticket-form');
        listoHTML.createTable.call(ticketDataTableObject, "tableDiv", false);
    }, function (err) { // If failed
        showHttpErrorMessage("main-content", err);
    });


}


$(document).on('click', '#submit_ticket', function () {
    if ($("#support-ticket-form").valid()) {
        const ticketText = $("#ticketText").val();
        const ticketID = $(".hiddenticketID").val();
        const customerID = getCustomerId();
        if (document.getElementById('file_imagefield').value) {
            upload(customerID, 'support-ticket-form', function (data) {
                debug("upload-data:", release, data);
                const attachedFile = "" + data.rows[0].s[2];
                //alert(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'");
                apiCall_listorante_admin_addticketdata(attachedFile, ticketText, ticketID, function (data) {
                    if (data._rc > 0) {
                        alert(getTextById(253) + data._rc);
                    } else {
                        supportServiceClicked(ticketID);
                    }
                }, function err() {
                    showHttpErrorMessage("main-content", err);
                });
            }, function (error) {
                alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
            });
        } else {
            const attachedFile = images.missing_image_txt;
            apiCall_listorante_admin_addticketdata(attachedFile, ticketText, ticketID, function (data) {
                if (data._rc > 0) {
                    alert(getTextById(253) + data._rc);
                } else {
                    supportServiceClicked(ticketID);
                }
            }, function err() {
                showHttpErrorMessage("main-content", err);
            });
        }
    }
});

$(document).on('click', '.createTicket', function () {

    const ticketdefid = $(this).data('ticketdefinition');
    const customerid = getCustomerId();
    apiCall_listorante_admin_createticket(customerid, ticketdefid, function (data) {
        supportButtonClicked();
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });

});

$(document).on('click', '.viewMessageDetails', function () {
    const ticketid = $(this).data("ticketid");
    //const messageId = $(this).data("id");
    const attachedfile = $(this).data("attachedfile");
    const attachedfilePath = getImagePath() + "/" + attachedfile;
    const customertext = $(this).data("customertext");
    const ticketText = FormTextArea(336, "ticketText", "rows='3' required readonly ", customertext);
    const cancelButton = FormButton(602, "backToList", "", "support-button");
    const attachedFile = formDisplayImage(337, "", "", attachedfilePath);
    const hiddenField = formHiddenInput("hiddenticketID", ticketid, "ticketID_" + ticketid);
    const form1 = [ticketText, attachedFile, hiddenField, cancelButton];
    listoHTML.buildForm("#main-content", 10, form1, 'support-ticket-form');
});

$(document).on('click', '#backToList', function () {
    const ticketid = $('.hiddenticketID').val();
    supportServiceClicked(ticketid);
});


$(document).on('click', '#support_button', function () {
    supportButtonClicked();
});

$(document).on('click', '#back', function () {
    supportButtonClicked();
});

$(document).on('click', '.supportIcon', function () {
    const ticketId = $(this).data('id');
    supportServiceClicked(ticketId);
});



$(document).on('click', '.support_button', function () {
    supportButtonClicked();
});

/*-----------------------------------*/ 
/*---------------END OF LISTO_BL.JS--------------------*/ 
