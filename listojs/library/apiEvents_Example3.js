"use strict";

/*******   Block for dummy functions    *****/

var module = "MOD0";
var release = "REL1";

function debug2(module, str, release, data) {
    if (data) {
        console.log(module + "-" + release + ": " + str + ", data: " + JSON.stringify(data));
    } else {
        console.log(module + "-" + release + ": " + str);
    }
}

function apiCall(path, dataObject, event1CallBack, httpErrorCallBack, httpMethod) {
    debug2(module, httpMethod + "-call ' to " + path, release, null);
    event1CallBack(dataObject);
}

function httpErrorCallBack(txt) {
    // TODO
    console.error(txt);
}

/**********  End of dummy functions   ****************/





const functionForApiCall = function (objectId, dataObject, path, method) {
    if (!dataObject) {
        console.error("Cannot send apiCall1 with null dataobject.");
    } else if (!objectId) {
        console.error("Null object-id for apiCall1.");
    } else {
        // TODO
        let eventCallBack;
        switch (objectId) {
        case "butt1":
                //TODO
            eventCallBack = function (dataObject) {
                console.log("Triggered by object 'butt1' with following data: " + JSON.stringify(dataObject));
            };
            break;
        case "input1":
                //TODO
            eventCallBack = function (dataObject) {
                const rml = {
                    attribute1: dataObject.attribute1,
                    attribute2: $("#input1").val()
                };
                dataObject = rml;
                console.log("Triggered by Object 'input1' with following data: " + JSON.stringify(dataObject));
                $("#textarea1").text(dataObject.attribute1 + "-" + dataObject.attribute2);
            };
            break;
        default:
            //TODO
            eventCallBack = function (dataObject) {
                console.log("DEFAULT trigger with following data: " + JSON.stringify(dataObject));
                $("#butt1").text(objectId);
            };
        }
        console.log("Object " + objectId + " is calling api");
        apiCall(path, dataObject, eventCallBack, httpErrorCallBack, method);
    }
};

$(document)
    .on(
        'click',
        '.clickEventApiCall_1_1',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            const attribute1 = $(this).data("attribute1");
            const attribute2 = $(this).data("attribute2");
            if (!attribute1) {
                console.error("Calling object with id '" + objectId + "' is missing  mandatory data-attribute 'attribute1'");
            }
            if (!attribute2) {
                console.error("Calling object with id '" + objectId + "' is missing  mandatory data-attribute 'attribute2'");
            }
            let dataObject;
            if (attribute1 && attribute2) {
                dataObject = {
                    "attribute1": attribute1,
                    "attribute2": attribute2
                };
            }
            functionForApiCall(objectId, dataObject,"path/to/apicall1","post");
        }
    );

$(document)
    .on(
        'keyup',
        '.keyUpEventApiCall_1_1',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            const attribute1 = $(this).data("attribute1");
            const attribute2 = $(this).data("attribute2");
            if (!attribute1) {
                console.error("Calling object with id '" + objname + "' is missing  mandatory data-attribute 'attribute1'");
            }
            if (!attribute2) {
                console.error("Calling object with id '" + objname + "' is missing  mandatory data-attribute 'attribute2'");
            }
            let dataObject;
            if (attribute1 && attribute2) {
                dataObject = {
                    "attribute1": attribute1,
                    "attribute2": attribute2
                };
            }
            functionForApiCall(objectId, dataObject,"path/to/apicall1","post");
        }
    );