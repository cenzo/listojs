/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'listorante'
	Module: 'Waiter'
	Version: 1.30
***************************************/

/***************************************
	CALL-ID: listorante.Waiter.apiCall1
	Create a new order-list and initiate bill
	***************************************/
function apiCall_listorante_waiter_createbill
	(tablenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"tablenumber" : tablenumber
	};
	return apiCall("listorante.Waiter.apiCall1","/1/waiter/createbill", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall2
	Add a product-item to the order-list
	***************************************/
function apiCall_listorante_waiter_addtoproductlist
	(idbillnumber,
	idorderedproduct,
	tablenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idbillnumber" : idbillnumber,
	"idorderedproduct" : idorderedproduct,
	"tablenumber" : tablenumber
	};
	return apiCall("listorante.Waiter.apiCall2","/1/waiter/addtoproductlist", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall3
	Delete a product from the order-list
	***************************************/
function apiCall_listorante_waiter_delitem
	(idorder,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("listorante.Waiter.apiCall3","/1/waiter/delitem", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall4
	Set notice on bill 
	***************************************/
function apiCall_listorante_waiter_billremark
	(idbill,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idbill" : idbill
	};
	return apiCall("listorante.Waiter.apiCall4","/1/waiter/billremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall5
	Set remark on an ordered item 
	***************************************/
function apiCall_listorante_waiter_itemremark
	(idorder,
	remark,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"remark" : remark
	};
	return apiCall("listorante.Waiter.apiCall5","/1/waiter/itemremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall6
	Show shopping cart of a table 
	***************************************/
function apiCall_listorante_waiter_showcart
	(billnumber,
	tablenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"billnumber" : billnumber,
	"tablenumber" : tablenumber
	};
	return apiCall("listorante.Waiter.apiCall6","/1/waiter/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall8
	Waiter Dashboard 
	***************************************/
function apiCall_listorante_waiter_waiterdash
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Waiter.apiCall8","/1/waiter/waiterdash", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall10
	Show bills of a table 
	***************************************/
function apiCall_listorante_waiter_tablebills
	(tablenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"tablenumber" : tablenumber
	};
	return apiCall("listorante.Waiter.apiCall10","/1/waiter/tablebills", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall11
	Show status of ordered items of a bill 
	***************************************/
function apiCall_listorante_waiter_deliverystatus
	(idbillnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idbillnumber" : idbillnumber
	};
	return apiCall("listorante.Waiter.apiCall11","/1/waiter/deliverystatus", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall12
	Send listed items as order to kitchen
	***************************************/
function apiCall_listorante_waiter_sendorder
	(idbillnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idbillnumber" : idbillnumber
	};
	return apiCall("listorante.Waiter.apiCall12","/1/waiter/sendorder", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall13
	Get notifications of customers for the currently logged in waiter
	***************************************/
function apiCall_listorante_waiter_getnotifications
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Waiter.apiCall13","/1/waiter/getnotifications", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall14
	Delete a customer-notification for the currently logged in waiter
	***************************************/
function apiCall_listorante_waiter_deletenotification
	(idnotification,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idnotification" : idnotification
	};
	return apiCall("listorante.Waiter.apiCall14","/1/waiter/deletenotification", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall15
	Set number of guests for a bill
	***************************************/
function apiCall_listorante_waiter_setguestcount
	(guestcount,
	idbill,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"guestcount" : guestcount,
	"idbill" : idbill
	};
	return apiCall("listorante.Waiter.apiCall15","/1/waiter/setguestcount", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall16
	Mark a bill with current status
	***************************************/
function apiCall_listorante_waiter_setbillstatus
	(billstatus,
	idbill,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"billstatus" : billstatus,
	"idbill" : idbill
	};
	return apiCall("listorante.Waiter.apiCall16","/1/waiter/setbillstatus", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall17
	Archiviation of a bill
	***************************************/
function apiCall_listorante_waiter_archivebill
	(idbill,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idbill" : idbill
	};
	return apiCall("listorante.Waiter.apiCall17","/1/waiter/archivebill", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall18
	Select waiter profile data
	***************************************/
function apiCall_listorante_waiter_getwaiterprofile
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Waiter.apiCall18","/1/waiter/getwaiterprofile", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall19
	Update profile data
	***************************************/
function apiCall_listorante_waiter_editwaiterprofile
	(email,
	first_name,
	last_name,
	waitername,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"last_name" : last_name,
	"waitername" : waitername
	};
	return apiCall("listorante.Waiter.apiCall19","/1/waiter/editwaiterprofile", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall20
	Change waiter password
	***************************************/
function apiCall_listorante_waiter_changewaiterpassword
	(password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"password" : password
	};
	return apiCall("listorante.Waiter.apiCall20","/1/waiter/changewaiterpassword", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall21
	Change waiter profile Image
	***************************************/
function apiCall_listorante_waiter_changewaiterimage
	(image,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"image" : image
	};
	return apiCall("listorante.Waiter.apiCall21","/1/waiter/changewaiterimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall22
	Show orders of all registered users for a bill
	***************************************/
function apiCall_listorante_waiter_userorders
	(idbill,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idbill" : idbill
	};
	return apiCall("listorante.Waiter.apiCall22","/1/waiter/userorders", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Waiter.apiCall23
	Re-Assign orders of a registered user to a different bill
	***************************************/
function apiCall_listorante_waiter_reassignuserorders
	(idfrombill,
	idtobill,
	iduser,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idfrombill" : idfrombill,
	"idtobill" : idtobill,
	"iduser" : iduser
	};
	return apiCall("listorante.Waiter.apiCall23","/1/waiter/reassignuserorders", dataObject, callBack, httpErrorCallBack,"put");
}


