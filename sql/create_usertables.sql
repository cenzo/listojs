use c1200;
-- auth_user definition

GRANT Delete ON c1200.* TO 'listorante'@'%';
GRANT Insert ON c1200.* TO 'listorante'@'%';
GRANT References ON c1200.* TO 'listorante'@'%';
GRANT Select ON c1200.* TO 'listorante'@'%';
GRANT Update ON c1200.* TO 'listorante'@'%';


CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstpass` varchar(64) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `encpass` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email_UN` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

-- auth_permission definition

CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `permname` varchar(255) NOT NULL,
  `isactive` smallint NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_name` (`permname`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

-- auth_user_permissions definition

CREATE TABLE `auth_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` int NOT NULL,
  `permissionid` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_uniq` (`userid`,`permissionid`),
  KEY `auth_user_user_permission_key` (`permissionid`),
  CONSTRAINT `auth_user_permission_fk` FOREIGN KEY (`permissionid`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_permissions_user_fk` FOREIGN KEY (`userid`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_session` (
  `idsession` bigint unsigned NOT NULL AUTO_INCREMENT,
  `session` varchar(75) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `iduser` int NOT NULL,
  `ip` varchar(45) NOT NULL,
  `application` int NOT NULL,
  `instance` int NOT NULL,
  `sessionstart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  `sessionend` datetime DEFAULT NULL,
  PRIMARY KEY (`idsession`),
  UNIQUE KEY `auth_session_UN` (`session`),
  KEY `auth_session_FK` (`iduser`),
  CONSTRAINT `auth_session_FK` FOREIGN KEY (`iduser`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


INSERT INTO auth_permission (id, permname, isactive) VALUES(5, 'USER', 1);

 
CREATE TABLE `writecount` (
  `uid` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `inserttime` datetime NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
