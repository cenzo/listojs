/*   Created with UIO: stap-software.com/uio ©  - All rights reserved     */
/*     --------- ------------------ ProjectName: P_blog_instance65_application99_module999--------- ------------------     */
/*     Creation time:2023-12-21T09:26:02.693Z 	Project-version: 1680816055     */
//Event-listener for requirement "Article-area" of "load"-event. (Requirement 104)
dgi("widget104").addEventListener("load",m30UIOwidget104load);

//Event-listener for requirement "username" of "keyup"-event. (Requirement 106)
dgi("widget106").addEventListener("keyup",m30UIOwidget106keyup);

//Event-listener for requirement "password" of "keyup"-event. (Requirement 107)
dgi("widget107").addEventListener("keyup",m30UIOwidget107keyup);

//Event-listener for requirement "login button" of "click"-event. (Requirement 108)
dgi("widget108").addEventListener("click",m30UIOwidget108click);

//Event-listener for requirement "Listed old article right" of "load"-event. (Requirement 111)
dgi("widget111").addEventListener("load",m30UIOwidget111load);

//Event-listener for requirement "Main article paragraph" of "load"-event. (Requirement 114)
dgi("widget114").addEventListener("load",m30UIOwidget114load);

//Event-listener for requirement "Main article Header" of "click"-event. (Requirement 115)
dgi("widget115").addEventListener("click",m30UIOwidget115click);

//Event-listener for requirement "Link 1 topics" of "change"-event. (Requirement 121)
dgi("widget121").addEventListener("change",m30UIOwidget121change);

//Event-listener for requirement "Search article" of "keyup"-event. (Requirement 141)
dgi("widget141").addEventListener("keyup",m30UIOwidget141keyup);

//Event-listener for requirement "Search Button" of "click"-event. (Requirement 175)
dgi("widget175").addEventListener("click",m30UIOwidget175click);

//Event-listener for requirement "Back-Button" of "click"-event. (Requirement 144)
dgi("widget144").addEventListener("click",m30UIOwidget144click);

