/*   Created with UIO: stap-software.com/uio ©  - All rights reserved     */
/*     --------- ------------------ ProjectName: P_blog_instance65_application99_module999--------- ------------------     */
/*     Creation time:2023-12-21T09:26:02.693Z 	Project-version: 1680816055     */


const m30UIOwidget104load=function(){

	const dataObject={};
	//Send parameter to receiver 1: UIOWareWindow104.articleID==>Backend101.articleID
	const articleIDValue=dgi("widget104").getAttribute("data-articleID");
	if (articleIDValue)
		setBackendValue("articleID",articleIDValue);
	dataObject.articleID=articleIDValue;
	if (customFunctionm30UIOwidget104load) customFunctionm30UIOwidget104load();
	uioInfo("Function 'Article-area'"," for widget m30UIOwidget104",dataObject);
	apiCall("65/99/999/0","UIO0articlearea",dataObject,"GET",callBackm30UIOwidget104load);

}

const m30UIOwidget106keyup=function(){

	const dataObject={};
	//Send parameter to receiver 1: UIOWareMockTextField106.username==>mBtn108.username
	const usernameValue=dgi("UIOWareMockTextField106").value;
	if (usernameValue)
		dgi("UIOWareMockButton108").setAttribute("data-username",usernameValue);
	if (customFunctionm30UIOwidget106keyup) customFunctionm30UIOwidget106keyup();
	uioInfo("Function 'username'"," for widget m30UIOwidget106",dataObject);

}

const m30UIOwidget107keyup=function(){

	const dataObject={};
	//Send parameter to receiver 1: UIOWareMockTextField107.password==>mBtn108.password
	const passwordValue=dgi("UIOWareMockTextField107").value;
	if (passwordValue)
		dgi("UIOWareMockButton108").setAttribute("data-password",passwordValue);
	if (customFunctionm30UIOwidget107keyup) customFunctionm30UIOwidget107keyup();
	uioInfo("Function 'password'"," for widget m30UIOwidget107",dataObject);

}

const m30UIOwidget108click=function(){

	const dataObject={};
	if (customFunctionm30UIOwidget108click) customFunctionm30UIOwidget108click();
	uioInfo("Function 'login button'"," for widget m30UIOwidget108",dataObject);

}

const m30UIOwidget111load=function(){

	const dataObject={};
	//Send parameter to receiver 1: UIOWareWindow111.topicslistofids==>Backend101.topicslistofids
	const topicslistofidsValue=dgi("widget111").getAttribute("data-topicslistofids");
	if (topicslistofidsValue)
		setBackendValue("topicslistofids",topicslistofidsValue);
	dataObject.topicslistofids=topicslistofidsValue;
	if (customFunctionm30UIOwidget111load) customFunctionm30UIOwidget111load();
	uioInfo("Function 'Listed old article right'"," for widget m30UIOwidget111",dataObject);
	apiCall("65/99/999/4","UIO4listedoldarticleright",dataObject,"POST",callBackm30UIOwidget111load);

}

const m30UIOwidget114load=function(){

	const dataObject={};
	if (customFunctionm30UIOwidget114load) customFunctionm30UIOwidget114load();
	uioInfo("Function 'Main article paragraph'"," for widget m30UIOwidget114",dataObject);

}

const m30UIOwidget115click=function(){

	const dataObject={};
	//Send parameter to receiver 1: UIOWareTextChapter115.articleid==>Window143.articleid
	const articleidValue=dgi("UIOWareTextChapter115").getElementsByTagName("p");
	if (articleidValue)
		dgi("widget143").setAttribute("data-articleid",articleidValue);
	if (customFunctionm30UIOwidget115click) customFunctionm30UIOwidget115click();
	uioInfo("Function 'Main article Header'"," for widget m30UIOwidget115",dataObject);

}

const m30UIOwidget121change=function(){

	const dataObject={};
	if (customFunctionm30UIOwidget121change) customFunctionm30UIOwidget121change();
	uioInfo("Function 'Link 1 topics'"," for widget m30UIOwidget121",dataObject);

}

const m30UIOwidget141keyup=function(){

	const dataObject={};
	if (customFunctionm30UIOwidget141keyup) customFunctionm30UIOwidget141keyup();
	uioInfo("Function 'Search article'"," for widget m30UIOwidget141",dataObject);

}

const m30UIOwidget175click=function(){

	const dataObject={};
	if (customFunctionm30UIOwidget175click) customFunctionm30UIOwidget175click();
	uioInfo("Function 'Search Button'"," for widget m30UIOwidget175",dataObject);

}

const m30UIOwidget144click=function(){

	const dataObject={};
	if (customFunctionm30UIOwidget144click) customFunctionm30UIOwidget144click();
	uioInfo("Function 'Back-Button'"," for widget m30UIOwidget144",dataObject);

}