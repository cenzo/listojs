/*   Created with UIO: stap-software.com/uio ©  - All rights reserved     */
/*     --------- ------------------ ProjectName: P_blog_instance65_application99_module999--------- ------------------     */
/*     Creation time:2023-12-21T09:26:02.693Z 	Project-version: 1680816055     */


const DEBUG=true;
const uioInfo=function(msg1,msg2,msg3){
   if (DEBUG) console.info(msg1,msg2,msg3);
}

const apiCall=function(operation,callerObject,dataObject,method,callBackfunction) {
	
	uioInfo('APICALL ',operation,callerObject,dataObject,method,callBackfunction);
	const dummyData={};
	dummyData.dummyInt1=1;
	dummyData.dummyInt2=2;
	dummyData.dummyString1="string1";
	dummyData.dummyString2="string2";
	dummyData.parameters=dataObject;
	callBackfunction(dummyData);
	
}

const ApplicationValues={};
const BackendValues={};
const FrontendValues={};

const dgi=function(id){return document.getElementById(id);}
const showSection=function(id){
   // TODO: customize, if needed
   for (let i=1;i<5;i++){
      if (i===id)dgi("section"+i).style.visibility="visible";
	  else dgi("section"+i).style.visibility="hidden";
   }
}

const showOverflowPage=function(id){
	  // TODO: customize, if needed
	  dgi("page"+id).style.visibility="visible";
}
const hideOverflowPage=function(id){
      // TODO: customize, if needed
	  dgi("page"+id).style.visibility="hidden";
}

const setApplicationValue=function(parameterName,parameterValue){
	ApplicationValues[parameterName]=parameterValue;
	//TODO write customization
	}


const setBackendValue=function(parameterName,parameterValue){
	BackendValues[parameterName]=parameterValue;
	//TODO write customization
	}


const setFrontendValue=function(parameterName,parameterValue){
	FrontendValues[parameterName]=parameterValue;
	//TODO write customization
	}


const getApplicationValue=function(parameterName){
	//TODO write customization
	return ApplicationValues[parameterName];
	}


const getBackendValue=function(parameterName){
	//TODO write customization
	return BackendValues[parameterName];
	}


const getFrontendValue=function(parameterName){
	//TODO write customization
	return FrontendValues[parameterName];
	}


// ID: 103
//		Widget 'Top Area'
//		html-id: m30UIOwidget103
/*
		 Top Area with menu and other stuff
*/


// ID: 104
//		Widget 'Article-area'
//		html-id: m30UIOwidget104
/*
		 Here we see the articles
*/


//REQUIREMENT: 104.load
//Function for widget "Article-area" triggered by "load"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget104load=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget104' and 'load'-event

	}
	const callBackm30UIOwidget104load=function(returnedData){
	/*Specifications for  api- (or general backend-) processing*/

	//TODO: Implement what the callBack-function of the apiCall must do with the retrieved data

	}

// ID: 105
//		Widget 'Logo'
//		html-id: m30UIOwidget105
/*
		 Logo on top left position
*/


// ID: 106
//		Widget 'username'
//		html-id: m30UIOwidget106
/*
		 Username (must disappear after login and be replaced with a text or i
mage of the user)

*/


//REQUIREMENT: 106.keyup
//Function for widget "username" triggered by "keyup"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget106keyup=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget106' and 'keyup'-event

	}

// ID: 107
//		Widget 'password'
//		html-id: m30UIOwidget107
/*
		 Password field (must disappear after login and replaced by current ti
me or similar)

*/


//REQUIREMENT: 107.keyup
//Function for widget "password" triggered by "keyup"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget107keyup=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget107' and 'keyup'-event

	}

// ID: 108
//		Widget 'login button'
//		html-id: m30UIOwidget108
/*
		 Login button (must disappear after click and replaced by signout-button)
*/


//REQUIREMENT: 108.click
//Function for widget "login button" triggered by "click"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget108click=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget108' and 'click'-event

	}

// ID: 109
//		Widget 'Menu2'
//		html-id: m30UIOwidget109
/*
		 Menu2 with newest article or similar
*/


// ID: 111
//		Widget 'Listed old article right'
//		html-id: m30UIOwidget111
/*
		 Here comes a list with all the passed articles, sorted in chronological order
*/


//REQUIREMENT: 111.load
//Function for widget "Listed old article right" triggered by "load"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget111load=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget111' and 'load'-event

	}
	const callBackm30UIOwidget111load=function(returnedData){
	/*Specifications for  api- (or general backend-) processing*/

	//TODO: Implement what the callBack-function of the apiCall must do with the retrieved data

	}

// ID: 112
//		Widget 'Main article'
//		html-id: m30UIOwidget112
/*
		 Here comes the main article with some picture on top left
*/


// ID: 113
//		Widget 'main article picture'
//		html-id: m30UIOwidget113
/*
		 The main article should always have a picture in top left
*/


// ID: 114
//		Widget 'Main article paragraph'
//		html-id: m30UIOwidget114
/*
		 The main article should be have a big visible text
*/


//REQUIREMENT: 114.load
//Function for widget "Main article paragraph" triggered by "load"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget114load=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget114' and 'load'-event

	}

// ID: 115
//		Widget 'Main article Header'
//		html-id: m30UIOwidget115
/*
		 A nice header for the main article
*/


//REQUIREMENT: 115.click
//Function for widget "Main article Header" triggered by "click"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget115click=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget115' and 'click'-event

	}

// ID: 116
//		Widget 'Other articles'
//		html-id: m30UIOwidget116
/*
		 Here come other recent articles
*/


// ID: 117
//		Widget 'Old article 1'
//		html-id: m30UIOwidget117
/*
		 Older first article
*/


// ID: 118
//		Widget 'Old article 2'
//		html-id: m30UIOwidget118
/*
		 Older article 2
*/


// ID: 119
//		Widget 'Older article 3'
//		html-id: m30UIOwidget119
/*
		 Older article 3
*/


// ID: 120
//		Widget 'Footer'
//		html-id: m30UIOwidget120
/*
		 Footer with data privacy, cookies a.s.o.
*/


// ID: 121
//		Widget 'Link 1 topics'
//		html-id: m30UIOwidget121
/*
		 Links of older articles sorted by topic 1
*/


//REQUIREMENT: 121.change
//Function for widget "Link 1 topics" triggered by "change"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget121change=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget121' and 'change'-event

	}

// ID: 122
//		Widget 'Link 2 topics'
//		html-id: m30UIOwidget122
/*
		 Links of older articles sorted by topic 1
*/


// ID: 123
//		Widget 'Link 3 topics'
//		html-id: m30UIOwidget123
/*
		 Links of older articles sorted by topic 3
*/


// ID: 124
//		Widget 'mLink 124'
//		html-id: m30UIOwidget124
/*
		Description of widget124
*/


// ID: 125
//		Widget 'mLink 125'
//		html-id: m30UIOwidget125
/*
		Description of widget125
*/


// ID: 126
//		Widget 'mLink 126'
//		html-id: m30UIOwidget126
/*
		Description of widget126
*/


// ID: 127
//		Widget 'mLink 127'
//		html-id: m30UIOwidget127
/*
		Description of widget127
*/


// ID: 128
//		Widget 'mLink 128'
//		html-id: m30UIOwidget128
/*
		Description of widget128
*/


// ID: 129
//		Widget 'mLink 129'
//		html-id: m30UIOwidget129
/*
		Description of widget129
*/


// ID: 130
//		Widget 'mLink 130'
//		html-id: m30UIOwidget130
/*
		Description of widget130
*/


// ID: 132
//		Widget 'mLink 132'
//		html-id: m30UIOwidget132
/*
		Description of widget132
*/


// ID: 133
//		Widget 'mLink 133'
//		html-id: m30UIOwidget133
/*
		Description of widget133
*/


// ID: 134
//		Widget 'mLink 134'
//		html-id: m30UIOwidget134
/*
		Description of widget134
*/


// ID: 135
//		Widget 'mLink 135'
//		html-id: m30UIOwidget135
/*
		Description of widget135
*/


// ID: 136
//		Widget 'mLink 136'
//		html-id: m30UIOwidget136
/*
		Description of widget136
*/


// ID: 137
//		Widget 'mLink 137'
//		html-id: m30UIOwidget137
/*
		Description of widget137
*/


// ID: 138
//		Widget 'mLink 138'
//		html-id: m30UIOwidget138
/*
		Description of widget138
*/


// ID: 139
//		Widget 'Menu 1'
//		html-id: m30UIOwidget139
/*
		Description of widget139
*/


// ID: 141
//		Widget 'Search article'
//		html-id: m30UIOwidget141
/*
		 Search for an article
*/


//REQUIREMENT: 141.keyup
//Function for widget "Search article" triggered by "keyup"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget141keyup=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget141' and 'keyup'-event

	}

// ID: 166
//		Widget 'Edit article'
//		html-id: m30UIOwidget166
/*
		 Every article must have an edit article link if user is logged in as editor
*/


// ID: 173
//		Widget 'Edit main article'
//		html-id: m30UIOwidget173
/*
		 Edit link for the main article opens the edit page
*/


// ID: 175
//		Widget 'Search Button'
//		html-id: m30UIOwidget175
/*
		 After click on search the reuslts must appear in new window
*/


//REQUIREMENT: 175.click
//Function for widget "Search Button" triggered by "click"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget175click=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget175' and 'click'-event

	}

// ID: 143
//		Widget 'Window 143'
//		html-id: m30UIOwidget143
/*
		Description of widget143
*/


// ID: 144
//		Widget 'Back-Button'
//		html-id: m30UIOwidget144
/*
		 Back to Homepage button
*/


//REQUIREMENT: 144.click
//Function for widget "Back-Button" triggered by "click"-event

	//Inserted in project-version '1680816055'

	const customFunctionm30UIOwidget144click=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm30UIOwidget144' and 'click'-event

	}

// ID: 145
//		Widget 'Read article window'
//		html-id: m30UIOwidget145
/*
		 Here we can read the selected article
*/


// ID: 146
//		Widget 'Selected article-header'
//		html-id: m30UIOwidget146
/*
		 Header of selected article
*/


// ID: 147
//		Widget 'Image of selected article'
//		html-id: m30UIOwidget147
/*
		Description of widget147
*/


// ID: 148
//		Widget 'mTxtPrgrph 148'
//		html-id: m30UIOwidget148
/*
		Description of widget148
*/


// ID: 149
//		Widget 'Subheader selected article'
//		html-id: m30UIOwidget149
/*
		Description of widget149
*/


// ID: 150
//		Widget 'Menu'
//		html-id: m30UIOwidget150
/*
		 must be the same on all pages
*/


// ID: 151
//		Widget 'Footer page2'
//		html-id: m30UIOwidget151
/*
		 Must be the same on all pages
*/


// ID: 153
//		Widget 'Window with search results'
//		html-id: m30UIOwidget153
/*
		Description of widget153
*/


// ID: 154
//		Widget 'Menus'
//		html-id: m30UIOwidget154
/*
		Description of widget154
*/


// ID: 155
//		Widget 'Footer always the same'
//		html-id: m30UIOwidget155
/*
		Description of widget155
*/


// ID: 159
//		Widget 'List of result links'
//		html-id: m30UIOwidget159
/*
		Description of widget159
*/


// ID: 161
//		Widget 'mLink 161'
//		html-id: m30UIOwidget161
/*
		Description of widget161
*/


// ID: 162
//		Widget 'mLink 162'
//		html-id: m30UIOwidget162
/*
		Description of widget162
*/


// ID: 163
//		Widget 'mLink 163'
//		html-id: m30UIOwidget163
/*
		Description of widget163
*/


// ID: 164
//		Widget 'mLink 164'
//		html-id: m30UIOwidget164
/*
		Description of widget164
*/


// ID: 168
//		Widget 'Editor Window'
//		html-id: m30UIOwidget168
/*
		 Here we write a new or update existing articles
*/


// ID: 169
//		Widget 'Same menu as always'
//		html-id: m30UIOwidget169
/*
		Description of widget169
*/


// ID: 170
//		Widget 'same footer for all pages'
//		html-id: m30UIOwidget170
/*
		Description of widget170
*/


// ID: 171
//		Widget 'WYSIWYG- Editor'
//		html-id: m30UIOwidget171
/*
		 WYSIWYG tool, e .g. Summer Note
*/
