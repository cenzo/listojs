/*   Created with UIO: stap-software.com/uio ©  - All rights reserved     */
/*     --------- ------------------ ProjectName: P_coins_instance65_application99_module999--------- ------------------     */
/*     Creation time:2023-12-27T16:55:18.831Z 	Project-version: 1649167882     */
//Event-listener for requirement "selection 107" of "change"-event. (Requirement 107)
dgi("selection107").addEventListener("change",m10UIOwidget107change);

//Event-listener for requirement "button 108" of "click"-event. (Requirement 108)
dgi("button108").addEventListener("click",m10UIOwidget108click);

