/*   Created with UIO: stap-software.com/uio ©  - All rights reserved     */
/*     --------- ------------------ ProjectName: P_coins_instance65_application99_module999--------- ------------------     */
/*     Creation time:2023-12-27T16:55:18.831Z 	Project-version: 1649167882     */


const m10UIOwidget107change=function(){

	const dataObject={};
	//Send parameter from sender 'selection 107' to receiver 'Frontend': selection107.SelectWert==>Frontend102.SelectWert
	const SelectWert=getValue("selection107","SelectWert");
		setFrontendValue("SelectWert",SelectWert);
		dataObject.SelectWert=SelectWert;
	//Send parameter from sender 'selection 107' to receiver 'Some interesting chapter-text': selection107.SelectWert==>textParagraph111.SelectWert
	dgi("textParagraph111").innerText=SelectWert;
	//Send parameter from sender 'selection 107' to receiver 'httpUrl 120': selection107.SelectWert==>httpUrl120.SelectWert
	dgi("httpUrl120").setAttribute("href",SelectWert);
	dgi("httpUrl120").innerText=SelectWert;
	if (customFunctionm10UIOwidget107change) customFunctionm10UIOwidget107change();
	uioInfo("Function 'selection 107'"," for widget m10UIOwidget107",dataObject);

}

const m10UIOwidget108click=function(){

	const dataObject={};
	if (customFunctionm10UIOwidget108click) customFunctionm10UIOwidget108click();
	uioInfo("Function 'button 108'"," for widget m10UIOwidget108",dataObject);

}