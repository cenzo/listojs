/*   Created with UIO: stap-software.com/uio ©  - All rights reserved     */
/*     --------- ------------------ ProjectName: P_coins_instance65_application99_module999--------- ------------------     */
/*     Creation time:2023-12-27T16:55:18.831Z 	Project-version: 1649167882     */


const DEBUG=true;
const uioInfo=function(...args){
   if (DEBUG) {
    for (let i = 0; i < args.length; i++) console.info(args[i]);
	}
}
const getValue = function (elementId,dataName) {
    try {
        const el =dgi(elementId);
        const elementClass = el.getAttribute("id");
        if (elementClass.includes("selection"))
            return el.options[el.selectedIndex].value;
        else if (elementClass.includes("input")) return el.value;
        else if (dataName){
            const dataValue = el.getAttribute(`data-${dataName}`);
            return dataValue;
        }else{
            const dataValue = el.getAttribute(`data-value`);
            return dataValue;
        }
    } catch (err) {
        console.error(err);
    }
}
const apiCall=function(operation,callerObject,dataObject,method,callBackfunction) {
	
	uioInfo('APICALL ',operation,callerObject,dataObject,method,callBackfunction);
	const dummyData={};
	dummyData.dummyInt1=1;
	dummyData.dummyInt2=2;
	dummyData.dummyString1="string1";
	dummyData.dummyString2="string2";
	dummyData.parameters=dataObject;
	callBackfunction(dummyData);
	
}

const ApplicationValues={};
const BackendValues={};
const FrontendValues={};

const dgi=function(id){return document.getElementById(id);}
const showSection=function(id){
   // TODO: customize, if needed
   for (let i=1;i<3;i++){
      if (i===id)dgi("section"+i).style.visibility="visible";
	  else dgi("section"+i).style.visibility="hidden";
   }
}

const showOverflowPage=function(id){
	  // TODO: customize, if needed
	  dgi("page"+id).style.visibility="visible";
}
const hideOverflowPage=function(id){
      // TODO: customize, if needed
	  dgi("page"+id).style.visibility="hidden";
}

const setApplicationValue=function(parameterName,parameterValue){
	ApplicationValues[parameterName]=parameterValue;
	//TODO write customization
	}


const setBackendValue=function(parameterName,parameterValue){
	BackendValues[parameterName]=parameterValue;
	//TODO write customization
	}


const setFrontendValue=function(parameterName,parameterValue){
	FrontendValues[parameterName]=parameterValue;
	//TODO write customization
	}


const getApplicationValue=function(parameterName){
	//TODO write customization
	return ApplicationValues[parameterName];
	}


const getBackendValue=function(parameterName){
	//TODO write customization
	return BackendValues[parameterName];
	}


const getFrontendValue=function(parameterName){
	//TODO write customization
	return FrontendValues[parameterName];
	}


// ID: 103
//		Widget 'window 103'
//		html-id: m10UIOwidget103
/*
		Description of widget103
*/


// ID: 105
//		Widget 'input 105'
//		html-id: m10UIOwidget105
/*
		Description of widget105
*/


// ID: 106
//		Widget 'output 106'
//		html-id: m10UIOwidget106
/*
		Description of widget106
*/


// ID: 107
//		Widget 'selection 107'
//		html-id: m10UIOwidget107
/*
		Description of widget107
*/


//REQUIREMENT: 107.change
//Function for widget "selection 107" triggered by "change"-event

	//Project-serializer-ID: '1649167882'

	const customFunctionm10UIOwidget107change=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm10UIOwidget107' and 'change'-event

	}

// ID: 108
//		Widget 'button 108'
//		html-id: m10UIOwidget108
/*
		Description of widget108
*/


//REQUIREMENT: 108.click
//Function for widget "button 108" triggered by "click"-event

	//Project-serializer-ID: '1649167882'

	const customFunctionm10UIOwidget108click=function(){
	/*Specifications for frontend processing*/

	//TODO: Implement functionalities what this custom-function must do for the combination of object 'm10UIOwidget108' and 'click'-event

	}

// ID: 109
//		Widget 'mTextField 109'
//		html-id: m10UIOwidget109
/*
		Description of widget109
*/


// ID: 110
//		Widget 'table 110'
//		html-id: m10UIOwidget110
/*
		Description of widget110
*/


// ID: 111
//		Widget 'Some interesting chapter-text'
//		html-id: m10UIOwidget111
/*
		Description of widget111
*/


// ID: 115
//		Widget 'window 115'
//		html-id: m10UIOwidget115
/*
		Description of widget115
*/


// ID: 116
//		Widget 'textList 116'
//		html-id: m10UIOwidget116
/*
		Description of widget116
*/


// ID: 117
//		Widget 'image 117'
//		html-id: m10UIOwidget117
/*
		Description of widget117
*/


// ID: 118
//		Widget 'label 118'
//		html-id: m10UIOwidget118
/*
		Description of widget118
*/


// ID: 120
//		Widget 'httpUrl 120'
//		html-id: m10UIOwidget120
/*
		Description of widget120
*/
