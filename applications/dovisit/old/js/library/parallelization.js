// global variables:
let CALL_COUNTER = 0;
let APPLICATION_SERVER_ARRAY = [selectedServer + 801];

let TIMEOUT = 15000;
let PARALLEL_CALLS = 4;
let primitiveErrorHandling = function (status) {
    console.error("An error occurred, Status=", status);
}

function newCallID() {
    return CALL_COUNTER++;
}

class ParallelCall {
    resultStartRow;
    resultEndRow;
    serverArray;
    httpErrorCallBack;
    dataEncoding;
    dataBufferSize;
    callBackInProgress;
    responseStateArray;
    // apiID, apiUrl,
    // apiParams, callBack, httpErrorCallBack, method, encoding
    constructor(apiParams, endPointPath, callBackFunction, httpMethod) {
        // TODO ensure uniqueness of id
        this.id = newCallID();
        this.completed = false;
        this.callParameters = apiParams;
        this.endPoint = endPointPath;
        this.callBack = callBackFunction;
        this.method = httpMethod;
        this.callBackInProgress = false;
        this.dataMap = new Map();
        this.dataResultSizeIdentified = false;
        this.parallelCalls = PARALLEL_CALLS;
        this.isReadOnly = 1;
        this.winningRequest = -1;

        this.resultStartRow = null;
        this.resultEndRow = null;
        this.serverArray = null;
        this.httpErrorCallBack = null;
        this.dataEncoding = null;
        this.dataBufferSize = null;
        this.callBackInProgress = false;
        this.responseStateArray = [];
    }

    setParallelCalls(n) {
        this.parallelCalls = n;
    }

    setStartDataPosition(pos) {
        this.resultStartRow = pos;
    }

    setEndDataPosition(pos) {
        this.resultEndRow = pos;
    }

    setServers(arr) {
        this.serverArray = arr;
        this.responseStateArray = [];
    }

    setHttpErrorHandling(f) {
        this.httpErrorCallBack = f;
    }

    setEncoding(enc) {
        this.dataEncoding = enc;
    }

    setBufferSize(n) {
        this.dataBufferSize = n;
    }

    addDataFragment(dataPartition, xhttpResponse) {
        this.dataMap.set(dataPartition, xhttpResponse);
    }

    endOfDataReached(xhttpResponse) {
        // TODO: adapt to JSON-structure of response
        const endReached = xhttpResponse['eod'];
        if (endReached === "true") return true;
        else return false;
    }

    dataMapComplete() {
        console.warn("//TODO: implement method ParallelCall.dataMapComplete")
        if (this.dataMap.size === 0) return false;
        else if (this.dataResultSizeIdentified === true && this.resultEndRow <= this.dataBufferSize) return true;
        else if (this.responseStateArray.length !== this.serverArray.length) return false;
        for (let i = 0; i < this.serverArray.length; i++) {
            // TODO
            // if (this.dataMap.size === 0) etc...

        }
        return true;
    }

    dataRetrieved(dataFragment) {
        console.warn("//TODO: implement method ParallelCall.dataRetrieved(iteration) for dataFragment", dataFragment)
        if (this.dataResultSizeIdentified === false || this.dataMap.size === 0) return false;
        //TODO
        else return true; // temporary dummy code
        // else if (this.dataMap.has(dataFragment) && this.dataMap.get(dataFragment).length<= this.dataBufferSize) ) return true; (??)

    }

    getStartPosition(dataPartition) {
        console.info("//TODO: implement method ParallelCall.getStartPosition(dataPartition) for dataPartition", dataPartition, " and dataMap ", this.dataMap);

        return dataPartition * this.dataBufferSize;
        //function getFirstEmpty(key,value) {
        //    let lastSize=value.length;
        //....
        // }

    }

    getEndPosition(dataPartition) {
        console.warn("//TODO: implement method ParallelCall.getEndPosition(dataPartition) for dataPartition", dataPartition, " and dataMap ", this.dataMap);
        return (dataPartition + 1) * this.dataBufferSize;
    }

    run() {
        if (this.resultStartRow === null) this.setStartDataPosition(0);
        if (this.resultEndRow === null) this.setEndDataPosition(1000);
        if (this.serverArray === null) this.setServers(APPLICATION_SERVER_ARRAY);
        if (this.httpErrorCallBack === null) this.setHttpErrorHandling(primitiveErrorHandling);
        if (this.dataEncoding === null) this.setEncoding("html");
        if (this.dataBufferSize === null) this.setBufferSize(100);

        const xhrArray = [];
        let errorsOccurred = false;
        let endpointUrl;
        let calledServerIndex;
        const currentCall = this;
        for (let i = 0; i < Math.max(this.serverArray.length, this.dataMap.size, this.parallelCalls); i++) {

            xhrArray[i] = new XMLHttpRequest();
            xhrArray[i].timeout = TIMEOUT;
            calledServerIndex = i % this.serverArray.length;
            endpointUrl = this.serverArray[calledServerIndex] + this.endPoint;
            const startPos = 0;
            const endPos = "maximum";
            //console.info(i, ": fetch data-partitions from row ", startPos, " to ", endPos, " with endpoint-url '", endpointUrl, "'");

            xhrArray[i].ontimeout = (e) => {
                console.error("Request " + i + " timed out:" + e);
            };

            xhrArray[i].onreadystatechange = function () {
                if (currentCall.completed === true && i !== currentCall.winningRequest) {
                    xhrArray[i].abort();
                    console.info("Request ", i, " aborted at state " + xhrArray[i].readyState +
                        " because parallel request " + currentCall.winningRequest + " was faster.");
                } else if (xhrArray[i].readyState === 2) {
                    if (xhrArray[i].status >= 400) {
                        xhrArray[i].abort();
                        console.info("Request ", i, " aborted after ready-State 2 due to errors  ");
                        errorsOccurred = true;
                    } else if (xhrArray[i].status < 400) {
                        if (currentCall.completed === false) {
                            currentCall.completed = true;
                            currentCall.winningRequest = i;
                        } else {
                            xhrArray[i].abort();
                            console.info("Request ", i, " aborted after ready-State 2 giving priority to request "
                                + currentCall.winningRequest);
                        }
                    }
                    currentCall.responseStateArray[i] = xhrArray[i].status;
                } else if (xhrArray[i].readyState === 4) {
                    currentCall.callBackInProgress = true;
                    currentCall.callBack(xhrArray[i].response);
                    if (errorsOccurred === true) currentCall.httpErrorCallBack(currentCall.responseStateArray);
                }
            };
            xhrArray[i].open(currentCall.method, endpointUrl, true);
            xhrArray[i].setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhrArray[i].setRequestHeader("Authorization", "Bearer " + sessionToken,);

            let formDataString = "start=" + startPos + "&end=" + endPos;

            const keys = Object.keys(currentCall.callParameters);
            if (currentCall.callParameters && currentCall.callParameters instanceof Object && keys.length > 0) {
                console.info("Call parameter data: ", currentCall.callParameters);
                keys.forEach(key => {
                    console.info("Setting formData: ", key, "=", currentCall.callParameters[key]);
                    formDataString += "&" + key + "=" + currentCall.callParameters[key];
                });
            }
            xhrArray[i].send(formDataString);

        }

        // currentCall.run(); *!!! BEWARE!!! Infinity-loop danger
    }

}