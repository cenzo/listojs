"use strict";
const analyzeRequestedPage = function (urlToAnalyze, callbackIcons, callbackMeta, errorCallBack) {
        console.log("Worker trying to analyze page '" + urlToAnalyze + "'...");
        const useridForBilling = sessionStorage.getItem("dovisUserID");
        if (useridForBilling) {
            apiCall('', `/8/icons`, {userid: useridForBilling, url: urlToAnalyze}, function (iconsRead) {
                callbackIcons(iconsRead);
                apiCall('', `/8/meta`, {userid: useridForBilling, url: urlToAnalyze}, function (metaRead) {
                    callbackMeta(metaRead);
                }, function (metaRead) {
                    console.error("could not read metadata");
                    errorCallBack(metaRead);
                }, "post", "url")
            }, function (iconsRead) {
                console.error("could not read icons");
                errorCallBack(iconsRead);
            }, "post", "url");
            return true;
        } else {
            return false;
        }
    }
;