"use strict";

let OPN_LNK = "Click here to open the link:";
let RSRV_LNK = "Reserve this short url:";
let UNKNOWN_ERR="Unknown error";
let dgi = function (el) {
    return document.getElementById(el);
}

dgi("back_home").href = APPLICATION_HOMEPAGE;

let setClickTexts = function () {
    let l = navigator.language || navigator.browserLanguage;
    if (l.startsWith("de")) {
        OPN_LNK = "Hier klicken um zur Seite zu gelangen:";
        RSRV_LNK = "Diese Kurz-Url reservieren:";
        UNKNOWN_ERR="Fehler";
    } else if (l.startsWith("it")) {
        OPN_LNK = "Clicca qui per accedere al link:";
        RSRV_LNK = "Prenota questa Url breve:";
        UNKNOWN_ERR="Errore";
    }
}
let bid = function (r, s, t,sc) {
    r.setAttribute("href",
        `${APPLICATION_HOMEPAGE}?bidurl=${sc}`)
    s.innerHTML = `<img src="img/shopping-icon.png" style="max-width:50px;" alt="reserve ${sc}"><h4>${APPLICATION_HOMEPAGE}/${sc}</h4>`;
    t.innerText = RSRV_LNK;
}
console.log("pathname on /t: " + window.location.pathname + " \n search: " + window.location.search + "\nhash:" + window.location.hash);
console.log("server: ", selectedServer);
// in production use this instead of next line: let p = window.location.pathname;
let p = window.location.search.substring(1);
let sup = p.substring(1 + p.lastIndexOf("/"), p.length);
let rdUrl = dgi("rdUrl");
let rdirLabel = dgi("linkLabel");
let txt = dgi("clickText");
let shortUrl = dgi("shorturl");
let sc;
let redirect = function () {
    if (sup.endsWith('%2b')) {
        sc = sup.substring(0, sup.length - 3);
    } else if (sup.endsWith('+')) {
        sc = sup.substring(0, sup.length - 1);
    } else {
        sc = sup;
    }

    let x = new XMLHttpRequest();
    x.open("GET", `${selectedServer}${customerID}/8/public/select?shortcut=${sc}`, true);
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xst = -1;
    x.onreadystatechange = function () {
        console.log("status", x.status);
        xst = x.readyState;
        if (xst === 4) {
            if (x.status >= 200 && x.status <= 204) {
                let jArr = JSON.parse(x.responseText);
                let jObj = jArr[0];
                shortUrl.innerText = `${APPLICATION_HOMEPAGE}/${sc}`;
                rdUrl.setAttribute("target", "_blank");
                if (jObj.count === 0)
                    bid(rdUrl, rdirLabel, txt,sc);
                else {
                    jArr = JSON.parse(x.responseText);
                    jObj = jArr[0];
                    let pg = jObj.rows[0].s[2];
                    let dPg = decodeURIComponent(pg);
                    let cat = jObj.rows[0].s[4];
                    if (!sc) window.location.href = APPLICATION_HOMEPAGE;
                    if (cat < 6) {
                        rdUrl.setAttribute("href",
                            `${dPg}`);
                        rdirLabel.innerText = `${dPg}`;
                        txt.innerText = OPN_LNK;
                    } else bid(rdUrl, rdirLabel, txt,sc);
                }
            } else {
                rdirLabel.innerHTML = `<br/>
                          <br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${x.status}&nbsp;&nbsp;`;
                rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
            }
            dgi("ad1").innerHTML = `<span><img style="width: 75px;max-height: 75px;" src="img/testimonials-1.jpg" alt="ad1"></span>`;
            dgi("ad2").innerHTML = `<span><img style="width: 75px;max-height: 75px;" src="img/testimonials-2.jpg" alt="ad2"></span>`;
            dgi("ad3").innerHTML = `<span><img style="width: 75px;max-height: 75px;" src="img/testimonials-3.jpg" alt="ad3"></span>`;
            dgi("ad4").innerHTML = `<span><img style="width: 75px;max-height: 75px;" src="img/testimonials-1.jpg" alt="ad4"></span>`;
            dgi("ad5").innerHTML = `<span><img style="width: 75px;max-height: 75px;" src="img/testimonials-2.jpg" alt="ad5"></span>`;
        }
    };
    try {
        x.send();

    } catch (err) {
        console.error(err.toString());
        rdirLabel.innerHTML = `<br/><br/>&nbsp;&nbsp;${UNKNOWN_ERR}&nbsp;&nbsp;`;
        rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
    }
};

