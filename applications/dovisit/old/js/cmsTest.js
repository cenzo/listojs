const cmsTest={
  api: `<div>API</div>`,
  showCase: `<ul class="list-group"">
  <li class="list-group-item">kindergarten</li>
  <li class="list-group-item">mailing</li>
  <a class="list-group-item" href="https://www.dovis.it/t/test/" target="_blank">dovis server test page</a>
  </ul>`,
  test: `<div class="form-group"><h3>Test</h3>
  <ul>This is a test for the footer
  <li class="list-group-item" style="display:inline-block;"><a class="badge-pill rounded" onclick="loadFile('./js/index.js','footer-showJS')">index.js</a> </li>
  <li class="list-group-item" style="display:inline-block;"><a class="badge-pill" onclick="loadFile('./js/localizationV1/localization_functions.js','footer-showJS')">localization_functions.js</a> </li>
  <li class="list-group-item" style="display:inline-block;"><a class="badge-pill" onclick="loadFile('./js/apiEvents_dovis.js','footer-showJS')">apiEvents.js</a> </li>
  <li class="list-group-item" style="display:inline-block;"><a class="badge-pill" onclick="loadFile('./js/dovis_function.js','footer-showJS')">dovis_functions.js</a> </li>
  </ul></div>`
}