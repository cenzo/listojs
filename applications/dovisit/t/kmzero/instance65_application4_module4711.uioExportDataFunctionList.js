

//Data-Function for requirement "Backend" triggered by "submit"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget101submitData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	console.info("Function 'Backend'","Data for widget m4UIOwidget101",dataObject);

}

//Data-Function for requirement "Frontend" triggered by "submit"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget102submitData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	const callBack=function(returnedData){
	//TODO: write functionalities here what the callBack-function of the apiCall must do with the retrieved data

	}
	apiCall("65/4/4711/1","UIO1frontend",dataObject,"POST",callBack);
	console.info("Function 'Frontend'","Data for widget m4UIOwidget102",dataObject);

}

//Data-Function for requirement "password" triggered by "keyup"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget104keyupData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	console.info("Function 'password'","Data for widget m4UIOwidget104",dataObject);

}

//Data-Function for requirement "loginbutton" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget106clickData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	const callBack=function(returnedData){
	//TODO: write functionalities here what the callBack-function of the apiCall must do with the retrieved data

	}
	apiCall("65/4/4711/3","UIO3loginbutton",dataObject,"POST",callBack);
	console.info("Function 'loginbutton'","Data for widget m4UIOwidget106",dataObject);

}

//Data-Function for requirement "loginbutton" triggered by "mouseenter"-event
/*Specifications for api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget106mouseenterData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	const callBack=function(returnedData){
	//TODO: write functionalities here what the callBack-function of the apiCall must do with the retrieved data

	}
	apiCall("65/4/4711/4","UIO4loginbutton",dataObject,"POST",callBack);
	console.info("Function 'loginbutton'","Data for widget m4UIOwidget106",dataObject);

}

//Data-Function for requirement "Inizia tracciamento" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
let deltaX=0;
let deltaY=0;

const m4UIOwidget109clickData=function(dataObject){

      console.log("call m4UIOwidget109clickData");
		const setPosition=function(position) {
			//deltaX+=0.100;
			//deltaY+=0.100;
			const lat=position.coords.latitude+deltaX;
			const lon=position.coords.longitude+deltaY;
			const date = new Date();
            let ts = date.toISOString();
			 
		  trackedPath.push(`\n(${lat.toFixed(4)},${lon.toFixed(4)},${ts})`);
		  document.getElementById("field112").value=lat;
		  document.getElementById("field113").value=lon;
		}
	
	  if (navigator.geolocation) {
		if (doTracking) {	 
			navigator.geolocation.getCurrentPosition(setPosition);
			showCoordinates();
		}
	  } else { 
		x.innerHTML = "Geolocation is not supported by this browser.";
	  }
}

//Data-Function for requirement "Inizia tracciamento" triggered by "wheel"-event
/*Specifications for api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget109wheelData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	console.info("Function 'Inizia tracciamento'","Data for widget m4UIOwidget109",dataObject);

}

//Data-Function for requirement "Stop tracciamento" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget110clickData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	console.info("Function 'Stop tracciamento'","Data for widget m4UIOwidget110",dataObject);

}

//Data-Function for requirement "Stop tracciamento" triggered by "wheel"-event
/*Specifications for api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget110wheelData=function(dataObject){

	//TODO: write functionalities here what to do with the dataObject
	console.info("Function 'Stop tracciamento'","Data for widget m4UIOwidget110",dataObject);

}

//Data-Function for requirement "testZoom" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget111changeData=function(dataObject){

    showCoordinates();


}

//Data-Function for requirement "testLon" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget112changeData=function(dataObject){

    showCoordinates();

}

//Data-Function for requirement "testLat" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget113changeData=function(dataObject){

    showCoordinates();


}

//Data-Function for requirement "testY" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget115changeData=function(dataObject){

	showTiles();

}

//Data-Function for requirement "testX" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget116changeData=function(dataObject){

	showTiles();

}


//Data-Function for requirement "Mostra percorso registrato" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget121clickData=function(dataObject){

    console.info("m4UIOwidget121clickData",dataObject);
	document.getElementById("output120").innerText=dataObject.trackedPathArray;

}
