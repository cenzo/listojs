//Event-listener for requirement "Backend" for "submit"-event
//document.getElementById("widget101").addEventListener("submit",m4UIOwidget101submit);

//Event-listener for requirement "Frontend" for "submit"-event
//document.getElementById("widget102").addEventListener("submit",m4UIOwidget102submit);

//Event-listener for requirement "password" for "keyup"-event
document.getElementById("widget104").addEventListener("keyup",m4UIOwidget104keyup);

//Event-listener for requirement "loginbutton" for "click"-event
document.getElementById("widget106").addEventListener("click",m4UIOwidget106click);

//Event-listener for requirement "loginbutton" for "mouseenter"-event
document.getElementById("widget106").addEventListener("mouseenter",m4UIOwidget106mouseenter);

//Event-listener for requirement "Inizia tracciamento" for "click"-event
document.getElementById("widget109").addEventListener("click",m4UIOwidget109click);

//Event-listener for requirement "Inizia tracciamento" for "wheel"-event
document.getElementById("widget109").addEventListener("wheel",m4UIOwidget109wheel);

//Event-listener for requirement "Stop tracciamento" for "click"-event
document.getElementById("widget110").addEventListener("click",m4UIOwidget110click);

//Event-listener for requirement "Stop tracciamento" for "wheel"-event
document.getElementById("widget110").addEventListener("wheel",m4UIOwidget110wheel);

//Event-listener for requirement "testZoom" for "change"-event
document.getElementById("widget111").addEventListener("change",m4UIOwidget111change);

//Event-listener for requirement "testLon" for "change"-event
document.getElementById("widget112").addEventListener("change",m4UIOwidget112change);

//Event-listener for requirement "testLat" for "change"-event
document.getElementById("widget113").addEventListener("change",m4UIOwidget113change);

//Event-listener for requirement "testY" for "change"-event
document.getElementById("widget115").addEventListener("change",m4UIOwidget115change);

//Event-listener for requirement "testX" for "change"-event
document.getElementById("widget116").addEventListener("change",m4UIOwidget116change);

//Event-listener for requirement "Mostra percorso registrato" for "click"-event
document.getElementById("widget121").addEventListener("click",m4UIOwidget121click);

let sessionToken;
const apiCall=function(operation,callerObject,dataObject,method,callBackfunction) {
	
	 const url=operation;
	 const xhttp = new XMLHttpRequest();
     console.info(`Opening url '${url}' with method '${method}'...`);

	console.info('APICALL ',operation,callerObject,dataObject,method,callBackfunction);
	    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            console.info(`xhttp-response: ${this.response}`);
            let responseObject;
            try {
                responseObject = JSON.parse(this.response);
            }catch  {
                responseObject = {};
                responseObject.response=this.response;
            }
            return callBackfunction(this.status, responseObject);
        }
    };
    xhttp.open(method, url, true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
    if (sessionToken) xhttp.setRequestHeader("Authorization", "Bearer " + sessionToken);
    if (dataObject) xhttp.send(dataObject);
    else xhttp.send();
	}
	
	
const lon2tile=function(lon,zoom) {
	const n = Math.pow(2,zoom);
	const deg=(lon+180)/360;
	const x=Math.floor(n*deg);
	console.log("lon=",lon,"zoom=",zoom,"n=",n,"deg=",deg,"x:",x);
	return x; 
	}
	
	
const lat2tile=function(lat,zoom)  {
	console.log("lat=",lat);

	return (Math.floor((1-Math.log(Math.tan(lat*Math.PI/180) + 1/Math.cos(lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,zoom))); 
	}

const showCoordinates=function(){
	const z=document.getElementById("field111").value;
	const lon=100*document.getElementById("field113").value/100;
	const lat=100*document.getElementById("field112").value/100;
	const x=lon2tile(lon,z);
	const y=lat2tile(lat,z);
	 
    const url=`https://tile.thunderforest.com/outdoors/${z}/${x}/${y}.png?apikey=50811c6b5db84478a2f4942e972aa374`;
	document.getElementById("image114").setAttribute("src",url);
	
	 
	console.info("Tile data (x,y,z): ",x,y,z,"position:",lon,lat);
	document.getElementById("field115").value=x.toFixed(0);
	document.getElementById("field116").value=y.toFixed(0);
}
let trackedPath=[];
const showTiles=function(){
	const z=document.getElementById("field111").value;
	const x=document.getElementById("field115").value;
	const y=document.getElementById("field116").value;
 	
    const url=`https://tile.thunderforest.com/outdoors/${z}/${x}/${y}@2x.png?apikey=50811c6b5db84478a2f4942e972aa374`;
	document.getElementById("image114").setAttribute("src",url);
	const n = Math.pow(2,z);
	const lon_deg = ((x / n) * 360.0) - 180.0
	const calc = (1 - 2 * y / n);
    const lat_rad = Math.atan(Math.sinh(Math.PI * calc));
    const lat_deg = lat_rad * 180.0 / Math.PI;
	console.info("longitude,latitude",lon_deg,lat_deg);
 
	document.getElementById("field113").value=lon_deg.toFixed(5);
	document.getElementById("field112").value=lat_deg.toFixed(5);
}
let doTracking=false;
let positionInterval;
const getLocation=function () {
  if (navigator.geolocation && doTracking) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

