
const TableStyle = {
    tableHeadOpen: `<table class="table table-hover table-sm">`,
    tableHeadClose: ``,
    thOpen: '<th>',
    thClose: '</th>',
    trOpen: '<tr>',
    trClose: '</tr>',
    tdOpen: '<td>',
    tdClose: '</td>',
    aOpen: '<a',
    aAttributes: '>',
    aClose: '</a>',
    bottom: '</table>',
    elements: [],
};