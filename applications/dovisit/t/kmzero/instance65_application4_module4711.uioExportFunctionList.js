

//Function for requirement "Backend" triggered by "submit"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget101submit=function(){
	const dataObject={
	userIsLoggedIn:"fill_userIsLoggedIn_with_data_from_appropriate_DOMelement",
	userIsLoggedIn:"fill_userIsLoggedIn_with_data_from_appropriate_DOMelement",
	userIsLoggedIn:"fill_userIsLoggedIn_with_data_from_appropriate_DOMelement",
	xPosition:"fill_xPosition_with_data_from_appropriate_DOMelement",
	yPosition:"fill_yPosition_with_data_from_appropriate_DOMelement",
	zPosition:"fill_zPosition_with_data_from_appropriate_DOMelement",
	recordTimestamp:"fill_recordTimestamp_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget101submitData(dataObject);

}

//Function for requirement "Frontend" triggered by "submit"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget102submit=function(){
	const dataObject={
	xPosition:"fill_xPosition_with_data_from_appropriate_DOMelement",
	yPosition:"fill_yPosition_with_data_from_appropriate_DOMelement",
	zPosition:"fill_zPosition_with_data_from_appropriate_DOMelement",
	recordTimestamp:"fill_recordTimestamp_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget102submitData(dataObject);

}

//Function for requirement "password" triggered by "keyup"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget104keyup=function(){
	const dataObject={
	password:"fill_password_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget104keyupData(dataObject);

}

//Function for requirement "loginbutton" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget106click=function(){
	const dataObject={
	username:"fill_username_with_data_from_appropriate_DOMelement",
	password:"fill_password_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget106clickData(dataObject);

}

//Function for requirement "loginbutton" triggered by "mouseenter"-event
/*Specifications for api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget106mouseenter=function(){
	const dataObject={
	}

	m4UIOwidget106mouseenterData(dataObject);

}

//Function for requirement "Inizia tracciamento" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget109click=function(){
	const dataObject={
	startRecording:"fill_startRecording_with_data_from_appropriate_DOMelement"
	}
    doTracking=true;
	positionInterval=setInterval(m4UIOwidget109clickData, 10000) // run  every 10 seconds
	

}

//Function for requirement "Inizia tracciamento" triggered by "wheel"-event
/*Specifications for api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget109wheel=function(){
	const dataObject={
	clickParameter1:"fill_clickParameter1_with_data_from_appropriate_DOMelement",
	clickParameter2:"fill_clickParameter2_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget109wheelData(dataObject);

}

//Function for requirement "Stop tracciamento" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget110click=function(){
	const dataObject={
	stopRecording:"fill_stopRecording_with_data_from_appropriate_DOMelement"
	}
    doTracking=false;
	clearInterval(positionInterval);
	m4UIOwidget110clickData(dataObject);

}

//Function for requirement "Stop tracciamento" triggered by "wheel"-event
/*Specifications for api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget110wheel=function(){
	const dataObject={
	clickParameter1:"fill_clickParameter1_with_data_from_appropriate_DOMelement",
	clickParameter2:"fill_clickParameter2_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget110wheelData(dataObject);

}

//Function for requirement "testZoom" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget111change=function(){
	const dataObject={
	testZoom:"fill_testZoom_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget111changeData(dataObject);

}

//Function for requirement "testLon" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget112change=function(){
	const dataObject={
	testLongit:"fill_testLongit_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget112changeData(dataObject);

}

//Function for requirement "testLat" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget113change=function(){
	const dataObject={
	testLatitude:"fill_testLatitude_with_data_from_appropriate_DOMelement"
	}

	m4UIOwidget113changeData(dataObject);

}

//Function for requirement "testY" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget115change=function(){
	const dataObject={
	}

	m4UIOwidget115changeData(dataObject);

}

//Function for requirement "testX" triggered by "change"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget116change=function(){
	const dataObject={
	}

	m4UIOwidget116changeData(dataObject);

}


//Function for requirement "Mostra percorso registrato" triggered by "click"-event
/*Specifications for  api- (or general backend-) processing*/
/*Specifications for frontend processing*/
const m4UIOwidget121click=function(){
	const dataObject={
	trackedPathArray:trackedPath
	}

	m4UIOwidget121clickData(dataObject);

}
