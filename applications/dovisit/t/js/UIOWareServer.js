const callUioApi = function (action, userID, project, callbackFunction) {
    const NO_PROJECT_DATA_IS_SET = "no project-data was set to be posted;"
    const CANNOT_UPLOAD_LOCAL_PROJECT = "Cannot upload a local project. Only public projects can be updated on the server";
    let url, httpMethod, content;
    let params = ``;
    const commitOperation = action.toLowerCase().trim();
    const api = getAPIServerPath() + "900/9/";
    switch (commitOperation) {
        case "list":
            httpMethod = "GET";
            url = api + "9000/4/_nul_/x/x/public";
            break;
        case "create":
            httpMethod = "PUT";
            url = api + "9000/1";
            if (!project) {
                UIOTool.errorMsg(NO_PROJECT_DATA_IS_SET);
                throw new Error(NO_PROJECT_DATA_IS_SET);
            }
            params += `names=filename&types=STRING&value1=${encodeURIComponent(project)}`;
            break;
        case "load":
            httpMethod = "POST";
            url = api + "9000/2";
            if (!project) {
                UIOTool.errorMsg(NO_PROJECT_DATA_IS_SET);
                throw new Error(NO_PROJECT_DATA_IS_SET);
            }
            params += `names=filename&types=STRING&value1=${encodeURIComponent(project)}`;
            break;
        case "save":
            httpMethod = "POST";
            if (!project) {
                UIOTool.errorMsg(NO_PROJECT_DATA_IS_SET);
                throw new Error(NO_PROJECT_DATA_IS_SET);
            }
            if (isLocalProject) {
                //UIOTool.errorMsg(CANNOT_UPLOAD_LOCAL_PROJECT);
                throw new Error(CANNOT_UPLOAD_LOCAL_PROJECT);
            }
            //params += `names=filename,jsoncontent&types=STRING,STRING&value1=u${currentDovisUserID}.${project.projectName}.json&value2=${encodeURIComponent(JSON.stringify(project))}`;
            params += `names=filename,jsoncontent&types=STRING,STRING&value1=${project.projectName}.json&value2=${encodeURIComponent(JSON.stringify(project))}`;
            url = api + "9000/3";
            break;
        case "html":
            httpMethod = "POST";
            if (!project) {
                UIOTool.errorMsg(NO_PROJECT_DATA_IS_SET);
                throw new Error(NO_PROJECT_DATA_IS_SET);
            }
            params += `names=filename,instanceID,applicationID,moduleID&types=STRING,integer,integer,integer&value1=${project.projectName}.json&value2=${currentDovisUserID}&value3=${project.id}&value4=4711`;
            url = api + "9001/7";
            break;
        case "usersettings":
            httpMethod = "POST";
            params += `names=_nul_&types=_nul_`;
            url = api + "9001/8";
            break;
        default:
            throw new Error(`Invalid commit-operation '${action}'. Only create,load and save are allowed.`);
    }


    const xhttp = new XMLHttpRequest();
    console.info(`Opening url '${url}' with method '${httpMethod}'...`);

    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            console.info(`xhttp-response: ${this.response}`);
            let responseObject;
            try {
                responseObject = JSON.parse(this.response);
            } catch {
                responseObject = {};
                responseObject.response = this.response;
            }
            return callbackFunction(this.status, responseObject);
        }
    };
    xhttp.open(httpMethod, url, true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
    if (sessionToken) xhttp.setRequestHeader("Authorization", "Bearer " + sessionToken);
    if (commitOperation !== "list") xhttp.send(params);
    else xhttp.send();
}
