const ApiTester = {};
ApiTester.constants = {
    LOGIN_ERROR: "Login error",
    server: "https://backend.dovis.it",
    baseUrl: "/RMLRest2-1.14.5/rml/",
    customerID: 801,
    applicationID: 8,
    appModule: 8001
}

const getUserToken = function (userName, pass,callback) {
    const xhttp = new XMLHttpRequest();
    const test = ApiTester.constants;
    const postUrl = `${test.server}${test.baseUrl}${test.customerID}/${test.applicationID}/${test.appModule}/login`;
    let jsonResponse = {};
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                jsonResponse = JSON.parse(xhttp.responseText);
                const testToken = jsonResponse.bearerToken.split(";")[0];
                callback(testToken);
            } else if (xhttp.status >= 400)  {
                    httpLog(xhttp.status, "");
            } else {
                httpLog(xhttp.status, ApiTester.constants.LOGIN_ERROR);
            }
        }
    };
    xhttp.open("POST", postUrl, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + userName + "&password=" + pass);
}

const userApiCall = function (apiCall, userName, pass) {
    const xhttp = new XMLHttpRequest();
    const test = ApiTester.constants;
    const postUrl = `${test.server}${test.baseUrl}${test.customerID}/${test.applicationID}/${test.appModule}/login`;
    let jsonResponse = {};
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                jsonResponse = JSON.parse(xhttp.responseText);
                const testToken = jsonResponse.bearerToken.split(";")[0];
                // we need userEmail for payment checkout:
                const userEmail = jsonResponse.bearerToken.split(";")[1];
                testApi(apiCall, testToken);
            } else if (xhttp.status >= 400) {
                if (xhttp.status === 400) {
                    httpLog(xhttp.status, "bad request");
                } else if (xhttp.status === 401) {
                    httpLog(xhttp.status, "not authenticated");
                } else if (xhttp.status === 403) {
                    httpLog(xhttp.status, "not authorized");
                } else {
                    httpLog(xhttp.status, "");
                }
            } else {
                httpLog(xhttp.status, ApiTester.constants.LOGIN_ERROR);
            }
        }
    };
    xhttp.open("POST", postUrl, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + userName + "&password=" + pass);
}

const httpLog = function (status, str) {
    console.error(`HTTP${status}: ${str}`);
}

const parallelApiCall = function (apiCall, userArray, pwArray) {
    for (let i = 0; i < userArray.length; i++) userApiCall(apiCall, userArray[i], pwArray[i]);
}

const testApi = function (apiCall, token) {
    callApi(apiCall, token, apiCall['callBack']);
}
const isEmpty = function (object) {
    if (!object) return false;
    return Object.keys(object).length === 0;
}
const callApi = function (apiCall, token, callbackFunction) {
    const url = ApiTester.constants.server + ApiTester.constants.baseUrl + ApiTester.constants.customerID
        + "/" + ApiTester.constants.applicationID + apiCall.endpoint;
    const xhttp = new XMLHttpRequest();
    console.log(`Opening url '${url}' with method '${apiCall.method}'...`);
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            const responseObject = JSON.parse(this.response);
            console.log(`http-rc: ${this.status}`);
            return callbackFunction(this.status, responseObject);
        }
    };
    xhttp.open(apiCall.method, url, true);
    xhttp.setRequestHeader("Authorization", "Bearer " + token);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

    if (isEmpty(apiCall.parameters)) xhttp.send();
    else xhttp.send(apiCall.parameters);
}
const  create_UUID=function(){
    let dt = new Date().getTime();
    let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

const test1 = function () {

    //const userArray1 = ['tester1', 'tester2', 'tester3', 'tester4', 'tester5'];
    //const userPw1 = ['tester1PW', 'tester2PW', 'tester3PW', 'tester4PW', 'tester5PW'];
    const userArray1 = ['tester4','tester5'];
    const userPw1 = ['tester4PW','tester5PW'];
    const shortcutName = function (length) {
        let result = [];
        let characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result.push(characters.charAt(Math.floor(Math.random() *
                charactersLength)));
        }
        return result.join('');
    };
    const apiCall1 = {};
    apiCall1.method = "PUT";
    apiCall1.endpoint = "/user/create";
    apiCall1.parameters = "urlstring=http%3A%2F%2Fwww.blabla" + create_UUID() + ".com&shortcut="+shortcutName(6)+"&summary=testSummary&keywords=testKeyword=&author=test&subject=&image1=&image2=&image3=&articleabstract=&taglist=testTag&icon=&idcategory=1&description=testdescription\"";

    apiCall1.callBack = function (status, response) {
        if (status < 200) console.error("could not place call", response);
        else if (status > 204) console.error("error in response", response);
        else console.info("call succeeded", response);
    }
    parallelApiCall(apiCall1, userArray1, userPw1);

}

const test2 = function () {

    const userArray1 = ['tester1', 'tester2', 'tester3', 'tester4', 'tester5'];
    const userPw1 = ['tester1PW', 'tester2PW', 'tester3PW', 'tester4PW', 'tester5PW'];



    const apiCall = {};
    apiCall.method = "POST";
    apiCall.endpoint = "/user/usershortcuts";
    apiCall.parameters = null;

    apiCall.callBack = function (status, response) {
        if (status < 200) console.error("could not place call", response);
        else if (status > 204) console.error("error in response", response);
        else console.info("call succeeded", response);
    }
    parallelApiCall(apiCall, userArray1, userPw1);

}


const fillTokens=function(){
    const userArray = ['tester1', 'tester2', 'tester3', 'tester4', 'tester5'];
    const userPw = ['tester1PW', 'tester2PW', 'tester3PW', 'tester4PW', 'tester5PW'];
    const tokens=[];

    for(let i=1;i<=5;i++){
        const j=Math.round(i-1);
        getUserToken(userArray[j],userPw[j]),function(token){
            tokens.push(token);
        };
    }

    console.log(tokens);
}