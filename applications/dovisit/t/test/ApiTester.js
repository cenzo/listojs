const ApiTester = {};
let tokens = [];
const userArray = ['tester01', 'tester02', 'tester03', 'tester04', 'tester05', 'tester06', 'tester07', 'tester08', 'tester09', 'tester10',
    'tester11', 'tester12', 'tester13', 'tester14', 'tester15', 'tester16', 'tester17', 'Tester18', 'tester19', 'tester20', 'tester21', 'tester22'];
const userPw = ['tester1PW', 'tester2PW', 'tester3PW', 'tester4PW', 'tester5PW', 'tester6PW', 'tester7PW', 'tester8PW', 'tester9PW', 'tester10PW',
    'tester11PW', 'tester12PW', 'tester13PW', 'tester14PW', 'tester15PW', 'tester16PW', 'tester17PW', 'tester18PW', 'tester19PW', 'tester20PW', 'tester21PW', 'tester22PW'];
const userTokenMap = new Map();

ApiTester.constants = {
    /* base url for localhost is always   "/RMLRest2/rml/" */
    LOGIN_ERROR: "Login error",
    server: "http://localhost:8080",
    baseUrl: "/RMLRest2/rml/",
    customerID: 801,
    applicationID: 8,
    appModule: 8001,
    serverVersion: "1.22"
}
const setCustomer = function () {
    console.info("Selected customer is: " + ApiTester.constants.customerID);
    const customerSelector = document.getElementById("selectCustomer");
    const customer = customerSelector.value;
    ApiTester.constants.customerID = customer;
    console.info("Selected customer changed to: " + ApiTester.constants.customerID);
}
const setServer = function () {
    console.info("Selected serverUrl was: " + ApiTester.constants.server + ApiTester.constants.baseUrl);
    const serverSelector = document.getElementById("selectServer");
    const server = serverSelector.value;
    if (server === "local") {
        ApiTester.constants.server = "http://localhost:8080";
        ApiTester.constants.baseUrl = "/RMLRest2/rml/"
    } else if (server === "backend") {
        ApiTester.constants.server = "https://backend.dovis.it";
        ApiTester.constants.baseUrl = "/RMLRest2-" + ApiTester.constants.serverVersion + "/rml/"
    } else console.error("undefined server data have been selected");
    console.info("Selected serverUrl changed to: " + ApiTester.constants.server + ApiTester.constants.baseUrl);
}
const shortcutName = function (length) {
    let result = [];
    let characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        const idxAt = Math.floor(Math.random() *
            charactersLength)
        result.push(characters.charAt(idxAt));
    }
    const sc = result.join('');

    return sc;
};
const create_UUID = function () {
    let dt = new Date().getTime();
    let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = (dt + Math.random() * 16) % 16 | 0;
        dt = Math.floor(dt / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });

    return uuid;
}
const apiCallBack = function (token, status, response) {
    const responseObject = {}
    responseObject.status = status;
    responseObject.response = response;

    if (status === 200) {
        showResponse(token, responseObject, false);
        const table=document.getElementById("viewResults");
        sortTable(table);
    } else {
        showResponse(token, responseObject, true);
        const table=document.getElementById("viewResults");
        sortTable(table);
        console.info("responseObject", responseObject);
    }
};
const apiCall1 = {};
apiCall1.method = "PUT";
apiCall1.endpoint = "/user/create";
apiCall1.parameters = "urlstring=http%3A%2F%2Fwww.blabla" + create_UUID() + ".com&shortcut=" + shortcutName(6) + "&summary=testSummary&keywords=testKeyword=&author=test&subject=&image1=&image2=&image3=&articleabstract=&taglist=testTag&icon=&idcategory=1&description=testdescription\"";
apiCall1.callBack = apiCallBack;
apiCall1.isSecured = true;
const apiCall2 = {};
apiCall2.method = "POST";
apiCall2.endpoint = "/user/usershortcuts";
apiCall2.parameters = null;
apiCall2.callBack = apiCallBack;
apiCall2.isSecured = true;
const apiLogout = {};
apiLogout.method = "POST";
apiLogout.endpoint = "/logout";
apiLogout.parameters = "?token=";
apiLogout.callBack = function () {
    tokens = [];
};
apiLogout.isSecured = false;

const pingServer = function () {
    const xhttp = new XMLHttpRequest();
    const test = ApiTester.constants;
    const url = `${test.server}${test.baseUrl}0/0/1001/1/_nul_/_nul_/_nul_/public`;
    let serverResponse = {};
    const showServer = document.getElementById("showUser");
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                serverResponse = JSON.parse(xhttp.responseText);
                const dataHandleCount = serverResponse['session-monitor']['dataHandleCount'];
                showServer.innerText = dataHandleCount + " threads connected";
                viewServerData(serverResponse);
            } else {
                if (xhttp.status === 0) showServer.innerText = "Server '" + test.server + "' did  not respond."
                else showServer.innerText = "Check failed with status " + xhttp.status
            }
        } else {
            showServer.innerText = "Calling url " + url + " ...";
        }
    };
    xhttp.open("GET", url, true);
    //xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xhttp.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhttp.send();
}

const getUserToken = function (userName, pass, callback) {
    const xhttp = new XMLHttpRequest();
    const test = ApiTester.constants;
    const postUrl = `${test.server}${test.baseUrl}${test.customerID}/${test.applicationID}/${test.appModule}/login`;
    let jsonResponse = {};
    const showToken = document.getElementById("showToken");
    const showUser = document.getElementById("showUser");
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                jsonResponse = JSON.parse(xhttp.responseText);
                const testToken = jsonResponse.bearerToken.split(";")[0];
                userTokenMap.set(testToken, userName);
                showUser.innerText = userName;
                showToken.innerHTML = `<br><b>${userName}: ${testToken}</b>`;
                callback(testToken);
            } else if (xhttp.status >= 400) {
                httpLog(xhttp.status, "");
            } else {
                httpLog(xhttp.status, ApiTester.constants.LOGIN_ERROR);
            }
        }
    };
    xhttp.open("POST", postUrl, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + userName + "&password=" + pass);
}

const loginUserAndCallApi = function (apiCall, userName, pass) {
    const xhttp = new XMLHttpRequest();
    const test = ApiTester.constants;
    const postUrl = `${test.server}${test.baseUrl}${test.customerID}/${test.applicationID}/${test.appModule}/login`;
    let jsonResponse = {};
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                jsonResponse = JSON.parse(xhttp.responseText);
                const testToken = jsonResponse.bearerToken.split(";")[0];
                // we need userEmail for payment checkout:
                const userEmail = jsonResponse.bearerToken.split(";")[1];
                testApi(apiCall, testToken);
            } else if (xhttp.status >= 400) {
                if (xhttp.status === 400) {
                    httpLog(xhttp.status, "bad request");
                } else if (xhttp.status === 401) {
                    httpLog(xhttp.status, "not authenticated");
                } else if (xhttp.status === 403) {
                    httpLog(xhttp.status, "not authorized");
                } else {
                    httpLog(xhttp.status, "");
                }
            } else {
                httpLog(xhttp.status, ApiTester.constants.LOGIN_ERROR);
            }
        }
    };
    xhttp.open("POST", postUrl, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("username=" + userName + "&password=" + pass);
}

const httpLog = function (status, str) {
    console.error(`HTTP${status}: ${str}`);
}

const parallelLoginApiCall = function (apiCall, userArray, pwArray) {
    for (let i = 0; i < userArray.length; i++) loginUserAndCallApi(apiCall, userArray[i], pwArray[i]);
}

const testApi = function (apiCall, token) {
    callApi(apiCall, token, apiCall['callBack']);
}

const isEmpty = function (object) {
    if (!object) return false;
    return Object.keys(object).length === 0;
}
const callApi = function (apiCall, token, callbackFunction) {
    const url = ApiTester.constants.server + ApiTester.constants.baseUrl + ApiTester.constants.customerID
        + "/" + ApiTester.constants.applicationID + apiCall.endpoint;
    const xhttp = new XMLHttpRequest();
    // console.log(`Opening url '${url}' with method '${apiCall.method}'...`);
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            const responseObject = JSON.parse(this.response);
            console.log(`http-rc: ${this.status}`);
            return callbackFunction(token, this.status, responseObject);
        }
    };
    xhttp.open(apiCall.method, url, true);
    xhttp.setRequestHeader("Authorization", "Bearer " + token);
    xhttp.setRequestHeader("Access-Control-Allow-Origin", "https://backend.dovis.it");
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

    if (isEmpty(apiCall.parameters)) xhttp.send();
    else xhttp.send(apiCall.parameters);
}


const test1WithLogin = function () {
    parallelLoginApiCall(apiCall1, userArray, userPw);
}

const test2WithLogin = function () {
    parallelLoginApiCall(apiCall2, userArray, userPw);
}
/*--------------------------------------------------------------------------------------------------------------------*/


const callApiWithToken = function (idx, apiCall) {
    const url = ApiTester.constants.server + ApiTester.constants.baseUrl + ApiTester.constants.customerID
        + "/" + ApiTester.constants.applicationID + apiCall.endpoint;
    const xhttp = new XMLHttpRequest();
    // console.info(`Opening url '${url}' with method '${apiCall.method}'...`);
    const token = tokens[idx];
    xhttp.onreadystatechange = function () {
        let inError = false;
        if (this.readyState === 4) {
            // let responseObject;
            // if (this.response) try {
            //     responseObject = JSON.parse(this.response);
            // } catch (err) {
            //     inError=true;
            //     responseObject=this.response;
            //     console.log("ERROR: --> " + err.toString()+" (responseObject="+responseObject+")");
            // }
            // console.info(userArray[idx], `http-rc: ${this.status}`);
            // if (inError===true)return apiCall.callBack(token, this.status, this.response);
            // else return apiCall.callBack(token, this.status, responseObject);
            return apiCall.callBack(token, this.status, this.response);
        }
    };


    xhttp.open(apiCall.method, url, true);
    if (apiCall.isSecured) {
        xhttp.setRequestHeader("Authorization", "Bearer " + token);
    } else if (apiCall.endpoint === "/logout") {
        apiCall.parameters = "token=" + token;
    }
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

    if (isEmpty(apiCall.parameters)) {
        // console.info("Call does not contain parameters:");
        xhttp.send();
    } else {
        // console.info("Sending parameter(s):", apiCall.parameters);
        xhttp.send(apiCall.parameters);
    }
}

const fillTokenArray = function () {
    for (let i = 1; i <= userArray.length; i++) {
        const j = i - 1;
        getUserToken(userArray[j], userPw[j], function (token) {
            tokens.push(token);
            userTokenMap.set(token, userArray[j]);
            console.info(userArray[j], token);
        });
    }
}

const parallelizationTestCase = function (conf) {
    document.getElementById("showUser").innerText = ``;
    document.getElementById("showToken").innerHTML = ``;
    console.info("Start test with " + tokens.length + " tokens...:");
    for (let i = 1; i <= userArray.length; i++) {
        apiCall1.parameters = "urlstring=http%3A%2F%2Fwww.blabla"
            + create_UUID() + ".com&shortcut=" + shortcutName(6)
            + "&summary=testSummary&keywords=testKeyword=&author=test&subject=&image1=&image2=&image3=&articleabstract=&taglist=testTag&icon=&idcategory=1&description=testdescription\"";
        const j = i - 1;
        if (Array.isArray(conf)) {
            for (let idx = 0; idx < conf.length; idx++)
                callApiWithToken(j, conf[idx]);
        } else callApiWithToken(j, conf);
    }
}


const test1 = function () {
    const viewerDiv = document.getElementById("viewResults");
    viewerDiv.innerHTML = ``;
    parallelizationTestCase(apiCall1);
}

const test2 = function () {
    const viewerDiv = document.getElementById("viewResults");
    viewerDiv.innerHTML = ``;
    parallelizationTestCase(apiCall2);
}

const test3 = function () {
    const viewerDiv = document.getElementById("viewResults");
    viewerDiv.innerHTML = ``;
    const calls = [];
    calls.push(apiCall1);
    calls.push(apiCall2);
    parallelizationTestCase(calls);
}
const testLogout = function () {
    parallelizationTestCase(apiLogout);
    document.getElementById("showUser").innerHTML = '';
    document.getElementById("showToken").innerHTML = '';
    document.getElementById("viewResults").innerHTML = '';
}

const showTokens = function () {
    const showToken = document.getElementById("showToken");
    const showUser = document.getElementById("showUser");
    showUser.innerHTML = '';
    showToken.innerHTML = '';
    console.info(tokens.length + " Tokens are set.");
    for (let i = 0; i < tokens.length; i++) {
        showUser.innerHTML = `<h3>${i}</h3>`;
        showToken.innerHTML += `<br><b>${userArray[i]} ==> ${tokens[i]}</b>`;
    }
    showUser.innerHTML = `<h3>${tokens.length} tokens of ${userArray.length} are set</h3>`;
}
