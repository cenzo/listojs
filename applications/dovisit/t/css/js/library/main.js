"use strict";

console.log(" 1 --- Customer ID before call of main.js: " + customerID);
var customerIDChanged = false;
//let role;
let sessionToken, currentUserID;
if (customerID && applicationID) currentUserID = sessionStorage.getItem(`listo_userID_${customerID}_${applicationID}`);

let bearerCookieName, bearerCookie, release, RELEASE = "1.2.1", backendDomain = `dovis.it`, module = `main.js`;
let labelAlertObjectID = "messageLabelId";

function getAPIServerPath() {

    console.log("selectedServer=" + selectedServer);
    return selectedServer;

}

/*function getUserRole() {
    const urlRoleID = parseInt(getUrlVars()['role'], 10);
    const notNeg = (urlRoleID >= 0);
    //console.log("urlRoleID=" + urlRoleID + " isNaN:" + isNaN(urlRoleID) + ", is >=0" + notNeg);
    if (!isNaN(urlRoleID) && urlRoleID >= 0) {
        sessionStorage.setItem("theRoleID", "" + urlRoleID);
        role = urlRoleID;
        console.log("role=" + role);
        return urlRoleID;
    } else {
        if (!isNaN(role) && role >= 0) {
            return role;
        } else {
            const storedRole = parseInt(sessionStorage.theRoleID, 10);
            if (!isNaN(storedRole) && storedRole >= 0) {
                console.log("storedRole " + storedRole + " retrieved from session");
                role = storedRole;
                return role;
            } else {
                console.log("No user-role is set!");
            }
        }
    }
}

role = getUserRole();*/


/*function getCustomerId() {
    if (!isNaN(customerID) && customerID > 0) {
        return customerID;
    } else {
        const urlCustomerID = parseInt(getUrlVars()['cust'], 10);
        if (urlCustomerID && !isNaN(urlCustomerID) && urlCustomerID > 0) {
            sessionStorage.setItem("theCustomerID", "" + urlCustomerID);
            return urlCustomerID;
        } else {
            const storedCustomer = parseInt(sessionStorage.theCustomerID, 10);
            if (!isNaN(storedCustomer) && storedCustomer > 0) {
                console.log("customerID " + storedCustomer + " retrieved from session");
                customerID = storedCustomer;
                return customerID;
            } else {
                console.error("No customerID is set!");
            }
        }
    }
}

function setCustomerId() {
    let oldCustomerID;
    if (!customerID) {
        oldCustomerID = null;
    } else {
        oldCustomerID = customerID;
    }
    const cid = getCustomerId();
    if (!isNaN(cid)) {
        customerID = cid;
        console.log("customerID has been set to: " + cid);
    } else {
        console.warn("customerID is NaN");
    }
    if (cid && oldCustomerID != customerID) {
        customerIDChanged = true;
    }
};*/


const getCookie = function (cname) {
    debug2("main.js", "get cookie", release, [cname]);
    const name = cname + "=";
    const decodedCookie = decodeURIComponent(document.cookie);
    //debug("cookie:", release, decodedCookie);
    const ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};


//setCustomerId();

//console.log(" 2 --- Customer ID after call of main.js: " + customerID);

//role = getUserRole();

function debug(message, loggedRelease, param1, param2, param3, param4, param5) {
    if (loggedRelease === RELEASE) {
        console.log("Module last loaded: " + module + ", logged release-code:" + loggedRelease + "/ current release:" + RELEASE);
        console.log(message + "\n--------------");
        if (param1)
            console.log(param1.toString() + ": " + JSON.stringify(param1)
                + "\n--------------");
        if (param2)
            console.log(param2.valueOf() + ": " + JSON.stringify(param2)
                + "\n--------------");
        if (param3)
            console.log(param3.isPrototypeOf(String) + ": " + JSON.stringify(param3)
                + "\n--------------");
        if (param4)
            console.log(param4 + ": " + JSON.stringify(param4)
                + "\n--------------");
        if (param5)
            console.log(typeof param5 + " Debug-value5: " + JSON.stringify(param5)
                + "\n--------------");
    }

}

function debug2(module, message, loggedRelease, loggedValues) {
    if (loggedRelease === RELEASE) {
        console.log("::::: --- Debug module '" + module + "'. Message: " + message + " ---------- :::::");
        for (let m = 0; m < loggedValues.length; m++) {
            console.log(":::\tLogged value " + m + ": " + "(" + typeof loggedValues[m] + ") " + JSON.stringify(loggedValues[m]));
        }
        console.log("::::: ------------- :::::");
    }
}

function getThemeName(themeID) {
//TODO
}

function showRelease(currmod) {
    if (currmod) debug("\tCurrent module:" + currmod + "\tLast loaded module: " + module + "\tlogged release: " + release + "\tGlobal RELEASE: " + RELEASE, "" + release);
    else debug("\tLast loaded module: " + module + "\tlogged release: " + release + "\tGlobal RELEASE: " + RELEASE, "" + release);
}

const showVersion = function () {
    document.getElementById("version").innerText = `Version: ${backendVersion}(backend)/${frontendVersion}(frontend)`;
}


function isListoNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

const knownMethods = ["post", "put", "get", "patch", "delete"];

function apiCall(apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method, encoding) {
    console.info("*********apiCall '", apiID, "' with token ", sessionToken);
    if (currentUserID) sessionToken = sessionStorage.getItem(`listo_${customerID}_${applicationID}_${currentUserID}`);
    if (!knownMethods.includes(method)) console.error("UNKNOWN HTTP-method '" + method + "'. Check lower/uppercase");
    /*if (!bearerCookie || bearerCookie === -1) {
        //const key = "listo_" + customerID + "_" + applicationID + "_" + role;
        debug2("main.js", "calling api with new key", release, [apiUrl, apiParams, key]);
        bearerCookie = sessionStorage.getItem(key);
        //console.log("Calling api " + apiID + "and tried to retrieve bearerCookie with key " + key + ", bearerCookie=" + bearerCookie);
        //console.log(JSON.stringify(sessionStorage));
        bearerCookie = getCookie(key);
        //console.log("Auth-cookie read as: " + bearerCookie + " with key " + key);
        //debug2("main.js", "get auth token from session storage", release, [key, customerID, applicationID, bearerCookie, sessionStorage]);
    }*/
    if (!customerIDChanged && selectedServer) {
        // selectedServer is set, and also  stored in sessionStorage
        console.info("main.js", "calling api: USECASE A", [apiUrl, apiParams, sessionToken]);
        doApiCall(selectedServer, apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method, encoding);
    } else if (!customerIDChanged && sessionStorage.getItem("selectedServer")) {
        // selectedServer is not set, but it was stored in sessionStorage
        console.info("main.js", "calling api: USECASE B", [apiUrl, apiParams, sessionToken]);
        doApiCall(sessionStorage.getItem("selectedServer"), apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method, encoding);
        //console.info("\n\n\t:::Selected server from session in MAIN.js: " + sessionStorage.getItem("selectedServer"));
    } else {
        console.info("main.js", "calling api: USECASE C", [apiUrl, apiParams, sessionToken]);
        // selectedServer is not set, and not stored in sessionStorage
        // or, so far, no customerID has been set
        // or the customerID has just been set to a new value
        const cid = getCustomerId();
        const app = applicationID;
        $.ajax({
            url: `https://backend.${backendDomain}${backendVersion}server/${cid}/${app}`,
            type: "get",
            data: {}
        }).done(
            function (data) {
                //debug2("main.js", "calling api: USECASE C", release, [apiUrl, apiParams, bearerCookie, data]);
                const newSelectedServer = `https://backend${data.nodeid}.${backendDomain}${backendVersion}`;
                console.log("\n\n\t:::NEW selected server in MAIN.js during call '" + apiUrl + "': " + newSelectedServer + "(node " + data.nodeid + ")");
                selectedServer = newSelectedServer;
                sessionStorage.setItem("selectedServer", newSelectedServer);
                doApiCall(newSelectedServer, apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method, encoding);
                customerIDChanged = false;
            }).fail(
            function (error) {
                const err = readErrors(error);
                console.error(`HTTP-ERROR in method ${apiUrl}, status=${error.status}`,
                    release, err, apiParams);
                httpErrorCallBack(err);
            });
    }
}

const displayHTTPErrorInDiv = function (status) {
    $(`#${errordiv}`).html(`<h4>HTTP ${status} </h4>`);
    $(`#${errordiv}`).attr("style", "visibility:visible;");
    $(`#${errordiv}`).show();
};

const hideLoadIcon = function (icon) {
    if (icon) icon.style.visibility = "hidden";
}
const htmlEncode = function (rawStr) {
    return rawStr.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
        return '&#' + i.charCodeAt(0) + ';';
    });
};
const htmlDecode = function (input) {
    let doc = new DOMParser().parseFromString(input, "text/html");
    return doc.documentElement.textContent;
}

function doApiCall(server, apiID, apiUrl, apiParams, callBack, httpErrorCallBack, method, encoding) {


    let url;
    url = "" + server + customerID + apiUrl + "?LOC=" + getCurrentLanguage();
    const _loadIcon = document.getElementById("loadingIcon");
    if (_loadIcon) _loadIcon.style.visibility = "visible";
    //debug(method + "-APICALL: ID=" + apiID, release);
    //debug("         URL=" + url, release);
    //debug("         token=" + bearerCookie, release);
    //debug("dataObject before:", release, apiParams);
    for (let key in apiParams) {
        let encStr = apiParams[key];
        if (!encoding || encoding === "html" || encoding === "HTML") {
            if (encStr && !isListoNumeric(encStr)) {
                try {
                    let str = decodeURIComponent(htmlDecode(encStr));
                    str = str.replace(/\\/g, "&#92;").replace(/\//g, "&#47;").replace(/'/g, "&#39;").replace(/</g, "&#10094;").replace(/>/g, "&#10095;").replace(/"/g, "&#34;")
                        .replace(/\u276E/g, "&#10094;").replace(/\u276F/g, "&#10095;");
                    apiParams[key] = str;
                } catch (err) {
                    apiParams[key] = "n.n.";
                    //  apiParams[key] = encStr.replace(/\\/g, "&#92;").replace(/\//g, "&#47;").replace(/'/g, "&#39;").replace(/</g, "&#10094;").replace(/>/g, "&#10095;").replace(/"/g, "&#34;")
                    //      .replace(/\u276E/g, "&#10094;").replace(/\u276F/g, "&#10095;");
                }
            }
        } else if (encoding === "url" || encoding === "URL") {
            const encodedHtml =  encodeURIComponent(encStr);
            console.info("encoded String:", encodedHtml);
            apiParams[key] = encodedHtml;
        } else if (!isListoNumeric(apiParams[key]) && encoding === "HTMLENCODE") {
            const encodedHtml = htmlEncode(encStr)
            console.info("htmlEncoded raw String:", encodedHtml);
            apiParams[key] = encodedHtml;
        }
    }


    if (method === "undefined") {
        console.warn("api-call '" + apiID + "' with empty  HTTP-method");
        $.get(url, {}).done(
            function (data) {
                if (data._rc > 0)
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                callBack(data);
                hideLoadIcon(_loadIcon);
            }).fail(
            function (error) {
                const err = readErrors(error);
                displayHTTPErrorInDiv(error.status);
                debug("HTTP-ERROR in " + method + "-API-CALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
                hideLoadIcon(_loadIcon);
            });
    } else if (method === "get") {
        $.ajax({
            url: url,
            type: method,
            data: apiParams
        }).done(
            function (data) {
                if (data._rc && data._rc > 0) {
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                    httpErrorCallBack(readErrors(data));
                    hideLoadIcon(_loadIcon);
                } else {
                    callBack(data);
                    hideLoadIcon(_loadIcon);
                }
            }).fail(
            function (error) {
                const err = readErrors(error);
                displayHTTPErrorInDiv(error.status);
                debug("HTTP-ERROR in " + method + "-API-CALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
                hideLoadIcon(_loadIcon);
            });
    } else if (method === "post" || method === "put" || method === "patch") {

        $.ajax({
            url: url,
            type: method,
            data: apiParams,
            headers: {
                "Authorization": "Bearer " + sessionToken,
                "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
            }
        }).done(
            function (data) {
                if (data._rc && data._rc > 0) {
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                    httpErrorCallBack(readErrors(data));
                    hideLoadIcon(_loadIcon);
                } else {
                    callBack(data);
                    hideLoadIcon(_loadIcon);
                }
            }).fail(
            function (error) {
                const err = readErrors(error);
                displayHTTPErrorInDiv(error.status);
                debug("HTTP-ERROR in " + method + "-API-CALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
                hideLoadIcon(_loadIcon);
            });
    } else if (method === "delete") {
        $.ajax({
            url: url,
            type: method
        }).done(
            function (data) {
                if (data._rc && data._rc > 0) {
                    debug("ERROR IN " + method + "-API-CALL, rc=" + data._rc, release, data,
                        apiParams);
                    httpErrorCallBack(readErrors(data));
                    hideLoadIcon(_loadIcon);
                } else {
                    callBack(data);
                    hideLoadIcon(_loadIcon);
                }
            }).fail(
            function (error) {
                const err = readErrors(error);
                displayHTTPErrorInDiv(error.status);
                debug("HTTP-ERROR in " + method + "-APIC-ALL, status=" + error.status,
                    release, err, apiParams);
                httpErrorCallBack(err);
                hideLoadIcon(_loadIcon);
            });
    }
}


const readErrors = function (errorData) {
    let err = {};
    if (errorData._rc) err._rc = errorData._rc;
    if (errorData._rcA) err._rcA = errorData._rcA;
    if (errorData.Exception) err.Exception = errorData.Exception;
    if (errorData.status) err.status = errorData.status;
    if (errorData.responseText) err.responseText = errorData.responseText;
    if (errorData.readyState) err.readyState = errorData.readyState;
    err.ErrorData = JSON.stringify(errorData);
    return err;
}


function upload(customerIdentifier, form, callBack, httpErrorCallBack) {
    const formData = document.querySelector('#' + form);
    const data = new FormData(formData);
    const upUrl = '' + getAPIServerPath() + customerIdentifier + '/1/upload/';

    debug2('main.js', 'Upload file: ' + upUrl, release, [""]);
    $.ajax({
        url: upUrl,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        timeout: 600000,
        headers: {
            'Authorization': 'Bearer ' + bearerCookie
        }
    }).done(function (data) {
        if (data._rc && data._rc > 0) {
            debug("ERROR IN UPLOAD-API-CALL, rc=" + data._rc, release, data._rc);
            httpErrorCallBack(readErrors(data));
        } else {
            debug('Done: ', release, data._rc);
            callBack(data);
        }
    }).fail(function (error) {
        const err = readErrors(error);
        debug('ERROR IN upload-function, status=' + error.status, "" + release, error);
        httpErrorCallBack(err);
    });
}


function showHttpErrorMessage(div, error) {
    let errorID;
    if (error.status) {
        errorID = error.status;
    } else {
        errorID = 499;
    }
    try {
        let msg = "";
        for (let key in error) {
            msg += key + ": " + error[key] + "\n";
        }
        let displayMsg = getTextById(errorID);
        if (error._rc) {
            displayMsg += `<br>rc: ${error._rc}`;
        }
        if (error._rcA) {
            displayMsg += `<br>application-rc: ${error._rcA}`;
        }
        const ta = `<textarea id="errorTextArea" rows="5" cols="50" readonly style="display:none;">${msg}</textarea>`;
        $('#' + div).html('<b><small>' + displayMsg + '</small></b><br><br><br><br>' + ta);
    } catch (err) {
        debug("Error in error-handling:", release + "", err.message, error);
        const ta = `<textarea id="errorTextArea" rows="5" cols="50" readonly style="display:none;">${JSON.stringify(error)}</textarea>`;
        $('#' + div).html(`<b><small> Unknown ERROR</small></b><br><br><br><small>${JSON.stringify(err)}</small><br><br>${ta}`);
        //if (error.status && error.status > 0) $('#' + div).html('<b>ERROR ' + error.status + ' ' + getTextById(error.status) + '</b>');
        //else if (error.status === 0) $('#' + div).html('<b>  ERROR (status=0: network or other connection problem)</b>');
        //else $('#' + div).html('<b> Unknown ERROR</b>');
    }
    //debug("Error is:", ""+release, error, error.message);
}

$(document).on('click', '.show-ErrorTextArea', function () {
    let ta = document.getElementById("errorTextArea");
    ta.hidden = false;
    ta.setAttribute("style", "background-color: lightgray;overflow-x: none;")
});


function getIndex(data, variableName) {
    for (let vindex = 0; vindex < data._VariableLabels.length; vindex++) {
        if (data._VariableLabels[vindex] === variableName)
            return vindex;
    }
}

function get(data, rowIndex, variableName) {
    const idx = getIndex(data, variableName);
    return data.rows[rowIndex].s[idx];
}


const listoHTML = {
    IDENTIFIER: 'self',
    jsonRMLData: null,
    columnIndices: null,
    header: null,
    actions: null,
    dataColumnNames: null,
    dataColumnIndices: null,
    tableStyleOptions: null,
    checkCallBack: null,
    checkActions: function (idx, element) {
        if (this.actions[idx].length !== 2) {
            console.error(`ERROR[CRHTMLTABLE9']: at index ${idx} for element ${element}:  array actions must have 2 values: name and label',
                ${this.actions}`);
            element
                .html(
                    '<b>ERROR [CRHTMLTABLE9'
                    + ']: in caller object "'
                    + this.IDENTIFIER
                    + '" of "createHTMLTable": array actions must have 2 values: name and label.</b>');
            return true;
        } else if (this.checkCallBack != null) return this.checkCallBack(idx, element);
        else
            return false;
    },
    fillActions: function (row, rowData, ca, dataNameCount) {
    },
    rowFormat: function (rowIndex, sDataArray) {
    },
    cellFormat: function (columnIndex, rowIndex, TDopen, data, TDclose) {
    },
    createTable:
        function (targetDiv, useDataTablePlugin, actionPosition) {
            const targetElement = document.getElementById(targetDiv);
            //targetElement.innerHTML = TableStyle.loadingIcon;
            targetElement.innerHTML = "<h4>Loading ...</h4>";
            let htmlError = ``;
            let localUrl;
            'use strict';
            let error = 0;
            if (this.jsonRMLData == null) {
                error = 5;
                htmlError =
                    `<b>ERROR [CRHTMLTABLE ${error}]: in calling object '  ${this.IDENTIFIER}
                     ': missing jsonRMLData.</b>`;
            } else if (this.columnIndices == null || this.columnIndices.length === 0) {
                error = 6;
                htmlError =
                    `<b>ERROR [CRHTMLTABLE' ${error}]: in calling object '
                     ${this.IDENTIFIER}': bad column-Indices (null or zero-length): '${this.columnIndices}'</b>`;
            } else if (useDataTablePlugin && this.columnIndices.length !== this.header.length) {
                error = 7;
                htmlError =
                    `<b>ERROR [CRHTMLTABLE${error}]: in calling object 
                    '${this.IDENTIFIER}': header and column-Indices do not have same length: header=[${this.header}] columnIndices=[${this.columnIndices}]</b>`;
            }
            let rowDiv = `${TableStyle.pre}<table id="${this.IDENTIFIER}" class="${TableStyle.classes}">`
            rowDiv += TableStyle.tableHeadOpen;
            for (let h = 0; h < this.header.length; h++) {
                if (this.header[h]) rowDiv += `${TableStyle.thOpen}${this.header[h]}${TableStyle.thClose}`;
            }
            rowDiv += TableStyle.tableHeadClose;
            const rowCount = this.jsonRMLData.count;

            if (this.dataColumnNames.length !== this.dataColumnIndices.length) {
                error = 1;
                htmlError =
                    `<b>ERROR [CRHTMLTABLE${error}]: in calling object 'this.IDENTIFIER'  of "createHTMLTable": length of the "data"-column names and indices does not match: 
                     "data"-columnNames=[${this.dataColumnNames}] "data"-columnIndices=[${this.dataColumnIndices}]</b>`;
            }
            // else if (this.actions.length !== this.dataColumnNames.length) {
            //     error = 2;
            //     htmlError = `<b>ERROR [CRHTMLTABLE${error}]: in calling object '${this.IDENTIFIER}'  of "createHTMLTable": length of arrays for actions and "data"-columnNames does not match:
            //          actions=[${this.actions}] "data"columnNames=['${this.dataColumnNames}]</b>`;
            // }
            for (let i = 0; i < rowCount; i++) {
                if (error > 0)
                    break;
                const rowData = this.jsonRMLData.rows[i];
                if (rowData !== undefined) {
                    rowDiv += TableStyle.trOpen;
                    let theRow = [];
                    if (this.rowFormat) {
                        theRow = this.rowFormat(i, rowData.s);
                    } else {
                        theRow = rowData.s;
                    }
                    if (actionPosition && actionPosition === "left") {
                        for (let ca = 0; ca < this.actions.length; ca++) {
                            rowDiv += TableStyle.tdOpen;
                            if (this.checkActions && this.checkActions(ca, targetElement, function (ca, targetElement) {
                            }) === true) {
                                error = 3;
                                throw new Error("invalid action at position 'left' for index " + ca);
                                break;
                            }
                            if (this.actions[ca][0]) rowDiv += `${TableStyle.aOpen} class="${this.actions[ca][0]}"`;
                            const caDataNameCount = this.dataColumnNames[ca].length;
                            const caDataInputCount = this.dataColumnIndices[ca].length;
                            if (caDataNameCount !== caDataInputCount) {
                                error = 4;
                                htmlError = `<b>ERROR [CRHTMLTABLE${error}]: in caller object '${this.IDENTIFIER}' of 
                                "createHTMLTable": wrong length for name and "data" parameters in action ${ca}:
                                <br>"Data"-name  is '${this.dataColumnNames[ca]}'
                                <br>"Data"-index is '${this.dataColumnIndices[ca]}'
                                 </b>`;
                                break;
                            }
                            rowDiv = this.fillActions(rowDiv, rowData, ca, caDataNameCount);
                            console.log("rowDiv left", rowDiv)
                        }
                    }
                    for (let c = 0; c < this.columnIndices.length; c++) {
                        if (theRow) {
                            const theCell = theRow[this.columnIndices[c]];
                            if (this.cellFormat) {
                                const theCellFormat = this.cellFormat(c, i, TableStyle.tdOpen, theCell,
                                    TableStyle.tdClose);
                                if (theCellFormat) rowDiv += theCellFormat;
                            } else if (theCell) {
                                rowDiv += TableStyle.tdOpen + theCell + TableStyle.tdClose;
                            }
                        }
                    }
                    if (actionPosition && actionPosition === "right") {
                        for (let ca = 0; ca < this.actions.length; ca++) {
                            rowDiv += TableStyle.tdOpen;
                            if (this.checkActions(ca, targetElement) === true) {
                                error = 3;
                                throw new Error("invalid action at position 'right' for index " + ca);
                                break;
                            }
                            rowDiv += `${TableStyle.aOpen} class="${this.actions[ca][0]}"`;
                            if (!Array.isArray(this.dataColumnNames[ca]) || !Array.isArray(this.dataColumnIndices[ca])) {
                                error = 4.1;
                                htmlError = `<b>ERROR [CRHTMLTABLE${error}]: in caller object '${this.IDENTIFIER}' of 
                                "createHTMLTable": "data"-columnNames and "data"-columnIndices in action ${ca} must be arrays:
                                <br>"Data"-name  is '${this.dataColumnNames[ca]}' isArray:${Array.isArray(this.dataColumnNames[ca])}
                                <br>"Data"-index is '${this.dataColumnIndices[ca]}' isArray:${Array.isArray(this.dataColumnNames[ca])}
                                 </b>`;
                                break;
                            }
                            const caDataNameCount = this.dataColumnNames[ca].length;
                            const caDataInputCount = this.dataColumnIndices[ca].length;
                            if (caDataNameCount !== caDataInputCount) {
                                error = 4;
                                htmlError = `<b>ERROR [CRHTMLTABLE${error}]: in caller object '${this.IDENTIFIER}' of 
                                "createHTMLTable": wrong length for "name" and "data" parameters in action ${ca}:
                                <br>"Data"-name  is '${this.dataColumnNames[ca]}' and has length ${this.dataColumnNames[ca].length}
                                <br>"Data"-index is '${this.dataColumnIndices[ca]}' and has length ${this.dataColumnIndices[ca].length}
                                 </b>`;
                                break;
                            }
                            rowDiv = this.fillActions(rowDiv, rowData, ca, caDataNameCount);
                            console.info("rowDiv right", rowDiv)
                        }
                    }
                    rowDiv += TableStyle.trClose;
                }
            }
            rowDiv += TableStyle.bottom;
            if (error === 0) {
                targetElement.innerHTML = rowDiv;
            } else {
                targetElement.innerHTML = htmlError;
                console.error("Error " + error, htmlError);
            }
            if (!useDataTablePlugin) {
                console.info("Using custom table without dataTable-plugin for " + this.IDENTIFIER);
                return;
            } else {
                if (locale === 'en') {
                    localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json';
                } else if (locale === 'it') {
                    localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Italian.json';
                } else if (locale === 'de') {
                    localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json';
                } else if (locale === 'es') {
                    localUrl = '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json';
                } else if (locale === 'fr') {
                    localUrl = 'cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json';
                }
                if (this.tableStyleOptions === null) {
                    console.info("Applying default table-style-options to table " + this.IDENTIFIER);
                    $("#" + this.IDENTIFIER).dataTable({
                        retrieve: true,
                        paging: false,
                        scrollY: '200px',
                        scrollCollapse: true,
                        fixedColumns: false,
                        fixedHeader: true,
                        'pageLength': 15,
                        'language': {
                            'url': localUrl,
                        }
                    });
                } else {
                    this.tableStyleOptions.language = {'url': localUrl,};
                    console.info("Applying custom table-style-options to table " + this.IDENTIFIER);
                    //console.info("this.tableStyleOptions", this.tableStyleOptions);
                    $("#" + this.IDENTIFIER).dataTable(this.tableStyleOptions);
                }
            }
        },
    buildForm:
        function (div, width, array, id) {
            const pre = FormStyle.pre1 + width + FormStyle.pre2 + id + FormStyle.pre3;
            const suf = FormStyle.suf;
            $(div).html('');
            let rowDiv = pre;
            for (let i = 0; i < array.length; i++) {
                rowDiv += array[i];
            }
            rowDiv += suf;
            $(div).html(rowDiv);
        },
    createForm:
        function (div, width, array, id) {
            const pre = FormStyle.pre1 + width + FormStyle.pre2 + id + FormStyle.pre3;
            const suf = FormStyle.suf;
            $(div).html('');
            let rowDiv = pre;
            for (let i = 0; i < array.length; i++) {
                const funcLen = array[i].length;
                const objType = array[i][0];
                switch (funcLen) {
                    case (2):
                        rowDiv += objType(array[i][1]);
                        break;
                    case (3):
                        rowDiv += objType(array[i][1], array[i][2]);
                        break;
                    case (4):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3]);
                        break;
                    case (5):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4]);
                        break;
                    case (6):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5]);
                        break;
                    case (7):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6]);
                        break;
                    case (8):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8]);
                        break;
                    case (9):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8], array[i][9]);
                        break;
                    case (10):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8], array[i][9], array[i][10]);
                        break;
                    case (11):
                        rowDiv += objType(array[i][1], array[i][2], array[i][3], array[i][4],
                            array[i][5], array[i][6], array[i][7], array[i][8], array[i][9], array[i][10], array[i][11]);
                        break;
                    default:
                }
            }
            rowDiv += suf;
            $(div).html(rowDiv);
        },
    createTour:
        function (targetDiv, stepshtml, effect, orientation) {
            $(targetDiv).html(stepshtml);
            $(targetDiv).steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: effect, /*slideLeft*/
                autoFocus: true,
                stepsOrientation: orientation /*"vertical"*/
            });
        }

};

// Email Validation Function
function validateEmail(email) {
    const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

// FORM Validation
$(document).ready(function () {
    $('form').validate();
});


if (locale === "en") {

} else if (locale === "it") {
    $.extend($.validator.messages, {
        required: "Campo obbligatorio",
        remote: "Controlla questo campo",
        email: "Inserisci un indirizzo email valido",
        url: "Inserisci un indirizzo web valido",
        date: "Inserisci una data valida",
        dateISO: "Inserisci una data valida (ISO)",
        number: "Inserisci un numero valido",
        digits: "Inserisci solo numeri",
        creditcard: "Inserisci un numero di carta di credito valido",
        equalTo: "Il valore non corrisponde",
        extension: "Inserisci un valore con un&apos;estensione valida",
        maxlength: $.validator.format("Non inserire pi&ugrave; di {0} caratteri"),
        minlength: $.validator.format("Inserisci almeno {0} caratteri"),
        rangelength: $.validator.format("Inserisci un valore compreso tra {0} e {1} caratteri"),
        range: $.validator.format("Inserisci un valore compreso tra {0} e {1}"),
        max: $.validator.format("Inserisci un valore minore o uguale a {0}"),
        min: $.validator.format("Inserisci un valore maggiore o uguale a {0}"),
        nifES: "Inserisci un NIF valido",
        nieES: "Inserisci un NIE valido",
        cifES: "Inserisci un CIF valido",
        currency: "Inserisci una valuta valida"
    });
} else if (locale === "de") {
    $.extend($.validator.messages, {
        required: "Dieses Feld ist ein Pflichtfeld.",
        maxlength: $.validator.format("Geben Sie bitte maximal {0} Zeichen ein."),
        minlength: $.validator.format("Geben Sie bitte mindestens {0} Zeichen ein."),
        rangelength: $.validator.format("Geben Sie bitte mindestens {0} und maximal {1} Zeichen ein."),
        email: "Geben Sie bitte eine gültige E-Mail Adresse ein.",
        url: "Geben Sie bitte eine gültige URL ein.",
        date: "Bitte geben Sie ein gültiges Datum ein.",
        number: "Geben Sie bitte eine Nummer ein.",
        digits: "Geben Sie bitte nur Ziffern ein.",
        equalTo: "Bitte denselben Wert wiederholen.",
        range: $.validator.format("Geben Sie bitte einen Wert zwischen {0} und {1} ein."),
        max: $.validator.format("Geben Sie bitte einen Wert kleiner oder gleich {0} ein."),
        min: $.validator.format("Geben Sie bitte einen Wert größer oder gleich {0} ein."),
        creditcard: "Geben Sie bitte eine gültige Kreditkarten-Nummer ein."
    });
}

function makeid(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function getLabelAlertObjectId() {
    return labelAlertObjectID;
}

function setLabelAlertObject(labelDivId) {
    labelAlertObjectID = labelDivId;
};

function labelAlert(msg) {
    const labelId = getLabelAlertObjectId();
    document.getElementById(labelId).innerText = msg;
}

/*function logout() {
    const logoutPath = getAPIServerPath() + customerID + "/1/logout";
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send("token=" + bearerCookie);
    xhttp.onreadystatechange = function () {
        debug("logoutPath=" + logoutPath, 0);
        debug("token=" + bearerCookie, 0);
        if (this.readyState == 4) {
            sessionStorage.removeItem("selectedServer");
            window.open("access.html?cust=" + customerID, "_self");
            if (this.status >= 400) {
                debug("ERROR: logout-status: " + this.status, 0);
            }
        }
    };
}

$(document).on("click", ".logout", function () {
    logout();
});
*/
function requestNewListoranteInstance(newCustomerID, callBack, httpErrorCallBack) {
    const params = {"database": 1, "instance": newCustomerID, "replicatedInstances": 1, "environment": environment};
    return apiCall("global.listoranteRequest", "/0/customer/listorequest", params, callBack, httpErrorCallBack, "post");
}


function createNewListoranteInstance(newCustomerID, email, activationToken, callBack, httpErrorCallBack) {
    const params = {
        "database": 1,
        "instance": newCustomerID,
        "email": email,
        "activationToken": activationToken,
        "replicatedInstances": 1
    };
    return apiCall("global.listoranteCreate", "/0/customer/listocreate", params, callBack, httpErrorCallBack, "post");
}

function getStripeAccountID(callback) {
    if (stripe_customer_id) {
        callback(stripe_customer_id);
        return stripe_customer_id;
    }
    apiCall_listorante_public_settings("STRIPE_ACCOUNT_ID", function (data) {
        if (data.count > 0) {
            stripe_customer_id = data.rows[0].s[0];
            is_stripe_registered = true;
            callback(stripe_customer_id);
            return stripe_customer_id;
        }
    }, function (error) {
        showHttpErrorMessage("main-content", error);
    });
}

function getNode(cb, errCb) {
    $.ajax({
        url: getAPIServerPath() + "/server/nodeid",
        type: "post",
        data: {},
        headers: {
            "Authorization": "Bearer " + bearerCookie,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
    }).done(
        function (data) {
            cb(data);
        }).fail(
        function (error) {
            errCb(error);
        });
}

function stripePayment(application, productID, price, productName, productDescription, currency, quantity, callBack, httpErrorCallBack) {
    /*
    *
    *  @FormParam("productID") int productID,
			@FormParam("price") int price, @FormParam("product") String productName,
			@FormParam("productDescription") String productDescription, @FormParam("currency") String currency,
			@FormParam("quantity") int quantity
    **/

    const params = {
        "productID": productID,
        "price": price,
        "product": productName,
        "productDescription": productDescription,
        "currency": currency,
        "quantity": quantity
    };
    return apiCall("payment.stripe", "/" + application + "/stripe", params, callBack, httpErrorCallBack, "post");
}

function sendMail(application, sender, recipient, mailSubject, mailBody, senderKey, callBack, httpErrorCallBack) {
    debug2("main", "Sending Email", RELEASE, ["Sender:" + sender, "Recipient:" + recipient]);
    const params = {
        "sender": sender,
        "recipient": recipient,
        "subject": mailSubject,
        "body": mailBody,
        "senderKey": senderKey
    };
    return apiCall("sendMail", "/" + application + "/sendmail", params, callBack, httpErrorCallBack, "post");
}


function stripeConnectPayment(stripeAccountID, application, productID, price, productName, productDescription,
                              currency, quantity, customerEmail, paymentMethod, callBack, httpErrorCallBack) {

    const params = {
        "sellerStripeID": stripeAccountID,
        "productID": productID,
        "price": price,
        "product": productName,
        "productDescription": productDescription,
        "currency": currency,
        "quantity": quantity,
        "customerEmail": customerEmail,
        "paymentMethod": paymentMethod
    };
    return apiCall("payment.stripe", "/" + application + "/stripeconnect", params, callBack, httpErrorCallBack, "post");
}

function updateCredits(application, sessionid, callBack, httpErrorCallback) {
    const params = {};
    return apiCall("update.credits", "/" + application + "/updatecredits/" + sessionid, params, callBack, httpErrorCallback, "post");
}

function getCreditCharges(callBack, httpErrorCallback) {
    const params = {};
    return apiCall("credit.showcharges", "/" + applicationID + "/showcreditcharges/", params, callBack, httpErrorCallback, "post");
}

function getCustomerCredits(callback, errorcallback) {
    $.ajax({
        url: getAPIServerPath() + getCustomerId() + "/" + applicationID + "/showcredits",
        type: "post",
        data: {},
        headers: {
            "Authorization": "Bearer " + bearerCookie,
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
    }).done(
        function (data) {
            callback(data);
        }).fail(
        function (error) {
            errorcallback(error);
        });
}


function uuidv4() {
    const uid = ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
    return uid.replace("-", "s");
}

function createNewStripeAccount(stripeAuthCode, callBack, httpErrorCallback) {
    const params = {"stripe_auth_code": stripeAuthCode};
    return apiCall("createNewStripeAccount", "/" + applicationID + "/createstripeaccount", params, callBack, httpErrorCallback, "post");
}


const csv2JSON = function (csvFile, delimiter, lineSeparator, returnAsObject) {
    const lines = csvFile.split(lineSeparator);
    let result = [];
    // NOTE: If your columns contain commas in their values, you'll need
    // to deal with those before doing the next step
    // (you might convert them to &&& or something, then covert them back later)
    // jsfiddle showing the issue https://jsfiddle.net/
    let headers = lines[0].split(delimiter);

    for (let i = 1; i < lines.length; i++) {
        let obj = {};
        let currentLine = lines[i].split(delimiter);
        for (let j = 0; j < headers.length; j++) {
            obj[headers[j]] = currentLine[j];
        }
        result.push(obj);
    }

    if (returnAsObject === true) return result; //JavaScript object
    else return JSON.stringify(result); //JSON
}

/**
 * Takes a raw CSV string and converts it to a JavaScript object.
 * @param {string} text The raw CSV string.
 * @param {string[]} headers An optional array of headers to use. If none are
 * given, they are pulled from the first line of `text`.
 * @param {string} quoteChar A character to use as the encapsulating character.
 * @param {string} delimiter A character to use between columns.
 * @returns {object[]} An array of JavaScript objects containing headers as keys
 * and row entries as values.
 */
const csvToJson = function (text, headers, quoteChar = '"', delimiter = ',') {
    const regex = new RegExp(`\\s*(${quoteChar})?(.*?)\\1\\s*(?:${delimiter}|$)`, 'gs');

    const match = line => [...line.matchAll(regex)]
        .map(m => m[2])  // we only want the second capture group
        .slice(0, -1);   // cut off blank match at the end

    const lines = text.split('\n');
    const heads = headers ?? match(lines.shift());

    return lines.map(line => {
        return match(line).reduce((acc, cur, i) => {
            // Attempt to parse as a number; replace blank matches with `null`
            const val = cur.length <= 0 ? null : Number(cur) || cur;
            const key = heads[i] ?? `column${i}`;
            return {...acc, [key]: val};
        }, {});
    });
}

const readTextFile = function (file, element, fill = "innerText", transformCallBack) {
    const rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            if (rawFile.status === 200 || rawFile.status == 0) {
                let allText = rawFile.responseText;
                if (fill === "innerText") element.innerText = allText;
                else element.innerHTML = transformCallBack(allText);
            }
        }
    }
    rawFile.send(null);
    fetch(file)
        .then(response => response.text())
        .then(text => console.info(text))
}

const previewFile = function (element, file, transformerCallBack) {
    element.innerHTML = TableStyle.loadingIcon;
    const reader = new FileReader();
    reader.addEventListener("load", () => {
        // this will then display a text file
        if (transformerCallBack) {
            element.innerHTML = transformerCallBack(reader.result);
        } else {
            element.innerHTML = ``;
            element.innerText = reader.result;
        }
    }, false);

    if (file) {
        console.info("file", file);
        reader.readAsText(file);
    }
}


const getUniqueRMLObjects = function (rmlData, keyIndex) {
    let seen = {};
    let out = [];
    let j = 0;
    const arr = rmlData.rows;

    if (!Array.isArray(arr)) {
        console.error("Not an rml-Data-object:", rmlData);
        throw  Error("Argument 1 of getUniqueRMLObjects is not an rml Data-Object");
    }
    for (let i = 0; i < arr.length; i++) {
        let item = arr[i].s[keyIndex];

        if (seen[item] !== 1) {
            seen[item] = 1;
            out[j++] = arr[i];
        }
    }
    return out;
}

const loadFile = function (filePath, div) {
    let result = null;
    const xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", filePath, false);
    xmlHttp.send();
    if (xmlHttp.status == 200) {
        result = xmlHttp.responseText;
    }
    document.getElementById(div).innerText = result;
}