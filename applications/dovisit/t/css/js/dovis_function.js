"use strict";

const MAX_TABLE_DATA_SIZE = 1000;
const SHORTCUT_LENGTH = 6;
let newItemAdded = false;
const isLoggedIn = function () {
    const dovisUser = sessionStorage.getItem("dovisUser");
    if (dovisUser) {
        return true;
    } else {
        return false;
    }
};

const isImage = function (element) {
    if (!element) {
        return false;
    }
    return /^https:(\/\/)(\S+)(\.)(gif|jpg|jpeg|tiff|png|ico|svg)(\?v\S+)*$/i.test(element);
};

const notEmpty = function (value) {
    return value && value != "undefined" && value != "null" && value.length > 0;
};

const isEmpty = function (value) {
    return !notEmpty(value);
};
const showLoading = function (boolean) {
    if (boolean === true) document.getElementById("loadingIcon").style.visibility = "visible";
    else document.getElementById("loadingIcon").style.visibility = "hidden";
}
const DELETED_ROW_FLAG = "deleted row";

const userUrlTableObject = {
    IDENTIFIER: "user-urls",
    jsonRMLData: null,
    header: [`<span class="badge btn-outline-primary bg-primary icon-calendar">&nbsp;<i class="fa fa-sort fa-2x" aria-hidden="true" style="font-size: x-small"></i></span>`,
        ``,
        ``,
        ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``,
    ],
    columnIndices: [18, 2, 9, 10, 1, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 0],
    actions: [],
    dataColumnIndices: [],
    dataColumnNames: [],
    tableStyleOptions: {
        scrollY: '800px',
        scrollCollapse: false,
        paging: false,
        fixedHeader: true,
        autoWidth: false,
        order: [[0, "desc"]],
        responsive: true,
        sDom: '<"top"flip>rt<"bottom"p><"clear">',
        columnDefs: [
            {targets: [0], width: "100%", css: "dovisColumn1"},
            /*{targets: [1], width: "98%"},*/
            {targets: '_all', width: "0%"},
            {targets: [0], visible: true},
            {targets: '_all', visible: false}
        ],
        pageLength: 25,
    },
    fillActions: function (row, rowData, ca, dataNameCount) {
        for (let caIdx = 0; caIdx < dataNameCount; caIdx++) {
            row += `data-${this.dataColumnNames[ca][caIdx]}="
                                ${rowData.s[this.dataColumnIndices[ca][caIdx]]}"`;
        }
        row += `${TableStyle.aAttributes}${this.actions[ca][1]}
                            ${TableStyle.aClose}`;
        row += TableStyle.tdClose;
        return row;
    },
    rowFormat: function (rowIndex, dataArray) {
        return dataArray;
    },
    cellFormat: function (colIndex, rowIndex, TDopen, cellData, TDclose) {
        const cat = parseInt(this.jsonRMLData.rows[rowIndex].s[16]);

        const decodedUrl = decodeURIComponent(this.jsonRMLData.rows[rowIndex].s[2]);
        let icon;
        let iconUrl = this.jsonRMLData.rows[rowIndex].s[12];
        if (iconUrl && isImage(iconUrl)) {
            icon = `<img src="${this.jsonRMLData.rows[rowIndex].s[12]}" class="dovisthumb">`
        } else icon = ``;
        const creationDatetime = this.jsonRMLData.rows[rowIndex].s[10];
        const idshortcut = this.jsonRMLData.rows[rowIndex].s[0];
        const shortcut = this.jsonRMLData.rows[rowIndex].s[9];
        let desc = ``;
        let tl = ``, kw = ``;

        let taglist = this.jsonRMLData.rows[rowIndex].s[11];
        let description = this.jsonRMLData.rows[rowIndex].s[4];
        let keywords = this.jsonRMLData.rows[rowIndex].s[3];

        let userHtml = this.jsonRMLData.rows[rowIndex].s[19];

        let userDesc = ``;
        //if (isEmpty(userDesc)) userDesc = ``;
        if (isEmpty(userHtml)) userHtml = ``;
        if (isEmpty(taglist)) taglist = ``;
        if (isEmpty(keywords)) keywords = ``;
        if (isEmpty(description) && isEmpty(userDesc)) description = ``;
        //else if (isEmpty(description) && notEmpty(userDesc)) description = userDesc;
        //else if (notEmpty(userDesc)) description += `<br>${userDesc}`
        if (notEmpty(description)) desc = `<span class="dovis_small float-start" id="desc${idshortcut}" >${description}&nbsp;</span><br>&nbsp;&nbsp;`;
        if (notEmpty(taglist))
            tl = `<span class="dovis_small float-start" id="taglist${idshortcut}"> <i class="fa fa-tags"></i>  [...${taglist}...]  &nbsp;</span><br>&nbsp;&nbsp;`;
        if (notEmpty(keywords))
            kw = ` <span class="dovis_small float-start" id ="keywordlist${idshortcut}">  <i class="fa fa-sticky-note"></i> [...${keywords}...] &nbsp; </span><br>&nbsp;&nbsp;`;
        const metadiv = `
                       ${desc} 
                       ${tl} 
                       ${kw}`;
        let linkInfo;
        let shareDescription = `${description} &nbsp; ${userDesc}`;
        let shareButtonVisibility = "visible";
        if (cat === 1) {
            linkInfo = `<i class="fa fa-unlink fa-lg" id="icon${this.jsonRMLData.rows[rowIndex].s[0]}"></i>`;
            shareButtonVisibility = "hidden";
        } else if (cat === 2) {
            linkInfo = `<i class="fa fa-link fa-lg" id="icon${this.jsonRMLData.rows[rowIndex].s[0]}"></i>`;
        } else if (cat > 2) {
            linkInfo = `<i class="fa fa-forward fa-lg" id="icon${this.jsonRMLData.rows[rowIndex].s[0]}"></i>`;
        }
        const timestamp = creationDatetime.toString().substring(0, 16);
        //const tmpAdImg = `testimonials-${Math.max(idshortcut % 4, 1)}`;

        if (colIndex === 0) {
            return `${TDopen}
                        <div id="shortcutrow${idshortcut}">
                           <div style="visibility:hidden;height:1px;">
                              ${timestamp}
                           </div>  
                            
                            <a title="https://dovis.it/${shortcut}"  class="dovislink float-start" href="${decodedUrl}" target="_blank">${icon}&nbsp;${decodedUrl}</a>                              
                           <br>&nbsp;
                              ${metadiv} 
                           <br>
                           <div class="float-start">
                              <span class="dbadge open-preview"  data-sc="${shortcut}">
                                 <i class="fa fa-external-link fa-sm"></i>
                              </span>
                              <span id="sc_properties_${idshortcut}" class="dbadge90 show_shortcut_properties" data-url="${decodedUrl}"
                                 data-idshortcut="${idshortcut}"
                                 data-linkcategory="${cat}"
                                 data-keywords="${keywords}"
                                 data-description="${description}&nbsp;${userDesc}"
                                 data-tags="${taglist}">
                                 <i class="fa fa-cog fa-sm">&nbsp;${linkInfo}&nbsp;</i>&nbsp;${shortcut.toUpperCase()}&nbsp;&nbsp;&nbsp;
                             </span>
                             <span id="share_${idshortcut}" class="dbadge show_share"
                                style="visibility:${shareButtonVisibility}" data-userHtml='${htmlEncode(userHtml)}'
                                data-shortcut="${shortcut}" data-urldescriptiontext="${shareDescription}" data-idshortcut="${idshortcut}"> 
                                <i class="fa fa-share-alt fa-sm" aria-hidden="true"></i>
                             </span> 
                             <small><small><br>${timestamp}</small></small>
                           </div>
                         
                           <div class="ad">
                           <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8975961160217250"
                                     crossorigin="anonymous">
                           </script>
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-format="fluid"
                                     data-ad-layout-key="-f8+6l-1t-cu+tp"
                                     data-ad-client="ca-pub-8975961160217250"
                                     data-ad-slot="7442951102">
                                </ins>
                                <script>
                                     (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                                
                           </div>
                          
                                <span class="delete-shortcut  right" id="delete_${idshortcut}" data-idsc="${idshortcut}"> 
                                   <i class="fa fa-trash fa-2x"></i>
                                </span> 
                         
                       </div>
                     
                      ${TDclose}`;
        } else return;
    }
};
const emailCustomersTableObject = {
    IDENTIFIER: "email-customers",
    jsonRMLData: null,
    header: [],
    columnIndices: [],
    actions: [],
    // dataColumnIndices: [[0, 1, 2, 3]],
    // dataColumnNames: [['idemail', 'email', 'firstname', 'lastname']],
    dataColumnIndices: [],
    dataColumnNames: [],
    tableStyleOptions: {
        scrollY: '200px',
        scrollCollapse: false,
        paging: false,
        fixedHeader: true,
        autoWidth: false,
        order: [[0, "desc"]],
        responsive: true,
        sDom: '<"top"flip>rt<"bottom"p><"clear">',
        columnDefs: [
            {targets: [0], visible: true},
            {targets: [1], visible: true},
            {targets: [2], visible: true},
        ],
        pageLength: 25,
    },
    checkActions: listoHTML.checkActions,
    fillActions: function (row, rowData, ca, dataNameCount) {
        return row;
    },
    rowFormat: function (rowIndex, dataArray) {
        return dataArray;
    },
    cellFormat: function (colIndex, rowIndex, TDopen, cellData, TDclose) {
        return `${TDopen}<b class="dovis_small">${cellData}</b>${TDclose}`;
    }
};
const checkUrl = new RegExp(/^(ftp|http|https):\/\/[^ "]+$/);

const blink = function (element, duration, repeat) {
    for (let r = 0; r < repeat; r++) {
        $(`#${element}`).fadeIn(duration);
        $(`#${element}`).fadeOut(duration);
    }
    $(`#${element}`).fadeIn(2 * duration);
};

const shortcutName = function (length) {
    let result = [];
    let characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
};

const createShortcutName = function (callBack, errorCallBack) {
    const sc = shortcutName(SHORTCUT_LENGTH);
    const scObject = {shortcut: sc};
    apiCall('', `/8/user/checkifshortcutexists`, scObject,
        function (dataArray) {
            const data = dataArray[0];
            if (data.count == 0)
                callBack(sc);
            else callBack(shortcutName(SHORTCUT_LENGTH));
        },
        function (err) {
            console.error("could not select last rowId for shortcut: " + JSON.parse(err));
            errorCallBack(err);
        }, "post");
};

const shorten = function (string, isString, len) {
    let shortenedString;
    if (len === undefined) len = 255;
    if (string && string.toString().length > len) {
        if (isString === undefined) {
            shortenedString = string.toString().substring(0, len - 1);
        } else if (isString === false) shortenedString = null;
    } else shortenedString = string;
    return shortenedString;
};

const allTags = function (obj) {
    const badTagArray = ["viewport"];
    let tags = ``;
    for (let tag in obj) {
        if (!badTagArray.includes(tag.toLowerCase())) {
            tags += `${obj[tag]} `;
        }
    }
    //remove all whitespaces, tabs, newline etc.:
    tags = tags.replace(/\s\s+/g, ' ');
    return tags;
};
const analyzeRequestedPage = function (idOfUrl, urlToAnalyze, callbackIcons, callbackMeta, errorCallBack) {
        const useridForBilling = sessionStorage.getItem("dovisUserID");
        const userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21";
        if (useridForBilling) {
            apiCall('', `/8/meta`, {
                userid: useridForBilling,
                url: urlToAnalyze,
                useragent: userAgent
            }, function (metaRead) {
                apiCall('', `/8/icons`, {
                    userid: useridForBilling,
                    url: urlToAnalyze,
                    useragent: userAgent
                }, function (iconsRead) {
                    let updates;
                    if (metaRead["meta-tags"]) {
                        updates = {
                            "idurl": idOfUrl,
                            "keywords": shorten(metaRead["meta-tags"].keywords),
                            "description": shorten(metaRead["meta-tags"].description, true),
                            "subject": shorten(metaRead["meta-tags"].subject),
                            "articleabstract": shorten(allTags(metaRead["meta-tags"]), true, 1023),
                            "summary": shorten(metaRead["meta-tags"].summary),
                            "author": shorten(metaRead["meta-tags"].author, true),
                            "image1": shorten(metaRead["meta-tags"].image1, false),
                            "image2": shorten(metaRead["meta-tags"].image2, false),
                            "image3": shorten(metaRead["meta-tags"].image3, false)
                        };
                    }
                    if (iconsRead.icons && Array.isArray(iconsRead.icons)) {
                        for (let i = 0; i < iconsRead.icons.length; i++)
                            if (isImage(iconsRead.icons[i].href)) {
                                updates.icon = iconsRead.icons[i].href;
                                break;
                            }
                    }
                    apiCall('', `/8/user/urledit`, updates, function (dataArray) {
                        const data = dataArray[0];
                        console.info("Data in urledit", data);
                        callbackIcons(iconsRead);
                        callbackMeta(metaRead);
                    }, function (errUrledit) {
                        console.error(errUrledit);
                    }, "post", "url");
                }, function (errIcons) {
                    console.error("could not read metadata");
                    errorCallBack(errIcons);
                }, "post", "url")
            }, function (errMeta) {
                console.error("could not read icons");
                errorCallBack(errMeta);
            }, "post", "url");
            return true;
        } else
            return false;
    }
;

const functionForProject_dovisto = function (objectId, dataObject, path, method, encoding) {
        document.getElementById("dovisactions").style.visibility = "visible";

        const createSaveShortcutDialog = function (urlID, urlString, shortcut) {
            let actionName;
            let linkData = ` data-shortcut="${shortcut}"`;
            if (urlID != null) {
                actionName = "click_createselected";
                linkData += ` data-idurl="${urlID}"`;
            } else {
                actionName = "click_createwoatt";
                linkData += ` data-urlstring="${urlString}"`;
            }
            let tagList = `<div class="text-md-center"><input type="text" placeholder="Tags" id="shortCut_taglist"/>
                    &nbsp;<span class="badge-outline badge-dark rounded" style ="padding: 0.65em;" id="shortCut_taglist_icon"><i class="fa fa-tags fa-lg"></i></span>`;
            let action = `${tagList}<p class="dovis" id="scurl_linkage">dovis.it/${shortcut}&nbsp;&nbsp;<i class="fa fa-arrow-right"></i>
                     &nbsp;${urlString}<br>${urlString.length}&nbsp;${TEXT_NUMBER_OF_CHARACTERS}</p>&nbsp;&nbsp;
                <a class="btn btn-primary ${actionName}" id="save_shortcut" ${linkData}
                data-idcategory="1">${TEXT_SAVE_THIS_SHORTURL}
                    <i class="fa fa-save"></i>&nbsp;&nbsp;<i class="fa fa-unlink"></i>&nbsp;&nbsp;</a></p>
                   <p><a class="btn btn-primary ${actionName}" id="publish_shortcut" ${linkData}
                     data-idcategory="2">${TEXT_PUBLISH_THIS_SHORTURL}&nbsp;&nbsp;
                  <i class="fa fa-globe"></i>&nbsp;&nbsp;<i class="fa fa-link"></i>
                   </a></p></a><h3 id="newshortcut_h3">dovis.it/${shortcut}</h3></div>`;
            return action;
        };


        if (!dataObject) {
            console.error("Cannot send apiCall with null dataobject.");
        } else if (!objectId) {
            console.error("Null object-id for apiCall.");
        } else {
            let eventCallBack;
            let errorCallBack;
            const callingObject = $(`#${objectId}`);
            switch (objectId) {
                case "createsubmit":
                    if (!$("#longurl_input")) {
                        blink("longurl_input", 500, 3);
                    } else {
                        if (!isLoggedIn()) {
                            blink("login", 100, 3);
                        } else
                            try {
                                const validationLabel = $("#validationlabel");
                                const objectInnerHtml = callingObject.html();
                                const userInput = $("#longurl_input").val();
                                const urlstring = userInput.trim();
                                validationLabel.html(``);
                                eventCallBack = function (dataArray) {
                                    const data = dataArray[0];
                                    callingObject.html(`<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>`);
                                    let urlId = null;
                                    const sessionData = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                                    if (sessionData && sessionData.count > MAX_TABLE_DATA_SIZE) {
                                        const dataSizeWarning = $("#datasize-warning");
                                        console.error("user has too much data, need to write new frontend for users which exceed " + MAX_TABLE_DATA_SIZE + " items");
                                        dataSizeWarning.show();
                                        dataSizeWarning.fadeIn(2500);
                                        alert(TEXT_MAXIMUM_DATASIZE_EXCEEDED);
                                        callingObject.html(objectInnerHtml);
                                    } else {
                                        let urlAlreadySavedForUser = false;
                                        if (data.count > 0) {
                                            urlId = data.rows[0].s[0];
                                            console.log("url already exists: url-id=" + urlId);
                                            const tableData = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                                            if (tableData) {
                                                for (let row = 0; row < tableData.count; row++) {
                                                    if (tableData.rows[row].s[1] === urlId) {
                                                        validationLabel.html(`<p>${TEXT_URL_ALREADY_SAVED}
                                               &nbsp;<i class="fa fa-exclamation"></i></p>
                                               <h4><a href="${APPLICATION_HOMEPAGE}/${tableData.rows[row].s[9]}">
                                                    ${APPLICATION_HOMEPAGE}/${tableData.rows[row].s[9]}</a></h4>`);
                                                        validationLabel.show();
                                                        urlAlreadySavedForUser = true;
                                                        callingObject.html(objectInnerHtml);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                        if (!urlAlreadySavedForUser) {
                                            console.log("new url ");
                                            newItemAdded = true;
                                            createShortcutName(function (sc) {
                                                console.log("call createShortcutName ...sc=" + sc);
                                                $("#saveScDialog").html(``);
                                                validationLabel.hide();
                                                let urlData = createSaveShortcutDialog(urlId, decodeURIComponent(urlstring), sc);
                                                $("#shortCut_taglist").attr("placeholder", getTextById(48));
                                                console.log("Text of taglist-input-field: " + getTextById(48));
                                                const table = `<div id="saveScDialog" class="text-md-center" id="tableForSubmittingNewShortcut">
                                                           <br/> ${urlData}
                                                            </div>`;
                                                callingObject.after(table);
                                                const taglistElement = document.getElementById("shortCut_taglist");
                                                taglistElement.addEventListener("keyup", function () {
                                                    $("#publish_shortcut").attr("data-taglist", $("#shortCut_taglist").val());
                                                    $("#save_shortcut").attr("data-taglist", $("#shortCut_taglist").val());
                                                });
                                                callingObject.html(objectInnerHtml);
                                            }, function (err) {
                                                console.error("could not create new shortcutname: " + JSON.parse(err));
                                                callingObject.html(objectInnerHtml);
                                            });
                                        }
                                    }
                                    errorCallBack = function () {
                                        callingObject.html(`<i class="fa fa-exclamation-triangle"></i>`);
                                    }
                                }
                            } catch
                                (err) {
                                console.error(err);
                                callingObject.html(`<i class="fa fa-exclamation-triangle"></i>`);
                            }
                    }
                    break;
                case "publish_shortcut":
                case "save_shortcut" :
                    let urlList = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                    const newcount = 1 + urlList.count;
                    urlList.count = newcount;
                    const newArrayElement = [];
                    const newObj = {"rn": newcount, "s": newArrayElement};
                    urlList.rows.push(newObj);
                    listoHTML.createTable.call(userUrlTableObject, "urllist-div", true);
                    eventCallBack = function () {
                        const urlstring = $("#longurl_input").val();
                        const taglist = $("#shortCut_taglist").val();
                        const shortcut = $("#publish_shortcut").data("shortcut");
                        const linkcategory = $("#publish_shortcut").data("idcategory");
                        if (shortcut && taglist) {
                            sessionStorage.setItem("current_shortcut", shortcut);
                            sessionStorage.setItem("shortcut_tags", taglist);
                        }
                        apiCall('', '/8/user/selecturlid', {"urlstring": urlstring}, function (dataArray) {
                            const data = dataArray[0];
                            const idurl = data.rows[0].s[0];
                            sessionStorage.setItem(SESSIONDATA_URLID_KEY, idurl);
                            sessionStorage.setItem(SESSIONDATA_URLSTRING_KEY, urlstring);
                            analyzeRequestedPage(idurl, urlstring, function (icons) {
                                    if (icons['statuscode'] && icons['statuscode'] >= 400) {
                                        console.error("icons.statuscode: " + icons.statuscode);
                                    } else console.info("icons", icons);
                                },
                                function (meta) {
                                    //sessionStorage.removeItem("usershortcuts");

                                    if (meta['statuscode'] && meta['statuscode'] >= 400) {
                                        console.error("meta.statuscode: " + meta['statuscode']);
                                    } else if (meta['statuscode'] && meta['statuscode'] >= 200 && meta['statuscode'] <= 204) {
                                        console.info("meta", meta);
                                        if (data) {
                                            const dt = new Date();
                                            const utcDate = dt.toUTCString();
                                            newArrayElement[2] = urlstring;
                                            newArrayElement[9] = shortcut;
                                            newArrayElement[10] = utcDate;
                                            newArrayElement[16] = linkcategory;
                                            newArrayElement[18] = utcDate;
                                            sessionStorage.setItem(SESSIONDATA_KEY, JSON.stringify(urlList));
                                            userUrlTableObject.jsonRMLData = urlList;
                                            listoHTML.createTable.call(userUrlTableObject, "urllist-div", true);
                                        }
                                    }
                                }, function (err) {
                                    console.error(err);
                                });
                        }, function (errSelectUrl) {
                            console.error(errSelectUrl);
                        }, "post", "url");
                        $("#shortCut_taglist").remove();
                        $("#tableForSubmittingNewShortcut").remove();
                        hideLongUrl();

                        $("#longurl_input").val(``);
                        setDovisVisible(["shortcutlistfunctions", "urllist-div"], dovisNavigationIDs);


                    }
                    break;
                case "save_edit":
                    eventCallBack = function (dataArray) {
                        const data = dataArray[0];
                        console.log("userdata were modified: " + JSON.stringify(data));
                    }
                    break;
                case
                "create_newurl_submit":
                    try {
                        console.log("calling new insert of url with many params");
                    } catch (err) {
                        console.error(err);
                    }
                    break;
                case
                "create_personalized_shorturl":
                    eventCallBack = function (dataArray) {
                        const data = dataArray[0];
                    };
                    break;
                case
                "settings":
                    eventCallBack = function (data) {
                        showLoading(true);
                        const settingsHtml = buildSettingsPage(data);
                        $("#settings-div").html(settingsHtml);
                        showLoading(false);
                    }
                    break;
                case
                "showUserUrlsButton":
                    eventCallBack = function (dataArray) {
                        const data = dataArray[0];
                        if (data.count < MAX_TABLE_DATA_SIZE) {
                            const storedData = JSON.stringify(data);
                            sessionStorage.setItem(SESSIONDATA_KEY, storedData);
                            userUrlTableObject.jsonRMLData = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                            listoHTML.createTable.call(userUrlTableObject, "urllist-div", true);
                            newItemAdded = false;
                            // add tags to database
                            const shortcut = sessionStorage.getItem("current_shortcut");
                            const taglist = sessionStorage.getItem("shortcut_tags");
                            if (shortcut && taglist) {
                                apiCall('', '/8/user/checkifshortcutexists', {"shortcut": shortcut}, function (dataArray) {
                                        const data = dataArray[0];
                                        sessionStorage.removeItem("current_shortcut");
                                        sessionStorage.removeItem("shortcut_tags");
                                        const idshortcut = data.rows[0].s[0];
                                        if (idshortcut) {
                                            const tagArray = taglist.split(" ");
                                            for (let idx = 0; idx < tagArray.length; idx++) {
                                                const tagAtIdx = tagArray[idx];
                                                apiCall('', '/8/user/tagshortcut', {
                                                    "tag": tagAtIdx,
                                                    "idshortcut": idshortcut
                                                }, function (response) {
                                                    console.log(JSON.stringify(response));
                                                }, function (err) {
                                                    console.error(err);
                                                }, "post", "url");
                                            }
                                        }
                                    },
                                    function (err) {
                                        console.error(err);
                                    }, "post", "url");
                            }
                        } else {
                            let dataSizeWarning = $("#datasize-warning");
                            // TODO
                            // for (let i=0;i<MAX_TABLE_DATA_SIZE;i++)
                            userUrlTableObject.jsonRMLData = data;
                            listoHTML.createTable.call(userUrlTableObject, "urllist-div", true);

                            newItemAdded = false;
                            $("#urllist-div").before(`<h4 id="datasize-warning"><i  class="fa fa-exclamation fa-3x"></i>
                                                    <br>${TEXT_MAXIMUM_DATASIZE_EXCEEDED}</h4><br>`);
                            alert(TEXT_MAXIMUM_DATASIZE_EXCEEDED);
                            console.info(TEXT_MAXIMUM_DATASIZE_EXCEEDED, data);
                            dataSizeWarning.fadeOut(4500);
                            $("#datasize-warning").remove();
                        }

                    }
                    break;
                default:
                    eventCallBack = function (dataObject) {
                        console.log("Call triggered per default with following data: " + JSON.stringify(dataObject));
                    };
            }
            try {
                apiCall("dovis", "/" + applicationID + "/" + path, dataObject, function (data) {
                        eventCallBack(data);
                        document.getElementById("waitMessage").style.visibility = "hidden";
                    }, function (err) {
                        try {
                            if (err) showHttpErrorMessage("dovis-errors", err);
                        } catch (err1) {
                            try {
                                showHttpErrorMessage("dovis-errors", JSON.parse(err));
                            } catch (err2) {
                                $("#dovis-errors").html(TEXT_SERVER_ERROR);
                            }
                        }
                    }, method, encoding
                );
            } catch (error) {
                console.error(error);
            }
        }
        console.log(` ******* Object ${objectId}  has called the api`);
    }
;
