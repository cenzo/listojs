"use strict";

let text = "Click here to open the link";
let SALE_TEXT = "Reserve this short url";

document.getElementById("back_home").href = APPLICATION_HOMEPAGE;

function getUrlVars() {
    const lets = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        lets[key] = value;
    });
    return lets;
}


function setClickText() {
    const lang = navigator.language || navigator.browserLanguage;
    // TODO include other languages
    if (lang.startsWith("de")) {
        text = "Hier klicken um zur Seite zu gelangen";
        SALE_TEXT = "Diese Kurz-Url reservieren";
    } else if (lang.startsWith("it")) {
        text = "Clicca qui per accedere al link";
        SALE_TEXT = "Prenota questa Url breve";
    }
    return `<br>${text}<br><i class="fa fa-arrow-down"></i><br>`;
}

console.log("pathname on /t: " + window.location.pathname + " \n search: " + window.location.search + "\nhash:" + window.location.hash);
console.log("Selected server: ",selectedServer);
const redirect = function () {
    const path = window.location.pathname;
    const shortUrlParam = path.substring(1 + path.lastIndexOf("/"), path.length);

    let showUrlOnly = true;
    let shortUrl;
    if (shortUrlParam.endsWith('%2b')) {
        shortUrl = shortUrlParam.substring(0, shortUrlParam.length - 3);
    } else if (shortUrlParam.endsWith('+')) {
        shortUrl = shortUrlParam.substring(0, shortUrlParam.length - 1);
    } else {
        shortUrl = shortUrlParam;
        showUrlOnly = false;
    }
    const xhttp = new XMLHttpRequest();
    // xhttp2 for tracking: better be done later with a log-analyzer
    //const xhttp2 = new XMLHttpRequest();
    xhttp.open("GET", `${selectedServer}${customerID}/8/public/select?shortcut=${shortUrl}`, true);
    //xhttp2.open("GET", `${selectedServer}${customerID}/8/public/track?shortcut=${shortUrl}`, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //xhttp2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xhttpState = -1;
    xhttp.onreadystatechange = function () {
        xhttpState = xhttp.readyState;
        if (xhttpState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                let jsonArray = JSON.parse(xhttp.responseText);
                let jsonObject = jsonArray[0];
                if (jsonObject.count === 0) {
                    document.getElementById("redirection").innerHTML = `<i class="fas fa-unlink"></i><br/>
                    <h5>${APPLICATION_HOMEPAGE}/${shortUrl}</h5>
                     <h5 class="text-md-center"><i class="fa fa-shopping-cart"></i>&nbsp;${SALE_TEXT}</h5>`;
                    document.getElementById("redirectionUrl").setAttribute
                    ("href", `${APPLICATION_HOMEPAGE}?bidurl=${shortUrl}`);
                } else {
                    //xhttp2.send();

                    jsonArray = JSON.parse(xhttp.responseText);
                    jsonObject = jsonArray[0];

                    const page = jsonObject.rows[0].s[2];
                    const decodedPage = decodeURIComponent(page);
                    const cat = jsonObject.rows[0].s[4];
                    const summary = jsonObject.rows[0].s[2];
                    if (summary && summary != 'undefined') {
                        const title = document.getElementsByTagName("title");
                        for (let i = 0; i < title.length; i++) title[i].innerText = summary;
                    }
                    if (cat > 2 && !showUrlOnly) window.location.href = decodedPage;
                    else {
                        const redirectionUrl=document.getElementById("redirectionUrl");
                        redirectionUrl.setAttribute("target","_blank");
                        if (!shortUrl) window.location.href = APPLICATION_HOMEPAGE;
                        const shortUrlText = document.getElementById("shorturl");
                        let redirection = document.getElementById("redirection");
                        redirection.innerHTML = `<i class="fas fa-link"></i>&nbsp;&nbsp;${decodedPage}`;
                        shortUrlText.innerText = `${APPLICATION_HOMEPAGE}/${shortUrl}`;
                        const hint = document.createElement("span");
                        hint.innerHTML = setClickText();
                        shortUrlText.append(hint);
                        if (cat > 1) redirectionUrl.setAttribute("href",
                            `${decodedPage}`);
                        else {
                            redirection.innerHTML = `<a href="${APPLICATION_HOMEPAGE}?bidurl=${shortUrl}" target="_blank"><i class="fas fa-unlink"></i><br/>
                        <h4 class="text-md-center"><i class="fas fa-shopping-cart"></i>&nbsp;${SALE_TEXT}</h4></a>`;
                        }
                        document.getElementById("ads").innerHTML = `<hr>
                            <span>
                            <img style="width: 75px;max-height: 75px;" src="img/testimonials-1.jpg">
                            <img style="width: 75px;max-height: 75px;"  src="img/testimonials-2.jpg">
                            <img style="width: 75px;max-height: 75px;"  src="img/testimonials-3.jpg">
                            </span>
                            <hr>`;
                    }
                }
            } else {
                document.getElementById("redirection").innerHTML = `<br/>
                          <i class="fas fa-exclamation-circle"></i>
                          <br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${xhttp.status}&nbsp;&nbsp;`;
                document.getElementById("redirectionUrl").setAttribute("href", `${APPLICATION_HOMEPAGE}`);
            }
        }
    };
    try {
        xhttp.send();
    } catch (err) {
        console.error(err.toString());
        document.getElementById("redirection").innerHTML = `<br/><i class="fas fa-exclamation-triangle"></i><br/>&nbsp;&nbsp;UNKNOWN ERROR OCCURRED&nbsp;&nbsp;`;
        document.getElementById("redirectionUrl").setAttribute("href", `${APPLICATION_HOMEPAGE}`);
    }
};

