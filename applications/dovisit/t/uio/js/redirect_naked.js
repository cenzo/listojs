"use strict";

let OPN_LNK = "Click here to open the link:";
let ABOUT=" About dovis.it"
let RSRV_LNK = "Reserve this short url";
let BID_LNK = "Place a bid for this short url";
let UNKNOWN_ERR = "Unknown error";
let dgi = function (el) {
    return document.getElementById(el);
}
let dce = function (el) {
    return document.createElement(el);
}
let ac = function (p, c) {
    p.appendChild(c);
}

let l = navigator.language || navigator.browserLanguage;
if (l.startsWith("de")) {
    OPN_LNK = "Hier klicken um zur Seite zu gelangen";
    RSRV_LNK = "Diese Kurz-Url reservieren:";
    BID_LNK = "Angebot für diese Kurz-Url abgeben";
    UNKNOWN_ERR = "Fehler";
    ABOUT="Über dovis.it"
} else if (l.startsWith("it")) {
    OPN_LNK = "Clicca qui per accedere al link:";
    RSRV_LNK = "Prenota questa Url breve";
    BID_LNK = "Partecipa all'asta per questa url breve";
    UNKNOWN_ERR = "Errore";
    ABOUT="Cos'é dovis.it"
}

dgi("back_home").href = APPLICATION_HOMEPAGE;
dgi("about").innerText = ABOUT;

let bid = function (r, s, t, sc, c) {
    r.setAttribute("href",
        `${APPLICATION_HOMEPAGE}?bidurl=${sc}`)
    if (c === 1 || c > 5) {
        s.innerHTML = ` <a href="mailto:support@dovis.it?subject=BID on short url: ${sc}">
                         <img src="img/judge-gavel.jpg" style="max-width:50px;" alt="reserve ${sc}">
                            <b>${BID_LNK.toUpperCase()}</b>
                          </a>`;
        t.innerText = `${APPLICATION_HOMEPAGE.toLowerCase()} / ${sc.toUpperCase()}`;
    } else {
        s.innerHTML = ` <a href="mailto:support@dovis.it?subject=RESERVE short url: ${sc}">
                            <img src="img/shopping-icon.png" style="max-width:50px;" alt="reserve ${sc}">
                            <b>${RSRV_LNK.toUpperCase()}</b>
                         </a>`;
        t.innerText = `${APPLICATION_HOMEPAGE.toLowerCase()} / ${sc.toUpperCase()}`;
    }
}
console.log("pathname on /t: " + window.location.pathname + " \n search: " + window.location.search + "\nhash:" + window.location.hash);
console.log("server: ", selectedServer);
//production:
let p = window.location.pathname;
//test:
//let p = window.location.search.substring(1);
let sup = p.substring(1 + p.lastIndexOf("/"), p.length);
let rdUrl = dgi("rdUrl");
let rdirLabel = dgi("linkLabel");
let txt = dgi("clickText");
let shortUrl = dgi("shorturl");
let shortUrlp = dgi("shorturlp");
let shortUrlpp = dgi("shorturlpp");
let shUrl=dgi("shUrl");
let shUrlp=dgi("shUrlp");
let shUrlpp=dgi("shUrlpp");
let sc;
let getAttrs = false;
let getUDesc = false;
let redirect = function () {
    if (sup.endsWith('%2b')) {
        sc = sup.substring(0, sup.length - 3);
    } else if (sup.endsWith('++')) {
        sc = sup.substring(0, sup.length - 2);
        getAttrs = true;
        getUDesc = true;
    } else if (sup.endsWith('+')) {
        sc = sup.substring(0, sup.length - 1);
        getUDesc = true;
    } else {
        sc = sup;
    }

    let x = new XMLHttpRequest();
    x.open("GET", `${selectedServer}${customerID}/8/public/select?shortcut=${sc}`, true);
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xst = -1;
    x.onreadystatechange = function () {
        console.log("status", x.status);
        xst = x.readyState;
        if (xst === 4) {
            if (x.status >= 200 && x.status <= 204) {
                let jArr = JSON.parse(x.responseText);
                let jObj = jArr[0];

                shortUrl.innerText = `${APPLICATION_HOMEPAGE}  /  ${sc.toUpperCase()}`;
                shortUrlp.innerText = `  ${sc.toUpperCase()}+`;
                shortUrlpp.innerText = `  ${sc.toUpperCase()}++`;

                if (jObj['count'] === 0)
                    bid(rdUrl, rdirLabel, txt, sc, 0);
                else {
                    let v = jObj['_VariableLabels'];
                    let s = jObj.rows[0].s;
                    let pg = s[2];
                    let dPg = decodeURIComponent(pg);
                    let cat = parseInt(s[4], 10);
                    console.log(s, s[4], "link-category", cat);
                    if (!sc) window.location.href = APPLICATION_HOMEPAGE;
                    if (cat > 1 && cat < 6) {
                        rdUrl.setAttribute("href",
                            `${dPg}`);
                        shUrl.setAttribute("href",`${APPLICATION_HOMEPAGE}/${sc}`);
                        shUrlp.setAttribute("href",`${APPLICATION_HOMEPAGE}/${sc}+#udesclink`);
                        shUrlpp.setAttribute("href",`${APPLICATION_HOMEPAGE}/${sc}++#attrslink`);
                        rdUrl.setAttribute("target", "_blank");
                        rdirLabel.innerText = `${dPg}`;
                        txt.innerText = OPN_LNK;
                        if (getUDesc) {
                            const htm = dce("div");
                            if (s[9] && s[9] != 'undefined') {
                                htm.innerHTML = `${s[9]}`;
                            } else {
                                htm.innerHTML = "<hr> - No user-description available - <hr>";
                            }
                            ac(dgi("udesc"), htm);

                            if (getAttrs) {
                                const meta=dce("div");
                                meta.innerHTML=`<b><u>METADATA:</u></b><br><br>`
                                ac(dgi("attrs"), meta);
                                const br=dce("br");
                                ac(meta,br);
                                let metAvail=false;
                                let sv = function (i) {
                                    if (s[i] && s[i] != 'undefined') {
                                        metAvail=true;
                                        let p = dce("div");
                                        let t = v[i].toUpperCase();
                                        p.innerHTML = `<u>${t}:</u>&nbsp;${s[i]}`;
                                        const br=dce("br");
                                        ac(dgi("attrs"), p);
                                        ac(dgi("attrs"),br);
                                    }
                                }
                                for (let l = 10; l < 16; l++) sv(l);
                                if (!metAvail)dgi("attrs").innerText="- No metadata available -";
                            }
                        }
                    } else bid(rdUrl, rdirLabel, txt, sc, cat);
                }
            } else {
                rdirLabel.innerHTML = `<br/>
                          <br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${x.status}&nbsp;&nbsp;`;
                rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
            }
           /* dgi("ad1").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-1.jpg" alt="ad1"> `;
            dgi("ad2").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-2.jpg" alt="ad2"> `;
            dgi("ad3").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-3.jpg" alt="ad3"> `;
            dgi("ad4").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-1.jpg" alt="ad4"> `;
            dgi("ad5").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-2.jpg" alt="ad5"> `;*/
        }
    };
    try {
        x.send();

    } catch (err) {
        console.error(err.toString());
        rdirLabel.innerHTML = `<br/><br/>&nbsp;&nbsp;${UNKNOWN_ERR}&nbsp;&nbsp;`;
        rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
    }
};

