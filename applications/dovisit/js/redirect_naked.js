"use strict";

let OPN_LNK = "Open this link in new page";
let ABOUT = " About dovis.it"
let RSRV_LNK = "Reserve this short url";
let BID_LNK = "Place a bid for this short url";
let UNKNOWN_ERR = "Unknown error";
let TEXT_NO_METADATA = "No metadata available";
let TEXT_NO_USERDESC = "No user description available";
let TEXT_BY = "This page was created by ";
let TEXT_CREATED = " Created";
let TEXT_LAST_UPDATED = "Last update";
let TEXT_ANON = "Undiclosed";

let dgi = function (el) {
    return document.getElementById(el);
}
let dce = function (el) {
    return document.createElement(el);
}
let ac = function (p, c) {
    p.appendChild(c);
}

let l = navigator.language;// || navigator.browserLanguage;
if (l.startsWith("de")) {
    OPN_LNK = "Diesen Link in neuer Seite öffnen";
    RSRV_LNK = "Diese Kurz-Url reservieren:";
    BID_LNK = "Angebot für diese Kurz-Url abgeben";
    UNKNOWN_ERR = "Fehler";
    ABOUT = "Über dovis.it";
    TEXT_NO_METADATA = "Keine Metadaten vorhanden";
    TEXT_NO_USERDESC = "Keine Beschreibung vorhanden";
    TEXT_BY = "Seite wurde erstellt von: ";
    TEXT_CREATED = " Erstellt";
    TEXT_LAST_UPDATED = "Letzte Aktualisierung";
    TEXT_ANON="Nicht öffentlich";
} else if (l.startsWith("it")) {
    OPN_LNK = "Apri questo link in nuova pagina";
    RSRV_LNK = "Prenota questa Url breve";
    BID_LNK = "Partecipa all'asta per questa url breve";
    UNKNOWN_ERR = "Errore";
    ABOUT = "Cos'é dovis.it";
    TEXT_NO_METADATA = "Non ci sono metadati disponibili";
    TEXT_NO_USERDESC = "Non é stata fornita una descrizione per il link";
    TEXT_BY = "Pagina creata da: ";
    TEXT_CREATED = " Data di creazione";
    TEXT_LAST_UPDATED = "Ultimo aggiornamento";
    TEXT_ANON="non pubblicato";
}

dgi("back_home").href = APPLICATION_HOMEPAGE;
dgi("about").innerText = ABOUT;

let bid = function (r, s, t, sc, c) {
    r.setAttribute("href",
        `${APPLICATION_HOMEPAGE}?bidurl=${sc}`)
    if (c === 1 || c > 5) {
        s.innerHTML = ` <a href="mailto:support@dovis.it?subject=BID on short url: ${sc}">
                         <img src="img/judge-gavel.jpg" style="max-width:50px;" alt="reserve ${sc}">
                            <b>${BID_LNK.toUpperCase()}</b>
                          </a>`;
        t.innerText = `${APPLICATION_HOMEPAGE.toLowerCase()} / ${sc.toUpperCase()}`;
    } else {
        s.innerHTML = ` <a href="mailto:support@dovis.it?subject=RESERVE short url: ${sc}">
                            <img src="img/shopping-icon.png" style="max-width:50px;" alt="reserve ${sc}">
                            <b>${RSRV_LNK.toUpperCase()}</b>
                         </a>`;
        t.innerText = `${APPLICATION_HOMEPAGE.toLowerCase()} / ${sc.toUpperCase()}`;
    }
}


console.log("pathname on /t: " + window.location.pathname + " \n search: " + window.location.search + "\nhash:" + window.location.hash);
console.log("server: ", selectedServer);
//production:
let p = window.location.pathname;
//test:
//let p = window.location.search.substring(1);
let sup = p.substring(1 + p.lastIndexOf("/"), p.length);
let rdUrl = dgi("rdUrl");
let rdUrl2 = dgi("rdUrl2");
let rdirLabel = dgi("linkLabel");
let txt = dgi("clickText");
let shortUrl = dgi("shorturl");
let shortUrlp = dgi("shorturlp");
let shortUrlpp = dgi("shorturlpp");
let shUrl = dgi("shUrl");
let shUrlp = dgi("shUrlp");
let shUrlpp = dgi("shUrlpp");
let sc;
let getAttrs = false;
let getUDesc = false;
let s, v;
let blogUserName, blogTs, blogUserTitle, blogUserLastName, blogUserEmail, blogUpdateTs, showAuthor, showEmail;
const showUserDesc = function () {
    dgi("udesc").innerHTML = ``;
    const htm = dce("div");
    if (s[9] && s[9] !== 'undefined') {
        htm.innerHTML = `${s[9]}`;
    } else {
        htm.innerHTML = `<hr> - ${TEXT_NO_USERDESC} - <hr>`;
    }
    ac(dgi("udesc"), htm);
}
const showMetadata = function () {
    dgi("attrs").innerHTML = ``;
    const meta = dce("div");

    ac(dgi("attrs"), meta);
    const br = dce("br");
    ac(meta, br);
    let metAvail = false;
    let sv = function (i) {
        if (s[i] && s[i] !== 'undefined') {
            metAvail = true;
            let p = dce("div");
            let t = v[i].toUpperCase();
            p.innerHTML = `<u>${t}:</u>&nbsp;${s[i]}`;
            const br = dce("br");
            ac(dgi("attrs"), p);
            ac(dgi("attrs"), br);
        }
    }
    for (let l = 10; l < 16; l++) sv(l);
    if (!metAvail) dgi("attrs").innerText = TEXT_NO_METADATA;
}
let redirect = function () {
    if (sup.endsWith('%2b')) {
        sc = sup.substring(0, sup.length - 3);
    } else if (sup.endsWith('++')) {
        sc = sup.substring(0, sup.length - 2);
        getAttrs = true;
        getUDesc = true;
    } else if (sup.endsWith('+')) {
        sc = sup.substring(0, sup.length - 1);
        getUDesc = true;
    } else {
        sc = sup;
    }

    let x = new XMLHttpRequest();
    x.open("GET", `${selectedServer}${customerID}/8/public/select?shortcut=${sc}`, true);
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xst = -1;
    x.onreadystatechange = function () {
        console.log("status", x.status);
        xst = x.readyState;
        if (xst === 4) {
            if (x.status >= 200 && x.status <= 204) {
                let jArr = JSON.parse(x.responseText);
                let jObj = jArr[0];

                shortUrl.innerHTML = `<b>${APPLICATION_HOMEPAGE}/<h4>${sc.toUpperCase()}</h4></b>`;

                if (jObj['count'] === 0) {
                    bid(rdUrl, rdirLabel, txt, sc, 0);
                    shortUrlp.innerText = ``;
                    shortUrlpp.innerText = ``;
                } else {
                    shortUrlp.innerText = `  ${sc.toUpperCase()}+`;
                    shortUrlpp.innerText = `  ${sc.toUpperCase()}++`;
                    v = jObj['_VariableLabels'];
                    s = jObj.rows[0].s;
                    blogUserTitle = s[16];
                    blogUserName = s[17];
                    blogUserLastName = s[18];
                    blogUserEmail = s[19];
                    blogTs = s[20];
                    blogUpdateTs = s[21];
                    showAuthor = s[22] === "true";
                    showEmail = s[23] === "true";
                    if (blogUserTitle) dgi("usertitle").innerText = blogUserTitle;
                    else dgi("usertitle").innerText = ``;
                    let subtitle = `<p> ${TEXT_BY} `;
                    if (blogUserName && blogUserLastName && showAuthor === true) subtitle += `${blogUserName} ${blogUserLastName} | `;
                    else subtitle += ` <small><i> ${TEXT_ANON} </i></small> | `
                    if (blogUserEmail && showEmail === true) subtitle += ` <small>[${blogUserEmail.replace('@', ' at ')}]</small> | `;

                    if (blogTs) subtitle += ` <small>${TEXT_CREATED}: ${blogTs}</small> | `;
                    if (blogUpdateTs) subtitle += ` <small>${TEXT_LAST_UPDATED}: ${blogUpdateTs}</small>  `;
                    subtitle += `</p>`;
                    dgi("subtitle").innerHTML = subtitle;
                    let pg = s[2];
                    let dPg = decodeURIComponent(pg);
                    let cat = parseInt(s[4], 10);
                    console.log(s, s[4], "link-category", cat);
                    if (!sc) window.location.href = APPLICATION_HOMEPAGE;
                    if (cat > 1 && cat < 6) {
                        rdUrl.setAttribute("href",
                            `${dPg}`);
                        rdUrl2.setAttribute("href",
                            `${dPg}`);
                        shUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}/${sc}`);
                        //shUrlp.setAttribute("href",`${APPLICATION_HOMEPAGE}/${sc}+#udesclink`);
                        //shUrlpp.setAttribute("href",`${APPLICATION_HOMEPAGE}/${sc}++#attrslink`);
                        rdUrl.setAttribute("target", "_blank");
                        rdUrl2.setAttribute("target", "_self");
                        rdirLabel.innerText = `${dPg}`;
                        txt.innerText = OPN_LNK;
                        if (getUDesc) {
                            showUserDesc();
                            if (getAttrs) {
                                showMetadata();
                            }
                        }
                    } else bid(rdUrl, rdirLabel, txt, sc, cat);
                }
            } else {
                rdirLabel.innerHTML = `<br/>
                          <br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${x.status}&nbsp;&nbsp;`;
                rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
            }
            /* dgi("ad1").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-1.jpg" alt="ad1"> `;
             dgi("ad2").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-2.jpg" alt="ad2"> `;
             dgi("ad3").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-3.jpg" alt="ad3"> `;
             dgi("ad4").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-1.jpg" alt="ad4"> `;
             dgi("ad5").innerHTML = `<img style="width: 125px;max-height: 125px;" src="img/testimonials-2.jpg" alt="ad5"> `;*/
        }
    };
    try {
        x.send();

    } catch (err) {
        console.error(err.toString());
        rdirLabel.innerHTML = `<br/><br/>&nbsp;&nbsp;${UNKNOWN_ERR}&nbsp;&nbsp;`;
        rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
    }
};

