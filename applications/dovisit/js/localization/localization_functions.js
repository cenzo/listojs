"use strict";
const langDebug = true;
let userLanguage = null;


function setElemValue(id) {
    try {
        document.getElementById('text' + id).value = getTextById(id);
    } catch (err) {
        if (langDebug) console.warn("Could not set text" + id);
    }
}

function setElemTextIgnoreErrors(id) {
    //if (langDebug) console.log('Setting text for element with id "text' + id + '" ');
    try {
        document.getElementById('text' + id).innerText = getTextById(id);
        if (langDebug) console.log("... text" + id + " was set.");
    } catch (err) {
        if (langDebug) console.log("--- Could not set text" + id);
    }
}

function setElemText(id) {
    let elem;
    try {
        elem = document.getElementById('text' + id);
    } catch (error) {
        if (langDebug) console.error(error);
    }
    if (elem) {
        try {
            elem.innerText = getTextById(id);
        } catch (err2) {
            try {
                if (langDebug) console.error("Inner text for id 'text" + id + "'+could not be set. Try to set text-value for element with  id 'text" + id
                    + "': " + JSON.stringify(err2));
                setElemValue(id);
            } catch (err3) {
                if (langDebug) console.error("Could not set text for id 'text" + id + "': " + JSON.stringify(err3));
            }
        }
    } else {
        if (langDebug) console.error("Localization-ERROR: id 'text" + id
            + "' does not exist in this document.");
    }
}

const setLanguage = function () {
    const divs = document.querySelectorAll('.set-language');
    divs.forEach(el => el.addEventListener('click', event => {
        let rowDiv = languageSelect.pre;
        languageSelect.languages.forEach(
            function (key, lang) {
                rowDiv += `<a href="javascript:void(0)" name="language" class="selectTheLanguage ${languageSelect.styleClass}" data-lang="${lang.id}" data-callback="${languageSelect.callback}" >&nbsp;${lang.name}</a>&nbsp;`;
            });
        rowDiv += languageSelect.suf;
        document.getElementById(languageSelect.targetDiv).innerHTML = rowDiv;
    }));
};
setLanguage();

/*
$(document)
    .on(
        'click',
        '.set-language',
        function () {
            let rowDiv = languageSelect.pre;

            $
                .each(
                    languageSelect.languages,
                    function (key, lang) {
                        rowDiv += `<a href="javascript:void(0)" name="language" class="selectTheLanguage ${languageSelect.styleClass}" data-lang="${lang.id}" data-callback="${languageSelect.callback}" >&nbsp;${lang.name}</a>&nbsp;`;
                    });
            rowDiv += languageSelect.suf;
            $('#' + languageSelect.targetDiv).html(rowDiv);
        });

$(document).on('change', '#langSelect', function () {
    const radios = document.getElementsByName('language');

    let i = 0;
    const length = radios.length;
    for (; i < length; i++) {
        if (radios[i].checked) {
            // do whatever you want with the checked radio
            const href = new URL(location.href);

            href.searchParams.set('lang', radios[i].id);
            location.href = href.toString();

            // only one radio can be logically checked, don't check the rest
            break;
        }
    }
});*/

function getUrlVars() {
    const vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function languageIsAvailable(lan) {
    let found = false;
    for (let i = 0; i < languageSelect.languages.length; i++) {
        if (languageSelect.languages[i].id == lan) {
            found = true;
            break;
        }
    }
    if (found) return true;
    else return false;
}

function getLocale() {
    const theLanguage = getUrlVars()["lang"];
    if ((theLanguage) && (theLanguage.length > 1) && languageIsAvailable(theLanguage)) {
        locale = theLanguage;
        sessionStorage.setItem("currentLanguage", locale);
        if (langDebug) console.log("Switched locale via url parameter to '" + locale + "'");
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        return locale;
    }
    if (locale && languageIsAvailable(locale)) {
        sessionStorage.setItem("currentLanguage", locale);
        if (langDebug) console.log("Locale is already set to: '" + locale + "'");
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        return locale;
    }
    if (navigator.languages !== undefined) {
        locale = navigator.languages[0].substring(0, 2);
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        if (langDebug) console.log("Default locale selected from first element in browser-list: '" + locale + "'");
    } else if (navigator.language !== undefined) {
        if (langDebug) console.log("Default browser-locale selected: '" + locale + "'");
        locale = navigator.language.substring(0, 2);
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
    }
    if (languageIsAvailable(locale)) return locale;
    else {
        console
            .info("Locale '"
                + locale + "' is not available for this application. Returning default-locale: 'en'");
        locale = "en";
        if (!sessionStorage.getItem("currentLanguage")) sessionStorage.setItem("currentLanguage", locale);
        return locale;
    }
}

var locale = getLocale();

function getCurrentLanguage() {
    const uriLanguage = getUrlVars()["lang"];
    const sessionLanguage = sessionStorage.getItem("currentLanguage");
    if (uriLanguage) {
        //if (langDebug) console.log("Got current language from uri: " + uriLanguage);
        return uriLanguage;
    } else if (sessionLanguage) {
        // if (langDebug) console.log("Got current language from session: " + sessionLanguage);
        return sessionLanguage;
    } else if(locale!==null && locale!=="undefined")
        return locale;
    else return 'en';
}


function getCurrency() {
    switch (locale) {
        case ("de"):
            return " €";
        case ("en"):
            return " €";
        case ("it"):
            return " €";
        case ("es"):
            return " €";
        case ("us"):
            return " $";
        case ("uk"):
            return " £";
        default:
            return "  ";
    }
}

function getTextById(id) {
    const theText = getLocaleTextById(id);
    if (theText)  return theText;
    else if (theText === null)
        return "ERROR_NLV_" + locale + "_id_" + id;
    else return "TEXT_" + locale + "_" + id;

}

function getLocaleTextById(id) {
    if (userLanguage === null) {
        userLanguage = getCurrentLanguage();
    }
    switch (userLanguage) {
        case ("de"):
            if (typeof texts_de !== 'undefined')
                return texts_de["text" + id];
            else
                return null;
            break;
        case ("it"):
            if (typeof texts_it !== 'undefined')
                return texts_it["text" + id];
            else
                return null;
            break;
        case ("es"):
            if (typeof texts_es !== 'undefined')
                return texts_es["text" + id];
            else
                return null;
            break;
        case ("us"):
            if (typeof texts_us !== 'undefined')
                return texts_us["text" + id];
            else
                return null;
            break;
        case ("en"):
            if (typeof texts_en !== 'undefined')
                return texts_en["text" + id];
            else
                return null;
            break;
        case ("uk"):
            if (typeof texts_uk !== 'undefined')
                return texts_uk["text" + id];
            else
                return null;
            break;
        default:
            console.error("ERROR: locale '" + locale+"' was not found");
            return texts_en["text" + id];
    }

}

/*
function getText(mnemonic) {
    const theText = getLocaleTextByMnemonic(mnemonic);
    if (theText === null)
        return "ERROR_NLV_" + locale + "_m_" + mnemonic;
    return theText;
}

function getLocaleTextByMnemonic(mnemonic) {
    const lang = getCurrentLanguage();
    switch (lang) {
        case ("de"):
            if (typeof mnemonics_de !== 'undefined')
                return mnemonics_de[mnemonic];
            else
                return null;
            break;
        case ("it"):
            if (typeof mnemonics_it !== 'undefined')
                return mnemonics_it[mnemonic];
            else
                return null;
            break;
        case ("es"):
            if (typeof mnemonics_es !== 'undefined')
                return mnemonics_es[mnemonic];
            else
                return null;
            break;
        case ("us"):
            if (typeof mnemonics_us !== 'undefined')
                return mnemonics_us[mnemonic];
            else
                return null;
            break;
        case ("en"):
            if (typeof mnemonics_en !== 'undefined')
                return mnemonics_en[mnemonic];
            else
                return null;
            break;
        case ("uk"):
            if (typeof mnemonics_uk !== 'undefined')
                return mnemonics_uk[mnemonic];
            else
                return null;
            break;
        default:
            return "ERROR: NOLOC_" + locale;
    }
};

function setLanguage(id) {
    sessionStorage.setItem("currentLanguage", id);
    apiCall_listorante_public_assignedtexts(getLayoutId(), function (data) {
        for (let k = 0; k < data.count; k++) {
            let languageElement = document.getElementById(data.rows[k].s[0]);
            //debug("assignedText",RELEASE,k,data.rows[k].s[0],data.rows[k].s[3]);
            if (languageElement) {
                languageElement.innerText = data.rows[k].s[3];
            }
        }
        if (languageSelect.preCallback) {
            languageSelect.preCallback();
        }
        if (getUrlVars()['lang']) {
            const href = new URL(location.href);
            href.searchParams.set('lang', id);
            location.href = href.toString();
        }
        setTexts();
        if (languageSelect.postCallback) {
            languageSelect.postCallback();
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

jQuery(document)
    .on(
        'click',
        '.selectTheLanguage',
        function () {
            const id = $(this).attr('data-lang');
            //const func = $(this).attr('data-languageCallBackFunction');
            //setLanguage(id, func);
            setLanguage(id);
        });*/


