"use strict";
const DOVIS_VERSION = frontendVersion;
const MAXIMUM_ALLOWED_FILESIZE_CSV = 100000;
const SESSIONDATA_KEY = "usershortcuts";
const SESSIONDATA_URLID_KEY = "urlID";
const SESSIONDATA_URLSTRING_KEY = "urlString";
let USER_CATEGORY = 5;
let USER_PRODUCT = "FREE";
let isPublicUser = true;
let lastPing;
const CALLBACKPAGE_USER_REGISTRATION = APPLICATION_HOMEPAGE;
const APPLICATION_LOGO = "dovisit_logo_small.png";
const RESET_PASSWORD_PAGE = APPLICATION_HOMEPAGE;
const JSON_HTML_URL = `${CALLBACKPAGE_USER_REGISTRATION}/email.json`;
const JSON_HTML_WELCOME_URL = `${CALLBACKPAGE_USER_REGISTRATION}/welcomemail.json`;
const instruction = window.location.search;
const urlParams = new URLSearchParams(window.location.search);

console.log("instruction=" + instruction);

/*--------------------------------------------------------------------------------------------------------------*/

const dovisNavigationIDs = ["bid-div", "settings-div", "create-newshortcut-div", "urllist-div", "dovis-errors",
    "showMailAccounts-div", "manageMailAccounts", "mailCampaignResult", "shortcutlistfunctions"];

/*--------------------------------------------------------------------------------------------------------------*/
const buildRegistrationConfirmationForm = function (challenge1, challenge2, email, regCode) {
    document.getElementById("newpassword").remove();
    document.getElementById("passwordhelp").remove();
    console.log("email=" + email);
    $("#registrationEmail").attr("placeholder", "");
    $("#registrationEmail").attr("value", email);
    $("#registrationEmail").attr("disabled", "true");
    const challenge = `${TEXT_WHAT_IS_THE_SUM_OF}${challenge1}&nbsp;${TEXT_AND}&nbsp;${challenge2} &nbsp;?`;
    $("#registrationEmail").after(`<input id="validationcode" class="form-control form-control-lg" placeholder=""
                                   type="text" value="${regCode}" disabled>`).after(`<label id="label_validation_code">${regCode}</label><i class="fa fa-key fa-2x"></i>`);
    $("#registrationEmail").after(`<p id="challengequestion">${challenge}</p><br>`);
    $("#registrationEmail").after(`<input id="challengeresponse" class="form-control form-control-lg" placeholder="${challenge}"
                                   type="number" required>`);
    $("#registrationEmail").after(`<input id="confirmpassword" class="form-control form-control-lg" placeholder="${TEXT_CONFIRM_PASSWORD}"
                                   type="password" required>`);
    $("#registrationEmail").after(`<input id="newpassword" name = "psw" class="form-control form-control-lg passwordCheck" placeholder="${TEXT_ENTER_PASSWORD}"
                                   type="password" required title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                   pattern=${"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"}>`);
    $("#registrationEmail").after(`<input id="newusername" class="form-control form-control-lg" placeholder=""
                                   type="text" value="${email}" disabled>`);
    $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-user-plus"></i></span>&nbsp;${TEXT_REGISTER_NOW}`);
    $("#registrationButton").attr("class", `btn btn-block btn-lg btn-primary confirmRegistration`);
    $("#newpassword").after(`<div id="passwordhelp">
                             <ul class="list-group">
                              <li class="list-group-item list-group-item-dark">${TEXT_PASSWORD_HINT}</li>
                               <li class="list-group-item list-group-item-primary"> ${TEXT_LOWERCASE_LETTER}<i id="letter" class="fa fa-user-secret"></i></li>
                              <li class="list-group-item list-group-item-primary">${TEXT_UPPERCASE_LETTER}<i id="capital" class="fa fa-user-secret"></i></li>
                                <li class="list-group-item list-group-item-primary">${TEXT_NUMBER}<i class="fa fa-user-secret" id="number"></i></li>
                                <li  class="list-group-item  list-group-item-primary"> ${TEXT_MINIMUM_LENGTH}<i class="fa fa-user-secret" id="length"></i></li>
                              </ul>
                            </div>`);
    $("#registrationMessage").hide();
    $("#newusername").hide();
};


const buildResetPwForm = function (resetCode, userName) {
    document.getElementById("newpassword").remove();
    document.getElementById("passwordhelp").remove();
    $("#registrationEmail").val(userName);
    $("#registrationEmail").attr("disabled", true);
    $("#registrationButton").attr("disabled", false);
    $("#registrationButton").attr("class", "btn btn-primary setNewPass");
    $("#registrationIcon").attr("disabled", false);
    $("#registrationIcon").html(`<i class ="fa fa-user-secret">&nbsp;${getTextById(77)}</i>`);
    $("#registrationButton").attr("data-user", userName);
    $("#registrationButton").attr("data-code", resetCode);

    $("#text_ready_getstarted").html(`<i class="fa fa-key">${TEXT_PASSWORD_CRITERIA}</i>`);
    $("#registrationEmail").after(`<input id="confirmpassword" class="form-control form-control-lg" placeholder="${TEXT_CONFIRM_PASSWORD}"
                                   type="password" required>`);
    $("#registrationEmail").after(`<input id="newpassword" name = "psw" class="form-control form-control-lg passwordCheck" placeholder="${TEXT_ENTER_PASSWORD}"
                                   type="password" required title="${TEXT_PASSWORD_CRITERIA}"
                                   pattern=${"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"}>`);
    $("#newpassword").after(`<div id="passwordhelp">
                             <ul class="list-group">
                              <li class="list-group-item list-group-item-dark">${TEXT_PASSWORD_HINT}</li>
                               <li class="list-group-item list-group-item-primary"> ${TEXT_LOWERCASE_LETTER}<i id="letter" class="fa fa-user-secret"></i></li>
                              <li class="list-group-item list-group-item-primary">${TEXT_UPPERCASE_LETTER}<i id="capital" class="fa fa-user-secret"></i></li>
                                <li class="list-group-item list-group-item-primary">${TEXT_NUMBER}<i class="fa fa-user-secret" id="number"></i></li>
                                <li  class="list-group-item  list-group-item-primary"> ${TEXT_MINIMUM_LENGTH}<i class="fa fa-user-secret" id="length"></i></li>
                              </ul>
                            </div>`);
};

const buildBidForm = function (shUrl) {
    const htm = `<div id="bidForm" class="rounded  dovis top text-md-center">
      <!--h4>${TEXT_RESERVE_URL}</h4>
      <input type="text" value="${shUrl}">
    <a class="btn btn-primary"><i class="fa fa-copyright"></i></a-->
    <br>
    <a href="mailto:support@dovis.it?subject=Reservation request for short url ${shUrl}">${TEXT_RESERVE_URL}</a>
    </div>`;

    $("#slogan-container-div").html(`<h2>${APPLICATION_HOMEPAGE}/${shUrl}</h2>`);
    $("#bid-div").html(htm);
    setDovisVisible("bid-div", dovisNavigationIDs);

    console.info("Bid for url:", shUrl);
}

if (instruction && instruction.startsWith("?register")) {
    document.getElementById("dovisactions").style.visibility = "hidden";
    document.getElementById("navbarSupportedContent").style.visibility = "hidden";
    const registrationCode = urlParams.get('register');
    const mail = urlParams.get('mail');
    const cr1 = urlParams.get('cr1');
    const cr2 = urlParams.get('cr2');
    console.log("registrationCode=" + registrationCode + " mail: " + mail + " cr1: " + cr1 + "cr2: " + cr2);
    window.location.hash = 'registrationIcon';
    if (!mail || !cr1 || !cr2) {
        $("#text_ready_getstarted").html(`<i class="fa fa-exclamation fa-2x"></i>&nbsp;&nbsp;${TEXT_REGISTRATION_EXPIRED}`);
    } else {
        buildRegistrationConfirmationForm(cr1, cr2, mail, registrationCode);
    }
} else if (instruction && instruction.startsWith("?resetpassw")) {
    document.getElementById("dovisactions").style.visibility = "hidden";
    document.getElementById("navbarSupportedContent").style.visibility = "hidden";
    const resetCode = urlParams.get('resetpassw');
    const userName = urlParams.get('user');
    console.log("resetCode=" + resetCode + " userName: " + userName);
    window.location.hash = 'registrationEmail';
    if (!resetCode) {
        $("#text_ready_getstarted").html(`<i class="fa fa-exclamation fa-2x"></i>&nbsp;&nbsp;${TEXT_REGISTRATION_EXPIRED}`);
    } else {
        buildResetPwForm(resetCode, userName);
    }
} else if (instruction && instruction.startsWith("?bidurl")) {
    const bidurl = urlParams.get('bidurl');

    console.log("bidurl=" + bidurl);
    buildBidForm(bidurl);
}


$("#login").text(TEXT_LOGIN);
$("#signup").text(getTextById(61));
$("#registrationIcon").text(getTextById(61));
$("#login_username").attr("placeholder", getTextById(62));
$("#login_pass").attr("placeholder", getTextById(63));
$("#registrationEmail").attr("placeholder", getTextById(64));
setElemText(53);
setElemText(54);
setElemText(55);
setElemText(56);
setElemText(57);
setElemText(58);
setElemText(59);
setElemText(60);
setElemText(65);
setElemText(67);
showVersion();
// This must be a hidden field: for dovis and the normal module it is 8001
const appModule = 8001;

const hideMenu = function () {
    $("#settings").fadeOut();
    $("#showSubmitButton").fadeOut();
    $("#showUserUrlsButton").fadeOut();
    $("#logoutButton").fadeOut();
};
const hideLongUrl = function () {
    $("#longUrlText").hide();
    document.getElementById("longUrlText").removeAttribute("class");
}

const doPing = function () {
    // ping the server at least every 60 seconds. Otherwise the server will logout the user
    let ms = Date.now();
    const timeDiff = ms - lastPing;
    //console.info("ping, timeDIff",timeDiff);
    if (timeDiff > 60000) {
        const xhttp = new XMLHttpRequest();
        const pingUrl = `${getAPIServerPath()}${customerID}/${applicationID}/ping`;
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4) {
                console.info("ping:", timeDiff, pingUrl);
                const pingResponse = xhttp.responseText;
                if (xhttp.status > 199 && xhttp.status < 300) {
                    console.info("pingResponse after ", timeDiff, " milliseconds: ", pingResponse, xhttp.status);
                } else if (xhttp.status === 401) {
                    console.info("Timeout, pingResponse after ", timeDiff, " milliseconds: ", pingResponse, xhttp.status);
                    const blink=document.getElementById("main-content");
                    blink.innerHTML="";
                    blink.innerText=TEXT_LOGIN_PLEASE;
                    blink.style.visibility = "visible";
                    doLogout();
                } else console.error("Error in pingResponse after ", timeDiff, " milliseconds:", pingResponse, xhttp.status);
            }
        }
        xhttp.open("GET", pingUrl, true);
        xhttp.setRequestHeader("Authorization", `Bearer ${sessionToken}`);
        xhttp.send();
        lastPing = ms;
    }
}

let ping = function () {
};
const setLoggedInLayout = function () {
    console.info("------------- setLoggedInLayout -------------------")
    const userName = sessionStorage.getItem("dovisUser");
    $("#login_username").remove();
    $("#login_pass").remove();
    $("#signup").remove();
    $("#login").removeClass("doLogin");
    $("#login").removeClass("btn-primary");
    $("#login").addClass("btn-default");
    $("#create-newshortcut-div").hide();
    $("#login").html(``);
    $("#login").after(`&nbsp;<a class="btn btn-outline-primary click_usersettings" id ="settings" style="z-index: 2;"><i class="fa fa-user-circle">&nbsp;${userName}</i></a>&nbsp;`);
    $("#settings").before(`<a class="btn btn-outline-primary doLogout" id ="logoutButton"><i class="fa fa-power-off"></i></a>&nbsp;`);


    $("#settings").after(`&nbsp;<a class="btn btn-outline-primary click_usershortcuts" id ="showUserUrlsButton"><i class="fa fa-list-ul"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-outline-primary show_submit" id ="showSubmitButton"><i class="fa fa-plus-square icon-link"></i></a>&nbsp;`);
    $("#validationlabel").hide();
    $("#section_calltoaction").remove();
    $("#section_signup").remove();
    $("#createsubmit").removeClass("click_propose");
    $("#createsubmit").addClass("click_selecturlid");
    $("#slogan-container-div").remove();
    $("#text65").remove();
    $("#passwordForgotten").remove();
    lastPing = Date.now();
    ping = function () {
        if (sessionToken && sessionToken !== 'undefined') {
            doPing();
            console.info("ping-token", sessionToken);
        } else console.error("Cannot ping, sessionToken is undefined:", sessionToken);
    }

};

const setDovisVisible = function (arrayOrElement, elementIDsArray) {
    console.info("------------- setDovisVisible -------------------")
    const loadingElement = document.getElementById("loadingIcon");
    const h3 = document.getElementById("waitMessage");
    h3.style.visibility = "visible";
    h3.innerText = TEXT_LOADING;
    loadingElement.style.visibility = "visible";
    const isArray = Array.isArray(arrayOrElement);

    if (!isArray) $("#" + arrayOrElement).show();
    else {
        for (let i = 0; i < arrayOrElement.length; i++) {
            $("#" + arrayOrElement[i]).show();
        }
    }
    for (let i = 0; i < elementIDsArray.length; i++) {
        if ((isArray && !arrayOrElement.includes(elementIDsArray[i])) || (!isArray && arrayOrElement !== elementIDsArray[i])) $("#" + elementIDsArray[i]).hide();
    }
    h3.style.visibility = "hidden";

    loadingElement.style.visibility = "hidden";
};


const buildSettingsPage = function (userdata) {
    $("#settings-div").html("<h3>Loading ...</h3>");
    if (userdata) console.info("userdata", userdata);
    else {
        console.error("No userdata available");
        return;
    }
    const data = userdata[0]
    const roles = userdata[1];
    if (data.count && roles.count) console.log("clicked usersettings. userdata.length=" + data.count + " roles.length=" + roles.count);
    const last_login_data = Date.parse(data.rows[0].s[0]);
    const date = new Date(last_login_data);
    const last_login = date.toString();
    const username = data.rows[0].s[1];
    const first_name = data.rows[0].s[3];
    const last_name = data.rows[0].s[4];
    const email = data.rows[0].s[5];
    const userid = data.rows[0].s[7];
    console.log("roles: " + JSON.stringify(roles));
    for (let r = 0; r < roles.count; r++) {
        let role = roles.rows[r].s[0];
        if (role != 25 && role > USER_CATEGORY)
            USER_CATEGORY = roles.rows[r].s[0];
        console.log("role:" + role + " USER_CATEGORY:" + USER_CATEGORY);
    }


    if (USER_CATEGORY == 6) {
        USER_PRODUCT = "STANDARD"
    } else if (USER_CATEGORY == 7) {
        USER_PRODUCT = "PREMIUM"
    } else if (USER_CATEGORY == 8) {
        USER_PRODUCT = "ENTERPRISE"
    }
    let firstNamePlaceholder = getTextById(74);
    let lastNamePlaceholder = getTextById(75);
    let firstNameValue = ``;
    if (notEmpty(first_name)) firstNameValue = `value="${first_name}"`;
    let lastNameValue = ``;
    if (notEmpty(last_name)) lastNameValue = `value="${last_name}"`;
    currentDovisUserID = data.rows[0].s[7];
    currentUserID = currentDovisUserID;
    sessionStorage.setItem("dovisUserID", currentDovisUserID);
    sessionStorage.setItem(`listo_userID_${customerID}_${applicationID}`, currentDovisUserID);
    sessionStorage.setItem(`listo_${customerID}_${applicationID}_${currentDovisUserID}`, sessionToken);
    console.log("current dovis-userid:" + currentDovisUserID);
    const storedData = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
    let lastUrls = ``;
    if (storedData) {
        const last = Math.min(5, storedData.count);
        for (let r = 0; r < last; r++) {
            const urlStr = storedData.rows[r].s[2];
            const len = Math.min(85, urlStr.length);
            const idx = urlStr.lastIndexOf("\/\/") + 2;
            let urlSubstr = `${urlStr.substr(idx, len)}...`;

            lastUrls += `<a href="${APPLICATION_HOMEPAGE}/${storedData.rows[r].s[9]}" target="_blank">
                  <span class="badge badge-pill badge-primary">${storedData.rows[r].s[9]}</span>&nbsp;
                  <span class="badge badge-pill badge-outline">${urlSubstr}</span></a><br/>`;

        }
    }
    let mailings = ``;
    if (USER_CATEGORY > 5)
        mailings = ` <span class="badge badge-pill badge-light">
                              <i class="fa fa-users" aria-hidden="true"></i>
                              <i class="fa fa-envelope-square" aria-hidden="true"></i>
                              ${getTextById(87)}
                          </span>`;
    const html = ` <div class="container-lg">
                                <i class="fa fa-remove fa-2x close-user-settings" style="visibility:hidden" id="close-user-settings"></i>&nbsp;&nbsp;&nbsp;
                                <a id="save_edit" style="visibility:hidden;" class="btn btn-primary click_updateusersettings">
                                     <i class="fa fa-save"></i>
                                </a>
                                <i class="fa fa-edit edituser-settings text-right"> ${getTextById(76)}</i>
                     
                                    <div class="form-row rounded">
                                        <i class="fa fa-user"></i>&nbsp;
                                        <input id="edit-username" type="text" disabled value="${username}" class="rounded">
                                    </div>
                                    <div class="form-row rounded">
                                        <i class="fa fa-envelope"></i>
                                        <input id="edit-email" type="text" disabled value="${email}" class="rounded">
                                    </div>
                                    <div class="form-row rounded">
                                        <i class="fa fa-id-card-o"></i>
                                        <input id="edit-firstname" type="text" disabled ${firstNameValue} placeHolder="${firstNamePlaceholder}" class="rounded">
                                    </div>
                                    <div class="form-row rounded">
                                        <i class="fa fa-id-card-o"></i>
                                        <input id="edit-lastname" type="text" disabled ${lastNameValue} placeHolder="${lastNamePlaceholder}" class="rounded">
                                    </div>
                           
                                <div class="row">  
                                         <span class ="badge badge-pill badge-light"><i class="fa fa-id-badge"></i> &nbsp;${getTextById(79)}: &nbsp;${userid}</span>
                                         <span class ="badge badge-pill badge-light"><i class="fa fa-certificate"></i>&nbsp;${getTextById(80)}:${USER_PRODUCT}  &nbsp;</span>
                                         <div  style="padding: 0.5em;">
                                             <span class ="badge badge-pill badge-light"><i class="fa fa-handshake-o"></i>&nbsp;<a class="edit-plan" style="color:white;">${getTextById(81)}</a></span>
                                         </div>
                                         <!--div  class="chg-word" data-email="${email}" style="padding: 0.5em;">
                                              <span class="badge badge-pill badge-light">
                                                  <i class="fa fa-user-secret" aria-hidden="true"></i>
                                                  <i class="fa fa-pencil" aria-hidden="true"></i> 
                                                  ${getTextById(77)}
                                              </span>
                                         </div-->
                                         <div id="showCustomerEmailsList" class="showCustomerEmails" style="padding: 0.5em;">
                                            ${mailings}
                                         </div>
                                </div>
             
                      <div class="row">
                         <div class="dovis_small  border-light" id="lastCreated">
                            ${getTextById(73)}:
                            <br>
                           ${lastUrls}  
                         </div>
                         <div class="border-light" >
                              <span class="badge badge-pill badge-outline"><i class="fa fa-link show_submit fa-2x"></i></span>
                              <span class="badge badge-pill badge-outline"><i class="fa fa-list-ul click_usershortcuts fa-2x"></i></span>
                         </div>
                      </div>
                         
               
                        <HR>
                        <div class="dovis_small">
                            ${getTextById(72)}:${last_login} <br/>
                            Dovis-Version: ${DOVIS_VERSION}
                        </div>
                    </div>`;
    return html;
}
/*-------------------   start build page      ------------------------*/
$("#section_signup").hide();
$("#section_calltoaction").hide();
$("#navigation").hide();
$("#header").hide();
let currentDovisUserID;


$("#text_ready_getstarted").text(TEXT_READY_GET_STARTED);


$("#longurl_input").prop("placeholder", TEXT_YOUR_LONG_URL);
hideLongUrl();


$("#slogan").text(TEXT_SLOGAN);
$("#createsubmit").html(`<i class="fa fa-random fa-lg"></i>&nbsp;${TEXT_CREATE}`);


if (isLoggedIn()) {
    setLoggedInLayout();
    //pageRefreshed = true;
    $("#showUserUrlsButton").click();
    //pageRefreshed = false;
} else {
    $("#login_username").hide();
    $("#login_pass").hide();
}

$("#header").fadeIn(750);
$("#navigation").show();
$("#section_calltoaction").show();
$("#section_signup").show();

/*-------------------   end of build page      ------------------------*/

$(document).on('click', '.doRegistration', function () {
        let responseObject = {errorMnemonic: ""};
        $("#registrationMessage").attr("style", "visibility: hidden");
        const userEmail = $("#registrationEmail").val();
        if (validateEmail(userEmail)) {
            console.log(`Start registration of user with Email ${userEmail}`);
            $("#registrationIcon").attr("style", "visibility: visible;");
            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope"></i></span>&nbsp;${TEXT_SENDING_EMAIL}`);
            blink("registrationButton", 250, 5);
            blink("registrationEmail", 250, 5);
            const xhttp = new XMLHttpRequest();
            const postUrl = `${getAPIServerPath()}0/0/customer/asktoregister/${customerID}/${applicationID}`;
            xhttp.open("POST", postUrl, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState === 4) {
                    try {
                        console.log("xhttp.status=" + xhttp.status);
                        responseObject = JSON.parse(xhttp.responseText);
                        console.log("responseObject: " + responseObject);
                        if (xhttp.status > 0) {
                            if (xhttp.status <= 204) {
                                $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-check-circle"></i></span>${TEXT_EMAIL_WAS_SENT}`);
                                $("#text_ready_getstarted").html(`<span><i class="fa fa-envelope-open"></i></span>${TEXT_CHECK_YOUR_EMAIL}`);
                                blink("text_ready_getstarted", 500, 3);
                            } else if (xhttp.status === 422) {
                                if (responseObject.errorMnemonic.startsWith("asktoregister.3")) {
                                    $("#text_ready_getstarted").text(TEXT_EMAIL_ALREADY_REGISTERED);
                                    $("#registrationButton").text(`${TEXT_INVALID_EMAIL} `);
                                } else if (responseObject.errorMnemonic.toString().startsWith("asktoregister.7")) {
                                    $("#registrationButton").text(`Failed to send eMail to ${userEmail}`);
                                }
                            } else if (xhttp.status >= 400) {
                                $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                            }
                        } else {
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                            $("#registrationMessage").text(`${TEXT_SERVER_UNREACHABLE}`);
                            $("#registrationMessage").attr("style", "visible");
                        }
                    } catch
                        (error) {
                        console.error(error);
                        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                    }
                }
            };
            const activationStart = encodeURIComponent(`${TEXT_REGISTER_MAIL_BODY_START}<a href="${CALLBACKPAGE_USER_REGISTRATION}?register=REGISTRATION_VALIDATION_CODE&cr1=CRVALUE1&cr2=CRVALUE2&mail=EMAIL">`);
            const activationEnd = encodeURIComponent(`</a>${TEXT_REGISTER_MAIL_BODY_END}`);
            console.log("activationStart: " + activationStart);
            console.log("activationEnd: " + activationEnd);
            const body = `email=${userEmail}&mailbodymessagestart=${activationStart}&module=${appModule}&mailbodymessageend=${activationEnd}&mailsubjectmessage=${TEXT_REGISTER_MAIL_SUBJECT}&callbackpage=${CALLBACKPAGE_USER_REGISTRATION}&jsonurl=${JSON_HTML_URL}`;
            console.log("body: " + body);
            xhttp.send(body);
        } else {
            console.error("invalid email");
            $("#registrationMessage").attr("style", "visible");
            $("#registrationMessage").text(TEXT_INVALID_EMAIL);
        }
    }
)
;
let mandatoryWarningscreated = false;
let passwordValid = false;
let registrationIsConfirmed = false;

$(document).on('click', '.confirmRegistration', function () {
    const pattern = $("#newpassword").attr("pattern");
    const reg = new RegExp(pattern);
    let responseObject = {errorMnemonic: "", message: "", httpStatus: -1};
    const userEmail = $("#registrationEmail").val();
    const username = $("#newusername").val();
    const confirmpassword = $("#confirmpassword").val();
    const password = $("#newpassword").val();
    const cr = $("#challengeresponse").val();
    const code = $("#validationcode").val();
    const addedRole = $("#applicationAdditionalUserRole").val();
    $("#text_ready_getstarted").text(``);
    if (!mandatoryWarningscreated) {
        $("#newusername").after(`<label id="mandatory_username" class="badge badge-info"></label>`);
        $("#newpassword").after(`<label id="mandatory_pw" class="badge badge-info"></label>`);
        $("#confirmpassword").after(`<label id="mandatory_pwconfirm" class="badge badge-info"></label>`);
        $("#challengeresponse").after(`<label id="mandatory_cr" class="badge badge-info"></label>`);
        $("#validationcode").after(`<label id="mandatory_code" class="badge badge-info"></label>`);
        mandatoryWarningscreated = true;
    }
    if (!username) {
        $('#mandatory_username').text(TEXT_MANDATORY_FIELD);
        blink("mandatory_username", 250, 3);
    } else {
        $("#mandatory_username").text(``);
    }
    if (!password) {
        $("#mandatory_pw").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_pw", 250, 3);
    } else {
        $("#mandatory_pw").text(``);
    }
    if (!confirmpassword) {
        $("#mandatory_pwconfirm").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_pwconfirm", 250, 3);
    } else {
        $('#mandatory_pwconfirm').text(``);
    }
    if (!cr) {
        $("#mandatory_cr").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_cr", 250, 3);
    } else {
        $("#mandatory_cr").text(``);
    }
    if (!code) {
        $("#mandatory_code").attr("style", "visibility:visible");
        $("#mandatory_code").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_code", 250, 3);
    } else {
        $("#mandatory_code").text(``);
    }
    let pswMatch = false;
    const psw = $("#newpassword").val();
    const cpsw = $("#confirmpassword").val();
    if (psw.toString().normalize() === cpsw.toString().normalize()) {
        pswMatch = true;
        if (confirmpassword) {
            $("#mandatory_pwconfirm").text(``);
        }
    } else if (passwordValid === true) {
        pswMatch = false;
        console.error("Passwords do not match");
        $("#mandatory_pwconfirm").text(TEXT_PASSWORDS_DO_NOT_MATCH);
    }
    let patternMatch = false;
    if (!reg.test(password)) {
        $("#text_ready_getstarted").text(TEXT_PASSWORD_CRITERIA);
    } else {
        patternMatch = true;
    }

    if (registrationIsConfirmed === true) {
        blink("registrationButton", 250, 5);
    } else if (userEmail && code && cr > 0 && username && password && confirmpassword && pswMatch === true && passwordValid === true && patternMatch === true) {
        $("#challengequestion").hide();
        $("#newusername").attr("disabled", "true");
        $("#newpassword").attr("disabled", "true");
        $("#confirmpassword").attr("disabled", "true");
        $("#validationcode").attr("disabled", "true");
        $("#challengeresponse").attr("disabled", "true");
        $("#newpassword").fadeOut();
        $("#confirmpassword").fadeOut();
        $("#validationcode").fadeOut();
        $("#challengeresponse").fadeOut();
        $("#label_validation_code").hide();
        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope"></i></span>&nbsp;${TEXT_SENDING_EMAIL}`);
        blink("registrationButton", 250, 5);
        console.log(`Start confirming registration of user with Email "${userEmail}and role ${addedRole}`);
        const xhttp = new XMLHttpRequest();
        const postUrl = `${getAPIServerPath()}0/0/customer/register/${code}`;

        xhttp.open("POST", postUrl, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        let xhttpState = -1;

        xhttp.onreadystatechange = function () {
            xhttpState = xhttp.readyState;
            if (xhttpState === 4) {
                try {
                    if (xhttp.status > 0) {
                        if (xhttp.status <= 204) {
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope-open"></i></span>&nbsp;${TEXT_EMAIL_WAS_SENT}`);
                            registrationIsConfirmed = true;
                            $("#text_ready_getstarted").fadeIn();
                            $("#text_ready_getstarted").text(getTextById(32));
                            blink("text_ready_getstarted", 500, 3);
                        } else {
                            //TODO distinguish http403 and others
                            responseObject = JSON.parse(xhttp.responseText);
                            $("#text_ready_getstarted").fadeIn();
                            $("#text_ready_getstarted").text(`HTTP${responseObject.httpStatus}: ${responseObject.message}`);
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                        }
                    } else {
                        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                        $("#registrationMessage").text(`${TEXT_SERVER_UNREACHABLE}`);
                        $("#registrationMessage").attr("style", "visible");
                    }
                } catch (error) {
                    console.error(error);
                    $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-triangle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                }
            }
        };
        //userEmail && code && cr > 0 && username && password && confirmpassword && pswMatch === true && passwordValid === true
        xhttp.send(`username=${username}&pass=${password}&cr=${cr}&callbackpage=${CALLBACKPAGE_USER_REGISTRATION}
                          &module=${appModule}&mailSubject=${TEXT_EMAIL_CONFIRM_SUBJECT}&jsonurl=${JSON_HTML_WELCOME_URL}
                          &mailbodystart=${TEXT_EMAIL_CONFIRM_START}&mailbodyend=${TEXT_EMAIL_CONFIRM_END}&customerrole=${addedRole}`);
    }
});

$(document).on('focusin', '.passwordCheck', function () {
    $("#passwordhelp").fadeIn();
});

$(document).on('focusout', '.passwordCheck', function () {
    $("#passwordhelp").fadeOut();
});


$(document).on('change paste keyup', '.passwordCheck', function () {
    $("#registrationMessage").attr("style", "visibility: hidden");
    let hasLowerCase = false;
    let hasUpperCase = false;
    let hasNumber = false;
    let minLength = false;

    const valid = "fa-check";
    const invalid = "fa-user-secret";
    let myInput = document.getElementById("newpassword");
    let letter = document.getElementById("letter");
    let capital = document.getElementById("capital");
    let number = document.getElementById("number");
    let length = document.getElementById("length");
    let lowerCaseLetters = /[a-z]/g;
    if (myInput.value.match(lowerCaseLetters)) {
        letter.classList.remove(invalid);
        letter.classList.add(valid);
        hasLowerCase = true;
    } else {
        letter.classList.remove(valid);
        letter.classList.add(invalid);
        hasLowerCase = false;
    }

    // Validate capital letters
    let upperCaseLetters = /[A-Z]/g;
    if (myInput.value.match(upperCaseLetters)) {
        capital.classList.remove(invalid);
        capital.classList.add(valid);
        hasUpperCase = true;
    } else {
        capital.classList.remove(valid);
        capital.classList.add(invalid);
        hasUpperCase = false;
    }

    // Validate numbers
    let numbers = /[0-9]/g;
    if (myInput.value.match(numbers)) {
        number.classList.remove(invalid);
        number.classList.add(valid);
        hasNumber = true;
    } else {
        number.classList.remove(valid);
        number.classList.add(invalid);
        hasNumber = false;
    }

    // Validate length
    if (myInput.value.length >= 8) {
        length.classList.remove(invalid);
        length.classList.add(valid);
        minLength = true;
    } else {
        length.classList.remove(valid);
        length.classList.add(invalid);
        minLength = false;
    }
    if (hasLowerCase === true && hasUpperCase === true && hasNumber === true && minLength === true) {
        passwordValid = true;
        $("#passwordhelp").fadeOut();
    } else {
        passwordValid = false;
        $("#passwordhelp").fadeIn();
    }
});


$(document).on('click', '.showloginfields', function () {
    $("#login_username").attr("style", "visibility: visible;");
    $("#login_pass").attr("style", "visibility: visible;");
    $("#signup").hide();
    $("#login").removeClass("showloginfields");
    $("#login").addClass("doLogin");
    $("#text65").hide();
    $("#passwordForgotten").hide();
});


let hintIsActive = false;

let hintLogin = `<span class="badge badge-info dovis" id="blinking_login_message">${TEXT_MANDATORY_FIELD}</span>`;
let hintPass = `<span class="badge badge-info dovis" id="blinking_pass_message">${TEXT_MANDATORY_FIELD}</span>`;
$("#login_username").after(hintLogin);
$("#login_pass").after(hintPass);

$("#blinking_login_message").hide();
$("#blinking_pass_message").hide();

$(document).on('click', '.doLogin', function () {
    const passWelement = document.getElementById("login_pass");
    passWelement.addEventListener("input", function () {
        $("#passwordForgotten").hide();
    });

    const theUserName = $("#login_username").val();
    const pass = $("#login_pass").val();
    const loginMessage = function (status, msg) {
        console.log("login-status=" + status + ": " + msg);
        if (status == 400 || status == 401 || status == 403) {
            $("#text65").show();
            $("#passwordForgotten").show();
        }
        if (!hintIsActive) {
            $("#login_username").after(hintLogin);
            $("#login_pass").after(hintPass);
            hintIsActive = true;
        }
        $("#blinking_login_message").text(msg);
        $("#blinking_pass_message").text(msg);
        blink("blinking_login_message", 100, 3);
        blink("blinking_pass_message", 100, 3);
        blink("login_username", 150, 3);
        blink("login_pass", 150, 3);
    };

    if (!theUserName) {
        loginMessage(0, TEXT_MANDATORY_FIELD);
    } else if (!pass) {
        loginMessage(0, TEXT_MANDATORY_FIELD);
    } else {
        const wMesg = document.getElementById("waitMessage");
        wMesg.style.visibility = "visible";
        const pre_login_html = $("#login").html();
        $("#login").html(`<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>&nbsp;${theUserName}`);
        const xhttp = new XMLHttpRequest();
        const postUrl = `${getAPIServerPath()}${customerID}/${applicationID}/${appModule}/login`;
        console.log("postUrl", postUrl);
        let jsonResponse = {bearerToken: null};
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4) {
                $("#login").html(pre_login_html);
                if (xhttp.status > 0 && xhttp.status <= 204) {
                    isPublicUser = false;
                    $("#passwordHelp").remove();
                    jsonResponse = JSON.parse(xhttp.responseText);
                    sessionToken = jsonResponse.bearerToken.split(";")[0];
                    // we need userEmail for payment checkout:
                    const userEmail = jsonResponse.bearerToken.split(";")[1];
                    sessionStorage.setItem("dovisUser", theUserName);
                    sessionStorage.setItem("dovisUserEmail", userEmail);
                    sessionStorage.setItem("listo_" + customerID + "_" + applicationID + "_" + currentDovisUserID, sessionToken);
                    $("#blinking_login_message").remove();
                    $("#blinking_pass_message").remove();
                    setLoggedInLayout();


                    $("#showUserUrlsButton").click();
                    $("#settings").click();
                    $("#showSubmitButton").click();

                    document.getElementById("main-content").addEventListener("mousemove", ping);
                    document.getElementById("main-content").addEventListener("click", ping);

                } else if (xhttp.status >= 400) {
                    if (xhttp.status === 400) {
                        loginMessage(xhttp.status, `HTTP400 ${TEXT_LOGIN_NO_AUTHENTICATION}`);
                    } else if (xhttp.status === 401) {
                        loginMessage(xhttp.status, TEXT_LOGIN_NO_AUTHENTICATION);
                    } else if (xhttp.status === 403) {
                        loginMessage(xhttp.status, `HTTP403 ${TEXT_LOGIN_NO_AUTHORIZATION}`);
                    } else {
                        loginMessage(xhttp.status, `${xhttp.status}: ${TEXT_LOGIN_SERVER_ERROR}`);
                    }
                } else {
                    loginMessage(xhttp.status, TEXT_LOGIN_ERROR);
                }

                wMesg.style.visibility = "hidden";
            }
        };
        xhttp.open("POST", postUrl, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("username=" + theUserName + "&password=" + pass);
    }
})
;


const doLogout = function () {
    const logoutPath = `${getAPIServerPath()}${customerID}/${applicationID}/logout`;
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    const bearerString = sessionToken;
    //sessionStorage.getItem("listo_" + customerID + "_" + applicationID + "_" + role);

    xhttp.send("token=" + bearerString);
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (xhttp.status > 0 && xhttp.status <= 204) {
                sessionStorage.removeItem("dovisUser");
                sessionStorage.removeItem("dovisUserID");
                sessionStorage.removeItem("currentLanguage");
                sessionStorage.removeItem("selectedServer");
                sessionStorage.removeItem("dovisUserEmail");
                sessionStorage.removeItem("usershortcuts");
                sessionStorage.removeItem("currentLanguage");
                // This is the session key(=bearer-key):
                sessionStorage.removeItem(`listo_${customerID}_${applicationID}_${currentDovisUserID}`);
                sessionStorage.removeItem(`listo_userID_${customerID}_${applicationID}`);
                window.open(APPLICATION_HOMEPAGE, "_self");
            } else {
                $("#logoutIcon").text(`HTTP${this.status}`);
                $("#logoutIcon").removeClass("fa-sign-out");
                $("#logoutIcon").addClass("fa-exclamation");
            }
        }
    };
}
$(document).on('click', '.doLogout', doLogout);


$(document).on('click', '.reset_pw', function () {
    $("#registrationIcon").text(TEXT_RESET_PASSWORD_QUESTION);
    $("#text_ready_getstarted").html(`<i class="fa fa-envelope">${TEXT_ENTER_YOUR_EMAIL}</i>`);
    location.href = "#registrationEmail";
    $("#registrationEmail").attr("placeholder", TEXT_ENTER_YOUR_EMAIL);
    $("#registrationButton").removeClass("doRegistration");
    $("#registrationButton").addClass("sendNewPass");
});

$(document).on('click', '.sendNewPass', function () {
    const subject = "dovis.it";
    const html = `<html><br><br><br><a href="${APPLICATION_HOMEPAGE}/?resetpassw=RESET_LINK&user=USERNAME_OR_EMAIL">
            ${TEXT_RESET_PASSWORD_QUESTION}</a>
            <br><img src="${APPLICATION_HOMEPAGE}/img/${APPLICATION_LOGO}"></body></html>`;
    const email = $("#registrationEmail").val();
    if (notEmpty(email)) {
        $("#registrationMessage").attr("style", "visibility:hidden");
        $("#registrationEmail").attr("placeholder", "");
        $("#registrationMessage").text("");
        const path = `${getAPIServerPath()}0/0/customer/askresetpw/${customerID}/${applicationID}/${appModule}`;
        const xhttp = new XMLHttpRequest();
        xhttp.open("POST", path, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(`username=${encodeURIComponent(email)}&mailSubject=${encodeURIComponent(subject)}&mailbody=${encodeURIComponent(html)}`);
        let textReadyGetstarted = $("#text_ready_getstarted");
        $("#registrationIcon").html(`<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>`);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4) {
                if (xhttp.status > 0 && xhttp.status <= 204) {
                    textReadyGetstarted.text(TEXT_EMAIL_WAS_SENT);
                    $("#registrationIcon").html(`<i class="fa fa-envelope-open fa-2x"></i>`);
                    $("#registrationEmail").attr("disabled", true);
                    $("#registrationButton").attr("disabled", true);
                } else if (xhttp.status === 422) {
                    textReadyGetstarted.text(TEXT_SORRY + ". " + TEXT_INVALID_EMAIL);
                    $("#registrationIcon").html(`<i class="fa fa-exclamation-triangle fa-2x"></i>`);
                }
            } else {
                textReadyGetstarted.text(`${TEXT_SERVER_ERROR} HTTP${xhttp.status}`);
                $("#registrationIcon").html(`<i class="fa fa-exclamation-triangle fa-2x"></i>`);
            }
        }
    } else {
        console.error("No email");
        blink("registrationEmail", 100, 3);
        blink("registrationMessage", 100, 3);
        $("#registrationMessage").text(TEXT_INVALID_EMAIL);
        $("#registrationMessage").attr("style", "visibility:visible");
        $("#registrationEmail").attr("placeholder", TEXT_ENTER_YOUR_EMAIL);
    }
})
;


$(document).on('click', '.setNewPass', function () {
    const pattern = $("#newpassword").attr("pattern");
    const reg = new RegExp(pattern);
    const pass = $("#newpassword").val();
    const passConfirm = $("#confirmpassword").val();
    const subject = `dovis.it: ${TEXT_PASSWORD_WAS_CHANGED}`;
    const html = `<html><br><br><br><a href="${APPLICATION_HOMEPAGE}>
            PASSCHANGED_TEXT</a>
            <br><img src="${APPLICATION_HOMEPAGE}/img/${APPLICATION_LOGO}"></body></html>`;
    if (!notEmpty(pass) || !notEmpty(passConfirm)) {
        blink("newpassword", 100, 3);
        blink("confirmpassword", 100, 3);
        $("#registrationMessage").text(TEXT_MANDATORY_FIELD);
        $("#registrationMessage").attr("style", "visibility: visible");
    } else if (pass !== passConfirm) {
        blink("newpassword", 100, 3);
        blink("confirmpassword", 100, 3);
        $("#registrationMessage").text(TEXT_PASSWORDS_DO_NOT_MATCH);
        $("#registrationMessage").attr("style", "visibility: visible");
    } else if (!reg.test(pass)) {
        $("#registrationMessage").text(TEXT_PASSWORD_CRITERIA);
        $("#registrationMessage").attr("style", "visibility: visible");
    } else {
        const code = $(this).data("code");
        const user = $(this).data("user");
        const path = `${getAPIServerPath()}0/0/customer/resetpw/${applicationID}/${customerID}/${appModule}/${user}/${code}`;
        const xhttp = new XMLHttpRequest();
        xhttp.open("POST", path, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(`newpass=${pass}&emailBody=${encodeURIComponent(html)}&emailSubject=${encodeURIComponent(subject)}&infotext=${encodeURIComponent(TEXT_PASSWORD_WAS_CHANGED)}`);
        let textReadyGetstarted = $("#text_ready_getstarted");
        $("#registrationIcon").html(`<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>`);
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4) {
                if (xhttp.status > 0 && xhttp.status <= 204) {
                    textReadyGetstarted.text(`${TEXT_PASSWORD_WAS_CHANGED}. ${TEXT_EMAIL_WAS_SENT}`);
                    $("#registrationIcon").html(`<i class="fa fa-envelope-open fa-2x"></i>`);
                    $("#registrationEmail").attr("disabled", true);
                    $("#registrationButton").remove();
                    $("#newpassword").remove();
                    $("#confirmpassword").remove();
                } else {
                    textReadyGetstarted.text(`${TEXT_SERVER_ERROR} HTTP${xhttp.status}`);
                    $("#registrationIcon").html(`<i class="fa fa-exclamation-triangle fa-2x"></i>`);
                }
            }
        };
        $("#registrationMessage").attr("style", "visibility: hidden");
    }
})
;

$(document).on('click', '.close-user-settings', function () {
    $("#close-user-settings").attr("style", "visibility:hidden");
    $("#edit-username").attr("disabled", true);
    $("#edit-email").attr("disabled", true);
    $("#edit-firstname").attr("disabled", true);
    $("#edit-lastname").attr("disabled", true);
    $("#save_edit").attr("style", "visibility:hidden;");
});


$(document).on('click', '.edituser-settings', function () {
    $("#close-user-settings").attr("style", "visibility:visible");
    $("#edit-username").removeAttr("disabled");
    //$("#edit-email").removeAttr("disabled");
    $("#edit-firstname").removeAttr("disabled");
    $("#edit-lastname").removeAttr("disabled");
    $("#save_edit").attr("style", "visibility:visible;");
});

$(document).on('click', '.chg-word', function () {
    $("#newpassword").remove();
    $("#confirmpassword").remove();
    $("#registrationButton").remove();
    $("#registrationMessage").remove();
    $("#text_ready_getstarted").remove();
    $("#closechgword").remove();
    const email = $(this).data("email");
    //console.log("key:" + sessionStorage.getItem("listo_" + customerID + "_" + applicationID + "_" + role));
    //const code = sessionStorage.getItem("listo_" + customerID + "_" + applicationID + "_" + role);
    //const code=sessionStorage.getItem(`listo_${customerID}_${applicationID}_${currentUserID}`);
    $(this).after(`<i id="closechgword" class="fa fa-remove close-chgword fa-2x"></i><br><label id="text_ready_getstarted"></label>
                     <input id="newpassword" class="form-control form-control-lg" type="password" 
                     pattern=${"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"}>
                   <input id="confirmpassword" class="form-control form-control-lg" type="password">
         <a class="btn btn-primary setNewPass" data-user="${email}" data-code="${sessionToken}"
          id="registrationButton"><i id="registrationIcon" class="fa fa-save fa-1x"></i></a>
         <label id="registrationMessage">${TEXT_PASSWORD_CRITERIA}</label>`);
});
$(document).on('click', '.close-chgword', function () {
    $("#newpassword").remove();
    $("#confirmpassword").remove();
    $("#registrationButton").remove();
    $("#registrationMessage").remove();
    $("#text_ready_getstarted").remove();
    $("#closechgword").remove();
});

$(document).on('click', '.edit-plan', function () {
    console.log("clicked: edit-plan");
    $("#subscriptionplan").remove();
    let check5 = ``, check6 = ``, check7 = ``, check8 = ``;
    if (USER_CATEGORY === 5) {
        check5 = "checked"
    } else if (USER_CATEGORY === 6) {
        check6 = "checked"
    } else if (USER_CATEGORY === 7) {
        check7 = "checked"
    } else if (USER_CATEGORY === 8) {
        check8 = "checked"
    }
    const subscriptionPlan = `<div id="subscriptionplan" class="click_editplan text-center border rounded">
          <a href="${APPLICATION_HOMEPAGE}"><i class="fa fa-remove  fa-2x"></i></a><br>
           <div class="form-check rounded dovis" style="padding: 1.5em;">
               <input class="form-check-input" type="radio" name="subscriptions" id="subscription1" ${check5} value="FREE">FREE<br>
               <input class="form-check-input" type="radio" name="subscriptions" id="subscription2" ${check6} value="STANDARD">STANDARD<br>
               <input class="form-check-input" type="radio" name="subscriptions" id="subscription3" ${check7} value="PREMIUM">PREMIUM<br>
               <input class="form-check-input" type="radio" name="subscriptions" id="subscription3" ${check8} value="ENTERPRISE">ENTERPRISE<br><br>
               <h4 id="selected_subscription"> THIS PAGE IS IN DEVELOPMENT. COMING SOON ...</h4>
               <a class="btn btn-primary update_subscription"><i class="fa fa-save"></i></a>
          </div>
      </div>`;
    hideMenu();
    $("#main-content").html(``);
    $("#main-content").after(subscriptionPlan);
});

$(document).on('click', '.update_subscription', function () {
    const subscription = $("input[name='subscriptions']:checked").val();
    $("#selected_subscription").text(subscription);
});


$(document).on('click', '.cms', function () {
    const pageName = $(this).data('page');
    const pageHtml = cmsContent[pageName];
    $("#cms-div").html(pageHtml);

    document.getElementById("footer-showJS").innerHTML = ``;
});

$(document).on('click', '.cmsElement', function () {
    const elementName = $(this).data('element');
    const elementHtml = cmsContent[elementName];
    $(this).html(elementHtml);
    document.getElementById("footer-showJS").innerHTML = ``;
});

const buildMailAccountsList = function () {

    return `<a class="click_usersettings">
          <i class="fa fa-remove  fa-2x"></i></a><br>
           <div class="form-check rounded dovis_small" style="padding: 1.5em;width:100%;">
           <div id="uploadCSVPreview" class="container border border-light" style="width:100%;height:125px; overflow-x:auto;overflow-y:auto;">
             ${TEXT_EXPLAIN_UPLOAD_FILE} ${Math.floor(MAXIMUM_ALLOWED_FILESIZE_CSV / 1000)} Kb
           </div> 
           <i id="uploadClock" class="fa fa-file fa-2x"></i> 
            <br> 
            <i class="fa fa-upload  fa-2x"> <input type="file" id="emailCSV" /></i>   <p id="csvFileInfo"></p> 
            <br> 
            <b id="columnSelection" style="color: #0d6efd; visibility: hidden">${TEXT_SELECT_EMAIL_FIRSTNAME_LASTNAME}</b>
            <br><span id="showSelectedColumns" style="visibility: hidden;">
           <i class="fa fa-at"></i><i class="fa fa-envelope-o"></i>&nbsp; &nbsp; &nbsp;<b id="email-column"></b><br>
            ${getTextById(74)}:&nbsp; &nbsp; &nbsp;<b id="name-column"></b><br>
            ${getTextById(75)}:&nbsp; &nbsp; &nbsp;<b id="lastname-column"></b></span><br>
          
            <select id="emailColumnSelect" style="visibility: hidden;background: #0d6efd;color: #ffffff">
            </select>
            <select id="firstNameColumnSelect" style="visibility: hidden;background: #0d6efd;color: #ffffff">
            </select>
            <select id="lastNameColumnSelect" style="visibility: hidden;background: #0d6efd;color: #ffffff">
            </select>
           
            <p id="showHeader">  </p>  <b id="showHeaderIconInfo"></b>
            <hr>
            <i id="uploadFileIcon" class="fa fa-cloud-upload fa-2x start-upload" data-iduser=${currentDovisUserID} style="color: #0d6efd; visibility: hidden">${getTextById(92)}</i>
            <div id="csvImport"></div>
          </div> `
        ;
}
const footerElement = document.getElementById("footer");
footerElement.innerHTML = cmsContent.footer;
footerElement.removeAttribute("class");

function clearCampaignResults() {
    document.getElementById("mailCampaignResult").innerHTML = ``;
    setDovisVisible(["shortcutlistfunctions", "urllist-div"], dovisNavigationIDs);
}