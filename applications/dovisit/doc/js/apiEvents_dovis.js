// Paste here all the selected events you need for your project
// and change them according to your project-requirements
let ENCODING;
const httpErrorCallBack = function (err) {
    try {
        if (err) showHttpErrorMessage("dovis-errors", err);
    } catch (err1) {
        try {
            showHttpErrorMessage("dovis-errors", JSON.parse(err));
        } catch (err2) {
            $("#dovis-errors").html(TEXT_SERVER_ERROR);
        }
    }
}
const createInvalidUrlMessage = function (stringOfUrl) {
    if (!stringOfUrl.startsWith("http")) {
        return `<p class="badge-danger">http://* | https://* </p>
                               <p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    } else {
        return `<p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    }
};
/***************************************
 CALL-ID: dovisto.User.apiCall10 (createwoatt)
 Description: Save a new combination shortcut/url without url-attributes
 ***************************************/
$(document)
    .on(
        'click',
        '.click_createwoatt',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idcategory = $(this).data("idcategory");
            const shortcut = $(this).data("shortcut");
            let taglist = $(this).data("taglist");
            const urlstring = $(this).data("urlstring");

            if (!idcategory) {
                console.error(`Variable 'idcategory' in object with id ${objectId} has no value.`);
            }

            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }

            if (!taglist) {
                //console.error(`Variable 'taglist' in object with id ${objectId} has no value.`);
                taglist = null;
            }

            if (!urlstring) {
                console.error(`Variable 'urlstring' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idcategory && shortcut && urlstring) {
                $("#loadingIcon").attr("style", "visibility:visible")
                newItemAdded = true;
                dataObject = {
                    "idcategory": idcategory,
                    "shortcut": shortcut,
                    "taglist": taglist,
                    "urlstring": urlstring
                };
                functionForProject_dovisto(objectId, dataObject, "user/createwoatt", "put", "url");
            }
        }
    );

$(document)
    .on(
        'click', '.analyze_new_url', function () {
            console.log("analyze_new_url was clicked ... ");
            const longUrl = $("#longurl_input").val().trim();
            if (!longUrl) {
                $("#validationlabel").text(TEXT_MANDATORY_FIELD);
                $("#validationlabel").show();
            } else if (checkUrl.test(longUrl)) {
                console.log("Analyzing " + longUrl + " ... ");
                analyzeRequestedPage(longUrl, function (data) {
                    let icons = data['icons'];
                    for (let i = 0; i < icons.length; i++)
                        console.log("icon " + i + ": " + icons[i].href);
                }, function (data) {
                    console.log("meta-tags: " + data["meta-tags"]);
                });
                $("#validationlabel").hide();
            } else {
                $("#validationlabel").text(TEXT_INVALID_URL);
                $("#validationlabel").show();
            }
        });

/***************************************
 CALL-ID: dovisto.User.apiCall1 (create)
 Description: Create a new shortcut for a new  url
 ***************************************/
$(document)
    .on(
        'click',
        '.click_create',
        function () {
            $("#shortCut_taglist").attr("placeholder", TEXT_FILL_SOME_TAGS);
            blink("shortCut_taglist", 50, 5);
            blink("shortCut_taglist_icon", 50, 5);
            const table = $("#tableForSubmittingNewShortcut");
            const taglistField = $("#shortCut_taglist");
            if (table) table.remove();
            if (taglistField) taglistField.remove();
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const articleabstract = $(this).data("articleabstract");
            const author = $(this).data("author");
            const description = $(this).data("description");
            const icon = $(this).data("icon");
            const idcategory = $(this).data("idcategory");
            const image1 = $(this).data("image1");
            const image2 = $(this).data("image2");
            const image3 = $(this).data("image3");
            const keywords = $(this).data("keywords");
            const shortcut = $(this).data("shortcut");
            const subject = $(this).data("subject");
            const summary = $(this).data("summary");
            const taglist = $(this).data("taglist");
            const urlstring = $(this).data("urlstring");
            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }
            if (!urlstring) {
                console.error(`Variable 'urlstring' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (shortcut && urlstring) {
                dataObject = {
                    "articleabstract": articleabstract,
                    "author": author,
                    "description": description,
                    "icon": icon,
                    "idcategory": idcategory,
                    "image1": image1,
                    "image2": image2,
                    "image3": image3,
                    "keywords": keywords,
                    "shortcut": shortcut,
                    "subject": subject,
                    "summary": summary,
                    "taglist": taglist,
                    "urlstring": urlstring
                };
            }
            functionForProject_dovisto(objectId, dataObject, "user/create", "put", "url");

        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall2 (createselected)
 Description: Create a new shortcut for an existing url
 ***************************************/
$(document)
    .on(
        'click',
        '.click_createselected',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idcategory = $(this).data("idcategory");
            const idurl = $(this).data("idurl");
            const shortcut = $(this).data("shortcut");
            let taglist = $(this).data("taglist");

            if (!idcategory) {
                console.error(`Variable 'idcategory' in object with id ${objectId} has no value.`);
            }

            if (!idurl) {
                console.error(`Variable 'idurl' in object with id ${objectId} has no value.`);
            }

            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }

            if (!taglist) {
                taglist = null;
                //console.error(`Variable 'taglist' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idcategory && idurl && shortcut) {
                const urlString = $("#longurl_input").val();
                analyzeRequestedPage(idurl, urlString, function (icons) {
                        if (icons.statuscode && icons.statuscode >= 400) {
                            console.error("icons.statuscode: " + icons.statuscode);
                        }
                    },
                    function (meta) {
                        if (meta.statuscode && meta.statuscode >= 400) {
                            console.error("meta.statuscode: " + meta.statuscode);
                        }
                    }, function (err) {
                        console.error(err);
                    });
                dataObject = {
                    "idcategory": idcategory,
                    "idurl": idurl,
                    "shortcut": shortcut,
                    "taglist": taglist
                };
                functionForProject_dovisto(objectId, dataObject, "user/createselected", "put");
            }
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall3 (checkifshortcutexists)
 Description: Check if a shortcut already exists
 ***************************************/
$(document)
    .on(
        'click',
        '.click_checkifshortcutexists',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const shortcut = $(this).data("shortcut");

            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (shortcut) {
                dataObject = {
                    "shortcut": shortcut
                };
            }
            functionForProject_dovisto(objectId, dataObject, "checkifshortcutexists", "post");
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall4 (newshortcutkey)
 Description: Select the last key of the shortcut-table
 ***************************************/
$(document)
    .on(
        'click',
        '.click_newshortcutkey',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let dataObject = {};
            const longUrl = $("#longurl_input").val().trim();
            const vl = $("#validationlabel");
            //const lut = $(`#longUrlText`);
            vl.hide();
            vl.text(``);
            //lut.html(``);
            //lut.hide();
            hideLongUrl();
            let userLoginMessage;
            if (isLoggedIn()) {
                userLoginMessage = "";
            } else {
                userLoginMessage = TEXT_LOGIN_PLEASE;
            }
            if (longUrl.length < 3) {
                vl.text(`${getTextById(7)} ${userLoginMessage}`);
                vl.show();
            } else if (!checkUrl.test(longUrl)) {
                vl.text(`${TEXT_INVALID_URL}  ${userLoginMessage}`);
                lut.html(createInvalidUrlMessage(longUrl));
                vl.show();
                lut.show();
            } else {
                try {
                    functionForProject_dovisto(objectId, dataObject, "user/newshortcutkey", "post");
                } catch (error) {
                    console.error(error);
                    $("#createsubmit").html(`<i class="fa fa-exclamation-triangle"></i>`);
                }
            }
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall5 (selecturlid)
 Description: Select the id of an existing url (and hence also check if the url exists)
 ***************************************/
$(document)
    .on(
        'click',
        '.click_selecturlid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            if (!isLoggedIn()) {
                blink("login", 100, 3);
            }
            console.log("current object-id is: " + objectId);
            const li = $("#longurl_input");
            const lut = $("#longUrlText");
            const vl = $("#validationlabel");
            $(this).data("urlstring", li.val());
            //lut.html(``);
            //lut.hide();
            hideLongUrl();
            let urlstring = li.val().trim();
            if (!urlstring) {
                blink(objectId, 25, 3);
                blink("longurl_input", 250, 3);
                vl.text(TEXT_MANDATORY_FIELD);
                vl.show();
                //lut.text(``);
                //lut.hide();
                hideLongUrl();
            } else if (!urlstring.startsWith("http://") && !urlstring.startsWith("https://")) {
                urlstring = `https://${urlstring}`;
                $("#longurl_input").val(urlstring);
                $("#validationlabel").text(urlstring);
            }
            if (urlstring && !checkUrl.test(urlstring)) {
                console.error("apiEvents: urlstring=" + urlstring + " last index: " + checkUrl.lastIndex);
                blink(objectId, 25, 3);
                vl.text(TEXT_INVALID_URL);
                vl.show();
                lut.html(createInvalidUrlMessage(urlstring));
                lut.show();
            } else if (urlstring) {
                let dataObject = {
                    "urlstring": urlstring
                };
                console.log("Check id of url " + urlstring + "...");
                functionForProject_dovisto(objectId, dataObject, "user/selecturlid", "post");
            }
        }
    );


/***************************************
 CALL-ID: dovisto.User.apiCall6 (usershortcuts)
 Description: Select all shortcuts of the user
 ***************************************/
$(document)
    .on(
        'click',
        '.click_usershortcuts',
        function () {
            console.log("click_usershortcuts");
            $("#validationlabel").html(``);
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let dataObject = {};
            const sessionUrlData = sessionStorage.getItem(SESSIONDATA_KEY);
            if (!sessionUrlData || newItemAdded) {
                functionForProject_dovisto(objectId, dataObject, "user/usershortcuts", "post");
            } else if (isEmpty($("#urllist-div").html())) {
                $("#loadingIcon").show();
                userUrlTableObject.jsonRMLData = JSON.parse(sessionUrlData);
                listoHTML.createTable.call(userUrlTableObject, "urllist-div", true);

                // $("#loadingIcon").hide();
            }
            setDovisVisible(["shortcutlistfunctions", "urllist-div"], dovisNavigationIDs);
        }
    );
let settingsCalled = false;
/***************************************
 CALL-ID: dovisto.User.apiCall7 (usersettings)
 Description: Show user's settings
 ***************************************/
$(document)
    .on(
        'click',
        '.click_usersettings',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let dataObject = {};
            const wMesg = document.getElementById("waitMessage");
            wMesg.style.visibility = "visible";

            functionForProject_dovisto(objectId, dataObject, "user/usersettings", "post");
            setDovisVisible("settings-div", dovisNavigationIDs);
        }
    );
/** Custom event: when user clicks on a short-url in the displayed table **/
let urlInfo;
$(document)
    .on(
        'click',
        '.show_shortcut_properties',
        function () {
            $("#urlstring").remove();
            let check1 = "value=1";
            let check2 = "value=2";
            let check3 = "value=3";
            const linkcategory = $(this).data("linkcategory");
            const idshortcut = $(this).data("idshortcut");
            if (linkcategory == 1) check1 += " checked";
            else if (linkcategory == 2) check2 += " checked";
            else if (linkcategory > 2) check3 += " checked";
            $(`#sc_properties_${idshortcut}`).removeClass("show_shortcut_properties");
            $(`#sc_properties_${idshortcut}`).addClass("hide_shortcut_properties");
            const trashIcon=document.getElementById(`delete_${idshortcut}`);
            if (trashIcon) trashIcon.style.visibility="hidden";
            let enableOption3 = ``;
            let subscribeOption3 = ``;
            let subscribeHint = ``;
            let radioSelectionClass = "click_redirectmode";
            if (USER_CATEGORY === 5) {
                enableOption3 = `disabled`;
                subscribeOption3 = `<br> <a class="edit-plan">${getTextById(81)}&nbsp;<i class="fa fa-handshake-o"></i></a><br>`;
                subscribeHint = getTextById(82);
                radioSelectionClass = "click_shortcutenable";
            }
            urlInfo = `<div id="urlstring" class="rounded" style="padding: 0.5em;
                       align-items;  text-sm-left; background-color: #b3d7ff; color: black">
                       <div class="text-right"><i class="fa fa-window-close fa-2x hide_shortcut_properties"
                           id="close_properties_icon"></i></div>
                          <i class="fa fa-external-link fa-lg">&nbsp;<small>${TEXT_SET_REDIRECTION_MODUS}</small></i>
                               <a id="setRedirectModeButton" class="btn btn-primary ${radioSelectionClass}"
                                  style="visibility: hidden;" data-idshortcut="${idshortcut}"  data-enable="">
                                  <i class="fa fa-check" ></i></a>
                          <div class="form-check rounded" style="padding: -0.42em;" id="categoryRadio">
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat1" ${check1}>
                                <i class = "fa fa-unlink"></i>
                                &nbsp;  ${getTextById(69)}</input><br>
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat2" ${check2}>
                               <i class = "fa fa-link"></i> &nbsp; ${getTextById(70)}</input><br>
                               <!--
                               <small>${subscribeOption3}</small>
                               <small>${subscribeHint}</small><br>
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat3" ${check3} ${enableOption3}>
                                  <i class = "fa fa-forward"></i>&nbsp;${getTextById(71)}</input>
                                  -->
                          </div>
                          <hr>
                          <div   class="rounded" style="padding: -0.42em;">
                          <i class="fa fa-tags fa-lg">&nbsp;</i>
                          <input type="text" id="inputUsertags" placeHolder="${TEXT_FILL_SOME_TAGS}" class="rounded">&nbsp;&nbsp;
                           <a class="dovislink">
                           <i class="fa fa-save fa-3x click_tagshortcut" data-idshortcut="${idshortcut}" id="addTagsToShortcut"></i> </a>
                          <i id="infoTaglistAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          </div>
                          <hr>
                          <!--
                          <div class="rounded" style="padding: -0.42em;">
                          <i class="fa fa-quote-left fa-lg"> </i>&nbsp;&nbsp;
                          <textarea rows="3" style="height:100%;" id="inputUserdesc" placeHolder="${TEXT_ADD_YOUR_DESCRIPTION}"></textarea>
                         <i class="fa fa-quote-right fa-lg"></i>&nbsp;&nbsp;&nbsp;
                           <a class="dovislink"> <i class="fa fa-save fa-3x click_shortcutdesc" id="click_shortcutdesc" data-idshortcut="${idshortcut}"></i>
                            </a>
                           <i id="infoUserDescAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          </div>
                          -->
                          <div class="doviseditor">
                             <style>
                                img{
                                    max-width:200px;
                                    max-height:200px;
                                    } 
                                 table,th,td{
                                    text-align:left;
                                    }
                             </style>
                             <div id="summernote"></div>
                          </div>
                           <a class="dovislink"> <i class="fa fa-save fa-3x click_shortcutdesc" id="click_shortcutdesc" data-idshortcut="${idshortcut}"></i>
                            </a>
                          <i id="infoUserDescAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          <hr>
                          </div>`;

            $(this).parent().before(urlInfo);
            $("#summernote").summernote({
                placeholder: TEXT_ADD_YOUR_DESCRIPTION,
                minHeight: 300,
                maxHeight: 700,
                focus: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['para', ['ul', 'ol']],
                    ['insert', ['link', 'picture']],
                    ['view', ['codeview']],
                ],
            });
            const tags = $(`#taglist${idshortcut}`).text();
            if (tags) {
                $(`#inputUsertags`).val(tags);
            }
            const userHtml = $(`#share_${idshortcut}`).attr("data-userhtml");
            console.info("userHtml", userHtml);
            if (userHtml) {
                $('#summernote').summernote('pasteHTML', userHtml);
            }
            $(document).ready(function () {
                $('input[type=radio][name="shortUrlCategoryRadio"]').on('change', function () {
                    $("#setRedirectModeButton").attr("style", "visibility: visible;");
                });
            });

            $("#urlstring").hide();
            $("#urlstring").fadeIn(250);
        }
    );


/** Custom event: when user clicks on the share-icon in the displayed table **/
let shareoptions;
$(document)
    .on(
        'click',
        '.show_share',
        function () {
            if (shareoptions) {
                $("#sharediv").remove();
            }


            const shortcut = $(this).data("shortcut");
            const idshortcut = $(this).data("idshortcut");

            const descriptionText = $(this).data("urldescriptiontext");
            const tags = $(this).data("taglist");
            $(`#share_${idshortcut}`).removeClass("show_share");
            $(`#share_${idshortcut}`).addClass("hide_share");
            const trashIcon=document.getElementById(`delete_${idshortcut}`);
            if (trashIcon) trashIcon.style.visibility="hidden";
            let twitterTags = ``;
            if (notEmpty(tags)) twitterTags = `&hashtags=${encodeURIComponent(tags)}`;
            const urlString = encodeURIComponent(`${APPLICATION_HOMEPAGE}/${shortcut}`);
            shareoptions = `<div id="sharediv" class="rounded text-md-center" style="padding: 0.2em;
                       align-items; background-color: #b3d7ff; color: black">
                       <div class="text-right hide_share" data-idshortcut="${idshortcut}"><i class="fa fa-window-close fa-2x"></i></div>
                       <br>
                       <span>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=${APPLICATION_HOMEPAGE}/${shortcut}">
                                <i class="fa fa-facebook-f fa-2x"></i></a> &nbsp;&nbsp;&nbsp;
                     <a href="https://www.linkedin.com/shareArticle?mini=true&url=${APPLICATION_HOMEPAGE}/${shortcut}">
                                <i class="fa fa-linkedin fa-2x"></i></a>&nbsp;&nbsp;
                     <a href="https://telegram.me/share/url?url=${APPLICATION_HOMEPAGE}/${shortcut}&text=${descriptionText}">
                                <i class="fa fa-telegram fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                     <a href="whatsapp://send?text=${APPLICATION_HOMEPAGE}/${shortcut}" data-action="share/whatsapp/share"  target="_blank">
                                <i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></a>    &nbsp;&nbsp;&nbsp;
                      <a href="https://twitter.com/intent/tweet?text=${encodeURIComponent(descriptionText)}&url=${urlString}${twitterTags}">
                        <i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>              &nbsp;&nbsp;&nbsp;
                       <a href="mailto:?subject=${descriptionText}&body=${APPLICATION_HOMEPAGE}/${shortcut}">
                                <i class="fa fa-envelope fa-2x"></i></a>
                     </span>&nbsp;&nbsp;&nbsp;
                       <span id="mailing_${shortcut}" class="dbadge85 mailing_action"   
                             data-shortcut="${shortcut}" data-idshortcut="${idshortcut}"> 
                              <i class="fa fa-users fa-2x"></i>
                             <i class="fa fa-envelope-square fa-2x"></i>
                       </span>
               </div>`;
            $(this).after(shareoptions);
            $("#sharediv").hide();
            $("#sharediv").fadeIn(500);
        });


$(document)
    .on(
        'click',
        '.analyzeurl', function () {
            const url = $(this).data("url");
            console.log("---- Analyze '" + url + "': .... ");
            analyzeRequestedPage(url);
        });

$(document)
    .on(
        'click',
        '.hide_shortcut_properties',
        function () {
            if (urlInfo) {
                $("#urlstring").fadeOut(250);
                $("#urlstring").remove();
            }
            const idshortcut = $(this).data("idshortcut");

            $(`#sc_properties_${idshortcut}`).removeClass("hide_shortcut_properties");
            $(`#sc_properties_${idshortcut}`).addClass("show_shortcut_properties");
            const trashIcon=document.getElementById(`delete_${idshortcut}`);
            if (trashIcon) trashIcon.style.visibility="visible";
            //$(this).addClass("show_shortcut_properties");
        }
    );


$(document)
    .on(
        'click',
        '.hide_share',
        function () {
            const idshortcut = $(this).data("idshortcut");
            $(`#share_${idshortcut}`).removeClass("hide_share");
            $(`#share_${idshortcut}`).addClass("show_share");
            if (shareoptions) {
                $("#sharediv").fadeOut(500);
                $("#sharediv").remove();
            }
            document.getElementById(`delete_${idshortcut}`).style.visibility="visible";

            //  $(this).removeClass("hide_share");
           // $(this).addClass("show_share");
        }
    );

$(document).on('click', '.show_submit', function () {
    setDovisVisible("create-newshortcut-div", dovisNavigationIDs);
    $("#createsubmit").removeClass("disabled");
    $("#createsubmit").addClass("click_selecturlid");
    $("#create_shortcut_with_metadata").remove();
    $("#create_shortcut_without_metadata").remove();
    $("#shortCut_taglist").remove();
    $("#shortCut_taglist_icon").remove();
    $("#newshortcut_h3").remove();
    $("#scurl_linkage").remove();
    $("#save_shortcut").remove();
    $("#publish_shortcut").remove();
    const sessionData = sessionStorage.getItem(SESSIONDATA_KEY);
    const urlID = sessionStorage.getItem(SESSIONDATA_URLID_KEY);
    const urlString = sessionStorage.getItem(SESSIONDATA_URLSTRING_KEY);
    if (!sessionData && urlID && urlString) {
        analyzeRequestedPage(urlID, urlString, function () {
        }, function () {
            sessionStorage.removeItem(SESSIONDATA_URLID_KEY);
            sessionStorage.removeItem(SESSIONDATA_URLSTRING_KEY);
        }, function (err) {
            console.error(err);
        });
    }
});

$(document).on('click', '.click_propose', function () {
    blink("login", 75, 3);
    blink("signup", 75, 3);
    blink("createsubmit", 50, 5);
    $("#validationlabel").text(TEXT_LOGIN_PLEASE);
    $("#validationlabel").show();
    console.log(TEXT_LOGIN_PLEASE);
    blink("validationlabel", 50, 5);
});
let options;

$(document).on('click', '.setRedirectMode', function () {
    const linkcategory = $(this).data("linkcategory");
    const idshortcut = $(this).data("idshortcut");
    console.log("clicked: " + TEXT_SET_REDIRECTION_MODUS + "category=" + linkcategory);
    const icon = $(`#icon${idshortcut}`);
    console.log(`change icon #icon${idshortcut}`);
    icon.removeAttr("class");
    icon.addClass("fa fa-check fa-lg");

});

$(document).on('click', '.show-analytics', function () {
    console.log("clicked: show-analytics");
    $("#analyticspage").remove();
    const analyticspage = `<div id="analyticspage" class="click_editplan text-center border rounded">
          <a href="${APPLICATION_HOMEPAGE}"><i class="fa fa-remove  fa-2x"></i></a><br>
           <div class="form-check rounded dovis" style="padding: 1.5em;">
              <i class="fa fa-bar-chart fa-3x"></i>
              <i class="fa fa-table fa-3x"></i>
              <i class="fa fa-tachometer fa-3x"></i>
              <i class="fa fa-digital-tachograph fa-3x"></i>
              <i class="fa fa-line-chart fa-3x"></i>
              <br><br>
               <h4> THIS PAGE IS IN DEVELOPMENT. COMING SOON ...</h4>
          </div>
      </div>`;
    hideMenu();
    $("#main-content").html(``);
    $("#main-content").after(analyticspage);
});

function populateWithAllEMails() {
    console.info("Send Emails to ALL");

    const eventCallBack = function (data) {
        const recipients = document.getElementById("mail_campaign_recipients");
        recipients.innerText = ``;
        const allEmails = data[0].rows;
        let recipientString = ``;
        for (let i = 0; i < allEmails.length; i++) recipientString += allEmails[i].s[1] + ", ";
        recipients.innerText = recipientString;
        recipients.setAttribute("class", "border border-primary rounded");
        document.getElementById("mail_campaign_subject").removeAttribute("disabled");
        document.getElementById("mail_campaign_subject").removeAttribute("class");
        //document.getElementById("mail_campaign_body").removeAttribute("disabled");
    }
    apiCall("8.8002.6", "/8/standardsubscription/selectcustomeremails", {}, eventCallBack, httpErrorCallBack, "post", ENCODING);
};

function showEmailRecipients() {
    console.info("Send Emails to selected");
};

function showEMailGroups() {
    console.info("Send Emails to groups");
};

function enableMailSending() {
    if (document.getElementById("mail_campaign_subject").value.trim() !== "") document.getElementById("sendEmailCampaign").setAttribute("class", "dbadge85 sendEmailCampaign");
    else document.getElementById("sendEmailCampaign").setAttribute("class", "");
}


/** Custom event: mailing action after click on 'show-share' **/
$(document).on('click', '.mailing_action', function () {
    const sc = $(this).data("shortcut");
    const idShortcut = $(this).data("idshortcut");
    console.info("clicked mailing-action id:", idShortcut, "shortcut:", sc);

    const mailActionDiv = `<h3> dovis.it/${sc} </h3>  
           <!--
           <select id="select_mailrecipient_groups" class="selectpicker" multiple data-live-search="true" style="visibility: hidden;">
              <option>Group1</option>
              <option>Ketchup</option>
              <option>Relish</option>
            </select>
        
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio_EMAIL_TO_ALL" value="sendToAll" onchange="populateWithAllEMails()" >
              <label class="form-check-label" for="inlineRadio1">${TEXT_EMAIL_TO_ALL}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio_EMAIL_TO_GROUP" value="sendToGroups"   onchange="showEMailGroups()" disabled>
              <label class="form-check-label" for="inlineRadio2">${TEXT_EMAIL_TO_GROUP}</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio_EMAIL_TO_SELECTED" value="sendToSelected" onchange="showEmailRecipients()" disabled>
              <label class="form-check-label" for="inlineRadio3">${TEXT_EMAIL_TO_SELECTED}</label>
            </div>
            -->
            <br>
          
            <div id="mail_campaign_recipients" class="container"  style="font-size:0.8rem;max-height:200px;overflow-y:scroll;padding:25px;"></div><br>
              <i  class="fa fa-envelopes-bulk"></i><br>
             <input type="text" id="mail_campaign_subject"  placeHolder="${TEXT_TYPE_EMAIL_SUBJECT}" size="45" disabled oninput="enableMailSending()"></input><br>      
              <div id="summernote"></div>   
           
            <!--i class="fa fa-check-double fa-2x"></i--> 
            <span id="sendEmailCampaign" class="subjectHint"  data-idshortcut="${idShortcut}" data-shortcut="${sc}">
                <i class="fa fa-envelope-o fa-2x" ></i>
            </span>`;
    const div = document.createElement("div");
    div.innerHTML = mailActionDiv;
    const upperDiv = document.getElementById("manageMailAccounts");
    setDovisVisible("manageMailAccounts", dovisNavigationIDs);
    upperDiv.innerHTML = div.outerHTML;
    $("#summernote").summernote({
        placeholder: TEXT_TYPE_EMAIL_BODY,
        minHeight: 300,
        maxHeight: 700,
        focus: true
    });
    populateWithAllEMails();
});


/***************************************
 CALL-ID: dovisto.User.apiCall8 (updateusersettings)
 Description: Edit user settings
 ***************************************/
$(document)
    .on(
        'click',
        '.click_updateusersettings',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const email = $("#edit-email").val();
            const firstname = $("#edit-firstname").val();
            const lastname = $("#edit-lastname").val();
            const username = $("#edit-username").val();

            if (!email) {
                console.error(`Variable 'email' in object with id ${objectId} has no value.`);
            }

            if (!firstname) {
                console.error(`Variable 'firstname' in object with id ${objectId} has no value.`);
            }

            if (!lastname) {
                console.error(`Variable 'lastname' in object with id ${objectId} has no value.`);
            }

            if (!username) {
                console.error(`Variable 'username' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (email && firstname && lastname && username) {
                dataObject = {
                    "email": email,
                    "firstname": firstname,
                    "lastname": lastname,
                    "username": username
                };
            }
            functionForProject_dovisto(objectId, dataObject, "user/updateusersettings", "post");
            $("#edit-username").attr("disabled", true);
            $("#edit-email").attr("disabled", true);
            $("#edit-firstname").attr("disabled", true);
            $("#edit-lastname").attr("disabled", true);
            $("#save_edit").attr("style", "visibility:hidden;");
            $("#close-user-settings").attr("style", "visibility:hidden");
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall12 (tagshortcut)
 Description: Add a tag to a shortcut and its url (BEWARE: this call will assign to a url only the first element of a comma-
 or space-separated list!)
 ***************************************/
$(document)
    .on(
        'click',
        '.click_tagshortcut',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idshortcut = $(this).data("idshortcut");
            const tag = encodeURIComponent($("#inputUsertags").val());

            if (!idshortcut) {
                console.error(`Variable 'idshortcut' in object with id ${objectId} has no value.`);
            }

            if (!tag) {
                console.error(`Variable 'tag' in object with id ${objectId} has no value.`);
                blink("inputUsertags", 100, 3);
            }

            let dataObject;
            if (idshortcut && tag) {
                dataObject = {
                    "idshortcut": idshortcut,
                    "tag": tag
                };
                $("#infoTaglistAdded").attr("style", "visibility:visible");
                $("#infoTaglistAdded").fadeIn();
                functionForProject_dovisto(objectId, dataObject, "user/tagshortcut", "post");
                $(`#taglist${idshortcut}`).text($("#inputUsertags").val());
            }
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall13 (shortcutenable)
 Description: Enable or disable the redirection of a shortcut, by setting category=1(no redirect) or
 category=2(redirect with preview-page and ads)
 ***************************************/
$(document)
    .on(
        'click',
        '.click_shortcutenable',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let enabled = true;
            const enable = $("input[name='shortUrlCategoryRadio']:checked").val();
            if (enable == 1) enabled = false;
            const idshortcut = $(this).data("idshortcut");

            if (!enable) {
                console.error(`Variable 'enable' in object with id ${objectId} has no value.`);
            }

            if (!idshortcut) {
                console.error(`Variable 'idshortcut' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (enable && idshortcut) {
                dataObject = {
                    "enable": enabled,
                    "idshortcut": idshortcut
                };
                functionForProject_dovisto(objectId, dataObject, "user/shortcutenable", "post");
                const icon = $(`#icon${idshortcut}`);
                icon.removeAttr("class");
                if (enabled) {
                    icon.addClass("fa fa-link fa-lg");
                    $(`#share_${idshortcut}`).attr("style", "width: 45px; height:20px; visibility:visible");
                } else {
                    icon.addClass("fa fa-unlink fa-lg");
                    $(`#share_${idshortcut}`).attr("style", "width: 45px; height:20px; visibility:hidden");
                }

            }
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall14 (shortcutdesc)
 Description: Set a description for the shortcut
 ***************************************/
$(document)
    .on(
        'click',
        '.click_shortcutdesc',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            const descTyped = $("#summernote").summernote("code");
            //const descInput = encodeURIComponent(descTyped);
            const len = descTyped.length;//Math.min(255, descInput.length);
            //const desc = descInput.substring(0, len);
            const idshortcut = $(this).data("idshortcut");

            if (!descTyped) {
                console.error(`Variable 'desc' in object with id ${objectId} has no value.`);
            }

            if (!idshortcut) {
                console.error(`Variable 'idshortcut' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (len > 20000) {
                blink("summernote", 100, 3);
                $("#summernote").attr("style", "border:red;");
                alert(TEXT_NUMBER_OF_CHARACTERS + ": " + len + " (>20.000)");
            } else if (descTyped && idshortcut) {
                $("#inputUserdesc").attr("style", "border:none;");
                dataObject = {
                    "desc": descTyped,
                    "idshortcut": idshortcut
                };

                const snDiv = document.getElementsByClassName('note-editable');
                const m = snDiv[0];

                console.info("editor-content:", m.innerHTML, descTyped);
                functionForProject_dovisto(objectId, dataObject, "user/shortcutdesc", "post", "url");
                $("#infoUserDescAdded").attr("style", "visibility:visible");
                $("#infoUserDescAdded").fadeIn();
                $(`#desc${idshortcut}`).text($("#inputUserdesc").val());
            } else {
                blink("inputUserdesc", 100, 3);
            }
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall13 (redirectmode)
 Description: Enable or disable the redirection of a shortcut, by setting the appropriate
 link-category
 ***************************************/
$(document)
    .on(
        'click',
        '.click_redirectmode',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const category = $('#categoryRadio input:radio:checked').val();
            const idshortcut = $(this).data("idshortcut");

            if (!category) {
                console.error(`Variable 'category' in object with id ${objectId} has no value.`);
            }

            if (!idshortcut) {
                console.error(`Variable 'idshortcut' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (category && idshortcut) {
                dataObject = {
                    "category": category,
                    "idshortcut": idshortcut
                };
            }
            functionForProject_dovisto(objectId, dataObject, "standardsubscription/redirectmode", "post");
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall2 (bid)
 Description: Place a reservation-bid for a shortcut

 Id of triggering Object: document
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.placeBid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idshortcut = $(this).data("idshortcut");
            const price = $(this).data("price");

            if (!idshortcut) {
                console.error(`Variable 'idshortcut' in object with id ${objectId} has no value.`);
            }

            if (!price) {
                console.error(`Variable 'price' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idshortcut && price) {
                dataObject = {
                    "idshortcut": idshortcut,
                    "price": price
                };
            }
            const eventCallBack = function (dataObject) {
                //TODO
            };

            apiCall("8.8002.2", "/8/standardsubscription/bid", dataObject, eventCallBack, httpErrorCallBack, "post", ENCODING);
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall3 (receivedbids)
 Description: Select received bids for an own shortcut

 Id of triggering Object: document
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.show-receivedBids',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            let dataObject = {};
            const eventCallBack = function (dataObject) {
                //TODO
            };

            apiCall("8.8002.3", "/8/standardsubscription/receivedbids", dataObject, eventCallBack, httpErrorCallBack, "post", ENCODING);
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall4 (shortcutbids)
 Description: Select the highest bid for a shortcut

 Id of triggering Object: document
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.select-highestbid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            let dataObject = {};
            const eventCallBack = function (dataObject) {
                //TODO
            };

            apiCall("8.8002.4", "/8/standardsubscription/shortcutbids", dataObject, eventCallBack, httpErrorCallBack, "post", ENCODING);
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall5 (uploadcustomeremails)
 Description: Upload new customer-emails to the server

 Id of triggering Object: uploadFileIcon
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.start-upload',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const csvstring = $(this).data("csvstring");
            const iduser = $(this).data("iduser");

            if (!csvstring) {
                console.error(`Variable 'csvstring' in object with id ${objectId} has no value.`);
            }

            if (!iduser) {
                console.error(`Variable 'iduser' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (csvstring && iduser) {
                dataObject = {
                    "csvstring": csvstring,
                    "iduser": iduser
                };

            }
            console.log("Clicked start upload. dataObject:", dataObject);
            const eventCallBack = function (data) {
                console.info("fire callBack from 8.8002.5 uploadcustomeremails. objectid:", objectId, "data:", data);
                if (data._rc === 0 && data.status === 200) {
                    const callBack8002_6 = function (data) {
                        console.info("fire callBack from 8.8002.6 loadcustomeremails", "data:", data);
                        setDovisVisible("showMailAccounts-div", dovisNavigationIDs);
                        document.getElementById("showMailAccounts-div").innerHTML = `<h3 class="showCustomerEmails">${TEXT_MAILS_WERE_UPLOADED}</h3>`;
                    }
                    apiCall("8.8002.6", "/8/standardsubscription/loadcustomeremails", data, callBack8002_6, httpErrorCallBack, "post", ENCODING);
                } else console.error("upload of customer emails failed");
            };

            apiCall("8.8002.5", "/8/standardsubscription/uploadcustomeremails", dataObject, eventCallBack, httpErrorCallBack, "post", ENCODING);
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall6 (selectcustomeremails)
 Description: Select all customer-emails from the customer-base

 Id of triggering Object: document
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.showCustomerEmails',
        function () {
            isUploadingEmails=false;
            console.info("Clicked showCustomerEmails");
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            document.getElementById("showMailAccounts-div").innerHTML = ``;
            setDovisVisible("showMailAccounts-div", dovisNavigationIDs);
            let dataObject = {};
            const eventCallBack = function (data) {
                console.info("dataObject", data);
                emailCustomersTableObject.jsonRMLData = data[0];
                emailCustomersTableObject.header = [`email`, `name`, `last name`, `created`, `updated`];
                emailCustomersTableObject.columnIndices = [1, 2, 3, 4, 5];
                console.info("emailCustomersTableObject", emailCustomersTableObject);
                listoHTML.createTable.call(emailCustomersTableObject, "showMailAccounts-div", true, "right");
                const currentDiv = document.getElementById("showMailAccounts-div");
                const uploadIcon = document.createElement("i");
                uploadIcon.setAttribute("class", "fa fa-upload fa-2x manage-mailaccounts");

                uploadIcon.innerText = TEXT_UPLOAD_FILE;
                currentDiv.appendChild(uploadIcon);

            };

            apiCall("8.8002.6", "/8/standardsubscription/selectcustomeremails", dataObject, eventCallBack, httpErrorCallBack, "post", ENCODING);
        }
    );

/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall7 (updatecustomeremail)
 Description: Update a customer-email in the customer-base

 Id of triggering Object: document
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.updateCustomerEmail',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const email = $(this).data("email");
            const firstname = $(this).data("firstname");
            const idemail = $(this).data("idemail");
            const lastname = $(this).data("lastname");

            if (!email) {
                console.error(`Variable 'email' in object with id ${objectId} has no value.`);
            }

            if (!firstname) {
                console.error(`Variable 'firstname' in object with id ${objectId} has no value.`);
            }

            if (!idemail) {
                console.error(`Variable 'idemail' in object with id ${objectId} has no value.`);
            }

            if (!lastname) {
                console.error(`Variable 'lastname' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (email && firstname && idemail && lastname) {
                dataObject = {
                    "email": email,
                    "firstname": firstname,
                    "idemail": idemail,
                    "lastname": lastname
                };
            }
            const eventCallBack = function (dataObject) {
                //TODO
            };

            apiCall("8.8002.7", "/8/standardsubscription/updatecustomeremail", dataObject, eventCallBack, httpErrorCallBack, "post", ENCODING);
        }
    );

$(document)
    .on(
        'click',
        '.open-preview',
        function () {
            const shortcut = $(this).data("sc");
            window.open(`${APPLICATION_HOMEPAGE}/${shortcut}`, '_blank');
           // location.href = `${APPLICATION_HOMEPAGE}/${shortcut}`;
        });

$(document).on('click', '.manage-mailaccounts', function () {
        isUploadingEmails=true;
        document.getElementById("manageMailAccounts").innerHTML = buildMailAccountsList();
        setDovisVisible("manageMailAccounts", dovisNavigationIDs);
        const inputElement = document.getElementById("emailCSV");
        inputElement.addEventListener("change", handleFiles, false);

        const targetDiv = document.getElementById("uploadCSVPreview");
        const csvFileInfo = document.getElementById("csvFileInfo");
        const csvImport = document.getElementById("csvImport");
        let emailData;

        const analyzeFile = function (delimiter, quoteChar, string) {
            let zeroHeader = [];
            let delimiterName;
            if (delimiter === ',') delimiterName = "comma (,)";
            else if (delimiter === ';') delimiterName = "semicolon (;)";
            else if (delimiter === '\t') delimiterName = "tab space (\\t)";
            else if (delimiter === ' ') delimiterName = "space ( )";
            const json = csvToJson(string, zeroHeader, '"', delimiter);
            let transformResult = `${TEXT_CHARACTER_WAS_USED_AS_DELIMITER}: <b>'${delimiterName}'</b><br>`;
            let errorResult = `<HR><h6>${TEXT_ERRORLIST}:</h6>`;
            let countValids = 0, countErrors = 0;
            const result = {};
            result.usedDelimiter = delimiterName;
            result.header = [];
            result.rows = [];

            for (let i = 0; i < json.length; i++) {
                try {
                    const row = {};
                    row.s = [];
                    row.rn = i;
                    let rowHasEmail = false;
                    const rowLength = Object.keys(json[i]).length;
                    for (let col = 0; col < rowLength; col++) {
                        try {
                            const column = json[i]["column" + col];
                            if (column) {
                                if (!result.header.includes("column" + col)) result.header.push("column" + col);
                                if (validateEmail(column))
                                    rowHasEmail = true;
                                row.s.push(column);
                            }
                        } catch (err) {
                            console.error(err);
                        }
                    }
                    if (rowHasEmail) row.s.push(1);
                    else row.s.push(0);
                    result.rows.push(row);
                } catch (err) {
                    console.error(err);
                }
            }
            result.header.push("validityCheck");
            result.arrayWithValidRows = [];
            const lastColumnIndex = result.header.length - 1;
            for (let i = 0; i < result.rows.length; i++) {
                try {

                    const row = result.rows[i];
                    const validationMarker = row.s[lastColumnIndex];
                    if (validationMarker === 1) transformResult += `<br>${TEXT_RECORD} ${1 + i}:&nbsp;`;
                    let rowString = ``;

                    for (let field = 0; field < result.header.length; field++) {
                        const columnValue = row.s[field];
                        rowString += columnValue;

                        if (validationMarker === 1)
                            transformResult += columnValue;
                        if (field < lastColumnIndex && columnValue) {
                            if (validationMarker === 1) transformResult += delimiter;
                            rowString += delimiter;
                        }
                    }

                    if (validationMarker === 1) {
                        countValids++;
                        row.rn = countValids;
                        row.s.splice(lastColumnIndex, 1);
                        result.arrayWithValidRows.push(row);
                    } else {
                        countErrors++;
                        errorResult += `<br><b>${TEXT_RECORD} ${1 + i}: ${TEXT_NO_VALID_EMAIL_FOUND} - </b>${rowString}`;
                    }
                } catch (err) {
                }
            }
            result.delimiter = delimiter;
            result.header.splice(result.header.length - 1, 1);
            result.validRecordsList = transformResult;
            result.erroneousRecordsList = errorResult;
            result.numberOfValidRows = result.arrayWithValidRows.length;
            result.numberOfInValidRows = countErrors;
            return result;
        }

        const addColumnSelectors = function (headerColumns) {
            const options = [];
            for (let i = 0; i < headerColumns.length; i++) {
                const opt = {};//document.createElement('option');
                opt.value = headerColumns[i];
                opt.text = opt.value;
                options.push(opt);
            }
            const showSelectedColumns = document.getElementById("showSelectedColumns");

            const emailColumnSelect = document.getElementById("emailColumnSelect");
            const firstNameColumnSelect = document.getElementById("firstNameColumnSelect");
            const lastNameColumnSelect = document.getElementById("lastNameColumnSelect");
            const columnSelection = document.getElementById("columnSelection");
            // show selected values
            const emailShow = document.getElementById("email-column");
            const nameShow = document.getElementById("name-column");
            const lastNameShow = document.getElementById("lastname-column");
            const uploadIcon = document.getElementById("uploadFileIcon");
            showSelectedColumns.style.visibility = "visible";


            emailColumnSelect.addEventListener('change', function () {
                updateOptions(emailColumnSelect, firstNameColumnSelect, lastNameColumnSelect);
                emailShow.innerText = emailColumnSelect.value;
                updateTable();

            });

            firstNameColumnSelect.addEventListener('change', function () {
                updateOptions(firstNameColumnSelect, emailColumnSelect, lastNameColumnSelect);
                nameShow.innerText = firstNameColumnSelect.value;
                updateTable();

            });

            lastNameColumnSelect.addEventListener('change', function () {
                updateOptions(lastNameColumnSelect, emailColumnSelect, firstNameColumnSelect);
                lastNameShow.innerText = lastNameColumnSelect.value
                uploadIcon.style.visibility = "visible";
                updateTable();
            });

            function buildUploadCsvString(columnIndices) {
                if (columnIndices.length === 3) {
                    const inputData = emailCustomersTableObject.jsonRMLData.rows;
                    let csvString = ``;
                    for (let r = 0; r < inputData.length; r++) {
                        const rowArray = inputData[r].s;
                        const email = rowArray[columnIndices[0]];
                        const firstName = rowArray[columnIndices[1]];
                        const lastName = rowArray[columnIndices[2]];
                        const rowString = `${email};${currentDovisUserID};${firstName};${lastName}\n`;
                        csvString += rowString;
                    }
                    uploadIcon.setAttribute("data-csvstring", csvString);
                }
            }

            function updateTable() {
                emailCustomersTableObject.header = [];
                emailCustomersTableObject.columnIndices = [];
                emailCustomersTableObject.header.push(` Email`);
                emailCustomersTableObject.header.push(`${getTextById(74)}`);
                emailCustomersTableObject.header.push(`${getTextById(75)}`);
                emailCustomersTableObject.columnIndices.push(emailColumnSelect.value - 1);
                emailCustomersTableObject.columnIndices.push(firstNameColumnSelect.value - 1);
                emailCustomersTableObject.columnIndices.push(lastNameColumnSelect.value - 1);
                const uniqueEmails = getUniqueRMLObjects(emailData, emailCustomersTableObject.columnIndices[0]);
                const validUniqueEmails = [];
                for (let i = 0; i < uniqueEmails.length; i++) {
                    if (validateEmail(uniqueEmails[i].s[emailCustomersTableObject.columnIndices[0]])) validUniqueEmails.push(uniqueEmails[i]);
                }
                document.getElementById("showHeaderIconInfo").innerText = `${TEXT_UNIQUE_EMAILS_IN_COLUMN_NUMBER_X_IS} ${emailCustomersTableObject.columnIndices[0] + 1}: ${validUniqueEmails.length} `;
                emailCustomersTableObject.jsonRMLData.rows = validUniqueEmails;
                buildUploadCsvString(emailCustomersTableObject.columnIndices);
                listoHTML.createTable.call(emailCustomersTableObject, "csvImport", true);
            }

            function fill(selection, optionElements) {
                selection.innerHTML = ``;
                const opt = document.createElement("option");
                opt.setAttribute("selected", "true");
                opt.setAttribute("disabled", "true");
                opt.text = getTextById(93);//"Select"
                selection.appendChild(opt);
                for (let i = 0; i < optionElements.length; i++) {
                    const opt = document.createElement("option");
                    opt.setAttribute("label", optionElements[i].text);
                    opt.text = optionElements[i].text;
                    selection.appendChild(opt);
                }
            }

            function updateOptions(selected, other1, other2) {
                // Get the selected values
                const selectedValue = selected.value;
                const other1Value = other1.value;
                const other2Value = other2.value;


                // Filter the options for the other select elements to exclude the selected values
                const options1 = options.filter(function (option) {
                    return option.value !== other1Value && option.value !== other2Value;
                });

                const options2 = options.filter(function (option) {
                    return option.value !== selectedValue && option.value !== other2Value;
                });

                const options3 = options.filter(function (option) {
                    return option.value !== selectedValue && option.value !== other1Value;
                });
                fill(selected, options1);
                fill(other1, options2);
                fill(other2, options3);
                selected.value = selectedValue;
                other1.value = other1Value;
                other2.value = other2Value;

            }


            fill(emailColumnSelect, options);
            fill(firstNameColumnSelect, options);
            fill(lastNameColumnSelect, options);


            emailColumnSelect.style.visibility = "visible";
            firstNameColumnSelect.style.visibility = "visible";
            lastNameColumnSelect.style.visibility = "visible";
            columnSelection.style.visibility = "visible";
        }

        function handleFiles() {
            csvFileInfo.innerHTML = ``;
            targetDiv.innerHTML = ``;
            csvImport.innerHTML = ``;
            document.body.style.cursor = "progress";
            const transformCsv = function (string) {
                targetDiv.innerHTML = ``;
                csvImport.innerHTML = ``;

                emailCustomersTableObject.jsonRMLData = {};
                let result1 = {}, result2 = {}, result3 = {}, result4 = {};
                try {
                    result1 = analyzeFile(";", '"', string);
                } catch (err) {
                    console.error(err);
                }
                try {
                    result2 = analyzeFile(",", '"', string);
                } catch (err) {
                    console.error(err);
                }
                try {
                    result3 = analyzeFile("\t", '"', string);
                } catch (err) {
                    console.error(err);
                }
                try {
                    result4 = analyzeFile(" ", '"', string);
                } catch (err) {
                    console.error(err);
                }
                let result = {};

                try {
                    const bestResult = Math.max(result1.numberOfValidRows, result2.numberOfValidRows, result3.numberOfValidRows, result4.numberOfValidRows);
                    if (bestResult === result1.numberOfValidRows) result = result1;
                    else if (bestResult === result2.numberOfValidRows) result = result2;
                    else if (bestResult === result3.numberOfValidRows) result = result3;
                    else if (bestResult === result4.numberOfValidRows) result = result4;
                    if (result.numberOfValidRows > 0) {
                        emailCustomersTableObject.jsonRMLData.variableNames = result.header;
                        emailCustomersTableObject.jsonRMLData._variableLabels = result.header;
                        emailCustomersTableObject.header = result.header;
                        emailCustomersTableObject.columnIndices = [];
                        emailCustomersTableObject.jsonRMLData.rows = result.arrayWithValidRows;
                        // We need a separate object for filtering the unique Emails!
                        emailData = {};
                        emailData.rows = result.arrayWithValidRows;
                        emailData.variableNames = result.header;
                        emailData._variableLabels = result.header;

                        for (let idx = 0; idx < result.header.length; idx++) {
                            emailCustomersTableObject.columnIndices.push(idx);
                            emailCustomersTableObject.header[idx] = 1 + idx;
                        }
                        emailCustomersTableObject.jsonRMLData.count = result.numberOfValidRows;
                        emailCustomersTableObject.jsonRMLData.columnCount = result.header.length;
                        let headerSample = `<table><tr>`;
                        const td = `<td style="border-style: solid;border-width: 1px;text-align:center;">`;
                        for (let i = 0; i < result.header.length; i++) headerSample += `${td}${i + 1}</td>&nbsp;`
                        headerSample += "</tr><tr>"
                        for (let i = 0; i < result.header.length; i++) headerSample += `${td}${emailCustomersTableObject.jsonRMLData.rows[0].s[i]}</td>&nbsp;`
                        headerSample += "</tr><tr>";
                        for (let i = 0; i < result.header.length; i++) headerSample += `${td}${emailCustomersTableObject.jsonRMLData.rows[1].s[i]}</td>&nbsp;`
                        headerSample += "</tr><tr>";
                        for (let i = 0; i < result.header.length; i++) headerSample += `${td}...</td>&nbsp;`
                        headerSample += "</tr><table>";
                        document.getElementById("showHeader").innerHTML = headerSample;
                        document.getElementById("showHeaderIconInfo").innerHTML = `<i class="fa fa-info fa-2x"></i>`;
                        listoHTML.createTable.call(emailCustomersTableObject, "csvImport", true);
                        addColumnSelectors(emailCustomersTableObject.header);
                    }
                    if (result.numberOfInValidRows === 0 && result.numberOfValidRows > 0) {
                        return result.validRecordsList;
                    } else if (result.numberOfInValidRows > 0 && result.numberOfValidRows === 0) {
                        return `<HR>${TEXT_NO_VALID_EMAILS_PARSED}  ${result.numberOfInValidRows} ${TEXT_RECORDS_PRODUCED_ERRORS}<br> 
                            ${result.erroneousRecordsList}`;
                    } else {
                        return `<HR>${result.numberOfValidRows} ${TEXT_EMAILS_WERE_PARSED}.  ${result.numberOfInValidRows} ${TEXT_RECORDS_PRODUCED_ERRORS}.<br>
                            ${result.validRecordsList}${result.erroneousRecordsList}`;
                    }
                } catch (err) {
                    csvFileInfo.innerHTML += `<b>${err}</b>`;
                    return `<HR><b>${TEXT_ERROR}</b> - ${TEXT_COULD_NOT_PROCESS_DATA} <b>(${err})</b><HR>${string}${result.erroneousRecordsList}${result.validRecordsList}`;
                }


            }

            const csvFile = this.files[0];

            const max_allowed = Math.round(MAXIMUM_ALLOWED_FILESIZE_CSV / 1000);
            const fileSize=Math.round(csvFile.size / 1024);
            csvFileInfo.innerHTML = `${TEXT_FILETYPE} <b>'${csvFile.type}'</b>`;
            if (!csvFile.type || csvFile.type === "") csvFileInfo.innerHTML += `<br><b>${TEXT_WARNING}:</b> ${TEXT_FILE_UNKNOWN_TYPE}.`;
            if (csvFile.type !== "text/plain") csvFileInfo.innerHTML += `<br><b>${TEXT_WARNING}:</b>${TEXT_NOT_A_TEXTFILE}. (${TEXT_FILETYPE} <b>'${csvFile.type}'</b>. ${TEXT_SHOULD_BETTER_BE} <b>'text/plain'</b>)<br>`;
            if (fileSize > max_allowed) {
                targetDiv.innerHTML += `<b>${TEXT_ERROR}:</b> ${TEXT_FILE_TOO_BIG} (${fileSize} Kb; ${TEXT_MAXIMUM_ALLOWED_SIZE_IS} ${max_allowed} Kb)<br>`;
            } else {

                const uploadClockIcon = document.getElementById("uploadClock");
                uploadClockIcon.style.visibility = "visible";
                uploadClockIcon.setAttribute("class", "fa fa-cog fa-spin fa-1x fa-fw fa-2x");
                previewFile(targetDiv, csvFile, transformCsv);
                uploadClockIcon.style.visibility = "hidden";


            }
            document.body.style.cursor = "auto";
        }

    }
);


/***************************************
 CALL-ID: dovisto.StandardSubscription.apiCall10 (getsenderinfo)
 Description: Get Email sender data

 Id of triggering Object: document
 Triggering event(s): click

 ***************************************/
$(document)
    .on(
        'click', '.sendEmailCampaign',

        function () {
            let
                shortcut = ``;
            let objectId = "unassigned";

            try {
                objectId = $(this).attr("id");
                shortcut = $(this).data("shortcut");

            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            console.info("sendEmailCampaign: clicked on object " + objectId)
            const hint = document.createElement("b");
            const eventCallBack = function (dataObject) {

                const senderName = dataObject[0].rows[0].s[1];
                const senderLastName = dataObject[0].rows[0].s[2];
                const senderMail = dataObject[0].rows[0].s[3];
                const optOutLink = `&nbsp;<b><a href='mailto:${senderMail}?cc=support@dovis.it&subject=OPT OUT from ${senderMail}-list on ${APPLICATION_HOMEPAGE}'>Opt out</a></b>`;

                const disclaimer = `\n\n<br><br><br><br><small>${APPLICATION_HOMEPAGE}/${shortcut}<br> ${TEXT_EMAIL_SENT_ON_BEHALF_OF}: ${senderMail} (${senderName} ${senderLastName})&nbsp;${TEXT_EMAIL_OPTOUT}${optOutLink}</small><br>`
                const emailsubject = document.getElementById("mail_campaign_subject").value;

                const emailtext = $('#summernote').summernote('code');
                const userlist = document.getElementById("mail_campaign_recipients").innerText;

                const emailLongText = `${APPLICATION_HOMEPAGE}/${shortcut}\n${APPLICATION_HOMEPAGE}/${shortcut}+\n\n\n ${emailtext}\n-------------------\n ${disclaimer}`;
                const mailDataObject = {
                    "emailsubject": emailsubject,
                    "emailtext": emailLongText,
                    "sessiontoken": sessionToken,
                    "userlist": userlist
                };

                const afterSending = function (data) {
                    try {
                        let succeeded = [], failed = [], successList = ``, failedList = ``;
                        const good = data['successful'][0];
                        if (good) {
                            succeeded = good.split(" ");
                            successList = `<h3>${TEXT_EMAIL_WAS_SENT}</h3> <i class="fa fa-close fa-2x" onclick="clearCampaignResults()"></i><br>
                                              `;
                        }
                        const bad = data['failed'][0];
                        if (bad) {
                            failed = bad.split(" ");
                        }

                        failedList = `<br>`;
                        if (data._rc === 0) {
                            for (let i = 0; i < succeeded.length; i++) successList = `${successList}, ${succeeded[i]}`;
                            successList = `${successList}<br><br><i class="fa fa-check-square-o fa-3x" onclick="clearCampaignResults()"></i>`;
                            for (let j = 0; j < failed.length; j++) failedList += `${failed[j]}<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>`;
                            document.getElementById("mailCampaignResult").innerHTML = `<p>${successList}</p><p>${failedList}</p>`;
                        } else document.getElementById("mailCampaignResult").innerHTML = `<h3>${TEXT_SERVER_ERROR}: (Http${data.status})</h3>`;
                        setDovisVisible("mailCampaignResult", dovisNavigationIDs);
                    } catch (err) {
                        console.error(err);
                    }
                    document.getElementById("mailCampaignIcon").style.visibility = "hidden";
                    mailIcon.removeChild(hint);
                }
                apiCall("8.8002.9", "/8/standardsubscription/sendmailtocustomers", mailDataObject, afterSending, httpErrorCallBack, "post", "HTMLENCODE");
            };
            apiCall("8.8002.10", "/8/standardsubscription/getsenderinfo", {}, eventCallBack, httpErrorCallBack, "post", ENCODING);
            setDovisVisible("urllist-div", dovisNavigationIDs);

            hint.innerText = `${TEXT_SENDING_EMAIL}`;
            const mailIcon = document.getElementById("mailCampaignIcon");
            mailIcon.style.visibility = "visible";
            mailIcon.appendChild(hint);
        }
    )
;
$(document)
    .on(
        'click', '.subjectHint', function () {
            blink("mail_campaign_subject", 50, 6);
            blink("inlineRadio_EMAIL_TO_SELECTED", 40, 4);
            blink("inlineRadio_EMAIL_TO_GROUP", 50, 5);
            blink("inlineRadio_EMAIL_TO_ALL", 60, 6);
        });

$(document)
    .on(
        'click', '.delete-shortcut', function () {
            let divId, idshortcut;
            let objectId = "unassigned";

            try {
                objectId = $(this).attr("id");
                idshortcut = $(this).data("idsc");
                const divIdStr = "shortcutrow" + idshortcut;
                const divId = document.getElementById(divIdStr);
                divId.innerHTML = ``;
            } catch (err) {
                console.error("id of shortcut", divId, err);
            }

            const eventCallBack = function (returnedData) {

                console.info("Shortcut was deleted? :", returnedData);

            }
            const apiParams = {};
            apiParams.idshortcut = idshortcut;
            console.info("delete shortcut: clicked on object " + objectId);
            apiCall("8.8001.15", "/8/user/deleteshortcut", apiParams, eventCallBack, httpErrorCallBack, "post", ENCODING);
        });

$(document)
    .on(
        'click', '.optout',

        function () {
            let emailId ;

            try {
                emailId = $(this).data("emailid");
                console.info("Clicked to delete emailid:",emailId);

            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
        });