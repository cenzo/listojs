const errordiv = "dovis-errors";
const TableStyle = {
    loadingIcon: `<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>`,
    pre:``,
    classes :`table table-hover table-striped table-sm text-sm-left dataTable`,
    colOpen: `<col>`,
    colClose: `</col>`,
    tableHeadOpen: `<thead><tr>`,
    tableHeadClose: `</tr></thead><tbody>`,
    thOpen: `<th class="text-sm-left">`,
    thClose: `</th>`,
    trOpen: `<tr class="text-left">`,
    trClose: `</tr>`,
    tdOpen: `<td class="text-left">`,
    tdClose: `</td>`,
    aOpen: `<a`,
    aAttributes: `>`,
    aClose: `</a>`,
    bottom: `</table>`,
    elements: [],
};
// Alternatives:
//pre:`<div class="col">`,
//pre:`<div class="col" style="overflow-x: scroll; table-layout: fixed;">`,
//classes :`table table-hover  table-sm text-sm-left dataTable style="table-layout: fixed`,
//bottom: `</table></div>`,

const FormStyle = {
    pre1: '<div class="row"><div class="col-md-',
    pre2: '"><div class="box box-primary"><form role="form" id=',
    pre3: ' enctype="multipart/form-data"><div class="box-body">',
    suf: '</div></form></div></div></div>',
};
