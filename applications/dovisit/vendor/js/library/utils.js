let layoutTexts = {mappings: []};
let layoutTextsLoaded = false;
let layoutChanged = true;
let layoutID;
getLayoutId();


function getLayoutId() {
    if (layoutID && !layoutChanged) {
        debug("Layout is already set to: " + layoutID, release);
        return layoutID;
    }
    apiCall_listorante_public_layoutid(function (data) {
        layoutID = data.rows[0].s[0];
        layoutChanged = false;
        layoutTextsLoaded = false;
        debug("Layout was selected and is now set to: " + layoutID, release, layoutID, layoutChanged, layoutTextsLoaded);
        assignLayoutTexts();
        return layoutID;
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

function assignLayoutTexts() {
    if (layoutTextsLoaded) {
        for (let i = 0; i < layoutTexts.count; i++) {
            debug("TODO: re-assign loaded text  " + i, release);
            document.getElementById(layoutTexts.getKey(i)).innerText = layoutTexts.getValue(i);
        }
        return;
    }
    if (layoutID) {
        apiCall_listorante_public_assignedtexts(layoutID, function (data) {
            for (let i = 0; i < data.count; i++) {
                let map = {};
                map.text_id = data.rows[i].s[0];
                map.text_value = data.rows[i].s[3];
                //debug("text-map " + i + ":", 0, map);
                layoutTexts.mappings.push(map);
                let textElement = document.getElementById(map.text_id);
                if (textElement) textElement.innerText = map.text_value;
                else debug("Element with id '" + map.text_id + "' does not exist in this document", release);
            }
            //debug("layout-Texts:", 0, layoutTexts);
            layoutTextsLoaded = true;
        }, function (err) {
            showHttpErrorMessage("main-content", err.status);
        });
    } else {
        debug("Could not assign texts, layoutID is undefined ", release, layoutID, layoutChanged, layoutTextsLoaded);
    }
}

stripe_customer_id = getStripeAccountID(function(found_stripe_id){
    if (found_stripe_id) {
        console.log("Customer has following stripe-account: " + found_stripe_id);
        is_stripe_registered = true;
    } else {
        console.log("No stripe-account found for this customer ");
    }
});

