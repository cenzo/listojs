// global variables:
let CALL_COUNTER = 0;
let APPLICATION_SERVER_ARRAY = [selectedServer+801];

let TIMEOUT = 2000;
let PARALLEL_CALLS = 4;
let primitiveErrorHandling = function (status) {
    console.error("An error occurred, Status=", status);
}

function newCallID() {
    return CALL_COUNTER++;
}

class ParallelCall {
    resultStartRow;
    resultEndRow;
    serverArray;
    httpErrorCallBack;
    dataEncoding;
    dataBufferSize;
    callBackInProgress;
    responseStateArray;
    // apiID, apiUrl,
    // apiParams, callBack, httpErrorCallBack, method, encoding
    constructor(apiParams, endPointPath, callBackFunction, httpMethod) {
        // TODO ensure uniqueness of id
        this.id = newCallID();
        this.completed = false;
        this.callParameters = apiParams;
        this.endPoint = endPointPath;
        this.callBack = callBackFunction;
        this.method = httpMethod;
        this.callBackInProgress = false;
        this.dataMap = new Map();
        this.dataResultSizeIdentified = false;
        this.parallelCalls = PARALLEL_CALLS;
        this.isReadOnly = 1;

        this.resultStartRow = null;
        this.resultEndRow = null;
        this.serverArray = null;
        this.httpErrorCallBack = null;
        this.dataEncoding = null;
        this.dataBufferSize = null;
        this.callBackInProgress = false;
        this.responseStateArray = [];
    }

    setParallelCalls(n) {
        this.parallelCalls = n;
    }

    setStartDataPosition(pos) {
        this.resultStartRow = pos;
    }

    setEndDataPosition(pos) {
        this.resultEndRow = pos;
    }

    setServers(arr) {
        this.serverArray = arr;
        this.responseStateArray = [];
    }

    setHttpErrorHandling(f) {
        this.httpErrorCallBack = f;
    }

    setEncoding(enc) {
        this.dataEncoding = enc;
    }

    setBufferSize(n) {
        this.dataBufferSize = n;
    }

    addDataFragment(dataPartition, xhttpResponse) {
        this.dataMap.set(dataPartition, xhttpResponse);
    }

    endOfDataReached(xhttpResponse) {
        // TODO: adapt to JSON-structure of response
        const endReached = xhttpResponse['eod'];
        if (endReached === "true") return true;
        else return false;
    }

    dataMapComplete() {
        console.warn("//TODO: implement method ParallelCall.dataMapComplete")
        if (this.dataMap.size === 0) return false;
        else if (this.dataResultSizeIdentified === true && this.resultEndRow <= this.dataBufferSize) return true;
        else if (this.responseStateArray.length !== this.serverArray.length) return false;
        for (let i = 0; i < this.serverArray.length; i++) {
            // TODO
            // if (this.dataMap.size === 0) etc...

        }
        return true;
    }

    dataRetrieved(dataFragment) {
        console.warn("//TODO: implement method ParallelCall.dataRetrieved(iteration) for dataFragment", dataFragment)
        if (this.dataResultSizeIdentified === false || this.dataMap.size === 0) return false;
        //TODO
        else return true; // temporary dummy code
        // else if (this.dataMap.has(dataFragment) && this.dataMap.get(dataFragment).length<= this.dataBufferSize) ) return true; (??)

    }

    getStartPosition(dataPartition) {
        console.info("//TODO: implement method ParallelCall.getStartPosition(dataPartition) for dataPartition", dataPartition, " and dataMap ", this.dataMap);

        return dataPartition * this.dataBufferSize;
        //function getFirstEmpty(key,value) {
        //    let lastSize=value.length;
        //....
        // }

    }

    getEndPosition(dataPartition) {
        console.warn("//TODO: implement method ParallelCall.getEndPosition(dataPartition) for dataPartition", dataPartition, " and dataMap ", this.dataMap);
        return (dataPartition + 1) * this.dataBufferSize;
    }

    run() {
        if (this.resultStartRow === null) this.setStartDataPosition(0);
        if (this.resultEndRow === null) this.setEndDataPosition(1000);
        if (this.serverArray === null) this.setServers(APPLICATION_SERVER_ARRAY);
        if (this.httpErrorCallBack === null) this.setHttpErrorHandling(primitiveErrorHandling);
        if (this.dataEncoding === null) this.setEncoding("html");
        if (this.dataBufferSize === null) this.setBufferSize(100);

        const xhrArray = [];
        let errorsOccurred = false;
        let endpointUrl;
        let calledServerIndex;
        const currentCall = this;
        for (let i = 0; i < Math.max(this.serverArray.length, this.dataMap.size, this.parallelCalls); i++) {
            // if all parallel calls have fetched their dataPartitions, then no new xmlhttprequest is needed
            // if we have a write-transaction then only one call (the fastest) will go ahead
            if (this.dataRetrieved(i) || this.isReadOnly === 0) continue;
            xhrArray[i] = new XMLHttpRequest();
            xhrArray[i].timeout = TIMEOUT;
            calledServerIndex = i % this.serverArray.length;
            endpointUrl = this.serverArray[calledServerIndex] + this.endPoint;
            const startPos = this.getStartPosition(i);
            const endPos = this.getEndPosition(i);
            console.info(i, ": fetch data-partitions from row ", startPos, " to ", endPos," with endpoint-url '",endpointUrl,"'");


            xhrArray[i].onreadystatechange = function () {
                if (currentCall.isreadOnly === 0) {
                    xhrArray[i].abort();
                    console.info("Write-Fetch ", i, " aborted in readyState ", xhrArray[i].readyState, " because other parallel write-fetch-call was faster than this one");
                } else if (xhrArray[i].readyState === 2) {
                    const readOnly = xhrArray[i].getResponseHeader("readOnlyTransaction");
                    if (readOnly === 0) {
                        currentCall.isreadOnly = 0;
                        if (xhrArray[i].status < 400) currentCall.completed = true;
                        else if (xhrArray[i].status >= 400) {
                            currentCall.responseStateArray[i] = xhrArray[i].status;
                            xhrArray[i].abort();
                            console.info("Fetch ", i, " aborted due to errors. Ready-State: ", xhrArray[i].readyState);
                            errorsOccurred = true;
                        }
                    } else if (currentCall.completed === true && currentCall.callBackInProgress === true) {
                        xhrArray[i].abort();
                        console.info("Fetch ", i, " aborted because other parallel fetch-call was faster than this one");
                    }
                } else if (xhrArray[i].readyState === 4) {
                    currentCall.responseStateArray[i] = xhrArray[i].status;
                    // TODO: adapt to JSON-structure of response
                    currentCall.addDataFragment(i, xhrArray[i].response);
                    if (currentCall.endOfDataReached(xhrArray[i].response)) {
                        currentCall.setEndDataPosition(xhrArray[i].response["resultSize"]);
                        currentCall.dataResultSizeIdentified = true;
                    }
                    if (currentCall.dataMapComplete()) {
                        currentCall.completed = true;
                        currentCall.callBack(currentCall.dataMap);
                        currentCall.callBackInProgress = true;
                        if (errorsOccurred === true) currentCall.httpErrorCallBack(currentCall.responseStateArray);
                        return;
                    }
                }
            };
            xhrArray[i].open(currentCall.method, endpointUrl, true);
            xhrArray[i].setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhrArray[i].send("token=" + sessionToken);
            xhrArray[i].send("start=" + startPos);
            xhrArray[i].send("end=" + endPos);
        }
        currentCall.run();
    }

}