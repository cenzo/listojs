/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'listorante'
	Module: 'Kitchen'
	Version: 1.02
***************************************/

/***************************************
	CALL-ID: listorante.Kitchen.apiCall1
	select all items currently in preparation in kitchen
	***************************************/
function apiCall_listorante_kitchen_items
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Kitchen.apiCall1","/1/kitchen/items", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Kitchen.apiCall2
	Update delivery status of an ordered item
	***************************************/
function apiCall_listorante_kitchen_setprodstatus
	(idorder,
	status,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"status" : status
	};
	return apiCall("listorante.Kitchen.apiCall2","/1/kitchen/setprodstatus", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'listorante'
	Module: 'Kitchen'
	Version: 1.02
***************************************/

/***************************************
	CALL-ID: listorante.Kitchen.apiCall1
	select all items currently in preparation in kitchen
	***************************************/
function apiCall_listorante_kitchen_items
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Kitchen.apiCall1","/1/kitchen/items", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Kitchen.apiCall2
	Update delivery status of an ordered item
	***************************************/
function apiCall_listorante_kitchen_setprodstatus
	(idorder,
	status,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"status" : status
	};
	return apiCall("listorante.Kitchen.apiCall2","/1/kitchen/setprodstatus", dataObject, callBack, httpErrorCallBack,"put");
}


