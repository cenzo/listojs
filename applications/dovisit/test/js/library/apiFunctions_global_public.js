/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'global'
	Module: 'Public'
	Version: 1.1
***************************************/

/***************************************
	CALL-ID: global.Public.apiCall1
	Check new customer registration
	***************************************/
function apiCall_global_public_checkRegistration
	(domainName,
	email,
	isSubDomain,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName,
	"email" : email,
	"isSubDomain" : isSubDomain
	};
	return apiCall("global.Public.apiCall1","/0/public/checkRegistration", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: global.Public.apiCall2
	Customer registration with domain
	***************************************/
function apiCall_global_public_doRegistration
	(domainName,
	email,
	firstName,
	isSubDomain,
	lastName,
	password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName,
	"email" : email,
	"firstName" : firstName,
	"isSubDomain" : isSubDomain,
	"lastName" : lastName,
	"password" : password
	};
	return apiCall("global.Public.apiCall2","/0/public/doRegistration", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: global.Public.apiCall3
	Customer registration for a specific application
	***************************************/
function apiCall_global_public_doAppRegistration
	(appid,
	custid,
	token,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"appid" : appid,
	"custid" : custid,
	"token" : token
	};
	return apiCall("global.Public.apiCall3","/0/public/doAppRegistration", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: global.Public.apiCall4
	Confirm registration
	***************************************/
function apiCall_global_public_confirmRegistration
	(customerid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid
	};
	return apiCall("global.Public.apiCall4","/0/public/confirmRegistration", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: global.Public.apiCall5
	Confirm application registration
	***************************************/
function apiCall_global_public_confirmAppRegistration
	(applicationid,
	customerid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"applicationid" : applicationid,
	"customerid" : customerid
	};
	return apiCall("global.Public.apiCall5","/0/public/confirmAppRegistration", dataObject, callBack, httpErrorCallBack,"post");
}
