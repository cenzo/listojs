const showResponse = function (userToken, response, isError) {

    const viewerDiv = document.getElementById("viewResults");
    const responseDiv = document.createElement("tr");
    responseDiv.style.borderWidth = "2px";
    responseDiv.style.borderColor = "blue";
    const infoCell = document.createElement("td");
    //const userInfo = document.createElement("b");
    let userInfo;
    const userResponse = document.createElement("b");
    const userName = userTokenMap.get(userToken);
    if (userName)
        userInfo = userName;
    else userInfo = "no userName was retrieved";
    if (isError === true) {
        if (response && response.status === 401) userResponse.innerHTML = `<div class="unAuthenticatedResult">${userInfo} HTTP ${response.status} ${response.response}</div>`;
        else if (response && response.status > 401) userResponse.innerHTML = `<div class="errorResult"> ${userInfo} HTTP ${response.status} ${response.response}</div>`;
        else
            userResponse.innerHTML = `<div class="errorResult"> ${userInfo} No response - Unknown Error, response: ${response} status:${response.status}</div>`
    } else try {
        const arr = JSON.parse(response.response);
        let rowSum = 0;
        for (let i = 0; i < arr.length; i++) rowSum += arr[i].count;
        if (rowSum) userResponse.innerHTML = ` <div class="goodResult"> ${userInfo}&nbsp;&#9; HTTP ${response.status} rc: ${JSON.parse(response.response)[0]['_rc']} &nbsp;&#9;${rowSum} records</div>`;
        else userResponse.innerHTML = ` <div class="goodResult"> ${userInfo}&nbsp;&#9; HTTP ${response.status} rc: ${JSON.parse(response.response)[0]['_rc']} </div>`;

    } catch (err) {
        //console.error(err);
        userResponse.innerHTML = ` <div class="goodResult">${userInfo} Err-result: ${JSON.parse(response[0])} </div>`;
        //userResponse.innerText = err;
    }

    viewerDiv.appendChild(responseDiv);
    responseDiv.appendChild(infoCell);
    infoCell.append(userResponse);
    //userResponse.append(userInfo);
}

const viewServerData = function (response) {
    const settings = response['settings'];
    const exceptionCounts = response['ExceptionCount by errorNumber'];
    const exceptionDescriptions = response['Stacktrace by errorNumber'];
    const exceptionList = [];
    for (let [key, value] of Object.entries(exceptionCounts)) {
        const exceptionItem = {};
        exceptionItem.exceptionID = key;
        exceptionItem.exceptionCount = value;
        exceptionItem.description = exceptionDescriptions[key];
        exceptionList.push(exceptionItem);
    }
    exceptionList.sort((a, b) => b.exceptionCount - a.exceptionCount);
    const table = document.createElement("table");
    for (let i = 0; i < exceptionList.length; i++) {
        const row = document.createElement("tr");
        const td1 = document.createElement("td");
        const td2 = document.createElement("td");
        const td3 = document.createElement("td");
        td1.innerText = exceptionList[i].exceptionID;
        td2.innerText = exceptionList[i].exceptionCount;
        //td3.innerText = exceptionList[i].description;

        const descArray = [];
       /* for (let [key, value] of Object.entries(exceptionList[i].description)) {
            for (let [key2, value2] of Object.entries(value)){
                const descObj = {};
                descObj.str = `${key}.${key2}=${value2}`;
                descArray.push(descObj);
            }
        }*/
        for (let [key, value] of Object.entries(exceptionList[i].description)) {

                const descObj = {};
                descObj.str = `${key}=${value}`;
                descArray.push(descObj);

        }
        descArray.sort((a, b) => a.str.localeCompare(b.str));

        for (let j = 0; j < descArray.length; j++) {
            td3.innerText += `${descArray[j].str}    `;
        }
        row.appendChild(td1);
        row.appendChild(td2);
        row.appendChild(td3);
        table.appendChild(row);

    }
    const tableDiv = document.getElementById("viewResults")
    tableDiv.innerHTML = table.outerHTML;
}

const sortTable = function (table) {
    let rows, switching, i, x, y, shouldSwitch;
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
          first, which contains table headers):*/
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getElementsByTagName("TD")[0];
            y = rows[i + 1].getElementsByTagName("TD")[0];
            // Check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                // If so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}