const TEXT_REGISTER_MAIL_BODY_START = "Click on this activation-code to complete your registration:";
const TEXT_REGISTER_MAIL_BODY_END = "The code is valid for only two hours. ";
const TEXT_REGISTER_MAIL_SUBJECT = "uioWare: your activation-code";
const CALLBACKPAGE_USER_REGISTRATION = "https://www.dovis.it/t";
const JSON_HTML_URL = `${CALLBACKPAGE_USER_REGISTRATION}/email.json`;
const TEXT_MANDATORY_FIELD = "This field is mandatory";
const TEXT_LOGIN_BAD_REQUEST = "Bad Request";
const TEXT_LOGIN_NO_AUTHENTICATION = "Authentication failed";
const TEXT_LOGIN_NO_AUTHORIZATION = "Not authorized";
const TEXT_LOGIN_SERVER_ERROR = "Login failed. Problems on Server";
const TEXT_LOGIN_ERROR = "Login failed. Unexpected Problem on  Frontend or Server";


// Email Validation Function
function validateEmail(email) {
    const regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

const register = function () {
    const register = document.getElementById("signup");
    register.style.visibility = "hidden";
    const registrationEmailField = document.getElementById("usernameField");
    registrationEmailField.style.visibility = "visible";
    registrationEmailField.style.top = "50px";
    registrationEmailField.setAttribute("placeholder", "Type Your Email");
    const login = document.getElementById("signin");
    login.style.visibility = "hidden";
    const sendButton = document.getElementById("sendButton");
    sendButton.style.visibility = "visible";
    sendButton.innerText = "Signup!";
    sendButton.setAttribute("class", "bi bi-envelope");
    sendButton.setAttribute("onclick", "doRegistration()");
    sendButton.style.visibility = "visible";

}

const login = function () {
    const register = document.getElementById("signup");
    register.style.visibility = "hidden";
    const signin = document.getElementById("signin");
    signin.style.visibility = "hidden";
    const registrationEmailField = document.getElementById("usernameField");
    registrationEmailField.style.visibility = "visible";
    registrationEmailField.setAttribute("placeholder", "Type Your Username");
    const passField = document.getElementById("passField");
    passField.style.visibility = "visible";
    const secretIcon = document.getElementById("secret");
    secretIcon.style.visibility = "visible";
    const loginUserIcon = document.getElementById("loginUserIcon");
    loginUserIcon.style.visibility = "visible";
    const sendButton = document.getElementById("sendButton");
    sendButton.style.visibility = "visible";
    sendButton.innerText = "Login";
    sendButton.setAttribute("class", "bi bi-box-arrow-in-right");
    sendButton.setAttribute("onclick", "doLogin()");
}
const doLogin = function () {
    const sendButton = document.getElementById("sendButton");
    sendButton.style.visibility = "hidden";
    const register = document.getElementById("signup");
    register.style.visibility = "hidden";
    const registrationEmailField = document.getElementById("usernameField");
    registrationEmailField.style.visibility = "hidden";

    const passField = document.getElementById("passField");
    passField.style.visibility = "hidden";
    const secretIcon = document.getElementById("secret");
    secretIcon.style.visibility = "hidden";
    const loginUserIcon = document.getElementById("loginUserIcon");
    loginUserIcon.style.visibility = "visible";
    loginUserIcon.style.right = "350px";
    const userName = registrationEmailField.value;
    const pass = passField.value;

    authenticateUser(userName, pass);


}
let hintIsActive = false, isPublicUser = false;
const authenticateUser = function (theUserName, pass) {
    const sendButton = document.getElementById("sendButton");
    const hintLogin = document.getElementById("hintLogin");
    const hintPass = document.getElementById("hintPass");


    const login_userName = document.getElementById("usernameField");
    const login_pass = document.getElementById("passField");
    const loginUserIcon = document.getElementById("loginUserIcon");
    loginUserIcon.style.visibility = "hidden";
    login_userName.addEventListener("input", function () {
        if (hintLogin) hintLogin.style.visibility = "hidden";
        if (hintPass) hintPass.style.visibility = "hidden";
        loginUserIcon.style.visibility = "hidden";
    });
    login_pass.addEventListener("input", function () {
        if (hintLogin) hintLogin.style.visibility = "hidden";
        if (hintPass) hintPass.style.visibility = "hidden";
        loginUserIcon.style.visibility = "hidden";
    });
    sendButton.addEventListener("click", function () {
        if (hintLogin) hintLogin.style.visibility = "hidden";
        if (hintPass) hintPass.style.visibility = "hidden";
        loginUserIcon.style.visibility = "hidden";
    });


    const loginMessage = function (status, msg) {
        console.log("login-status=" + status + ": " + msg, "hintIsActive", hintIsActive);
        if (!hintIsActive) {
            const hintLoginP = document.createElement("p");
            const hintPassP = document.createElement("p");
            hintLoginP.setAttribute("id", "hintLogin");
            hintPassP.setAttribute("id", "hintPass");
            console.info("appending hints...");
            sendButton.appendChild(hintLoginP);
            sendButton.appendChild(hintPassP);
            hintIsActive = true;
        }
        const hintLogin = document.getElementById("hintLogin");
        const hintPass = document.getElementById("hintPass");
        if (status === 400 || status === 401 || status === 403) {
            hintLogin.innerText = msg;
            hintPass.innerText = "Please retry.";
            hintLogin.style.visibility = "visible";
            hintPass.style.visibility = "visible";
            login_userName.style.visibility = "visible";
            login_pass.style.visibility = "visible";
            sendButton.style.visibility = "visible";
        }
    };

    if (!theUserName) {
        loginMessage(0, TEXT_MANDATORY_FIELD);
    } else if (!pass) {
        loginMessage(0, TEXT_MANDATORY_FIELD);
    } else {
        const waitIcon = document.createElement("div");
        waitIcon.setAttribute("class", "spinner-grow");
        waitIcon.setAttribute("role", "status");
        const secondDiv = document.getElementById("secondDiv");
        secondDiv.appendChild(waitIcon);

        const xhttp = new XMLHttpRequest();
        const postUrl = `${getAPIServerPath()}${customerID}/${applicationID}/${appModule}/login`;
        console.info("postUrl", postUrl);
        let jsonResponse = {bearerToken: null};
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4) {
                secondDiv.removeChild(waitIcon);

                if (xhttp.status > 0 && xhttp.status <= 204) {
                    isPublicUser = false;
                    loginUserIcon.style.visibility = "visible";
                    jsonResponse = JSON.parse(xhttp.responseText);
                    sessionToken = jsonResponse.bearerToken.split(";")[0];
                    // we need userEmail for payment checkout:
                    const userEmail = jsonResponse.bearerToken.split(";")[1];
                    loginUserIcon.innerText = userEmail;
                    sessionStorage.setItem("dovisUser", theUserName);
                    sessionStorage.setItem("dovisUserEmail", userEmail);
                    sessionStorage.setItem("listo_" + customerID + "_" + applicationID + "_" + currentDovisUserID, sessionToken);
                    const signout = document.getElementById("signin");
                    signout.setAttribute("id", "signout");
                    signout.setAttribute("class", "bi bi-door-closed");
                    signout.setAttribute("onclick", "logout()");
                    signout.style.visibility = "visible";
                    signout.innerText = "Exit";
                } else if (xhttp.status >= 400) {
                    if (xhttp.status === 400) {
                        loginMessage(xhttp.status, `HTTP400 ${TEXT_LOGIN_BAD_REQUEST}`);
                    } else if (xhttp.status === 401) {
                        loginMessage(xhttp.status, `HTTP401 ${TEXT_LOGIN_NO_AUTHENTICATION}`);
                    } else if (xhttp.status === 403) {
                        loginMessage(xhttp.status, `HTTP403 ${TEXT_LOGIN_NO_AUTHORIZATION}`);
                    } else {
                        loginMessage(xhttp.status, `${xhttp.status}: ${TEXT_LOGIN_SERVER_ERROR}`);
                    }
                } else {
                    loginMessage(xhttp.status, TEXT_LOGIN_ERROR);
                }

            }
        };
        xhttp.open("POST", postUrl, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("username=" + theUserName + "&password=" + pass);
    }
}
const doRegistration = function () {

    const registrationEmailField = document.getElementById("usernameField");
    const sendButton = document.getElementById("sendButton");
    const waitIcon = document.createElement("div");
    waitIcon.setAttribute("class", "spinner-grow");
    waitIcon.setAttribute("role", "status");
    const secondDiv = document.getElementById("secondDiv");
    secondDiv.appendChild(waitIcon);
    let responseObject = {
        errorMnemonic: ''
    };

    const userEmail = registrationEmailField.value;
    if (validateEmail(userEmail)) {
        console.log(`Start registration of user with Email ${userEmail}`);

        const xhttp = new XMLHttpRequest();
        const postUrl = `${getAPIServerPath()}0/0/customer/asktoregister/900/9`;
        xhttp.open('POST', postUrl, true);
        xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4) {
                secondDiv.removeChild(waitIcon);
                try {
                    console.log('xhttp.status=' + xhttp.status);
                    responseObject = JSON.parse(xhttp.responseText);
                    console.log('responseObject: ' + responseObject);
                    if (xhttp.status > 0) {
                        if (xhttp.status <= 204) {
                            sendButton.innerText = "Please check your Email-folder";


                        } else if (xhttp.status === 422) {
                            if (responseObject.errorMnemonic.startsWith('asktoregister.3')) {
                                sendButton.innerText = "Invalid Email:  already registered."

                            } else if (responseObject.errorMnemonic.toString().startsWith('asktoregister.7')) {
                                sendButton.innerText = `Failed to send eMail to ${userEmail}`;
                            }
                        } else if (xhttp.status >= 400) {
                            sendButton.innerText = `Sorry: Server Error: HTTP${xhttp.status}`;
                        }
                    } else {
                        sendButton.innerText = `Sorry: Request was not sent: HTTP${xhttp.status}`;
                    }
                } catch (error) {
                    console.error(error);
                    sendButton.innerText = `Sorry, unexpected Server Error: HTTP${xhttp.status}`;
                }
            }
        };
        const activationStart = encodeURIComponent(`${TEXT_REGISTER_MAIL_BODY_START}<a href="${CALLBACKPAGE_USER_REGISTRATION}?register=REGISTRATION_VALIDATION_CODE&cr1=CRVALUE1&cr2=CRVALUE2&mail=EMAIL">`);
        const activationEnd = encodeURIComponent(`</a>${TEXT_REGISTER_MAIL_BODY_END}`);
        console.log('activationStart: ' + activationStart);
        console.log('activationEnd: ' + activationEnd);
        const body = `email=${userEmail}&mailbodymessagestart=${activationStart}&module=9000&mailbodymessageend=${activationEnd}&mailsubjectmessage=${TEXT_REGISTER_MAIL_SUBJECT}&callbackpage=${CALLBACKPAGE_USER_REGISTRATION}&jsonurl=${JSON_HTML_URL}`;
        console.log('body: ' + body);
        xhttp.send(body);
    } else {
        console.error('invalid email');
        sendButton.innerText = "Invalid Email";
    }


    registrationEmailField.style.visibility = "hidden";


    sendButton.setAttribute("class", "bi bi-envelope-paper");
}

const logout = function () {
    //TODO
    // do logout
    const register = document.getElementById("signup");
    register.style.visibility = "visible";
    const signin = document.getElementById("signout");
    signin.setAttribute("id", "signin");
    signin.setAttribute("class", "bi bi-door-open");
    signin.setAttribute("onclick", "login()");
    signin.style.visibility = "visible";
    signin.innerText = "ENTER";
    const loginUserIcon = document.getElementById("loginUserIcon");
    loginUserIcon.style.visibility = "hidden";
    window.location.reload();
}