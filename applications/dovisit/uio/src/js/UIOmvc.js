const DEBUG_COMMIT = false;

const Constants = {
    POSITION_CORRECTION_FACTOR: 20,
    CM_PER_INCH: 2.54,
    PIXEL_IN_CM: 0.0264583333,
    ICON_INPUT: "bi bi-box-arrow-in-down-right",
    ICON_OUTPUT: "bi bi-box-arrow-up-right",
    ICON_WINDOW: "bi bi-window",
    ICON_WINDOW_STACK: "bi bi-window-stack",
    ICON_QUESTION_MARK: "bi bi-patch-question-fill",
    ICON_SELECTION_LIST: "bi bi-card-checklist",
    ICON_RADIO_CHECK_BOX: "bi bi-ui-checks-grid",
    ICON_LINK: "bi bi-link",
    ICON_BUTTON: "bi bi-menu-button-wide-fill",
    STYLE_BORDER_INFO: "border border-info rounded",
    ICON_SMALL_SIZE: "0.75em",
    ICON_MEDIUM_SIZE: "1.1em",
    ICON_LARGE_SIZE: "2.25em",

    UIOWAREPROJECTS: "UIOWareprojects",
    UIOWARE_CURRENT_PROJECT: "UIOWare_currentProject",
    ICON_TEXTAREA: "bi bi-textarea-t",
    ICON_TEXTFIELD: "bi bi-input-cursor-text",
    ICON_TABLE: "bi bi-table",
    ICON_TEXT_CHAPTER: "bi bi-blockquote-right",
    ICON_IMAGE: "bi bi-images",
    ICON_BACKEND: "bi bi-server",
    ICON_FRONTEND: "bi bi-app",
    ICON_APPLICATION: "bi bi-file-binary"
};

const isInt = function (value) {
    return !isNaN(value) &&
        parseInt(Number(value)) == value &&
        !isNaN(parseInt(value, 10));
};
let sessionHasChildrenPositionListener = false;
const UIOModelViewOperation = {
    CREATE_WIDGET: "newWidget",
    DELETE_WIDGET: "deleteWidget",
    RESIZE_WIDGET: "resizeWidget",
    MOVE_WIDGET: "moveWidget",
    UPDATE_WIDGET: "updateWidget",
    ASSIGN_PARENT: "assignParent",
    PAINT_WIDGET: "placeWidgetOnScreen",
    HIDE_WIDGET: "hideWidget",
    CLONE_WIDGET: "cloneWidget",
    CREATE_PAGE: "createUioPage",
    LOAD_PAGE: "loadUioPage",
    DELETE_PAGE: "deleteUioPage",
    UPDATE_PAGE: "updateUioPage"
};

const
    UIOWareWidget =
        {
            UIOWareMockTextField: "mTxtfld",
            UIOWareTextField: "Textfield",
            UIOWareTextarea: "Textarea",
            UIOWareOutput: "Output",
            UIOWareInput: "Input",
            UIOWareTable: "Table",
            VLineUiWare: "vLine",
            HLineUiWare: "hLine",
            EditableFieldUIWare: "editableField",
            UIOWareWindow: "Window",
            UIOWareMockTextarea: "mTextarea",
            UIOWareMockSelectionList: "mSelect",
            UIOWareMockRadioCheckBox: "mRCkBox",
            UIOWareMockLink: "mLink",
            UIOWareMockButton: "mBtn",
            UIOWareTextChapter: "mTxtPrgrph",
            UIOWareImage: "image",
            UIOWareBackend: "Backend",
            UIOWareFrontend: "Frontend",
            UIOWareApplication: "Application"
        };

const UIOWarePageType = {
    SECTION: "section",
    OVERFLOW: "overflow",
    FILE: "file"
}

