const callUioApi = function (action, userID, project, callbackFunction) {
    let url, httpMethod, content;
    if (!project) throw new Error(`No project data was passed`);
    const commitOperation = action.toLowerCase().trim();
    const api = getAPIServerPath() + "900/9/";
    switch (commitOperation) {
        case "list":
            httpMethod = "GET";
            url = api + "9000/4/_nul_/x/x/public";
            break;
        case "create":
            httpMethod = "PUT";
            url = api + "9000/1";
            //sessionToken=sessionStorage.getItem("listo_" + customerID + "_" + applicationID + "_" + currentDovisUserID);
            break;
        case "load":
            httpMethod = "POST";
            url = api + "9000/2/";
            break;
        case "save":
            httpMethod = "POST";
            if (!project) throw new Error("no project-data was set to be posted");
            content = `&jsoncontent=${encodeURIComponent(JSON.stringify(project))}`
            url = api + "9000/3";
            break;
        default:
            throw new Error(`Invalid commit-operation '${action}'. Only create,load and save are allowed.`);
    }
    let params;
    if (commitOperation === "load") {
        params = `filename=${encodeURIComponent(project)}`;
    } else if (commitOperation !== "list") {
        const projectName = project['projectName'];
        const regex = /[^\w]/g;
        let cleanProjectName = projectName.replace(regex, "").toLocaleLowerCase().trim();
        const fileName = `uio${userID}.${cleanProjectName}.json`;
        params = `filename=${encodeURIComponent(fileName)}`;
        if (commitOperation === "save") params += content;
    }
    const xhttp = new XMLHttpRequest();
    console.info(`Opening url '${url}' with method '${httpMethod}'...`);

    xhttp.onreadystatechange = function () {
        if (this.readyState === 4) {
            const responseObject = JSON.parse(this.response);
            console.info(`xhttp-response: ${this.response}`);
            return callbackFunction(this.status, responseObject);

        }
    };
    xhttp.open(httpMethod, url, true);
    xhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
    xhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhttp.setRequestHeader("Authorization", "Bearer " + sessionToken);
    //if (commitOperation !== "list") xhttp.send(params);
    //else
    xhttp.send();
}
