"use strict";
jQuery(document).on('click', '.listoPayCreateTicket', function () {
    const currentUrl = window.location.href;
    sessionStorage.setItem("payment_callingPage", currentUrl);
    const productID = $(this).attr('data-productID');
    const productName = $(this).attr('data-productName');
    const productDescription = $(this).attr('data-productDescription');
    const price = $(this).attr('data-price') / 100;
    const currency = $(this).attr('data-currency');
    const qty = $(this).attr('data-qty');
    const customText = $(this).attr('data-customText');
    const qrText = createQRText(productID, productName, productDescription, price, currency, qty, customText);
    makeCode(qrText);
});

$("#firstName").on("blur", function () {
    const qrText = getCurrenQRText();
    makeCode(qrText);
}).on("keydown", function (e) {
    if (e.keyCode == 13) {
        const qrText = getCurrenQRText();
        makeCode(qrText);
    }
});
$("#lastName").on("blur", function () {
    const qrText = getCurrenQRText();
    makeCode(qrText);
}).on("keydown", function (e) {
    if (e.keyCode == 13) {
        const qrText = getCurrenQRText();
        makeCode(qrText);
    }
});

function getCurrenQRText() {
    const productID = document.getElementById("selectedProductID").innerText;
    const price = document.getElementById("priceOfselectedProduct").innerText;
    const productName = document.getElementById("selectedProduct").innerText;
    const productDescription = document.getElementById("descOfselectedProduct").innerText;
    const currency = document.getElementById("currency").innerText;
    const qty = document.getElementById("quantity").innerText;
    const firstName = document.getElementById("firstName").value;
    const lastName = document.getElementById("lastName").value;
    const seller = document.getElementById("shopName");
    const customText = document.getElementById("customText");
    const uid = uuidv4();
    let qrText = "##: " + qty + " | " + productID + " | " + productName + " | " + productDescription + " | " + price + " | " + currency + "[" + firstName
        + " " + lastName + "][Ticket-id:  " + uid + "][Seller: " + seller + "]\n" + customText;
    return qrText;
}

function createQRText(productID, productName, productDescription, price, currency, qty, customText) {
    document.getElementById("selectedProductID").innerText = productID;
    document.getElementById("selectedProduct").innerText = productName;
    document.getElementById("descOfselectedProduct").innerText = productDescription;
    document.getElementById("priceOfselectedProduct").innerText = price;
    document.getElementById("currency").innerText = currency;
    document.getElementById("quantity").innerText = qty;
    document.getElementById("customText").innerText = customText;
    const seller = document.getElementById("shopName").innerText;
    const firstName = document.getElementById("firstName").value;
    const lastName = document.getElementById("lastName").value;
    const uid = uuidv4();
    let qrText = "##: " + qty + " | " + productID + " | " + productName + " | " + productDescription + " | " + price + " | " + currency + "[" + firstName
        + " " + lastName + "][Ticket-id:  " + uid + "][Seller: " + seller + "]\n" + customText;
    return qrText;
}

function makeCode(text) {
    document.getElementById("qrcode").innerText = "";
    var qrcode = new QRCode(document.getElementById("qrcode"), {
        width: 100,
        height: 100
    });

    qrcode.makeCode(text);
}

jQuery(document).on('click', '.listoPayCheckout', function () {
    console.log("Trying to check out customer...");
    let firstName = document.getElementById("firstName")
    let lastName = document.getElementById("lastName")
    let labelFirstName = document.getElementById("labelFirstName")
    let labelLastName = document.getElementById("labelLastName")


    if (!firstName.value) {
        labelFirstName.innerText=getTextById(725);
        firstName.focus();
        return;
    }
    if (!lastName.value) {
        labelLastName.innerText=getTextById(725)
        lastName.focus();
        return;
    }
    const stripe_account = document.getElementById("stripeCustomerID").innerText;
    const productID = document.getElementById("selectedProductID").innerText;
    const price = document.getElementById("priceOfselectedProduct").innerText;
    const productName = document.getElementById("selectedProduct").innerText;
    const productDescription = document.getElementById("descOfselectedProduct").innerText;
    const currency = document.getElementById("currency").innerText;
    const qty = document.getElementById("quantity").innerText;

    console.log("Checkout clicked:\nStripe-account:" + stripe_account + "\n productID:" + productID + ",\n price:" + price + ",\n product: " + productName + ",\nDescription: " +
        productDescription + ",\n" + currency + ",\nqty: " + qty);
    const priceWDigits = price * 100;
    stripeConnectPayment(stripe_account, 1, productID, priceWDigits, productName, productDescription, currency, qty, function (data) {
        console.log("Data returned from stripe: " + JSON.stringify(data));
        const stripe_session_id = data.id;
        if (!stripe_session_id) {
            console.error("NO STRIPE SESSION ID FOUND: " + JSON.stringify(data));
            //showHttpErrorMessage("main-content", data);
        }
        console.log("Returned session id:" + stripe_session_id);
        connectCheckout("" + stripe_session_id, "" + stripe_account).then(function (data) {
            console.log("checkout completed");
            console.log("Data returned from session: " + JSON.stringify(data));
        }, function (err) {
            console.error("checkout failed: " + err);
        });
    }, function (err) {
        console.error(err);
    });

});