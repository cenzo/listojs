/*const LAYOUT_SUB_TYPE = {
    STRUCTURE: "structure",
    COLOR: "color",
    IMAGE: "image",
    FONT: "font",
    TEXT: "text"
};
let layOutData;
setLayoutTexts();
let currentView = null;

function setLayoutTexts() {
    setElemText(200);
    setElemText(201);
    setElemText(202);
    setElemText(203);
    setElemText(204);
    setElemText(262);
}

let selectedLayoutId = null;

function getSelectedLayoutId() {
    return selectedLayoutId ? selectedLayoutId : (sessionStorage.getItem("selectedLayoutId") ? sessionStorage.getItem("selectedLayoutId") : "2");
}
function setSelectedLayoutId(_selectedLayoutId) {
    selectedLayoutId = _selectedLayoutId;
}

/**
 * common code for show listing of layout element
 * @param type
 * @param successData
 * /
function showListForLayout(type, data) {
    let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
            const tableDiv = [formRowDiw, "tableDivID", 12];
            const iFrameDiv = [formRowDiw, "iFrameDivID", 12];
            layOutData =successData;
            //TODO add event-listener for onChange of listbox (point nr 4 of specification)
            // just for example I select here hardcoded the second theme/layout
            selectedTheme = successData.rows[2].s[7];
            const listBox = [formSelectBox, 262, "get-Theme", "listBoxID", successData.rows[0].s[6], successData.rows[1].s[6],successData.rows[2].s[6]];
            //TODO add event-listener for click of refresh-Button (point nr 7 of specification)
            const refreshButton = [FormButton, 600, "refreshButtonId", "", "refresh-preview",];
            const action1 = ["create-edit" + type + "-form", actionModifyStyle.elements[0] + getTextById(105) + actionModifyStyle.elements[1]];
            const action2 = ["set-default-" + type + "", actionDeleteStyle.elements[0] + getTextById(216) + actionDeleteStyle.elements[1]];
            const dataColumnNames1 = ["id", "layoutid", "elementlabel", "cssrootelement", "value", "name", "typeid", "alt_text", "defaultvalue"];
            const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 7, 8];
            const dataColumnNames2 = ["id"];
            const dataColumnIndices2 = [0];
            const admin = {
                IDENTIFIER: "admin-edit-layout-" + type,
                jsonRMLData: data,
                header: [getTextById(201), getTextById(205), getTextById(202), getTextById(203),'',''],
                columnIndices: [2, 3, 4, 8],
                actions: [action1, action2],
                dataColumnIndices: [dataColumnIndices1, dataColumnIndices1],
                dataColumnNames: [dataColumnNames1, dataColumnNames1],
                rowFormat: function (rowIndex, dataArray) {
                    return dataArray;
                },
                cellFormat : function(colIndex,rowIndex,TDopen,data,TDclose){
                    {
                        if (type == "images" && colIndex === 2 ) {
                            return TDopen + "<img src='"+getImagePath()+"/" + data
                                + "' "+ThumbStyle.default +">" + TDclose;
                        } else {
                            return TDopen + data + TDclose;
                        }
                    }
                }
            };
            const theForm = [listBox, tableDiv, refreshButton, iFrameDiv];
            listoHTML.createForm("#main-content", 12, theForm, "layoutEdit");
            listoHTML.createTable.call(admin, "tableDivID");
            const lpT = layoutPageTemplate("150%", "500px", selectedTheme,"layoutFrame");
            $("#iFrameDivID").html(lpT);
            setDefaultSelectedLayoutValue();
        }, function err(data) {
            showHttpErrorMessage(data._rc);
        }
    );
};

function setDefaultSelectedLayoutValue(){
    if(!layOutData) {return;}
    const layoutIdArray = [];
    const layoutNameArray = [];
    for(let i=0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutName = layoutNameArray[layoutIdArray.indexOf(getSelectedLayoutId())];
    $("#listBoxID").val(_selectedLayoutName);
}

/**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
function initDataTableForLayoutElems(_DataFunction, layoutId, layoutType) {
    currentView = layoutType;
    _DataFunction(layoutId, function (successData) {
        showListForLayout(layoutType, successData);
    }, function (errorData) {
        showHttpErrorMessage("main-content", errorData);
    });
}

function initLayoutFunction(_type) {
    switch (_type) {
        case "texts" :
            initDataTableForLayoutElems(apiCall_listorante_admin_layouttexts, getSelectedLayoutId(), "texts");
            break;
        case "colors":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutcolors, getSelectedLayoutId(), "colors");
            break;
        case "fonts":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutfonts, getSelectedLayoutId(), "fonts");
            break;
        case "structures":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutstructures, getSelectedLayoutId(), "structures");
            break;
        case "images":
            initDataTableForLayoutElems(apiCall_listorante_admin_layoutimages, getSelectedLayoutId(), "images");
            break;
        default:
            break;
    }
}

/**
 * show layout structure
 * /
function showLayoutStructures() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutstructures, getSelectedLayoutId(), "structures");
}

/**
 * show layout colors
 */
function showLayoutColors() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutcolors, getSelectedLayoutId(), "colors");
}

/**
 * show layout images
 * /
 function showLayoutImages() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutimages, getSelectedLayoutId(), "images");
}

 /**
 * show layout fonts
 * /
 function showLayoutFonts() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layoutfonts, getSelectedLayoutId(), "fonts");
}

 /**
 * show layout text
 * /
 function showLayoutText() {
    initDataTableForLayoutElems(apiCall_listorante_admin_layouttexts, getSelectedLayoutId(), "texts");
}

 /**
 * bind structure list events
 *
 * /
 $(document).on('click', '.layout-list', function () {
    const layoutSubType = $(this).data("type");
    switch (layoutSubType) {
        case LAYOUT_SUB_TYPE.STRUCTURE :
            showLayoutStructures();
            break;
        case LAYOUT_SUB_TYPE.COLOR :
            showLayoutColors();
            break;
        case LAYOUT_SUB_TYPE.IMAGE :
            showLayoutImages();
            break;
        case LAYOUT_SUB_TYPE.FONT :
            showLayoutFonts();
            break;
        case LAYOUT_SUB_TYPE.TEXT :
            showLayoutText();
            break;
    }

});
 const saveLayoutData = (function () {
    return function (saveData) {
        return new Promise(function (resolve, reject) {
            apiCall_listorante_admin_updatelayout(saveData.layoutelementname , saveData.layoutId, saveData.value, function (data) {
                resolve(data);
            }, function (error) {
                reject(error)
            });
        });
    }
})();

 $(document).on('click', '.create-editfonts-form', function () {
    generateForm(this, "fonts")
});
 $(document).on('click', '.create-editstructures-form', function () {
    generateForm(this, "structures")
});
 $(document).on('click', '.create-editcolors-form', function () {
    generateForm(this, "colors")
});
 $(document).on('click', '.create-edittexts-form', function () {
    generateForm(this, "texts")
});
 $(document).on('click', '.create-editimages-form', function () {
    generateForm(this, "images")
});

 $(document).on('change', '#listBoxID', function () {
    const selectdLayoutName = $(this).val();
    const layoutIdArray = [];
    const layoutNameArray = [];
    for(let i=0; i < layOutData.rows.length; i++) {
        layoutNameArray.push(layOutData.rows[i].s[6]);
        layoutIdArray.push(layOutData.rows[i].s[0]);
    }
    const _selectedLayoutId = layoutIdArray[layoutNameArray.indexOf(selectdLayoutName)];
    refreshIframeDataByFromApiSelectedLayoutData(_selectedLayoutId);
    apiCall_listorante_admin_setlayoutid(_selectedLayoutId,function (successData) {
        sessionStorage.setItem("selectedLayoutId", _selectedLayoutId);
        setSelectedLayoutId(_selectedLayoutId);
        initLayoutFunction(currentView);
    }, function err(data) {
        showHttpErrorMessage(data._rc);
    });

});

 function generateForm(that, layoutType) {
    currentView = layoutType;
    const iFrameDiv = [formRowDiw, "iFrameDivID", 12];
    const refreshButton = [FormButton, 600, "refreshEditButtonId", "", "refresh-edit-preview"];
    const elementLabel = $(that).data("elementlabel");
    const id = $(that).data("id");
    const cssVariable=$(that).data("cssrootelement");
    const value = $(that).data("value");
    const defaultValue = $(that).data("defaultvalue");
    const alt_Text = $(that).data("alt_text");
    debug("create edit " + layoutType + " form", 0, elementLabel, id);
    const layoutElementName = [formTextInput, 201, "layout_" + layoutType + "_element_name", 'rows="1" required disabled ' +
    'alt="' + alt_Text + '" title="' + alt_Text + '"',
        elementLabel];
    let layoutValue = null, layoutDefaultValue = null;
    const imageSrc = $(this).data('layout_images_default_value');

    if (layoutType === "texts") {
        layoutValue = [FormTextArea, 202, "layout_" + layoutType + "_value", "rows=2 required ", value];
        layoutDefaultValue = [FormTextArea, 203, "layout_" + layoutType + "_default_value", "rows=2 disabled required ", defaultValue];
    } else if(layoutType === 'images'){
        layoutValue  = [formImage, 202, "layout_" + layoutType + "_value", "","","Choose Image"];
        layoutDefaultValue = [formDisplayImage, 203, "layout_" + layoutType + "_default_value", "longdesc=\"" + value + "\"", getImagePath() + "/" + value,"Default Image"];

    } else {
        layoutValue = [formTextInput, 202, "layout_" + layoutType + "_value", 'rows="1" required ' +
        'alt="' + alt_Text + '" title="' + alt_Text + '"',
            value];
        layoutDefaultValue = [formTextInput, 203, "layout_" + layoutType + "_default_value", 'rows="1" required disabled ',
            defaultValue];
    }
    const cssField = [formTextInput, 205, "layout_" + layoutType + "_cssrootelement", 'rows="1" required disabled',
        cssVariable];

    const hidden_layout_id = [formHiddenInput, "", id, "id_of_edited_layout_" + layoutType + ""];
    const submitButton = [FormButton, 215, "update-layout-" + layoutType + "", "", "update-layout-" + layoutType + ""];
    const form1 = [layoutElementName, cssField, layoutValue, layoutDefaultValue, hidden_layout_id, submitButton,refreshButton,iFrameDiv];
    listoHTML.createForm("#main-content", 12, form1, "layout-" + layoutType + "-edit-form");
    let selectedTheme = "";
    apiCall_listorante_public_layouts(function (successData) {
        selectedTheme =successData.rows[2].s[7];
        const lpT = layoutPageTemplate("150%", "500px", selectedTheme,"layoutFrame");
        $("#iFrameDivID").html(lpT);
        refreshIframeDataBySelectedLayoutData(getSelectedLayoutId());
    }, function err(data) {
        showHttpErrorMessage(data._rc);
    });
}

 const updateLayoutButtonAction = (function () {
    return function (type, value) {
        let layoutelementname = $('#layout_' + type + '_cssrootelement').val();
        let layoutelementvalue = $('#layout_' + type + '_value').val();
        /**
 * removing first dashes
 * @type {void | string | never}
 * /
 layoutelementname = layoutelementname.replace("--", "");
 layoutelementvalue = layoutelementvalue.replace(/#|_/g, '');
 const _DataRow = {
            layoutelementname: layoutelementname,
            value: value || layoutelementvalue,
            layoutId: getSelectedLayoutId()
        };
 apiCall_listorante_admin_updatelayout(escape(_DataRow.layoutelementname), _DataRow.layoutId, escape(_DataRow.value), function (data) {
            initLayoutFunction(type);
        }, function (error) {
            showHttpErrorMessage("main-content", error);
        });
 }
 })();


 const resetLayoutButtonAction = (function () {
    return function (type, that) {
        let layoutelementname = $(that).data("cssrootelement");
        let layoutelementvalue = $(that).data("defaultvalue");
        layoutelementname = layoutelementname.replace("--", "");
        layoutelementvalue = layoutelementvalue.replace(/#|_/g, '');
        if (type == 'images') {
            // to get file name from actual default "css property values"
            const urrls = layoutelementvalue.split(",");
            const url__ = urrls[0];
            const filePath = url__.replace("url('", "").replace("')", "");
            layoutelementvalue = filePath.substring(filePath.lastIndexOf('/') + 1);
        }
        const _DataRow = {
            layoutelementname: layoutelementname,
            value: layoutelementvalue,
            layoutId: getSelectedLayoutId()
        };
        apiCall_listorante_admin_updatelayout(escape(_DataRow.layoutelementname), _DataRow.layoutId, escape(_DataRow.value), function (data) {
            initLayoutFunction(type);
        }, function (error) {
            showHttpErrorMessage("main-content", error);
        });
    }
})();

 $(document).on('click','.update-layout-structures',function () {
    updateLayoutButtonAction("structures");
});

 $(document).on('click','.update-layout-colors',function () {
    updateLayoutButtonAction("colors");
});

 $(document).on('click','.update-layout-fonts',function () {
    updateLayoutButtonAction("fonts");
});

 $(document).on('click','.update-layout-texts',function () {
    updateLayoutButtonAction("texts");
});

 $(document).on('click','.update-layout-images',function () {
    const custID = getCustomerId();
    if (document.getElementById('layout_images_value').value) {
        upload(custID, 'layout-images-edit-form', function (data) {
            debug("upload-data:", release, data);
            const imageNew = "" + data.rows[0].s[2];
            const image_thumbNew = imageNew;
            debug(getTextById(255) + document.getElementById('layout_images_value').value + getTextById(256) + imageNew + "'", 0);
            updateLayoutButtonAction("images",image_thumbNew);
        }, function (error) {
            alert(getTextById(257) + document.getElementById('layout_images_value').value + getTextById(258) + error.status);
        });
    }
});

 $(document).on('click','.set-default-structures',function () {
    resetLayoutButtonAction("structures", this);
});

 $(document).on('click','.set-default-colors',function () {
    resetLayoutButtonAction("colors", this);
});

 $(document).on('click','.set-default-fonts',function () {
    resetLayoutButtonAction("fonts", this);
});

 $(document).on('click','.set-default-texts',function () {
    resetLayoutButtonAction("texts", this);
});
 $(document).on('click','.set-default-images',function () {
    resetLayoutButtonAction("images", this);
});

 $(document).on('click','.refresh-preview',function () {
     refreshIframeDataByFromApiSelectedLayoutData(getSelectedLayoutId());
     refreshIframeTextDataByFromApiSelectedLayoutData(getSelectedLayoutId());
});
 $(document).on('click','.refresh-edit-preview',function () {
    updateEditRootElementCssStyleElement();
});

 function getLayoutDataByType(functionDefinition, layoutId) {
    return new Promise(function(resolve, reject){
        functionDefinition(layoutId, resolve, reject);
    })
}
 /**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
 function refreshIframeDataByFromApiSelectedLayoutData(layoutId, layoutType) {
    apiCall_listorante_admin_updatecss(layoutId,function (successData) {
        apiCall_listorante_admin_getcss(layoutId,function (layoutTypeData) {
            setLayoutTypeValuesForPreviewIframe(layoutTypeData);
        }, function err(data) {
            showHttpErrorMessage(data._rc);
        });
    },function err(data) {
        showHttpErrorMessage(data._rc);
    });
}
 /**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
 function refreshIframeTextDataByFromApiSelectedLayoutData(layoutId, layoutType) {
    apiCall_listorante_admin_layouttexts(layoutId,function (layoutTypeData) {
        replaceApiIframeTextFromLayout(layoutTypeData,layoutType);
     },function err(data) {
        showHttpErrorMessage(data._rc);
    });
}
 /**
 * common init datatable function
 * @param _DataFunction
 * @param layoutId
 * @param layoutType
 * /
 function refreshIframeDataBySelectedLayoutData(layoutId, layoutType) {
    const allLayoutDataPromises = [
        getLayoutDataByType(apiCall_listorante_admin_layoutcolors, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layouttexts, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layoutfonts, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layoutstructures, layoutId),
        getLayoutDataByType(apiCall_listorante_admin_layoutimages, layoutId)
    ];
    Promise.all(allLayoutDataPromises).then(function(layoutTypeData){
        setLayoutTypeValuesForPreview(layoutType, layoutTypeData);
    }).catch(function (errors) {
    })
}
 var setLayoutTypeValuesForPreview = (function(){
    return function(type, layoutTypeData){
        const iFrameData = {};
        for(let i=0; i < layoutTypeData.length; i++) {
            for (let j = 0; j < layoutTypeData[i].rows.length; j++) {
                const typeKeyName = layoutTypeData[i].rows[j].s[5] + 's';
                if(!iFrameData[typeKeyName]) {
                    iFrameData[typeKeyName] = {};
                }
                iFrameData[typeKeyName][layoutTypeData[i].rows[j].s[3]] = layoutTypeData[i].rows[j].s[4];
            }
        }
        buildIFrameRootCSS(iFrameData, type);
        replaceIframeTextFromLayout(iFrameData);
    }
})();
 var setLayoutTypeValuesForPreviewIframe = (function(){
    return function(layoutTypeData){
        const styleRootElement = getRootElementCssStyleElement();
        if(styleRootElement) {
            styleRootElement.html("");
            const itemCss = [];
            for(let i=0; i < layoutTypeData.rows.length; i++) {
                itemCss.push(layoutTypeData.rows[i].s[0]);
            }
            styleRootElement.append(itemCss.concat(""));
        }
    }
})();
 const getPreviewIframeElement = (function () {
    return function (elementNodeName) {
        const iframeElement = $('#layoutFrame');
        if (iframeElement.length > 0) {
            return iframeElement.contents().find(elementNodeName)
        } else {
            return null;
        }
    }
})();

 function CssParser(){
}
 CssParser.prototype.removeComments= function(css) {
    return css.replace(/\/\*(\r|\n|.)*\*\//g,"");
};
 CssParser.prototype.parseCSSBlock = function(css) {
    const rule = {};
    const declarations = css.split(';');
    declarations.pop();
    const len = declarations.length;
    for (let i = 0; i < len; i++)
    {
        const loc = declarations[i].indexOf(':');
        const property = $.trim(declarations[i].substring(0, loc));
        const value = $.trim(declarations[i].substring(loc + 1));

        if (property != "" && value != "")
            rule[property] = value;
    }
    return rule;
};
 CssParser.prototype.parseCSS = function(css) {
    const rules = {};
    css = this.removeComments(css);
    const blocks = css.split('}');
 blocks.pop();
 const len = blocks.length;
 for (let i = 0; i < len; i++)
 {
        const pair = blocks[i].split('{');
        rules[$.trim(pair[0])] = this.parseCSSBlock(pair[1]);
    }
    return rules;
};
 var getRootElementCssStyleElement = (function(){
    return function(){
        const iframeHeadElement = getPreviewIframeElement("head");
        if (iframeHeadElement.length > 0) {
            let styleRootElement = iframeHeadElement.find("#iFrameLayoutStyle");
            if(styleRootElement.length === 0){
                styleRootElement = $("<style></style>");
                styleRootElement.prop("id", "iFrameLayoutStyle");
                styleRootElement.prop("type", "text/css");
                iframeHeadElement.append(styleRootElement)
            }
            return styleRootElement;
        } else {
            return null;
        }
    }
})();
 function updateEditRootElementCssStyleElement() {
    const layoutTypes = ["structures", "colors", "images", "fonts"];
    if(layoutTypes.indexOf(currentView) > -1) {
        const layoutelementname = $('#layout_' + currentView + '_cssrootelement').val();
        const layoutelementvalue = $('#layout_' + currentView + '_value').val();
        const styleRootElement = getRootElementCssStyleElement();
        if(styleRootElement) {
            const pastRules = styleRootElement[0].innerHTML;
            const _cssParser = new CssParser();
            const oldCssRules = _cssParser.parseCSS(pastRules);
            let rootElementVariableString = ":root {\n";
            const oldRootCssRules = oldCssRules[":root"];
            for (let key in oldRootCssRules) {
                if(currentView === "images") {
                    if(layoutelementname === key) {
                        const imageUrl = "url('../assets/images/" + layoutelementvalue + "'), url('../assets/images/" + layoutelementvalue + "')";
                        rootElementVariableString += "\t"+(key+" : "+imageUrl+"; \n");
                    } else {
                        rootElementVariableString += "\t"+(key+" : "+oldRootCssRules[key]+"; \n");
                    }
                } else {
                    if(layoutelementname === key) {
                        rootElementVariableString += "\t"+(key+" : "+layoutelementvalue+"; \n");
                    } else {
                        rootElementVariableString += "\t"+(key+" : "+oldRootCssRules[key]+"; \n");
                    }
                }
            }
            rootElementVariableString += "}";
            styleRootElement[0].innerHTML = rootElementVariableString;
        }
    } else {
        // update text here.
    }
}
 function buildIFrameRootCSS(layoutItemValues) {
    const rootElementCss = getRootElementCssVariableValues(layoutItemValues);
    const styleRootElement = getRootElementCssStyleElement();
    if(styleRootElement) {
        styleRootElement[0].innerHTML = rootElementCss;
    }
}
 function buildApiDataIFrameRootCSS(layoutItemValues) {
    const styleRootElement = getRootElementCssStyleElement();
    if(styleRootElement) {
        styleRootElement[0].innerHTML = layoutItemValues;
    }
}
 function getRootElementCssVariableValues(layoutItemValues) {
    const layoutTypes = ["structures", "colors", "images", "fonts"];
    let rootElementVariableString = ":root {\n";
    for (let i=0; i<layoutTypes.length; i++) {
        //rootElementVariableString += "\n/ * * dynamic variable started : "+layoutTypes[i]+"**/
/*
    for (let key in layoutItemValues[layoutTypes[i]]) {
        if(layoutTypes[i] === "images") {
            const imageUrl = "url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "'), url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "')";
            rootElementVariableString += "\t"+(key+" : "+imageUrl+"; \n");
        } else {
            rootElementVariableString += "\t"+(key+" : "+layoutItemValues[layoutTypes[i]][key]+"; \n");
        }
    }
    // rootElementVariableString += "\n/** dynamic variable ended : "+layoutTypes[i]+"**/
/*}
rootElementVariableString += "}";
return rootElementVariableString;
}
function getIFrameRootElementCssVariableValues(layoutItemValues) {
const layoutTypes = ["structures", "colors", "images", "fonts"];
let rootElementVariableString = ":root {\n";
for (let i=0; i<layoutTypes.length; i++) {
    for (let key in layoutItemValues[layoutTypes[i]]) {
        if(layoutTypes[i] === "images") {
            const imageUrl = "url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "'), url('../assets/images/" + layoutItemValues[layoutTypes[i]][key] + "')";
            rootElementVariableString += "\t"+(key+" : "+imageUrl+"; \n");
        } else {
            rootElementVariableString += "\t"+(key+" : "+layoutItemValues[layoutTypes[i]][key]+"; \n");
        }
    }
}
rootElementVariableString += "}";
return rootElementVariableString;
}
function replaceIframeTextFromLayout(layoutItemValues) {
const iframeBodyElement = getPreviewIframeElement("body");
for (let key in layoutItemValues.texts) {
    iframeBodyElement.find("#"+key).text(layoutItemValues.texts[key])+";";
}
}
function replaceApiIframeTextFromLayout(layoutItemValues) {
const iframeBodyElement = getPreviewIframeElement("body");
for (let i=0; i<layoutItemValues.rows.length; i++) {
    iframeBodyElement.find("#"+layoutItemValues.rows[i].s[3]).text(layoutItemValues.rows[i].s[4])+";";
}
}
*/