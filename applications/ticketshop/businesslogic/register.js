module = "register.js";
applicationID = 0;

// temporary workaround:
// delete following two lines, as soon as replication works well
selectedServer = `https://backend2.listorante.com${backendVersion}`;
getAPIServerPath();

/**
 * to parse response from request do check registration
 * @param _successCheckRegistrationResultData
 * @returns {{emailIsAlreadyExists: boolean, desiredSubDomainIsAnAvailableSubDomainForRegistration: boolean, desiredDomainIsAnAvailableDomainForRegistration: boolean, registrationCheckSuccess: boolean}}
 */
function parseCheckRegistrationRequestResult(_successCheckRegistrationResultData) {
    var validationStatus = {
        registrationCheckSuccess: false,
        emailIsAlreadyExists: true,
        desiredDomainIsAnAvailableDomainForRegistration: false,
        desiredSubDomainIsAnAvailableSubDomainForRegistration: false
    };
    for (var i = 0; i < _successCheckRegistrationResultData.length; i++) {
        var resultRow = _successCheckRegistrationResultData[i];
        if (resultRow.variableNames[0] === "message") {
            var subResultRow = resultRow.rows[0];
            validationStatus.emailIsAlreadyExists = subResultRow.s[0].indexOf('is already registered') > -1;
        } else if (resultRow.variableNames[0] === "subdomainmsg") {
            var subResultRow = resultRow.rows[0];
            if (subResultRow.s[1] === "0") {
                validationStatus.desiredSubDomainIsAnAvailableSubDomainForRegistration = true;
            }
        } else if (resultRow.variableNames[0] === "domainmsg") {
            var subResultRow = resultRow.rows[0];
            if (subResultRow.s[1] === "0") {
                validationStatus.desiredDomainIsAnAvailableDomainForRegistration = true;
            }
        }
    }
    validationStatus.registrationCheckSuccess = validationStatus.emailIsAlreadyExists == false && validationStatus.desiredSubDomainIsAnAvailableSubDomainForRegistration && validationStatus.desiredDomainIsAnAvailableDomainForRegistration;
    return validationStatus;
}

/**
 *
 * @param email
 * @param name_of_desired_domain
 * @param isSubDomain
 * @returns {Promise<any>}
 */
function checkRegistrationEmailAndDomain(email, name_of_desired_domain, isSubDomain) {
    let registrationCheckResult;
    return new Promise(function (resolve, reject) {
        apiCall_global_public_checkRegistration(name_of_desired_domain, email, isSubDomain, function (successCheckRegistrationResultData) {
            registrationCheckResult = parseCheckRegistrationRequestResult(successCheckRegistrationResultData);
            if (registrationCheckResult.registrationCheckSuccess) {
                /**
                 * Registration check success
                 * ask if customer wants to register this domain
                 * showmodal
                 */
                resolve({
                    registrationCheckSuccess: registrationCheckResult.registrationCheckSuccess,
                    showEmailNotAvailableError: registrationCheckResult.emailIsAlreadyExists,
                    showDesiredDomainIsAnAvailableDomainForRegistrationError: !registrationCheckResult.desiredDomainIsAnAvailableDomainForRegistration,
                    desiredSubDomainIsAnAvailableSubDomainForRegistration: !registrationCheckResult.desiredSubDomainIsAnAvailableSubDomainForRegistration
                });
            } else {
                /**
                 * Registration check failed , either email already exists or domain is not available
                 */
                reject({
                    registrationCheckSuccess: registrationCheckResult.registrationCheckSuccess,
                    showEmailNotAvailableError: registrationCheckResult.emailIsAlreadyExists,
                    showDesiredDomainIsAnAvailableDomainForRegistrationError: !registrationCheckResult.desiredDomainIsAnAvailableDomainForRegistration,
                    desiredSubDomainIsAnAvailableSubDomainForRegistration: !registrationCheckResult.desiredSubDomainIsAnAvailableSubDomainForRegistration
                });
            }
        }, function (error) {
            /**
             * something went wrong with actual API request
             */
            console.error(error);
            reject({
                registrationCheckSuccess: false,
                showEmailNotAvailableError: registrationCheckResult.emailIsAlreadyExists,
                showDesiredDomainIsAnAvailableDomainForRegistrationError: !registrationCheckResult.desiredDomainIsAnAvailableDomainForRegistration
            });
        })
    });
}

/**
 * Register Button Click Event
 */
$("#registerButton").click(function () {
    var validator = $("#register_form").data("bootstrapValidator");
    validator.validate();
    if (validator.isValid()) {
        var registrationFormData = getRegistrationFormData();
        var registrationPromise = checkRegistrationEmailAndDomain(registrationFormData.email, registrationFormData.name_of_desired_domain, registrationFormData.isSubDomain);
        registrationPromise.then(function (registrationResult) {
            /**
             * do success code here
             */
            $("#confirmationPopup").modal({backdrop: "static", esc: false});
        }).catch(function (registrationError) {
            if (registrationError.showEmailNotAvailableError) {
                $(".email-already-exists-error").parent(".form-group").addClass("has-error");
                $(".email-already-exists-error").show();
            } else {
                $(".email-already-exists-error").parent(".form-group").removeClass("has-error");
                $(".email-already-exists-error").hide();
                if (registrationError.showDesiredDomainIsAnAvailableDomainForRegistrationError) {
                    $("#domainConfirmationPopup").modal({backdrop: "static", esc: false});
                }
            }
            if (registrationError.showDesiredDomainIsAnAvailableDomainForRegistrationError) {
                $(".domain-already-exists-error").parent(".form-group").addClass("has-error");
                $(".domain-already-exists-error").show();
            } else {
                $(".domain-already-exists-error").parent(".form-group").removeClass("has-error");
                $(".domain-already-exists-error").hide();
            }
            /**
             * do error handling for registration check
             */
        })
    }
});

function hideModal(selector) {
    $(selector).modal("hide");
}

/**
 *send registration request
 *
 */
function sendRegistrationRequest() {
    // send registration request
    const registrationFormData = getRegistrationFormData();
    apiCall_global_public_doRegistration(
        registrationFormData.name_of_desired_domain,
        registrationFormData.email,
        registrationFormData.firstName,
        //TODO for later usage
        //registrationFormData.isSubDomain,
        0,
        registrationFormData.lastName,
        registrationFormData.password,
        function (data) {
            $("#registrationRequestSent").show();
            $("#requestRecipient").show();
            $('#register_form').trigger("reset");
            $('#isSubDomain').prop('checked', false).iCheck('update');
            $('#myCheckbox').prop('checked', false).iCheck('update');
            $("#registerButton").hide();
            $("#registration-choice").hide();
            $("#editButton").hide();
            $("#confirmButton").hide();
            console.log("Registraton has been sent.");
            const newCustomerID = data[1].rows[0].s[0];
            debug("new customer id", release, newCustomerID);
            requestNewListoranteInstance(newCustomerID, function () {
                debug("Listorante for new customer id was requested", release, newCustomerID);
            }, function (err) {
                debug("Listorante for new customer could not be requested: "+err.message, release, newCustomerID);
            })
            // success api request
            document.getElementById("text703").hidden = true;
            document.getElementById("text716").hidden = true;
            document.getElementById("register_form").hidden = true;
            const mailB = document.createElement("b");
            mailB.innerText = registrationFormData.email;
            document.getElementById("text732").appendChild(mailB);
        },
        function (_Failoredata) {
            // something went wrong
            debug("Unknown error: " + _Failoredata, release);
        }
    )
}

$("#confirmationPopup #modal-btn-confirm").click(function () {
    sendRegistrationRequest();
    hideModal("#confirmationPopup");
});
$("#confirmationPopup #modal-btn-no").click(function () {
    console.log("users doesn't want to buy");
    hideModal("#confirmationPopup");
});
$("#domainConfirmationPopup #modal-btn-confirm").click(function () {
    // re-edit code
    $("#editButton").trigger("click");
    hideModal("#domainConfirmationPopup");
});
$("#domainConfirmationPopup #modal-btn-no").click(function () {
    // don't want to check another domain
    $("#registration-choice").show();
    $("#firstName, #lastName, #email, #password, #repassword, #name_of_desired_domain").prop("disabled", true);
    $("#myCheckbox, #isSubDomain").iCheck("disable");
    hideModal("#domainConfirmationPopup");
    $("#confirmButton").show();
    $("#registerButton").hide();
});
/**
 * Edit Button Click Event
 */
$("#editButton").click(function () {
    $("#firstName, #lastName, #email, #password, #repassword, #name_of_desired_domain").prop("disabled", false);
    $("#isSubDomain, #registrationChoice_1, #registrationChoice_2, #registrationChoice_3").iCheck("enable");
    $("#registration-choice").hide();
    $("#editButton").hide();
    $("#confirmButton").hide();
    $("#registerButton").show();
});

/**
 * Confirm Button Click Event
 */
// TODO change for later implementation
//$("#confirmButton").click(function(){
$("#registerButton").click(function () {
    console.log("Registration clicked");
    var validator = $("#register_form").data("bootstrapValidator");
    validator.validate();
    if (validator.isValid()) {
        /**
         * here is the main issue, should we send registration request or
         * do we have any other API end point which accepts registration Option as param
         * for now we used send registration request
         **/
        sendRegistrationRequest();
    }
});

/**
 * Register Form Valid function
 */
function registerFormValidFunction() {
    $("#firstName, #lastName, #email, #password, #repassword, #name_of_desired_domain").prop("disabled", true);
    $("#myCheckbox, #isSubDomain").iCheck("disable");
    $("#registerButton").hide();
    $("#editButton, #confirmButton").show();
}

/**
 * To check domain is valid or not
 * @param name_of_desired_domain
 * @returns {*}
 */
function isValidDomain(name_of_desired_domain) {
    // TODO change for later implementation
    //var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/);
    //return name_of_desired_domain.match(re);
    return true;
}

/**
 * To check sub domain is valid or not, check only string and numbers, without special characters
 * @param name_of_desired_domain
 * @returns {*}
 */
function isValidSubDomain(name_of_desired_domain) {
    // TODO change for later implementation
    //var re = new RegExp(/^\s*[a-zA-Z0-9,\s]+\s*$/);
    //return name_of_desired_domain.match(re);
    return true;
}

/**
 * get registration for data
 */
function getRegistrationFormData() {
    return {
        firstName: $("#firstName").val(),
        lastName: $("#lastName").val(),
        email: $("#email").val(),
        password: "dummyPassword",
        // TODO change for later implementation
        //password : $("#password").val(),
        // name_of_desired_domain : $("#name_of_desired_domain").val(),
        // isSubDomain : $("#isSubDomain").is(":checked") ? "1" : "0",
        name_of_desired_domain: $("#email").val(),
        isSubDomain: "0",
        registrationChoice: $("[name=registrationChoice]:checked").val(),
    }
}

$(document).ready(function () {
    /**
     * for validator
     */
    $("#register_form").bootstrapValidator({
        feedbackIcons: {
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            firstName: {
                validators: {
                    notEmpty: {
                        message: getTextById(725)
                    }
                }
            },
            lastName: {
                validators: {
                    notEmpty: {
                        message: getTextById(725)
                    }
                }
            },
            restaurantName: {
                validators: {
                    notEmpty: {
                        message: getTextById(725)
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: getTextById(725)
                    },
                    emailAddress: {
                        message: getTextById(268)
                    },
                   // patternMismatch: {
                   //     message: getTextById(268)
                   // },
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: getTextById(725)
                    },
                    identical: {
                        field: 'repassword',
                        message: getTextById(608)
                    }
                }
            },
            repassword: {
                validators: {
                    notEmpty: {
                        message: getTextById(725)
                    },
                    identical: {
                        field: 'password',
                        message: getTextById(608)
                    }
                }
            },
            // TODO postponed to later usage
            /* name_of_desired_domain: {
                 validators: {
                     notEmpty: {
                         message: getTextById(118)
                     },
                     callback: {
                         message: getTextById(119),
                         callback: function(value, validator, $field) {
                             if (value === '') {
                                 return true;
                             }


                              // Check the password strength

                             if($("#isSubDomain").is(":checked")) {
                                 if (!isValidSubDomain(value)) {
                                     return {
                                         valid: false,
                                         message: getTextById(123)
                                     };
                                 } else {
                                     return true;
                                 }
                             } else {
                                 if (!isValidDomain(value)) {
                                     return {
                                         valid: false,
                                         message: getTextById(119)
                                     };
                                 } else {
                                     return true;
                                 }
                             }
                             return true;
                         }
                     }
                 }
             }*/
        }
    }).on("success.form.bv", function (e) {
    });
});


/**
 * to set text
 **/
function setTexts() {
    const email = getTextById(75);
    const fName = getTextById(76);
    const lName = getTextById(80);
    const passW = getTextById(81);
    const rPass = getTextById(81);
    const ristN = getTextById(724);
    document.getElementById("email").placeholder = email;
    document.getElementById("firstName").placeholder = fName;
    document.getElementById("lastName").placeholder = lName;
    document.getElementById("password").placeholder = passW;
    document.getElementById("repassword").placeholder = rPass;
    document.getElementById("restaurantName").placeholder = ristN;

    for (let i = 701; i < 733; i++)
        if (i != 715) setElemText(i);
    setElemText(339);


}


$(document).ready(function () {
    $('[data-toggle="tooltip"]').attr('data-toggle', 'tooltip')
        .attr('data-placement', 'right')
        .attr('title', getTextById(122))
        .tooltip({
            trigger: 'manual'
        })
        .tooltip('show');
    //setTexts();
});
