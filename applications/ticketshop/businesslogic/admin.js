let softwareProduct = "Listorante Admin";

module = "admin.js";
release = "_d1a6f";
applicationID = 1;
//setTexts();
getCustomerId();


function setTexts() {

    //setElemText(5);
    setElemText(8);
    setElemText(9);
    setElemText(10);
    setElemText(11);
    setElemText(12);
    setElemText(13);
    setElemText(14);
    setElemText(15);
    setElemText(16);
    setElemText(17);
    setElemText(18);
    setElemText(19);
    setElemText(20);
    /*  setElemText(21);
      setElemText(22);
      setElemText(23);
      setElemText(24);
      setElemText(25);*/
    setElemText(26);
    setElemText(39);
    setElemText(40);
    setElemText(41);
    // setElemText(104);

    setElemText(748);
    const moreInfoText = getTextById(225);
    assignInnerText("moreinfoText_Orders", moreInfoText);
    assignInnerText("moreinfoText_Bills", moreInfoText);
    assignInnerText("moreinfoText_Messages", moreInfoText);
    assignInnerText("moreinfoText_Guests", moreInfoText);
    assignInnerText("ordersText", getTextById(12));
    assignInnerText("billsText", getTextById(9));
    assignInnerText("messagesText", getTextById(13));
    assignInnerText("guestsText", getTextById(8));
    document.getElementById('loading').innerHTML = "";
    console.log("Set load-image to: " + images.logosmall);
}

function assignInnerText(elementName, text) {
    try {
        const element = document.getElementById(elementName);
        if (element) {
            element.innerText = text;
        }
    } catch (err) {
        console.warn(err);
    }
}

function showShoppingPage() {
    getCustomerCredits(function (data) {
        const chargeTable = formRowDiw("chargesTableID", 6);
        getCreditCharges(function (data) {
            const chargesTableObject = {
                IDENTIFIER: "charges-data",
                jsonRMLData: data,
                header: ['#', getTextById(743)],
                columnIndices: [0, 1],
                actions: [],
                dataColumnIndices: [],
                dataColumnNames: []
            };
            listoHTML.createTable.call(chargesTableObject, "chargesTableID", false);
        }, function (err) {
            showHttpErrorMessage("main-content", err);
        });
        let creditsObj = {};
        creditsObj.amount = "n.n.";
        creditsObj.lastupdate = "n.n.";
        creditsObj.amount = data.rows[0].s[0];
        creditsObj.lastupdate = data.rows[0].s[1];
        const creditsAmount = formLabel(creditsObj.amount, "");
        console.log("Credits retrieved successfully: " + JSON.stringify(data));
        try {
            const buyButton = formButtonWithIconAndBadge(744, 'badgebutton', 'bg-green',
                creditsObj.amount, 'fa fa-battery-3', 'btn btn-app buyCredits');
            const breaks = breakLines(2);
            const creditsLastUpDated = formLabel(creditsObj.lastupdate + ")", "");
            const labelCreditAmount = formLabel(getTextById(742) + "&nbsp;&nbsp;&nbsp;&nbsp;", "");
            const labelCreditLastUpdated = formLabel("&nbsp;&nbsp;&nbsp;(" + getTextById(743) + "&nbsp;&nbsp;&nbsp;", "");
            const buyForm = [labelCreditAmount, creditsAmount, labelCreditLastUpdated, creditsLastUpDated, breaks, buyButton, chargeTable];
            // TODO tbd for suscriptions in next version(s), DO NOT DELETE!
            // const subscriptions = formButtonWithIcon(749, "subscriptionButtonID", "glyphicon glyphicon-certificate", "buySubscription");
            // const endSubscription = formButtonWithIcon(750, "endSubscriptionButtonID", "glyphicon glyphicon-off", "endSubscription");
            // const buyForm = [labelCreditAmount, creditsAmount, labelCreditLastUpdated, creditsLastUpDated, breaks, buyButton, subscriptions, endSubscription, chargeTable];
            listoHTML.buildForm("#main-content", 8, buyForm);
        } catch (err) {
            console.error(JSON.stringify(err));
        }
    }, function (error) {
        const errJsonized = JSON.stringify(error);
        console.error("Credits could not be retrieved : " + errJsonized);
        showHttpErrorMessage("main-content", error);
    });
}

/*
 * 
 * 
 * Admin-Menu/ Function 2/ Common function for all sub-functions: Show existing
 * tables
 */

function showTables() {
    apiCall_listorante_public_showtables(function (data) {

        const action1 = [
            "edit_element_table",
            actionModifyStyle.elements[0] + getTextById(105)
            + actionModifyStyle.elements[1]];
        const action2 = [
            "delete_element_table",
            actionDeleteStyle.elements[0] + getTextById(106)
            + actionDeleteStyle.elements[1]];
        // End of TODO
        const dataColumnNames1 = ["idcustomertable", "places", "isseat",
            "description", "description_locale"];
        const dataColumnIndices1 = [0, 1, 2, 3, 4];
        const dataColumnNames2 = ["idcustomertable"];
        const dataColumnIndices2 = [0];
        const tableObject = {
            IDENTIFIER: "admin-showTables",
            jsonRMLData: data,
            header: [getTextById(43), getTextById(42),
                getTextById(45), "", ""],
            columnIndices: [4, 3, 1],
            actions: [action1, action2],
            dataColumnIndices: [dataColumnIndices1,
                dataColumnIndices2],
            dataColumnNames: [dataColumnNames1, dataColumnNames2],
            rowFormat: function (rowIndex, sArray) {
                const transform = [];
                // transform = sArray;

                if (sArray[1] < 3) {
                    for (let l = 0; l < sArray.length; l++)
                        transform[l] = DataStyle.evidence2Open
                            + sArray[l]
                            + DataStyle.evidence2Close;
                    //debug("sArray[1]<3:", release, sArray,
                    //  transform);
                    return transform;
                } else if (rowIndex === 0) {
                    for (let l = 0; l < sArray.length; l++)
                        transform[l] = DataStyle.evidence3Open
                            + sArray[l]
                            + DataStyle.evidence3Close;
                    //debug("rowindex 0 or 1:", release, sArray,
                    //transform);
                    return transform;
                } else {
                    return sArray;
                }
            },
            /*cellFormat: function (columnIndex, rowIndex, TDopen, data,
                                  TDclose) {
                if (columnIndex === 2 && data > 0) {
                    return TDopen + DataStyle.evidence1Open + data
                        + DataStyle.evidence1Close + TDclose;
                } else {
                    return TDopen + data + TDclose;
                }
            },*/
        };
        listoHTML.createTable.call(tableObject, "main-content");
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}

$(document).on("click", ".table-list", function () {
    showTables();
})

/*
 * Admin-Menu/ Function 1 / Dashboard
 * 
 * 
 */

// 1. Table Overview for dashboard
$(document)
    .on(
        'click',
        '.table-overview',
        function () {
            apiCall_listorante_admin_admindash(function (data) { // If success
                const tableOverviewTableObject = {
                    IDENTIFIER: "admin-dash-overview-table",
                    jsonRMLData: data[2],
                    header: [getTextById(28), getTextById(63),
                        getTextById(97), getTextById(64),
                        getTextById(65), getTextById(66), getTextById(67),
                        getTextById(612), getTextById(613)],
                    columnIndices: [1, 3, 4, 5, 7, 10, 11, 12, 13],
                    actions: [],
                    dataColumnIndices: [],
                    dataColumnNames: [],
                    cellFormat: function (c, r, o, cell, cl) {
                        let currency;
                        if (c === 4) currency = getCurrency();
                        else currency = "";
                        if (c === 0) return `${o}${cell}${cl}`;
                        else if (c === 3 || c === 4 || c === 8) return `${o}${Number(cell).toFixed(2)}${currency}${cl}`;
                        else return `${o}${parseInt(cell, 10)}${cl}`;
                    }
                };
                listoHTML.createTable.call(
                    tableOverviewTableObject, "main-content");
            }, function (err) { // If failed
                showHttpErrorMessage("main-content", err);
            });
        });

// 2. Kitchen Overview for preparing items
$(document)
    .on(
        'click',
        '.kitchen-overview',
        function () {
            $("#main-content").html("");
            $("#main-content").html(images.loading);
            apiCall_listorante_admin_orders(
                function (data) { // If success to get data
                    // Create table Object
                    const tableObject = {
                        actions: [],
                        IDENTIFIER: "admin-itemtable",
                        jsonRMLData: data,
                        header: [getTextById(68),
                            getTextById(69), getTextById(36),
                            getTextById(50), getTextById(70),
                            getTextById(71), getTextById(72),
                            getTextById(73), getTextById(74)],
                        columnIndices: [1, 2, 4, 5, 6, 8, 10, 11,
                            12],
                        dataColumnIndices: [],
                        dataColumnNames: [],
                        rowFormat: function (rowIndex, sArray) {
                            const status = sArray[8];
                            if (sArray[9] === "1") {
                                sArray[8] = DataStyle.statusOrderedStartTag
                                    + " id='orderStatus"
                                    + sArray[0]
                                    + "'>"
                                    + status
                                    + DataStyle.statusOrderedEndTag;
                            } else if (sArray[9] === "2") {
                                sArray[8] = DataStyle.statusPreparationOpenTag
                                    + " id='orderStatus"
                                    + sArray[0]
                                    + "'>"
                                    + status
                                    + DataStyle.statusPreparationEndTag;
                            } else if (sArray[9] === "3") {
                                sArray[8] = DataStyle.statusDeliveredStartTag
                                    + " id='orderStatus"
                                    + sArray[0]
                                    + "'>"
                                    + status
                                    + DataStyle.statusDeliveredEndTag;
                            }
                            return sArray;
                        }
                    };
                    listoHTML.createTable.call(tableObject,
                        "main-content");

                }, function (err) { // If failed to get data
                    showHttpErrorMessage("main-content", err);
                });
        });

// 3. Bills Overview for preparing items
$(document).on(
    'click',
    '.bills-overview',
    function () {
        apiCall_listorante_admin_admindash(function (data) { // If success to
            // get data
            // Create new action Objects
            const action1 = ["viewCart", '<i class="fa fa-search"></i>'];
            //console.log(data);
            // Create table Object
            const dataColumnNames1 = ["idcustomertable", "description_en", "totalbills", "minbillstatus", "maxbillstatus", "mintimestamp", "maxtimestamp", "idbill"];
            const dataColumnIndices1 = [0, 1, 2, 3, 4, 5, 6, 2];

            const billsTableObject = {
                IDENTIFIER: "admin-dash-bills-table",
                jsonRMLData: data[0],
                header: [getTextById(68), getTextById(93),
                    getTextById(95), getTextById(35),
                    getTextById(70), getTextById(36),
                    getTextById(97), getTextById(98), ""],
                columnIndices: [2, 0, 3, 4, 5, 6, 8, 9],
                actions: [action1],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1]
            };

            listoHTML.createTable.call(billsTableObject,
                "main-content");

        }, function (err) { // If failed to get data
            showHttpErrorMessage("main-content", err);
        });
    })

/* SHOW BILL OVERVIEW ITEMS */

$(document).on('click', '.viewCart', function () {

    const billnumber = $(this).data('idbill');
    apiCall_listorante_admin_showcart(billnumber, function (data) {
        //console.log(data);
        update2_Cart_Admin(data);
    }, function (err) {
        showHttpErrorMessage(err);
    });

});


let update2_Cart_Admin = function (data) {
    let rowCount = 0;
    let TOTAL;
    let sArr2;
    sArr2 = [];
    const cartObject = {
        actions: [],
        dataColumns: [],
        dataColumnNames: [],
        dataColumnIndices: [],
        IDENTIFIER: 'waiter-Box2-table',
        jsonRMLData: data,
        header: ['', '', ''],
        columnIndices: [0, 1, 2, 3, 4],
        rowFormat: function (rIdx, sArr) {
            rowCount++;
            if (sArr[1] !== 'TOTAL') {
                if (rowCount == parseInt(data.count, 10)) {
                    sArr2[0] = cartCol1(sArr);
                    sArr2[1] = cartCol2(sArr);
                    sArr2[2] = cartLastRow(sArr, TOTAL);
                } else {
                    sArr2[0] = cartCol1(sArr);
                    sArr2[1] = cartCol2(sArr);
                    sArr2[2] = cartCol3(sArr);
                }
                if (document.getElementById('box1ProdCount' + sArr[2])) {
                    $("#box1ProdCount" + sArr[2]).html(parseInt(sArr[0]) + " x");
                }
                return sArr2;
            } else if (sArr[1] === 'TOTAL') {
                TOTAL = sArr;
            }
        },
        cellFormat: function (colIdx, rowIdx, TDo, cellData, TDc) {
            if (colIdx < 3) {
                return `${TDo}${cellData}${TDc}`;
            } else {
                return ``;
            }
        }
    };
    listoHTML.createTable.call(cartObject, 'main-content', true);
};


/*
 * Admin-Menu/ Function 2 / Add new restaurant-table
 * 
 * 
 */
// 1. Create form with editable data
$(document)
    .on(
        'click',
        '.add-new-table',
        function () {
            //const description = [FormTextArea, 42,
            //  "table_add_description", "rows='1' required "];
            const description = formTextInput(42, "table_add_description", "rows='1' required", "");
            //const description_locale = [FormTextArea, 43,
            //  "table_add_description_locale", "rows=2 required "];
            const description_locale = formTextInput(43, "table_add_description_locale", "rows='2' required", "");
            const places = formNumericInput(45, "table_add_places", "rows='1' required", "");
            //const places = [FormTextArea, 45, "table_add_places",
            //  'cols="4" rows="1"'];
            //const isseat = [FormCheckBox, 44, "table_add_isseat", ""];
            //const submitButton = [FormButton, 40, "add_table", "",
            //  "add-table"];
            //const isseat = FormCheckBox(44, "table_add_isseat","","");
            const submitButton = FormButton(40, "add_table", "", "add-table");
            // forms: array of objects
            const form1 = [description, description_locale, places,
                //isseat,
                submitButton];
            listoHTML.buildForm("#main-content", 6, form1);
        })

// 2. Submit the form with the data of the new restaurant table
$(document).on(
    'click',
    '#add_table',
    function () {
        // TODOs 1: never use String literals! Use getTextById(<n>) or
        // getText(<mnemonic>) instead!
        // If no suitable textid exists, create one in localizedText.js
        // for your
        // language

        // TODOs 2: Do never use alert, but implement a modal instead

        // TODOs 3: Use best-practice equality comparator (In this case,
        // probably '===')
        // or implement a best-practice validation-approach
        const description = $("#table_add_description").val();
        const description_locale = $("#table_add_description_locale").val();
        const places = $("#table_add_places").val();

        if (description == "" || description_locale == "" || places == "") {
            alert(getTextById(251));
            return false;
        }

        if (!$.isNumeric(places)) {
            alert(getTextById(252));
            return false;
        }

        // let isseat = 0;
        // if ($("#table_add_isseat").is(":checked")) {
        //     isseat = 1;
        // }
        apiCall_listorante_admin_tableinsert(description,
            description_locale, 0, places, function (data) {
                const release = RELEASE;
                if (data._rc > 0) {
                    //alert(getTextById(253) + data._rc);
                    document.getElementById("main-content").innerText = getTextById(253);
                } else {
                    for (let i = 1; i <= parseInt(places); i++) {
                        let username = description_locale.replace(/[^a-zA-Z]/g, "") + i;
                        debug("Try to create user '" + username + "'...:", release);
                        apiCall_listorante_admin_newstaff("nomail@here", "" + getTextById(76), 1, 0, "" + getTextById(102),
                            "public", username, function (data) {
                                debug2(module, "data of new staff", release, [data]);
                                if (data._rc > 0) {
                                    const respTxt = "Anonymous customer " + i + " for table '" + description + "' could not be created. ";
                                    debug("" + respTxt, "" + release);
                                    data.status = data._rc;
                                    data.responseText = respTxt;
                                    showHttpErrorMessage("main-content", data);
                                } else {
                                    debug("Anonymous customer " + i + " for table '" + description + "' was created. Username: " + username, release);
                                    let userId;
                                    try {
                                        userId = data[1].rows[0].s[0];
                                    } catch (err) {
                                        if (!userId) {
                                            debug("" + getTextById(254), "" + release);
                                            let errData = {};
                                            errData.status = 1400;
                                            errData.responseText = getTextById(254);
                                            showHttpErrorMessage("main-content", errData);
                                        } else {
                                            showHttpErrorMessage("main-content", err);
                                        }
                                        return false;
                                    }
                                    apiCall_listorante_admin_editprofile("nomail@here", "" + getTextById(76), userId,
                                        images.missing_image, 1, 0, "" + getTextById(102), username, function (data) {
                                            if (data._rc > 0) {
                                                {
                                                    const respTxt = "Anonymous customer " + i + " for table '" + description + "' was created, but could not be edited. ";
                                                    debug("" + respTxt, "" + release);
                                                    data.status = data._rc;
                                                    data.responseText = respTxt;
                                                    showHttpErrorMessage("main-content", data);
                                                }
                                            } else {
                                                debug("Anonymous customer " + i + " for table '" + description + "' was edited. Username: " + username, release);
                                                apiCall_listorante_admin_assignrole(5, userId, function (data) {
                                                    if (data._rc > 0) {
                                                        {
                                                            const respTxt = "Anonymous customer " + i + " for table '" + description + "' was created and edited, but role could not be assigned. ";
                                                            debug("" + respTxt, "" + release);
                                                            data.status = data._rc;
                                                            data.responseText = respTxt;
                                                            showHttpErrorMessage("main-content", data);
                                                        }
                                                    }
                                                }, function (err) {
                                                    showHttpErrorMessage("main-content", err);
                                                });
                                            }
                                        }, function (err) {
                                            showHttpErrorMessage("main-content", err);
                                        });
                                }
                            }, function (err) {
                                showHttpErrorMessage("main-content", err);
                            });
                    }
                    showTables();
                }
            }, function err() {
                showHttpErrorMessage("main-content", err);
            });
    });

// End of Admin-Menu/ Function 2 / Add new restaurant-table

/*
 * Admin-Menu/ Function 2 / Edit data of restaurant-table
 * 
 * 
 */
// 1. Create form with editable data
$(document)
    .on(
        'click',
        '.edit_element_table',
        function () {
            debug("Edit elements:  description="
                + $(this).data("description"), release);
            const table_id = $(this).data("idcustomertable");
            const description = FormTextArea(42,
                "table_edit_description", "rows='1' required ",
                $(this).data("description"));
            const description_locale = formTextInput(43,
                "table_edit_description_locale",
                "rows=1 required ",
                $(this).data("description_locale"));
            const places = formNumericInput(45, "table_edit_places",
                'cols="4" rows="1" required', $(this).data("places"));
            //const isSeat = FormCheckBox(44, "table_edit_isseat", "",
            //  $(this).data("isseat"));
            const submitButton = FormButton(215, "",
                "data-idcustomertable=\"" + table_id + "\"",
                "edit_table");
            const form1 = [description, description_locale, places,
                submitButton];
            // TODO following approach can be used later for adding customer-accounts
            //const newPassValue=makeid(6);
            //const newPass = formTextInput( 250,
            //  "table_edit_newPass",
            //"rows=1 required ",
            //newPassValue);
            //const form1 = [description, description_locale, places,
            //  isSeat, newPass, submitButton];
            listoHTML.buildForm("#main-content", 6, form1);
        })

// 2. Delete a restaurant-table directly from within the shown table-list
$(document).on('click', '.delete_element_table', function () {

    const idcustomertable = $(this).data("idcustomertable");
    if (idcustomertable == "") {
        debug("" + getTextById(254), "" + release);
        let errData = {};
        errData.status = 1400;
        errData.responseText = getTextById(254);
        showHttpErrorMessage("main-content", errData);
        return false;
    }
    apiCall_listorante_admin_tabledelete(idcustomertable, function (data) {
        const release = RELEASE;
        if (data._rc > 0) {
            {
                const respTxt = "Table with id '" + idcustomertable + "' could not be deleted. ";
                debug("" + respTxt, "" + release);
                data.status = data._rc;
                data.responseText = respTxt;
                showHttpErrorMessage("main-content", data);
            }
        } else {
            showTables();
        }
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
});

// 3. Edit restaurant-table-data with the form and submit
$(document)
    .on(
        'click',
        '.edit_table',
        function () {
            const release = RELEASE;
            //console.log(".edit-table");
            const idcustomertable = $(this).data("idcustomertable");
            const description = document
                .getElementById("table_edit_description").value;
            const description_locale = document
                .getElementById("table_edit_description_locale").value;
            let places = document.getElementById("table_edit_places").value;
            if (idcustomertable == "") {
                alert(getTextById(254));
                return false;
            }

            if (description == "" || description_locale == ""
                || places == "") {
                alert(getTextById(251));
                return false;
            }
            if (!$.isNumeric(places)) {
                alert(getTextById(252));
                return false;
            }
            let isseat = 0;
            if ($("#table_edit_isseat").is(":checked")) {
                isseat = 1;
                places = 1;
                $("#table_edit_places").value = 1;
            }
            debug("CHECK", release, description, description_locale,
                idcustomertable, isseat, places);
            apiCall_listorante_admin_tableedit(description,
                description_locale, idcustomertable, isseat, places,
                function (data) {
                    if (data._rc > 0) {
                        {
                            const respTxt = "Data of  table '" + description + "' could not be edited. ";
                            debug("" + respTxt, "" + release);
                            data.status = data._rc;
                            data.responseText = respTxt;
                            showHttpErrorMessage("main-content", data);
                        }
                    }
                    showTables();
                }, function (err) {
                    showHttpErrorMessage("main-content", err);
                });
        });

$(document).on('click', '.shopping', function () {
    showShoppingPage();
});


$(document).on('click', '.buyCredits', function () {
    const currentUrl = window.location.href;
    sessionStorage.setItem("payment_callingPage", currentUrl);
    setCustomerId();
    window.open("buy.html?cust=" + customerID, "_self");
});

$(document).on('click', '.show-Info', function () {
    var softwareInfo = showInfo();
    //const supportButton = [FormButton, 330, "support_button", "", "support-button"];
    const form1 = [];
    listoHTML.createForm("#main-content", 6, form1, "supportButtonForm");
    $('#main-content .box-primary').prepend(softwareInfo);
});

$(document).on('click', '.fadeIn-languageList', function languageListFadeIn() {
    $("#languageList").fadeIn(1000);
});



