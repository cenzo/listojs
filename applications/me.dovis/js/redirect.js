"use strict";

const DOVISIT_HOME = APPLICATION_HOMEPAGE

console.log("pathname: " + window.location.pathname + " \n search: " + window.location.search + "\nhash:" + window.location.hash);
const redirect = function () {
    const path = window.location.pathname;
    const shortUrlParam = path.substring(1 + path.lastIndexOf("/"), path.length);

    let showUrlOnly = true;
    let shortUrl;
    if (shortUrlParam.endsWith('%2b')) {
        shortUrl = shortUrlParam.substring(0, shortUrlParam.length - 3);
    } else if (shortUrlParam.endsWith('+')) {
        shortUrl = shortUrlParam.substring(0, shortUrlParam.length - 1);
    } else {
        shortUrl = shortUrlParam;
        showUrlOnly = false;
    }
    const xhttp = new XMLHttpRequest();
    const xhttp2 = new XMLHttpRequest();
    xhttp.open("GET", `${selectedServer}${customerID}/8/public/select?shortcut=${shortUrl}`, true);
    xhttp2.open("GET", `${selectedServer}${customerID}/8/public/track?shortcut=${shortUrl}`, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xhttpState = -1;
    xhttp.onreadystatechange = function () {
        xhttpState = xhttp.readyState;
        if (xhttpState === 4) {
            if (xhttp.status >= 200 && xhttp.status <= 204) {
                let jsonObject = JSON.parse(xhttp.responseText);
                if (jsonObject.count === 0) {
                    document.getElementById("redirection").innerHTML = `<i class="fas fa-unlink"></i><br/>
                     <h4 class="text-md-center"><i class="fas fa-check-circle"></i>&nbsp;PRAESTO!</h4>`;
                    document.getElementById("redirectionUrl").setAttribute
                    ("href", `${DOVISIT_HOME}?shorturl=${shortUrl}`);
                } else {
                    xhttp2.send();
                    const page = jsonObject.rows[0].s[2];
                    const decodedPage = decodeURIComponent(page);
                    const cat = jsonObject.rows[0].s[4];
                    if (cat > 3) window.location.href = decodedPage;
                    else {
                        document.getElementById("dovis_homepage").setAttribute("href",
                            APPLICATION_HOMEPAGE);
                        document.getElementById("img_dovis_homepage").setAttribute("href",
                            APPLICATION_HOMEPAGE);
                        if (!shortUrl) window.location.href = DOVISIT_HOME;
                        document.getElementById("shorturl").innerHTML = `<h4 class="text-md-center">
                       <i class="fas fa-globe"></i></h4>dovis.it/${shortUrl}`;
                        document.getElementById("redirection").innerHTML = `<i class="fas fa-arrow-right">
                                </i>&nbsp;<i class="fas fa-link"></i>&nbsp;&nbsp;${decodedPage}<br><br>`;
                        document.getElementById("shorturl").innerText = `dovis.it/${shortUrl}`;
                        if (cat > 1) document.getElementById("redirectionUrl").setAttribute("href",
                            `${decodedPage}`);
                        else document.getElementById("redirection").innerHTML = `<i class="fas fa-unlink"></i><br/>
                     <h4 class="text-md-center"><i class="fas fa-gavel"></i>&nbsp;ad auction</h4>`;
                        if (!showUrlOnly && cat == 3)
                            window.location.href = decodedPage;
                    }
                }
            } else {
                document.getElementById("redirection").innerHTML = `<br/>
                          <i class="fas fa-exclamation-circle"></i>
                          <br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${xhttp.status}&nbsp;&nbsp;`;
                document.getElementById("redirectionUrl").setAttribute("href", `${DOVISIT_HOME}`);
            }
        }
    };
    try {
        xhttp.send();
    } catch (err) {
        console.error(err.toString());
        document.getElementById("redirection").innerHTML = `<br/><i class="fas fa-exclamation-triangle"></i><br/>&nbsp;&nbsp;UNKNOWN ERROR OCCURRED&nbsp;&nbsp;`;
        document.getElementById("redirectionUrl").setAttribute("href", `${DOVISIT_HOME}`);
    }
};

