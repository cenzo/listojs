// Paste here all the selected events you need for your project
// and change them according to your project-requirements

const createInvalidUrlMessage = function (stringOfUrl) {
    if (!stringOfUrl.startsWith("http")) {
        return `<p class="badge-danger">http://* | https://* </p>
                               <p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    } else {
        return `<p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    }
};
/***************************************
 CALL-ID: dovisto.User.apiCall10 (createwoatt)
 Description: Save a new combination shortcut/url without url-attributes
 ***************************************/
$(document)
    .on(
        'click',
        '.click_createwoatt',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idcategory = $(this).data("idcategory");
            const shortcut = $(this).data("shortcut");
            let taglist = $(this).data("taglist");
            const urlstring = $(this).data("urlstring");

            if (!idcategory) {
                console.error(`Variable 'idcategory' in object with id ${objectId} has no value.`);
            }

            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }

            if (!taglist) {
                //console.error(`Variable 'taglist' in object with id ${objectId} has no value.`);
                taglist = null;
            }

            if (!urlstring) {
                console.error(`Variable 'urlstring' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idcategory && shortcut && urlstring) {
                dataObject = {
                    "idcategory": idcategory,
                    "shortcut": shortcut,
                    "taglist": taglist,
                    "urlstring": urlstring
                };
            }
            functionForProject_dovisto(objectId, dataObject, "user/createwoatt", "put", "url");
        }
    );

$(document)
    .on(
        'click', '.analyze_new_url', function () {
            console.log("analyze_new_url was clicked ... ");
            const longUrl = $("#longurl_input").val().trim();
            if (!longUrl) {
                $("#validationlabel").text(TEXT_MANDATORY_FIELD);
                $("#validationlabel").show();
            } else if (checkUrl.test(longUrl)) {
                console.log("Analyzing " + longUrl + " ... ");
                analyzeRequestedPage(longUrl, function (data) {
                    let icons = data['icons'];
                    for (let i = 0; i < icons.length; i++)
                        console.log("icon " + i + ": " + icons[i].href);
                }, function (data) {
                    console.log("meta-tags: " + data["meta-tags"]);
                });
                $("#validationlabel").hide();
            } else {
                $("#validationlabel").text(TEXT_INVALID_URL);
                $("#validationlabel").show();
            }
        });

/***************************************
 CALL-ID: dovisto.User.apiCall1 (create)
 Description: Create a new shortcut for a new  url
 ***************************************/
$(document)
    .on(
        'click',
        '.click_create',
        function () {
            $("#shortCut_taglist").attr("placeholder", TEXT_FILL_SOME_TAGS);
            blink("shortCut_taglist", 50, 5);
            blink("shortCut_taglist_icon", 50, 5);
            const table = $("#tableForSubmittingNewShortcut");
            const taglistField = $("#shortCut_taglist");
            if (table) table.remove();
            if (taglistField) taglistField.remove();
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const articleabstract = $(this).data("articleabstract");
            const author = $(this).data("author");
            const description = $(this).data("description");
            const icon = $(this).data("icon");
            const idcategory = $(this).data("idcategory");
            const image1 = $(this).data("image1");
            const image2 = $(this).data("image2");
            const image3 = $(this).data("image3");
            const keywords = $(this).data("keywords");
            const shortcut = $(this).data("shortcut");
            const subject = $(this).data("subject");
            const summary = $(this).data("summary");
            const taglist = $(this).data("taglist");
            const urlstring = $(this).data("urlstring");
            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }
            if (!urlstring) {
                console.error(`Variable 'urlstring' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (shortcut && urlstring) {
                dataObject = {
                    "articleabstract": articleabstract,
                    "author": author,
                    "description": description,
                    "icon": icon,
                    "idcategory": idcategory,
                    "image1": image1,
                    "image2": image2,
                    "image3": image3,
                    "keywords": keywords,
                    "shortcut": shortcut,
                    "subject": subject,
                    "summary": summary,
                    "taglist": taglist,
                    "urlstring": urlstring
                };
            }
            functionForProject_dovisto(objectId, dataObject, "user/create", "put", "url");

        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall2 (createselected)
 Description: Create a new shortcut for an existing url
 ***************************************/
$(document)
    .on(
        'click',
        '.click_createselected',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idcategory = $(this).data("idcategory");
            const idurl = $(this).data("idurl");
            const shortcut = $(this).data("shortcut");
            let taglist = $(this).data("taglist");

            if (!idcategory) {
                console.error(`Variable 'idcategory' in object with id ${objectId} has no value.`);
            }

            if (!idurl) {
                console.error(`Variable 'idurl' in object with id ${objectId} has no value.`);
            }

            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }

            if (!taglist) {
                taglist = null;
                //console.error(`Variable 'taglist' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idcategory && idurl && shortcut) {
                const urlString = $("#longurl_input").val();
                analyzeRequestedPage(idurl, urlString, function (icons) {
                        if (icons.statuscode && icons.statuscode >= 400) {
                            console.err("icons.statuscode: " + icons.statuscode);
                        }
                    },
                    function (meta) {
                        if (meta.statuscode && meta.statuscode >= 400) {
                            console.err("meta.statuscode: " + meta.statuscode);
                        }
                    }, function (err) {
                        console.err(err);
                    });
                dataObject = {
                    "idcategory": idcategory,
                    "idurl": idurl,
                    "shortcut": shortcut,
                    "taglist": taglist
                };
            }
            functionForProject_dovisto(objectId, dataObject, "user/createselected", "put");
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall3 (checkifshortcutexists)
 Description: Check if a shortcut already exists
 ***************************************/
$(document)
    .on(
        'click',
        '.click_checkifshortcutexists',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const shortcut = $(this).data("shortcut");

            if (!shortcut) {
                console.error(`Variable 'shortcut' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (shortcut) {
                dataObject = {
                    "shortcut": shortcut
                };
            }
            functionForProject_dovisto(objectId, dataObject, "checkifshortcutexists", "post");
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall4 (newshortcutkey)
 Description: Select the last key of the shortcut-table
 ***************************************/
$(document)
    .on(
        'click',
        '.click_newshortcutkey',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let dataObject = {};
            const longUrl = $("#longurl_input").val().trim();
            const vl = $("#validationlabel");
            const lut = $(`#longUrlText`);
            vl.hide();
            vl.text(``);
            lut.html(``);
            lut.hide();
            let userLoginMessage;
            if (isLoggedIn()) {
                userLoginMessage = "";
            } else {
                userLoginMessage = TEXT_LOGIN_PLEASE;
            }
            if (longUrl.length < 3) {
                vl.text(`${getTextById(7)} ${userLoginMessage}`);
                vl.show();
            } else if (!checkUrl.test(longUrl)) {
                vl.text(`${TEXT_INVALID_URL}  ${userLoginMessage}`);
                lut.html(createInvalidUrlMessage(longUrl));
                vl.show();
                lut.show();
            } else {
                try {
                    functionForProject_dovisto(objectId, dataObject, "user/newshortcutkey", "post");
                } catch (error) {
                    console.error(error);
                    $("#createsubmit").html(`<i class="fa fa-exclamation-triangle"></i>`);
                }
            }
        }
    );

/***************************************
 CALL-ID: dovisto.User.apiCall5 (selecturlid)
 Description: Select the id of an existing url (and hence also check if the url exists)
 ***************************************/
$(document)
    .on(
        'click',
        '.click_selecturlid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            console.log("current object-id is: " + objectId);
            const li = $("#longurl_input");
            const lut = $("#longUrlText");
            const vl = $("#validationlabel");
            $(this).data("urlstring", li.val());
            lut.html(``);
            lut.hide();
            const urlstring = li.val().trim();
            if (!urlstring) {
                blink(objectId, 25, 3);
                blink("longurl_input", 250, 3);
                vl.text(TEXT_MANDATORY_FIELD);
                vl.show();
                lut.text(``);
                lut.hide();
            } else if (!checkUrl.test(urlstring)) {
                console.error("apiEvents: urlstring=" + urlstring + " last index: " + checkUrl.lastIndex);
                blink(objectId, 25, 3);
                vl.text(TEXT_INVALID_URL);
                vl.show();
                lut.html(createInvalidUrlMessage(urlstring));
                lut.show();
            } else {
                let dataObject = {
                    "urlstring": urlstring
                };
                console.log("Check id of url " + urlstring + "...");
                functionForProject_dovisto(objectId, dataObject, "user/selecturlid", "post");
            }
        }
    );


/***************************************
 CALL-ID: dovisto.User.apiCall6 (usershortcuts)
 Description: Select all shortcuts of the user
 ***************************************/
$(document)
    .on(
        'click',
        '.click_usershortcuts',
        function () {
            console.log("click_usershortcuts");
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let dataObject = {};
            const sessionUrlData = sessionStorage.getItem("usershortcuts");
            if (!sessionUrlData) {
                functionForProject_dovisto(objectId, dataObject, "user/usershortcuts", "post");
            } else if (pageRefreshed) {
                userUrlTableObject.jsonRMLData = JSON.parse(sessionStorage.getItem("usershortcuts"));
                listoHTML.createTable.call(userUrlTableObject, "urllist-div", false);
            }
            setDovisVisible("urllist-div", dovisNavigationIDs);
        }
    );
let settingsCalled = false;
/***************************************
 CALL-ID: dovisto.User.apiCall7 (usersettings)
 Description: Show user's settings
 ***************************************/
$(document)
    .on(
        'click',
        '.click_usersettings',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            let dataObject = {};
            if (settingsCalled === false) {
                functionForProject_dovisto(objectId, dataObject, "user/usersettings", "post");
                settingsCalled = true;
            }
            setDovisVisible("settings-div", dovisNavigationIDs);
        }
    );
/** Custom event: when user clicks on a short-url in the displayed table **/
let urlInfo;
$(document)
    .on(
        'click',
        '.show_shortcut_properties',
        function () {
            // if (urlInfo) {
            $("#urlstring").remove();
            //}
            //const urlstring = $(this).data("url");
            const keywords = $(this).data("keywords");
            const description = $(this).data("description");
            const linkcategory = $(this).data("linkcategory");
            let desc, tl, kw;
            let metadiv;
            let taglist = $(this).data("tags");
            if (description && description != "undefined") desc = `<p class="dovis_small"><i class="fa fa-quote-left fa-1"></i>
                 ${description}<i class="fa fa-quote-right fa-1"></i></p>`;
            else desc = null;
            if (taglist && taglist != "undefined") tl = `<p class="dovis_small"><i class="fa fa-tags"></i> ${taglist}</p>`;
            else tl = null;
            if (keywords && keywords != "undefined") kw = `<p class="dovis_small"><i class="fa fa-sticky-note"></i> ${keywords}</p></small>`;
            else kw = null;
            $(this).removeClass("show_shortcut_properties");
            $(this).addClass("hide_shortcut_properties");
            let linkInfo;
            if (linkcategory == 1) {
                linkInfo = `<i class="fa fa-link"><small>text for "link this url"</small></i>`
            } else {
                linkInfo = `<i class="fa fa-unlink"><small>text for "UN-link this url"</small></i>`
            }
            if (desc || tl || kw) {
                metadiv = `<div class="rounded text-sm-left" id="share-button">`;
                if (desc != null) metadiv += desc;
                if (tl != null) metadiv += tl;
                if (kw != null) metadiv += kw;
                metadiv += `</div>`;
            } else metadiv = ``;
            urlInfo = `<div id="urlstring" class="rounded" style="padding: 0.2em;
                       align-items; background-color: #b3d7ff; color: black">
                       <a class="btn btn-light">
                          <p>${linkInfo}
                          <i class="fa fa-tags"><small>text for "set some tags"</small></i>
                         <i class="fa fa-clock"><small>text for "set termination date"</small></i>
                          <i class="fa fa-gavel"><small>text for "bids"</small></i>
                          <i class="fa fa-money-bill"><small>text for "allow ads"</small></i>
                          <i class="fa fa-images"><small>text for "insert images"</small></i>
                           <i class="fa fa-qrcode"><small>text for "create qr"</small></i>
                           </p>
                           ${metadiv}
                       </a>
               </div>`;
            //if (shareoptions)
            $(this).parent().after(urlInfo);
            //else $(this).after(urlInfo);
            $("#urlstring").hide();
            $("#urlstring").fadeIn(250);
        }
    );


/** Custom event: when user clicks on the share-icon in the displayed table **/
let shareoptions;
$(document)
    .on(
        'click',
        '.show_share',
        function () {
            if (shareoptions) {
                $("#sharediv").remove();
            }
            $(this).removeClass("show_share");
            $(this).addClass("hide_share");
            const shortcut = $(this).data("shortcut");
            shareoptions = `<div id="sharediv" class="rounded text-md-center" style="padding: 0.2em;
                       align-items; background-color: #b3d7ff; color: black">
                       <span>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=${APPLICATION_HOMEPAGE}/${shortcut}"><i class="fa fa-facebook-f fa-2x"></i></a>
                    &nbsp;&nbsp;&nbsp;
                     <a href="https://www.linkedin.com/shareArticle?mini=true&url=${APPLICATION_HOMEPAGE}/${shortcut}"><i class="fa fa-linkedin fa-2x"></i></a>
                     &nbsp;&nbsp;&nbsp;
                     <a href="whatsapp://send?text=${APPLICATION_HOMEPAGE}/${shortcut}" data-action="share/whatsapp/share"  target="_blank"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></a>
                     &nbsp;&nbsp;&nbsp;
                     <a href="mailto:info@example.com?&subject=${APPLICATION_HOMEPAGE}/${shortcut}&body=${APPLICATION_HOMEPAGE}/${shortcut}"><i class="fa fa-envelope fa-2x"></i></a>
                     </span>
               </div>`;
            $(this).after(shareoptions);
            $("#sharediv").hide();
            $("#sharediv").fadeIn(500);
        });


$(document)
    .on(
        'click',
        '.analyzeurl', function () {
            const url = $(this).data("url");
            console.log("---- Analyze '" + url + "': .... ");
            analyzeRequestedPage(url);
        });

$(document)
    .on(
        'click',
        '.hide_shortcut_properties',
        function () {
            if (urlInfo) {
                $("#urlstring").fadeOut(250);
                $("#urlstring").remove();
            }
            $(this).removeClass("hide_shortcut_properties");
            $(this).addClass("show_shortcut_properties");
        }
    );


$(document)
    .on(
        'click',
        '.hide_share',
        function () {
            if (shareoptions) {
                $("#sharediv").fadeOut(500);
                $("#sharediv").remove();
            }
            $(this).removeClass("hide_share");
            $(this).addClass("show_share");
        }
    );

$(document).on('click', '.show_submit', function () {
    setDovisVisible("create-newshortcut-div", dovisNavigationIDs);
    $("#createsubmit").removeClass("disabled");
    $("#createsubmit").addClass("click_selecturlid");

    $("#create_shortcut_with_metadata").remove();
    $("#create_shortcut_without_metadata").remove();
    $("#shortCut_taglist").remove();
    $("#shortCut_taglist_icon").remove();
    $("#newshortcut_h3").remove();
    $("#scurl_linkage").remove();
    $("#save_shortcut").remove();
    $("#publish_shortcut").remove();
    const sessionData = sessionStorage.getItem(SESSIONDATA_KEY);
    const urlID = sessionStorage.getItem(SESSIONDATA_URLID_KEY);
    const urlString = sessionStorage.getItem(SESSIONDATA_URLSTRING_KEY);
    if (!sessionData && urlID && urlString) {
        analyzeRequestedPage(urlID, urlString, function () {
        }, function () {
        }, function (err) {
            console.err(err);
        });
    }
});

$(document).on('click', '.click_propose', function () {
    blink("login", 75, 3);
    blink("signup", 75, 3);
    blink("createsubmit", 50, 5);
    $("#validationlabel").text(TEXT_LOGIN_PLEASE);
    $("#validationlabel").show();
    console.log(TEXT_LOGIN_PLEASE);
    blink("validationlabel", 50, 5);
});
