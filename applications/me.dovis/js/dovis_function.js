"use strict";

const MAX_TABLE_DATA_SIZE = 1000;
const SHORTCUT_LENGTH = 6;
const isLoggedIn = function () {
    const dovisUser = sessionStorage.getItem("dovisUser");
    if (dovisUser) {
        return true;
    } else {
        return false;
    }
};

function isImage(element) {
    if (!element) {
        return false;
    }
    return /^https:(\/\/)(\S+)(\.)(gif|jpg|jpeg|tiff|png|ico|svg)(\?v\S+)*$/i.test(element);
}

const userUrlTableObject = {
    IDENTIFIER: "user-urls",
    jsonRMLData: null,
    header: [`<span class="badge badge-dark icon-calendar">&nbsp;<i class="fa fa-sort fa-2x" aria-hidden="true"></i></span>`,
        `<span class="badge badge-primary icon-link">&nbsp;<i class="fa fa-sort fa-2x" aria-hidden="true"></i></span>`,
        ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``, ``,
    ],
    columnIndices: [18, 2, 9, 10, 1, 3, 4, 5, 6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 0],
    actions: [],
    dataColumnIndices: [],
    dataColumnNames: [],
    tableStyleOptions: {
        paging: true,
        fixedHeader: true,
        autoWidth: false,
        order: [[0, "desc"]],
        responsive: true,
        sDom: '<"top"ipfl>rt<"bottom"p><"clear">',
        columnDefs: [
            {targets: [0], width: "5%"},
            {targets: [2], width: "100%"},
            {targets: '_all', width: "0%"},
            {targets: [0, 1, 2], visible: true},
            {targets: '_all', visible: false}
        ],
        pageLength: 25,
    },
    rowFormat: function (rowIndex, dataArray) {
        return dataArray;
    },
    cellFormat: function (colIndex, rowIndex, TDopen, cellData, TDclose) {
        {
            const decodedUrl = decodeURIComponent(this.jsonRMLData.rows[rowIndex].s[2]);
            let icon;
            let iconUrl = this.jsonRMLData.rows[rowIndex].s[12];
            if (isImage(iconUrl)) {
                icon = `<span style="width: 70px;"><img src="${this.jsonRMLData.rows[rowIndex].s[12]}"
                                                           class="dovisthumb"></span>`
            } else icon = `<span style="width: 95px;"></span>`;
            const creationDatetime = this.jsonRMLData.rows[rowIndex].s[10];
            const shortcut = this.jsonRMLData.rows[rowIndex].s[9];
            if (colIndex === 0) {
                return `${TDopen}<span class="badge badge-dark icon-calendar" style="width: 95px;">
                         ${creationDatetime.toString().substring(0, 10)}</span>${TDclose}`;
            } else if (colIndex === 2) {
                let linkInfo;
                if (this.jsonRMLData.rows[rowIndex].s[16] == 1) {
                    linkInfo = `<i class="fa fa-unlink fa-lg"></i>`
                } else {
                    linkInfo = `<i class="fa fa-link fa-lg"></i>`
                }

                return `${TDopen}<div class="rounded"
                     title="https://dovis.it/${shortcut}">&nbsp;
                     <a class="dovislink" href="${APPLICATION_HOMEPAGE}/${shortcut}%2b"><i class="fa fa-globe fa-3x"></i></a>
                      <span class="badge badge-primary badge-pill show_shortcut_properties" style="width: 100px; height:30px; padding-top:0.72em;"
                       data-url="${decodedUrl}"
                     data-linkcategory="${this.jsonRMLData.rows[rowIndex].s[16]}"
                     data-keywords="${this.jsonRMLData.rows[rowIndex].s[3]}"
                     data-description="${this.jsonRMLData.rows[rowIndex].s[4]}"
                     data-tags="${this.jsonRMLData.rows[rowIndex].s[17]}">
                     <i class="fa fa-cog fa-lg"></i>&nbsp;&nbsp;${shortcut}&nbsp;&nbsp;
                       </span>
                       <span class="badge badge-primary badge-pill show_share" style="width: 45px; height:30px; padding-top:0.72em;"
                       data-shortcut="${shortcut}"
                       ><i class="fa fa-share-alt fa-1x"></i></span>
                       <p> ${icon}&nbsp;<a class="dovislink" href="${decodedUrl}">${linkInfo}${decodedUrl}</a></p>
                       </div>${TDclose}`;
            } else {
                return `<td class="dovis">${cellData}</td>`;
            }
        }
    }
};
const checkUrl = new RegExp(/^(ftp|http|https):\/\/[^ "]+$/);


const blink = function (element, duration, repeat) {
    for (let r = 0; r < repeat; r++) {
        $(`#${element}`).fadeIn(duration);
        $(`#${element}`).fadeOut(duration);
    }
    $(`#${element}`).fadeIn(2 * duration);
};

const shortcutName = function (length) {
    var result = [];
    var characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result.push(characters.charAt(Math.floor(Math.random() *
            charactersLength)));
    }
    return result.join('');
};

const createShortcutName = function (callBack, errorCallBack) {
    const sc = shortcutName(SHORTCUT_LENGTH);
    const scObject = {shortcut: sc};
    apiCall('', `/8/user/checkifshortcutexists`, scObject,
        function (data) {
            if (data.count == 0)
                callBack(sc);
            else callBack(shortcutName(SHORTCUT_LENGTH));
        },
        function (err) {
         console.error("could not select last rowId for shortcut: " + JSON.parse(err));
            errorCallBack(err);
        }, "post");
};

const shorten = function (string, isString, len) {
    let shortenedString;
    if (len === undefined) len = 255;
    if (string && string.toString().length > len) {
        if (isString === undefined) {
            shortenedString = string.toString().substring(0, len - 1);
        } else if (isString === false) shortenedString = null;
    } else shortenedString = string;
    return shortenedString;
};

const allTags = function (obj) {
    const badTagArray = ["viewport"];
    let tags = ``;
    for (let tag in obj) {
        if (!badTagArray.includes(tag.toLowerCase())) {
            tags += `${obj[tag]} `;
        }
    }
    //remove all whitespaces, tabs, newline etc.:
    tags = tags.replace(/\s\s+/g, ' ');
    return tags;
};
const analyzeRequestedPage = function (idOfUrl, urlToAnalyze, callbackIcons, callbackMeta, errorCallBack) {
        console.log("Try to analyze page '" + urlToAnalyze + "'...");
        const useridForBilling = sessionStorage.getItem("dovisUserID");
        if (useridForBilling) {
            apiCall('', `/8/meta`, {userid: useridForBilling, url: urlToAnalyze}, function (metaRead) {
                apiCall('', `/8/icons`, {userid: useridForBilling, url: urlToAnalyze}, function (iconsRead) {
                    let updates;
                    if (metaRead["meta-tags"]) {
                        updates = {
                            "idurl": idOfUrl,
                            "keywords": shorten(metaRead["meta-tags"].keywords),
                            "description": shorten(metaRead["meta-tags"].description, true),
                            "subject": shorten(metaRead["meta-tags"].subject),
                            "articleabstract": shorten(allTags(metaRead["meta-tags"]), true, 1023),
                            "summary": shorten(metaRead["meta-tags"].summary),
                            "author": shorten(metaRead["meta-tags"].author, true),
                            "image1": shorten(metaRead["meta-tags"].image1, false),
                            "image2": shorten(metaRead["meta-tags"].image2, false),
                            "image3": shorten(metaRead["meta-tags"].image3, false)
                        };
                    }
                    if (iconsRead.icons && Array.isArray(iconsRead.icons)) {
                        for (let i = 0; i < iconsRead.icons.length; i++)
                            if (isImage(iconsRead.icons[i].href)) {
                                updates.icon = iconsRead.icons[i].href;
                                break;
                            }
                    }
                    const updateJson = JSON.stringify(updates);
                    console.log(`url-update: ${updateJson}`);
                    apiCall('', `/8/user/urledit`, updates, function (data) {
                        console.log("Result of url-edit:\n" + JSON.stringify(data));
                        callbackIcons(iconsRead);
                        callbackMeta(metaRead);
                    }, function (errUrledit) {
                     console.error(errUrledit);
                    }, "post", "url");
                }, function (errIcons) {
                 console.error("could not read metadata");
                    errorCallBack(errIcons);
                }, "post", "url")
            }, function (errMeta) {
             console.error("could not read icons");
                errorCallBack(errMeta);
            }, "post", "url");
            return true;
        } else
            return false;
    }
;

const functionForProject_dovisto = function (objectId, dataObject, path, method, encoding) {

        /*  const updateUrlDialog = function (urlID, iconUrls, metaTagsObject, shortcut) {
              let icon;
              let tagList = `<div class="text-md-center"><input type="text" placeholder="Tags" id="shortCut_taglist"/>
                      &nbsp;<span class="badge badge-primary" style ="padding: 0.65em;" id="shortCut_taglist_icon"><i class="fa fa-tags"></i></span>`;
              let linkData = ` data-shortcut="${shortcut}"`;
              linkData += ` data-urlid="${urlID}"`;
              if (iconUrls && iconUrls.icons && iconUrls.icons.length > 0) {
                  icon = shorten(iconUrls.icons[0].href, false);
                  if (icon) linkData += `data-icon="${icon}"`; else linkData += `data-icon=""`;
              }
              let metaTags;
              if (metaTagsObject) metaTags = metaTagsObject["meta-tags"];
              if (metaTags) {
                  let description = shorten(metaTags.description);
                  let keywords = shorten(metaTags.keywords);
                  let author = shorten(metaTags.author);
                  let articleabstract = shorten(allTags(metaTags), false, 1024);
                  let subject = shorten(metaTags.subject);
                  let summary = shorten(metaTags.summary);
                  let image1 = shorten(metaTags.image1, false);
                  let image2 = shorten(metaTags.image2, false);
                  let image3 = shorten(metaTags.image3, false);
                  if (description) linkData += `data-description="${description}"`; else linkData += `data-description=""`;
                  if (keywords) linkData += `data-keywords="${keywords}"`; else linkData += `data-keywords=""`;
                  if (author) linkData += `data-author="${author}"`; else linkData += `data-author=""`;
                  if (articleabstract) {
                      linkData += `data-articleabstract="${articleabstract}"`;
                  } else {
                      linkData += `data-articleabstract=""`;
                  }
                  if (subject) linkData += `data-subject="${subject}"`; else linkData += `data-subject=""`;
                  if (summary) linkData += `data-summary="${summary}"`; else linkData += `data-summary=""`;
                  if (image1) linkData += `data-image1="${image1}"`; else linkData += `data-image1=""`;
                  if (image2) linkData += `data-image2="${image2}"`; else linkData += `data-image2=""`;
                  if (image3) linkData += `data-image3="${image3}"`; else linkData += `data-image3=""`;
              }
              let action = `${tagList}<p id="scurl_linkage">dovis.it/${shortcut}&nbsp;&nbsp;<i class="fa fa-arrow-right"></i>
                       &nbsp;${url}</p>&nbsp;&nbsp;
                     <a class="btn btn-primary click_create" id="save_shortcut" ${linkData}
                       data-idcategory="1">${TEXT_SAVE_THIS_SHORTURL}
                    <i class="fa fa-save"></i>&nbsp;&nbsp;<i class="fa fa-unlink"></i>&nbsp;&nbsp;</a></p>
                     <p><a class="btn btn-primary click_create" id="publish_shortcut" ${linkData}
                       data-idcategory="2">${TEXT_PUBLISH_THIS_SHORTURL}&nbsp;&nbsp;
                    <i class="fa fa-globe"></i>&nbsp;&nbsp;<i class="fa fa-link"></i>
                     </a></p></a><h3 id="newshortcut_h3">dovis.it/${shortcut}</h3></div>`;
              return action;
          };*/
        const createSaveShortcutDialog = function (urlID, urlString, shortcut) {
            let actionName;
            let linkData = ` data-shortcut="${shortcut}"`;
            if (urlID != null) {
                actionName = "click_createselected";
                linkData += ` data-idurl="${urlID}"`;
            } else {
                actionName = "click_createwoatt";
                linkData += ` data-urlstring="${urlString}"`;
            }
            let tagList = `<div class="text-md-center"><input type="text" placeholder="Tags" id="shortCut_taglist"/>
                    &nbsp;<span class="badge badge-primary" style ="padding: 0.65em;" id="shortCut_taglist_icon"><i class="fa fa-tags"></i></span>`;
            let action = `${tagList}<p id="scurl_linkage">dovis.it/${shortcut}&nbsp;&nbsp;<i class="fa fa-arrow-right"></i>
                     &nbsp;${urlString}</p><p>${urlString.length}&nbsp;${TEXT_NUMBER_OF_CHARACTERS}</p>&nbsp;&nbsp;
                   <a class="btn btn-primary ${actionName}" id="save_shortcut" ${linkData}
                     data-idcategory="1">${TEXT_SAVE_THIS_SHORTURL}
                  <i class="fa fa-save"></i>&nbsp;&nbsp;<i class="fa fa-unlink"></i>&nbsp;&nbsp;</a></p>
                   <p><a class="btn btn-primary ${actionName}" id="publish_shortcut" ${linkData}
                     data-idcategory="2">${TEXT_PUBLISH_THIS_SHORTURL}&nbsp;&nbsp;
                  <i class="fa fa-globe"></i>&nbsp;&nbsp;<i class="fa fa-link"></i>
                   </a></p></a><h3 id="newshortcut_h3">dovis.it/${shortcut}</h3></div>`;
            return action;
        };


        if (!dataObject) {
         console.error("Cannot send apiCall with null dataobject.");
        } else if (!objectId) {
         console.error("Null object-id for apiCall.");
        } else {
            let eventCallBack;
            let errorCallBack;
            const callingObject = $(`#${objectId}`);
            switch (objectId) {
                case "createsubmit":
                    console.log("current dovis userID: " + currentDovisUserID);
                    if (!isLoggedIn()) {
                        blink("login", 500, 3);
                    } else if (!$("#longurl_input")) {
                        blink("longurl_input", 500, 3);
                    } else {
                        try {
                            const validationLabel = $("#validationlabel");
                            const objectInnerHtml = callingObject.html();
                            const userInput = $("#longurl_input").val();
                            const urlstring = userInput.trim();
                            eventCallBack = function (data) {
                                callingObject.html(`<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>`);
                                let urlId = null;
                                const sessionData = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                                if (sessionData && sessionData.count > MAX_TABLE_DATA_SIZE) {
                                    const dataSizeWarning = $("#datasize-warning");
                                 console.error("user has too much data, need to write new frontend for users which exceed " + MAX_TABLE_DATA_SIZE + " items");
                                    dataSizeWarning.show();
                                    dataSizeWarning.fadeIn(2500);
                                    alert(TEXT_MAXIMUM_DATASIZE_EXCEEDED);
                                    callingObject.html(objectInnerHtml);
                                } else {
                                    let urlAlreadySavedForUser = false;
                                    if (data.count > 0) {
                                        urlId = data.rows[0].s[0];
                                        console.log("url already exists: url-id=" + urlId);
                                        const tableData = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                                        for (let row = 0; row < tableData.count; row++) {
                                            if (tableData.rows[row].s[1] === urlId) {
                                                validationLabel.html(`<p>${TEXT_URL_ALREADY_SAVED}
                                               &nbsp;<i class="fa fa-exclamation"></i></p>
                                               <h4><a href="${APPLICATION_HOMEPAGE}/${tableData.rows[row].s[9]}">
                                                    ${APPLICATION_HOMEPAGE}/${tableData.rows[row].s[9]}</a></h4>`);
                                                validationLabel.show();
                                                urlAlreadySavedForUser = true;
                                                callingObject.html(objectInnerHtml);
                                                break;
                                            }
                                        }
                                    }
                                    if (!urlAlreadySavedForUser) {
                                        console.log("new url ");
                                        let sc;
                                        createShortcutName(function (scName) {
                                            sc = scName;
                                            console.log("call createShortcutName ...sc=" + sc);
                                            validationLabel.hide();
                                            let urlData = createSaveShortcutDialog(urlId, decodeURIComponent(urlstring), sc);
                                            $("#shortCut_taglist").attr("placeholder", getTextById(48));
                                            console.log("Text of taglist-input-field: " + getTextById(48));
                                            const table = `<br/><div class="text-md-center" id="tableForSubmittingNewShortcut">
                                                            ${urlData}
                                                            </div>`;
                                            callingObject.after(table);
                                            const taglistElement = document.getElementById("shortCut_taglist");
                                            taglistElement.addEventListener("keyup", function () {
                                                $("#publish_shortcut").attr("data-taglist", $("#shortCut_taglist").val());
                                                $("#save_shortcut").attr("data-taglist", $("#shortCut_taglist").val());
                                            });
                                            callingObject.html(objectInnerHtml);
                                        }, function (err) {
                                         console.error("could not create new shortcutname: " + JSON.parse(err));
                                            callingObject.html(objectInnerHtml);
                                        });
                                    }
                                }
                                errorCallBack = function () {
                                    callingObject.html(`<i class="fa fa-exclamation-triangle"></i>`);
                                }
                            }
                        } catch
                            (err) {
                         console.error(err);
                            callingObject.html(`<i class="fa fa-exclamation-triangle"></i>`);
                        }
                    }
                    break;
                case
                "publish_shortcut"
                :
                case
                "save_shortcut"
                :
                    eventCallBack = function () {
                        const urlstring = $("#longurl_input").val();
                        const shortcut = "NEW";
                        apiCall('', '/8/user/selecturlid', {"urlstring": urlstring}, function (data) {
                            const idurl = data.rows[0].s[0];
                            sessionStorage.setItem(SESSIONDATA_URLID_KEY, idurl);
                            sessionStorage.setItem(SESSIONDATA_URLSTRING_KEY, urlstring);
                            analyzeRequestedPage(idurl, urlstring, function (icons) {
                                    if (icons.statuscode && icons.statuscode >= 400) {
                                     console.error("icons.statuscode: " + icons.statuscode);
                                    }
                                },
                                function (meta) {
                                    //sessionStorage.removeItem("usershortcuts");
                                    if (meta.statuscode && meta.statuscode >= 400) {
                                       console.error("meta.statuscode: " + meta.statuscode);
                                    } //else $("#showUserUrlsButton").click();
                                }, function (err) {
                                   console.error(err);
                                });
                        }, function (errSelectUrl) {
                           console.error(errSelectUrl);
                        }, "post", "url");
                        $("#shortCut_taglist").remove();
                        $("#tableForSubmittingNewShortcut").remove();
                        $("#longUrlText").html(``);
                        $("#longUrlText").hide();
                        $("#longurl_input").val(``);
                        let data = JSON.parse(sessionStorage.getItem(SESSIONDATA_KEY));
                        if (data) {
                            //console.log("sessiondata:\n" + data);
                            const newcount = 1 + data.count;
                            data.count = newcount;
                            const newArrayElement = [];
                            const dt = new Date();
                            const utcDate = dt.toUTCString();
                            newArrayElement[2] = urlstring;
                            newArrayElement[9] = shortcut;
                            newArrayElement[10] = utcDate;
                            newArrayElement[18] = utcDate;
                            const newObj = {"rn": newcount, "s": newArrayElement};
                            data.rows.push(newObj);
                            sessionStorage.setItem(SESSIONDATA_KEY, JSON.stringify(data));
                            userUrlTableObject.jsonRMLData = data;
                            listoHTML.createTable.call(userUrlTableObject, "urllist-div", false);
                            sessionStorage.removeItem(SESSIONDATA_KEY);
                            $("#showSubmitButton").click();//triggers page analysis
                            $("#showUserUrlsButton").click()
                        } else $("#showUserUrlsButton").click();
                    }
                    break;
                case
                "create_newurl_submit"
                :
                    try {
                        console.log("calling new insert of url with many params");
                    } catch (err) {
                     console.error(err);
                    }
                    break;
                case
                "create_personalized_shorturl"
                :
                    eventCallBack = function (data) {

                    };
                    break;
                case
                "settings"
                :
                    eventCallBack = function (data) {
                        const last_login = data.rows[0].s[0];
                        const username = data.rows[0].s[1];
                        const first_name = data.rows[0].s[3];
                        const last_name = data.rows[0].s[4];
                        const email = data.rows[0].s[5];
                        currentDovisUserID = data.rows[0].s[7];
                        sessionStorage.setItem("dovisUserID", currentDovisUserID);
                        console.log("current dovis-userid:" + currentDovisUserID);
                        $("#settings-div").html(`<ul class ="list-group">Username:  <a class="badge badge-pill badge-info text-sm-left">  ${username}  </a>
                        Email:&nbsp;&nbsp;<a class="badge badge-pill badge-info text-sm-left">  ${email}  </a><br/>
                       First Name:&nbsp;&nbsp; <a class="badge badge-pill badge-info text-sm-left">  ${first_name}  </a><br/>
                       Last Name:&nbsp;&nbsp; <a class="badge badge-pill badge-info text-sm-left">  ${last_name}  </a><br/>
                       Last Login:&nbsp;&nbsp; <a class="badge badge-pill badge-info text-sm-left">  ${last_login}  </a><br/></ul></div>`);
                    };
                    break;
                case
                "showUserUrlsButton"
                :
                    $("#urllist-div").html(`<br><br><h3><i class="fa fa-cog fa-spin fa-1x fa-fw fa-3x"></i></h3>`);
                    $("#validationlabel").hide();
                    eventCallBack = function (data) {
                        if (data.count < MAX_TABLE_DATA_SIZE) {
                            sessionStorage.setItem("usershortcuts", JSON.stringify(data));
                            userUrlTableObject.jsonRMLData = data;
                            listoHTML.createTable.call(userUrlTableObject, "urllist-div", false);
                        } else {
                            let dataSizeWarning = $("#datasize-warning");
                            // TODO
                            // for (let i=0;i<MAX_TABLE_DATA_SIZE;i++)
                            userUrlTableObject.jsonRMLData = data;
                            listoHTML.createTable.call(userUrlTableObject, "urllist-div", false);
                            //if (!dataSizeWarning) {
                            $("#urllist-div").before(`<h4 id="datasize-warning"><i  class="fa fa-exclamation fa-3x"></i>
                                                    <br>${TEXT_MAXIMUM_DATASIZE_EXCEEDED}</h4><br>`);
                            //  dataSizeWarning = $("#datasize-warning");
                            //}
                            alert(TEXT_MAXIMUM_DATASIZE_EXCEEDED);
                            dataSizeWarning.fadeOut(4500);
                            $("#datasize-warning").remove();
                        }
                    }
                    ;
                    break;
                default:
                    eventCallBack = function (dataObject) {
                        console.log("Call triggered per default with following data: " + JSON.stringify(dataObject));
                    };
            }
            try {
                apiCall("dovis", "/" + applicationID + "/" + path, dataObject, eventCallBack, function (err) {
                        try {
                            if (err) showHttpErrorMessage("dovis-errors", err);
                        } catch (err1) {
                            try {
                                showHttpErrorMessage("dovis-errors", JSON.parse(err));
                            } catch (err2) {
                                $("#dovis-errors").html(TEXT_SERVER_ERROR);
                            }
                        }
                    }, method, encoding
                );
            } catch (error) {
             console.error(error);
            }
        }
        console.log(` ******* Object ${objectId}  has called the api`);
    }
;
