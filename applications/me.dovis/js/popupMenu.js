"use strict";
const containerHeight = 100;
$(document).ready(function () {
    let displayPopupTimer;

    $(".fa-bars").click(function (event) {
        let $this = $(this);
        if (tagMenu.isVisible($this)) {
            tagMenu.clearHideMenuTimer();
        } else {
            displayPopupTimer = window.setTimeout(function () {
                tagMenu.show($this);
            }, 500);
        }
        return false;
    }, function (event) {
        if (tagMenu.isVisible()) {
            tagMenu.hide();
        } else {
            window.clearTimeout(displayPopupTimer);
        }
    });


});

let tagMenu = (function createTagMenu() {
    /*
     * Config lets 
     */
    // set request address (https://bla bla bla) without [number]
    let urlAddress = "/the/path/to/apicall/";
    // edit this strings to change shown text
    let msg = {
        requestFailMessage: "Please try again.",
        requestFail: "Request failed!",
        requestSuccess: function (reqValue) {
            return 'Value "' + reqValue + '" was send to Server; Server answers:'
        },
        btnText: "Send Request",
        btnTitle: "Send Request",
        tbValidationFail: "Validation error!",
        tbValidationFailMessage: "Request value is not a number.",
        loadingRequest: "Loading...",
        loadingRequestMessage: "Please wait."
    };

    /*
     * Private fields 
     */
    let containerDiv = $("<div></div>", {
        "id": "tag-menu-container"
    });
    let divMenu = $('<div id="tag-menu"></div>');
    let innerDiv = $("<div></div>");
    let headingDiv = $('<div class="tm-heading"></div>');
    let input = $('<input type="text" />');
    let btn = $('<a href="#" title="' + msg.btnTitle + '">' + msg.btnText + '</a>');
    let contentDiv = $('<div class="tm-description">');

    let isVisible = false;
    let hideMenuTimer;
    let currentTag = undefined;

    /*
     * Private methods
     */
    function sendRequest(id) {
        let req = $.ajax({
            url: urlAddress + id,
            dataType: "text",
            beforeSend: function () {
                let content = '<h3>' + msg.loadingRequest + '</h3><p>' + msg.loadingRequestMessage + '</p>';

                containerHeight = setContentHtml(content);
                containerDiv.animate({height: containerHeight}, 500);
            },
            complete: function () {
                $(".loading_msg").hide();
            }
        });

        return req;
    };

    function setPosition(eventTarget) {
        let height = eventTarget.outerHeight();
        let position = eventTarget.position();

        let posLeft = position.left;
        //let posTop = position.top + height;
        let posTop = position.top;
        containerDiv.css({top: posTop, left: posLeft});
    };

    function setContentHtml(content) {
        contentDiv.html(content);
        let currentHeight = divMenu.outerHeight();

        return currentHeight;
    };


    /*
     * Init events
     */
    containerDiv.click(function (event) {
        if (isVisible) {
            window.clearTimeout(hideMenuTimer);
        }
    }, function (event) {
        tagMenu.hide();
    });

    btn.click(function (e) {
        let tbValue = input.val();
        tbValue = parseInt(tbValue, 10);
        if (!isNaN(tbValue)) {
            let request = sendRequest(tbValue);
            request.done(function (data, textStatus, jqXHR) {
                let content = '<h3>' + msg.requestSuccess(tbValue) + '</h3><p>' + data + '</p>';
                containerHeight = setContentHtml(content);
                containerDiv.animate({height: containerHeight}, 500);
            });

            request.fail(function (jqXHR, textStatus, errorThrown) {
                let content = '<h3>' + msg.requestFail + '</h3><p>' + msg.requestFailMessage + '</p>';
                containerHeight = setContentHtml(content);
                containerDiv.animate({height: containerHeight}, 500);
            });
        } else {
            let content = '<h3>' + msg.tbValidationFail + '</h3><p>' + msg.tbValidationFailMessage + '</p>';
            containerHeight = setContentHtml(content);
            containerDiv.animate({height: containerHeight}, 500);
        }

        return false;
    });

    /*
     * Create component
     */
    headingDiv.append(input).append(btn);
    innerDiv.append(headingDiv).append(contentDiv);
    divMenu.append(innerDiv);
    containerDiv.append(divMenu);

    return {
        show: function (eventTarget) {
            isVisible = true;
            currentTag = eventTarget;
            setPosition(eventTarget);
            containerDiv.appendTo("body");
            let content = eventTarget.data("content");
            let animHeight = setContentHtml(content);
            let animWidth = divMenu.outerWidth();
            containerDiv.width(0).height(0);
            containerDiv.css({visibility: 'visible'});
            containerDiv.animate({height: animHeight, width: animWidth}, 175);
        },
        hide: function () {
            hideMenuTimer = window.setTimeout(function () {
                isVisible = false;
                currentTag = undefined;
                containerDiv.detach();
                containerDiv.css({visibility: 'hidden'});
                contentDiv.html("");
                input.val("");
            }, 0);
        },
        clearHideMenuTimer: function () {
            window.clearTimeout(hideMenuTimer);
        },
        isVisible: function (eventTarget) {
            if (eventTarget) {
                return isVisible && eventTarget === currentTag;
            } else {
                return isVisible;
            }
        }
    };
})()