"use strict";

const TEXT_YOUR_LONG_URL = getTextById(1);
const TEXT_CREATE_PERSONALIZED_URL = getTextById(4);
const TEXT_SLOGAN = getTextById(5);
const TEXT_CREATE = getTextById(6);
const TEXT_INVALID_URL = getTextById(8);
const TEXT_NUMBER_OF_CHARACTERS = getTextById(9);
const TEXT_SERVER_ERROR = getTextById(11);
const TEXT_SERVER_UNREACHABLE = getTextById(12);
const TEXT_SENDING_EMAIL = getTextById(13);
const TEXT_REGISTER_NOW = getTextById(14);
const TEXT_ENTER_VALIDATIONCODE = getTextById(15);
const TEXT_WHAT_IS_THE_SUM_OF = getTextById(16);
const TEXT_AND = getTextById(17);
const TEXT_EMAIL_WAS_SENT = getTextById(18);
const TEXT_ENTER_USERNAME = getTextById(19);
const TEXT_ENTER_PASSWORD = getTextById(20);
const TEXT_CONFIRM_PASSWORD = getTextById(21);
const TEXT_PASSWORDS_DO_NOT_MATCH = getTextById(22);
const TEXT_PASSWORD_HINT = getTextById(23);
const TEXT_MANDATORY_FIELD = getTextById(24);
const TEXT_LOWERCASE_LETTER = getTextById(25);
const TEXT_UPPERCASE_LETTER = getTextById(26);
const TEXT_NUMBER = getTextById(27);
const TEXT_MINIMUM_LENGTH = getTextById(28);
const TEXT_READY_GET_STARTED = getTextById(29);
const TEXT_CHECK_YOUR_EMAIL = getTextById(30);
const TEXT_INVALID_EMAIL = getTextById(31);
const TEXT_EMAIL_CONFIRM_SUBJECT = getTextById(32);
const TEXT_EMAIL_CONFIRM_START = getTextById(33);
const TEXT_EMAIL_CONFIRM_END = getTextById(34);
const TEXT_REGISTER_MAIL_BODY_START = getTextById(35);
const TEXT_REGISTER_MAIL_BODY_END = getTextById(36);
const TEXT_REGISTER_MAIL_SUBJECT = getTextById(10);
const TEXT_LOGIN_NO_AUTHENTICATION = getTextById(37);
const TEXT_LOGIN_NO_AUTHORIZATION = getTextById(38);
const TEXT_LOGIN_SERVER_ERROR = getTextById(39);
const TEXT_LOGIN_ERROR = getTextById(40);
const TEXT_LOGIN = getTextById(41);
const TEXT_LOGIN_PLEASE = getTextById(42);
const TEXT_ONE_MONTH_FREE = getTextById(43);
const TEXT_START_AT_PRICE = getTextById(44);
const TEXT_YOUR_CHOICE = getTextById(45);
const TEXT_PLEASE_REGISTER_FOR_ROLE25 = getTextById(46);
const TEXT_SAVE_THIS_SHORTURL = getTextById(47);
const TEXT_FILL_SOME_TAGS = getTextById(48);
const TEXT_SORRY = getTextById(49);
const TEXT_PUBLISH_THIS_SHORTURL = getTextById(50);
const TEXT_URL_ALREADY_SAVED = getTextById(51);
const TEXT_MAXIMUM_DATASIZE_EXCEEDED = getTextById(52);
const TEXT_REGISTRATION_EXPIRED = getTextById(66);
const TEXT_RESET_PASSWORD_QUESTION = getTextById(67);

$("#login").text(getTextById(41));
$("#signup").text(getTextById(61));
$("#registrationIcon").text(getTextById(61));
$("#login_username").attr("placeholder", getTextById(62));
$("#login_pass").attr("placeholder", getTextById(63));
$("#registrationEmail").attr("placeholder", getTextById(64));


setElemText(57);
setElemText(58);
setElemText(59);
setElemText(60);
setElemText(65);


const CALLBACKPAGE_USER_REGISTRATION = APPLICATION_HOMEPAGE;
const APPLICATION_LOGO="dovisit_logo_small.png";
const RESET_PASSWORD_PAGE = "";
const JSON_HTML_URL = `${CALLBACKPAGE_USER_REGISTRATION}/email.json`;
const JSON_HTML_WELCOME_URL = `${CALLBACKPAGE_USER_REGISTRATION}/welcomemail.json`;
const dovisNavigationIDs = ["settings-div", "create-newshortcut-div", "urllist-div", "dovis-errors"];
const SESSIONDATA_KEY = "usershortcuts";
const SESSIONDATA_URLID_KEY = "urlID";
const SESSIONDATA_URLSTRING_KEY = "urlString";

let pageRefreshed;

const setLoggedInLayout = function () {
    const userName = sessionStorage.getItem("dovisUser");
    $("#login_username").remove();
    $("#login_pass").remove();
    $("#signup").remove();
    $("#login").removeClass("doLogin");
    $("#login").removeClass("btn-primary");
    $("#login").addClass("btn-default");
    $("#create-newshortcut-div").hide();
    $("#login").html(`<i class="fa fa-user-circle"></i>&nbsp;${userName}`);
    $("#login").after(`&nbsp;<a class="btn btn-primary click_usersettings" id ="settings"><i class="fa fa-ellipsis-h"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary doLogout" id ="logoutButton"><i class="fa fa-power-off"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary click_usershortcuts" id ="showUserUrlsButton"><i class="fa fa-list-ul"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary show_submit" id ="showSubmitButton"><i class="fa fa-plus-square icon-link"></i></a>&nbsp;`);
    $("#validationlabel").hide();
    $("#section_calltoaction").remove();
    $("#section_signup").remove();
    $("#createsubmit").removeClass("click_propose");
    $("#createsubmit").addClass("click_selecturlid");
    $("#slogan-container-div").remove();
    $("#text65").remove();
    pageRefreshed = false;
};

const setDovisVisible = function (element, elementIDsArray) {
    elementIDsArray.forEach(function (value) {
        if (element === value) {
            $("#" + value).show();
        } else {
            $("#" + value).hide();
        }
    });
};

const buildRegistrationConfirmationForm = function (challenge1, challenge2, email, regCode) {
    document.getElementById("newpassword").remove();
    document.getElementById("passwordhelp").remove();
    // const storedObject = JSON.parse(sessionStoreObject);
    // const challenge1 = storedObject.challengevalue1;
    // const challenge2 = storedObject.challengevalue2;
    // const email = storedObject["request-for-recipient"];
    console.log("email=" + email);
    $("#registrationEmail").attr("placeholder", "");
    $("#registrationEmail").attr("value", email);
    $("#registrationEmail").attr("disabled", "true");
    const challenge = `${TEXT_WHAT_IS_THE_SUM_OF}${challenge1}&nbsp;${TEXT_AND}&nbsp;${challenge2} &nbsp;?`;
    $("#registrationEmail").after(`<input id="validationcode" class="form-control form-control-lg" placeholder=""
                                   type="text" value="${regCode}" disabled>`).after(`<label id="label_validation_code">${regCode}</label><i class="fa fa-key fa-2x"></i>`);
    $("#registrationEmail").after(`<p id="challengequestion">${challenge}</p><br>`);
    $("#registrationEmail").after(`<input id="challengeresponse" class="form-control form-control-lg" placeholder="${challenge}"
                                   type="number" required>`);
    $("#registrationEmail").after(`<input id="confirmpassword" class="form-control form-control-lg" placeholder="${TEXT_CONFIRM_PASSWORD}"
                                   type="password" required>`);
    $("#registrationEmail").after(`<input id="newpassword" name = "psw" class="form-control form-control-lg passwordCheck" placeholder="${TEXT_ENTER_PASSWORD}"
                                   type="password" required title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                   pattern=${"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"}>`);
    $("#registrationEmail").after(`<input id="newusername" class="form-control form-control-lg" placeholder=""
                                   type="text" value="${email}" disabled>`);
    $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-user-plus"></i></span>&nbsp;${TEXT_REGISTER_NOW}`);
    $("#registrationButton").attr("class", `btn btn-block btn-lg btn-primary confirmRegistration`);
    $("#newpassword").after(`<div id="passwordhelp">
                             <ul class="list-group">
                              <li class="list-group-item list-group-item-dark">${TEXT_PASSWORD_HINT}</li>
                               <li class="list-group-item list-group-item-primary"> ${TEXT_LOWERCASE_LETTER}<i id="letter" class="fa fa-user-secret"></i></li>
                              <li class="list-group-item list-group-item-primary">${TEXT_UPPERCASE_LETTER}<i id="capital" class="fa fa-user-secret"></i></li>
                                <li class="list-group-item list-group-item-primary">${TEXT_NUMBER}<i class="fa fa-user-secret" id="number"></i></li>
                                <li  class="list-group-item  list-group-item-primary"> ${TEXT_MINIMUM_LENGTH}<i class="fa fa-user-secret" id="length"></i></li>
                              </ul>
                            </div>`);
    $("#registrationMessage").hide();
    $("#newusername").hide();
}

/*-------------------   start build page      ------------------------*/
// This must be a hidden field: for dovis and the normal module it is 8001
const appModule = 7000;
let currentDovisUserID;
if (SHOW_TEST_MESSAGE) {
    document.getElementById("testline").setAttribute("style", "visibility:visible");
}

$("#text_ready_getstarted").text(TEXT_READY_GET_STARTED);

const instruction = window.location.search;
const urlParams = new URLSearchParams(window.location.search);

console.log("instruction=" + instruction);
if (instruction && instruction.startsWith("?register")) {
    const registrationCode = urlParams.get('register');
    const mail = urlParams.get('mail');
    const cr1 = urlParams.get('cr1');
    const cr2 = urlParams.get('cr2');
    //const registrationCode = instruction.substring(1 + instruction.lastIndexOf("="), instruction.length);
    console.log("registrationCode=" + registrationCode + " mail: " + mail + " cr1: " + cr1) + "cr2: " + cr2;
    window.location.hash = 'registrationIcon';
    //const storedResponseObject = sessionStorage.getItem("dovis_user_registration");
    if (!mail || !cr1 || !cr2) {
        $("#text_ready_getstarted").html(`<i class="fa fa-exclamation fa-2x"></i>&nbsp;&nbsp;${TEXT_REGISTRATION_EXPIRED}`)
    } else {
        buildRegistrationConfirmationForm(cr1, cr2, mail, registrationCode);
    }
}

$("#longurl_input").prop("placeholder", TEXT_YOUR_LONG_URL);
$("#longUrlText").hide();
$("#slogan").text(TEXT_SLOGAN);
$("#createsubmit").html(`<i class="fa fa-random fa-lg"></i>&nbsp;${TEXT_CREATE}`);


if (isLoggedIn()) {
    setLoggedInLayout();
    pageRefreshed = true;
    $("#showUserUrlsButton").click();
    pageRefreshed = false;
} else {
    $("#login_username").hide();
    $("#login_pass").hide();
}

/*-------------------   end of build page      ------------------------*/

$(document).on('click', '.doRegistration', function () {
        let responseObject = {errorMnemonic: ""};
        $("#registrationMessage").attr("style", "visibility: hidden");
        const userEmail = $("#registrationEmail").val();
        if (validateEmail(userEmail)) {
            console.log(`Start registration of user with Email ${userEmail}`);
            $("#registrationIcon").attr("style", "visibility: visible;");
            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope"></i></span>&nbsp;${TEXT_SENDING_EMAIL}`);
            blink("registrationButton", 250, 5);
            blink("registrationEmail", 250, 5);
            const xhttp = new XMLHttpRequest();
            const postUrl = `${getAPIServerPath()}0/0/customer/asktoregister/${customerID}/${applicationID}`;
            xhttp.open("POST", postUrl, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState === 4) {
                    try {
                        console.log("xhttp.status=" + xhttp.status);
                        responseObject = JSON.parse(xhttp.responseText);
                        console.log("responseObject: " + responseObject);
                        if (xhttp.status > 0) {
                            if (xhttp.status <= 204) {
                                $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-check-circle"></i></span>${TEXT_EMAIL_WAS_SENT}`);
                                $("#text_ready_getstarted").html(`<span><i class="fa fa-envelope-open"></i></span>${TEXT_CHECK_YOUR_EMAIL}`);
                                blink("text_ready_getstarted", 500, 3);
                            } else if (xhttp.status === 422) {
                                if (responseObject.errorMnemonic.startsWith("asktoregister.3")) {
                                    $("#registrationButton").text(`${TEXT_INVALID_EMAIL} `);
                                } else if (responseObject.errorMnemonic.toString().startsWith("asktoregister.7")) {
                                    $("#registrationButton").text(`Failed to send eMail to ${userEmail}`);
                                }
                            } else if (xhttp.status >= 400) {
                                $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                            }
                        } else {
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                            $("#registrationMessage").text(`${TEXT_SERVER_UNREACHABLE}`);
                            $("#registrationMessage").attr("style", "visible");
                        }
                    } catch
                        (error) {
                        console.error(error);
                        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                    }
                }
            };
            const activationStart = encodeURIComponent(`${TEXT_REGISTER_MAIL_BODY_START}<a href="${CALLBACKPAGE_USER_REGISTRATION}?register=REGISTRATION_VALIDATION_CODE&cr1=CRVALUE1&cr2=CRVALUE2&mail=EMAIL">`);
            const activationEnd = encodeURIComponent(`</a>${TEXT_REGISTER_MAIL_BODY_END}`);
            console.log("activationStart: " + activationStart);
            console.log("activationEnd: " + activationEnd);
            const body = `email=${userEmail}&mailbodymessagestart=${activationStart}&module=${appModule}&mailbodymessageend=${activationEnd}&mailsubjectmessage=${TEXT_REGISTER_MAIL_SUBJECT}&callbackpage=${CALLBACKPAGE_USER_REGISTRATION}&jsonurl=${JSON_HTML_URL}`;
            console.log("body: " + body);
            xhttp.send(body);
        } else {
            console.error("invalid email");
            $("#registrationMessage").attr("style", "visible");
            $("#registrationMessage").text(TEXT_INVALID_EMAIL);
        }
    }
)
;
let mandatoryWarningscreated = false;
let passwordValid = false;
let registrationIsConfirmed = false;
$(document).on('click', '.confirmRegistration', function () {

    let responseObject = {errorMnemonic: "", message: "", httpStatus: -1};
    const userEmail = $("#registrationEmail").val();
    const username = $("#newusername").val();
    const confirmpassword = $("#confirmpassword").val();
    const password = $("#newpassword").val();
    const cr = $("#challengeresponse").val();
    const code = $("#validationcode").val();
    const addedRole = $("#applicationAdditionalUserRole").val();
    if (!mandatoryWarningscreated) {
        $("#newusername").after(`<label id="mandatory_username" class="badge badge-info"></label>`);
        $("#newpassword").after(`<label id="mandatory_pw" class="badge badge-info"></label>`);
        $("#confirmpassword").after(`<label id="mandatory_pwconfirm" class="badge badge-info"></label>`);
        $("#challengeresponse").after(`<label id="mandatory_cr" class="badge badge-info"></label>`);
        $("#validationcode").after(`<label id="mandatory_code" class="badge badge-info"></label>`);
        mandatoryWarningscreated = true;
    }
    if (!username) {
        $('#mandatory_username').text(TEXT_MANDATORY_FIELD);
        blink("mandatory_username", 250, 3);
    } else {
        $("#mandatory_username").text(``);
    }
    if (!password) {
        $("#mandatory_pw").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_pw", 250, 3);
    } else {
        $("#mandatory_pw").text(``);
    }
    if (!confirmpassword) {
        $("#mandatory_pwconfirm").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_pwconfirm", 250, 3);
    } else {
        $('#mandatory_pwconfirm').text(``);
    }
    if (!cr) {
        $("#mandatory_cr").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_cr", 250, 3);
    } else {
        $("#mandatory_cr").text(``);
    }
    if (!code) {
        $("#mandatory_code").attr("style", "visibility:visible");
        $("#mandatory_code").text(TEXT_MANDATORY_FIELD);
        blink("mandatory_code", 250, 3);
    } else {
        $("#mandatory_code").text(``);
    }
    let pswMatch = false;
    const psw = $("#newpassword").val();
    const cpsw = $("#confirmpassword").val();
    if (psw.toString().normalize() === cpsw.toString().normalize()) {
        pswMatch = true;
        if (confirmpassword) {
            $("#mandatory_pwconfirm").text(``);
        }
    } else if (passwordValid === true) {
        pswMatch = false;
        console.error("Passwords do not match");
        $("#mandatory_pwconfirm").text(TEXT_PASSWORDS_DO_NOT_MATCH);
    }

    if (registrationIsConfirmed === true) {
        blink("registrationButton", 250, 5);
    } else if (userEmail && code && cr > 0 && username && password && confirmpassword && pswMatch === true && passwordValid === true) {
        $("#challengequestion").hide();
        $("#newusername").attr("disabled", "true");
        $("#newpassword").attr("disabled", "true");
        $("#confirmpassword").attr("disabled", "true");
        $("#validationcode").attr("disabled", "true");
        $("#challengeresponse").attr("disabled", "true");
        $("#newpassword").fadeOut();
        $("#confirmpassword").fadeOut();
        $("#validationcode").fadeOut();
        $("#challengeresponse").fadeOut();
        $("#label_validation_code").hide();
        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope"></i></span>&nbsp;${TEXT_SENDING_EMAIL}`);
        blink("registrationButton", 250, 5);
        console.log(`Start confirming registration of user with Email "${userEmail}and role ${addedRole}`);
        const xhttp = new XMLHttpRequest();
        const postUrl = `${getAPIServerPath()}0/0/customer/register/${code}`;

        xhttp.open("POST", postUrl, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        let xhttpState = -1;

        xhttp.onreadystatechange = function () {
            xhttpState = xhttp.readyState;
            if (xhttpState === 4) {
                try {
                    if (xhttp.status > 0) {
                        if (xhttp.status <= 204) {
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope-open"></i></span>&nbsp;${TEXT_EMAIL_WAS_SENT}`);
                            registrationIsConfirmed = true;
                            $("#text_ready_getstarted").fadeIn();
                            $("#text_ready_getstarted").text(getTextById(32));
                            blink("text_ready_getstarted", 500, 3);
                        } else {
                            //TODO distinguish http403 and others
                            responseObject = JSON.parse(xhttp.responseText);
                            $("#text_ready_getstarted").fadeIn();
                            $("#text_ready_getstarted").text(`HTTP${responseObject.httpStatus}: ${responseObject.message}`);
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                        }
                    } else {
                        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                        $("#registrationMessage").text(`${TEXT_SERVER_UNREACHABLE}`);
                        $("#registrationMessage").attr("style", "visible");
                    }
                } catch (error) {
                    console.error(error);
                    $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-triangle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                }
            }
        };
        //userEmail && code && cr > 0 && username && password && confirmpassword && pswMatch === true && passwordValid === true
        xhttp.send(`username=${username}&pass=${password}&cr=${cr}&callbackpage=${CALLBACKPAGE_USER_REGISTRATION}
                          &module=${appModule}&mailSubject=${TEXT_EMAIL_CONFIRM_SUBJECT}&jsonurl=${JSON_HTML_WELCOME_URL}
                          &mailbodystart=${TEXT_EMAIL_CONFIRM_START}&mailbodyend=${TEXT_EMAIL_CONFIRM_END}&customerrole=${addedRole}`);
    }
});

$(document).on('focusin', '.passwordCheck', function () {
    $("#passwordhelp").fadeIn();
});

$(document).on('focusout', '.passwordCheck', function () {
    $("#passwordhelp").fadeOut();
});


$(document).on('change paste keyup', '.passwordCheck', function () {

    let hasLowerCase = false;
    let hasUpperCase = false;
    let hasNumber = false;
    let minLength = false;

    const valid = "fa-check";
    const invalid = "fa-user-secret";
    let myInput = document.getElementById("newpassword");
    let letter = document.getElementById("letter");
    let capital = document.getElementById("capital");
    let number = document.getElementById("number");
    let length = document.getElementById("length");
    let lowerCaseLetters = /[a-z]/g;
    if (myInput.value.match(lowerCaseLetters)) {
        letter.classList.remove(invalid);
        letter.classList.add(valid);
        hasLowerCase = true;
    } else {
        letter.classList.remove(valid);
        letter.classList.add(invalid);
        hasLowerCase = false;
    }

    // Validate capital letters
    let upperCaseLetters = /[A-Z]/g;
    if (myInput.value.match(upperCaseLetters)) {
        capital.classList.remove(invalid);
        capital.classList.add(valid);
        hasUpperCase = true;
    } else {
        capital.classList.remove(valid);
        capital.classList.add(invalid);
        hasUpperCase = false;
    }

    // Validate numbers
    let numbers = /[0-9]/g;
    if (myInput.value.match(numbers)) {
        number.classList.remove(invalid);
        number.classList.add(valid);
        hasNumber = true;
    } else {
        number.classList.remove(valid);
        number.classList.add(invalid);
        hasNumber = false;
    }

    // Validate length
    if (myInput.value.length >= 8) {
        length.classList.remove(invalid);
        length.classList.add(valid);
        minLength = true;
    } else {
        length.classList.remove(valid);
        length.classList.add(invalid);
        minLength = false;
    }
    if (hasLowerCase === true && hasUpperCase === true && hasNumber === true && minLength === true) {
        passwordValid = true;
        $("#passwordhelp").fadeOut();
    } else {
        passwordValid = false;
        $("#passwordhelp").fadeIn();
    }
});


$(document).on('click', '.showloginfields', function () {
    $("#login_username").attr("style", "visibility: visible;");
    $("#login_pass").attr("style", "visibility: visible;");
    $("#signup").hide();
    // $("#longurl_input").hide();
    // $("#createsubmit").hide();
    $("#login").removeClass("showloginfields");
    $("#login").addClass("doLogin");
    $("#text65").hide();
});


let hintIsActive = false;
const passwordHelp = `<span id="passwordHelp"><br><a class="badge badge-danger reset_pw">
${TEXT_RESET_PASSWORD_QUESTION}</a></span>`;


let hintLogin = `<span class="badge badge-info dovis" id="blinking_login_message">${TEXT_MANDATORY_FIELD}</span>`;
let hintPass = `<span class="badge badge-info dovis" id="blinking_pass_message">${TEXT_MANDATORY_FIELD}</span>`;
$("#login_username").after(hintLogin);
$("#login_pass").after(hintPass);

$("#blinking_login_message").hide();
$("#blinking_pass_message").hide();
$("#blinking_pass_message").after(passwordHelp);
$("#passwordHelp").hide();

$(document).on('click', '.doLogin', function () {
    const passWelement = document.getElementById("login_pass");
    passWelement.addEventListener("input", function () {
        $("#passwordHelp").hide();
    });
    const userName = $("#login_username").val();
    const pass = $("#login_pass").val();
    const loginMessage = function (status, msg) {
        console.log("login-status=" + status + ": " + msg);
        if (status == 400 || status == 401 || status == 403) {
            $("#passwordHelp").show();
        }
        if (!hintIsActive) {
            $("#login_username").after(hintLogin);
            $("#login_pass").after(hintPass);
            hintIsActive = true;
        }
        $("#blinking_login_message").text(msg);
        $("#blinking_pass_message").text(msg);
        blink("blinking_login_message", 100, 3);
        blink("blinking_pass_message", 100, 3);
        blink("login_username", 150, 3);
        blink("login_pass", 150, 3);
    };

    if (!userName) {
        loginMessage(0, TEXT_MANDATORY_FIELD);
    } else if (!pass) {
        loginMessage(0, TEXT_MANDATORY_FIELD);
    } else {
        const pre_login_html = $("#login").html();
        $("#login").html(`<i class="fa fa-cog fa-spin fa-1x fa-fw"></i>&nbsp;${userName}`);
        const xhttp = new XMLHttpRequest();
        const postUrl = `${getAPIServerPath()}${customerID}/${applicationID}/${appModule}/login`;
        let jsonResponse = {bearerToken: null};
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4) {
                $("#login").html(pre_login_html);
                if (xhttp.status > 0 && xhttp.status <= 204) {
                    $("#passwordHelp").remove();
                    jsonResponse = JSON.parse(xhttp.responseText);
                    sessionToken = jsonResponse.bearerToken.split(";")[0];
                    // we need userEmail for payment checkout:
                    const userEmail = jsonResponse.bearerToken.split(";")[1];
                    sessionStorage.setItem("dovisUser", userName);
                    sessionStorage.setItem("dovisUserEmail", userEmail);
                    sessionStorage.setItem("listo_" + customerID + "_" + applicationID + "_" + role, sessionToken);
                    $("#blinking_login_message").remove();
                    $("#blinking_pass_message").remove();
                    setLoggedInLayout();
                    $("#settings").click();
                    $("#showUserUrlsButton").click();
                    $("#showSubmitButton").click();
                } else if (xhttp.status >= 400) {
                    if (xhttp.status === 400) {
                        loginMessage(xhttp.status, `HTTP400 ${TEXT_LOGIN_NO_AUTHENTICATION}`);
                    } else if (xhttp.status === 401) {
                        loginMessage(xhttp.status, TEXT_LOGIN_NO_AUTHENTICATION);
                    } else if (xhttp.status === 403) {
                        loginMessage(xhttp.status, `HTTP403 ${TEXT_LOGIN_NO_AUTHORIZATION}`);
                    } else {
                        loginMessage(xhttp.status, `${xhttp.status}: ${TEXT_LOGIN_SERVER_ERROR}`);
                    }
                } else {
                    loginMessage(xhttp.status, TEXT_LOGIN_ERROR);
                }
            }
        };
        xhttp.open("POST", postUrl, true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send("username=" + userName + "&password=" + pass);
    }
})
;


$(document).on('click', '.doLogout', function () {
    const logoutPath = `${getAPIServerPath()}${customerID}/${applicationID}/logout`;
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", logoutPath, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    const bearerString = sessionStorage.getItem("listo_" + customerID + "_" + applicationID + "_" + role);
    xhttp.send("token=" + bearerString);
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (xhttp.status > 0 && xhttp.status <= 204) {
                sessionStorage.removeItem("dovisUser");
                sessionStorage.removeItem("dovisUserID");
                sessionStorage.removeItem("currentLanguage");
                sessionStorage.removeItem("selectedServer");
                sessionStorage.removeItem("dovisUserEmail");
                sessionStorage.removeItem("usershortcuts");
                sessionStorage.removeItem("currentLanguage");
                sessionStorage.removeItem("listo_" + customerID + "_" + applicationID + "_" + role);
                window.open(APPLICATION_HOMEPAGE, "_self");
            } else {
                $("#logoutIcon").text(`HTTP${this.status}`);
                $("#logoutIcon").removeClass("fa-sign-out");
                $("#logoutIcon").addClass("fa-exclamation");
            }
        }
    };
});


$(document).on('click', '.reset_pw', function () {
    $("#registrationIcon").text(TEXT_RESET_PASSWORD_QUESTION);
    $("#text_ready_getstarted").hide();
    location.href = "#registrationIcon";
    const button = $("#registrationButton");
    button.removeClass("doRegistration");
    button.addClass("sendNewPass");
});

$(document).on('click', '.sendNewPass', function () {
    const subject = "dovis.it";

    const html = `<html><br><br><br><a href="${APPLICATION_HOMEPAGE}/?resetpassw="RESET_LINK">
            ${TEXT_RESET_PASSWORD_QUESTION}
                   &nbsp;RESET_LINK</a><br><img src="${APPLICATION_HOMEPAGE}/img/${APPLICATION_LOGO}"></body></html>`;
    const email = $("#registrationEmail").val();
    const path = `${getAPIServerPath()}0/0/customer/askresetpw/${customerID}/${applicationID}/${appModule}`;
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", path, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(`username=${encodeURIComponent(email)}&mailSubject=${encodeURIComponent(subject)}&mailbody=${encodeURIComponent(html)}`);
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState == 4) {
            if (xhttp.status > 0 && xhttp.status <= 204) {
                $("#text_ready_getstarted").text(TEXT_EMAIL_WAS_SENT);
            } else {
                $("#text_ready_getstarted").text(`HTTP${xhttp.status}`);
            }
        }
    };
});