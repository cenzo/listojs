"use strict";
const themeID = 20;
const customerTheme = "slick-opl";
release = RELEASE;
scrollingCustomized = true;
scrollingDuration=1000;

function guestScroll(boxName) {
    //debug2("slickopl.js", "guestscroll", release, [boxName, scrollingDuration]);
    $('html, body').animate({
        scrollTop: $("#" + boxName).offset()
    }, scrollingDuration);
}

//  Elements of box0
function categoryHeader() {
    return `  `;
}

function categoryBottom() {
    return ` `;
}

function categoryItem(rowNum, catItemArray) {
    /*
    *   catItemArray:
    * [
    * 0: id of productcategory,
    * 1: description of category,
    * 2: parent-id of category,
    * 3: level in category tree,
    * 4: position in category tree,
    * 5: name of category,
    * 6: image of category
    * ]
    *
    * */
    let categoryList = ` <div class="col-lg-6 col-md-6 col-xs-8 p-0">
                <a href="javascript:void(0);" class="blog-bg get-products"  data-categoryText="${catItemArray[5]}"
                             data-category="${catItemArray[0]}">
                <div class="feature-item featured-border1">
                    <div class="float-left">
                      <img src=${getImagePath()}/${catItemArray[6]}
                             style="min-width:125px; max-width:125px; min-height:125px; max-height:125px; border-radius: 15%;"> &nbsp;&nbsp;
                    </div>
                    <p>
                     <div class="feature-info float-left">
                        <h4>${catItemArray[5]}</h4>
                        <p>${catItemArray[1]}</p>
                    </div>
                </div>
                </a>
                 <a href="javascript:void(0);" class="get-products btn btn-app previous-button" data-category="${catItemArray[2]}">&nbsp;<b>^</b></a>
            </div>`;

    return categoryList;

};

//  Elements of box1
function productsHeader() {
    return `
                <a href="javascript:void(0);" class="get-products btn btn-app previous-button" data-category="0">
                <div>
                    <div>
                     <h1><strong><i class="lni lni-pointer-left"></i> </strong></h1>
                    </div>
                    <p>
                     <div class="feature-info float-left">
                   <h1><strong><i class='lni lni-angle-double-left'></i></strong></h1>
                      <p></p>
                    </div>
                </div> </a>
            `;
}

function productsBottom() {
    return ``;
}

function productItem(rowNum, prodItemArray) {
    /*
    * prodItemArray:
    * [
    * 0: idproduct,
    * 1: position in list,
    * 2: product-name,
    * 3: product long description,
    * 4: price,
    * 5: image,
    * 6: id of product category,
    * 7: product-label set by the restaurant (alternatively to idproduct),
    * 8: product short description
    *
    *
    * */
    let productList = ` <div class="col-lg-4 col-md-4 col-xs-8 p-0">
               <a href="javascript:void(0);"   class="add-product" data-product="${prodItemArray[0]}">
                <div class="feature-info float-left">
                <div class="feature-item featured-border1">
                    <div class="float-left">
                     <img src=${getImagePath()}/${prodItemArray[5]} width="125px" style="border-radius: 15%;">
                    </div> &nbsp;&nbsp;
                    <h4>&nbsp;&nbsp;${prodItemArray[2]} </h4>
                        <h6>&nbsp;&nbsp;${prodItemArray[8]} </h6>
                        <p> ${prodItemArray[3]} </p>
                        <p><strong>&nbsp;&nbsp;${parseFloat(prodItemArray[4]).toFixed(2)}</strong> <strong>€</strong> </p>
                        <p>&nbsp;&nbsp;${getTextById(733)}</p>
                    </div>
                </div>
                </a>
            </div>`;
    return productList;
};

//  Elements of box2
function cartHeader() {
    return `<div>`;
}

function cartBottom() {
    return `</div>`;
}

function cartItem(rowNum, cartItemArray) {
    /*
    * cartItemArray:
    * [
    * 0: quantity,
    * 1: productname,
    * 2: id of product,
    * 3: price,
    * 4: pricesum (=quantity*price),
    * 5: tablenumber,
    * 6: id of bill
    * ]
    *
    *
    * */
    if (cartItemArray[1] == "TOTAL") return `<div>
                                                   <h3>&nbsp;${cartItemArray[1]}
                                                   <u>${cartItemArray[4]}&nbsp;${getCurrency()}</u></b>
                                                   </h3>
                                               </div>
                                           </div></td>`;
    else return `<small>(id:${cartItemArray[2]})</small><h3>&nbsp;${cartItemArray[1]}</h3>
                   <span  class="fh5co-price">${cartItemArray[3]}${getCurrency()}</span></td><td>&nbsp;(x${cartItemArray[0]})
                           <span  class="fh5co-price">&nbsp;${cartItemArray[4]}&nbsp;${getCurrency()}</span>`;
};


//  Elements of box3
function deliveryHeader() {
    return "<div>";
}

function deliveryBottom() {
    return "</div>";
}

function orderStatusItem(rowNum, orderStatusItemArray) {
    /*
    * orderStatusItemArray:
    * [
    * 0: id of order,
    * 1: id of product,
    * 2: productname,
    * 3: minutes passed since order,
    * 4: delivery status (listed, ordered, in preparation, in delivery),
    * 5: id of delivery status,
    * 6: billnumber,
    * 7: remarks
    * ]
    *
    *
    * */
    const row = `<small>(id:${orderStatusItemArray[1]})</small><h3 style="color: #fff;">&nbsp;${orderStatusItemArray[2]}</h3>
                  <span  class="fh5co-price">${orderStatusItemArray[3]} minutes</span>&nbsp;(${orderStatusItemArray[4]})
                           <td><span  class="fh5co-price">&nbsp;${orderStatusItemArray[7]} </span>
                 `;
    return row;
};

//////////////////////////////////////////////////////////////////////
const showThemeInfo = function () {
    console.log("\n\tTheme-ID: " + themeID + "\n\tTheme-Name: " + customerTheme);
};

debug("name of waiter:", RELEASE, waiterName);
showThemeInfo();
document.getElementById("confirmOrder").innerText = getTextById(273);
document.getElementById("startOrder").innerText = getTextById(761);

