"use strict";

const httpErrorCallBack = function(err){
    $("#msg").text(err);
};

const apiCall = function (path, dataObject, eventCallBack, httpErrorCallBack, method){
    console.log(`Calling ${path} with ${dataObject} ${eventCallBack(dataObject)} ${httpErrorCallBack} ${method}`);
    const dobj = eventCallBack(dataObject);
    $("#msg").text(dobj);
};

const functionForProject_medovis = function (objectId, dataObject, path, method) {
    $("#msg").text(``);
    if (!dataObject) {
        console.error("Cannot send apiCall1 with null dataobject.");
    } else if (!objectId) {
        console.error("Null object-id for apiCall1.");
    } else {
        // TODO
        let eventCallBack;
        console.log("ObjectId="+objectId);
        switch (objectId) {
        case "butt1":
                //TODO
            eventCallBack = function (dataObject) {
                console.log("Triggered by object 'butt1' with following data: " + JSON.stringify(dataObject));
            };
            break;
        case "searchInput":
                //TODO
            eventCallBack = function (dataObject) {
                const rml = {
                    idlang: dataObject.idlang,
                    content: $("#searchInput").val(),
                    refkey: dataObject.refkey
                };
                dataObject = rml;
                console.log("Triggered by Object 'search' with following data: " + JSON.stringify(dataObject));
                $("#msg").text(dataObject.attribute1 + "-" + dataObject.content);
                return JSON.stringify(dataObject)
            };
            break;
        default:
            //TODO
            eventCallBack = function (dataObject) {
                return JSON.stringify(dataObject);
            };
        }
        //console.log("Object " + objectId + " is calling api");
        apiCall(path, dataObject, eventCallBack, httpErrorCallBack, method);
    }
};



