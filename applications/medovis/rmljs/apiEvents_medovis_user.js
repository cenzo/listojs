// Paste here all the selected events you need for your project
// and change them according to your project-requirements




/***************************************
 CALL-ID: medovis.User.apiCall3
 No description provided
 ***************************************/
$(document)
    .on(
        'mouseenter',
        '.mouseenter_startbiography',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            let dataObject = {};
            functionForProject_medovis(objectId, dataObject, "startbiography", "put");
        }
    );


/***************************************
 CALL-ID: medovis.User.apiCall3
 No description provided
 ***************************************/
$(document)
    .on(
        'mouseleave',
        '.mouseleave_startbiography',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            let dataObject = {};
            functionForProject_medovis(objectId, dataObject,"startbiography","put");
        }
    );


/***************************************
 CALL-ID: medovis.User.apiCall3
 No description provided
 ***************************************/
$(document)
    .on(
        'dblclick',
        '.dblclick_startbiography',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            let dataObject = {};
            functionForProject_medovis(objectId, dataObject,"startbiography","put");
        }
    );


/***************************************
 CALL-ID: medovis.User.apiCall2
 No description provided
 ***************************************/
$(document)
    .on(
        'keypress',
        '.keypress_translatecontent',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const content = $(this).data("content");
            const idlang = $(this).data("idlang");
            const refkey = $(this).data("refkey");

            if (!content) {
                console.error(`Variable 'content' in object with id ${objectId} has no value.`);
            }

            if (!idlang) {
                console.error(`Variable 'idlang' in object with id ${objectId} has no value.`);
            }

            if (!refkey) {
                console.error(`Variable 'refkey' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (content && idlang && refkey) {
                dataObject = {
                    "content":content,
                    "idlang":idlang,
                    "refkey":refkey
                };
            }
            functionForProject_medovis(objectId, dataObject,"translatecontent","put");
        }
    );