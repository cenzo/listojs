

/***  Triggers for 'select'-event    ******************/
/***  SELECT AND AMEND THE APPROPRIATE TRIGGERS  ******************/

/***************************************
	CALL-ID: medovis.User.apiCall1
	Insert new first content for the main language
	***************************************/
$(document)
	.on(
		'select',
		'.select_newcontent',
		function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

			const content = $(this).data("content");
			const idpart = $(this).data("idpart");
			const idtag = $(this).data("idtag");
			const sortorder = $(this).data("sortorder");

		if (!content) {
			 console.error(`Variable 'content' in object with id ${objectId} has no value.`);
		}

		if (!idpart) {
			 console.error(`Variable 'idpart' in object with id ${objectId} has no value.`);
		}

		if (!idtag) {
			 console.error(`Variable 'idtag' in object with id ${objectId} has no value.`);
		}

		if (!sortorder) {
			 console.error(`Variable 'sortorder' in object with id ${objectId} has no value.`);
		}

		let dataObject;
		if (content && idpart && idtag && sortorder) {
			dataObject = {
				"content":content,
				"idpart":idpart,
				"idtag":idtag,
				"sortorder":sortorder
			};
		}
		functionForProject_medovis(objectId, dataObject,"newcontent","put");
		}
	);

/***************************************
	CALL-ID: medovis.User.apiCall2
	No description provided
	***************************************/
$(document)
	.on(
		'select',
		'.select_translatecontent',
		function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

			const content = $(this).data("content");
			const idlang = $(this).data("idlang");
			const refkey = $(this).data("refkey");

		if (!content) {
			 console.error(`Variable 'content' in object with id ${objectId} has no value.`);
		}

		if (!idlang) {
			 console.error(`Variable 'idlang' in object with id ${objectId} has no value.`);
		}

		if (!refkey) {
			 console.error(`Variable 'refkey' in object with id ${objectId} has no value.`);
		}

		let dataObject;
		if (content && idlang && refkey) {
			dataObject = {
				"content":content,
				"idlang":idlang,
				"refkey":refkey
			};
		}
		functionForProject_medovis(objectId, dataObject,"translatecontent","put");
		}
	);

/***************************************
	CALL-ID: medovis.User.apiCall3
	No description provided
	***************************************/
$(document)
	.on(
		'select',
		'.select_startbiography',
		function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

		let dataObject = {};
		functionForProject_medovis(objectId, dataObject,"startbiography","put");
		}
	);

/***************************************
	CALL-ID: medovis.User.apiCall4
	No description provided
	***************************************/
$(document)
	.on(
		'select',
		'.select_selectbiography',
		function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

		let dataObject = {};
		functionForProject_medovis(objectId, dataObject,"selectbiography","put");
		}
	);

