/**
 *
 */

function setTexts() {
    // TODO write here customized texts
    console.log("START HERE");
}

function guest0_categories(category, cattext, jsonRMLdata) {

    var elem = document.getElementById("box0");
    elem.innerHTML = "";
    debug("jsonRMLdata, category '" + category + "'", 0, jsonRMLdata);

    //var menuCat = document.createElement("div");
    // menuCat.setAttribute("id", "fh5co-cat" + category);
    // menuCat.setAttribute("class", "animated");
    // menuCat.setAttribute("data-section", "cat_" + category);
    //document.getElementById("fh5co-container").appendChild(menuCat);
    if (category != '0') {
        var catDiv = document.createElement("a");
        // catDiv.setAttribute('data-nav-section', 'top');
        catDiv.setAttribute('data-category', '0');
        catDiv.setAttribute('class', 'get-products');
       catDiv.setAttribute('href', 'javascript:void(0)');
        var top = document.createElement("i");
        top.setAttribute("class", "glyphicon glyphicon-menu-up");
        catDiv.appendChild(top);
        elem.appendChild(catDiv);
    }
    var firstLevel = 1000;
    for (var k = 0; k < jsonRMLdata.count; k++) {
        // Show only top-level of the categories:
        if (parseInt(jsonRMLdata.rows[k].s[3], 10) > firstLevel) {
            break;
        } else if (k > 0) {
            var bTag = document.createElement("b");
            bTag.innerText = " - ";

            elem.appendChild(bTag);
        }
        catDiv = document.createElement("a");
        // catDiv.setAttribute('data-nav-section', 'cat_' + jsonRMLdata.rows[k].s[0]);
        catDiv.setAttribute('class', 'get-products');
        catDiv.setAttribute('href', 'javascript:void(0)');
        if (k === 0) {
            firstLevel = parseInt(jsonRMLdata.rows[k].s[3], 10);
        }
        //else catDiv.setAttribute('class', 'get-products');
        catDiv.setAttribute('data-category', jsonRMLdata.rows[k].s[0]);
        catDiv.innerText = jsonRMLdata.rows[k].s[1];
        elem.appendChild(catDiv);


    }
    document.getElementById("categoryDesc").innerText=cattext;
    // menuCat.appendChild(box1);
}


function update2_Cart(jsonData) {


}


function guest1_product(jsonRMLdata) {
    // TODO write here the customization-code
    debug("products: ", 0, jsonRMLdata);
    document.getElementById("box1").innerHTML="";


    for (var k = 0; k < jsonRMLdata.count; k++) {

        var fooDesc = document.createElement("div");
        fooDesc.setAttribute("class", "col-lg-4 col-md-4 col-sm-6");
        var imgAnchor = document.createElement("a");
        imgAnchor.setAttribute("class", "fh5co-card-item image-popup");
        var fig = document.createElement("figure");
        var img = document.createElement("img");
        var imgSrc=getImagePath()+"/"+jsonRMLdata.rows[k].s[5];
        img.setAttribute("class", "img-responsive");
        img.setAttribute("src", imgSrc);
        fig.appendChild(img);
        imgAnchor.appendChild(fig);
        var div2 = document.createElement("div");
        div2.setAttribute('class', 'fh5co-text');
        var h2 = document.createElement("h2");
        h2.innerText = jsonRMLdata.rows[k].s[2];
        var p = document.createElement("p");
        var description = jsonRMLdata.rows[k].s[3];
        p.innerText = description.substr(0, 30);
        div2.appendChild(h2);
        div2.appendChild(p);
        
        var fooPrice = document.createElement("p");
        var fooPriceSpan = document.createElement("span");
        var addProduct=document.createElement("i");
        addProduct.setAttribute("class","add-product glyphicon glyphicon-shopping-cart");
        addProduct.setAttribute("data-product",jsonRMLdata.rows[k].s[0]);
        fooPriceSpan.setAttribute("class", "price cursive-font");
        fooPriceSpan.innerHTML = jsonRMLdata.rows[k].s[4]+" € <br />";
        fooPriceSpan.appendChild(addProduct);
        fooPrice.appendChild(fooPriceSpan);
        div2.appendChild(fooPrice);
        imgAnchor.appendChild(div2);
        fooDesc.appendChild(imgAnchor);
        document.getElementById("box1").appendChild(fooDesc);
    }

    


}

function guest2_cart(jsonRMLdata) {
    // TODO write here the customization-code
    debug("box2:", 0, jsonRMLdata);
    update2_Cart(jsonRMLdata);
}

function guest3_orderstatus(jsonData) {

}

var LanguageSelect = {
    pre: "",
    suf: ""
}


jQuery(document).on('click', '.listojs-Cart', function () {
    //event.preventDefault();

    apiCall_listorante_guest_showcart(idbill, function (data) {
        $('.navbar-nav li').each(function () {
            $(this).find('a').removeClass('active');
        });

        update2_Cart(data)
        $('.listojs-Cart').addClass('active');
    }, function (err) {
        debug("ERROR:", release, err);
    });
})


jQuery(document).on('click', '.listojs-OrderStatus', function (event) {
    event.preventDefault();
    apiCall_listorante_guest_deliverystatus(idbill, function (data) {
        $('.navbar-nav li').each(function () {
            $(this).find('a').removeClass('active');
        });
        guest3_orderstatus(data)
        $('.listojs-OrderStatus').addClass('active');
    }, function (err) {
        debug("ERROR:", release, err);
    });
})