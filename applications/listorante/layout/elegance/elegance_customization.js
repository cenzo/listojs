"use strict";
const themeID = 7;
const customerTheme = "elegance";

//  Elements of box0
function categoryHeader() {
    return `<h3>Header</h3>`;
}

function categoryBottom() {
    const release = RELEASE;
    const html=document.getElementById("box0").innerHTML;
    debug2("elegance", "box0.innerHTML", release, [html]);
    return html;
    //return ``;
}

let dNew;

function categoryItem(rowNum, catItemArray, totalRows) {
    /*
    *   catItemArray:
    * [
    * 0: id of productcategory,
    * 1: description of category,
    * 2: parent-id of category,
    * 3: level in category tree,
    * 4: position in category tree,
    * 5: name of category,
    * 6: image of category
    * ]
    *
    * */
   // const release = RELEASE;
    let categoryList;
    dNew = document.createElement("div");
    dNew.setAttribute("class", "item");

    categoryList =  `<div class="item animate" data-animate="fadeInUp"><div class="portfolio-item"><div class="thumb"><img src="http://www.listorante.com/thirdparty/elegance/images/item-1.jpg" alt=""></div><div class="thumb-inner animate" data-animate="fadeInUp"><h4>templatemo is the best</h4><p>Please tell free Bootstrap CSS templates.</p></div></div></div>`;
    dNew.innerHTML = categoryList;
    //debug2("elegance", "category", release, [catItemArray, dNew.innerHTML]);
    document.getElementById("box0").appendChild(dNew);

    if (parseInt(rowNum + "", 10) === parseInt(totalRows + "", 10) - 1) {
       // const bCheck = document.getElementById("box0");
    } else {
        return ``;
    }

};

//  Elements of box1
function productsHeader() {
    return ``;
}

function productsBottom() {
    return ``;
}

function productItem(rowNum, prodItemArray) {
    /*
    * prodItemArray:
    * [
    * 0: idproduct,
    * 1: position in list,
    * 2: product-name,
    * 3: product long description,
    * 4: price,
    * 5: image,
    * 6: id of product category,
    * 7: product-label set by the restaurant (alternatively to idproduct),
    * 8: product short description
    *
    *
    * */
    let productList = `<div class="col-md-4">
                         <div class="fh5co-blog animate-box fadeInUp animated-fast">
                             <a href="javascript:void(0);"   class="blog-bg add-product"
                                           data-product="${prodItemArray[0]}" style="background-image: url(${getImagePath()}/${prodItemArray[5]});"></a>
                             <a href="javascript:void(0);" class="add-product" data-product="${prodItemArray[0]}" style="color:#fff;">
                             <div class="blog-text">
                                <ul class="stuff">
                                   <li><strong><u>${prodItemArray[2]}</u></strong></li>
                                   <li>${prodItemArray[8]}</li>
                                   <li><strong><a href="javascript:void(0)"   class="add-product"
                                           data-product="${prodItemArray[0]}">${Number(prodItemArray[4]).toFixed(2)}
                                            &nbsp;${getCurrency()}<i class="icon-arrow-right22"></i></a></strong></li>
                                   <!--<li><a href="javascript:void(0);" data-categoryText="up"
                                           data-category="${prodItemArray[6]}" class="get-products">
                                           <i class="icon-arrow-right22"></i> <i class="glyphicon glyphicon-level-up"></i></a></li>-->
                                <li>${getTextById(733)}</li>
                                <li>${prodItemArray[3]}</li>
                                </ul>
                             </div></a>
                         </div>
                      </div>`;
    return productList;
};

//  Elements of box2
function cartHeader() {
    return `<table>`;
}

function cartBottom() {
    return `</table>`;
}

function cartItem(rowNum, cartItemArray) {
    /*
    * cartItemArray:
    * [
    * 0: quantity,
    * 1: productname,
    * 2: id of product,
    * 3: price,
    * 4: pricesum (=quantity*price),
    * 5: tablenumber,
    * 6: id of bill
    * ]
    *
    *
    * */
    if (cartItemArray[1] == "TOTAL") return `<tr><td><div class="fh5co-item animate-box fadeInUp animated-fast">
                                               <div class="fh5co-item">
                                                   <h3>&nbsp;${cartItemArray[1]}
                                                   <b class="fh5co-price">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                   <u>${cartItemArray[4]}&nbsp;${getCurrency()}</u></b>
                                                   </h3>
                                               </div>
                                           </div></td></tr>`;
    else return ` <tr class="fh5co-item animate-box fadeInUp animated-fast">
                  <td style="color: #fff width: 25px;"> <small>(id:${cartItemArray[2]})</small></td><td style="width: 300px;"><h3>&nbsp;${cartItemArray[1]}</h3></td>
                  </tr><tr class="fh5co-item animate-box fadeInDown animated-fast"">
                   <td style="width: 100px;"><span  class="fh5co-price">${cartItemArray[3]}${getCurrency()}</span></td><td>&nbsp;(x${cartItemArray[0]})</td>
                           <td style="width: 125px;"><span  class="fh5co-price">&nbsp;${cartItemArray[4]}&nbsp;${getCurrency()}</span></td>
                  </tr> `;
};


//  Elements of box3
function deliveryHeader() {
    return "<TABLE>";
}

function deliveryBottom() {
    return "</TABLE>";
}

function orderStatusItem(rowNum, orderStatusItemArray) {
    /*
    * orderStatusItemArray:
    * [
    * 0: id of order,
    * 1: id of product,
    * 2: productname,
    * 3: minutes passed since order,
    * 4: delivery status (listed, ordered, in preparation, in delivery),
    * 5: id of delivery status,
    * 6: billnumber,
    * 7: remarks
    * ]
    *
    *
    * */
    const row = ` <tr style="color: #fff;" class="fh5co-item animate-box fadeInLeft animated-fast">
                  <td> <small>(id:${orderStatusItemArray[1]})</small></td><td ><h3 style="color: #fff;">&nbsp;${orderStatusItemArray[2]}</h3></td>
                   <td style="color: #fff;"><span  class="fh5co-price">${orderStatusItemArray[3]} minutes</span></td><td>&nbsp;(${orderStatusItemArray[4]})</td>
                           <td><span  class="fh5co-price">&nbsp;${orderStatusItemArray[7]} </span></td>
                  </tr> `;
    return row;
};

//////////////////////////////////////////////////////////////////////
const showThemeInfo = function () {
    console.log("\n\tTheme-ID: " + themeID + "\n\tTheme-Name: " + customerTheme);
};

debug("name of waiter:", RELEASE, waiterName);
showThemeInfo();
