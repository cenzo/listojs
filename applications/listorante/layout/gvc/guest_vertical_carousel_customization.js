/**
 * 
 */

var release = 2;
function setTexts()
	{
		// TODO write here customized texts
		console.log("START HERE");
	}

function guest0_categories(category, cattext, jsonRMLdata)
	{
		console.log($.parseJSON('[{"id":"1","name":"Pizza Margherita ","descr":"Pizza Margherita ","price":"5.99","picurl":"https://images.listorante.com/c2753cd57e00308716b412b9a4aba7176bcba950650061159b15631ae2cfaec9.jpg"},{"id":"3","name":"Pizza Four Seasons","descr":"Pizza Four Seasons","price":"7.99","picurl":"https://images.listorante.com/quattrostagioni.png"},{"id":"4","name":"Pizza X","descr":"Pizza X","price":"8.99","picurl":"https://images.listorante.com/pizzax.png"},{"id":"10","name":"Pizza Margherita - small","descr":"Pizza Margherita - small","price":"3.99","picurl":"https://images.listorante.com/margherita.png"},{"id":"11","name":"Pizza Four Seasons - small","descr":"Pizza Four Seasons - small","price":"5.99","picurl":"https://images.listorante.com/quattrostagioni.png"},{"id":"5","name":"Pizza Y","descr":"Pizza Y","price":"6.99","picurl":"https://images.listorante.com/3c13328df67ccc88b8e6a460afec7b99fbd9f3cf28cf0e93a87866fdd12c3bf4.jpg"}]'));
		var elem = document.getElementById("menu-source");
		elem.innerHTML = "";
		
	

		sessionStorage.setItem('uniqueID', '');
		// TODO write here the customization-code
		for (var k = 0; k < jsonRMLdata.count; k++) {

			var itemdiv = document.createElement("div");
			itemdiv.setAttribute('id', 'cat_'+jsonRMLdata.rows[k].s[0]);
			itemdiv.setAttribute('data-parent', '0');
			itemdiv.setAttribute('data-items', '0');
			itemdiv.setAttribute('class', 'swiper-slide has-children');
			
			itemdiv.setAttribute('style','background-image:url(http://www.listorante.com/development/themes/carousel-vertical/assets/images/pizza.jpg)')
			var itemInnerDiv = document.createElement("div");
			itemInnerDiv.setAttribute('class','caption');
			itemInnerDiv.innerHTML = jsonRMLdata.rows[k].s[1];
			itemdiv.appendChild(itemInnerDiv);
			elem.appendChild(itemdiv);
			checksubcats(jsonRMLdata.rows[k].s[0], elem);
			checkProduct(jsonRMLdata.rows[k].s[0], elem);
			
			
		}

		
		 
        //Run controller
        setTimeout(function(){
        	var controller = new MenuController(
        {
            source         :'menu-source',
            carousel       :'swiper-container',
            container      :'menu-carousel',
            slide          :'swiper-slide-active',
            parent         :'data-parent',
            items          :'data-items',
            prtitle        :'presentation-title',
            prbox          :'presentation-items',
            prefix         :'main-menu',
            message        : 'Please select menu chapter.',


            //Swiper presets
            swiper         :{
                              // effect            : 'coverflow',
                              direction         : 'vertical',
                              slidesPerView     : slidesPerView,
                              spaceBetween      : 16,
                              speed             : 400,
                              grabCursor        : true,
                              centeredSlides    : true,
                              initialSlide      : 2,
                              roundLengths      : false,
                              centerInsufficientSlides: false,

                              // coverflowEffect   : {
                              //   rotate          : 0,
                              //   stretch         : 0,
                              //   depth           : 0,
                              //   modifier        : 0,
                              //   slideShadows    : true,
                              // },
                              navigation: {
                                nextEl: '.swiper-button-next',
                                prevEl: '.swiper-button-prev',
                              },

                            }
        }
    );
        	controller.start();	
        },3000);
        


		/*var elem = document.getElementById("menu-source");
		elem.innerHTML = "";
		var outerCol = document.createElement("DIV");
		outerCol.setAttribute("class", "col-lg-5");
		var catid = 0;

		for (var k = 0; k < 3; k++) {
			var menuList = document.createElement("DIV");
			menuList.setAttribute("class", "menu_list");
			var h1 = document.createElement("h1");
			h1.innerHTML = get(jsonRMLdata, k, "prodcatdesc");
			menuList.appendChild(h1);

			catid = get(jsonRMLdata, k, "idproductcategory");

			var ul = document.createElement("ul");
			ul.setAttribute("class", "list");
			custom1(catid, ul);
			menuList.appendChild(h1);
			menuList.appendChild(ul);
			outerCol.appendChild(menuList);

		}
		elem.appendChild(outerCol);
		menuList.setAttribute("class", "menu_list res-mr");

		for (var k = 3; k < jsonRMLdata.count; k++) {
			var outerCol2 = document.createElement("DIV");
			outerCol2.setAttribute("class", "col-lg-5 offset-lg-1");
			var arrayLength = jsonRMLdata.count;

			for (var k = 3; k < arrayLength; k++) {
				debug("Step2, category " + catid + ":", release, k);
				var menuList2 = document.createElement("DIV");
				menuList2.setAttribute("class", "menu_list res-mr");
				h1 = document.createElement("h1");
				h1.innerHTML = get(jsonRMLdata, k, "prodcatdesc");
				menuList2.appendChild(h1);

				catid = get(jsonRMLdata, k, "idproductcategory");
				var ul2 = document.createElement("ul");
				ul2.setAttribute("class", "list");
				custom1(catid, ul2);
				menuList2.appendChild(h1);
				menuList2.appendChild(ul2);
				outerCol2.appendChild(menuList2);
			}
		}
		elem.appendChild(outerCol2);*/

	}

function checkProduct(catId,elem){
	apiCall_listorante_public_product(catId, function(products)
			{
				if(products.count > 0){
					var jsonArray = {};
					for (var j = 0; j < products.count; j++) {
						var jsonProd = {};
						jsonProd.id = products.rows[j].s[0];
						jsonProd.name = products.rows[j].s[2];
						jsonProd.descr = products.rows[j].s[2];
						jsonProd.price = products.rows[j].s[3];
						jsonProd.picurl = getImagePath()+'/'+products.rows[j].s[4];
						jsonArray[j] = jsonProd;
					}

					$('#cat_'+catId).attr('data-items',JSON.stringify(jsonArray));

				}
			}, function(err)
			{
				debug("ERR", release, err);
			});
}

function checkSubProduct(catId,elem){
	apiCall_listorante_public_product(catId, function(products)
			{
				console.log('CHECKSUBPRODUCT---'+catId);
				if(products.count > 0){
					var jsonArray = {};
					for (var j = 0; j < products.count; j++) {
						var jsonProd = {};
						jsonProd.id = products.rows[j].s[0];
						jsonProd.name = products.rows[j].s[2];
						jsonProd.descr = products.rows[j].s[2];
						jsonProd.price = products.rows[j].s[3];
						jsonProd.picurl = getImagePath()+'/'+products.rows[j].s[4];
						jsonArray[j] = jsonProd;
					}

					$('#sub_'+catId).attr('data-items',JSON.stringify(jsonArray));

				}
			}, function(err)
			{
				debug("ERR", release, err);
			});
}


function checksubcats(catId,elem){
	apiCall_listorante_public_category(catId,function(jsonRMLdata){
			if(jsonRMLdata.count > 0){
				console.log('CHECKSUB-->'+catId);
				for (var k = 0; k < jsonRMLdata.count; k++) {
					if(jsonRMLdata.rows[k].s[0] != catId){
						var itemdiv = document.createElement("div");
						itemdiv.setAttribute('id', 'sub_'+jsonRMLdata.rows[k].s[0]);
						itemdiv.setAttribute('data-parent', 'cat_'+catId);
						itemdiv.setAttribute('data-items', '0');
						itemdiv.setAttribute('class', 'swiper-slide has-children');
						itemdiv.setAttribute('style','background-image:url(http://www.listorante.com/development/themes/carousel-vertical/assets/images/pizza.jpg)')
						var itemInnerDiv = document.createElement("div");
						itemInnerDiv.setAttribute('class','caption');
						itemInnerDiv.innerHTML = jsonRMLdata.rows[k].s[1];
						itemdiv.appendChild(itemInnerDiv);
						elem.appendChild(itemdiv);
						checkSubProduct(jsonRMLdata.rows[k].s[0], elem);
					}
				}

			}
	}, function(err){
				debug("ERR", release, err);
	});
}


function update2_Cart(jsonData)
	{
		var prodArray= [];
		apiCall_listorante_guest_deliverystatus(idbill, function(data){
			var d = data;
			var prodCount = d.count;
			for(var j=0; j<prodCount; j++){
				prodArray.push(d.rows[j].s[4]);
			}
			//console.log(JSON.stringify(prodArray));
			document.getElementById("presentation-items").innerHTML = "";
			var boxDiv = document.createElement("div");
			boxDiv.setAttribute("class", "");
			$('#presentation-title').text('My Order');
			
			var table = document.createElement("table");
			table.setAttribute("class", "table");
			var boxBody = document.createElement("tbody");
			var boxHead = document.createElement("thead");
			var trh = document.createElement("tr");
			var th1 = document.createElement("th");
			var th2 = document.createElement("th");
			//var th3 = document.createElement("th");
			var th4 = document.createElement("th");
			var th5 = document.createElement("th");
			var StatusValue = "";
			th1.innerHTML = "Quantity";
			th2.innerHTML = "Product Name";
			//th3.innerHTML = "Status";
			th4.innerHTML = "Unit Price";
			th5.innerHTML = "Price";
			trh.appendChild(th1);
			trh.appendChild(th2);
			//trh.appendChild(th3);
			trh.appendChild(th4);
			trh.appendChild(th5);
			boxHead.appendChild(trh);
			var arrayLength = jsonData.count;
			for (var i = 0; i < arrayLength; i++) {
				var qty = jsonData.rows[i].s[0];
				var prodname = jsonData.rows[i].s[1];
				var idproduct = jsonData.rows[i].s[2];
				var price = jsonData.rows[i].s[3];
				var pricesum = jsonData.rows[i].s[4];
				var id = document.createElement("span");
				// var con = parseInt(i) + 1; 
				if (i < prodCount){
					var StatusValue = prodArray[i];
				}
				
				id.setAttribute("class", "label label-success bg-purple");
				id.innerHTML = "id: " + idproduct;

				var tr = document.createElement("tr");
				var td1 = document.createElement("td");
				var td2 = document.createElement("td");
				var prod = document.createElement("span");
				//var td3 = document.createElement("td");
				var td4 = document.createElement("td");
				var td5 = document.createElement("td");
				prod.setAttribute("class", "badge bg bg-navy");
				prod.innerHTML = prodname;
				td1.innerHTML = qty;
				//td3.innerHTML = StatusValue;
				td2.appendChild(prod);
				td4.innerHTML = price + "&nbsp;" + getCurrency();
				td5.innerHTML = "<b>" + pricesum + getCurrency() + "</b>";
				if (qty > 1) {

					var qtyLabel = document.createElement("span");
					qtyLabel.setAttribute("class", "label label-success bg-purple");
					qtyLabel.innerHTML = "<b> x " + qty + "</b>";

					td2.appendChild(qtyLabel);
				}
				tr.appendChild(td1);
				tr.appendChild(td2);
				//tr.appendChild(td3);
				tr.appendChild(td4);
				tr.appendChild(td5);
				if (idproduct > 0)
					boxBody.appendChild(tr);
				else {
					var total = document.createElement("tr");
					total.innerHTML = "<tr><td></td><td></td><td>Total </td><td>" + "<ins><b>"
							+ pricesum + getCurrency() + "</b></ins>"
							+ "</td></tr>"
				}
			}
			if (total)
				boxBody.appendChild(total);
			table.appendChild(boxHead);
			table.appendChild(boxBody);
			boxDiv.appendChild(table);

			document.getElementById("presentation-items").appendChild(boxDiv);
		});

		

		
	}


function guest1_product(jsonRMLdata)
	{
		// TODO write here the customization-code
		debug("box1", release, jsonRMLdata);
	}

function guest2_cart(jsonRMLdata)
	{
		// TODO write here the customization-code
		debug("box2:", release, jsonRMLdata);
		update2_Cart(jsonRMLdata);
	}

function guest3_orderstatus(jsonData)
	{
		var prodArray= [];
		
			console.log(JSON.stringify(jsonData));
			document.getElementById("presentation-items").innerHTML = "";
			var boxDiv = document.createElement("div");
			boxDiv.setAttribute("class", "");

			$('#presentation-title').text("Items Delivery Status");

			var table = document.createElement("table");
			table.setAttribute("class", "table");
			var boxBody = document.createElement("tbody");
			var boxHead = document.createElement("thead");
			var trh = document.createElement("tr");
			var th1 = document.createElement("th");
			var th2 = document.createElement("th");
			var th3 = document.createElement("th");
			var th4 = document.createElement("th");
			var th5 = document.createElement("th");
			th1.innerHTML = "Item Name";
			th2.innerHTML = "Item Ordered";
			th3.innerHTML = "Item Status ";
			th4.innerHTML = "Bill Number";
			th5.innerHTML = "Remarks";
			trh.appendChild(th1);
			trh.appendChild(th2);
			trh.appendChild(th3);
			trh.appendChild(th4);
			trh.appendChild(th5);
			boxHead.appendChild(trh);
			var arrayLength = jsonData.count;
			for (var i = 0; i < arrayLength; i++) {
				var prodname = jsonData.rows[i].s[2];
				var min = jsonData.rows[i].s[3];
				var status = jsonData.rows[i].s[4];
				var billnumber = jsonData.rows[i].s[6];
				var Remarks = jsonData.rows[i].s[7];

				var tr = document.createElement("tr");
				var td1 = document.createElement("td");
				var td2 = document.createElement("td");
				var td3 = document.createElement("td");
				var td4 = document.createElement("td");
				var td5 = document.createElement("td");
				td1.innerHTML = prodname;
				td2.innerHTML = min+' mins ago';
				td3.innerHTML = status;
				td4.innerHTML = billnumber
				td5.innerHTML = Remarks;
				tr.appendChild(td1);
				tr.appendChild(td2);
				tr.appendChild(td3);
				tr.appendChild(td4);
				tr.appendChild(td5);
				boxBody.appendChild(tr);
			}

			table.appendChild(boxHead);
			table.appendChild(boxBody);
			boxDiv.appendChild(table);

			document.getElementById("presentation-items").appendChild(boxDiv);
	}

function custom1(catid, ul)
	{
		apiCall_listorante_public_product(catid, function(products)
			{
				for (var j = 0; j < products.count; j++) {
					var buy = document.createElement("a");
					buy.setAttribute("class", "add-product");
					buy.setAttribute("data-product", get(products, j,
						"idproduct"));
					var li = document.createElement("li");
					var h4 = document.createElement("h4");
					h4.innerHTML = get(products, j, "prodname");
					var price = document.createElement("span");
					price.innerHTML = get(products, j, "price");
					// var p = document.createElement("p");
					var img = document.createElement("img");
					img.setAttribute("src", getImagePath() + "/"
							+ get(products, j, "image"));
					img.setAttribute("width", "100");

					// p.appendChild(img);

					buy.appendChild(img);
					
					h4.appendChild(buy);
					h4.appendChild(price);
					li.appendChild(h4);
					li.appendChild(buy);

					ul.appendChild(li);
				}
			}, function(err)
			{
				debug("ERR", release, err);
			});
	}

var LanguageSelect = {
	pre : "",
	suf : ""
}


jQuery(document).on('click', '.listojs-Cart', function()
	{
		//event.preventDefault();

		apiCall_listorante_guest_showcart(idbill,function(data)
			{
				$('.navbar-nav li').each(function(){
					$(this).find('a').removeClass('active');
				});

				update2_Cart(data)
				$('.listojs-Cart').addClass('active');
			}, function(err)
			{
				debug("ERROR:",release,err);
			});
	})


jQuery(document).on('click', '.listojs-OrderStatus', function(event){
	event.preventDefault();
	apiCall_listorante_guest_deliverystatus(idbill,function(data)
		{
			$('.navbar-nav li').each(function(){
				$(this).find('a').removeClass('active');
			});
			guest3_orderstatus(data)
			$('.listojs-OrderStatus').addClass('active');
		}, function(err)
		{
			debug("ERROR:",release,err);
		});
})