release = RELEASE;

const themeID = 3;
const categoryHeader = function () {
};
const categoryItem = function () {
};
const categoryBottom = function () {
};
const productsHeader = function () {
};
const productItem = function () {
};
const productsBottom = function () {
};
const cartHeader = function () {
};
const cartItem = function () {
};
const cartBottom = function () {
};
const deliveryHeader = function () {
};
const deliveryItem = function () {
};
const deliveryBottom = function () {
};
const PROCERR = "Please report this bug. Processing stopped unexpectedly. CODE: ";


const ULStyle = {
    listart: '<li>',
    lianchorstart: '<a href="javascript:void(0);" class="navcategory" data-categoryid="',
    datadescription: '" data-description="',
    datasection: '" data-nav-section="',
    datacatstyle: '" data-idcatstyle="',
    lianchorspanclose: '"><span>',
    lianchorclose: '</span</a>',
    liclose: '</li>',
    liwithclassactive: '<li class="active">',
    elements: [],
};


const DIVstyle1 = {
    boxdivstart: '<div class="col-md-3 style1">',
    serviceboxdivstart: '<div class="box-services">',
    imageBoxdiv: '<div class="product-image to-animate">',
    imagetagstart: '<img src="',
    imagetagclose: '" /> ',
    productdivstart: '<div class="fh5co-post to-animate">',
    h3tagstart: '<h3>',
    h3tagclose: '</h3>',
    ptagstart: '<p>',
    ptagclose: '</p>',
    divClose: '</div>',
    elements: [],
};
const DIVstyle2 = {
    boxdivstart: '<div class="col-md-4">',
    gridboxdivstart: '<div class="project-grid to-animate-2" style="background-image: url(',
    backgroundimageclose: ');">',
    productdivstart: '<div class="desc" style="padding-left:2em; padding-right:2em;">',
    h3tagstart: '<h3>',
    h3tagclose: '</h3>',
    anchorstart: '<a><i class="glyphicon glyphicon-hand-right">&nbsp;',
    anchorclose: '</i>&nbsp;</a>',
    spanstart: '<span>',
    spanclose: '</span>',
    ptagstart: '<p>',
    ptagclose: '</p>',
    divClose: '</div>',
    elements: [],
};

const DIVstyle3 = {
    boxdivstart: '<div class="col-md-3 col-sm-6">',
    priceboxdivstart: '<div class="price-box to-animate" style="height:400px;">',
    imageBoxdiv: '<div class="price">',
    imagetagstart: "<img src='",
    imagetagclose: "' />",
    h2tagstart: '<h2 class="pricing-plan">',
    h2tagclose: '</h2>',
    ptagstart: '<p style="text-align:left; font-size:16px;">',
    ptagclose: '</p>',
    divClose: '</div>',
    elements: [],
};

/*function setLanguage(id) {
    locale = id;
    debug("setting language to " + id, release);
    console.log("setting language to " + id);
    apiCall_listorante_public_assignedtexts(getLayoutId(), function (data) {
        for (let k = 0; k < data.count; k++) {
            document.getElementById(data.rows[k].s[0]).innerText = data.rows[k].s[3];
        }

        getcategories();
        document.getElementById("languageDiv").innerHTML = "";
        document.getElementById("fh5co-header").scrollIntoView(true);
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });
}*/

function arrowStep(context, prev, next) {
    let start = "";
    if (prev === 0) {
        start = `<div class="text-center to-animate-2">
            <h1>
                <a href="javascript:void(0)" data-page="${next}"  class="take-tour"><i class="glyphicon glyphicon-forward"></i></a>
            </h1>
            <br>
        </div>`;
    } else if (next === 0) {
        start = `<div class="text-center to-animate-2">
           <h1>
              <a href="javascript:void(0)" data-page="${prev}"  class="take-tour"><i class="glyphicon glyphicon-backward"></i></a>
           </h1>
          <br>
        </div>`;
    } else {
        start = `<div class="text-center to-animate-2">
          <h1>
             <a href="javascript:void(0)" data-page="${prev}" class="take-tour"><i class="glyphicon glyphicon-backward"></i></a>
             <a href="javascript:void(0)" data-page="${next}" class="take-tour"><i class="glyphicon glyphicon-forward"></i></a>
          </h1>
          <br>
        </div>`;
    }
    return start;
}


function tourStepHtml(context, orderArray, surroundHtml) {
    apiCall_listorante_public_longtext(context, function (data) {
        const idx = orderArray.indexOf(context);
        let prev, nxt;
        if (idx === 0) prev = 0; else prev = orderArray[idx - 1];
        if (idx === orderArray.length - 1) nxt = 0;
        else nxt = orderArray[idx + 1];
        let stringValue = '<br><h1 class=\"text-right\"><p id =\"includedHTMLCloseButton\"><a href=\"javascript:void(0)\" class=\"closeIncludedHTML t\"><i class=\"glyphicon glyphicon-remove\"></i></a></p></h1>';
        stringValue += arrowStep(context, prev, nxt);
        if (data.rows.length === 0) {
            const noSuchPage = {
                "responseText": getTextById(404) + " (Context " + context + ")",
                "status": 404
            }
            showHttpErrorMessage("includedHTML", noSuchPage);
        } else {
            for (let i = 0; i < data.rows.length; i++) {
                debug("data.rows[i].s[6] ", 0, data.rows[i].s[6]);
                if (surroundHtml && data.rows[i].s[6] == 0) {
                    stringValue += surroundHtml(data.rows[i].s[0], i);
                    debug("surroundHtml ", 0, surroundHtml(data.rows[i].s[0], i));
                } else stringValue += data.rows[i].s[0];
            }
            stringValue += "<br><br>" + arrowStep(context, prev, nxt) + "<br>";
        }
        debug("stringValue", "" + release, stringValue);
        document.getElementById("includedHTML").innerHTML = stringValue;

        showTheIncludeSection();
    }, function (err) {
        if (err.responseText) return err.responseText;
        else return "ERROR: no content found for context " + context;
    });
}


function embedHTML(context, language, divID) {
    let embeddingDiv = document.getElementById(divID);
    locale = language;
    apiCall_listorante_public_longtext(context, function (data) {
        let stringValue = '<br><h1 class=\"text-right\"><p id =\"includedHTMLCloseButton\"><a href=\"javascript:void(0)\" class=\"closeIncludedHTML text-right\"><i class=\"glyphicon glyphicon-remove\"></i></a></p></h1>';
        if (data.rows.length === 0) {
            const noSuchPage = {
                "responseText": getTextById(404),
                "status": 404
            }
            showHttpErrorMessage(divID, noSuchPage);
        } else {
            for (let i = 0; i < data.rows.length; i++) stringValue += data.rows[i].s[0];
            embeddingDiv.innerHTML = stringValue;
            showTheIncludeSection();
        }
    }, function (err) {
        showHttpErrorMessage(divID, err);
    });
}


function setTexts() {
    console.log("Setting texts..");
    //setElemText(47);
    setElemText(225);
    //setElemText(614);
    setElemText(615);
    setElemText(713);
    setElemText(723);
    setElemText(735);
};


function getcategories() {
    console.log("Call getcategories()... ")
    let navHtml = '';
    apiCall_listorante_public_categories(function (data) {
        if (data._rc > 0) {
            // showHttpErrorMessage("box0", data);
            document.getElementById("interstitial").innerHTML = "ERROR: " + data._rc;
            return;
        } else {
            document.getElementById("interstitial").innerHTML = "";
        }
        jQuery('ul.listCategories').html(` <li class=" call-to-action">
                          <a class="log-in redirectToAccess" href="javascript:void(0);"><span
                                             id="text615">${getTextById(615)}</span> </a></li>`);
        document.getElementById("interstitial").innerHTML = "<!--" + PROCERR + "[LB1] Login-button was loaded. --> ";
        for (let i = 0; i < data.count; i++) {
            if (data.rows[i].s[13] === "1") {
                let idcatstyle = data.rows[i].s[12];
                if (i === 0) {
                    navHtml += ULStyle.liwithclassactive + ULStyle.lianchorstart + data.rows[i].s[1] + ULStyle.datadescription + data.rows[i].s[2] + ULStyle.datasection + data.rows[i].s[11] + ULStyle.datacatstyle + data.rows[i].s[12] + ULStyle.lianchorspanclose + data.rows[i].s[11] + ULStyle.lianchorclose + ULStyle.liclose;
                    jQuery('section.style' + idcatstyle).find('.cat_description').text(data.rows[i].s[2]);
                    jQuery('section.style' + idcatstyle).find('.cat_title').text(data.rows[i].s[11]);
                    jQuery('section.style' + idcatstyle).attr("data-section", data.rows[i].s[11]);
                    showProducts(data.rows[i].s[1], data.rows[i].s[12]);
                } else {
                    navHtml += ULStyle.listart + ULStyle.lianchorstart + data.rows[i].s[1] + ULStyle.datadescription + data.rows[i].s[2] + ULStyle.datasection + data.rows[i].s[11] + ULStyle.datacatstyle + data.rows[i].s[12] + ULStyle.lianchorspanclose + data.rows[i].s[11] + ULStyle.lianchorclose + ULStyle.liclose;
                }
            }
        }
        document.getElementById("currentserver").innerText = "[" + getAPIServerPath() + "/" + WEBSERVER + "]";
        jQuery('ul.listCategories').prepend(navHtml);
        document.getElementById("interstitial").innerHTML = "<!--" + PROCERR + "[ML1] menu was loaded -->";
    }, function (err) {
        debug("ERROR:", release, err);
        const errorPtag=document.createElement("p");
        const errorAtag=document.createElement("a");
        errorAtag.setAttribute("href","/index.html");
        errorAtag.innerText=getTextById(754);
        errorPtag.appendChild(errorAtag);
        errorPtag.setAttribute("id","errorpTag");
        showHttpErrorMessage("errorpTag", err);
        document.getElementById("interstitial").innerHTML = "";
    });
}


function showProducts(catID, idcatstyle) {
    apiCall_listorante_public_product(catID, function (products) {
        let productsHtml = '';
        for (var j = 0; j < products.count; j++) {

            if (idcatstyle == '1') {

                productsHtml += DIVstyle1.boxdivstart + DIVstyle1.serviceboxdivstart + DIVstyle1.imageBoxdiv + DIVstyle1.imagetagstart + getImagePath() + '/' + products.rows[j].s[5] + DIVstyle1.imagetagclose + DIVstyle1.divClose + DIVstyle1.productdivstart + DIVstyle1.h3tagstart + products.rows[j].s[2] + DIVstyle1.h3tagclose + DIVstyle1.ptagstart + products.rows[j].s[8] + DIVstyle1.ptagclose + DIVstyle1.divClose + DIVstyle1.divClose + DIVstyle1.divClose;

            } else if (idcatstyle == '2') {

                productsHtml += DIVstyle2.boxdivstart + DIVstyle2.gridboxdivstart + getImagePath() + '/' + products.rows[j].s[5] + DIVstyle2.backgroundimageclose + DIVstyle2.productdivstart + DIVstyle2.h3tagstart + DIVstyle2.anchorstart + products.rows[j].s[2] + DIVstyle2.anchorclose + DIVstyle2.h3tagclose + DIVstyle2.spanstart + products.rows[j].s[8] + DIVstyle2.spanclose + DIVstyle2.divClose + DIVstyle2.divClose + DIVstyle2.divClose;

            } else if (idcatstyle == '3') {
                productsHtml += DIVstyle3.boxdivstart + DIVstyle3.priceboxdivstart + DIVstyle3.h2tagstart + products.rows[j].s[2] + DIVstyle3.h2tagclose + DIVstyle3.imageBoxdiv + DIVstyle3.imagetagstart + getImagePath() + '/' + products.rows[j].s[5] + DIVstyle3.imagetagclose + products.rows[j].s[8] + DIVstyle3.divClose + DIVstyle3.ptagstart + products.rows[j].s[3] + DIVstyle3.ptagclose + DIVstyle3.divClose + DIVstyle3.divClose;
            }
        }
        jQuery('section.style' + idcatstyle).find('.productsRow').html(productsHtml);
        document.getElementById("interstitial").innerHTML = "<!-- " + PROCERR + "[PL1] products were loaded -->";
        jQuery('section.customsection').each(function () {
            $(this).addClass('hide');
        });
        jQuery('section.style' + idcatstyle).removeClass('hide');
        jQuery('section.style' + idcatstyle).fadeIn('slow');
        document.getElementById("interstitial").innerHTML = "";
    }, function (err) {
        debug("ERR", release, err);
        document.getElementById("interstitial").innerHTML = "";
    });

}


function loadcategorydata(catID, cat_title, cat_description, idcatstyle) {
    jQuery('.cat_description').text(cat_description);
    jQuery('.cat_title').text(cat_title);
    jQuery('section.style' + idcatstyle).attr("data-section", cat_title);
    // jQuery('.cat_description').parents('section').attr("data-section",cat_title);
    jQuery('.cat_description').parents('section').removeClass('animated');
    // jQuery('.cat_description').parents('section').addClass('to-animate');
    jQuery('.cat_title').parents('section').removeClass('animated');
    // jQuery('.cat_title').parents('section').addClass('to-animate');
    showProducts(catID, idcatstyle);
    jQuery('.navbar-header .js-fh5co-nav-toggle').trigger('click');
    //faqAnimate();
    hideTheIncludeSection();

}

getcategories();
setTexts();

jQuery(document).ready(function () {
    //console.log('check----' + customerid);
    if (customerID == '9' || customerID == 9) {
    } else {
        const errText = getTextById(404);
        const errorMessageTxt = document.getElementById('errorMessageID');
        //console.log("errorMessageTxt-Object: " + errorMessageTxt);
        if (errorMessageTxt) errorMessageTxt.innerText = errText;
        if (customerID) window.open("err.html?lang=" + locale, "_self");
    }
});

jQuery(document).on('click', '.navcategory', function () {
    $('.listCategories li').each(function () {
        $(this).removeClass('activeNavItem');
        $(this).removeClass('active');
    });
    $(this).parent('li').addClass('activeNavItem');
    var catID = $(this).data('categoryid');
    var cat_title = $(this).data('nav-section');
    var cat_description = $(this).data('description');
    var idcatstyle = $(this).data('idcatstyle');
    loadcategorydata(catID, cat_title, cat_description, idcatstyle);

});


jQuery(document)
    .on(
        'click',
        '.selectTheLanguage',
        function () {
            const id = $(this).attr('data-lang');
            console.log("language clicked: " + id);
            setLanguage(id);
            hideTheIncludeSection();
        });


function showTheIncludeSection() {
    //let section1 = document.getElementById("includedHTML");
    //section1.hidden = false;
    let section2 = document.getElementById("fh5co-pricing");
    let section3 = document.getElementById("fh5co-explore");
    let section4 = document.getElementById("fh5co-services");
    if (section2) section2.hidden = true;
    if (section3) section3.hidden = true;
    if (section4) section4.hidden = true;
}

function hideTheIncludeSection() {
    let section1 = document.getElementById("includedHTML");
    if (section1) section1.innerHTML = "";
    //section1.hidden = false;
    let section2 = document.getElementById("fh5co-pricing");
    let section3 = document.getElementById("fh5co-explore");
    let section4 = document.getElementById("fh5co-services");
    if (section2) section2.hidden = false;
    if (section3) section3.hidden = false;
    if (section4) section4.hidden = false;
}

jQuery(document).on(
    'click',
    '.closeIncludedHTML',
    function () {
        hideTheIncludeSection();
    }
);

jQuery(document).on('click', '.callPage', function () {
    const calledPage = $(this).data("page");
    let currentLanguage = locale;
    let calledLanguage = $(this).data("language");
    if (!calledLanguage) {
        calledLanguage = locale;
    }
    embedHTML(calledPage, calledLanguage, "includedHTML");
    showTheIncludeSection();
    document.getElementById('includedHTML').scrollIntoView(true);
    locale = currentLanguage;
});

jQuery(document).on('click', '.redirectToAccess', function () {
    // remove customerID, otherwise login will try to login with customerID=9 (homepage of listorante)
    sessionStorage.removeItem("theCustomerID");
    customerID = null;
    window.open("/applications/listorante/layout/adminLTE/access.html", "_self");
});

jQuery(document).on('click', '.take-tour', function () {

    const context = $(this).data("page");
    //const prevPage = $(this).data("prev");
    //const nextPage = $(this).data("next");
    const pageArray = [4, 5, 6];
    tourStepHtml(context, pageArray, function (sData, i) {
        const rem = i % 4;
        let str = `<div class="row">`;
        debug("rem", 0, rem, i);
        switch (rem) {
            case 0:
                str += `<div class="col-md-10 to-animate fadeInDown animated">${sData}</div>`
                break;
            case 1:
                str += `<div class="col-md-10 to-animate fadeInLeft animated">${sData}</div>`
                break;
            case 2:
                str += `<div class="col-md-10 to-animate fadeInRight animated">${sData}</div>`
                break;
            default:
                str += `<div class="col-md-10 to-animate fadeInUp animated">${sData}</div>`
        }
        str += `<br></div><br>`;
        return str;
    });


    if (document.getElementById("includedHtml")) document.getElementById("includedHtml").focus();
});

