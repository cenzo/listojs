"use strict";

let OPN_LNK = "Open this link";
let ABOUT = " About dovis.it"
let RSRV_LNK = "Reserve this short url";
let BID_LNK = "Place a bid for this short url";
let UNKNOWN_ERR = "Unknown error";
let TEXT_NO_METADATA = "No metadata available";
let TEXT_NO_USERDESC = "No user description available";
let TEXT_BY1 = "This page";
let TEXT_BY2 = "was created by ";
let TEXT_CREATED = " Created";
let TEXT_LAST_UPDATED = "Last update";
let TEXT_ANON = "Undiclosed";
let TEXT_CLICK_METADATA = "Click to read metadata";
let TEXT_CLICK_USERDESC = "Click to read a description";
let TEXT_TOS = "Terms of service";
let TEXT_PRIVACY = "Privacy Policy";


let dgi = function (el) {
    return document.getElementById(el);
}
let dce = function (el) {
    return document.createElement(el);
}
let ac = function (p, c) {
    p.appendChild(c);
}

let l = navigator.language;// || navigator.browserLanguage;
if (l.startsWith("de")) {
    OPN_LNK = "Diesen link öffnen";
    RSRV_LNK = "Diese Kurz-Url reservieren:";
    BID_LNK = "Angebot für diese Kurz-Url abgeben";
    UNKNOWN_ERR = "Fehler";
    ABOUT = "Über dovis.it";
    TEXT_NO_METADATA = "Keine Metadaten vorhanden";
    TEXT_NO_USERDESC = "Es wurde keine Beschreibung hinterlegt";
    TEXT_BY1 = "Diese Seite";
    TEXT_BY2 = "wurde erstellt von: ";
    TEXT_CREATED = " Erstellt";
    TEXT_LAST_UPDATED = "Letzte Aktualisierung";
    TEXT_ANON = "Nicht öffentlich";
    TEXT_CLICK_METADATA = "Hier klicken um die metadaten des Links zu lesen";
    TEXT_CLICK_USERDESC = "Hier klicken um die Beschreibung zu lesen";
    TEXT_TOS = "AGB";
    TEXT_PRIVACY = "Privacy-Richtlinie";
} else if (l.startsWith("it")) {
    OPN_LNK = "Apri questo link";
    RSRV_LNK = "Prenota questa Url breve";
    BID_LNK = "Partecipa all'asta per questa url breve";
    UNKNOWN_ERR = "Errore";
    ABOUT = "Cos'é dovis.it";
    TEXT_NO_METADATA = "Non ci sono metadati disponibili";
    TEXT_NO_USERDESC = "Non é stata fornita una descrizione per il link";
    TEXT_BY1 = "Questa pagina";
    TEXT_BY2 = "é stata creata da:";
    TEXT_CREATED = " Data di creazione";
    TEXT_LAST_UPDATED = "Ultimo aggiornamento";
    TEXT_ANON = "non pubblicato";
    TEXT_CLICK_METADATA = "Clicca qui per leggere i metadati di questo link";
    TEXT_CLICK_USERDESC = "Clicca qui per leggere una descrizione";
    TEXT_TOS = "Condizioni di utilizzo";
    TEXT_PRIVACY = "Regolamento privacy";
}
dgi("TOS").innerText = TEXT_TOS;
dgi("PRIVACY").innerText = TEXT_PRIVACY;
dgi("back_home").href = APPLICATION_HOMEPAGE;
dgi("about").innerText = ABOUT;
dgi("click_4_metadata").innerText = TEXT_CLICK_METADATA;
dgi("click_4_userdesc").innerText = TEXT_CLICK_USERDESC;
let goToDovis = function () {
    window.location.href = "https://test.dovis.it";
}
let goToTOS = function () {
    window.location.href = "https://test.dovis.it#TOS";
}
let goToPrivacy = function () {
    window.location.href = "https://test.dovis.it#PRIVACY";
}
let bid = function (r, s, t, sc, c) {
    r.setAttribute("href",
        `${APPLICATION_HOMEPAGE}?bidurl=${sc}`)
    if (c === 1 || c > 5) {
        s.innerHTML = ` <a href="mailto:support@dovis.it?subject=BID on short url: ${sc}">
                         <img src="img/judge-gavel.jpg" style="max-width:50px;" alt="reserve ${sc}">
                            <b>${BID_LNK.toUpperCase()}</b>
                          </a>`;
        t.innerText = `${APPLICATION_HOMEPAGE.toLowerCase()} / ${sc}`;
    } else {
        s.innerHTML = ` <a href="mailto:support@dovis.it?subject=RESERVE short url: ${sc}">
                            <img src="img/shopping-icon.png" style="max-width:50px;" alt="reserve ${sc}">
                            <b>${RSRV_LNK.toUpperCase()}</b>
                         </a>`;
        t.innerText = `${APPLICATION_HOMEPAGE.toLowerCase()} / ${sc}`;
    }
}


console.log("pathname: " + window.location.pathname + " \n search: " + window.location.search + "\nhash:" + window.location.hash);
console.log("server: ", selectedServer);

let p = window.location.pathname;

let sup = p.substring(1 + p.lastIndexOf("/"), p.length);
let rdUrl = dgi("rdUrl");
let rdUrl2 = dgi("rdUrl2");
let rdirLabel = dgi("linkLabel");
let txt = dgi("clickText");
let shortUrl = dgi("shorturl");
let shortUrlp = dgi("shorturlp");
let shortUrlpp = dgi("shorturlpp");
let shUrl = dgi("shUrl");
let shUrlp = dgi("shUrlp");
let shUrlpp = dgi("shUrlpp");
let sc;
let getAttrs = false;
let getUDesc = false;
let s, v;
let blogUserName, blogTs, blogUserTitle, blogUserLastName, blogUserEmail, blogUpdateTs, showAuthor, showEmail;

const showSomeDesc = function () {
    let pageTitle = ``;
    const goodTitle = function (str) {
        // Beware: some values are stored with string 'undefined' or 'null' in database
        return str && str !== 'undefined' && str !== 'null';
    }
    if (goodTitle(s[16])) pageTitle = s[16];
    else if (goodTitle(s[11])) pageTitle = s[11];
    else if (goodTitle(s[14])) pageTitle = s[14];
    else if (goodTitle(s[12])) pageTitle = s[12];
    else if (goodTitle(s[10])) pageTitle = s[10];
    else if (goodTitle(s[13])) pageTitle = s[13];
    dgi("usertitle").innerHTML = pageTitle;
}
const showNaked = function () {
    shortUrl.innerHTML = `<b>${sc}</b>`;
    shortUrlp.innerText = `${sc}+`;
    shortUrlpp.innerText = `${sc}++`;
    shUrlp.setAttribute("onclick", "showUserDesc()");
    shUrlpp.setAttribute("onclick", "showMetaData()");
    dgi("udesc").innerHTML = ``;
    dgi("attrs").innerHTML = ``;
}

const showUserDesc = function () {
    shortUrl.innerHTML = `<b>${sc}+</b>`;
    shortUrlp.innerText = `${sc}`;
    shortUrlpp.innerText = `${sc}++`;
    shUrlp.setAttribute("onclick", "showNaked()");
    shUrlpp.setAttribute("onclick", "showMetaData()");

    dgi("udesc").innerHTML = ``;
    dgi("attrs").innerHTML = ``;
    const htm = dce("div");
    if (s[9] && s[9] !== 'undefined') {
        htm.innerHTML = `${s[9]}`;
    } else {
        htm.innerHTML = `<hr> - ${TEXT_NO_USERDESC} - <hr>`;
    }
    ac(dgi("udesc"), htm);
}

const showMetaData = function () {
    shortUrl.innerHTML = `<b>${sc}++</b>`;
    shortUrlp.innerText = `${sc}+`;
    shortUrlpp.innerText = `${sc}`;
    shUrlp.setAttribute("onclick", "showUserDesc()");
    shUrlpp.setAttribute("onclick", "showNaked()");

    dgi("udesc").innerHTML = ``;
    dgi("attrs").innerHTML = ``;
    const meta = dce("div");

    ac(dgi("attrs"), meta);
    const br = dce("br");
    ac(meta, br);
    let metAvail = false;
    let sv = function (i) {
        if (s[i] && s[i] !== 'undefined') {
            metAvail = true;
            let p = dce("p");
            let t = v[i].toUpperCase();
            p.innerHTML = `${t}:&nbsp;${s[i]}`;
            p.setAttribute("class", "bluemeta");
            //const br = dce("br");
            ac(dgi("attrs"), p);
            //ac(dgi("attrs"), br);
        }
    }
    for (let l = 10; l < 16; l++) sv(l);
    if (!metAvail) dgi("attrs").innerText = TEXT_NO_METADATA;
    showSomeDesc();
}
let redirect = function () {
    shortUrl.innerHTML = `<b>${sup}</b> `;
    shortUrlp.innerText = `  ${sup}`;
    shortUrlpp.innerText = `  ${sup}`;
    txt.innerText = OPN_LNK;
    let lastC = sup.slice(-3)
    if (lastC === '%2b') {
        sc = sup.substr(0, sup.length - 3).toUpperCase();
    } else {
        lastC = sup.slice(-2);
        if (lastC === '++') {
            sc = sup.substr(0, sup.length - 2).toUpperCase();
            getAttrs = true;
        } else {
            lastC = sup.slice(-1);
            if (lastC === '+') {
                sc = sup.substr(0, sup.length - 1).toUpperCase();
                getUDesc = true;
            } else sc = sup.toUpperCase()
        }
    }

    let x = new XMLHttpRequest();
    x.open("GET", `${selectedServer}${customerID}/8/public/select?shortcut=${sc}`, true);
    x.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xst = -1;
    x.onreadystatechange = function () {
        console.log("status", x.status);
        xst = x.readyState;
        if (xst === 4) {
            if (x.status >= 200 && x.status <= 204) {
                let jArr = JSON.parse(x.responseText);
                let jObj = jArr[0];

                dgi("application_home").innerText = APPLICATION_HOMEPAGE;
                shortUrl.innerHTML = `<b>${sc}</b> `;

                if (jObj['count'] === 0) {
                    bid(rdUrl, rdirLabel, txt, sc, 0);
                    shortUrlp.innerText = ``;
                    shortUrlpp.innerText = ``;
                } else {
                    shortUrlp.innerText = `  ${sc}+`;
                    shortUrlpp.innerText = `  ${sc}++`;
                    v = jObj['_VariableLabels'];
                    s = jObj.rows[0].s;
                    blogUserTitle = s[16];
                    blogUserName = s[17];
                    blogUserLastName = s[18];
                    blogUserEmail = s[19];
                    blogTs = s[20];
                    blogUpdateTs = s[21];
                    showAuthor = s[22] === "true";
                    showEmail = s[23] === "true";
                    if (blogUserTitle) dgi("usertitle").innerText = blogUserTitle;
                    else dgi("usertitle").innerText = ``;
                    let subtitle = `<p> ${TEXT_BY1} (${APPLICATION_HOMEPAGE}/${sc}) ${TEXT_BY2}`;
                    if (blogUserName && blogUserLastName && showAuthor === true) subtitle += `${blogUserName} ${blogUserLastName} | `;
                    else subtitle += ` <small><i> ${TEXT_ANON} </i></small> | `
                    if (blogUserEmail && showEmail === true) subtitle += ` <small>[${blogUserEmail.replace('@', ' at ')}]</small> | `;

                    if (blogTs) subtitle += ` <small>${TEXT_CREATED}: ${blogTs}</small> | `;
                    if (blogUpdateTs) subtitle += ` <small>${TEXT_LAST_UPDATED}: ${blogUpdateTs}</small>  `;
                    subtitle += `</p>`;
                    dgi("subtitle").innerHTML = subtitle;
                    let pg = s[2];
                    let dPg = decodeURIComponent(pg);
                    let cat = parseInt(s[4], 10);

                    if (!sc) window.location.href = APPLICATION_HOMEPAGE;
                    if (cat > 1 && cat < 6) {
                        rdUrl.setAttribute("href",
                            `${dPg}`);
                        rdUrl2.setAttribute("href",
                            `${dPg}`);


                        rdUrl.setAttribute("target", "_self");
                        rdUrl2.setAttribute("target", "_self");
                        rdirLabel.innerText = `${dPg}`;


                        if (getUDesc)
                            showUserDesc();
                        else if (getAttrs)
                            showMetaData();
                        else showSomeDesc();

                    } else bid(rdUrl, rdirLabel, txt, sc, cat);
                }
            } else {
                rdirLabel.innerHTML = `<br/>
                          <br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${x.status}&nbsp;&nbsp;`;
                rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
            }
        }
        document.getElementsByTagName("title")[0].innerText = `dovis.it::${sc}`;
    };
    try {
        x.send();

    } catch (err) {
        console.error(err.toString());
        rdirLabel.innerHTML = `<br/><br/>&nbsp;&nbsp;${UNKNOWN_ERR}&nbsp;&nbsp;`;
        rdUrl.setAttribute("href", `${APPLICATION_HOMEPAGE}`);
    }
};

