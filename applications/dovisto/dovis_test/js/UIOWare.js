let uioProject = null;
let sessionUIOModel = null;
let controller = null;
let currentProjectID = 2;
let currentProjectIsSelected = false;
let isLocalProject;
//TODO: set session variable after login
sessionStorage.setItem("sessionUIOUser", "UIOUser");
const invisibleDiv = document.getElementById("rightDiv");
const closePropertyScreenIcon = document.getElementById("closePropertyScreen");
const movePropertyScreenIcon = document.getElementById("movePropertyScreenIcon");
const widgetInputSelection = document.getElementById("inputWidgetSelectionScreen");
const widgetOutputSelection = document.getElementById("outputWidgetSelectionScreen");
const msgScreen = document.getElementById("propertyScreenMessage");
const msgScreenCloseIcon = document.getElementById("closeDeleteScreen");
const inputPlusDiv = document.getElementById("inputPlus");
const outputPlusDiv = document.getElementById("outputPlus");
const editName = document.getElementById("editName");
const editDesc = document.getElementById("editDesc");
const windowIcon = document.getElementById("windowIcon");
/*-----------------Add new widgets in property screen ---------------------*/
//Input-widgets:
const addTextfield = document.getElementById("addTextfield");
const addSelectionList = document.getElementById("addSelectionList");
const addRadioCheckbox = document.getElementById("addRadioCheckbox");
const addLink = document.getElementById("addLink");
const addButton = document.getElementById("addButton");
const addOtherInputWidget = document.getElementById("addOtherInputWidget");

//Output-widgets:
const addTable = document.getElementById("addTable");
const addList = document.getElementById("addList");
const addOutputText = document.getElementById("addOutputText");
const addImages = document.getElementById("addImages");
const addOtherOutput = document.getElementById("addOtherOutput");
/*
document.getElementById("widgetImageSelect").addEventListener('change', function(evt){
    const widgetID=getPropertyScreen().getAttribute("targetobj");
    const widget=controller.getWidgetById(widgetID);
    UIOTool.handleUniqueFileSelect(evt,widget.uioName);
}, false);
*/
/*-----------------Add new widgets in property screen ---------------------*/

const uploadProject = document.getElementById("uploadProject");
const exportHtmlIcon = document.getElementById("exportHtmlIcon");
const addApiEndpoint = document.getElementById("addApiEndpoint");


let resizeWindow = false;
let isCreateWidget = false;

// TODO
/*addApiEndpoint.addEventListener("click", function () {
    const apiData = document.getElementById("apiData");
    const hr = document.createElement("hr");
    const title = document.createElement("h4");
    title.innerText = "New section for api-data";
    const triggerTitle = document.createElement("label");
    triggerTitle.innerText = "Triggering event:";
    const httpMethodTitle = document.createElement("label");
    httpMethodTitle.innerText = "HTTP-Method:";
    const triggerEventSelection = buildEventSelectionList();
    const httpMethodSelection = buildHttpMethodSelectionList();
    apiData.appendChild(hr);
    apiData.appendChild(title);
    apiData.appendChild(triggerTitle);
    apiData.appendChild(triggerEventSelection);
    apiData.appendChild(httpMethodTitle);
    apiData.appendChild(httpMethodSelection);
});*/

uploadProject.addEventListener("click", () => {
    if (uioProject) {
        document.body.style.cursor = "progress";
        try {
            callUioApi("save", currentDovisUserID, uioProject, function (status, response) {
                if (status === 200) {
                    if (response.message && response.filename) UIOTool.Msg(`Project was saved. Filename: ${response.filename} ${response.message}`);
                    else UIOTool.Msg(`Project was saved.`);
                } else {
                    if (response.message && response.filename) UIOTool.errorMsg(`Project could not be saved. Filename: ${response.filename} ${response.message}`);
                    else UIOTool.errorMsg(`Project could not be saved.`);
                }
                document.body.style.cursor = "auto";
            });
        } catch (err) {
            UIOTool.errorMsg(err);
        }
    } //else UIOTool.uioMessage(`Cannot upload: no project was loaded yet.`);
});
const exportHtml = function () {
    if (uioProject) {
        document.body.style.cursor = "progress";
        callUioApi("html", currentDovisUserID, uioProject, function (status, response) {
            let msg = ``;
            if (response.message) msg = response.message;
            if (status === 200)
                UIOTool.Msg(`Html/css/JS were exported. HTTP${status}  ${msg}`);
            else UIOTool.errorMsg(`Html/css/JS could not be exported. HTTP${status} ${msg}`);
            document.body.style.cursor = "auto";
        });
    }
}
exportHtmlIcon.addEventListener("click", exportHtml);

windowIcon.addEventListener("mousedown", () => {
    if (currentProjectIsSelected) {
        document.body.style.cursor = "move";
        isCreateWidget = true;
    }
});
windowIcon.addEventListener("mouseover", () => {
    if (currentProjectIsSelected) {
        windowIcon.style.background = "white";
        windowIcon.style.borderColor = "black";
        windowIcon.style.borderWidth = "2";
    }
});
windowIcon.addEventListener("mouseup", () => {
    if (currentProjectIsSelected) {
        isCreateWidget = false;
        document.body.style.cursor = "auto";
    }
});
windowIcon.addEventListener("mouseout", () => {
    if (currentProjectIsSelected) {
        windowIcon.style.background = "inherit";
        windowIcon.style.borderColor = "black";
        windowIcon.style.borderWidth = "1";
    }
});

/*------------------------------------------------------------------------------* /
//TODO remove in PRODUCTIVE RELEASE:
localStorage.removeItem(Constants.UIOWAREPROJECTS);
localStorage.removeItem(Constants.UIOWARE_CURRENT_PROJECT);
localStorage.removeItem(Constants.UIOWAREPROJECTS + ".1");
localStorage.removeItem(Constants.UIOWAREPROJECTS + ".2");
localStorage.removeItem(Constants.UIOWAREPROJECTS + ".3");
localStorage.removeItem(Constants.UIOWAREPROJECTS + ".undefined");
if (currentProjectID) localStorage.removeItem(Constants.UIOWAREPROJECTS + "." + currentProjectID);
/ *------------------------------------------------------------------------------*/


let currentProjectName = localStorage.getItem(Constants.UIOWARE_CURRENT_PROJECT);
console.log("currentProjectName", currentProjectName);


const removeWidget = function (id) {
    const widget = controller.getWidgetById(id);
    closePropertyScreen(widget.uioName);
    const modelData = {};
    modelData.objectToRemove = id;
    controller.deleteWidget(modelData);
};
const showProjectSettings = function () {
    const html = {};
    html.updateCase = "none";

    html.html = `<div  id="ppiValue">
                <div>
                    <label>ppi:</label>
                    <input id="ppiSlider" max="385"
                           min="32" onchange="updatePpi(this.value)" size="6" step="1" type="number"
                          />
                </div>
               <div class="form-check">
  <input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioDisabled" disabled>
  <label class="form-check-label" for="flexRadioDisabled">
    Relative resizing (%)
  </label>
</div>
<div class="form-check">
  <input class="form-check-input" type="radio" name="flexRadioDisabled" id="flexRadioCheckedDisabled" checked disabled>
  <label class="form-check-label" for="flexRadioCheckedDisabled">
       Absolute resizing (px)
  </label>
</div>
            </div>
              `;
    if (controller) {
        UIOTool.showApiUpdateBox(controller, null, html, "");
        document.getElementById("ppiSlider").setAttribute("value", ppi);
    }
}
const moveMessageScreen = function () {
    invisibleDiv.appendChild(getMessageScreen());
};
const getPropertyScreen = function () {
    return document.getElementById("propertyScreen");
};
const getMessageScreen = function () {
    return document.getElementById("propertyScreenMessage");
};
const closePropertyScreen = function () {
    const pScreen = document.getElementById("propertyScreen");
    invisibleDiv.appendChild(pScreen);
    pScreen.style.visibility = "hidden";
    closeMessage();
    closeWidgetSelection();
    const apiForm = document.getElementById("UIOToolMessageBox");
    if (apiForm) {
        UIOTool.closeReceiverUpdateForm(null);
        apiForm.remove();
    }
    document.getElementById("addSubNodes").style.visibility = "hidden";
    //setAllWidgetsOpacity("0.95");
};

const closeWidgetSelection = function () {
    const closeWidgetSelection = document.getElementById("closeWidgetSelection");
    const inputWidgetSelectionScreen = document.getElementById("inputWidgetSelectionScreen");
    const outputWidgetSelectionScreen = document.getElementById("outputWidgetSelectionScreen");
    if (closeWidgetSelection) closeWidgetSelection.style.visibility = "hidden";
    if (inputWidgetSelectionScreen) inputWidgetSelectionScreen.style.visibility = "hidden";
    if (outputWidgetSelectionScreen) outputWidgetSelectionScreen.style.visibility = "hidden";
};

const showMessage = function (currentElem, msg) {
    //TODO: refactor
    currentElem.appendChild(msgScreen);
    msgScreen.style.visibility = "visible";
    msgScreenCloseIcon.style.visibility = "visible";
    msgScreen.style.left = "100%";
    msgScreen.style.top = "0%";
    const divElement = document.getElementById("messageDiv");
    divElement.style.visibility = "visible";
    divElement.innerHTML = msg;
    msgScreen.focus();


};
const closeMessage = function () {
    msgScreen.style.visibility = "hidden";
    msgScreenCloseIcon.style.visibility = "hidden";
    const mDiv = document.getElementById("messageDiv");
    invisibleDiv.appendChild(msgScreen);
    mDiv.style.visibility = "hidden";
    const apiForm = document.getElementById("UIOToolMessageBox");
    if (apiForm) apiForm.remove();
    //setAllWidgetsOpacity("0.95")
};
const showProperties = function (widgetID) {
    const apiForm = document.getElementById("UIOToolMessageBox");
    if (apiForm) {
        UIOTool.closeReceiverUpdateForm(null);
        apiForm.remove();
    }
    document.getElementById("propertyScreen").style.visibility = "visible";
    const widget = controller.getWidgetById(widgetID);
    const pencil = document.createElement("i");
    const tmp_editName = document.getElementById(`tmp_${editName.getAttribute("id")}`);
    const tmp_editDesc = document.getElementById(`tmp_${editDesc.getAttribute("id")}`);
    if (tmp_editName || tmp_editDesc) {
        const msg = `<label style="font-size:0.45em;">   Please SAVE the data !</label><br>&nbsp;<i class="bi bi-exclamation-triangle" style="font-size: 1em;position:absolute;left:25px;"></i>`;
        showMessage(document.getElementById("closePropertyScreen"), msg);

        resizeDiv(getMessageScreen(), 240, 70, "silver");
        return;
    }


    document.getElementById("addSubNodes").style.visibility = "visible";
    pencil.setAttribute("class", "bi bi-pencil-square");
    if (widget) {
        let objectLabel = document.getElementById(`objectLabel`);
        let objectID = document.getElementById(`objectID`);
        objectID.innerText = `${widgetID}`;
        let objectType = document.getElementById(`activeNodes.${widget.uioName}.widgetType`);
        if (!objectType) {
            objectType = document.createElement("b");
            objectLabel.innerHTML = ``;
            objectLabel.appendChild(objectType);
            objectType.setAttribute("id", `activeNodes.${widget.uioName}.widgetType`)
        }
        objectType.innerText = `  ${widget.widgetType}  `;
        if (widgetID > 102) {
            objectType.setAttribute("data-uioaction", `updateWidget`)
            objectType.setAttribute("data-uioid", widgetID)
        }
        // else{
        //     objectLabel.innerHTML = `<b>UIO-type: ${widget.widgetType} </b> (id: ${widget.id})`;
        // }
        if (tmp_editName) tmp_editName.value = widget.uioLabel;
        if (tmp_editDesc) tmp_editDesc.value = widget.description;
        editName.innerHTML = pencil.outerHTML + widget.uioLabel;
        editDesc.innerText = widget.description;
        getPropertyScreen().setAttribute("targetObj", widget.id);
        //setAllWidgetsOpacity("0.95");
        getPropertyScreen().setAttribute("class", "border border-light rounded");
        resizeDiv(getPropertyScreen(), 600, 600, "lightsteelblue");
        document.getElementById(widget.uioName).appendChild(getPropertyScreen());
        getPropertyScreen().style.left = "-10px;";
        getPropertyScreen().style.top = "-10px";
        getPropertyScreen().style.resize = "both";
        //setAllWidgetsOpacity("0.25", widget.id);
        try {
            listApiCalls(widget);
        } catch (err) {
            UIOTool.errorMsg(err);
        }
    } else {
        UIOTool.errorMsg(`No widget with id ${widgetID}`);
        console.error(`No widget with id ${widgetID}`);
    }
};

const refreshPropertyScreen = function (widgetID) {
    const widget = controller.getWidgetById(widgetID);
    listApiCalls(widget);
};

const listApiCalls = function (widget) {

    const apiSectionDiv = document.getElementById("apiData");
    apiSectionDiv.innerHTML = ``;
    const apiCallsCount = widget.apiCalls.length;
    const inDataIcon = document.createElement("i");
    inDataIcon.setAttribute("class", "bi bi-box-arrow-in-down-right");
    apiSectionDiv.appendChild(inDataIcon);
    let inData;
    try {
        inData = controller.getIncomingData(widget);
        console.log("inData:", inData);
    } catch (err) {
        UIOTool.errorMsg(`Could not detect incoming data for widget ${widget.uioLabel} (id  ${widget.id})`)
    }
    if (inData && inData.length > 0) {
        const inDiv = document.createElement("div");
        inDiv.setAttribute("class", "border border-dark rounded");
        inDataIcon.innerText = "Incoming Data:";
        const incomingDataHeader = ["#", "name of parameter", "type", "sender-ID", "sender name", "page"];
        const inComingDataTable = UIOTool.createTable(incomingDataHeader, inData, function (inData) {
            const tbody = document.createElement("tbody");
            let count = 0;
            for (let i = 0; i < inData.length; i++) {
                const tr = document.createElement("tr");
                let parameterName = "?", parameterType = "?", page = "?", pageDescription = "?",
                    labelOfSenderWidget = "?",
                    senderID = "?";
                try {
                    page = inData[i].senderPage;
                    if (inData[i].senderpageDescription.length > 20) pageDescription = `${inData[i].senderpageDescription.substr(0, 17)}...`;
                    else pageDescription = inData[i].senderpageDescription
                    parameterName = inData[i].name;
                    parameterType = inData[i].type;
                    if (inData[i].senderLabel.length > 30) labelOfSenderWidget = `${inData[i].senderLabel.substr(0, 27)}...`;
                    else labelOfSenderWidget = inData[i].senderLabel;
                    senderID = inData[i].senderID;
                } catch (err) {
                    console.warn(err);
                }
                if (parameterName) {
                    count++;
                    tr.appendChild(UIOTool.createCell(null, count, false, null));
                    tr.appendChild(UIOTool.createCell(null, parameterName, false, null));
                    tr.appendChild(UIOTool.createCell(null, parameterType, false, null));
                    tr.appendChild(UIOTool.createCell(null, senderID, false, null));
                    tr.appendChild(UIOTool.createCell(null, labelOfSenderWidget, false, null));
                    tr.appendChild(UIOTool.createCell(null, `${page}-${pageDescription}`, false, null));
                    tbody.appendChild(tr);
                }
            }
            return tbody;
        });
        inDiv.appendChild(inComingDataTable);
        apiSectionDiv.appendChild(inDiv);
    } else if (inData && inData.length === 0) {
        inDataIcon.innerText = "This widget has no incoming Data";
        const hr = document.createElement("hr");
        apiSectionDiv.appendChild(hr);
    } else if (!inData) {
        inDataIcon.innerText = "--- ERROR while trying to detect incoming Data for this widget ---";
        const hr = document.createElement("hr");
        apiSectionDiv.appendChild(hr);
    }
    if (apiCallsCount === 0 || (apiCallsCount === 1 && widget.apiCalls[0].eventName === null && widget.apiCalls[0].method === null)) {
        const labelNoData = document.createElement("label");
        labelNoData.innerText = "No requirements were specified for this widget.";
        apiSectionDiv.appendChild(labelNoData);
    } else {
        const uioName = widget.uioName;
        for (let i = 0; i < apiCallsCount; i++) {
            const apiDiv = document.createElement("div");
            apiDiv.setAttribute("class", "border border-dark rounded");
            const epHeader = ["Specification", "Trigger", "HTTP"];
            const epData = widget.apiCalls[i];
            const specificationTable = UIOTool.createTable(epHeader, epData, function (data) {
                const tbody = document.createElement("tbody");
                const trStatus = document.createElement("tr");
                trStatus.appendChild(UIOTool.createCell(widget.id, `Status: ${data.status}`, true, `activeNodes.${uioName}.apiCalls[${i}].status`));
                tbody.appendChild(trStatus);
                const tr = document.createElement("tr");
                tr.appendChild(UIOTool.createCell(widget.id, `Frontend-info: ${widget.apiCalls[i].frontendRequirement}`, true, `activeNodes.${uioName}.apiCalls[${i}].frontendRequirement`));
                tr.appendChild(UIOTool.createCell(widget.id, data.eventName, true, `activeNodes.${uioName}.apiCalls[${i}].eventName`));
                tr.appendChild(UIOTool.createCell(widget.id, data.method, true, `activeNodes.${uioName}.apiCalls[${i}].method`));
                let idx = ``;
                if (i > 0) idx = `.${i}`;
                const uioCall = `uio${currentProjectID}${idx}.${widget.uioLabel.toLocaleLowerCase().replace(/[^a-z0123456789]/g, "")}`;
                tbody.appendChild(tr);
                const tr2 = document.createElement("tr");
                tr2.appendChild(UIOTool.createCell(widget.id, `Backend-info: ${widget.apiCalls[i].backendRequirement}`, true, `activeNodes.${uioName}.apiCalls[${i}].backendRequirement`));
                tr2.appendChild(UIOTool.createCell(null, `Proposed name for endpoint-url:  /${uioCall}`, false, null));
                tr2.appendChild(UIOTool.createCell(null, `Operation: ${currentProjectID}.${widget.id}${idx}`, false, null));
                tbody.appendChild(tr2);
                return tbody;
            });
            apiDiv.appendChild(specificationTable);

            const addEmptyCells = function (row, number) {
                for (let n = 0; n < number; n++) {
                    row.appendChild(UIOTool.createCell(null, null, false, null));
                }
            }

            const apiParameterArray = widget.apiCalls[i].apiParameters;
            const apiParameterHeader = ["", "#", "name of parameter", "type of parameter", `Send to`];
            const outDataIcon = document.createElement("i");
            outDataIcon.setAttribute("class", "bi bi-box-arrow-up-right");
            outDataIcon.innerText = "Outgoing Data:";
            apiSectionDiv.appendChild(outDataIcon);
            const parametersTable = UIOTool.createTable(apiParameterHeader, apiParameterArray, function (data) {
                const tbody = document.createElement("tbody");
                for (let j = 0; j < apiParameterArray.length; j++) {
                    let tr = document.createElement("tr");
                    const tdDelete = document.createElement("td");
                    const deleteIcon = document.createElement("i");
                    deleteIcon.setAttribute("class", "bi bi-dash-circle-dotted");
                    deleteIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                    deleteIcon.setAttribute("data-uioid", widget.id);
                    deleteIcon.innerText = ` Delete
                                             parameter #${j + 1}`;
                    deleteIcon.setAttribute("id", `deleteParameter-activeNodes.${uioName}.apiCalls[${i}].apiParameters[${j}]`);
                    tdDelete.appendChild(deleteIcon);
                    tr.appendChild(tdDelete);
                    tr.appendChild(UIOTool.createCell(widget.id, j + 1, false, null));
                    const paramNameIcon = document.createElement("i");
                    const paramTypeIcon = document.createElement("i");
                    paramNameIcon.innerText = data[j].name;
                    paramTypeIcon.innerText = data[j].type;
                    paramNameIcon.setAttribute("class", "bi bi-gear-fill");
                    paramTypeIcon.setAttribute("class", "bi bi-gear-fill");
                    paramNameIcon.setAttribute("id", `activeNodes.${uioName}.apiCalls[${i}].apiParameters[${j}].name`);
                    paramNameIcon.setAttribute("data-uioid", widget.id);
                    paramNameIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                    paramTypeIcon.setAttribute("id", `activeNodes.${uioName}.apiCalls[${i}].apiParameters[${j}].type`);
                    paramTypeIcon.setAttribute("data-uioid", widget.id);
                    paramTypeIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);

                    const tdName = document.createElement("td");
                    const tdType = document.createElement("td");
                    tdName.appendChild(paramNameIcon);
                    tdType.appendChild(paramTypeIcon);
                    tr.appendChild(tdName);
                    tr.appendChild(tdType);
                    const receivers = data[j].receiverWidgets;
                    if (!receivers) {
                        UIOTool.errorMsg(`ERROR: Widget ${widget.uioLabel}, (id: ${widget.id}) does not have a receivers-list`);
                    } else {
                        for (let l = 0; l < receivers.length; l++) {
                            const receiverWidget = controller.getWidgetById(receivers[l]);
                            let widgetLabel = "-", page = "-", receiver = "-";
                            if (receiverWidget) {
                                widgetLabel = UIOTool.shortString(receiverWidget.uioLabel, 15);
                                const widgetID = receiverWidget.id;
                                page = controller.getWidgetPage(receiverWidget).uioName;
                                receiver = `page: ${page}, widget: ${widgetLabel} (id: ${widgetID})`;
                            } else console.warn(`Invalid  widget-id for receiver of parameter '${data[j].name}': receivers[${l}]=${receivers[l]} in widget ${widget.id}`);
                            if (l > 0) {
                                tr = document.createElement("tr");
                                addEmptyCells(tr, 4);
                            }
                            tr.appendChild(UIOTool.createCell(widget.id, receiver, true, `activeNodes.${uioName}.apiCalls[${i}].apiParameters[${j}].receiverWidgets[${l}]`));
                            tbody.appendChild(tr);
                            const deleteReceiverIcon = document.createElement("i");
                            deleteReceiverIcon.setAttribute("class", "bi bi-dash-circle-dotted");
                            deleteReceiverIcon.setAttribute("id", `deleteReceiver-activeNodes.${uioName}.apiCalls[${i}].apiParameters[${j}].receiverWidgets[${l}]`);
                            deleteReceiverIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                            deleteReceiverIcon.setAttribute("data-uioid", widget.id);
                            const td = document.createElement("td");
                            td.appendChild(deleteReceiverIcon);
                            deleteReceiverIcon.innerText = `Delete receiver #${l + 1}`;
                            tr.appendChild(td);
                        }
                    }
                    tbody.appendChild(tr);
                    tr = document.createElement("tr");
                    addEmptyCells(tr, 4);
                    const addReceiverIcon = document.createElement("i");
                    addReceiverIcon.setAttribute("class", "bi bi-plus-circle-dotted ");
                    addReceiverIcon.setAttribute("id", `activeNodes.${uioName}.apiCalls[${i}].apiParameters[${j}].receiverWidgets[${receivers.length}]`);
                    addReceiverIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                    addReceiverIcon.setAttribute("data-uioid", widget.id);
                    const td = document.createElement("td");
                    td.appendChild(addReceiverIcon);
                    addReceiverIcon.innerText = "Add new receiver";
                    tr.appendChild(td);
                    tbody.appendChild(tr);
                }
                const addParam = document.createElement("i");
                addParam.setAttribute("class", "bi bi-plus-circle-dotted");
                addParam.setAttribute("id", `activeNodes.${uioName}.apiCalls[${i}].apiParameters[${apiParameterArray.length}]`);
                addParam.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                addParam.setAttribute("data-uioid", widget.id);
                addParam.innerText = ` Add new parameter`;
                const tr = document.createElement("tr");
                const td = document.createElement("td");
                const td2 = document.createElement("td");
                const td3 = document.createElement("td");
                td3.appendChild(addParam)
                tr.appendChild(td);
                tr.appendChild(td2);
                tr.appendChild(td3);
                tbody.appendChild(tr);
                return tbody;
            });
            apiDiv.appendChild(parametersTable);

            const apiDMLArray = widget.apiCalls[i].apiDMLs;
            const apiDMLHeader = ["#", "DML", ""];
            const statementsTable = UIOTool.createTable(apiDMLHeader, apiDMLArray, function (data) {

                const tbody = document.createElement("tbody");

                for (let k = 0; k < apiDMLArray.length; k++) {
                    const tr = document.createElement("tr");
                    tr.appendChild(UIOTool.createCell(widget.id, k + 1, false, null));

                    tr.appendChild(UIOTool.createCell(widget.id, data[k], true, `activeNodes.${uioName}.apiCalls[${i}].apiDMLs[${k}]`));
                    const tdDelete = document.createElement("td");
                    const deleteIcon = document.createElement("i");
                    deleteIcon.setAttribute("class", "bi bi-dash-circle-dotted");
                    deleteIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                    deleteIcon.setAttribute("data-uioid", widget.id);
                    deleteIcon.setAttribute("id", `deleteDML-activeNodes.${uioName}.apiCalls[${i}].apiDMLs[${k}]`);
                    deleteIcon.innerText = ` Delete statement`;
                    tdDelete.appendChild(deleteIcon);
                    tr.appendChild(tdDelete);

                    tbody.appendChild(tr);
                }
                const addDML = document.createElement("i");
                addDML.setAttribute("class", "bi bi-plus-circle-dotted ");
                addDML.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
                addDML.setAttribute("id", `activeNodes.${uioName}.apiCalls[${i}].apiDMLs[${apiDMLArray.length}]`);
                addDML.setAttribute("data-uiokeypath", `activeNodes.${uioName}.apiCalls[${i}].apiDMLs[${apiDMLArray.length}]`);
                addDML.setAttribute("data-uioid", widget.id);
                addDML.innerText = "Add new statement";
                const tr = document.createElement("tr");
                tr.appendChild(addDML);
                tbody.appendChild(tr);
                return tbody;
            });

            apiDiv.appendChild(statementsTable);
            const addSpecification = document.createElement("i");
            addSpecification.setAttribute("class", "bi bi-plus-circle-dotted");
            addSpecification.setAttribute("id", `activeNodes.${uioName}.apiCalls`);
            addSpecification.innerText = "Add Specification";
            //apiDiv.appendChild(addSpecification);
            apiSectionDiv.appendChild(apiDiv);
            apiSectionDiv.appendChild(addSpecification);
        }

    }
}
const editTextField = function (elementToBeChanged, tmpEditElementType, callBack) {
    const oldText = elementToBeChanged.innerText;
    const newParent = elementToBeChanged.parentElement;
    const parkerDiv = document.getElementById("rightDiv");
    const tmpEditElement = document.createElement(tmpEditElementType);
    tmpEditElement.setAttribute("class", "form-control");
    tmpEditElement.value = oldText;
    tmpEditElement.setAttribute("id", `tmp_${elementToBeChanged.getAttribute("id")}`);

    newParent.insertBefore(tmpEditElement, tmpEditElement.firstChild);
    parkerDiv.insertBefore(elementToBeChanged, parkerDiv.lastChild);
    const saveIcon = document.createElement("i");
    saveIcon.setAttribute("class", "bi bi-check-circle-fill");
    newParent.insertBefore(saveIcon, tmpEditElement.firstChild);
    tmpEditElement.value = elementToBeChanged.innerText;
    elementToBeChanged.style.visibility = "hidden";

    saveIcon.addEventListener("click", function () {
        msgScreen.style.visibility = "hidden";
        elementToBeChanged.innerText = ` ${tmpEditElement.value}`;
        elementToBeChanged.setAttribute("style", "font-size: 0.85em;");
        tmpEditElement.remove();
        saveIcon.remove();
        newParent.insertBefore(elementToBeChanged, newParent.firstChild);
        const editIcon = document.createElement("i");
        editIcon.setAttribute("class", "bi bi-pencil-square");
        elementToBeChanged.insertBefore(editIcon, elementToBeChanged.firstChild);
        callBack();
        closeMessage();
    })
};
/**------------------------- START  Listeners for adding widgets -------------------------- **/
//Input-widgets:
const randomPos = function () {
    const pos = {};
    pos.x = Math.round(Math.random() * 25) + 15;
    pos.y = Math.round(Math.random() * 25) + 15;
    return pos;
}

addTextfield.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareMockTextField, 150, 25, randomPos().x, randomPos().y);
});
addSelectionList.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareMockSelectionList, 150, 60, randomPos().x, randomPos().y);
});
addRadioCheckbox.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareMockRadioCheckBox, 35, 35, randomPos().x, randomPos().y);
});
addLink.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareMockLink, 110, 25, randomPos().x, randomPos().y);
});
addButton.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareMockButton, 80, 25, randomPos().x, randomPos().y);
});
addOtherInputWidget.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareInput, 150, 150, randomPos().x, randomPos().y);
});
//Output-Widgets:
addTable.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareTable, 150, 150, randomPos().x, randomPos().y);
});
addList.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareMockSelectionList, 120, 120, randomPos().x, randomPos().y);
});
addOutputText.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareTextChapter, 120, 120, randomPos().x, randomPos().y);
});
addImages.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareImage, 120, 120, randomPos().x, randomPos().y);
});
addOtherOutput.addEventListener("click", () => {
    document.body.style.cursor = "auto";
    const refId = Number(getPropertyScreen().getAttribute("targetObj")).valueOf();
    controller.createNewWidget(refId, UIOWareWidget.UIOWareOutput, 150, 150, randomPos().x, randomPos().y);
});

/**-------------------------END  Listeners for adding widgets -------------------------- **/

editName.addEventListener("click", function () {
    editTextField(editName, "input", function () {
        const updatedObject1 = {};
        const refId = getPropertyScreen().getAttribute("targetObj");
        const widget = controller.getWidgetById(refId);
        const widgetName = widget.uioName;
        if (controller.widgetLabelExists(editName.innerText)) {
            UIOTool.errorMsg(`The name '${editName.innerText}' is already in use!`);
            editName.innerText = widget.uioLabel;
        } else {
            updatedObject1.key = `activeNodes.${widgetName}.uioLabel`;
            updatedObject1.value = editName.innerText.replace(/[^A-Za-z0123456789\.\\/\- ]/g, "");
            updatedObject1.value = updatedObject1.value.replace(/[  ]/g, " ").trim();
            const changedProperties = [];
            changedProperties.push(updatedObject1);
            controller.updateWidget(refId, changedProperties);
        }
    });
});
editDesc.addEventListener("click", function () {
    editTextField(editDesc, "textarea", function () {
        const updatedObject = {};
        const refId = getPropertyScreen().getAttribute("targetObj");
        const widgetName = controller.getWidgetById(refId).uioName;
        updatedObject.key = `activeNodes.${widgetName}.description`;
        updatedObject.value = editDesc.innerText;
        const changedProperties = [];
        changedProperties.push(updatedObject);
        controller.updateWidget(refId, changedProperties);
    });

});

const resizeDiv = function (elem, width, height, divColor) {
    let bgColor;
    if (!divColor) bgColor = "inherit";
    else bgColor = divColor;
    elem.style.backgroundColor = bgColor;
    if (width === 1 || height === 1) elem.style.visibility = 'hidden';
    else elem.style.visibility = 'visible';
    let id = null;
    let pos = 0;
    clearInterval(id);
    id = setInterval(frame, 5);

    function frame() {
        if (pos > width && pos > height) {
            clearInterval(id);
        } else {
            pos += 15;
            if (pos < width) elem.style.width = pos + 'px';
            if (pos < height) elem.style.height = pos + 'px';
        }
    }
};

// const setAllWidgetsOpacity = function (numberString, excludedId) {
//     if (controller) {
//         const list = controller.getPages();
//         for (let i = 0; i < list.length; i++) {
//             const widget = document.getElementById(`widget${list[i]}`);
//             if (excludedId && widget) {
//                 if (list[i] !== excludedId) widget.style.opacity = numberString;
//             } else if (widget) widget.style.opacity = numberString;
//         }
//     } else console.log("No controller available to set widgets' opacity");
// };
closePropertyScreenIcon.addEventListener("click", function () {
    const addSubNodes = document.getElementById("addSubNodes");
    if (addSubNodes.parentElement !== getPropertyScreen())
        getPropertyScreen().insertBefore(addSubNodes, document.getElementById("propertyScreenForm"));
    getPropertyScreen().style.visibility = "hidden";
    closeMessage();
    closeWidgetSelection();

    document.getElementById("addSubNodes").style.visibility = "hidden";
    //setAllWidgetsOpacity("0.95");
});
movePropertyScreenIcon.addEventListener("click", () => {
    invisibleDiv.appendChild(document.getElementById("propertyScreen"));
});
widgetOutputSelection.addEventListener("click", function () {
    console.log("Click outputWidgetSelectionScreen");
    console.log("target Object:" + this.getAttribute("targetObj"));
});
inputPlusDiv.addEventListener("click", () => {
    document.getElementById("closeWidgetSelection").style.visibility = "visible";

    widgetOutputSelection.style.visibility = "hidden";
    resizeDiv(widgetInputSelection, 250, 70, "#75b4da");
    inputPlusDiv.appendChild(widgetInputSelection);
});
outputPlusDiv.addEventListener("click", () => {
    document.getElementById("closeWidgetSelection").style.visibility = "visible";
    widgetInputSelection.style.visibility = "hidden";

    resizeDiv(widgetOutputSelection, 200, 70, "#75b4da");
    outputPlusDiv.appendChild(widgetOutputSelection);
});

const openProjects = function () {
    console.log("open projects:");
    const projects = localStorage.getItem(Constants.UIOWAREPROJECTS);
    console.log("projects", projects)
    let projectsHTML, icon;
    icon = document.createElement("i");
    icon.setAttribute("class", "bi bi-kanban");
    icon.setAttribute("style", "font-size: 2em");
    if (!projects) {
        projectsHTML = document.createElement("p");
        projectsHTML.innerHTML = `<br>No projects found. Please add a new one.`;
    } else {
        const projectList = JSON.parse(projects);
        console.log(" -- projectList", projectList);
        const projectIds = projectList['projectIDs'];
        const projectNames = projectList['projectNames'];
        let li = `<optgroup label="Local projects">
                  <option selected disabled>Select project</option>`;
        const localProjects = [];
        for (let i = 0; i < projectIds.length; i++) {
            const idx = projectIds[i];
            const name = projectNames[i];
            // local projects must have negative indices
            // to be distinguished from server-projects
            localProjects.push(`<option value=${-1 * idx}>${name}</option>`);
        }
        const filteredLocal = localProjects.filter(UIOTool.onlyUnique);
        for (let i = 0; i < filteredLocal.length; i++) {
            li += filteredLocal[i];
        }
        li += `</optgroup>`;
        projectsHTML = document.createElement("select");
        projectsHTML.setAttribute("id", "selectProject");
        projectsHTML.setAttribute("class", "form-select");
        projectsHTML.setAttribute("onselect", "selectProject()");
        projectsHTML.setAttribute("onchange", "selectProject()");
        callUioApi("list", currentDovisUserID, uioProject, function (status, response) {
            if (status === 200) {
                const projectNamesObject = response['uioProjectNames'];
                console.info("projectNamesObject from server", projectNamesObject);
                const firstIdx = projectNamesObject['firstProjectId'];
                const lastIdx = projectNamesObject['latestProjectId'];
                li += `<optgroup label="Public projects">`;
                const liArray = [];
                for (let p = firstIdx; p <= lastIdx; p++) {
                    if (projectNamesObject[p]) liArray.push(projectNamesObject[p]);
                }
                console.log("before  filter", liArray)
                const filtered = liArray.filter(UIOTool.onlyUnique);
                console.log("after filter", filtered)
                for (let i = 0; i < filtered.length; i++)
                    if (filtered[i])
                        li += `<option value="${filtered[i]}">${filtered[i]}</option>`;
                li += `</optgroup>`;
                document.getElementById("selectProject").innerHTML = li;
            } else UIOTool.errorMsg(`Could not load projects from server: HTTP${status}`);
            return status;
        });
    }

    resizeDiv(getMessageScreen(), 300, 300, "silver");
    showMessage(invisibleDiv, icon.outerHTML + projectsHTML.outerHTML);

};


const searchProjectName = function (projectName, projectList) {
    let idx = 0;
    for (let i = 0; i < projectList.length; i++) {
        let isEqual = projectName.toLowerCase().valueOf().trim() === projectList[i].toLowerCase().valueOf().trim();
        if (isEqual) {
            idx = i + 1;
            break;
        }
    }
    return idx;
};
const searchProjectIndex = function (projectIndex, projectList) {
    let idx = 0;
    for (let i = 0; i < projectList.length; i++) {
        let isEqual = projectIndex === projectList[i];
        if (isEqual) {
            idx = i + 1;
            break;
        }
    }
    return idx;
};

const addProject = function (idx) {
    let sessionUIOProjectName, sessionUIOProjectID, sessionUIOProjectCodeName,
        sessionUIOProjectDescription,
        sessionUIOProjectGitUri;
    if (idx === 0) {
        const headerText = document.createElement("h3");
        const labelProjectID = document.createElement("label");
        const labelProjectName = document.createElement("label");
        const labelProjectCodeName = document.createElement("label");
        const labelProjectDescription = document.createElement("label");
        const labelProjectGitUrl = document.createElement("label");
        const labelAdd = document.createElement("label");
        const fieldProjectID = document.createElement("input");
        const fieldProjectName = document.createElement("input");
        const fieldProjectCodeName = document.createElement("input");
        const fieldProjectDescription = document.createElement("textarea");
        const fieldProjectGitUrl = document.createElement("input");
        const save = document.createElement("i");
        const cr1 = document.createElement("br");
        const cr2 = document.createElement("br");
        const cr3 = document.createElement("br");
        labelProjectID.innerText = `ID of project: `;
        labelProjectName.innerText = `Name of project: `;
        labelProjectCodeName.innerText = `Codename of project: `;
        labelProjectDescription.innerText = `Description of project: `;
        labelProjectGitUrl.innerText = `Git Url of project: `;
        labelAdd.innerText = `Add project`;
        save.setAttribute("class", "bi bi-folder-plus");
        save.style.fontSize = "1.55em";


        labelProjectID.setAttribute("id", "labelProjectID");
        //fieldProjectID.setAttribute("class", "form-control");
        fieldProjectID.setAttribute("class", "border border-light rounded");
        fieldProjectID.setAttribute("required", "true");
        fieldProjectID.setAttribute("type", "number");
        fieldProjectID.setAttribute("size", "5");


        fieldProjectName.setAttribute("class", "form-control");
        fieldProjectCodeName.setAttribute("class", "form-control");
        fieldProjectDescription.setAttribute("class", "form-control");
        fieldProjectGitUrl.setAttribute("class", "form-control");
        fieldProjectName.setAttribute("required", "true");

        fieldProjectID.setAttribute("id", "newProjectID");
        fieldProjectName.setAttribute("id", "newProjectName");
        fieldProjectCodeName.setAttribute("id", "newProjectCodeNameField");
        fieldProjectDescription.setAttribute("id", "newProjectDescriptionField");
        fieldProjectGitUrl.setAttribute("id", "newProjectGitUriField");


        fieldProjectName.setAttribute("required", "true");

        fieldProjectName.placeholder = `Set new project-name`;
        fieldProjectCodeName.placeholder = `Codename of the new project`;
        fieldProjectDescription.placeholder = `Set Description of this new project`;
        fieldProjectGitUrl.placeholder = `https://`;

        const newProjectForm = document.createElement("div");

        headerText.innerText = `Add new Project:`;
        const icon = document.createElement("i");
        icon.setAttribute("class", "bi bi-kanban");
        icon.appendChild(headerText);
        newProjectForm.appendChild(icon);
        newProjectForm.appendChild(labelProjectID);
        newProjectForm.appendChild(cr1);
        newProjectForm.appendChild(fieldProjectID);
        newProjectForm.appendChild(cr2);

        newProjectForm.appendChild(labelProjectName);
        newProjectForm.appendChild(fieldProjectName);
        newProjectForm.appendChild(cr3);
        newProjectForm.appendChild(labelProjectCodeName);
        newProjectForm.appendChild(fieldProjectCodeName);

        newProjectForm.appendChild(labelProjectDescription);
        newProjectForm.appendChild(fieldProjectDescription);

        newProjectForm.appendChild(labelProjectGitUrl);
        newProjectForm.appendChild(fieldProjectGitUrl);
        /*-----------------------------------------------------------------*/
        save.setAttribute("onclick", `addProject(1)`);
        /*-----------------------------------------------------------------*/

        save.appendChild(labelAdd);
        newProjectForm.appendChild(save);
        resizeDiv(getMessageScreen(), 350, 500, "silver");
        const html = newProjectForm.outerHTML;
        showMessage(invisibleDiv, html);
    } else {
        const newProjectNameField = document.getElementById("newProjectName");
        const newProjectIDField = document.getElementById("newProjectID");
        const labelProjectID = document.getElementById("labelProjectID");
        const newProjectCodeNameField = document.getElementById("newProjectCodeNameField");
        const newProjectDescriptionField = document.getElementById("newProjectDescriptionField");
        const newProjectGitUriField = document.getElementById("newProjectGitUriField");
        labelProjectID.innerText = "Project-ID:";
        sessionUIOProjectName = newProjectNameField.value;
        sessionUIOProjectCodeName = newProjectCodeNameField.value;
        sessionUIOProjectDescription = newProjectDescriptionField.value;
        sessionUIOProjectGitUri = newProjectGitUriField.value;
        sessionUIOProjectID = newProjectIDField.value;
        newProjectNameField.style.borderColor = "inherit";
        newProjectNameField.style.backgroundColor = "white";
        newProjectNameField.style.color = "black";
        const newProjectID = parseInt(newProjectIDField.value);
        const newProjectFloatID = parseFloat(newProjectIDField.value);

        let storedProjects = JSON.parse(localStorage.getItem(Constants.UIOWAREPROJECTS));
        let projectIDExists = false;
        let projectNameExists = false;
        if (storedProjects !== null) {
            projectIDExists = searchProjectIndex(sessionUIOProjectID, storedProjects['projectIDs']) > 0;
            projectNameExists = searchProjectName(sessionUIOProjectName, storedProjects['projectNames']) > 0;
        }
        const unallowedProjectName = sessionUIOProjectName.trim().toLowerCase() === "new";
        const badProjectName = !sessionUIOProjectName || projectNameExists || unallowedProjectName;
        const badProjectFormat = newProjectID <= 0 || newProjectFloatID - newProjectID !== 0;
        const badProjectID = !sessionUIOProjectID || badProjectFormat || projectIDExists;


        console.log("badProjectID", badProjectID, "id:", newProjectID, "is integer:", newProjectFloatID - newProjectID === 0, "floatValue:", newProjectFloatID, "badProjectFormat:", badProjectFormat);
        if (!sessionUIOProjectID || !sessionUIOProjectName || badProjectID || projectIDExists || projectNameExists) {
            if (badProjectName) {
                newProjectNameField.style.borderColor = "red";
                newProjectNameField.style.backgroundColor = "orange";
                newProjectNameField.style.color = "black";

                if (!sessionUIOProjectName) {
                    newProjectNameField.placeholder = "Please assign a project name";
                } else if (unallowedProjectName) {
                    newProjectNameField.placeholder = "Unallowed project name";
                } else if (projectNameExists) {
                    newProjectNameField.placeholder = "Project name already exists!";
                }
                newProjectNameField.value = null;
            } else {
                newProjectNameField.style.borderColor = "black";
                newProjectNameField.style.backgroundColor = "white";
                newProjectNameField.style.color = "black";
            }
            if (badProjectID) {
                newProjectIDField.style.borderColor = "red";
                newProjectIDField.style.backgroundColor = "orange";
                newProjectIDField.style.color = "black";
                if (badProjectFormat) {
                    newProjectIDField.placeholder = "Project-ID must be a positive integer";
                    labelProjectID.innerText = "Project-ID: (must be an integer > 0)";
                } else if (!sessionUIOProjectID) {
                    newProjectIDField.placeholder = "Please assign a project-ID";
                    labelProjectID.innerText = "Please assign a project-ID";
                } else if (projectIDExists) {
                    newProjectIDField.placeholder = "This project-ID is already in use.";
                    labelProjectID.innerText = `Project-ID ${sessionUIOProjectID} is already in use.`;
                }
                newProjectIDField.value = null;
            } else {
                newProjectIDField.style.borderColor = "black";
                newProjectIDField.style.backgroundColor = "white";
                newProjectIDField.style.color = "black";
            }


        } else {

            console.log("Creating new project...");
            console.log("!(Number.isInteger(currentProjectIDField.value)) || Number(currentProjectIDField.value).valueOf() < 0", !(Number.isInteger(newProjectIDField.value)) || Number(newProjectIDField.value).valueOf() < 0);
            sessionStorage.setItem("sessionUIOProjectName", sessionUIOProjectName);
            sessionStorage.setItem("sessionUIOProjectCodeName", sessionUIOProjectCodeName);
            sessionStorage.setItem("sessionUIOProjectDescription", sessionUIOProjectDescription);
            sessionStorage.setItem("sessionUIOProjectGitUri", sessionUIOProjectGitUri);
            const newProjectID = newProjectIDField.value;

            if (storedProjects === null) {
                const projectList = {};
                projectList.projectIDs = [];
                projectList.projectNames = [];
                localStorage.setItem(Constants.UIOWAREPROJECTS, JSON.stringify(projectList));
                storedProjects = JSON.parse(localStorage.getItem(Constants.UIOWAREPROJECTS));
            }
            storedProjects['projectIDs'].push(newProjectID);
            storedProjects['projectNames'].push(sessionUIOProjectName);
            localStorage.setItem(Constants.UIOWARE_CURRENT_PROJECT, sessionUIOProjectName);
            localStorage.setItem(Constants.UIOWAREPROJECTS, JSON.stringify(storedProjects));
            localStorage.setItem(`${Constants.UIOWAREPROJECTS}.${newProjectID}`, "NEW");

            buildProject(newProjectID);
        }
    }
};

const selectProject = function () {
    const availableProjects = JSON.parse(localStorage.getItem(Constants.UIOWAREPROJECTS));
    console.log("availableProjects ", availableProjects);
    const projectSelector = document.getElementById("selectProject");
    const selectedIndex = projectSelector.value;
    // index<0: get Data from local storage
    if (selectedIndex < 0) {
        isLocalProject = true;
        const selectedProject = -1 * selectedIndex;
        console.log("selectedProject", selectedProject);

        const projectIdx = searchProjectIndex(selectedProject, availableProjects['projectIDs']) - 1;
        console.log("projectIndex", projectIdx);

        currentProjectName = JSON.parse(localStorage.getItem(Constants.UIOWAREPROJECTS))['projectNames'][projectIdx];
        console.log("currentProjectName ", currentProjectName);
        localStorage.setItem(Constants.UIOWARE_CURRENT_PROJECT, currentProjectName);

        buildProject(selectedProject);
    } else {
        isLocalProject = false;
        const projectName = projectSelector.value;
        console.log("selected server-project", projectName);
        callUioApi("load", currentDovisUserID, projectName, function (status, response) {
            if (status === 200) {
                currentProjectName = projectName;
                const projectObject = response['uiocontent'];
                const projectID = projectObject['id'];
                console.info("Loaded project from server", projectObject);
                localStorage.setItem(Constants.UIOWARE_CURRENT_PROJECT, projectName);
                localStorage.setItem(`${Constants.UIOWAREPROJECTS}.${projectID}`, JSON.stringify(projectObject));
                let storedProjects = JSON.parse(localStorage.getItem(Constants.UIOWAREPROJECTS));
                storedProjects['projectIDs'].push(projectID);
                storedProjects['projectNames'].push(projectName);
                localStorage.setItem(Constants.UIOWARE_CURRENT_PROJECT, projectName);
                localStorage.setItem(Constants.UIOWAREPROJECTS, JSON.stringify(storedProjects));
                buildProject(projectID);
            } else UIOTool.errorMsg(`Could not load project ${projectName} from server: HTTP${status}`);
        });
    }
};


const clearScreen = function (projectId) {
    console.log("1. --- Clear screen and persist project " + projectId);

    if (controller) controller.persist(`${Constants.UIOWAREPROJECTS}.${projectId}`, uioProject);
    else {
        console.log("No controller is set. Returning");
        return;
    }
    if (uioProject !== null)
        console.log(`1.1 --- Clear screen uioProject.models`, uioProject.models);
    const pageID = controller.uioModel.UIO.currentPageId;
    const nodesToRemove = uioProject.models[0].UIO.pages[pageID].mappedNodes;
    console.log("1.2 --- Clear screen nodesToRemove", nodesToRemove);

    for (let i = 0; i < nodesToRemove.length; i++) {
        try {
            const w = controller.getWidgetById(nodesToRemove[i]);
            controller.setWidgetUIOName(w);
            console.log(`1.2.${i}.2 --- Clear screen remove element; `);
            let visibleWidget = document.getElementById(w.uioName);
            if (visibleWidget) {
                visibleWidget.remove();
                console.log(`1.2.${i}.2 --- Clear screen:  element was removed`);
            } else console.log(`1.2.${i}.error --- Clear screen, no such element: ${controller.uioModel.widgetPrefix()}${nodesToRemove[i]}`);
        } catch (error) {
            console.error(error);
            console.error(`1.2 --- Clear screen Could not remove element for widget with index ${nodesToRemove[i]}`);
        }
    }
    uioProject = null;
};
const loadPage = function (uioPageId) {
    // Append msgScreen to the right div, otherwise it might be removed
    // property-screen is moved by the  controller
    invisibleDiv.appendChild(msgScreen);
    let uioPageName = controller.uioModel.UIO.pages[uioPageId].uioName;
    const uioPageDescription = controller.uioModel.UIO.pages[uioPageId].uioDescription;
    const currentPageIcon = document.getElementById("currentPageName");
    currentPageIcon.innerText = `${uioPageName} `;
    currentPageIcon.title = uioPageDescription;
    let uioPage = document.getElementById(uioPageName);
    if (uioPage) uioPage.style.backgroundColor = "rgba(4, 160, 255, 0.4)";
    else console.warn("This page was not found:", uioPageName);
    const currentPage = controller.uioModel.UIO.currentPageId;
    uioPageName = controller.uioModel.UIO.pages[currentPage].uioName;
    uioPage = document.getElementById(uioPageName);
    if (uioPage) uioPage.style.backgroundColor = "inherit";
    else console.warn("This page was not found:", uioPageName);
    controller.buildPage(uioPageId);
    const editPage = document.getElementById("editPage");
    editPage.setAttribute("data-uioid", uioPageId);
    if (uioPageId !== 1) editPage.style.opacity = "1";
    else editPage.style.opacity = "0.25";
    const uioWorkarea = document.getElementById(sessionUIOModel.widget0ElementName);
    const rectangle = uioWorkarea.getBoundingClientRect();
    let horizontalSize = document.getElementById("horizontalSize");
    let verticalSize = document.getElementById("verticalSize");
    horizontalSize.innerText = " ~ " + Math.round(10 * rectangle.width * Constants.CM_PER_INCH / ppi) + " mm (" + ppi + "ppi)";
    verticalSize.innerText = " ~ " + Math.round(10 * rectangle.height * Constants.CM_PER_INCH / ppi) + " mm (" + ppi + "ppi)";

    const editPageName = document.getElementById("editPageName");
    const deletePage = document.getElementById("deletePage");
    deletePage.setAttribute("onclick", `deletePage(${uioPageId})`);
    editPageName.setAttribute("onclick", `editPage(${uioPageId})`);
    const editPageLabel = document.getElementById("editPageLabel");
    if (editPageLabel) editPageLabel.remove();
    const deletePageLabel = document.getElementById("deletePageLabel");
    if (deletePageLabel) deletePageLabel.remove();
    document.getElementById("editPageName").style.visibility = "visible";
    document.getElementById("deletePage").style.visibility = "visible";
};

const confirmDeletePage = function (pageID) {
    //!Important! always close property screen, otherwise it is removed from document
    closePropertyScreen();
    console.log("Deleting page " + pageID);
    const editPageIcon = document.getElementById("editPageName");
    const deletePageLabel = document.getElementById("deletePageLabel");
    editPageIcon.style.visibility = "visible";
    deletePageLabel.remove();
    const editPageLabel = document.getElementById("editPageLabel");
    if (editPageLabel) editPageLabel.remove();
    const deleteIcon = document.getElementById("deletePage");
    deleteIcon.setAttribute("onclick", `deletePage(${pageID})`);
    const deletePageProperties = [];
    deletePageProperties.push(pageID);
    if (!controller.modelViewChanged(controller.getWidgetById(0), deletePageProperties, UIOModelViewOperation.DELETE_PAGE)) {
        UIOTool.errorMsg(`Error: Could not delete page ${pageID}`)
    } else {
        console.info(`--------- Page ${pageID} was deleted.`);
        console.info(`--------- Reload start-page....`);
        loadPage(1);
        console.info(`--------- Start-page reloaded.`);
    }
}

const confirmEditPage = function (pageID) {
    console.log("Editing page " + pageID);
    const deletePageIcon = document.getElementById("deletePage");
    const editPageLabel = document.getElementById("editPageLabel");
    deletePageIcon.style.visibility = "visible";
    editPageLabel.remove();
    const deletePageLabel = document.getElementById("deletePageLabel");
    if (deletePageLabel) deletePageLabel.remove();
    const editIcon = document.getElementById("editPageName");
    editIcon.setAttribute("onclick", `editPage(${pageID})`);
    const updatePageObject = {};
    updatePageObject.key = `UIO.pages[${pageID}].uioDescription`;
    updatePageObject.value = editPageLabel.value;
    const editPageProperties = [];
    editPageProperties.push(updatePageObject);
    if (!controller.modelViewChanged(controller.getWidgetById(0), editPageProperties, UIOModelViewOperation.UPDATE_PAGE)) {
        UIOTool.errorMsg("Error: Could not edit page.")
    } else {
        paintPageList("currentPageName", "pageList");
        console.info(`--------- Page ${pageID} was renamed.`);
    }
}
const deletePage = function (uioPageID) {
    if (uioPageID === 1) return;
    const editPage = document.getElementById("editPage");
    const label = document.createElement("label");
    const deleteIcon = document.getElementById("deletePage");
    const editIcon = document.getElementById("editPageName");
    editIcon.style.visibility = "hidden";
    deleteIcon.setAttribute("onclick", `confirmDeletePage(${uioPageID})`);
    label.setAttribute("id", `deletePageLabel`);
    label.innerText = ` Delete page?`;
    editPage.appendChild(label);

}
const setPageType = function (uioPageID) {
    const val = document.getElementById("pageTypeSelection").valueOf();
    sessionUIOModel.UIO.pages[uioPageID].pageType = val;
    console.info("pageType=" + val);
}
const editPage = function (uioPageID) {
    if (uioPageID === 1) return;
    const editPage = document.getElementById("editPage");
    const textFieldPageName = document.createElement("input");

    textFieldPageName.setAttribute("type", "text");
    textFieldPageName.setAttribute("placeholder", "Type a new name");
    textFieldPageName.setAttribute("class", "uioPageSmall");
    const deleteIcon = document.getElementById("deletePage");
    const editIcon = document.getElementById("editPageName");
    deleteIcon.style.visibility = "hidden";
    editIcon.setAttribute("onclick", `confirmEditPage(${uioPageID})`);
    textFieldPageName.setAttribute("id", `editPageLabel`);
    textFieldPageName.innerText = ` Edit page?`;
    editPage.appendChild(textFieldPageName);

}
const paintPageList = function (currentPageElementID, pageListElementID) {
    const pageList = document.getElementById(pageListElementID);
    pageList.innerHTML = "";
    const pages = controller.getPages();
    document.getElementById(currentPageElementID).innerText = pages[controller.uioModel.UIO.currentPageId].uioName
        + " - " + pages[controller.uioModel.UIO.currentPageId].uioDescription;
    if (pages.length > 2) {
        for (let i = 1; i < pages.length; i++) {
            if (pages[i].deleted === true) {
                console.info(`Page ${i}: ${pages[i].uioDescription} is deleted. Will not be built`);
                continue;
            }
            console.log(i, "paint page" + pages[i].uioId);
            const div = document.createElement("div");
            const arrowIcon = document.createElement("i");
            const windowIcon = document.createElement("i");
            const label = document.createElement("label");

            label.innerText = ` ${pages[i].uioId}: `;

            arrowIcon.setAttribute("class", "bi bi-arrow-return-right uioPageSmall");
            windowIcon.setAttribute("class", "bi-window-stack uioPageSmall");
            label.setAttribute("class", "uioPageSmall");

            div.setAttribute("id", pages[i].uioName);
            /*----- ! Keep this just for the IDE ! -------*/
            if (false) loadPage(-1);
            /*--------------------------------------------*/
            div.setAttribute("onclick", `loadPage(${pages[i].uioId})`);

            div.appendChild(label);
            div.appendChild(arrowIcon);
            div.appendChild(windowIcon);
            const label2 = document.createElement("label");
            label2.innerText = `---  ${UIOTool.shortString(pages[i].uioDescription, 10)}`;
            label2.title = `${pages[i].uioName} - ${pages[i].uioDescription}`;
            label2.setAttribute("class", "uioPageSmall");
            div.appendChild(label2);
            pageList.appendChild(div);

        }
    }
};
const buildProject = function (projectId) {

    console.log(`2.  --- build project: currentProjectID is:`, currentProjectID);
    console.log("2.1 --- build project:", "local storage Constants.UIOWAREPROJECTS.projectId", `${Constants.UIOWAREPROJECTS}.${projectId}`);
    closePropertyScreen();
    closeMessage();

    if (currentProjectID) clearScreen(currentProjectID);
    else console.log("2.2 --- build project: No current project id is set");


    const storedProjectItem = localStorage.getItem(`${Constants.UIOWAREPROJECTS}.${projectId}`);
    try {

        // Project was set by addProject
        if (storedProjectItem === "NEW") {
            console.log("2.3a.1 --- build project: Try to build  NEW project");
            const sessionProject = sessionStorage.getItem("sessionUIOProjectName");
            const sessionProjectCode = sessionStorage.getItem("sessionUIOProjectCodeName");
            const sessionProjectDesc = sessionStorage.getItem("sessionUIOProjectDescription");
            const sessionProjectUser = sessionStorage.getItem("sessionUIOProjectUser");
            const sessionProjectGitUri = sessionStorage.getItem("sessionUIOProjectGitUri");

            uioProject = new UIOProject(projectId, sessionProject, sessionProjectCode, sessionProjectDesc, sessionProjectUser, sessionProjectGitUri);
            sessionUIOModel = new UIOModel(uioProject.id, 'UIO', null);
            uioProject.models.push(sessionUIOModel);
            uioProject.lastSaved = UIOTool.getTimestamp();
            // TODO: change after login is implemented. Up to then commit as anonymous user 4711

            /*
            New requirement: DO NOT COMMIT anything after login
             callUioApi("create", currentDovisUserID, uioProject, function (http, response) {
                 if (http === 200) {
                     if (response.message && response.filename)
                         UIOTool.uioMessage(`Project was created online as anonymous user.
                            Project-file is: ${response.filename}. ${response.message}`);
                     else UIOTool.uioMessage(`Project was created online as anonymous user.`);

                 } else {
                     if (response.filename && response.message)
                         UIOTool.errorMsg(`HTTP ${http}: Project with filename
                  '${response.filename}' could not be added online: ${response.message}`);
                     else UIOTool.errorMsg(`Could not create the project-file online: HTTP ${http}:`);
                 }
                 return response;
             });
             */
        } else {
            // Project was set by select project (?) //TODO check
            try {
                console.log("storedProjectItem", JSON.parse(storedProjectItem));
            } catch (error) {
                console.error("could not parse project");
            }
            try {
                console.log("storedProjectItem", storedProjectItem);
            } catch (error) {
                console.error("could not read  project")
            }
            const storedProject = JSON.parse(storedProjectItem);
            console.log("2.3b.1 --- build project: Try to build  this storedProject:", storedProject);
            uioProject = new UIOProject(storedProject.id, storedProject.projectName,
                storedProject.projectCodeName, storedProject.projectDescription, storedProject.userName, storedProject.gitUrl);
            uioProject.models = storedProject.models;
            console.log("2.3b.2 --- build project: stored models:", storedProject.models);
            sessionUIOModel = new UIOModel(uioProject.id, 'UIO', storedProject.models[0]);
        }
    } catch (err) {
        console.error(err);
        UIOTool.errorMsg(err);
        throw new Error(`STOP --- build project: Project with projectId ${projectId} could not be built`);
    }

    controller = new UIOController(sessionUIOModel);
    const currentProjectName = uioProject['projectName'];
    const lastSaved = uioProject.lastSaved;
    const projectCreationTimestamp = uioProject.createdAt;
    const projectNameIcon = document.getElementById("projectName");
    const projectTimeIcon = document.getElementById("projectSaveTime");
    projectTimeIcon.innerText = ` Last saved: ${lastSaved}`;
    projectTimeIcon.title = ` Creation time: ${projectCreationTimestamp}`
    projectNameIcon.innerText = ` Project '${currentProjectName}' (id: ${projectId})`;
    projectNameIcon.title = `projectCode: '${uioProject.projectCodeName}' url:${uioProject.gitUrl}'`;
    paintPageList("currentPageName", "pageList");
    const nodesToBuild = controller.getCurrentPage().mappedNodes;
    controller.buildWidgets(nodesToBuild);
    currentProjectID = projectId;
    currentProjectIsSelected = true;
    localStorage.setItem(`${Constants.UIOWAREPROJECTS}.${projectId}`, JSON.stringify(uioProject));

    document.getElementById("uioMetaData").style.opacity = "1";
    // const uploadProject = document.getElementById("uploadProject");
    uploadProject.style.opacity = "1";
    const editPage = document.getElementById("editPage");
    const pageID = editPage.getAttribute("data-uioid");
    if (pageID !== "0") editPage.style.opacity = "1";

};

const UIOWareInfo = function () {
    const info = `<h4>UIOWare &#169;</h4><label> Enpasoft GmbH</label><br><label>All rights reserved.</label>`;
    resizeDiv(msgScreen, 300, 110, "silver");
    showMessage(invisibleDiv, info);

    //TODO check if uioProject needs to have models as attributes
    console.log(`current project in memory`, uioProject);
    console.log("current projectName:", localStorage.getItem(Constants.UIOWARE_CURRENT_PROJECT));
    console.log("projects:", JSON.parse(localStorage.getItem(Constants.UIOWAREPROJECTS)));
    console.log("currentProjectID:", currentProjectID);
    //console.log("currently stored project:", localStorage.getItem(Constants.UIOWAREPROJECTS + "." + currentProjectID));
    console.log("current model:", sessionUIOModel);
};
let currentUioID = null;
let currentAction = null;
let currentWidget = null;

const doClientUIO = function (id, uioid, uiotype, uioaction, allAttributes, eventName) {
    try {
        if (eventName === "mousemove" && currentWidget && currentAction === UIOModelViewOperation.RESIZE_WIDGET) {
            controller.modelViewChanged(currentWidget, [], currentAction);
            const position = controller.getCurrentPosition();
            controller.setMouseStart(position.x, position.y);
        } else if (eventName === "mousemove" && currentWidget && currentAction === UIOModelViewOperation.MOVE_WIDGET) {
            //console.log("Moving widget " + currentWidget.id);
            if (currentWidget.isCurrentlyMoving) {
                controller.modelViewChanged(currentWidget, [], currentAction);
                const position = controller.getCurrentPosition();
                controller.setMouseStart(position.x, position.y);
                document.body.style.cursor = "move";
            }
        } else if (eventName === "mousedown" && uioid && uioaction === UIOModelViewOperation.RESIZE_WIDGET) {
            document.body.style.cursor = "crosshair";
            currentWidget = controller.getWidgetById(uioid);
            currentWidget.isCurrentlyResizing = true;
            currentUioID = uioid;
            currentAction = uioaction;
            const position = controller.getCurrentPosition();
            controller.setMouseStart(position.x, position.y);
            //console.log("Start Resizing widget " + uioid);
        } else if (eventName === "mousedown" && uioid && uioaction === UIOModelViewOperation.HIDE_WIDGET) {
            currentWidget = controller.getWidgetById(uioid);
            currentUioID = uioid;
            currentAction = UIOModelViewOperation.HIDE_WIDGET;
            controller.modelViewChanged(currentWidget, [], UIOModelViewOperation.HIDE_WIDGET);
        } else if (eventName === "mousedown" && uioid && uioaction === UIOModelViewOperation.DELETE_WIDGET) {
            currentWidget = controller.getWidgetById(uioid);
            const removeButton = document.createElement("i");
            const moveScreenButton = document.createElement("i");
            moveScreenButton.setAttribute("class", "bi bi-chevron-bar-right");
            removeButton.setAttribute("class", "bi bi-trash-fill");
            removeButton.setAttribute("onclick", `removeWidget(${currentWidget.id})`);
            moveScreenButton.setAttribute("onclick", `moveMessageScreen()`);
            removeButton.style.fontSize = "2em";
            removeButton.style.borderColor = "red";
            removeButton.style.borderWidth = "3";
            //setAllWidgetsOpacity("0.95");
            currentUioID = uioid;
            currentAction = UIOModelViewOperation.DELETE_WIDGET;
            const removeWidgetMessage = `${moveScreenButton.outerHTML}<br>
                                     <i class="bi bi-exclamation-triangle"></i>
                                     <i class="bi bi-patch-question"></i>
                                     <label>&nbsp;REMOVE<br>
                                     ${currentWidget.widgetType} '${currentWidget.uioLabel}'?
                                     </label><br>${removeButton.outerHTML}`;
            showMessage(document.getElementById(currentWidget.uioName), removeWidgetMessage);
            resizeDiv(getMessageScreen(), 155, 155, "lightsteelblue");
        }

        /*-------------------   UPDATE WIDGET -------------------------------------------------*/
        else if (eventName === "mousedown" && uioid && uioaction === UIOModelViewOperation.UPDATE_WIDGET) {
            currentWidget = controller.getWidgetById(uioid);
            const keyPath = allAttributes['id']['nodeValue'];
            const updateHTML = UIOTool.createUpdateHTML(currentWidget, keyPath, 'UIOTool.confirm()');
            UIOTool.showApiUpdateBox(controller, currentWidget, updateHTML, keyPath);
        }
        /*--------------------------------------------------------------------------------------*/

        else if (eventName === "mousedown" && uioid && isWidget(uiotype)) {
            currentWidget = controller.getWidgetById(uioid);
            currentWidget.isCurrentlyMoving = true;
            currentUioID = uioid;
            currentAction = UIOModelViewOperation.MOVE_WIDGET;
            const position = controller.getCurrentPosition();
            controller.setMouseStart(position.x, position.y);
            document.body.style.cursor = "move";
        } else if (uioaction === UIOModelViewOperation.DELETE_WIDGET) {
            document.body.style.cursor = "not-allowed";
        } else if (uioaction === UIOModelViewOperation.HIDE_WIDGET) {
            document.body.style.cursor = "pointer";
        } else if (uioaction === UIOModelViewOperation.CLONE_WIDGET) {
            document.body.style.cursor = "copy";
        } else if (uioaction === UIOModelViewOperation.RESIZE_WIDGET) {
            document.body.style.cursor = "crosshair";
        } else if (id && (id.startsWith("windowLabel") || id.startsWith("prop"))) {
            document.body.style.cursor = "help";
        } else if (uioaction === UIOModelViewOperation.CREATE_PAGE) {
            paintPageList("currentPageName", "pageList");
        } else if (uioaction === UIOModelViewOperation.DELETE_PAGE) {
            console.info(`--------- Repaint Page ${allAttributes[0]}after deletion`);
            paintPageList("currentPageName", "pageList");
        } else if (uioaction === UIOModelViewOperation.UPDATE_PAGE) {
            paintPageList("currentPageName", "pageList");
        } else {
            //console.log(eventName, id, uioid, uiotype, uioaction,);
            document.body.style.cursor = "auto";
        }
        if (eventName === "mouseup") {
            document.body.style.cursor = "auto";
            currentUioID = null;
            currentAction = null;
            currentWidget = null;
        }
        return true;
    } catch (err) {
        console.error(err);
        UIOTool.errorMsg(err);
        return false;
    }
};


const isWidget = function (uiotype) {
    return uiotype === UIOWareWidget.UIOWareWindow || uiotype === UIOWareWidget.UIOWareTable
        || uiotype === UIOWareWidget.UIOWareMockTextField || uiotype === UIOWareWidget.UIOWareTextarea
        || uiotype === UIOWareWidget.UIOWareMockTextarea || uiotype === UIOWareWidget.UIOWareInput
        || uiotype === UIOWareWidget.UIOWareMockSelectionList || uiotype === UIOWareWidget.UIOWareMockRadioCheckBox
        || uiotype === UIOWareWidget.UIOWareMockLink || uiotype === UIOWareWidget.UIOWareMockButton
        || uiotype === UIOWareWidget.UIOWareImage || UIOWareWidget.UIOWareTextChapter
        ;
};


let isFade = false;
const toggleFade = function () {
    if (!controller) return;
    const faderIcon = document.getElementById("toggleFade");
    if (isFade) {
        controller.noFade();
        isFade = false;
        faderIcon.setAttribute("class", "bi bi-stickies-fill");
    } else {
        controller.fade();
        isFade = true;
        faderIcon.setAttribute("class", "bi bi-stickies");
    }
}

const closeErrorMessage = function () {
    const elemText = document.getElementById("errorMessageText");
    const elem = document.getElementById("errorMessage");
    elemText.innerText = ``;
    elem.style.visibility = "hidden";
}