"use strict";

/*******   Block for dummy functions    *****/

var module = "MOD0";
var release = "REL1";

function debug2(module, str, release, data) {
    if (data) {
        console.log(module + "-" + release + ": " + str + ", data: " + JSON.stringify(data));
    } else {
        console.log(module + "-" + release + ": " + str);
    }
}

function apiCall(path, dataObject, event1CallBack, httpErrorCallBack, httpMethod) {
    debug2(module, httpMethod + "-call ' to " + path, release, null);
    event1CallBack(dataObject);
}

function httpErrorCallBack(txt) {
    // TODO
    console.error(txt);
}

/**********  End of dummy functions   ****************/



let callBackTriggeredByObject1 = function (rmlData) {
    // TODO
    debug2(module, "action for event1 triggered by obj1", release, rmlData);
};

let callBackTriggeredByObject2 = function (rmlData) {
    const attr1 = $("#input1").val();
    const attr2 = rmlData.attribute2;
    const rml = {
        attribute1: attr1,
        attribute2: attr2
    }
    debug2(module, "action for event1 triggered by obj2", release, rml);
    $("#textarea1").text(rml.attribute1 + "-" + rml.attribute2);
};

let callBackDefaultTrigger = function (rmlData) {
    // TODO
    debug2(module, "default action for event1 triggered by  default object", release, rmlData);
};

const functionForApiCall1 = function (objectId, dataObject) {
    // TODO
    let event1CallBack;
    switch (objectId) {
        case "butt1":
            event1CallBack = callBackTriggeredByObject1;
            break;
        case "input1":
            event1CallBack = callBackTriggeredByObject2;
            break;
        default:
            event1CallBack = callBackDefaultTrigger;
    }
    if (!dataObject) {
        console.error("Cannot call api with empty dataobject.");
    } else {
        apiCall("/path/to/rest/apicall", dataObject, event1CallBack, httpErrorCallBack, "post");
    }
};

$(document)
    .on(
        'click',
        '.clickEventApiCall_1_1',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            const attribute1 = $(this).data("attribute1");
            const attribute2 = $(this).data("attribute2");
            if (!attribute1) {
                console.error("Calling object with id '" + objectId + "' is missing  mandatory data-attribute 'attribute1'");
            }
            if (!attribute2) {
                console.error("Calling object with id '" + objectId + "' is missing  mandatory data-attribute 'attribute2'");
            }
            let dataObject;
            if (attribute1 && attribute2) {
                dataObject = {
                    "attribute1": attribute1,
                    "attribute2": attribute2
                };
            }
            functionForApiCall1(objectId, dataObject);
        }
    );

$(document)
    .on(
        'keyup',
        '.keyUpEventApiCall_1_1',
        function () {
            let objname = "unassigned";
            try {
                objname = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }
            const attribute1 = $(this).data("attribute1");
            const attribute2 = $(this).data("attribute2");
            if (!attribute1) {
                console.error("Calling object with id '" + objname + "' is missing  mandatory data-attribute 'attribute1'");
            }
            if (!attribute2) {
                console.error("Calling object with id '" + objname + "' is missing  mandatory data-attribute 'attribute2'");
            }
            let dataObject;
            if (attribute1 && attribute2) {
                dataObject = {
                    "attribute1": attribute1,
                    "attribute2": attribute2
                };
            }
            functionForApiCall1(objname, dataObject);
        }
    );