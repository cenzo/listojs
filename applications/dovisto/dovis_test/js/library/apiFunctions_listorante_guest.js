/***************************************
 JAVA-Script RESTclient-calls.
 API: 'listorante'
 Module: 'Guest'
 Version: 1.10
 ***************************************/

/***************************************
 CALL-ID: listorante.Guest.apiCall1
 Add a product-item to the order-list.
 ***************************************/
function apiCall_listorante_guest_addtoproductlist
(idbillnumber,
 idorderedproduct,
 tablenumber,
 waiternumber,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idbillnumber": idbillnumber,
        "idorderedproduct": idorderedproduct,
        "tablenumber": tablenumber,
        "waiternumber": waiternumber
    };
    return apiCall("listorante.Guest.apiCall1", "/1/guest/addtoproductlist", dataObject, callBack, httpErrorCallBack, "post");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall2
 Delete a product from the order-list
 ***************************************/
function apiCall_listorante_guest_delitem
(idorder,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idorder": idorder
    };
    return apiCall("listorante.Guest.apiCall2", "/1/guest/delitem", dataObject, callBack, httpErrorCallBack, "put");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall3
 Set remark on an ordered item
 ***************************************/
function apiCall_listorante_guest_itemremark
(idorder,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idorder": idorder
    };
    return apiCall("listorante.Guest.apiCall3", "/1/guest/itemremark", dataObject, callBack, httpErrorCallBack, "post");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall4
 Show shopping cart of a customer
 ***************************************/
function apiCall_listorante_guest_showcart
(billnumber,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "billnumber": billnumber
    };
    return apiCall("listorante.Guest.apiCall4", "/1/guest/showcart", dataObject, callBack, httpErrorCallBack, "post");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall5
 Show status of ordered items of a bill
 ***************************************/
function apiCall_listorante_guest_deliverystatus
(idbillnumber,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idbillnumber": idbillnumber
    };
    return apiCall("listorante.Guest.apiCall5", "/1/guest/deliverystatus", dataObject, callBack, httpErrorCallBack, "post");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall6
 Send listed items as order to kitchen
 ***************************************/
function apiCall_listorante_guest_sendorder
(idbillnumber,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idbillnumber": idbillnumber
    };
    return apiCall("listorante.Guest.apiCall6", "/1/guest/sendorder", dataObject, callBack, httpErrorCallBack, "put");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall7
 Send a notification to the currently assigned waiter
 ***************************************/
function apiCall_listorante_guest_sendnotification
(idbill,
 idwaiter,
 text,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idbill": idbill,
        "idwaiter": idwaiter,
        "text": text
    };
    return apiCall("listorante.Guest.apiCall7", "/1/guest/sendnotification", dataObject, callBack, httpErrorCallBack, "put");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall8
 Get meta-information about selected bill
 ***************************************/
function apiCall_listorante_guest_billinfo
(idbill,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idbill": idbill
    };
    return apiCall("listorante.Guest.apiCall8", "/1/guest/billinfo", dataObject, callBack, httpErrorCallBack, "post");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall9
 Initiate a new bill; it will be assigned to the chief-waiter (chief-waiter has idwaiter=1)
 ***************************************/
function apiCall_listorante_guest_createbill
(tablenumber,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "tablenumber": tablenumber
    };
    return apiCall("listorante.Guest.apiCall9", "/1/guest/createbill", dataObject, callBack, httpErrorCallBack, "post");
}

/***************************************
 CALL-ID: listorante.Guest.apiCall10
 Get order-information per user of the selected bill
 ***************************************/
function apiCall_listorante_guest_orderuserinfo
(idbill,
 callBack, httpErrorCallBack) {
    const dataObject = {
        "idbill": idbill
    };
    return apiCall("listorante.Guest.apiCall10", "/1/guest/orderuserinfo", dataObject, callBack, httpErrorCallBack, "post");
}


/***************************************
 CALL-ID: listorante.Guest.apiCall12
 Get the table-id of the logged-in guest
 ***************************************/
function apiCall_listorante_guest_getguesttable
(callBack, httpErrorCallBack) {
    const dataObject = {};
    return apiCall("listorante.Guest.apiCall12", "/1/guest/getguesttable", dataObject, callBack, httpErrorCallBack, "post");
}
