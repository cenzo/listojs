/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'listorante'
	Module: 'Admin'
	Version: 2.20
***************************************/

/***************************************
	CALL-ID: listorante.Admin.apiCall1
	Create a new product
	***************************************/
function apiCall_listorante_admin_newproduct
	(price,
	prod_desc,
	prod_image,
	prod_info,
	prod_label,
	prod_name,
	qty_in_stock,
	qty_threshold,
	unit,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"price" : price,
	"prod_desc" : prod_desc,
	"prod_image" : prod_image,
	"prod_info" : prod_info,
	"prod_label" : prod_label,
	"prod_name" : prod_name,
	"qty_in_stock" : qty_in_stock,
	"qty_threshold" : qty_threshold,
	"unit" : unit
	};
	return apiCall("listorante.Admin.apiCall1","/1/admin/newproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall2
	Create a new product-category underneath an existing parent category.
		('idproductcategory' here is the parent-id. The newly created category will be assigned a new id). 
		 
	***************************************/
function apiCall_listorante_admin_newcategory
	(hierarchy_1,
	hierarchy_2,
	hierarchy_3,
	hierarchy_4,
	hierarchy_5,
	idproductcategory,
	level,
	position,
	prodcatdesc,
	prodcatdesc_locale,
	prodcatimage,
	prodcatname,
	prodcatname_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"hierarchy_1" : hierarchy_1,
	"hierarchy_2" : hierarchy_2,
	"hierarchy_3" : hierarchy_3,
	"hierarchy_4" : hierarchy_4,
	"hierarchy_5" : hierarchy_5,
	"idproductcategory" : idproductcategory,
	"level" : level,
	"position" : position,
	"prodcatdesc" : prodcatdesc,
	"prodcatdesc_locale" : prodcatdesc_locale,
	"prodcatimage" : prodcatimage,
	"prodcatname" : prodcatname,
	"prodcatname_locale" : prodcatname_locale
	};
	return apiCall("listorante.Admin.apiCall2","/1/admin/newcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall3
	Create new staff-person
	***************************************/
function apiCall_listorante_admin_newstaff
	(email,
	first_name,
	is_active,
	is_superuser,
	last_name,
	password,
	username,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"is_active" : is_active,
	"is_superuser" : is_superuser,
	"last_name" : last_name,
	"password" : password,
	"username" : username
	};
	return apiCall("listorante.Admin.apiCall3","/1/admin/newstaff", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall4
	Delete a staff-person
	***************************************/
function apiCall_listorante_admin_deletestaff
	(USERIDVALUE,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"USERIDVALUE" : USERIDVALUE
	};
	return apiCall("listorante.Admin.apiCall4","/1/admin/deletestaff", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall5
	Assign role to staff person
	***************************************/
function apiCall_listorante_admin_assignrole
	(roleid,
	userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid,
	"userid" : userid
	};
	return apiCall("listorante.Admin.apiCall5","/1/admin/assignrole", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall6
	Revoke role from staff person
	***************************************/
function apiCall_listorante_admin_revokerole
	(roleid,
	userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid,
	"userid" : userid
	};
	return apiCall("listorante.Admin.apiCall6","/1/admin/revokerole", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall7
	select a layout
	***************************************/
function apiCall_listorante_admin_setlayoutid
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall7","/1/admin/setlayoutid", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall8
	select all possible structures of the selected layout
	***************************************/
function apiCall_listorante_admin_layoutstructures
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall8","/1/admin/layoutstructures", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall9
	select all possible images of the selected layout
	***************************************/
function apiCall_listorante_admin_layoutimages
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall9","/1/admin/layoutimages", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall10
	select all possible colors of the selected layout
	***************************************/
function apiCall_listorante_admin_layoutcolors
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall10","/1/admin/layoutcolors", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall11
	Update a layout-text. Works currently only for texts. This call nNeeds to be amende for superadmin
	***************************************/
function apiCall_listorante_admin_updatelayout
	(layoutelementname,
	layoutid,
	value,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutelementname" : layoutelementname,
	"layoutid" : layoutid,
	"value" : value
	};
	return apiCall("listorante.Admin.apiCall11","/1/admin/updatelayout", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall13
	edit a table
	***************************************/
function apiCall_listorante_admin_tableedit
	(description,
	description_locale,
	idcustomertable,
	isseat,
	places,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"description" : description,
	"description_locale" : description_locale,
	"idcustomertable" : idcustomertable,
	"isseat" : isseat,
	"places" : places
	};
	return apiCall("listorante.Admin.apiCall13","/1/admin/tableedit", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall14
	insert a new table
	***************************************/
function apiCall_listorante_admin_tableinsert
	(description,
	description_locale,
	isseat,
	places,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"description" : description,
	"description_locale" : description_locale,
	"isseat" : isseat,
	"places" : places
	};
	return apiCall("listorante.Admin.apiCall14","/1/admin/tableinsert", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall15
	Delete a table
	***************************************/
function apiCall_listorante_admin_tabledelete
	(idcustomertable,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcustomertable" : idcustomertable
	};
	return apiCall("listorante.Admin.apiCall15","/1/admin/tabledelete", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall16
	Update profile data
	***************************************/
function apiCall_listorante_admin_editprofile
	(email,
	first_name,
	iduser,
	image,
	is_active,
	is_staff,
	last_name,
	username,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"iduser" : iduser,
	"image" : image,
	"is_active" : is_active,
	"is_staff" : is_staff,
	"last_name" : last_name,
	"username" : username
	};
	return apiCall("listorante.Admin.apiCall16","/1/admin/editprofile", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall17
	Edit a product-category
	***************************************/
function apiCall_listorante_admin_editcategory
	(hierarchy_1,
	hierarchy_2,
	hierarchy_3,
	hierarchy_4,
	hierarchy_5,
	idproductcategory,
	idstyle,
	position,
	prodcatdesc,
	prodcatdesc_locale,
	prodcatimage,
	prodcatname,
	prodcatname_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"hierarchy_1" : hierarchy_1,
	"hierarchy_2" : hierarchy_2,
	"hierarchy_3" : hierarchy_3,
	"hierarchy_4" : hierarchy_4,
	"hierarchy_5" : hierarchy_5,
	"idproductcategory" : idproductcategory,
	"idstyle" : idstyle,
	"position" : position,
	"prodcatdesc" : prodcatdesc,
	"prodcatdesc_locale" : prodcatdesc_locale,
	"prodcatimage" : prodcatimage,
	"prodcatname" : prodcatname,
	"prodcatname_locale" : prodcatname_locale
	};
	return apiCall("listorante.Admin.apiCall17","/1/admin/editcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall18
	Edit a product
	***************************************/
function apiCall_listorante_admin_editproduct
	(idlabel,
	idprodstyle,
	idproduct,
	image,
	image_thumb,
	position,
	price,
	proddesc_locale,
	prodinfo_locale,
	prodname,
	prodname_locale,
	qty_in_stock,
	qty_unit,
	threshold_qty,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlabel" : idlabel,
	"idprodstyle" : idprodstyle,
	"idproduct" : idproduct,
	"image" : image,
	"image_thumb" : image_thumb,
	"position" : position,
	"price" : price,
	"proddesc_locale" : proddesc_locale,
	"prodinfo_locale" : prodinfo_locale,
	"prodname" : prodname,
	"prodname_locale" : prodname_locale,
	"qty_in_stock" : qty_in_stock,
	"qty_unit" : qty_unit,
	"threshold_qty" : threshold_qty
	};
	return apiCall("listorante.Admin.apiCall18","/1/admin/editproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall19
	Assign a product to a category
	***************************************/
function apiCall_listorante_admin_setproductcategory
	(ididhierarchy,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ididhierarchy" : ididhierarchy,
	"idproduct" : idproduct
	};
	return apiCall("listorante.Admin.apiCall19","/1/admin/setproductcategory", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall20
	Remove a product from a category
	***************************************/
function apiCall_listorante_admin_unsetproductcategory
	(ididhierarchy,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ididhierarchy" : ididhierarchy,
	"idproduct" : idproduct
	};
	return apiCall("listorante.Admin.apiCall20","/1/admin/unsetproductcategory", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall21
	Admin  Dashboard 
	***************************************/
function apiCall_listorante_admin_admindash
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall21","/1/admin/admindash", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall22
	select all items currently in preparation in kitchen
	***************************************/
function apiCall_listorante_admin_orders
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall22","/1/admin/orders", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall23
	Define a new product attribute
	***************************************/
function apiCall_listorante_admin_defineattribute
	(attributemnemonic,
	productattributename_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attributemnemonic" : attributemnemonic,
	"productattributename_locale" : productattributename_locale
	};
	return apiCall("listorante.Admin.apiCall23","/1/admin/defineattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall24
	Assign a  product attribute
	***************************************/
function apiCall_listorante_admin_setattribute
	(idattribute,
	idproduct,
	productattributevalue_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct,
	"productattributevalue_locale" : productattributevalue_locale
	};
	return apiCall("listorante.Admin.apiCall24","/1/admin/setattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall25
	De-assign a  product attribute
	***************************************/
function apiCall_listorante_admin_unsetattribute
	(idattribute,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct
	};
	return apiCall("listorante.Admin.apiCall25","/1/admin/unsetattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall26
	Delete an attribute-definition
	***************************************/
function apiCall_listorante_admin_deleteattributedefinition
	(idproductattributename,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproductattributename" : idproductattributename
	};
	return apiCall("listorante.Admin.apiCall26","/1/admin/deleteattributedefinition", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall27
	Insert a new image for a product
	***************************************/
function apiCall_listorante_admin_insertproductimage
	(idproduct,
	imagepath,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct,
	"imagepath" : imagepath
	};
	return apiCall("listorante.Admin.apiCall27","/1/admin/insertproductimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall28
	Delete an image of a product
	***************************************/
function apiCall_listorante_admin_deleteproductimage
	(idlnk_product_image,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlnk_product_image" : idlnk_product_image
	};
	return apiCall("listorante.Admin.apiCall28","/1/admin/deleteproductimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall29
	Define a new localized text 
	***************************************/
function apiCall_listorante_admin_newText
	(id,
	mnemonic,
	text_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id,
	"mnemonic" : mnemonic,
	"text_locale" : text_locale
	};
	return apiCall("listorante.Admin.apiCall29","/1/admin/newText", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall30
	Update a localized text 
	***************************************/
function apiCall_listorante_admin_editText
	(id,
	mnemonic,
	text_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id,
	"mnemonic" : mnemonic,
	"text_locale" : text_locale
	};
	return apiCall("listorante.Admin.apiCall30","/1/admin/editText", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall31
	select all user profiles 
	***************************************/
function apiCall_listorante_admin_userprofiles
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall31","/1/admin/userprofiles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall32
	select all roles
	***************************************/
function apiCall_listorante_admin_userroles
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall32","/1/admin/userroles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall33
	select roles of a given user
	***************************************/
function apiCall_listorante_admin_getuserroles
	(userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"userid" : userid
	};
	return apiCall("listorante.Admin.apiCall33","/1/admin/getuserroles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall34
	select owners of a given role
	***************************************/
function apiCall_listorante_admin_getroleowners
	(roleid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid
	};
	return apiCall("listorante.Admin.apiCall34","/1/admin/getroleowners", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall35
	select all possible fonts of the selected layout
	***************************************/
function apiCall_listorante_admin_layoutfonts
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall35","/1/admin/layoutfonts", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall36
	select all possible texts of the selected layout
	***************************************/
function apiCall_listorante_admin_layouttexts
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall36","/1/admin/layouttexts", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall37
	delete a product
	***************************************/
function apiCall_listorante_admin_deleteproduct
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("listorante.Admin.apiCall37","/1/admin/deleteproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall38
	select all css-elements of the selected layout
	***************************************/
function apiCall_listorante_admin_layoutelements
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall38","/1/admin/layoutelements", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall39
	move an existing category under a new parent category
	***************************************/
function apiCall_listorante_admin_linkcategory
	(idcategory,
	idparentcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory,
	"idparentcategory" : idparentcategory
	};
	return apiCall("listorante.Admin.apiCall39","/1/admin/linkcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall40
	delete a category
	***************************************/
function apiCall_listorante_admin_deletecategory
	(idcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory
	};
	return apiCall("listorante.Admin.apiCall40","/1/admin/deletecategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall41
	Edit a  product attribute value
	***************************************/
function apiCall_listorante_admin_editattribute
	(idattribute,
	idproduct,
	value,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct,
	"value" : value
	};
	return apiCall("listorante.Admin.apiCall41","/1/admin/editattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall42
	Edit a  product attribute name
	***************************************/
function apiCall_listorante_admin_editattributename
	(attributename,
	idproductattributename,
	mnemonic,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attributename" : attributename,
	"idproductattributename" : idproductattributename,
	"mnemonic" : mnemonic
	};
	return apiCall("listorante.Admin.apiCall42","/1/admin/editattributename", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall43
	Update a css-file with the saved element-types
	***************************************/
function apiCall_listorante_admin_updatecss
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall43","/1/admin/updatecss", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall44
	Get the css for a specific layoutid
	***************************************/
function apiCall_listorante_admin_getcss
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("listorante.Admin.apiCall44","/1/admin/getcss", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall45
	Show all open tickets
	***************************************/
function apiCall_listorante_admin_tickets
	(customerid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid
	};
	return apiCall("listorante.Admin.apiCall45","/1/admin/tickets", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall46
	Create a new support-ticket
	***************************************/
function apiCall_listorante_admin_createticket
	(customerid,
	ticketdefid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid,
	"ticketdefid" : ticketdefid
	};
	return apiCall("listorante.Admin.apiCall46","/1/admin/createticket", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall47
	Upload text and or data for a support-ticket
	***************************************/
function apiCall_listorante_admin_addticketdata
	(attachedfile,
	customertext,
	ticketid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attachedfile" : attachedfile,
	"customertext" : customertext,
	"ticketid" : ticketid
	};
	return apiCall("listorante.Admin.apiCall47","/1/admin/addticketdata", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall48
	Get all data of a support-ticket
	***************************************/
function apiCall_listorante_admin_ticketdata
	(ticketid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ticketid" : ticketid
	};
	return apiCall("listorante.Admin.apiCall48","/1/admin/ticketdata", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall49
	Show which services can be requested
	***************************************/
function apiCall_listorante_admin_ticketdef
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall49","/1/admin/ticketdef", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall50
	Show shopping cart of a customer 
	***************************************/
function apiCall_listorante_admin_showcart
	(billnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"billnumber" : billnumber
	};
	return apiCall("listorante.Admin.apiCall50","/1/admin/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall51
	get customer-ID
	***************************************/
function apiCall_listorante_admin_customerid
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall51","/1/admin/customerid", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall52
	change user password
	***************************************/
function apiCall_listorante_admin_changeuserpassword
	(iduser,
	password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iduser" : iduser,
	"password" : password
	};
	return apiCall("listorante.Admin.apiCall52","/1/admin/changeuserpassword", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall53
	store a new string for a certain context
	***************************************/
function apiCall_listorante_admin_storetext
	(idsemantic,
	stringvalue,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idsemantic" : idsemantic,
	"stringvalue" : stringvalue
	};
	return apiCall("listorante.Admin.apiCall53","/1/admin/storetext", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall54
	update a string for a certain context
	***************************************/
function apiCall_listorante_admin_updatetext
	(idsemantic,
	stringvalue,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idsemantic" : idsemantic,
	"stringvalue" : stringvalue
	};
	return apiCall("listorante.Admin.apiCall54","/1/admin/updatetext", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall55
	update a settings-value
	***************************************/
function apiCall_listorante_admin_setguestpassword
	(setting,
	val,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"setting" : setting,
	"val" : val
	};
	return apiCall("listorante.Admin.apiCall55","/1/admin/setguestpassword", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall56
	assign the place of a guest to a newly created table 
	***************************************/
function apiCall_listorante_admin_setguesttable
	(idtable,
	iduser,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idtable" : idtable,
	"iduser" : iduser
	};
	return apiCall("listorante.Admin.apiCall56","/1/admin/setguesttable", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall57
	Show the current amount of available credits
	***************************************/
function apiCall_listorante_admin_showCredits
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall57","/1/admin/showCredits", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall58
	Select the user-data of currently logged-in Administrator
	***************************************/
function apiCall_listorante_admin_adminuserdata
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Admin.apiCall58","/1/admin/adminuserdata", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Admin.apiCall59
	Insert the stripe account id
	***************************************/
function apiCall_listorante_admin_setstripeid
	(stripe_user_account,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"stripe_user_account" : stripe_user_account
	};
	return apiCall("listorante.Admin.apiCall59","/1/admin/setstripeid", dataObject, callBack, httpErrorCallBack,"post");
}


