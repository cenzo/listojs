/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'listorante'
	Module: 'Public'
	Version: 1.43
***************************************/

/***************************************
	CALL-ID: listorante.Public.apiCall1
	Show Products
	***************************************/
function apiCall_listorante_public_products
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Public.apiCall1","/1/public/products", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall2
	Show Categories
	***************************************/
function apiCall_listorante_public_categories
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Public.apiCall2","/1/public/categories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall3
	Show to which categories a given product belongs to
	***************************************/
function apiCall_listorante_public_productcategories
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("listorante.Public.apiCall3","/1/public/productcategories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall4
	Show child-categories of a category
	***************************************/
function apiCall_listorante_public_category
	(category,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"category" : category
	};
	return apiCall("listorante.Public.apiCall4","/1/public/category", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall5
	Show which products belong to the subtree of a given category
	***************************************/
function apiCall_listorante_public_product
	(idcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory
	};
	return apiCall("listorante.Public.apiCall5","/1/public/product", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall6
	Get a Text by id
	***************************************/
function apiCall_listorante_public_textbyid
	(id,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id
	};
	return apiCall("listorante.Public.apiCall6","/1/public/textbyid", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall7
	Get a Text by mnemonic
	***************************************/
function apiCall_listorante_public_textbymnemonic
	(mnemonic,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"mnemonic" : mnemonic
	};
	return apiCall("listorante.Public.apiCall7","/1/public/textbymnemonic", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall8
	New client registration
	***************************************/
function apiCall_listorante_public_register
	(domainName,
	eMail,
	firstName,
	lastName,
	passWord,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName,
	"eMail" : eMail,
	"firstName" : firstName,
	"lastName" : lastName,
	"passWord" : passWord
	};
	return apiCall("listorante.Public.apiCall8","/1/public/register", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: listorante.Public.apiCall9
	Check  a domain-name
	***************************************/
function apiCall_listorante_public_domaincheck
	(domainName,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName
	};
	return apiCall("listorante.Public.apiCall9","/1/public/domaincheck", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall10
	Show available layouts
	***************************************/
function apiCall_listorante_public_layouts
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Public.apiCall10","/1/public/layouts", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall11
	Show attributes of a product
	***************************************/
function apiCall_listorante_public_getproductattributes
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("listorante.Public.apiCall11","/1/public/getproductattributes", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall12
	Show all attribute-definitions 
	***************************************/
function apiCall_listorante_public_allproductattributes
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Public.apiCall12","/1/public/allproductattributes", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall13
	Select images of a product
	***************************************/
function apiCall_listorante_public_productimages
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("listorante.Public.apiCall13","/1/public/productimages", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall14
	select all tables
	***************************************/
function apiCall_listorante_public_showtables
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Public.apiCall14","/1/public/showtables", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall15
	Show parent-categories of a category
	***************************************/
function apiCall_listorante_public_parentcategories
	(category,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"category" : category
	};
	return apiCall("listorante.Public.apiCall15","/1/public/parentcategories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall16
	Select id of a product by its label 
	***************************************/
function apiCall_listorante_public_idproductbylabel
	(idlabel,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlabel" : idlabel
	};
	return apiCall("listorante.Public.apiCall16","/1/public/idproductbylabel", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall17
	Select all variable texts of a layout
	***************************************/
function apiCall_listorante_public_assignedtexts
	(layout,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layout" : layout
	};
	return apiCall("listorante.Public.apiCall17","/1/public/assignedtexts", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall18
	Show id of currently selected  layout
	***************************************/
function apiCall_listorante_public_layoutid
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("listorante.Public.apiCall18","/1/public/layoutid", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall19
	Select a long paragraph as html or text via the contextid
	***************************************/
function apiCall_listorante_public_longtext
	(idcontext,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcontext" : idcontext
	};
	return apiCall("listorante.Public.apiCall19","/1/public/longtext", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall20
	Select a long paragraph as html or text by its contextname
	***************************************/
function apiCall_listorante_public_longtextbystr
	(contextname,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"contextname" : contextname
	};
	return apiCall("listorante.Public.apiCall20","/1/public/longtextbystr", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: listorante.Public.apiCall21
	Get the value of a setting
	***************************************/
function apiCall_listorante_public_settings
	(setting,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"setting" : setting
	};
	return apiCall("listorante.Public.apiCall21","/1/public/settings", dataObject, callBack, httpErrorCallBack,"post");
}


