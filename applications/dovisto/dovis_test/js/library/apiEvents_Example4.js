/***  Triggers for 'keyDown'-event    ******************/
/***  SELECT AND AMEND THE APPROPRIATE TRIGGERS  ******************/

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_newproduct',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const price = $(this).data("price");
            const prod_desc = $(this).data("prod_desc");
            const prod_image = $(this).data("prod_image");
            const prod_info = $(this).data("prod_info");
            const prod_label = $(this).data("prod_label");
            const prod_name = $(this).data("prod_name");
            const qty_in_stock = $(this).data("qty_in_stock");
            const qty_threshold = $(this).data("qty_threshold");
            const unit = $(this).data("unit");

            if (!price) {
                console.error(`Variable 'price' in object with id ${objectId} has no value.`);
            }

            if (!prod_desc) {
                console.error(`Variable 'prod_desc' in object with id ${objectId} has no value.`);
            }

            if (!prod_image) {
                console.error(`Variable 'prod_image' in object with id ${objectId} has no value.`);
            }

            if (!prod_info) {
                console.error(`Variable 'prod_info' in object with id ${objectId} has no value.`);
            }

            if (!prod_label) {
                console.error(`Variable 'prod_label' in object with id ${objectId} has no value.`);
            }

            if (!prod_name) {
                console.error(`Variable 'prod_name' in object with id ${objectId} has no value.`);
            }

            if (!qty_in_stock) {
                console.error(`Variable 'qty_in_stock' in object with id ${objectId} has no value.`);
            }

            if (!qty_threshold) {
                console.error(`Variable 'qty_threshold' in object with id ${objectId} has no value.`);
            }

            if (!unit) {
                console.error(`Variable 'unit' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (price && prod_desc && prod_image && prod_info && prod_label && prod_name && qty_in_stock && qty_threshold && unit) {
                dataObject = {
                    "price": price,
                    "prod_desc": prod_desc,
                    "prod_image": prod_image,
                    "prod_info": prod_info,
                    "prod_label": prod_label,
                    "prod_name": prod_name,
                    "qty_in_stock": qty_in_stock,
                    "qty_threshold": qty_threshold,
                    "unit": unit
                };
            }
            functionForProject_elisto(objectId, dataObject, "newproduct", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_newcategory',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const hierarchy_1 = $(this).data("hierarchy_1");
            const hierarchy_2 = $(this).data("hierarchy_2");
            const hierarchy_3 = $(this).data("hierarchy_3");
            const hierarchy_4 = $(this).data("hierarchy_4");
            const hierarchy_5 = $(this).data("hierarchy_5");
            const idproductcategory = $(this).data("idproductcategory");
            const level = $(this).data("level");
            const position = $(this).data("position");
            const prodcatdesc = $(this).data("prodcatdesc");
            const prodcatdesc_locale = $(this).data("prodcatdesc_locale");
            const prodcatimage = $(this).data("prodcatimage");
            const prodcatname = $(this).data("prodcatname");
            const prodcatname_locale = $(this).data("prodcatname_locale");

            if (!hierarchy_1) {
                console.error(`Variable 'hierarchy_1' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_2) {
                console.error(`Variable 'hierarchy_2' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_3) {
                console.error(`Variable 'hierarchy_3' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_4) {
                console.error(`Variable 'hierarchy_4' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_5) {
                console.error(`Variable 'hierarchy_5' in object with id ${objectId} has no value.`);
            }

            if (!idproductcategory) {
                console.error(`Variable 'idproductcategory' in object with id ${objectId} has no value.`);
            }

            if (!level) {
                console.error(`Variable 'level' in object with id ${objectId} has no value.`);
            }

            if (!position) {
                console.error(`Variable 'position' in object with id ${objectId} has no value.`);
            }

            if (!prodcatdesc) {
                console.error(`Variable 'prodcatdesc' in object with id ${objectId} has no value.`);
            }

            if (!prodcatdesc_locale) {
                console.error(`Variable 'prodcatdesc_locale' in object with id ${objectId} has no value.`);
            }

            if (!prodcatimage) {
                console.error(`Variable 'prodcatimage' in object with id ${objectId} has no value.`);
            }

            if (!prodcatname) {
                console.error(`Variable 'prodcatname' in object with id ${objectId} has no value.`);
            }

            if (!prodcatname_locale) {
                console.error(`Variable 'prodcatname_locale' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (hierarchy_1 && hierarchy_2 && hierarchy_3 && hierarchy_4 && hierarchy_5 && idproductcategory && level && position && prodcatdesc && prodcatdesc_locale && prodcatimage && prodcatname && prodcatname_locale) {
                dataObject = {
                    "hierarchy_1": hierarchy_1,
                    "hierarchy_2": hierarchy_2,
                    "hierarchy_3": hierarchy_3,
                    "hierarchy_4": hierarchy_4,
                    "hierarchy_5": hierarchy_5,
                    "idproductcategory": idproductcategory,
                    "level": level,
                    "position": position,
                    "prodcatdesc": prodcatdesc,
                    "prodcatdesc_locale": prodcatdesc_locale,
                    "prodcatimage": prodcatimage,
                    "prodcatname": prodcatname,
                    "prodcatname_locale": prodcatname_locale
                };
            }
            functionForProject_elisto(objectId, dataObject, "newcategory", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_newstaff',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const email = $(this).data("email");
            const first_name = $(this).data("first_name");
            const is_active = $(this).data("is_active");
            const is_superuser = $(this).data("is_superuser");
            const last_name = $(this).data("last_name");
            const password = $(this).data("password");
            const username = $(this).data("username");

            if (!email) {
                console.error(`Variable 'email' in object with id ${objectId} has no value.`);
            }

            if (!first_name) {
                console.error(`Variable 'first_name' in object with id ${objectId} has no value.`);
            }

            if (!is_active) {
                console.error(`Variable 'is_active' in object with id ${objectId} has no value.`);
            }

            if (!is_superuser) {
                console.error(`Variable 'is_superuser' in object with id ${objectId} has no value.`);
            }

            if (!last_name) {
                console.error(`Variable 'last_name' in object with id ${objectId} has no value.`);
            }

            if (!password) {
                console.error(`Variable 'password' in object with id ${objectId} has no value.`);
            }

            if (!username) {
                console.error(`Variable 'username' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (email && first_name && is_active && is_superuser && last_name && password && username) {
                dataObject = {
                    "email": email,
                    "first_name": first_name,
                    "is_active": is_active,
                    "is_superuser": is_superuser,
                    "last_name": last_name,
                    "password": password,
                    "username": username
                };
            }
            functionForProject_elisto(objectId, dataObject, "newstaff", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_deletestaff',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const USERIDVALUE = $(this).data("USERIDVALUE");

            if (!USERIDVALUE) {
                console.error(`Variable 'USERIDVALUE' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (USERIDVALUE) {
                dataObject = {
                    "USERIDVALUE": USERIDVALUE
                };
            }
            functionForProject_elisto(objectId, dataObject, "deletestaff", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_assignrole',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const roleid = $(this).data("roleid");
            const userid = $(this).data("userid");

            if (!roleid) {
                console.error(`Variable 'roleid' in object with id ${objectId} has no value.`);
            }

            if (!userid) {
                console.error(`Variable 'userid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (roleid && userid) {
                dataObject = {
                    "roleid": roleid,
                    "userid": userid
                };
            }
            functionForProject_elisto(objectId, dataObject, "assignrole", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_revokerole',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const roleid = $(this).data("roleid");
            const userid = $(this).data("userid");

            if (!roleid) {
                console.error(`Variable 'roleid' in object with id ${objectId} has no value.`);
            }

            if (!userid) {
                console.error(`Variable 'userid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (roleid && userid) {
                dataObject = {
                    "roleid": roleid,
                    "userid": userid
                };
            }
            functionForProject_elisto(objectId, dataObject, "revokerole", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_setlayoutid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "setlayoutid", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_layoutstructures',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "layoutstructures", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_layoutimages',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "layoutimages", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_layoutcolors',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "layoutcolors", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_updatelayout',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutelementname = $(this).data("layoutelementname");
            const layoutid = $(this).data("layoutid");
            const value = $(this).data("value");

            if (!layoutelementname) {
                console.error(`Variable 'layoutelementname' in object with id ${objectId} has no value.`);
            }

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            if (!value) {
                console.error(`Variable 'value' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutelementname && layoutid && value) {
                dataObject = {
                    "layoutelementname": layoutelementname,
                    "layoutid": layoutid,
                    "value": value
                };
            }
            functionForProject_elisto(objectId, dataObject, "updatelayout", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editdepartment',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const cnt_participants = $(this).data("cnt_participants");
            const description = $(this).data("description");
            const description_locale = $(this).data("description_locale");
            const iddepartment = $(this).data("iddepartment");
            const isonlyone = $(this).data("isonlyone");

            if (!cnt_participants) {
                console.error(`Variable 'cnt_participants' in object with id ${objectId} has no value.`);
            }

            if (!description) {
                console.error(`Variable 'description' in object with id ${objectId} has no value.`);
            }

            if (!description_locale) {
                console.error(`Variable 'description_locale' in object with id ${objectId} has no value.`);
            }

            if (!iddepartment) {
                console.error(`Variable 'iddepartment' in object with id ${objectId} has no value.`);
            }

            if (!isonlyone) {
                console.error(`Variable 'isonlyone' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (cnt_participants && description && description_locale && iddepartment && isonlyone) {
                dataObject = {
                    "cnt_participants": cnt_participants,
                    "description": description,
                    "description_locale": description_locale,
                    "iddepartment": iddepartment,
                    "isonlyone": isonlyone
                };
            }
            functionForProject_elisto(objectId, dataObject, "editdepartment", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_createdepartment',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const cnt_participants = $(this).data("cnt_participants");
            const description = $(this).data("description");
            const description_locale = $(this).data("description_locale");
            const isonlyone = $(this).data("isonlyone");

            if (!cnt_participants) {
                console.error(`Variable 'cnt_participants' in object with id ${objectId} has no value.`);
            }

            if (!description) {
                console.error(`Variable 'description' in object with id ${objectId} has no value.`);
            }

            if (!description_locale) {
                console.error(`Variable 'description_locale' in object with id ${objectId} has no value.`);
            }

            if (!isonlyone) {
                console.error(`Variable 'isonlyone' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (cnt_participants && description && description_locale && isonlyone) {
                dataObject = {
                    "cnt_participants": cnt_participants,
                    "description": description,
                    "description_locale": description_locale,
                    "isonlyone": isonlyone
                };
            }
            functionForProject_elisto(objectId, dataObject, "createdepartment", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_deletedepartment',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const iddepartment = $(this).data("iddepartment");

            if (!iddepartment) {
                console.error(`Variable 'iddepartment' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (iddepartment) {
                dataObject = {
                    "iddepartment": iddepartment
                };
            }
            functionForProject_elisto(objectId, dataObject, "deletedepartment", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editprofile',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const email = $(this).data("email");
            const first_name = $(this).data("first_name");
            const iduser = $(this).data("iduser");
            const image = $(this).data("image");
            const is_active = $(this).data("is_active");
            const is_staff = $(this).data("is_staff");
            const last_name = $(this).data("last_name");
            const username = $(this).data("username");

            if (!email) {
                console.error(`Variable 'email' in object with id ${objectId} has no value.`);
            }

            if (!first_name) {
                console.error(`Variable 'first_name' in object with id ${objectId} has no value.`);
            }

            if (!iduser) {
                console.error(`Variable 'iduser' in object with id ${objectId} has no value.`);
            }

            if (!image) {
                console.error(`Variable 'image' in object with id ${objectId} has no value.`);
            }

            if (!is_active) {
                console.error(`Variable 'is_active' in object with id ${objectId} has no value.`);
            }

            if (!is_staff) {
                console.error(`Variable 'is_staff' in object with id ${objectId} has no value.`);
            }

            if (!last_name) {
                console.error(`Variable 'last_name' in object with id ${objectId} has no value.`);
            }

            if (!username) {
                console.error(`Variable 'username' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (email && first_name && iduser && image && is_active && is_staff && last_name && username) {
                dataObject = {
                    "email": email,
                    "first_name": first_name,
                    "iduser": iduser,
                    "image": image,
                    "is_active": is_active,
                    "is_staff": is_staff,
                    "last_name": last_name,
                    "username": username
                };
            }
            functionForProject_elisto(objectId, dataObject, "editprofile", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editcategory',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const hierarchy_1 = $(this).data("hierarchy_1");
            const hierarchy_2 = $(this).data("hierarchy_2");
            const hierarchy_3 = $(this).data("hierarchy_3");
            const hierarchy_4 = $(this).data("hierarchy_4");
            const hierarchy_5 = $(this).data("hierarchy_5");
            const idproductcategory = $(this).data("idproductcategory");
            const idstyle = $(this).data("idstyle");
            const position = $(this).data("position");
            const prodcatdesc = $(this).data("prodcatdesc");
            const prodcatdesc_locale = $(this).data("prodcatdesc_locale");
            const prodcatimage = $(this).data("prodcatimage");
            const prodcatname = $(this).data("prodcatname");
            const prodcatname_locale = $(this).data("prodcatname_locale");

            if (!hierarchy_1) {
                console.error(`Variable 'hierarchy_1' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_2) {
                console.error(`Variable 'hierarchy_2' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_3) {
                console.error(`Variable 'hierarchy_3' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_4) {
                console.error(`Variable 'hierarchy_4' in object with id ${objectId} has no value.`);
            }

            if (!hierarchy_5) {
                console.error(`Variable 'hierarchy_5' in object with id ${objectId} has no value.`);
            }

            if (!idproductcategory) {
                console.error(`Variable 'idproductcategory' in object with id ${objectId} has no value.`);
            }

            if (!idstyle) {
                console.error(`Variable 'idstyle' in object with id ${objectId} has no value.`);
            }

            if (!position) {
                console.error(`Variable 'position' in object with id ${objectId} has no value.`);
            }

            if (!prodcatdesc) {
                console.error(`Variable 'prodcatdesc' in object with id ${objectId} has no value.`);
            }

            if (!prodcatdesc_locale) {
                console.error(`Variable 'prodcatdesc_locale' in object with id ${objectId} has no value.`);
            }

            if (!prodcatimage) {
                console.error(`Variable 'prodcatimage' in object with id ${objectId} has no value.`);
            }

            if (!prodcatname) {
                console.error(`Variable 'prodcatname' in object with id ${objectId} has no value.`);
            }

            if (!prodcatname_locale) {
                console.error(`Variable 'prodcatname_locale' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (hierarchy_1 && hierarchy_2 && hierarchy_3 && hierarchy_4 && hierarchy_5 && idproductcategory && idstyle && position && prodcatdesc && prodcatdesc_locale && prodcatimage && prodcatname && prodcatname_locale) {
                dataObject = {
                    "hierarchy_1": hierarchy_1,
                    "hierarchy_2": hierarchy_2,
                    "hierarchy_3": hierarchy_3,
                    "hierarchy_4": hierarchy_4,
                    "hierarchy_5": hierarchy_5,
                    "idproductcategory": idproductcategory,
                    "idstyle": idstyle,
                    "position": position,
                    "prodcatdesc": prodcatdesc,
                    "prodcatdesc_locale": prodcatdesc_locale,
                    "prodcatimage": prodcatimage,
                    "prodcatname": prodcatname,
                    "prodcatname_locale": prodcatname_locale
                };
            }
            functionForProject_elisto(objectId, dataObject, "editcategory", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editproduct',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idlabel = $(this).data("idlabel");
            const idprodstyle = $(this).data("idprodstyle");
            const idproduct = $(this).data("idproduct");
            const image = $(this).data("image");
            const image_thumb = $(this).data("image_thumb");
            const position = $(this).data("position");
            const price = $(this).data("price");
            const proddesc_locale = $(this).data("proddesc_locale");
            const prodinfo_locale = $(this).data("prodinfo_locale");
            const prodname = $(this).data("prodname");
            const prodname_locale = $(this).data("prodname_locale");
            const qty_in_stock = $(this).data("qty_in_stock");
            const qty_unit = $(this).data("qty_unit");
            const threshold_qty = $(this).data("threshold_qty");

            if (!idlabel) {
                console.error(`Variable 'idlabel' in object with id ${objectId} has no value.`);
            }

            if (!idprodstyle) {
                console.error(`Variable 'idprodstyle' in object with id ${objectId} has no value.`);
            }

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            if (!image) {
                console.error(`Variable 'image' in object with id ${objectId} has no value.`);
            }

            if (!image_thumb) {
                console.error(`Variable 'image_thumb' in object with id ${objectId} has no value.`);
            }

            if (!position) {
                console.error(`Variable 'position' in object with id ${objectId} has no value.`);
            }

            if (!price) {
                console.error(`Variable 'price' in object with id ${objectId} has no value.`);
            }

            if (!proddesc_locale) {
                console.error(`Variable 'proddesc_locale' in object with id ${objectId} has no value.`);
            }

            if (!prodinfo_locale) {
                console.error(`Variable 'prodinfo_locale' in object with id ${objectId} has no value.`);
            }

            if (!prodname) {
                console.error(`Variable 'prodname' in object with id ${objectId} has no value.`);
            }

            if (!prodname_locale) {
                console.error(`Variable 'prodname_locale' in object with id ${objectId} has no value.`);
            }

            if (!qty_in_stock) {
                console.error(`Variable 'qty_in_stock' in object with id ${objectId} has no value.`);
            }

            if (!qty_unit) {
                console.error(`Variable 'qty_unit' in object with id ${objectId} has no value.`);
            }

            if (!threshold_qty) {
                console.error(`Variable 'threshold_qty' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idlabel && idprodstyle && idproduct && image && image_thumb && position && price && proddesc_locale && prodinfo_locale && prodname && prodname_locale && qty_in_stock && qty_unit && threshold_qty) {
                dataObject = {
                    "idlabel": idlabel,
                    "idprodstyle": idprodstyle,
                    "idproduct": idproduct,
                    "image": image,
                    "image_thumb": image_thumb,
                    "position": position,
                    "price": price,
                    "proddesc_locale": proddesc_locale,
                    "prodinfo_locale": prodinfo_locale,
                    "prodname": prodname,
                    "prodname_locale": prodname_locale,
                    "qty_in_stock": qty_in_stock,
                    "qty_unit": qty_unit,
                    "threshold_qty": threshold_qty
                };
            }
            functionForProject_elisto(objectId, dataObject, "editproduct", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_setproductcategory',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const ididhierarchy = $(this).data("ididhierarchy");
            const idproduct = $(this).data("idproduct");

            if (!ididhierarchy) {
                console.error(`Variable 'ididhierarchy' in object with id ${objectId} has no value.`);
            }

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (ididhierarchy && idproduct) {
                dataObject = {
                    "ididhierarchy": ididhierarchy,
                    "idproduct": idproduct
                };
            }
            functionForProject_elisto(objectId, dataObject, "setproductcategory", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_unsetproductcategory',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const ididhierarchy = $(this).data("ididhierarchy");
            const idproduct = $(this).data("idproduct");

            if (!ididhierarchy) {
                console.error(`Variable 'ididhierarchy' in object with id ${objectId} has no value.`);
            }

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (ididhierarchy && idproduct) {
                dataObject = {
                    "ididhierarchy": ididhierarchy,
                    "idproduct": idproduct
                };
            }
            functionForProject_elisto(objectId, dataObject, "unsetproductcategory", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_admindash',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "admindash", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_orders',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "orders", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_defineattribute',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const attribute_type = $(this).data("attribute_type");
            const attributemnemonic = $(this).data("attributemnemonic");
            const productattributename_locale = $(this).data("productattributename_locale");

            if (!attribute_type) {
                console.error(`Variable 'attribute_type' in object with id ${objectId} has no value.`);
            }

            if (!attributemnemonic) {
                console.error(`Variable 'attributemnemonic' in object with id ${objectId} has no value.`);
            }

            if (!productattributename_locale) {
                console.error(`Variable 'productattributename_locale' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (attribute_type && attributemnemonic && productattributename_locale) {
                dataObject = {
                    "attribute_type": attribute_type,
                    "attributemnemonic": attributemnemonic,
                    "productattributename_locale": productattributename_locale
                };
            }
            functionForProject_elisto(objectId, dataObject, "defineattribute", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_setattribute',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idattribute = $(this).data("idattribute");
            const idproduct = $(this).data("idproduct");
            const productattributevalue_locale = $(this).data("productattributevalue_locale");

            if (!idattribute) {
                console.error(`Variable 'idattribute' in object with id ${objectId} has no value.`);
            }

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            if (!productattributevalue_locale) {
                console.error(`Variable 'productattributevalue_locale' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idattribute && idproduct && productattributevalue_locale) {
                dataObject = {
                    "idattribute": idattribute,
                    "idproduct": idproduct,
                    "productattributevalue_locale": productattributevalue_locale
                };
            }
            functionForProject_elisto(objectId, dataObject, "setattribute", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_unsetattribute',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idattribute = $(this).data("idattribute");
            const idproduct = $(this).data("idproduct");

            if (!idattribute) {
                console.error(`Variable 'idattribute' in object with id ${objectId} has no value.`);
            }

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idattribute && idproduct) {
                dataObject = {
                    "idattribute": idattribute,
                    "idproduct": idproduct
                };
            }
            functionForProject_elisto(objectId, dataObject, "unsetattribute", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_deleteattributedefinition',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idproductattributename = $(this).data("idproductattributename");

            if (!idproductattributename) {
                console.error(`Variable 'idproductattributename' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idproductattributename) {
                dataObject = {
                    "idproductattributename": idproductattributename
                };
            }
            functionForProject_elisto(objectId, dataObject, "deleteattributedefinition", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_insertproductimage',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idproduct = $(this).data("idproduct");
            const imagepath = $(this).data("imagepath");

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            if (!imagepath) {
                console.error(`Variable 'imagepath' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idproduct && imagepath) {
                dataObject = {
                    "idproduct": idproduct,
                    "imagepath": imagepath
                };
            }
            functionForProject_elisto(objectId, dataObject, "insertproductimage", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_deleteproductimage',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idlnk_product_image = $(this).data("idlnk_product_image");

            if (!idlnk_product_image) {
                console.error(`Variable 'idlnk_product_image' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idlnk_product_image) {
                dataObject = {
                    "idlnk_product_image": idlnk_product_image
                };
            }
            functionForProject_elisto(objectId, dataObject, "deleteproductimage", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_newText',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const id = $(this).data("id");
            const mnemonic = $(this).data("mnemonic");
            const text_locale = $(this).data("text_locale");

            if (!id) {
                console.error(`Variable 'id' in object with id ${objectId} has no value.`);
            }

            if (!mnemonic) {
                console.error(`Variable 'mnemonic' in object with id ${objectId} has no value.`);
            }

            if (!text_locale) {
                console.error(`Variable 'text_locale' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (id && mnemonic && text_locale) {
                dataObject = {
                    "id": id,
                    "mnemonic": mnemonic,
                    "text_locale": text_locale
                };
            }
            functionForProject_elisto(objectId, dataObject, "newText", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editText',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const id = $(this).data("id");
            const mnemonic = $(this).data("mnemonic");
            const text_locale = $(this).data("text_locale");

            if (!id) {
                console.error(`Variable 'id' in object with id ${objectId} has no value.`);
            }

            if (!mnemonic) {
                console.error(`Variable 'mnemonic' in object with id ${objectId} has no value.`);
            }

            if (!text_locale) {
                console.error(`Variable 'text_locale' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (id && mnemonic && text_locale) {
                dataObject = {
                    "id": id,
                    "mnemonic": mnemonic,
                    "text_locale": text_locale
                };
            }
            functionForProject_elisto(objectId, dataObject, "editText", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_userprofiles',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "userprofiles", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_userroles',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "userroles", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_getuserroles',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const userid = $(this).data("userid");

            if (!userid) {
                console.error(`Variable 'userid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (userid) {
                dataObject = {
                    "userid": userid
                };
            }
            functionForProject_elisto(objectId, dataObject, "getuserroles", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_getroleowners',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const roleid = $(this).data("roleid");

            if (!roleid) {
                console.error(`Variable 'roleid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (roleid) {
                dataObject = {
                    "roleid": roleid
                };
            }
            functionForProject_elisto(objectId, dataObject, "getroleowners", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_layoutfonts',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "layoutfonts", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_layouttexts',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "layouttexts", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_deleteproduct',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idproduct = $(this).data("idproduct");

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idproduct) {
                dataObject = {
                    "idproduct": idproduct
                };
            }
            functionForProject_elisto(objectId, dataObject, "deleteproduct", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_layoutelements',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "layoutelements", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_linkcategory',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idcategory = $(this).data("idcategory");
            const idparentcategory = $(this).data("idparentcategory");

            if (!idcategory) {
                console.error(`Variable 'idcategory' in object with id ${objectId} has no value.`);
            }

            if (!idparentcategory) {
                console.error(`Variable 'idparentcategory' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idcategory && idparentcategory) {
                dataObject = {
                    "idcategory": idcategory,
                    "idparentcategory": idparentcategory
                };
            }
            functionForProject_elisto(objectId, dataObject, "linkcategory", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_deletecategory',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idcategory = $(this).data("idcategory");

            if (!idcategory) {
                console.error(`Variable 'idcategory' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idcategory) {
                dataObject = {
                    "idcategory": idcategory
                };
            }
            functionForProject_elisto(objectId, dataObject, "deletecategory", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editattribute',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idattribute = $(this).data("idattribute");
            const idproduct = $(this).data("idproduct");
            const value = $(this).data("value");

            if (!idattribute) {
                console.error(`Variable 'idattribute' in object with id ${objectId} has no value.`);
            }

            if (!idproduct) {
                console.error(`Variable 'idproduct' in object with id ${objectId} has no value.`);
            }

            if (!value) {
                console.error(`Variable 'value' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idattribute && idproduct && value) {
                dataObject = {
                    "idattribute": idattribute,
                    "idproduct": idproduct,
                    "value": value
                };
            }
            functionForProject_elisto(objectId, dataObject, "editattribute", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_editattributename',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const attributename = $(this).data("attributename");
            const idproductattributename = $(this).data("idproductattributename");
            const mnemonic = $(this).innerHTML;

            if (!attributename) {
                console.error(`Variable 'attributename' in object with id ${objectId} has no value.`);
            }

            if (!idproductattributename) {
                console.error(`Variable 'idproductattributename' in object with id ${objectId} has no value.`);
            }

            if (!mnemonic) {
                console.error(`Variable 'mnemonic' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (attributename && idproductattributename && mnemonic) {
                dataObject = {
                    "attributename": attributename,
                    "idproductattributename": idproductattributename,
                    "mnemonic": mnemonic
                };
            }
            functionForProject_elisto(objectId, dataObject, "editattributename", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_updatecss',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "updatecss", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_getcss',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const layoutid = $(this).data("layoutid");

            if (!layoutid) {
                console.error(`Variable 'layoutid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (layoutid) {
                dataObject = {
                    "layoutid": layoutid
                };
            }
            functionForProject_elisto(objectId, dataObject, "getcss", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_tickets',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const customerid = $(this).data("customerid");

            if (!customerid) {
                console.error(`Variable 'customerid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (customerid) {
                dataObject = {
                    "customerid": customerid
                };
            }
            functionForProject_elisto(objectId, dataObject, "tickets", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_createticket',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const customerid = $(this).data("customerid");
            const ticketdefid = $(this).data("ticketdefid");

            if (!customerid) {
                console.error(`Variable 'customerid' in object with id ${objectId} has no value.`);
            }

            if (!ticketdefid) {
                console.error(`Variable 'ticketdefid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (customerid && ticketdefid) {
                dataObject = {
                    "customerid": customerid,
                    "ticketdefid": ticketdefid
                };
            }
            functionForProject_elisto(objectId, dataObject, "createticket", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_addticketdata',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const attachedfile = $(this).data("attachedfile");
            const customertext = $(this).data("customertext");
            const ticketid = $(this).data("ticketid");

            if (!attachedfile) {
                console.error(`Variable 'attachedfile' in object with id ${objectId} has no value.`);
            }

            if (!customertext) {
                console.error(`Variable 'customertext' in object with id ${objectId} has no value.`);
            }

            if (!ticketid) {
                console.error(`Variable 'ticketid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (attachedfile && customertext && ticketid) {
                dataObject = {
                    "attachedfile": attachedfile,
                    "customertext": customertext,
                    "ticketid": ticketid
                };
            }
            functionForProject_elisto(objectId, dataObject, "addticketdata", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_ticketdata',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const ticketid = $(this).data("ticketid");

            if (!ticketid) {
                console.error(`Variable 'ticketid' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (ticketid) {
                dataObject = {
                    "ticketid": ticketid
                };
            }
            functionForProject_elisto(objectId, dataObject, "ticketdata", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_ticketdef',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "ticketdef", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_showcart',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const invoicenumber = $(this).data("invoicenumber");

            if (!invoicenumber) {
                console.error(`Variable 'invoicenumber' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (invoicenumber) {
                dataObject = {
                    "invoicenumber": invoicenumber
                };
            }
            functionForProject_elisto(objectId, dataObject, "showcart", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_customerid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "customerid", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_changeuserpassword',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const iduser = $(this).data("iduser");
            const password = $(this).data("password");

            if (!iduser) {
                console.error(`Variable 'iduser' in object with id ${objectId} has no value.`);
            }

            if (!password) {
                console.error(`Variable 'password' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (iduser && password) {
                dataObject = {
                    "iduser": iduser,
                    "password": password
                };
            }
            functionForProject_elisto(objectId, dataObject, "changeuserpassword", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_storetext',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idsemantic = $(this).data("idsemantic");
            const stringvalue = $(this).data("stringvalue");

            if (!idsemantic) {
                console.error(`Variable 'idsemantic' in object with id ${objectId} has no value.`);
            }

            if (!stringvalue) {
                console.error(`Variable 'stringvalue' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idsemantic && stringvalue) {
                dataObject = {
                    "idsemantic": idsemantic,
                    "stringvalue": stringvalue
                };
            }
            functionForProject_elisto(objectId, dataObject, "storetext", "put");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_updatetext',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const idsemantic = $(this).data("idsemantic");
            const stringvalue = $(this).data("stringvalue");

            if (!idsemantic) {
                console.error(`Variable 'idsemantic' in object with id ${objectId} has no value.`);
            }

            if (!stringvalue) {
                console.error(`Variable 'stringvalue' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (idsemantic && stringvalue) {
                dataObject = {
                    "idsemantic": idsemantic,
                    "stringvalue": stringvalue
                };
            }
            functionForProject_elisto(objectId, dataObject, "updatetext", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_setcustomerpassword',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const setting = $(this).data("setting");
            const val = $(this).data("val");

            if (!setting) {
                console.error(`Variable 'setting' in object with id ${objectId} has no value.`);
            }

            if (!val) {
                console.error(`Variable 'val' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (setting && val) {
                dataObject = {
                    "setting": setting,
                    "val": val
                };
            }
            functionForProject_elisto(objectId, dataObject, "setcustomerpassword", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_setcustomerdepartment',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const iddepartment = $(this).data("iddepartment");
            const iduser = $(this).data("iduser");

            if (!iddepartment) {
                console.error(`Variable 'iddepartment' in object with id ${objectId} has no value.`);
            }

            if (!iduser) {
                console.error(`Variable 'iduser' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (iddepartment && iduser) {
                dataObject = {
                    "iddepartment": iddepartment,
                    "iduser": iduser
                };
            }
            functionForProject_elisto(objectId, dataObject, "setcustomerdepartment", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_showCredits',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "showCredits", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_adminuserdata',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }


            let dataObject;
            functionForProject_elisto(objectId, dataObject, "adminuserdata", "post");
        }
    );

$(document)
    .on(
        'keyDown',
        'keyDownEventTriggerFor_setstripeid',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            const stripe_user_account = $(this).data("stripe_user_account");

            if (!stripe_user_account) {
                console.error(`Variable 'stripe_user_account' in object with id ${objectId} has no value.`);
            }

            let dataObject;
            if (stripe_user_account) {
                dataObject = {
                    "stripe_user_account": stripe_user_account
                };
            }
            functionForProject_elisto(objectId, dataObject, "setstripeid", "post");
        }
    );

