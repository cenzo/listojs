"use strict";

function blink(element,duration,repeat){
    for (let r=0;r<repeat;r++) {
        $(`#${element}`).fadeIn(duration);
        $(`#${element}`).fadeOut(duration);
    }
    $(`#${element}`).fadeIn(2*duration);
};

function showUrls() {
    const shortDuration = 100;
    const longDuration = 250;
    $("#proposalsTableID").attr("style", "visibility: visible;");
    $("#show_select_html").attr("style", "visibility: visible;");
    $("#show_customize_html").attr("style", "visibility: visible;");
    blink("proposalsTableID",shortDuration,3);
    blink("text3",shortDuration,2);
    blink("create_personalized_shorturl",longDuration,5);
};

const functionForProject_dovisto = function (objectId, dataObject, path, method) {
    const checkUrl = new RegExp(/^(ftp|http|https):\/\/[^ "]+$/);
    function hideUrls(){
        $("#proposalsTableID").attr("style", "visibility: hidden;");
        $("#show_select_html").attr("style", "visibility: hidden;");
        $("#show_customize_html").attr("style", "visibility: hidden;");
        $("#longUrlText").attr("style", "visibility: hidden;");
    }
    function createProposals(data) {
        const longUrl = $("#longurl_input").val();
        let longUrlDisplayed;
        const len = longUrl.length;
        if (len>40){
            longUrlDisplayed=`${longUrl.substring(0,25)}&nbsp;&nbsp;.&nbsp;.&nbsp;.&nbsp;.&nbsp;.&nbsp;.&nbsp;&nbsp;${longUrl.substring(len-5,len)}`;
        }else {
            longUrlDisplayed=longUrl;
        }
        $("#longUrlText").html(`<i class="fas fa-arrow-right"></i>&nbsp;&nbsp;${longUrlDisplayed}<br/>(${len}&nbsp;${getTextById(9)})`);
        $("#longUrlText").attr("style", "visibility: visible;");
        let tableHtml = `${TableStyle.tableHeadOpen}`;
        for (let i = 0; i < 3; i++) {
            let shorturlProposal = data.rows[i].s[1];
            tableHtml += `<tr><td><button type="submit" class="btn btn-block btn-lg btn-primary click_propose"  id="click_propose_${shorturlProposal}">&nbsp;
                            <i class="fas fa-link"></i>  dovis.it/${shorturlProposal}</i></button></td></tr>`;
        }
        return `${tableHtml}${TableStyle.bottom}`;
    }

    if (!dataObject) {
        console.error("Cannot send apiCall1 with null dataobject.");
    } else if (!objectId) {
        console.error("Null object-id for apiCall1.");
    } else {
        let eventCallBack;
        switch (objectId) {
            case "createsubmit":
                eventCallBack = function (data) {
                    hideUrls();
                    const longUrl = $("#longurl_input").val();
                    if (longUrl.length < 1) {
                        $("#validationlabel").text(getTextById(7));
                        $("#validationlabel").show();
                    }
                    else if (!checkUrl.test(longUrl)){
                        $("#validationlabel").text(getTextById(8));
                        $("#validationlabel").show();
                    }
                    else{
                        $("#validationlabel").hide();
                        console.log("data: " + JSON.stringify(data));
                        const proposalHtml = createProposals(data);
                        $("#proposalsTableID").html(proposalHtml);
                        showUrls();
                    }
                };
                break;
            case "input1":
                //TODO
                eventCallBack = function (dataObject) {
                    const rml = {
                        attribute1: dataObject.attribute1,
                        attribute2: $("#input1").val()
                    };
                    dataObject = rml;
                    console.log("Triggered by Object 'input1' with following data: " + JSON.stringify(dataObject));
                    $("#textarea1").text(dataObject.attribute1 + "-" + dataObject.attribute2);
                };
                break;
            default:
                //TODO
                eventCallBack = function (dataObject) {
                    console.log("DEFAULT trigger with following data: " + JSON.stringify(dataObject));
                    $("#butt1").text(objectId);
                };
                console.log(`Object ${objectId}  is calling api`);
        }
        apiCall(8, "/8/public/" + path, dataObject, eventCallBack, function (err) {
            showHttpErrorMessage("errormessage", err);
        }, method);
    }
};

// Example usage of createTable.call:
/* const action1 = [
      "click_propose",
      "<i class=\"fas fa-link\"></i>"
      ];
  const columnNames = ["shortcutproposal"];
  const columnIndicesForDataAttr = [0];

  console.log("Triggered by object 'createsubmit' with following data: " + JSON.stringify(dataObject));
  const proposalsTableObject = {
      IDENTIFIER: "proposal-data",
      jsonRMLData: data,
      header: ['short Url', ''],
      columnIndices: [1],
      actions: [action1],
      dataColumnIndices: [columnIndicesForDataAttr],
      dataColumnNames: [columnNames]
  };
  listoHTML.createTable.call(proposalsTableObject, "proposalsTableID", false);*/

