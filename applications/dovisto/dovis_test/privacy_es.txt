Política de privacidad
1. Introducción

En Enpasoft GmbH , que ejerce su actividad como (en adelante, «Empresa», incluidas las referencias «nosotros», «nuestro» o «nos»), valoramos su privacidad y la importancia de salvaguardar sus datos. La Política de privacidad (en adelante, «Política») describe nuestras prácticas en relación con las actividades establecidas a continuación. En cuanto a sus derechos, le informamos sobre cómo recopilamos, almacenamos, accedemos y procesamos la información de las personas. En esta Política, el término «Datos personales» hace referencia a toda la información que —por sí sola o en combinación con otra información disponible— pueda identificar a una persona.

Estamos comprometidos a proteger su privacidad de acuerdo con el más alto nivel de regulación. Por esta razón, cumplimos con las obligaciones estipuladas en los siguientes reglamentos:

    Reglamento General de Protección de Datos de la UE (RGPD);

Alcance

Esta Política se aplica a cualquier sitio web, dominio, aplicación, servicio o producto de Enpasoft GmbH.

Esta Política no se aplica a aplicaciones, sitios web, productos, servicios o plataformas de terceros a los que se pueda acceder a través de enlaces (que no pertenezcan a ) que podamos proporcionarle. Nosotros no poseemos ni dirigimos estos sitios, por lo que tienen sus propias prácticas de privacidad y recopilación de datos. Todos los Datos personales que usted proporcione a estos sitios web se regirán por la propia política de privacidad del tercero. No podemos aceptar ninguna responsabilidad respecto a las acciones o políticas de sitios independientes, ni tampoco somos responsables del contenido o las prácticas en materia de privacidad de dichos sitios.
Actividades de tratamiento

Esta Política se aplica cuando usted interactúa con nosotros al hacer lo siguiente:

    Utilizar nuestra aplicación y servicios como un usuario autorizado.
    Visitar cualquiera de nuestros sitios web sujetos a esta Política.
    Recibir cualquier comunicación de nosotros, como pueden ser los boletines, correos electrónicos, llamadas o mensajes de texto.

2. Datos personales que recopilamos
Qué Datos personales recopilamos

Cuando usted realiza una compra o intenta realizarla, nosotros recopilamos los siguientes tipos de Datos personales:

Tales datos incluyen:

    Información sobre su cuenta como su nombre, dirección de correo electrónico y contraseña
    Datos de pago como su dirección de facturación, número de teléfono, tarjeta de crédito, tarjeta de débito u otro método de pago

Cuando usted utiliza nuestros productos y/o funciones, recopilamos los siguientes tipos de Datos personales:

    Información sobre su cuenta como su nombre, dirección de correo electrónico y contraseña
    Datos de pago como su dirección de facturación, número de teléfono, tarjeta de crédito, tarjeta de débito u otro método de pago
    Contenido (publicaciones, comentarios, audio o documentos)

Cómo recopilamos sus Datos personales

Recopilamos Datos personales de las siguientes fuentes:

De usted. Puede proporcionarnos la información sobre su cuenta, datos de pago, información financiera, datos demográficos, información de compra, contenido, comentarios e información de productos completando formularios, utilizando nuestros productos o servicios, introduciendo la información en línea o comunicándose con nosotros por correo postal, teléfono, correo electrónico o de otra manera. Esto incluye los Datos personales que proporciona, por ejemplo, al llevar a cabo los siguientes actos:

    crear una cuenta o comprar productos en nuestro sitio web;
    utilizar nuestros productos o servicios;
    crear contenido a través de nuestros productos o servicios;
    mostrar interés en nuestros productos o servicios;
    descargar software y/o nuestra aplicación móvil;
    suscribirse a nuestro boletín;
    completar una encuesta voluntaria de investigación de mercado;
    contactarnos para hacer una consulta o para informarnos de un problema (por teléfono, correo electrónico, redes sociales o servicio de mensajería);
    iniciar sesión en nuestro sitio web a través de las redes sociales.

Tecnologías o interacciones automatizadas: A medida que interactúa con nuestro sitio web, podríamos recopilar automáticamente los siguientes tipos de datos (todos los descritos anteriormente): datos del dispositivo sobre su equipo, datos de uso sobre sus acciones y patrones de navegación y datos de contacto cuando las tareas realizadas a través de nuestro sitio web permanezcan incompletas, tales como pedidos incompletos o cestas abandonadas. Recopilamos estos datos mediante el uso de cookies, registros del servidor y otras tecnologías similares. Consulte nuestra sección de cookies (a continuación) para obtener más detalles.

Terceros. Podríamos obtener sus Datos personales, incluida la siguiente información, de varios terceros:

    información de la cuenta y datos de pago de otra persona cuando compra un regalo para usted en nuestro sitio web;
    datos del dispositivo y de uso de terceros, incluidos los proveedores de analíticas como Google;
    información de la cuenta y datos de pago de las plataformas de redes sociales cuando inicia sesión en nuestro sitio web utilizando dichas plataformas de redes sociales;
    contenido de servicios de comunicación, incluidos los proveedores de correo electrónico y redes sociales, cuando nos da permiso para acceder a sus datos en dichos servicios o redes de terceros;
    información de la cuenta y datos de pago de terceros, incluidas las organizaciones (como organismos públicos), asociaciones y grupos que comparten datos con el fin de prevenir y detectar fraudes y reducir el riesgo crediticio;
    información de la cuenta, datos de pago e información financiera de proveedores de servicios técnicos, pagos y envíos.

Si usted nos proporciona a nosotros —o a nuestros proveedores de servicios— Datos personales que pertenezcan a otra persona, usted reconoce que tiene la autoridad para hacer tal cosa y acepta que se utilicen de acuerdo con esta Política. Si cree que hemos recibido sus Datos personales de forma indebida, o para ejercer sus derechos relacionados con sus Datos personales de cualquier otra forma, comuníquese con nosotros mediante la información que encontrará en la sección «Contacto» a continuación.
Datos del dispositivo y de uso

Cuando usted visita un sitio web de Enpasoft GmbH, nosotros recopilamos y almacenamos información sobre su visita de forma automática mediante las cookies de navegador (archivos que enviamos a su equipo), o mediante tecnología similar. Puede configurar su navegador para que rechace todas las cookies o para que indique cuando se envía una cookie. La función Ayuda en la mayoría de los navegadores proporciona información sobre cómo aceptar cookies, desactivarlas o avisarle al recibir una nueva cookie. Es posible que no pueda utilizar alguna de las funciones de nuestro Servicio si no acepta el uso de cookies, por lo que le recomendamos que las deje activadas.
Qué datos recopilamos de terceros

Es posible que obtengamos sus Datos personales de terceros, como, por ejemplo, de empresas suscritas a servicios de Enpasoft GmbH, nuestros socios y otras fuentes. Nosotros no recopilamos estos Datos personales, pero sí lo hace un tercero, por lo que tal acción está sujeta a las políticas de privacidad y de recopilación de datos de tal tercero. Nosotros no tenemos ningún control ni podemos regular cómo determinados terceros gestionan sus Datos personales. Como siempre, usted tiene derecho a revisar y rectificar esta información. Si tiene alguna pregunta, en primer lugar debe comunicarse con el tercero correspondiente para obtener más información sobre sus Datos personales. En caso de que el tercero no respete sus derechos, puede contactar con el Delegado de protección de datos de Enpasoft GmbH (encontrará los detalles de contacto a continuación).

Nuestros sitios web y servicios podrían incluir enlaces a otros sitios web, aplicaciones y servicios operados por terceros. Las prácticas en materia de información de dichos servicios o de las redes sociales que albergan algunas páginas de medios sociales de nuestra marca están sujetas a las políticas de privacidad de terceros, las cuales usted debe revisar para comprender mejor las prácticas en materia de privacidad de estos terceros.
Finalidad y fundamento legal para el procesamiento de datos personales

Recopilamos y utilizamos sus Datos personales con su consentimiento para proporcionar, mantener y desarrollar nuestros productos y servicios, y comprender cómo mejorarlos.

Esta finalidad incluye los siguientes propósitos:

    Entregarle su producto o servicio
    Realizar pedidos, incluido el envío electrónico y no electrónico
    Construir un entorno seguro y protegido
    Verificar o autentificar su identidad
    Investigar y prevenir incidentes de seguridad como infracciones, ataques y accesos ilegales
    Proporcionar, desarrollar y mejorar nuestros productos y servicios
    Entregar, mantener, depurar y mejorar nuestros productos y servicios
    Permitirle acceder a los servicios de Enpasoft GmbH y crear cuentas
    Proporcionarle asistencia técnica y atención al cliente
    Organizar y producir campañas de publicidad y marketing
    Enviarle boletines y otras comunicaciones de marketing sobre productos, programas, servicios, eventos, competiciones, encuestas y promociones, tanto actuales como futuros, realizados por nosotros o llevados a cabo en nuestro nombre
    Organizar eventos o inscribir participantes y programar reuniones para eventos
    Para investigación y desarrollo
    Para comunicarnos con usted acerca de los Productos y Servicios

Al procesar sus Datos personales para proporcionar un producto o servicio, lo hacemos porque es necesario para cumplir con las obligaciones contractuales. Todo el proceso mencionado con anterioridad es necesario para satisfacer nuestros intereses legítimos de proporcionar productos y servicios, además de mantener nuestra relación con usted y proteger nuestro negocio, por ejemplo, frente a fraudes. Se requerirá su consentimiento para que podamos comenzar a prestarle servicios. En caso de que se realice algún cambio en el tipo de datos recopilados, se volverá a requerir su consentimiento. Es posible que, si usted no nos proporciona su consentimiento en el marco de nuestro contrato, algunos servicios no estén disponibles para usted.
Transferencia y almacenamiento de datos a nivel internacional

Siempre que sea posible, almacenamos y procesamos datos en servidores dentro de la región geográfica general donde usted reside (Nota: Podría estar fuera del país en el que reside). Sus Datos personales también pueden transferirse y conservarse en servidores que se encuentran fuera de su estado, provincia, país u otra jurisdicción gubernamental donde la legislación relativa a la protección de datos podría diferir de la de su jurisdicción. Tomaremos las medidas adecuadas para garantizar que sus Datos personales se traten de forma segura y de acuerdo con esta Política, así como de acuerdo con la legislación aplicable en materia de protección de datos. Puede encontrar más información sobre estas cláusulas aquí: https://eur-lex.europa.eu/legal-content/en/TXT/?uri=CELEX%3A32021D0914

Intercambio y divulgación

Compartiremos sus Datos personales con terceros solo en las formas establecidas en esta Política o en el momento en que se recopilen los Datos personales.
Requerimiento legal

Puede que utilicemos o revelemos sus Datos personales para cumplir con una obligación legal relacionada con una solicitud de una autoridad pública o gubernamental, o procedimientos judiciales, para evitar muertes o lesiones, o para proteger nuestros derechos y propiedades. Cuando sea posible y práctico, le informaremos con antelación sobre dicha divulgación.
Proveedores de servicio y otros terceros

Puede que utilicemos un proveedor de servicios externo, contratistas independientes, agencias o asesores para ofrecer nuestros productos y servicios, y para que nos ayuden a mejorarlos. Puede que compartamos sus Datos personales con agencias de marketing, proveedores de servicios de bases de datos, copias de seguridad y recuperación en caso de catástrofe, proveedores del servicio de correo electrónico, entre otros tipos de proveedores, pero solo para mantener y mejorar nuestros productos y servicios. Si desea obtener más información sobre los destinatarios de sus Datos personales, comuníquese con nosotros utilizando los datos de contado que encontrará en el apartado «Contacto» a continuación.
3. Cookies
¿Qué son las cookies?

Una cookie es un pequeño archivo de texto que contiene información que su buscador almacena en su dispositivo. La información de este archivo se suele compartir con el propietario del sitio, así como con los socios potenciales y agentes externos a la empresa. La recopilación de esta información podría servir para garantizar el adecuado funcionamiento del sitio o para mejorar su experiencia.
Cómo usamos las cookies

    Para ofrecerle la mejor experiencia posible, utilizamos los siguientes tipos de cookies:
    Estrictamente necesarias. Como aplicación web, es necesario que utilicemos ciertas cookies para ofrecer nuestro servicio.
    De preferencias.
        Utilizamos cookies de preferencias para recordar de qué manera le gusta utilizar nuestro servicio.
        Algunas cookies se utilizan para personalizar el contenido y ofrecerle una experiencia a medida. Por ejemplo, se podría utilizar la ubicación para ofrecerle servicios y ofertas cerca de usted.
    De marketing. Compartimos cookies con anunciantes o socios de terceros para poder proporcionarle una experiencia de marketing personalizada.
    También permitimos que nuestros socios externos pongan sus propias cookies en nuestro sitio web.

Cómo gestionar sus cookies

Siempre y cuando la cookie no sea estrictamente necesaria, podrá optar por permitir o no su uso en cualquier momento. Para modificar la forma de la que obtenemos información sobre usted, visite nuestro Administrador de cookies.

4. Conservación y eliminación

Solo conservaremos sus Datos personales durante el tiempo que sea necesario para el fin con el que se recopilaron y en la medida en que lo exija la legislación aplicable. Cuando ya no necesitemos sus Datos personales, los eliminaremos de nuestro sistema y/o tomaremos medidas para convertirlos en anónimos.
5. Fusión o adquisición

En caso de involucrarnos en una fusión, adquisición o venta de activos, sus Datos personales podrían transferirse. Le enviaremos un aviso antes de transferir sus Datos personales y antes de que estén sujetos a una política de privacidad diferente. En determinadas circunstancias, es posible que se nos solicite que divulguemos sus Datos personales si así lo exige la ley o en respuesta a solicitudes válidas de las autoridades públicas (por ejemplo, un tribunal o una agencia gubernamental).
6. Cómo velamos por la seguridad de sus datos

Contamos con mecanismos de seguridad organizativos y medidas de seguridad apropiadas para proteger sus Datos personales de pérdidas, usos o accesos accidentales no autorizados, así como de su alteración o divulgación.

Entre su navegador y nuestro sitio web se establece una conexión encriptada y segura dondequiera que estén involucrados sus Datos personales.

Requerimos que cualquier tercero contratado para procesar sus Datos personales en nuestro nombre cuente con medidas de seguridad para proteger sus datos y tratarlos de acuerdo con la ley.

En el desafortunado caso de una infracción relacionada con sus Datos personales, se lo notificaremos a usted y al organismo regulador en cuestión cuando estemos jurídicamente obligados a hacerlo.
7. Privacidad infantil

No recopilamos a sabiendas Datos personales de niños menores de 16 años años.
8. Sus derechos relativos a sus Datos personales

Según su ubicación geográfica y su nacionalidad, sus derechos se rigen por los reglamentos locales en materia de privacidad de datos. Entre tales derechos se incluyen los siguientes:

    Derecho de acceso (PIPEDA, art. 15 RGPD, CCPA/CPRA, CPA, VCDPA, CTDPA, UCPA, LGPD, POPIA)
    Tiene derecho a saber si estamos procesando sus Datos personales y a solicitar una copia de los Datos personales que están siendo objeto de tal procesamiento.

    Derecho de rectificación (PIPEDA, art. 16 RGPD, CPRA, CPA, VCDPA, CTDPA, LGPD, POPIA)
    Tiene derecho a que se rectifiquen los Datos personales incompletos o inexactos que están siendo objeto de procesamiento por nuestra parte.

    Derecho al olvido (derecho a la eliminación) (art. 17 RGPD, CCPA/CPRA, CPA, VCDPA, CTDPA, UCPA, LGPD, POPIA)
    Tiene derecho a solicitar que eliminemos los Datos personales que están siendo objeto de procesamiento por nuestra parte, a menos que necesitemos conservar dichos datos para cumplir con una obligación legal o para la formulación, el ejercicio o la defensa de demandas judiciales.

    Derecho a la limitación del procesamiento (art. 18 RGPD, LGPD)
    Tiene derecho a restringir el procesamiento de sus Datos personales por nuestra parte en determinadas circunstancias. En este caso, no procesaremos sus Datos con ningún otro propósito que no sea el de su almacenamiento.

    Derecho a la portabilidad (PIPEDA, art. 20 RGPD, LGPD)
    Tiene derecho a obtener los Datos personales que almacenamos sobre usted en formato estructurado y electrónico, y a transferirlos a otro responsable de procesamiento de datos, cuando se trate de (a) Datos personales que nos haya proporcionado y (b) si nuestro procesamiento está basado en su consentimiento o tiene como propósito cumplir una obligación contractual con usted o con el tercero que se suscriba a los servicios de .

    Derecho de oposición (art. 21 RGPD, LGPD, POPIA)
    Si la justificación jurídica del procesamiento de sus Datos personales ejercido por nuestra parte se basa en nuestro interés legítimo, tiene derecho a oponerse a tal procesamiento por motivos relacionados con su situación particular. Respetaremos su decisión a menos que tengamos motivos legítimos imperiosos para continuar con su procesamiento que prevalezcan sobre sus intereses y derechos, o en caso de que debamos continuar con el procesamiento de los Datos personales para la formulación, el ejercicio o la defensa de demandas legales.

    Derechos a presentar una queja (art. 77 RGPD, LGPD, POPIA)
    Tiene derecho a presentar una reclamación ante una autoridad de protección de datos local competente.

Retirada del consentimiento

Si ha dado su consentimiento para el procesamiento de sus Datos personales, tiene derecho a retirarlo en cualquier momento de forma gratuita, por ejemplo, cuando ya no quiera recibir mensajes publicitarios de nosotros. Si desea retirar su consentimiento, póngase en contacto con nosotros utilizando los datos de contacto que aparecen en la parte inferior de esta página.
Cómo ejercer sus derechos

Puede ejercer cualquiera de estos derechos relacionados con sus Datos personales enviando una solicitud a nuestro Equipo de Privacidad a través del formulario que aparece a continuación.

Por su propia privacidad y seguridad, es posible que le exijamos, a nuestra discreción, que verifique su identidad antes de proporcionar la información solicitada.
9. Cambios

Es posible que esta Política sufra alguna modificación en cualquier momento. En caso de que se produzca algún cambio en esta Política, publicaremos la versión actualizada en este sitio web. Al utilizar nuestros servicios, se le pedirá que lea y acepte nuestra Política de privacidad. De esta forma, podremos registrar que la ha aceptado y notificarle las modificaciones que esta Política pueda sufrir en el futuro.
10. Reclamaciones

Si desea hacer alguna reclamación sobre esta Política o sobre cualquier aspecto de los Datos personales que almacenamos sobre usted, póngase en contacto con nosotros a través de la dirección que se muestra arriba. Si no está conforme, tiene derecho a presentar una reclamación ante una autoridad de protección de datos local.

Si usted reside en el EEE, visite el sitio web para consultar la lista de las autoridades de protección de datos locales.
11. Contacto

Hemos simplificado el trámite para solicitar una copia de su información, darse de baja de la lista de correo electrónico, solicitar que se eliminen sus datos o hacer una pregunta sobre su protección de datos:

Para contactarnos, envíe un correo electrónico a support@dovis.it

Escríbanos a:

Oficial de privacidad de datos de Enpasoft GmbH

Küferstrasse 12, 69168, Wiesloch

Llamenos al:

+4962223171090
Powered by ENZUZO 
​