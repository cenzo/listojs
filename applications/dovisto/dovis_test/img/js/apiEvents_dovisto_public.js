// Paste here all the selected events you need for your project
// and change them according to your project-requirements



/***************************************
 CALL-ID: dovisto.Public.apiCall1 (propose)
 Description: Select nine short urls as proposal
 ***************************************/
$(document)
    .on(
        'click',
        '.click_propose',
        function () {
            let objectId = "unassigned";
            try {
                objectId = $(this).attr("id");
            } catch (err) {
                debug2(module, "no attribute 'id' found in this object", release, err);
            }

            let dataObject = {};
            functionForProject_dovisto(objectId, dataObject, "propose", "get");
        }
    );

