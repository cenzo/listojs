const cmsContent = {
    contact: `<div class="border border-light rounded" style="padding:15px;">

               <p>Enpasoft GmbH<br>
               Küferstrasse 12<br>
               69168 Wiesloch, Germany<br>
               <i class="fa fa-phone"></i>+49-6222-3171090</p> 
               <ul>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:support@dovis.it?subject=Referred to dovis.it:">${TEXT_GENERAL_ENQUIRIES}</a>&nbsp;<i class="fa fa-info fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=BUG-description:">${TEXT_SUBMIT_BUG}</a>&nbsp;<i class="fa fa-bug fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=FEATURE-Request:">${TEXT_SUBMIT_FEATURE}</a>&nbsp;<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i></li>
               </ul>`,
    about: `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
            <b>${TEXT_FREE_OF_CHARGE}</b><br><br><br>
            ${TEXT_PERSONAL_USE}<br>
                <ul>
                    <li> ${getTextById(105)}</li>
                    <li> ${getTextById(106)}</li>
                    <li > ${getTextById(107)}</li>
                    <li > ${getTextById(108)}</li>
                    <li > ${getTextById(109)}</li>
                </ul>
              ${TEXT_PROFESSIONAL_USE} - ${TEXT_MAILINGS}<br>
                 <ul>
                    <li > ${getTextById(112)}</li>
                    <li > ${getTextById(113)}</li>
                    <li > ${getTextById(114)}</li> 
               </ul>
               ${TEXT_CORS} <a href="#footnote-1">&nbsp;*&nbsp;</a> <br>
                 <ul>
                    <li > ${getTextById(116)}</li>
                    <li > ${getTextById(117)}</li>
                    <li > ${getTextById(118)}</li> 
               </ul>
               <p id="footnote-1">
            *<i class="fa fa-link" aria-hidden="true"> </i>&nbsp;<a href="https://www.dovis.it/CORSmdn" target="_parent">dovis.it/CORSmdn</a> &nbsp;
            <a href="https://www.dovis.it/CORSwiki" target="_parent">dovis.it/CORSwiki</a>
               <br><br>
            </div>`,
    footer: `<span>
        <div class="container dovis_small">
                <ul class="list-inline mb-2">
                    <a class="dovis_footer list-inline-item  cms btn btn-outline-light" data-page="about">
                           ${getTextById(57)}
                    </a> 
                    <a class="dovis_footer list-inline-item cms btn  btn-outline-light" data-page="contact">
                            ${getTextById(58)}
                    </a> 
                    <a class="dovis_footer list-inline-item btn btn-outline-light"  onclick="loadFile('./tos_${getCurrentLanguage()}.txt','cms-div')">
                            ${getTextById(59)}
                    </a> 
                    <a class="dovis_footer list-inline-item btn btn-outline-light"  onclick="loadFile('./privacy_${getCurrentLanguage()}.txt','cms-div')">
                            ${getTextById(60)}
                    </a>
                </ul>
                <p class="dovis_footer small mb-4 mb-lg-0"><em>© dovis.it 2022.  Version: ${backendVersion}/${frontendVersion} All Rights Reserved.
                      <a href="https://www.enpasoft.com">Enpasoft GmbH</a></em>
                <br><br><br>
                <p id="cms-div" class="dovis_small"></p>
                <em id="footer-showJS"></em>
                </p>
        </div>
    </span> `
}