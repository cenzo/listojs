class UIOProject {


    constructor(identifier, projectName, projectCodeName, projectDescription, userName, gitUrl) {
        if (!isInt(identifier)) throw new Error(`project id '${identifier}' is not a finite positive integer`);
        this.id = identifier;
        this.projectName = projectName;
        this.projectCodeName = projectCodeName;
        this.projectDescription = projectDescription;
        this.userName = userName;
        this.gitUrl = gitUrl;
        this.models = [];
        this.createdAt = UIOTool.getTimestamp();
        this.lastSaved = null;
    }

}


class UIOWidget {
    /**
     * @param idNumber
     * @param uioName
     * @param uioParentID
     * @param widgetType
     */

    uioParentID;
    isCurrentlyMoving;
    isCurrentlyResizing;
    hasChildrenPositionListener;
    width;
    height;
    description;
    widgetType;
    projectId;
    childrenIds;
    hidden;


    constructor(idNumber, uioParentId, widgetType, w, h, x, y) {
        this.id = idNumber;
        this.createdAt = UIOTool.getTimestamp();
        this.deletedAt = null;
        this.uioLabel = `${widgetType} ${this.id}`;
        this.uioName = null;
        this.uioParentID = uioParentId;


        this.widgetType = widgetType;
        this.hidden = false;
        this.width = w;
        this.height = h;
        this.xPosition = x;
        this.yPosition = y;
        this.isCurrentlyMoving = false;
        this.isCurrentlyResizing = false;
        this.hasChildrenPositionListener = false;
        this.innerTag = {};
        this.innerTag.style = {};
        this.description = `Description of widget${this.id}`;
        this.childrenIds = [];
        this.apiCalls = [];
        this.incomingData = [];

        this.incomingData.push("sender1Path");
        const apiCall1 = {};
        apiCall1.status = null;
        apiCall1.method = null;
        apiCall1.eventName = null;
        apiCall1.frontendRequirement = null;
        apiCall1.backendRequirement = null;
        apiCall1.apiParameters = [];
        apiCall1.apiDMLs = [];
        this.apiCalls.push(apiCall1);
        this.apiCalls[0].method = "-";
        this.apiCalls[0].eventName = "-";
        this.apiCalls[0].frontendRequirement = "Specifications for frontend processing";
        this.apiCalls[0].backendRequirement = "Specifications for  api- (or general backend-) processing";
        const params1 = {};
        params1.name = null;
        params1.type = "-";
        params1.receiverWidgets = [-1];
        this.apiCalls[0].apiParameters.push(params1);

    };
}

class UIOWidgetAppearance {
    background;
    backgroundImage;
    widgetType;
    icon;
    innerTag;
    styleClass;

    constructor(widgetType) {
        this.widgetType = widgetType;
        this.backgroundImage = null;
        this.styleClass = "border border-light rounded";
        this.innerTag = {};
        this.innerTag.style = {};
        switch (this.widgetType) {
            case UIOWareWidget.UIOWareWindow: {
                this.icon = Constants.ICON_WINDOW;
                this.background = "transparent";
                this.styleClass = "border border-light rounded";
            }
                break;
            case UIOWareWidget.UIOWareIcon: {
                this.icon = Constants.ICON_ICON;
                this.background = "transparent";
                this.innerTag.tag = "i";
                this.styleClass = "border border-light rounded";
            }
                break;
            case UIOWareWidget.UIOWareInput: {
                this.icon = Constants.ICON_INPUT;
                this.innerTag.tag = "div";
                this.innerTag.background = "transparent";
                this.styleClass = "border border-info rounded";
            }
                break;
            case UIOWareWidget.UIOWareOutput: {
                this.icon = Constants.ICON_OUTPUT;
                this.innerTag.tag = "div";
                this.styleClass = "border border-info rounded";
            }
                break;
            case UIOWareWidget.UIOWareTextarea: {
                this.icon = Constants.ICON_TEXTAREA;
                this.innerTag.tag = "textarea";
                this.innerTag.styleClass = "form-control";
            }
                break;
            case UIOWareWidget.UIOWareMockTextarea: {
                this.icon = Constants.ICON_TEXTAREA;
                this.innerTag.tag = "div";
                this.background = "white";
                this.styleClass = "form-control";
            }
                break;
            case UIOWareWidget.UIOWareTextField: {
                this.icon = Constants.ICON_TEXTFIELD;
                this.innerTag.tag = "textarea";
            }
                break;
            case UIOWareWidget.UIOWareMockTextField: {
                this.icon = Constants.ICON_TEXTFIELD;
                this.innerTag.tag = "div";
                this.background = "white";
                this.styleClass = "form-control";
            }
                break;
            case UIOWareWidget.UIOWareTable: {
                this.icon = Constants.ICON_TABLE;
                this.innerTag.tag = "div";
                this.innerTag.styleClass = "table table-striped table-hover table-sm";
                this.innerTag.style.top = "30px";
            }
                break;
            case UIOWareWidget.UIOWareMockSelectionList: {
                this.icon = Constants.ICON_SELECTION_LIST;
                this.innerTag.tag = "div";
                this.background = "inherit";
            }
                break;
            case UIOWareWidget.UIOWareMockRadioCheckBox: {
                this.icon = Constants.ICON_RADIO_CHECK_BOX;
                this.innerTag.tag = "div";
                this.background = "inherit";
            }
                break;
            case UIOWareWidget.UIOWareMockLink: {
                this.icon = Constants.ICON_LINK;
                this.innerTag.tag = "div";
                this.background = "inherit";
                this.innerTag.innerHTML = `<b><u>https://some.url</u></b>`;
                this.innerTag.style.fontSize = "9";
                this.styleClass = "border border-primary rounded";
                this.innerTag.styleClass = "float-end";
            }
                break;
            case UIOWareWidget.UIOWareBackend: {
                this.icon = Constants.ICON_BACKEND;
                this.background = "lightsteelblue";
            }
                break;
            case UIOWareWidget.UIOWareApplication: {
                this.icon = Constants.ICON_APPLICATION;
                this.background = "silver";
            }
                break;
            case UIOWareWidget.UIOWareFrontend: {
                this.icon = Constants.ICON_FRONTEND;
                this.background = "lightsteelblue";
            }
                break;
            case UIOWareWidget.UIOWareMockButton: {
                this.icon = Constants.ICON_BUTTON;
                this.innerTag.tag = "div";
                this.background = "lightsteelblue";
            }
                break;
            case UIOWareWidget.UIOWareTextChapter: {
                this.icon = Constants.ICON_TEXT_CHAPTER;
                this.innerTag.tag = "div";
                this.background = "silver";
            }
                break;
            case UIOWareWidget.UIOWareImage: {
                this.icon = Constants.ICON_IMAGE;
                this.innerTag.tag = "div";
                this.background = "silver";
            }
                break;
            default: {
                this.icon = Constants.ICON_QUESTION_MARK;
                this.innerTag.tag = "div"
            }
        }
    }
}

class UIOModel {


    constructor(uioProjectId, workareaElementName, dataObject) {
        console.log(`INFO: Name of UIO workareaElement is "${workareaElementName}"`);
        if (!workareaElementName) {
            console.error("workareaElement:", workareaElementName);
            UIOTool.errorMsg("ERROR: UIO workarea-name is not set. UIO-application cannot run");
            throw new Error("UIO workarea name is not set. UIO-application cannot run");
        }
        this.uioProjectID = uioProjectId;
        this.widget0ElementName = workareaElementName;
        const topParentElement = document.getElementById(this.widget0ElementName);
        console.log("INFO: UIO workareaElement", topParentElement);
        if (!topParentElement) {
            console.error("uio workarea-element", topParentElement);
            UIOTool.errorMsg("ERROR: UIO workarea-element is not set. UIO-application cannot run");
            throw new Error("UIO workarea element is not set. UIO-application cannot run");
        }
        const topParentRectangle = topParentElement.getBoundingClientRect();
        this.id = `${this.uioProjectID}${this.widget0ElementName}`;
        if (dataObject !== null) {
            this.oldestNode = dataObject.oldestNode;
            this.youngestNode = dataObject.youngestNode;
            this.youngestPage = dataObject.youngestPage;
            this.activeNodes = dataObject.activeNodes;
            this.deletedNodes = dataObject.deletedNodes;
            this.UIO = dataObject.UIO;
            this.leftMargin = topParentRectangle.left;
            this.topMargin = topParentRectangle.top;
        } else {
            this.oldestNode = 100;
            this.youngestNode = 100;
            this.youngestPage = 1;
            this.activeNodes = {};
            this.deletedNodes = {};
            this.deletedNodes.widgetCount = 0;
            this.UIO = {};
            this.UIO.entryPageId = this.youngestPage;
            this.UIO.currentPageId = this.youngestPage;
            this.UIO.securityRoles = [];
            this.UIO.pages = [];
            this.UIO.securityRoles = [];
            const page0 = {};
            page0.pageType = UIOWarePageType.SECTION;
            page0.targetWindow = null;
            page0.deleted = false;
            page0.createdAt = UIOTool.getTimestamp();
            page0.uioId = 0;
            page0.uioName = "Empty project page for index/id 0";
            page0.uioDescription = "This page must never be built in a real UIO project!";
            page0.callerWidgetId=0;
            const page1 = {};
            page1.pageType = UIOWarePageType.SECTION;
            page1.targetWindow = null;
            page1.uioId = this.youngestPage;
            page1.uioName = "Uio001";
            page1.uioDescription = "Start Page";
            page1.deleted = false;
            page1.createdAt = UIOTool.getTimestamp();
            page1.callerWidgetId=0;
            this.UIO.pages.push(page0);
            this.UIO.pages.push(page1);
            this.UIO.pages[this.youngestPage].mappedNodes = [];
            this.UIO.pages[this.youngestPage].workareaWidth = topParentRectangle.width;
            this.UIO.pages[this.youngestPage].workareaHeight = topParentRectangle.height;
            this.UIO.pages[this.youngestPage].createdAt = UIOTool.getTimestamp();
            const widgetTopParent = new UIOWidget(0, null, null,
                topParentElement.offsetWidth, topParentElement.offsetHeight,
                topParentRectangle.left, topParentRectangle.top);
            const applicationWidget = new UIOWidget(this.youngestNode, 0, UIOWareWidget.UIOWareApplication,
                topParentElement.offsetWidth / 4, topParentElement.offsetHeight / 5,
                topParentElement.offsetWidth + 10, 10);
            const backendWidget = new UIOWidget(this.youngestNode + 1, this.youngestNode, UIOWareWidget.UIOWareBackend,
                topParentElement.offsetWidth / 6, topParentElement.offsetHeight / 17,
                10, 40);
            const frontendWidget = new UIOWidget(this.youngestNode + 2, this.youngestNode, UIOWareWidget.UIOWareFrontend,
                topParentElement.offsetWidth / 6, topParentElement.offsetHeight / 17,
                10, 50 + topParentElement.offsetHeight / 17);
            this.youngestNode = frontendWidget.id;
            this.activeNodes.widgetCount = 3;
            widgetTopParent.uioName = this.widget0ElementName;
            widgetTopParent.projectId = this.uioProjectID;
            widgetTopParent.childrenIds = [];
            widgetTopParent.description = `The workarea of the model (element '${this.widget0ElementName}')`;
            widgetTopParent.domData = topParentRectangle;
            applicationWidget.uioLabel = `Application`;
            backendWidget.uioLabel = `Backend`;
            frontendWidget.uioLabel = `Frontend`;
            applicationWidget.description = `Application with all its third party components`;
            backendWidget.description = `Main components of the backend (api, database)`;
            frontendWidget.description = `Frontend-application(s) (browser, mobiles or any other type of client)`;
            this.activeNodes[`${this.widgetPrefix()}0`] = widgetTopParent;
            this.activeNodes[`${this.widgetPrefix()}${applicationWidget.id}`] = applicationWidget;
            this.activeNodes[`${this.widgetPrefix()}${backendWidget.id}`] = backendWidget;
            this.activeNodes[`${this.widgetPrefix()}${frontendWidget.id}`] = frontendWidget;
            this.UIO.pages[this.youngestPage].mappedNodes.push(applicationWidget.id);
            this.UIO.pages[this.youngestPage].mappedNodes.push(backendWidget.id);
            this.UIO.pages[this.youngestPage].mappedNodes.push(frontendWidget.id);
            this.leftMargin = topParentElement.getBoundingClientRect().left;
            this.topMargin = topParentElement.getBoundingClientRect().top;

        }
    }

    widgetPrefix() {
        return `m${this.uioProjectID}${this.widget0ElementName}widget`;
    }

}

let currentController = null;

class UIOController {
    #mouseStartXPosition;
    #mouseStartYPosition;
    #currentXPosition;
    #currentYPosition;


    // TODO refactor:
    //    include uioClient in constructor
    //    and use uioClient.doUio
    constructor(uioModel) {
        this.uioModel = uioModel;
        currentController = this;

        // TODO instantiate rollBackModel with all constructor variables
        // this.rollBackModel = new UIOModel();

        if (!this.uioModel) {
            UIOTool.errorMsg("Could not instantiate UIOController: no model was found");
            throw new Error("Could not instantiate UIOController: no model was found");
        }
        const uioWorkarea = document.getElementById(this.uioModel.widget0ElementName);
        if (!uioWorkarea) {
            UIOTool.errorMsg("Could not instantiate UIOController: document does not contain a widget0 to be used as UIOWorkarea");
            throw new Error("Could not instantiate UIOController: document does not contain a widget0 (UIOWorkarea)");
        }

        console.log(`Adding widget0 as mouse-Listener for the current session.`);
        document.addEventListener("mousemove", (e) => {
            this.setCurrentPosition(e.clientX, e.clientY);
            const data = UIOController.#uioListenerFunction(e);
            doClientUIO(data[0], data[1], data[2], data[3], data[4], "mousemove");
        });
        uioWorkarea.addEventListener("mouseover", (e) => {
            const data = UIOController.#uioListenerFunction(e);
            doClientUIO(data[0], data[1], data[2], data[3], data[4], "mouseover");
        });
        uioWorkarea.addEventListener("mouseleave", (e) => {
            const data = UIOController.#uioListenerFunction(e);
            doClientUIO(data[0], data[1], data[2], data[3], data[4], "mouseleave");
        });
        uioWorkarea.addEventListener("mousedown", (e) => {
            const data = UIOController.#uioListenerFunction(e);
            doClientUIO(data[0], data[1], data[2], data[3], data[4], "mousedown");
        });
        uioWorkarea.addEventListener("mouseup", () => {
            doClientUIO(null, null, null, null, null, "mouseup");
        });
        uioWorkarea.addEventListener("mouseenter", (e) => {
            const data = UIOController.#uioListenerFunction(e);
            doClientUIO(data[0], data[1], data[2], data[3], data[4], "mouseenter");
        });
    }

    static #getValuesFromRollback(rollBackData) {
        if (DEBUG_COMMIT) console.log("//TODO: implement how to get the appropriate rollBackData", rollBackData);
    };

    setWidgetUIOName(widget) {
        widget.uioName = `${this.uioModel.widgetPrefix()}${widget.id}`;
    };


    /* setSelectedChildID(widget, _childId) {
         console.log(`setSelectedChildID: controller setting child id ${_childId} for widget ${widget.uioName}`);
         widget.uioSelectedChildID = _childId;

         console.log(`selected child is now set to ${widget.uioSelectedChildID}`);
     }*/

    static #updateRollBackModel(model, changedAttributes) {
        if (DEBUG_COMMIT) console.log("//TODO update the current rollback-model with the successfully committed Attributes", model, changedAttributes);
        /*
        let pathList = changedAttributes['pathList'];
        let valueList = changedAttributes['valueList'];
        for (let i = 0; i < changedAttributes.length; i++) {
            this.updateObjectAttribute(model, pathList[i], valueList[i]);
        }*/
    };

    static #uioListenerFunction(e) {
        let uioid, uiotype, uioaction;
        const id = e.target['id'];
        const uioidAttr = e.target['attributes']['data-uioid'];
        const uiotypeAttr = e.target['attributes']['data-uiotype'];
        const uioactionAttr = e.target['attributes']['data-uioaction'];
        const allAttributes = e.target['attributes'];
        if (uioidAttr) uioid = uioidAttr.value;
        if (uiotypeAttr) uiotype = uiotypeAttr.value;
        if (uioactionAttr) uioaction = uioactionAttr.value;
        return [id, uioid, uiotype, uioaction, allAttributes];

    };

    persist(localStoragePath, project) {
        if (uioProject instanceof UIOProject) {
            console.log("writing project to local storage. Item-path is:", localStoragePath);
            localStorage.setItem(localStoragePath, JSON.stringify(project));
        } else
            console.error("cannot persist data. This object is not an UIOProject:", project);
    }

    updateWidget(widgetId, changedProperties) {
        console.log("update Widget", widgetId, changedProperties);
        const widget = this.getWidgetById(widgetId);
        this.modelViewChanged(widget, changedProperties, UIOModelViewOperation.UPDATE_WIDGET);
    };

    /*
      createNewPage(pageDescription) {
          / *const uioPage = {};
          uioPage.uioId = this.uioModel.UIO.pages.length;
          uioPage.uioName = pageName;
          uioPage.uioDescription = pageDescription;
          uioPage.mappedNodes = [];
          this.uioModel.UIO.pages.push(uioPage);* /
        // const pageData = {};
        // pageData.value = {};
        // pageData.value.page = pageDescription;
        const pageProperties = [];
        pageProperties.push(pageDescription);

        console.log(`Creating new page ${pageDescription}...`)
        const created = this.modelViewChanged(this.getWidgetById(0), pageProperties, UIOModelViewOperation.CREATE_PAGE);
        console.log(`Page created?`, created);
        if (created) console.log(this.getPages()[this.getPages().length - 1]);
    };


        updateParameterMappings(widget) {
            const unique = [];
            for (let i = 0; i < widget.apiCalls.length; i++) {
                const call = widget.apiCalls[i];
                const callIndex = i;

                for (let j = 0; j < call.apiParameters.length; j++) {
                    const parameter = call.apiParameters[j];
                    const callParameterIndex = j;
                    const name = parameter.name;

                    for (let k = 0; k < parameter.receiverWidgets.length; k++) {
                        const receiver = parameter.receiverWidgets[k];
                        const receiverIndex = k;
                        const receiverId = receiver.widgetId;
                        //example: call0.name.param0.102.0
                        const mapping = `${callIndex}.${name}.${callParameterIndex}.${receiverId}.${receiverIndex}`;
                        unique.push(mapping);
                    }
                }
            }
            const uniqueMappings = unique.filter(UIOTool.onlyUnique);
            let previousCall = null, previousParameter = null, previousReceiver = null;
            for (let i = 0; i < uniqueMappings.length; i++) {
                const arr = uniqueMappings[i].split("\.");
                const callIndex = arr[0];
                const parameter = arr[1];
                const parameterIndex = arr[2];
                const receiver = arr[3];
                const receiverIndex = arr[4];
                if (Number(callIndex) === Number(previousCall) && parameter === previousParameter && Number(receiver) === Number(previousReceiver)) {
                    console.warn("Deleting ", `widget.apiCalls[${callIndex}].apiParameters[${parameterIndex}].receiverWidgets[${receiverIndex}]`);
                    console.warn("Value is: ", widget.apiCalls[callIndex].apiParameters[parameterIndex].receiverWidgets[receiverIndex],);
                    widget.apiCalls[callIndex].apiParameters[parameterIndex].receiverWidgets.slice(receiverIndex, 1);
                }
                previousParameter = parameter;
                previousReceiver = receiver;
                previousCall = callIndex;
            }
            const mappings = this.uioModel.UIO.parameterMappings.filter(UIOTool.onlyUnique);
            // widgetID.call.parameterName->receiverID
            // example: 101.0.name->102
            for (let i = 0; i < mappings.length; i++) {
                const arr = mappings[i].split("->");
                const key = arr[0];
                const keyArr = key.split("\.");
                const widgetID = keyArr[0];
                if (widgetID === widget.id) this.uioModel.UIO.parameterMappings.splice(i, 1);
            }
            console.log("-----------------------------------------------------")
            for (let i = 0; i < widget.apiCalls.length; i++) {
                const call = widget.apiCalls[i];
                const callIndex = i;
                console.log("call" + i, `${widget.id}.call`, call);
                for (let j = 0; j < call.apiParameters.length; j++) {
                    const parameter = call.apiParameters[j];
                    const name = parameter.name;
                    console.log("parameter" + j, `${widget.id}.call${callIndex}.parameter${j}`, `name`, parameter, name);
                    for (let k = 0; k < parameter.receiverWidgets.length; k++) {
                        const receiver = parameter.receiverWidgets[k];
                        const receiverId = receiver.widgetId;
                        console.log("receiver" + k, `${widget.id}.call${i}.parameter${j}.receiver${k}`, `receiverId`, receiver, receiverId);

                        //example: call0.name.param0.102.0
                        this.uioModel.UIO.parameterMappings.push(`${widget.id}.${callIndex}.${name}->${receiverId}`);
                    }
                }
            }
            console.log("-----------------------------------------------------")

            this.uioModel.UIO.parameterMappings = this.uioModel.UIO.parameterMappings.filter(UIOTool.onlyUnique);
        }*/

    setMouseStart(xPosition, yPosition) {
        this.#mouseStartXPosition = xPosition;
        this.#mouseStartYPosition = yPosition;
    }

    #getMouseDeltaX() {
        return this.#currentXPosition - this.#mouseStartXPosition;
    }

    #getMouseDeltaY() {
        return this.#currentYPosition - this.#mouseStartYPosition;
    }

    setCurrentPosition(xPosition, yPosition) {
        this.#currentXPosition = xPosition;
        this.#currentYPosition = yPosition
    }

    getCurrentPosition() {
        const currentPosition = {};
        currentPosition.x = this.#currentXPosition;
        currentPosition.y = this.#currentYPosition;
        return currentPosition;
    }

    getCurrentUioPosition() {
        const currentUioPosition = {};
        currentUioPosition.x = this.#currentXPosition - controller.uioModel.leftMargin;
        currentUioPosition.y = this.#currentYPosition - controller.uioModel.topMargin;
        return currentUioPosition;
    }

    getCurrentPage() {
        return this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId];
    }

    createNewWidget(uioParentId, widgetType, w, h, x, y) {
        this.uioModel.youngestNode++;
        console.log(`----------- Setting youngest node of model ${this.uioModel.id} to ${this.uioModel.youngestNode}`);
        const newID = this.uioModel.youngestNode;
        try {
            console.log(`Creating widget ${newID} with parent ${uioParentId} at ${x},${y}...`);
            const builtWidget = new UIOWidget(newID, uioParentId, widgetType, w, h, x, y);
            console.log("parent widget:", this.getWidgetById(uioParentId));
            this.getWidgetById(uioParentId).childrenIds.push(newID);
            builtWidget.widget0ElementName = this.uioModel.widget0ElementName;
            builtWidget.projectId = this.uioModel.projectId;
            //builtWidget.containingPage = this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].uioName;
            this.setWidgetUIOName(builtWidget);
            this.modelViewChanged(builtWidget, [], UIOModelViewOperation.CREATE_WIDGET);
            const commitData = {};
            commitData.widgetID = newID;
            commitData.commitPaths = [];
            commitData.commitPaths.push(`//TODO define commitData for createWidget-case`);
            commitData.commitPaths.push(`//example: item 0 might be: activeNodes.['widget${newID}']`);
            this.#commit(commitData);
            return builtWidget;
        } catch (err) {
            UIOTool.errorMsg(err.toString());
            //showMessage(document.getElementById(this.getWidgetById(uioParentId).uioName), err.toString());
            console.trace(err);
            const rollBackData = {};
            rollBackData.pathList = [];
            rollBackData.pathList.push(`//TODO define rollBackData for createWidget-case.`);
            rollBackData.pathList.push(`//TODO example: item 0 might be: activeNodes.['widget${newID}']`);
            console.trace(err);
            this.#rollBack(rollBackData);
        }
    };

    deleteWidget(objectData) {
        try {
            const widget = this.getWidgetById(objectData['objectToRemove']);
            const changedProperties = [];
            changedProperties[0] = this.#getParentElement(widget);
            this.modelViewChanged(widget, changedProperties, UIOModelViewOperation.DELETE_WIDGET);
            const commitData = {};
            commitData.widgetID = widget.id;
            commitData.commitPaths = [];
            commitData.commitPaths.push(`//TODO define commitData for deleteWidget-case`);
            commitData.commitPaths.push(`//example: item 0 might be: activeNodes.['widget${objectData.objectToRemove}']`);
            this.#commit(commitData);
        } catch (err) {
            UIOTool.errorMsg(err.toString());
            //showMessage(this.#getParentElement(this.getWidgetById(0)), err.toString());
            console.trace(err);
            const rollBackData = {};
            rollBackData.pathList = [];
            rollBackData.pathList.push(`//TODO define rollBackData for deleteWidget-case, e.g.`);
            rollBackData.pathList.push(`//TODO example: item 0 might be: activeNodes.['widget${objectData.objectToRemove}']`);
            this.#rollBack(rollBackData);
        }
    }

    buildPage(uioPageID) {
        this.clearCurrentPage();
        const nodesToBuild = this.uioModel.UIO.pages[uioPageID].mappedNodes;
        this.uioModel.UIO.currentPageId = uioPageID;
        this.buildWidgets(nodesToBuild);
    }

    clearCurrentPage() {
        const nodesToBuild = this.getCurrentPage().mappedNodes;
        const workarea = this.getWidgetById(0);
        // Move the property- and message screen, otherwise we lose it
        const workareaElement = document.getElementById(workarea.uioName);
        workareaElement.appendChild(getPropertyScreen());
        closePropertyScreen();

        for (let i = 0; i < nodesToBuild.length; i++) {
            const widget = this.getWidgetById(nodesToBuild[i]);
            if (widget) {
                const widgetElement = document.getElementById(widget.uioName);
                console.log("clear page: remove widget", i, widget.id, nodesToBuild[i]);
                if (widgetElement) widgetElement.remove();
                else console.log(`--- WARNING: widgetElement ${widget.uioName}  not on screen`);
            } else console.warn(`--- WARNING: widget ${widget.uioName} was not found in model`);
        }
    }

    buildWidgets(nodesToBuild) {
        console.log("2.4 --- buildWidgets:  Try to BUILD screen with these nodes:", nodesToBuild);
        const widget0 = this.getWidgetById(0);
        this.modelViewChanged(widget0, [], UIOModelViewOperation.PAINT_WIDGET);

        for (let i = 0; i < nodesToBuild.length; i++) {
            try {
                const widget = this.getWidgetById(nodesToBuild[i]);
                console.log(`2.4.${i} --- buildWidgets: BUILD screen with  ${widget.uioName}`, widget.id, `nodesToBuild[i]`, nodesToBuild[i]);
                widget.widget0ElementName = this.uioModel.widget0ElementName;
                widget.projectId = this.uioModel.projectId;
                this.setWidgetUIOName(widget);
                this.modelViewChanged(widget, [], UIOModelViewOperation.CREATE_WIDGET);
            } catch (error) {
                UIOTool.errorMsg(error.toString());

                console.error(error);
                console.error(`Could not BUILD element for widget with index ${nodesToBuild[i]}`);
            }
        }

    }

    static #paintInnerTable() {
        return `<thead class="table-dark"><th>...</th><th>...</th><th>...</th></thead> <tbody>
                                  <tr><td><i class="bi bi-border"></i></td><td><i class="bi bi-border"></i></td><td><i class="bi bi-border"></i></td><tr>
                                    <td><i class="bi bi-border"></i></td><td><i class="bi bi-border"></i></td><td><i class="bi bi-border"></i></td></tr></tbody>`;
    }

    getIncomingData(widget) {
        // TODO refactor to improve future performance
        const inData = [];
        const widgets = this.getAllWidgetsArray();
        //console.log("this.getAllWidgetsArray().length="+this.getAllWidgetsArray().length)
        for (let i = 0; i < widgets.length; i++) {
            const apiCalls = widgets[i].apiCalls;
            //console.log(apiCalls.length+" apiCalls")
            for (let j = 0; j < apiCalls.length; j++) {
                const apiParameters = apiCalls[j].apiParameters;
                //console.log(apiParameters.length+" apiParameters")
                for (let k = 0; k < apiParameters.length; k++) {
                    const receivers = apiParameters[k].receiverWidgets;
                    //console.log(receivers.length+" receivers")
                    for (let l = 0; l < receivers.length; l++) {
                        //console.log("receivers[l] ,widget.id", receivers[l], widget.id);
                        if (receivers[l] === widget.id) {
                            const param = {};
                            const widgetPage = this.getWidgetPage(widgets[i]);
                            param.senderPage = widgetPage.uioName;
                            param.senderpageDescription = widgetPage.uioDescription;
                            param.senderID = widgets[i].id;
                            param.senderLabel = widgets[i].uioLabel;
                            param.name = apiParameters[k].name;
                            param.type = apiParameters[k].type;
                            inData.push(param);
                        }
                    }
                }
            }
        }

        return inData.filter(UIOTool.onlyUnique);
    }

    getObjectAttribute(path, update, newValue) {
        //console.log("Get attribute, path: ", path);
        if (update && newValue === "/delete/")
            console.log("path:", path, `Trying to delete ${this.uioModel[path]}`)
        let stack = path.split('\.');
        let model = this.uioModel;
        let shift, index;
        let parenthesisIdx1, parenthesisIdx2;
        while (stack.length > 1) {
            shift = stack.shift();
            parenthesisIdx1 = shift.indexOf('[');
            if (parenthesisIdx1 > 0) {
                parenthesisIdx2 = shift.indexOf(']');
                if (parenthesisIdx2 > 0) {
                    //  console.log("[model,shift] before[", model, shift, "]");
                    index = shift.substring(parenthesisIdx1 + 1, parenthesisIdx2);
                    shift = shift.substring(0, parenthesisIdx1);
                    //  console.log("[model,parenthesisIdx1,parenthesisIdx2,index,shift][", model, parenthesisIdx1, parenthesisIdx2, index, shift, "]");
                    model = model[shift][index];
                    // console.log("model[shift][index],stack", model, stack)
                } else throw Error(`Error in array-path '${path}': missing closing parenthesis `);
            } else model = model[shift];
        }
        let finalShift = stack.shift();
        parenthesisIdx1 = finalShift.indexOf('[');
        if (parenthesisIdx1 > 0) {
            parenthesisIdx2 = finalShift.indexOf(']');
            if (parenthesisIdx2 > 0) {
                index = finalShift.substring(parenthesisIdx1 + 1, parenthesisIdx2);
                finalShift = finalShift.substring(0, parenthesisIdx1);
                if (update && newValue === "/delete/") {
                    if (model[finalShift][index]) {
                        model[finalShift].splice(index, 1);
                        return `deleted: ${finalShift}[${index}]`;
                    } else return `Could not delete. (No such item, try refresh).`;
                }
                if (update) model[finalShift][index] = newValue;
                return model[finalShift][index];
            } else throw Error(`Error in array-path '${path}': missing closing parenthesis `);
        }
        if (update && newValue === "/delete/") {
            delete model[finalShift];
            return `deleted: ${finalShift}`;
        }
        if (update) model[finalShift] = newValue;
        return model[finalShift];
    };

    #commit(commitData) {
        // TODO: refactor
        if (DEBUG_COMMIT) console.log(`//TODO: implement commit --- ${JSON.stringify(commitData)}`);
        UIOController.#updateRollBackModel(this.uioModel, commitData);
    };


    #getParentElement(widget) {
        if (typeof widget.uioParentID === 'undefined') throw new Error(`'${widget.uioName}' has no parent-ID.`);
        else if (widget.uioParentID.constructor !== Number) throw new Error(`Parent-ID of widget '${widget.uioName}' is not a number.`);
        const parentWidget = this.getWidgetById(widget.uioParentID);
        const parentID = widget.uioParentID;
        if (parentID === 0) return document.getElementById(this.uioModel.widget0ElementName);
        if (!parentWidget) throw new Error(`There is no active parent widget 'widget${widget.uioParentID}' for widget '${widget.uioName}' .`);
        const parentElement = document.getElementById(parentWidget.uioName);
        if (!parentElement)
            throw new Error(`Parent-element of element '${widget.uioName}' was not found.`);
        return parentElement;
    };

    getParentWidget(widget) {
        //console.log(`getParentWidget: Getting this.uioModel.activeNodes[${this.uioModel.widgetPrefix()}${widget.uioParentID}]`);
        return this.uioModel.activeNodes[`${this.uioModel.widgetPrefix()}${widget.uioParentID}`];
    }

    getWidgetById(widgetID) {
        return this.uioModel.activeNodes[`${this.uioModel.widgetPrefix()}${widgetID}`];
    };

    getDeletedWidgetById(widgetID) {
        return this.uioModel.deletedNodes[`${this.uioModel.widgetPrefix()}${widgetID}`];
    };

    getPages() {
        const allPages = this.uioModel.UIO.pages;
        const activePages = [];
        for (let i = 0; i < allPages.length; i++) {
            if (allPages[i].deleted !== true) activePages.push(allPages[i]);
        }
        return activePages;
    };

    getWidgetPage(widget) {
        const p = this.getPages();
        //console.log("pages", p);
        for (let i = 1; i < p.length; i++) {
            const n = p[i].mappedNodes;
            //console.log("mappedNodes", n);
            for (let j = 0; j < n.length; j++)
                if (n[j] === widget.id) return p[i];
        }
    }

    modelViewChanged(widget, changedProperties, mvcOperation) {
        try {
            this.#changeModel(widget, changedProperties, mvcOperation);
            this.#changeView(widget, changedProperties, mvcOperation);
            return true;
        } catch (err) {
            console.error(err);
            UIOTool.errorMsg(err.toString());
            //showMessage(this.#getParentElement(widget), err.toString());
            return false;
        }
    }

    #rollBack(rollBackData) {
        // TODO
        const oldAttributes = UIOController.#getValuesFromRollback(rollBackData['pathList']);
        if (DEBUG_COMMIT) console.log("//TODO: implement rollBack", rollBackData, "oldAttributes for rollback", oldAttributes);
        UIOController.#updateRollBackModel(this.uioModel, oldAttributes);
    };

    #doWidgetDeletion(widget, pageID) {
        console.info(`Delete widget ${widget.id} of page ${pageID}...`);
        const array = this.uioModel.UIO.pages[pageID].mappedNodes;
        console.info("children of widget to delete:", widget.childrenIds);
        //1. Delete all children of the widget
        for (let i = 0; i < widget.childrenIds.length; i++) {
            const index = array.indexOf(widget.childrenIds[i]);
            if (index !== -1) {
                console.log("array before deletion", array);
                array.splice(index, 1);
                console.log("array after deletion", array);
                const deletedWidget = this.getWidgetById(widget.childrenIds[i]);
                const uioName = deletedWidget.uioName;
                deletedWidget.deletedAt = UIOTool.getTimestamp();
                console.log(`Try deleting ${uioName}`);
                this.uioModel.deletedNodes[uioName] = this.uioModel.activeNodes[uioName];
                delete this.uioModel.activeNodes[uioName];
                this.uioModel.activeNodes.widgetCount--;
                this.uioModel.deletedNodes.widgetCount++;
            } else throw new Error(`widget${widget.childrenIds[i]} not found in mappedNodes of  page ${this.uioModel.UIO[pageID]}`)
        }

        // We do NOT delete the widget from the list of the parent
        // (in case we want to resume from the deleted nodes, we will need this reference):
        // const parent=this.getParentWidget(widget);
        // const indexOfChild=parent.childrenIds.indexOf(widget.id);
        // parent.childrenIds.splice(indexOfChild,1);

        //2. Move the widget from active to deleted nodes:

        const index = array.indexOf(widget.id);
        if (index !== -1) {
            array.splice(index, 1);
            this.uioModel.deletedNodes[widget.uioName] = this.uioModel.activeNodes[widget.uioName];
            widget.deletedAt = UIOTool.getTimestamp();
            delete this.uioModel.activeNodes[widget.uioName];
            this.uioModel.activeNodes.widgetCount--;
            this.uioModel.deletedNodes.widgetCount++;
        } else throw new Error(`widget${widget.id} not found in mappedNodes of page ${this.uioModel.UIO[pageID]}`)
    }

    #changeModel(widget, changedProperties, mvcOperation) {
        if (!widget)
            throw new Error(`Tried to change view with a null or undefined object `);
        else if (widget.id !== 0 && (!widget.widget0ElementName || widget.widget0ElementName === 'undefined')) throw new Error(`Unallowed widget: no workarea is set`);
        else if (!widget.uioName)
            throw new Error(`Tried to change model with unnamed widget `);
        else if (!changedProperties)
            throw new Error(`Invalid or empty model-change properties were given for  '${widget.uioName}'`);
        else if (!Array.isArray(changedProperties)) throw new Error(`Properties to be changed must be specified as an array of strings.`);
        else if (!mvcOperation)
            throw new Error(`No model-change operation was passed for widget '${widget.uioName}'`);
        else {
            this.uioModel.lastSaved = UIOTool.getTimestamp();
            switch (mvcOperation) {
                case UIOModelViewOperation.PAINT_WIDGET:
                    // DO nothing: widget already exists in model
                    break;
                case UIOModelViewOperation.CREATE_PAGE:
                    this.uioModel.youngestPage++;
                    const newPage = {};
                    console.log("changedProperties", changedProperties);
                    newPage.uioId = this.uioModel.youngestPage;
                    newPage.deleted = false;
                    newPage.uioName = `Uio${UIOTool.pad(this.uioModel.youngestPage)}`;
                    newPage.uioDescription = changedProperties[0];
                    newPage.mappedNodes = [];
                    newPage.workareaWidth = this.getCurrentPage().workareaWidth;
                    newPage.workareaHeight = this.getCurrentPage().workareaHeight;
                    newPage.callerWidgetId=widget.id;
                    newPage.pageType=UIOWarePageType.SECTION;
                    this.uioModel.UIO.pages.push(newPage);
                    break;
                case UIOModelViewOperation.DELETE_PAGE:
                    const pageID = Number(changedProperties[0]);
                    console.info(`Delete page ${pageID}...`);
                    this.uioModel.UIO.pages[pageID].deleted = true;
                    this.uioModel.UIO.pages[pageID].deletedAt = UIOTool.getTimestamp();
                    const containedWidgets = this.uioModel.UIO.pages[pageID].mappedNodes;
                    //1. Delete all widgets of page
                    console.info("Delete all widgets of page ", pageID, containedWidgets);
                    for (let i = 0; i < containedWidgets.length; i++) {
                        const deletedWidget = this.getWidgetById(containedWidgets[i]);
                        const uioName = deletedWidget.uioName;
                        deletedWidget.deletedAt = UIOTool.getTimestamp();
                        console.info(`Try deleting ${uioName}`);
                        this.uioModel.deletedNodes[uioName] = this.uioModel.activeNodes[uioName];
                        delete this.uioModel.activeNodes[uioName];
                        this.uioModel.activeNodes.widgetCount--;
                        this.uioModel.deletedNodes.widgetCount++;
                    }

                    //  const page=this.getPages()[pageID];
                    //  if (page.deleted===true) throw new Error(`page ${pageID} is already deleted`);

                    //1. get  widget-IDs of the page
                    // const containedWidgets = page.mappedNodes;
                    // We do NOT delete the widget from the receiver-lists
                    // (in case we want to resume from the deleted nodes, we will need these references):

                    // Following code was not tested, keep for reference
                    // const allWidgets = this.getAllWidgetsArray();
                    // for (let i = 0; i < allWidgets.length; i++) {
                    //     const widget = allWidgets[i];
                    //     const widgetApiCalls = widget.apiCalls;
                    //     for (let wac = 0; wac < widgetApiCalls.length; wac++) {
                    //         const widgetParameters = widgetApiCalls[wac];
                    //         for (let p = 0; p < widgetParameters.length; p++) {
                    //             const receiverWidgets = widgetParameters[p].receiverWidgets;
                    //             //3.if any widget contains a widget of the page as receiver, then delete that receiver
                    //             for (let r = 0; r < receiverWidgets.length; r++) {
                    //                 if (containedWidgets.includes(receiverWidgets[r])) {
                    //                     receiverWidgets.splice(r, 1);
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //4. After removal of all receiver-references, delete all the widgets of the page
                    // for (let d = 0; d < containedWidgets.length; d++)
                    //  this.#doWidgetDeletion(this.getWidgetById(containedWidgets[d]), pageID);
                    break;
                case UIOModelViewOperation.UPDATE_PAGE:
                    if (changedProperties.length < 1) throw new Error(`Property-array for page-update is empty.`);
                    for (let i = 0; i < changedProperties.length; i++) {
                        const pageKey = changedProperties[i].key;
                        if (pageKey.indexOf(`UIO.pages`) <= -1)
                            throw new Error(`Property-array for page-update does not contain a correct path to a page.`);
                        const newValue = changedProperties[i].value;
                        console.info(`Editing page ${pageKey}...`);
                        const attrValue = this.getObjectAttribute(pageKey);
                        let infoValue;
                        if (attrValue && typeof attrValue === 'object') infoValue = JSON.stringify(attrValue);
                        else infoValue = attrValue;
                        console.info(`--- CHECK VALUE BEFORE MODEL-UPDATE ON PATH '${pageKey}':  '${infoValue}' `);
                        const updatedValue = this.getObjectAttribute(pageKey, true, newValue);
                        console.info(`--- updated value on path '${pageKey}' is now '${updatedValue}'`);
                    }
                    break;
                case UIOModelViewOperation.CREATE_WIDGET: {
                    if (changedProperties.length > 1) throw new Error(`Not allowed to provide properties while creating a new widget.`);
                    const newID = widget.id;
                    if (!newID || newID === 'undefined') throw new Error(` Error in id while trying to create new widget: newID='${newID}'`);
                    if (this.uioModel.activeNodes[newID]) throw new Error(`widget${newID} already exists`);
                    else if (this.getCurrentPage().mappedNodes.includes(widget.id))
                        console.warn(`page ${this.getCurrentPage().uioName} already contains ${newID} `);
                    //throw new Error(`page ${this.getCurrentPage().uioName} already contains ${newID} `);
                    else {
                        this.uioModel.activeNodes[widget.uioName] = widget;
                        this.uioModel.activeNodes.widgetCount++;
                        this.uioModel.UIO.pages[sessionUIOModel.UIO.currentPageId].mappedNodes.push(widget.id);
                    }
                }
                    break;
                case UIOModelViewOperation.DELETE_WIDGET: {
                    this.#doWidgetDeletion(widget, this.uioModel.UIO.currentPageId)
                }
                    break;
                case UIOModelViewOperation.MOVE_WIDGET:
                case UIOModelViewOperation.RESIZE_WIDGET: {
                    let deltaX = 0, deltaY = 0;
                    if (this.#getMouseDeltaX()) deltaX = this.#getMouseDeltaX();
                    if (this.#getMouseDeltaY()) deltaY = this.#getMouseDeltaY();
                    if (mvcOperation === UIOModelViewOperation.RESIZE_WIDGET) {
                        if (!widget.isCurrentlyResizing) throw new Error(`Tried to resize widget ${widget.id} but it is not marked to be resized`);
                        widget.width = Math.round((widget.width + deltaX) / 5) * 5;
                        widget.height = Math.round((widget.height + deltaY) / 5) * 5;
                    } else if (mvcOperation === UIOModelViewOperation.MOVE_WIDGET) {
                        if (!widget.isCurrentlyMoving) throw new Error(`Tried to move  widget ${widget.id} but it is not marked to be moved`);
                        widget.xPosition = Math.round((widget.xPosition + deltaX) / 5) * 5;
                        widget.yPosition = Math.round((widget.yPosition + deltaY) / 5) * 5;
                    }
                }
                    break;
                case UIOModelViewOperation.UPDATE_WIDGET: {
                    if (changedProperties.length === 0)
                        throw new Error(`Empty properties while trying to update widget '${widget.uioName}'`);
                    for (let i = 0; i < changedProperties.length; i++) {
                        const attributePath = changedProperties[i].key;
                        const attributeValue = changedProperties[i].value;
                        const attrValue = this.getObjectAttribute(attributePath);
                        let infoValue;
                        if (attrValue && typeof attrValue === 'object') infoValue = JSON.stringify(attrValue);
                        else infoValue = attrValue;
                        console.info(`--- CHECK VALUE BEFORE MODEL-UPDATE ON PATH '${attributePath}':  '${infoValue}' `);
                        const updatedValue = this.getObjectAttribute(attributePath, true, attributeValue);
                        console.info(`--- updated value on path '${attributePath}' is now '${updatedValue}'`);
                    }
                }
                    break;
                case UIOModelViewOperation.HIDE_WIDGET: {
                    if (widget.hidden) widget.hidden = false;
                    else widget.hidden = true;
                }
                    break;
                default:
                    throw new Error(`Unknown or unallowed operation '${mvcOperation}' while trying to change model for widget ${widget}`);
            }
        }
    };

    #changeView(widget, changedProperties, mvcOperation) {
        let element = document.getElementById(widget.uioName);
        if (mvcOperation !== UIOModelViewOperation.CREATE_WIDGET && mvcOperation !== UIOModelViewOperation.PAINT_WIDGET && !element)
            throw new Error(`Element with id '${widget.uioName}' does not exist`);
        switch (mvcOperation) {
            //How to display a  page must be deferred entirely to the client:
            case UIOModelViewOperation.CREATE_PAGE:
                doClientUIO(null, null, null, UIOModelViewOperation.CREATE_PAGE, changedProperties, null);
                break;
            case UIOModelViewOperation.DELETE_PAGE:
                const pageID = Number(changedProperties[0]);
                const page = this.uioModel.UIO.pages[pageID];
                const containedWidgets = page.mappedNodes;
                for (let i = 0; i < containedWidgets.length; i++) {
                    const widget = this.getDeletedWidgetById(containedWidgets[i]);
                    const elementId = widget.uioName;
                    console.info(`Try removing element ${elementId}...`);
                    const element = document.getElementById(elementId);
                    if (element) {
                        element.remove();
                    } else console.warn(`Element ${elementId} not found (probably already removed)`);
                }
                this.uioModel.UIO.currentPageId = 1;
                doClientUIO(null, null, null, UIOModelViewOperation.DELETE_PAGE, changedProperties, null);
                break;
            case UIOModelViewOperation.UPDATE_PAGE:
                this.uioModel.UIO.currentPageId = 1;
                doClientUIO(null, null, null, UIOModelViewOperation.UPDATE_PAGE, changedProperties, null);
                break;
            case UIOModelViewOperation.PAINT_WIDGET:
                if (widget.id !== 0)
                    throw new Error("Widgets must always be built entirely. This operation is no more supported: " + UIOModelViewOperation.PAINT_WIDGET);
                else if (changedProperties.length > 0)
                    throw new Error(`Only an empty property-array is allowed for painting widgets.   '${widget.uioName}' was not placed.`);
                /*if (widget.id !== 0 && !element)
                    throw new Error(`Element with id '${widget.uioName}' does not exist`);*/
                else /*if (widget.id === 0)*/ {
                    const workarea = document.getElementById(this.uioModel.widget0ElementName);
                    workarea.style.position = "absolute";
                    workarea.style.width = this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].workareaWidth + "px";
                    workarea.style.height = this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].workareaHeight + "px";
                } /*else {
                    console.log(`2.4. --- changeView: paint widget with  ${widget.uioName}`, widget.id);
                    //const parentElement = this.#getParentElement(widget);
                    //if (!parentElement)
                    //  throw new Error(`No parent element of widget '${widget.uioName}' exists.`);
                    this.paintWidget(widget);
                }*/

                break;
            case UIOModelViewOperation.CREATE_WIDGET: {
                if (changedProperties.length > 0)
                    throw new Error(`Only an empty property-array is allowed for creating widgets.   '${widget.uioName}' was not created.`);
                if (widget.id !== 0 && element)
                    throw new Error(`Element with id '${widget.uioName}' already exists`);
                else if (widget.id === 0) {
                    const workarea = document.getElementById(this.uioModel.widget0ElementName);
                    workarea.style.position = "absolute";
                    workarea.style.width = this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].workareaWidth + "px";
                    workarea.style.height = this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].workareaHeight + "px";
                } else {
                    console.log(`2.4. --- changeView: build widget with  ${widget.uioName}`, widget.id);
                    element = document.createElement("div");
                    element.setAttribute("id", widget.uioName);
                    const parentElement = this.#getParentElement(widget);
                    if (!parentElement)
                        throw new Error(`No parent element of widget '${widget.uioName}' exists.`);
                    parentElement.appendChild(element);
                    this.buildWidget(widget);
                }

            }
                break;
            case UIOModelViewOperation.DELETE_WIDGET: {
                const parentElement = changedProperties[0];
                parentElement.removeChild(element);
            }
                break;
            case UIOModelViewOperation.MOVE_WIDGET: {
                if (!widget.isCurrentlyMoving) throw new Error(`Called change view move for ${widget.uioName} but widget is not marked to be moved`);
                //const widgetParent = this.getParentWidget(widget);
                //const left = Math.round(100 * widget.xPosition / widgetParent.width)+"%";
                //const top = Math.round(100 * widget.yPosition / widgetParent.height)+"%";
                const left = widget.xPosition + "px";
                const top = widget.yPosition + "px";
                element.style.left = `${left}`;
                element.style.top = `${top}`;
            }
                break;
            case UIOModelViewOperation.RESIZE_WIDGET: {
                element.style.width = `${widget.width}px`;
                element.style.height = `${widget.height}px`;
                const domData = element.getBoundingClientRect();
                widget.domData = domData;
                if (widget.id === 0) {
                    this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].workareaWidth = domData.width;
                    this.uioModel.UIO.pages[this.uioModel.UIO.currentPageId].workareaHeight = domData.height;
                }
            }
                break;
            case UIOModelViewOperation.UPDATE_WIDGET: {
                this.paintWidget(widget);
            }
                break;
            case UIOModelViewOperation.HIDE_WIDGET: {
                const children = element.childNodes;
                if (widget.hidden) {
                    element.style.height = `0px`;
                    for (let i = 0; i < children.length; i++) {
                        if (children[i].id !== `hide${widget.id}` && children[i].id !== `prop${widget.id}` && children[i].id !== `prophide${widget.id}`)
                            children[i].style.visibility = "hidden";
                    }
                } else {
                    element.style.height = `${widget.height}px`;
                    for (let i = 0; i < children.length; i++) children[i].style.visibility = "visible";
                }
            }
                break;
            default:
                throw new Error(`Unknown or unallowed operation '${mvcOperation}' for changing view`)
        }

    };

    paintWidget(widget) {
        console.log(`2.5.2 --- paint Widget: paint screen with  ${widget.uioName}`, widget.id);
        const appearance = new UIOWidgetAppearance(widget.widgetType);
        const visualObject = document.getElementById(widget.uioName);
        const widgetName = widget.uioName;
        //let widgetParent = this.getParentWidget(widget);
        //const left = Math.round(100 * widget.xPosition / widgetParent.width)+"%";
        //const top = Math.round(100 * widget.yPosition / widgetParent.height)+"%";
        const left = widget.xPosition + "px";
        const top = widget.yPosition + "px";

        visualObject.setAttribute("id", widget.uioName);
        visualObject.setAttribute("style", `position:absolute; left:${left}; top:${top};width:${widget.width}px;height:${widget.height}px;border: 1px white dotted;`);
        visualObject.style.background = appearance.background;
        if (widget.widgetType !== UIOWareWidget.UIOWareFrontend && widget.widgetType !== UIOWareWidget.UIOWareBackend) {
            visualObject.setAttribute("data-uioid", widget.id);
            visualObject.setAttribute("data-uiotype", widget.widgetType);
        }
        widget.domData = visualObject.getBoundingClientRect();

        visualObject.setAttribute("class", appearance.styleClass);
        visualObject.title = `${widget.uioLabel} `;
        const propsIcon = document.getElementById(`prop${widget.id}`);
        const widgetIcon = document.getElementById(`hide${widget.id}`);
        const cloneIcon = document.getElementById(`${widgetName}CloneIcon`);
        const deleteIcon = document.getElementById(`${widgetName}DeleteIcon`);
        const deleteDiv = document.getElementById(`delete${widget.id}`);
        const resizeIcon = document.getElementById(`${widgetName}ResizeIcon`);
        const windowLabel = document.getElementById(`windowLabel${widget.id}`);
        windowLabel.setAttribute("class", "bi bi-tag");
        windowLabel.innerText = widget.uioLabel;
        windowLabel.setAttribute("style", "position:absolute;right:25px;top:5px;  " +
            " color: rgba(0, 0, 0, 1.0); border: 1px white solid;" +
            "background: rgba(4, 160, 255, 0.4); padding: 6px; border-radius: 5px;" +
            " font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif; font-size: 10px; transition: opacity 1s; ");
        windowLabel.title = widget.description;
        resizeIcon.setAttribute("data-uioaction", UIOModelViewOperation.RESIZE_WIDGET);
        resizeIcon.setAttribute("data-uioid", widget.id);

        windowLabel.setAttribute("class", "nofade");
        propsIcon.setAttribute("class", "bi bi-three-dots-vertical");
        widgetIcon.setAttribute("class", appearance.icon);
        cloneIcon.setAttribute("class", "bi bi-patch-plus nofade");
        deleteIcon.setAttribute("class", "bi bi-trash nofade");
        resizeIcon.setAttribute("class", "bi bi-bounding-box-circles nofade");

        widgetIcon.setAttribute("data-uioaction", UIOModelViewOperation.HIDE_WIDGET);
        widgetIcon.setAttribute("data-uioid", widget.id);
        resizeIcon.setAttribute("style", "position:absolute;right:-10px;bottom:-10px;");
        cloneIcon.setAttribute("style", "position:absolute;right:-27px;top:-15px;");
        deleteIcon.setAttribute("style", "position:absolute;right:-10px;top:-10px;");
        cloneIcon.setAttribute("data-uioaction", UIOModelViewOperation.CLONE_WIDGET);
        deleteIcon.setAttribute("data-uioaction", UIOModelViewOperation.DELETE_WIDGET);
        deleteIcon.setAttribute("data-uioid", widget.id);

        propsIcon.style.fontSize = Constants.ICON_SMALL_SIZE;
        widgetIcon.style.fontSize = Constants.ICON_MEDIUM_SIZE;
        cloneIcon.style.fontSize = Constants.ICON_MEDIUM_SIZE;
        deleteIcon.style.fontSize = Constants.ICON_SMALL_SIZE;
        resizeIcon.style.fontSize = Constants.ICON_SMALL_SIZE;
        propsIcon.style.position = "absolute";
        propsIcon.style.left = "17px";
        propsIcon.style.top = "5px";
        propsIcon.setAttribute("onclick", `showProperties(${widget.id})`);
        windowLabel.setAttribute("onclick", `showProperties(${widget.id})`);
        widgetIcon.style.position = "absolute";
        widgetIcon.style.left = "1px";
        widgetIcon.style.top = "1px";

        propsIcon.title = `Properties of ${widget.widgetType} ${widget.id}`;
        widgetIcon.title = `Hide ${widget.widgetType} ${widget.id}`;
        resizeIcon.title = `Resize ${widget.widgetType} ${widget.id}`;
        cloneIcon.title = `Clone ${widget.widgetType} ${widget.id}`;
        deleteIcon.title = `Delete ${widget.widgetType} ${widget.id}`;

        deleteDiv.style.right = "15px";
        deleteDiv.style.top = "15px";
        const innerTag = document.getElementById(`${widget.uioName}InnerTag`);
        if (appearance.innerTag.styleClass) innerTag.setAttribute("class", appearance.innerTag.styleClass);
        if (appearance.innerTag.innerHTML) innerTag.innerHTML = appearance.innerTag.innerHTML;
        if (appearance.innerTag.innerText) innerTag.innerText = appearance.innerTag.innerText;
        if (appearance.innerTag.style.top) innerTag.style.top = appearance.innerTag.style.top;
        if (appearance.innerTag.style.fontSize) innerTag.setAttribute("style", "font-size:" + appearance.innerTag.style.fontSize + "px;");

        if (widget.widgetType === UIOWareWidget.UIOWareApplication || widget.widgetType === UIOWareWidget.UIOWareFrontend || widget.widgetType === UIOWareWidget.UIOWareBackend) {
            resizeIcon.style.visibility = "hidden";
            cloneIcon.style.visibility = "hidden";
            deleteIcon.style.visibility = "hidden";
        }
    }

    buildWidget(widget) {
        console.log(`2.5.1 --- build Widget  ${widget.uioName}`, widget.id);
        const visualObject = document.getElementById(widget.uioName);
        const widgetName = widget.uioName;
        const propsIcon = document.createElement("i");
        const widgetIcon = document.createElement("i");
        const cloneIcon = document.createElement("i");
        const deleteIcon = document.createElement("i");
        const resizeIcon = document.createElement("i");
        const deleteDiv = document.createElement("div");
        const windowLabel = document.createElement("div");
        const appearance = new UIOWidgetAppearance(widget.widgetType);
        let innerTag = document.getElementById(`${widget.uioName}InnerTag`);
        if (!innerTag && appearance.innerTag.tag) {
            innerTag = document.createElement(appearance.innerTag.tag);
            innerTag.setAttribute("id", `${widget.uioName}InnerTag`);
        }
        widgetIcon.setAttribute("id", `hide${widget.id}`);
        propsIcon.setAttribute("id", `prop${widget.id}`);
        cloneIcon.setAttribute("id", `${widgetName}CloneIcon`);
        deleteIcon.setAttribute("id", `${widgetName}DeleteIcon`);
        resizeIcon.setAttribute("id", `${widgetName}ResizeIcon`);
        deleteDiv.setAttribute("id", `delete${widget.id}`);
        windowLabel.setAttribute("id", `windowLabel${widget.id}`);
        deleteDiv.appendChild(cloneIcon);
        deleteDiv.appendChild(deleteIcon);
        visualObject.appendChild(propsIcon);
        visualObject.appendChild(windowLabel);
        visualObject.appendChild(widgetIcon);
        visualObject.appendChild(deleteDiv);
        visualObject.insertBefore(resizeIcon, null);
        if (appearance.innerTag.tag) visualObject.appendChild(innerTag);
        this.paintWidget(widget);
    }

    fade() {
        const noFadeElements = document.getElementsByClassName("nofade");
        if (noFadeElements.length > 0) {
            for (let i = 0; i < noFadeElements.length; i++) {
                if (!noFadeElements[i].classList.contains("fade")) noFadeElements[i].classList.add("fade");
            }
        }
    }

    noFade() {
        const fadeElements = document.getElementsByClassName("nofade");
        if (fadeElements.length > 0) {
            for (let i = 0; i < fadeElements.length; i++) {
                if (fadeElements[i].classList.contains("fade")) fadeElements[i].classList.remove("fade");
            }
        }
    }

    getAllWidgetsObject() {
        return this.uioModel.activeNodes;
    }

    getAllWidgetsArray() {
        const widgets = [];
        for (let i = this.uioModel.oldestNode; i <= this.uioModel.youngestNode; i++) {
            const widget = this.getWidgetById(i);
            if (widget) widgets.push(widget);
        }
        return widgets;
    }

    widgetLabelExists(widgetUioLabel) {
        const widgets = this.getAllWidgetsArray();
        for (let i = 0; i < widgets.length; i++) {
            if (widgets[i].uioLabel.toUpperCase().trim() === widgetUioLabel.toUpperCase().trim()) return true;
        }
        return false;
    }
}

class UIOTool {
    static getTimestamp() {
        return new Date().toISOString();
    };

    static handleMultipleFileSelect(evt, elementID) {
        const files = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
        for (let i = 0, f; f = files[i]; i++) {
            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }
            const reader = new FileReader();
            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    const span = document.createElement('span');
                    span.innerHTML = ['<img class="thumb" src="', e.target.result,
                        '" title="', escape(theFile.name), '"/>'].join('');
                    document.getElementById(elementID).insertBefore(span, null);
                };
            })(f);
            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    static handleUniqueFileSelect(evt, elementId) {
        console.log("handleUniqueFileSelect evt", evt);
        const files = evt.target.files; // FileList object
        // Loop through the FileList and render image files as thumbnails.
        const f = files[0];
        // Only process image files.
        if (!f.type.match('image.*')) {
            UIOTool.errorMsg("File is not an image");
        }
        const reader = new FileReader();
        // Closure to capture the file information.
        reader.onload = (function (theFile) {
            return function (e) {
                // Render thumbnail.
                const span = document.createElement('span');
                span.innerHTML = ['<img class="thumb" src="', e.target.result,
                    '" title="', escape(theFile.name), '"/>'].join('');
                evt.target.nextElementSibling.innerHTML = span.outerHTML;
                document.getElementById(elementId).style.backgroundImage = span.outerHTML;
            };
        })(f);
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);

    }

    static shortString(str, maxLen) {
        if (maxLen > 3 && str && str.length > maxLen) return `${str.substr(0, maxLen - 3)}...`;
        else return str;
    }

    static pad(num) {
        let s = `00${num}`;
        return s.substr(s.length - 3);
    }

    static onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    static confirm() {
        let updateParameterNameField = document.getElementById("UIOToolUpdateTextField");
        let selectBoxSelectedIndex;//, parentOfAddSubNodes;
        let searchWidget = false;
        let createNewPage = false;
        let receiverSelected = false;
        const confirmBox = document.getElementById("UIOToolMessageBox");
        const confirmIcon = document.getElementById("UIOToolBoxConfirm");
        const selectBox = document.getElementById("UIOToolSelectBox");
        //const propertyScreenForm = document.getElementById("propertyScreenForm");
        const showSelectedValueField = document.getElementById("UioReceiverSelectedValue");
        if (selectBox) {
            const selectBoxOptions = selectBox.valueOf().options;
            selectBoxSelectedIndex = selectBoxOptions.selectedIndex;
        }
        const receiverSelectBox = document.getElementById("UIOToolReceiverSelectBox");

        /*
        let addSubNodes = document.getElementById("addSubNodes");
        if (addSubNodes) parentOfAddSubNodes = addSubNodes.parentElement;
        const getParentOfAddSubNodes = function () {
            addSubNodes = document.getElementById("addSubNodes");
            if (addSubNodes) parentOfAddSubNodes = addSubNodes.parentElement;
            if (parentOfAddSubNodes) return parentOfAddSubNodes.getAttribute("id");
            else return null;
        }
        */

        if (receiverSelectBox) {
            const createNew = document.getElementById("UIOToolCreateNewPageInput");
            createNew.style.visibility = "hidden";
            const receiverOptions = receiverSelectBox.valueOf().options;
            const receiverSelectedIndex = receiverOptions.selectedIndex;
            let selectedReceiver = receiverOptions[receiverSelectedIndex].innerText;
            if (selectedReceiver.startsWith("Search")) {
                const filter = document.getElementById("uioSelectionFilter");
                searchWidget = true;
                selectBox.style.visibility = "visible";
                updateParameterNameField.style.visibility = "visible";

                //if (getParentOfAddSubNodes() === "UIOToolMessageBox") {
                //    getPropertyScreen().insertBefore(addSubNodes, propertyScreenForm);
                // }
                filter.focus();
            } else if (selectedReceiver.startsWith("Create")) {
                createNew.style.visibility = "visible";
                //confirmBox.appendChild(addSubNodes);
                createNewPage = true;

                //  selectBox.style.visibility = "hidden";
                //  updateParameterNameField.style.visibility = "hidden";

                // if (getParentOfAddSubNodes() !== "UIOToolMessageBox") {
                //     confirmBox.insertBefore(addSubNodes, selectBox);
                //     addSubNodes.style.visibility = "visible";
                // }
                createNew.focus();
            } else if (!selectedReceiver.startsWith("Select")) {
                createNew.style.visibility = "false";
                receiverSelected = true;
                selectBox.style.visibility = "visible";
                updateParameterNameField.style.visibility = "visible";
                //if (getParentOfAddSubNodes() === "UIOToolMessageBox") {
                //    getPropertyScreen().insertBefore(addSubNodes, propertyScreenForm);
                //}
            }

            if (showSelectedValueField) showSelectedValueField.value = selectedReceiver;
        }
        if (receiverSelectBox && (updateParameterNameField && selectBoxSelectedIndex > 0 && (receiverSelected || createNewPage)))
            confirmIcon.style.visibility = "visible";
        else if (!receiverSelectBox && updateParameterNameField) confirmIcon.style.visibility = "visible";
        else if (!receiverSelectBox && selectBox && selectBoxSelectedIndex > 0) confirmIcon.style.visibility = "visible";
        else confirmIcon.style.visibility = "hidden";

    };

    static transformer(data, dataItemName) {
        let stringArray = [];
        if (dataItemName === 'pages') {
            for (let i = 0; i < data.length; i++) {
                stringArray.push(`page ${data[i].uioName}, (${data[i].uioDescription})`);
            }
        } else if (dataItemName === 'widgets')
            for (let i = controller.uioModel.oldestNode; i <= controller.uioModel.youngestNode; i++) {
                const widget = controller.getWidgetById(i);
                if (widget) {
                    const page = controller.getWidgetPage(widget);
                    const pageName = page.uioName;
                    const pageDesc = page.uioDescription;
                    stringArray.push(`${pageName}, id ${widget.id}: ${UIOTool.shortString(pageDesc, 15)}-${UIOTool.shortString(widget.uioLabel, 15)}`);
                } else console.log("INFO: widget with this id was not found (may be deleted):", i);
            }
        return stringArray;
    }

    static setReceiverFromList(widgetID, value) {
        const element = document.getElementById('UioReceiverSelectedValue');
        element.value = value;
        const parameterName = document.getElementById("UIOToolUpdateTextField");
        const parameterType = document.getElementById("UIOToolSelectBox");
        const UIOToolReceiverSelectBox = document.getElementById("UIOToolReceiverSelectBox");
        const confirmIcon = document.getElementById("UIOToolBoxConfirm");
        if (parameterName.value && parameterType.selectedIndex > 0) {
            UIOToolReceiverSelectBox[5].innerText = value;
            UIOToolReceiverSelectBox.selectedIndex = 5;
            confirmIcon.style.visibility = "visible";
        } else confirmIcon.style.visibility = "hidden";
    }

    static uioFilter(dataItemName, maxItems) {
        let array;
        if (dataItemName === 'pages')
            array = controller.getPages();
        else if (dataItemName === 'widgets')
            array = controller.uioModel.activeNodes;
        else throw new Error(`Invalid dataItemName '${dataItemName}'`);
        const stringArray = UIOTool.transformer(array, dataItemName);

        let inputField = document.getElementById("uioSelectionFilter");
        let dataDiv = document.getElementById("UIOToolArrayForFilter");
        if (!inputField || !dataDiv) return;
        if (dataDiv) dataDiv.innerHTML = "";
        let filterValue = inputField.value.toUpperCase();
        let count = 0;
        const results = [];
        for (let i = 0; i < stringArray.length; i++) {
            if (stringArray[i].toUpperCase().indexOf(filterValue) > -1) {
                count++;
                results.push(stringArray[i]);
                if (count > maxItems)
                    break;
            }
        }


        results.sort(function (a, b) {
            return a.toLowerCase().localeCompare(b.toLowerCase());
        });
        if (dataDiv) {
            for (let j = 0; j < results.length; j++) {
                const br = document.createElement("br");
                const listValue = document.createElement("a");

                const result = results[j].split(" ");
                const widgetID = result[2].substr(0, result[2].indexOf(":"));
                listValue.setAttribute("href", `javascript:UIOTool.setReceiverFromList(${widgetID},'${results[j]}')`);
                listValue.innerText = results[j];
                dataDiv.appendChild(br);
                dataDiv.appendChild(listValue);
            }
        }
    }

    static createSelectionFilter(objectType, maxItems) {
        const div = document.createElement("div");
        const dataDiv = document.createElement("div");
        dataDiv.setAttribute("id", "UIOToolArrayForFilter")
        const inputField = document.createElement("input");
        inputField.setAttribute("class", "uioApiSelect");
        inputField.setAttribute("type", "text");
        inputField.setAttribute("placeholder", "search");
        inputField.setAttribute("type", "text");
        inputField.setAttribute("id", "uioSelectionFilter");
        inputField.setAttribute("onfocus", "UIOTool.confirm()");
        //  ---------------------
        // Keep this just to show and find the function in IDE (!)
        const receiverFilter = function () {
            return "" + UIOTool.uioFilter(objectType, maxItems);
        }
        const htmlFilter = receiverFilter();
        div.setAttribute("title", htmlFilter);
        //  ---------------------
        inputField.setAttribute("onkeyup", `UIOTool.uioFilter('${objectType}',${maxItems})`);
        div.appendChild(inputField);
        div.appendChild(dataDiv);
        div.style.paddingBottom = "10px";
        return div;
    }

    static createCell(uioid, cellData, setIcon, keyPath) {
        const td = document.createElement("td");
        td.setAttribute("data-align", "left");
        if (setIcon) {
            const gearIcon = document.createElement("i");
            gearIcon.setAttribute("class", "bi bi-gear-fill");
            gearIcon.setAttribute("data-uioaction", UIOModelViewOperation.UPDATE_WIDGET);
            gearIcon.setAttribute("id", keyPath);
            gearIcon.setAttribute("data-uioid", uioid);
            gearIcon.innerText = `  ${cellData}  `;
            td.appendChild(gearIcon);
        } else td.innerText = cellData;
        return td;
    }

    static createTable(columnHeaders, data, buildTableBodyCallback) {
        const table = document.createElement("table");
        table.setAttribute("class", "table uioApi w-auto");
        const thead = document.createElement("thead");
        const tr = document.createElement("tr");
        thead.setAttribute("class", "thead-dark");
        for (let i = 0; i < columnHeaders.length; i++) {
            const th = document.createElement("th");
            th.setAttribute("scope", "col");
            th.innerText = columnHeaders[i];
            tr.appendChild(th);
        }
        thead.appendChild(tr);
        table.appendChild(thead);
        table.appendChild(buildTableBodyCallback(data));
        return table;
    }

    static closeReceiverUpdateForm(submitData) {
        const workarea = document.getElementById(controller.getWidgetById(0).uioName);
        const b = document.getElementById("UIOToolMessageBox");
        document.removeEventListener("click", this);
        if (submitData) document.removeEventListener("click", submitData);
        const addSubNodes = document.getElementById("addSubNodes");
        if (addSubNodes.parentElement !== getPropertyScreen())
            getPropertyScreen().insertBefore(addSubNodes, document.getElementById("propertyScreenForm"));
        workarea.removeChild(b);
    };


    static showApiUpdateBox(controller, widget, html, dataID) {
        // TODO: cleanup all unnecessary getElementByID-statements
        const commonPath = dataID.substring(0, dataID.lastIndexOf("\."));
        const workarea = document.getElementById(controller.getWidgetById(0).uioName);
        const updateSituation = html.updateCase;
        let boxDiv = document.getElementById("UIOToolMessageBox");
        //const addSubNodes = document.getElementById("addSubNodes");
        //const propertyScreenForm = document.getElementById("propertyScreenForm");
        //if (addSubNodes.parentElement !== propertyScreenForm) getPropertyScreen().insertBefore(addSubNodes, propertyScreenForm);
        if (boxDiv) {
            workarea.removeChild(boxDiv);
        }
        boxDiv = document.createElement("div");
        const content = document.createElement("div");
        content.setAttribute("id", "UIOToolMessageBoxContent");
        const closeIcon = document.createElement("i");
        const submitIcon = document.createElement("i");
        closeIcon.setAttribute("class", "bi bi-x");
        closeIcon.setAttribute("style", "font-size:1.25em;");
        submitIcon.setAttribute("class", "bi bi-check-circle-fill");
        submitIcon.setAttribute("style", "font-size:1.25em;visibility:hidden");
        submitIcon.setAttribute("id", "UIOToolBoxConfirm");
        boxDiv.setAttribute("id", "UIOToolMessageBox");
        boxDiv.setAttribute("class", "border border-dark rounded uioApi");
        boxDiv.style.position = "absolute";
        boxDiv.style.background = "white";
        boxDiv.style.width = "auto";
        boxDiv.style.height = "auto";
        boxDiv.style.zIndex = "3";
        boxDiv.style.padding = "10px";
        boxDiv.appendChild(closeIcon);
        closeIcon.addEventListener("click", UIOTool.closeReceiverUpdateForm);

        workarea.appendChild(boxDiv);
        boxDiv.appendChild(content);
        boxDiv.appendChild(submitIcon);
        submitIcon.style.visibility = "hidden";
        content.innerHTML = html.html;
        boxDiv.style.left = Math.round(controller.getCurrentUioPosition().x) + "px";
        boxDiv.style.top = Math.round(controller.getCurrentUioPosition().y) + "px";
        boxDiv.focus();
        const parameterNameElement = document.getElementById("UIOToolUpdateTextField");
        let currentName;
        if (updateSituation === "delete") {
            submitIcon.style.visibility = "visible";
        } else if (parameterNameElement && updateSituation === "inputField")
            currentName = controller.getObjectAttribute(dataID);
        else if (parameterNameElement && updateSituation === "form")
            currentName = controller.getObjectAttribute(commonPath + ".name");
        // set parameter-name
        if (parameterNameElement && currentName) parameterNameElement.value = currentName;
        const parameterTypeSelectBox = document.getElementById("UIOToolSelectBox");
        if (parameterTypeSelectBox && (updateSituation === "form" || updateSituation === "selectBox")) {
            const parameterTypeOptions = parameterTypeSelectBox.valueOf().options;
            const currentType = controller.getObjectAttribute(commonPath + ".type");
            // select parameter-type
            for (let i = 0; i < parameterTypeOptions.length; i++) {
                if (parameterTypeOptions[i].innerText === currentType) {
                    parameterTypeSelectBox.selectedIndex = `${i}`;
                    break;
                }
            }
            // select receiver
            if (updateSituation === "form") {
                const receiverSelectBox = document.getElementById("UIOToolReceiverSelectBox");
                const currentReceiver = controller.getObjectAttribute(dataID);
                const receiverOptions = receiverSelectBox.valueOf().options;
                for (let i = 0; i < receiverOptions.length; i++) {
                    if (receiverOptions[i].innerText.split(" ")[3] === `${currentReceiver}:`) {
                        receiverSelectBox.selectedIndex = `${i}`;
                        break;
                    }
                }

            }
        }
        const submitData = function () {
            let element, wordList;
            let apiUpdate = {};
            const apiUpdateReceiver = {};
            apiUpdate.key = dataID;
            const actionIcon = document.getElementById(dataID);
            const addNewSituation = actionIcon.innerText.toLowerCase().trim();
            console.log("addNewSituation", addNewSituation)
            let createNewPage = false;
            const changedProperties = [];
            if (updateSituation === "delete") {
                apiUpdate.key = dataID.substring(dataID.indexOf("-") + 1);
                apiUpdate.value = "/delete/";
                console.log("apiUpdate", apiUpdate);
                changedProperties.push(apiUpdate);
                const removerItem = document.getElementById(dataID);
                const deactivated = getPropertyScreen().getElementsByTagName("i");
                for (let i = 0; i < deactivated.length; i++) {
                    if (deactivated[i].getAttribute("data-uioid") === widget.id ||
                        deactivated[i].getAttribute("data-uioaction") === UIOModelViewOperation.UPDATE_WIDGET) {
                        deactivated[i].removeAttribute("data-uioid");
                        deactivated[i].removeAttribute("data-uioaction");
                        if (deactivated[i] === removerItem) {
                            const refresh = document.createElement("i");
                            refresh.innerText = ` Please click to refresh data.`;
                            refresh.setAttribute("onclick", `refreshPropertyScreen(${widget.id})`)
                            deactivated[i].style.opacity = "0.9";
                            deactivated[i].innerText = ``;
                            deactivated[i].appendChild(refresh);
                            refresh.setAttribute("class", "bi bi-arrow-clockwise");
                        } else {
                            deactivated[i].style.opacity = "0.75";
                            deactivated[i].setAttribute("class", "bi bi-hourglass-top");
                        }
                    }
                }
            } else if (updateSituation === "inputField") {
                element = document.getElementById("UIOToolUpdateTextField");
                apiUpdate.value = element.value;

                const action = actionIcon.innerText.toLowerCase().trim();
                console.log("action", action)
                // In case we are only updating the field, we must set the icon's text as its label
                // In case we are adding a new field (see below), a new row will be appended
                if (action.indexOf("add new statement") > -1) {
                    actionIcon.innerText = "Add new statement";
                } else document.getElementById(apiUpdate.key).innerText = apiUpdate.value;
                changedProperties.push(apiUpdate);
            } else if (updateSituation === "selectBox") {
                element = document.getElementById("UIOToolSelectBox");
                const options = element.valueOf().options;
                const selectedIndex = options.selectedIndex;
                apiUpdate.value = options[selectedIndex].innerText;
                document.getElementById(apiUpdate.key).innerText = apiUpdate.value;
                changedProperties.push(apiUpdate);
            } else if (updateSituation === "form") {
                /**   ------------ build receiver update Form ------------     **/
                if (addNewSituation.indexOf("parameter") <= -1) {
                    const receiverSelectBox = document.getElementById("UIOToolReceiverSelectBox");
                    const parameterTypeOptions = document.getElementById("UIOToolSelectBox");
                    const uioReceiverSelectedValue = document.getElementById("UioReceiverSelectedValue");
                    const receiverOptions = receiverSelectBox.valueOf().options;
                    let parameterNameEditedValue;
                    if (parameterNameElement.value) parameterNameEditedValue = parameterNameElement.value;
                    const parameterTypeSelectedIndex = parameterTypeOptions.selectedIndex;
                    const parameterTypeSelectedValue = parameterTypeOptions[parameterTypeSelectedIndex].innerText;
                    const receiverSelectedIndex = receiverOptions.selectedIndex;
                    const receiverSelectedValue = receiverOptions[receiverSelectedIndex].innerText;
                    if (receiverSelectedValue.toLowerCase().indexOf("create new receiver") > -1) createNewPage = true;
                    if (receiverSelectedValue) uioReceiverSelectedValue.value = receiverSelectedValue;
                    const apiUpdateName = {};
                    const apiUpdateType = {};
                    apiUpdateName.key = commonPath + ".name";
                    apiUpdateType.key = commonPath + ".type";
                    apiUpdateReceiver.key = dataID;
                    apiUpdateName.value = parameterNameEditedValue;
                    apiUpdateType.value = parameterTypeSelectedValue;
                    wordList = uioReceiverSelectedValue.value.split(" ");
                    apiUpdateReceiver.value = Number(wordList[2].substring(0, wordList[2].indexOf(":")));

                    //  console.log("apiUpdateReceiver,wordList", apiUpdateReceiver, wordList)
                    changedProperties.push(apiUpdateName);
                    changedProperties.push(apiUpdateType);
                    changedProperties.push(apiUpdateReceiver);
                    const paramNameElement = document.getElementById(apiUpdateName.key);
                    const paramTypeElement = document.getElementById(apiUpdateType.key);
                    if (paramNameElement) paramNameElement.innerText = parameterNameEditedValue;
                    if (paramTypeElement) paramTypeElement.innerText = parameterTypeSelectedValue;
                }
                /**   ------------ End build receiver update Form ------------     **/

            } else UIOTool.errorMsg(`invalid update situation '${updateSituation}'`);

            let chosenReceiver;
            let page = "?";
            if (wordList)
                page = wordList[0].substring(0, wordList[0].indexOf(","));

            // This is the "add something" situation
            if (addNewSituation.indexOf("add new") > -1) {
                console.log("addNewSituation:", addNewSituation);

                const idAttr = actionIcon.getAttribute("id");
                const idx1 = idAttr.lastIndexOf("[");
                const idx2 = idAttr.lastIndexOf("]");
                const oldIndex = Number(idAttr.substring(idx1 + 1, idx2));
                const newIndex = oldIndex + 1;
                const newIDAttr = `${idAttr.substring(0, idx1)}[${newIndex}]`;

                actionIcon.setAttribute("id", newIDAttr);
                const currentRow = actionIcon.parentElement.parentElement;
                const currentTable = actionIcon.parentElement.parentElement.parentElement;
                const newRow = document.createElement("tr");
                currentTable.insertBefore(newRow, currentRow);
                let td = document.createElement("td");

                newRow.appendChild(td);
                td = document.createElement("td");
                newRow.appendChild(td);
                const newIcon = document.createElement("i");
                newIcon.setAttribute("class", "bi bi-gear-fill");

                console.log("dataID", dataID);
                if (addNewSituation.indexOf("parameter") > -1 || addNewSituation.indexOf("receiver") > -1) {
                    let parameterNameEditedValue;
                    const parameterTypeOptions = document.getElementById("UIOToolSelectBox");
                    if (parameterNameElement.value) parameterNameEditedValue = parameterNameElement.value;
                    const parameterTypeSelectedIndex = parameterTypeOptions.selectedIndex;
                    const parameterTypeSelectedValue = parameterTypeOptions[parameterTypeSelectedIndex].innerText;
                    const receiverSelectBox = document.getElementById("UIOToolReceiverSelectBox");
                    const receiverOptions = receiverSelectBox.valueOf().options;
                    const receiverSelectedIndex = receiverOptions.selectedIndex;
                    const receiverSelectedValue = receiverOptions[receiverSelectedIndex].innerText;
                    const uioReceiverSelectedValue = document.getElementById("UioReceiverSelectedValue");
                    if (receiverSelectedValue) uioReceiverSelectedValue.value = receiverSelectedValue;
                    wordList = uioReceiverSelectedValue.value.split(" ");
                    const receiverID = Number(wordList[2].substring(0, wordList[2].indexOf(":")));
                    chosenReceiver = controller.getWidgetById(receiverID);
                    td = document.createElement("td");
                    newRow.appendChild(td);
                    newIcon.innerText = `${page}, ${UIOTool.shortString(chosenReceiver.uioLabel, 15)}`;
                    if (addNewSituation.indexOf("parameter") > -1) {
                        const apiUpdateParameter = {};
                        const newParameter = {};
                        newParameter.name = parameterNameEditedValue;
                        newParameter.type = parameterTypeSelectedValue;
                        const parameterReceiver = [];
                        parameterReceiver.push(receiverID);
                        newParameter.receiverWidgets = parameterReceiver;
                        apiUpdateParameter.key = dataID;
                        apiUpdateParameter.value = newParameter;
                        changedProperties.push(apiUpdateParameter);
                    }
                    // This is the add receiver situation
                    else if (addNewSituation.indexOf("receiver") > -1) {
                        td = document.createElement("td");
                        newRow.appendChild(td);
                        td = document.createElement("td");
                        td.innerText = " ** new **  ";
                        newRow.appendChild(td);
                        changedProperties.push(apiUpdateReceiver);
                    }
                } else if (addNewSituation.indexOf("statement") > -1) {
                    newIcon.innerText = ` ** new **  ${apiUpdate.value}`;
                }

                td.appendChild(newIcon);

            } else if (apiUpdateReceiver && chosenReceiver) document.getElementById(apiUpdateReceiver.key).innerText = `${page}, ${chosenReceiver.uioLabel} `;

            if (createNewPage) {
                const pageIndex = controller.getPages().length;
                const pageDescription = document.getElementById("UIOToolCreateNewPageInput").value;
                if (controller.modelViewChanged(widget, [pageDescription], UIOModelViewOperation.CREATE_PAGE)) {
                    const widget0 = controller.getWidgetById(0);
                    const newWindowWidget = controller.createNewWidget(0, UIOWareWidget.UIOWareWindow, widget0.width / 4, widget0.height / 4, widget0.width / 4, widget0.height / 4);
                    apiUpdateReceiver.value = newWindowWidget.id;
                    console.log("Create new receiver, dataID=", dataID);
                    const newReceiver = document.getElementById(dataID);
                    if (newReceiver) {
                        newReceiver.innerText = newWindowWidget.uioLabel;
                        controller.getPages()[pageIndex].mappedNodes.push(newWindowWidget.id);
                        const removeIndex = controller.getPages().indexOf(newWindowWidget.id);
                        controller.getPages()[controller.getCurrentPage().uioId].mappedNodes.splice(removeIndex, 1);
                        controller.modelViewChanged(widget, changedProperties, UIOModelViewOperation.UPDATE_WIDGET);
                        // remove this new widget from the current screen. It should be visible only on the new page
                        document.getElementById(newWindowWidget.uioName).remove();
                    } else {
                        UIOTool.errorMsg(`Could not place new receiver on new page.`);
                        console.error(`Element  ${newReceiver} to display  path ${dataID} was not found`);
                        //something went wrong with the new receiver. So remove it from the current screen
                        controller.modelViewChanged(newWindowWidget, [], UIOModelViewOperation.DELETE_WIDGET);
                    }
                } else UIOTool.errorMsg("Could not create a new page!");
            } else controller.modelViewChanged(widget, changedProperties, UIOModelViewOperation.UPDATE_WIDGET);

            UIOTool.closeReceiverUpdateForm(submitData);
            refreshPropertyScreen(widget.id);
        };
        submitIcon.addEventListener("click", submitData);
    }

    static createOptionList(valueArray, styleClassName, ariaLabel, callback, selectBoxID) {
        let options = [];
        const div = document.createElement("div");
        const select = document.createElement("select");
        valueArray.unshift("Select value");
        select.setAttribute("class", styleClassName);
        select.setAttribute("onchange", callback);
        select.setAttribute("aria-label", ariaLabel);
        if (!selectBoxID)
            select.setAttribute("id", "UIOToolSelectBox");
        else select.setAttribute("id", selectBoxID);
        for (let i = 0; i < valueArray.length; i++) {
            const option = document.createElement("option");
            option.setAttribute("class", styleClassName);
            options.push(option);
            options[i].value = i;
            if (i === 0) {
                option.setAttribute("aria-label", "empty list-option");
                option.setAttribute("disabled", "true");
                option.setAttribute("selected", "true");
            } else
                option.setAttribute("aria-label", valueArray[i]);
            options[i].innerText = valueArray[i];
            select.appendChild(options[i]);
        }
        div.appendChild(select);
        return div;
    }

    static buildReceiverForm(callback) {
        const parameterNameField = `<label>Parameter-Name:</label><br><input id="UIOToolUpdateTextField" type="text" class="uioApiSelect" placeholder="Type new value" onfocus="UIOTool.confirm()"><br>`;
        return parameterNameField + `<label>Parameter-Type:</label><br>` + UIOTool.buildDataTypeSelectionList(callback).outerHTML + UIOTool.buildReceiverSelectionList(callback).outerHTML;
    }

    static createUpdateHTML(widget, keyPath, callback) {
        let html = {};
        const keyPathPattern = keyPath.replace(widget.uioName, "").replace(/[0123456789]/g, "");
        let updateConstellation = 0;
        html.updateCase = "inputField";
        html.html = `<input id="UIOToolUpdateTextField" type="text" class="uioApiSelect" placeholder="Type new value" onfocus="UIOTool.confirm()">`;
        // BEWARE: do not delete double dots in the pattern!
        switch (keyPathPattern) {
            case `deleteDML-activeNodes..apiCalls[].apiDMLs`:
            case `deleteDML-activeNodes..apiCalls[].apiDMLs[]`:
                html.updateConstellation = 1;
                html.updateCase = "delete";
                html.html = `<label><b>Delete this statement?</b></label>`;
                break;
            case `deleteParameter-activeNodes..apiCalls[].apiParameters`:
            case `deleteParameter-activeNodes..apiCalls[].apiParameters[]`:
                html.updateConstellation = 2;
                html.updateCase = "delete";
                html.html = `<label><b>Delete this parameter?</b></label>`;
                break;
            case `delete-UIO.pages[]`:
                html.updateConstellation = 3;
                html.updateCase = "delete";
                html.html = `<label><b>Delete this page?</b></label>`;
                break;
            case `deleteReceiver-activeNodes..apiCalls[].apiParameters[].receiverWidgets`:
            case `deleteReceiver-activeNodes..apiCalls[].apiParameters[].receiverWidgets[]`:
                html.updateConstellation = 4;
                html.updateCase = "delete";
                html.html = `<label><b>Delete this receiver?</b></label>`;
                break;
            case `activeNodes..apiCalls[].apiParameters[].receiverWidgets`:
            case `activeNodes..apiCalls[].apiParameters[].receiverWidgets[]`:
            case `activeNodes..apiCalls[].apiParameters[]`:
                html.updateConstellation = 5;
                html.updateCase = "form";
                html.html = UIOTool.buildReceiverForm(callback);
                break;
            case `activeNodes..apiCalls[].eventName`:
                html.updateConstellation = 6;
                html.updateCase = "selectBox";
                html.html = UIOTool.buildEventSelectionList(callback).outerHTML;
                break;
            case `activeNodes..apiCalls[].method`:
                html.updateConstellation = 7;
                html.updateCase = "selectBox";
                html.html = UIOTool.buildHttpMethodSelectionList(callback).outerHTML;
                break;
            case `activeNodes..apiCalls[].apiParameters[].type`:
                html.updateConstellation = 8;
                html.updateCase = "selectBox";
                html.html = UIOTool.buildDataTypeSelectionList(callback).outerHTML;
                break;
            case `activeNodes..apiCalls[].status`:
                html.updateConstellation = 9;
                html.updateCase = "selectBox";
                html.html = UIOTool.buildSpecificationStatusSelectionList(callback).outerHTML;
                break;
            case `activeNodes..widgetType`:
                html.updateConstellation = 10;
                html.updateCase = "selectBox";
                html.html = UIOTool.buildUIOTypeSelectionList(callback).outerHTML;
                break;
            case `activeNodes..apiCalls[].apiParameters[].name`:
            case `activeNodes..apiCalls[].frontendRequirement`:
            case `activeNodes..apiCalls[].backendRequirement`:
            case `activeNodes..apiCalls[].apiDMLs[]`:
            case `UIO.pages[].uioDescription`:
                html.updateConstellation = 11;
                break;
            default:
                html.updateConstellation = 12;
                html.updateCase = "Error";
                html.html = `<label>Error: Could not determine valuePath</label>
               <label>ERROR: unknown path '${keyPath}' (pattern ${keyPathPattern})</label>`;
        }
        if (html.updateConstellation === 5 || html.updateConstellation === 7) {
            console.info("keyPath.split([)", keyPath.split("["));
        }
        return html;
    }


    static buildReceiverSelectionList(callback) {
        const MAX_WIDGETS = 7;

        let optionList, selectionFilter;
        const pageList = controller.getPages();
        console.log(" buildReceiverSelectionList  MAX_WIDGETS=" + MAX_WIDGETS + ", pageList=" + pageList)

        const div = document.createElement("div");
        if (pageList.length === 1) UIOTool.errorMsg("Model contains only page 0. Uio-models must have always at least two pages.");
        else if (pageList.length > 1) {
            let maxWidgetListLength;
            const widgetNamesList = [];
            const numberOfWidgets = controller.uioModel.activeNodes.widgetCount;
            if (numberOfWidgets > MAX_WIDGETS) {
                console.log("create searchable list of widgets (numberOfWidgets > MAX_WIDGETS)", numberOfWidgets, MAX_WIDGETS)
                maxWidgetListLength = MAX_WIDGETS;
                widgetNamesList.push("Search widget ...");
                selectionFilter = UIOTool.createSelectionFilter('widgets', MAX_WIDGETS);
                console.log("selectionFilter", selectionFilter);
                selectionFilter.visibility = "visible";

            } else maxWidgetListLength = 0;
            let count = 0;
            for (let p = 1; p < pageList.length; p++) {
                const widgetList = pageList[p].mappedNodes;
                const pageName = pageList[p].uioName;
                for (let i = 0; i < widgetList.length; i++) {
                    count++;
                    let widget = controller.getWidgetById(widgetList[i]);
                    let pageDesc, widgetLabel;
                    const pageDescription = pageList[p].uioDescription;
                    if (pageDescription.length > 15) pageDesc = `${pageDescription.substr(0, 12)}...`;
                    else pageDesc = pageDescription;
                    if (widget.uioLabel.length > 20) widgetLabel = `${widget.uioLabel.substr(0, 17)}...`;
                    else widgetLabel = widget.uioLabel;
                    if (widget) widgetNamesList.push(`${pageName}, id ${widget.id}: ${pageDesc}-${widgetLabel}`);
                    if (count === maxWidgetListLength) {
                        break;
                    }
                }
            }
            widgetNamesList.push("Create new receiver page  ...");
            optionList = UIOTool.createOptionList(widgetNamesList, "uioApiSelect",
                "selection of receiver-widgets", callback, "UIOToolReceiverSelectBox");

            const receiverLabel = document.createElement("label");
            receiverLabel.innerText = "Receiver:"

            const newPageName = document.createElement("input");
            newPageName.type = "text";
            newPageName.placeholder = "Create a new page";
            newPageName.setAttribute("id", "UIOToolCreateNewPageInput");
            newPageName.setAttribute("onfocus", "UIOTool.confirm()");
            newPageName.style.paddingBottom = "15px";
            newPageName.style.visibility = "hidden";

            const selectedValue = document.createElement("input");
            selectedValue.setAttribute("id", "UioReceiverSelectedValue");
            selectedValue.setAttribute("class", "uioApiSelect");
            selectedValue.setAttribute("disabled", "true");
            selectedValue.setAttribute("size", "50");
            selectedValue.style.backgroundColor = "white";
            selectedValue.style.border = "none";
            selectedValue.style.paddingTop = "5px";


            let br = document.createElement("br");
            div.appendChild(br);
            div.appendChild(receiverLabel);

            br = document.createElement("br");
            div.appendChild(br);
            div.appendChild(optionList);

            if (selectionFilter) {
                br = document.createElement("br");
                div.appendChild(br);
                div.appendChild(selectionFilter);
            }

            br = document.createElement("br");
            div.appendChild(br);
            div.appendChild(newPageName);

            br = document.createElement("br");
            div.appendChild(br);
            div.appendChild(selectedValue);
            return div;
        } else {
            UIOTool.errorMsg("Error: number of found pages is: " + pageList.length);
        }

    }

    static buildEventSelectionList(callback) {
        const eventNames = ["blur", "change", "click", "dblclick", "focus", "keydown", "keypress", "keyup", "load", "mouseenter", "mousedown", "mouseleave",
            "mousemove", "mouseover", "mouseout", "mouseup", "submit", "wheel"];
        return UIOTool.createOptionList(eventNames, "uioApiSelect", "selection of" +
            " trigger events for api-calls", callback);
    }


    static buildHttpMethodSelectionList(callback) {
        const httpNames = ["GET", "HEAD", "OPTIONS", "POST", "PUT", "OTHER HTTP", "NONE"];
        return UIOTool.createOptionList(httpNames, "uioApiSelect",
            "selection of HTTP methods  for api-calls", callback);
    }


    static buildDataTypeSelectionList(callback) {
        const typeNames = ["boolean", "[DATASET]", "date", "datetime", "double", "integer", "long", "string", "time"];
        return UIOTool.createOptionList(typeNames, "uioApiSelect",
            "selection of data types for api-calls", callback);
    }

    static buildSpecificationStatusSelectionList(callback) {
        const typeNames = ["proposed", "confirmed", "implementation", "completed"];
        return UIOTool.createOptionList(typeNames, "uioApiSelect",
            "selection of status for api-specification", callback);
    }

    static buildUIOTypeSelectionList(callback) {
        const typeNames = [UIOWareWidget.UIOWareWindow, UIOWareWidget.UIOWareImage, UIOWareWidget.UIOWareInput,
            UIOWareWidget.UIOWareMockButton, UIOWareWidget.UIOWareMockLink, UIOWareWidget.UIOWareMockRadioCheckBox,
            UIOWareWidget.UIOWareMockSelectionList, UIOWareWidget.UIOWareMockTextarea, UIOWareWidget.UIOWareMockTextField,
            UIOWareWidget.UIOWareTable, UIOWareWidget.UIOWareOutput, UIOWareWidget.UIOWareIcon];
        return UIOTool.createOptionList(typeNames, "uioApiSelect",
            "selection of status for api-specification", callback);
    }

    static buildUIOPageTypeSelectionList(callback) {
        const pageTypes = [UIOWarePageType.SECTION, UIOWarePageType.OVERFLOW, UIOWarePageType.FILE];
        return UIOTool.createOptionList(pageTypes, "uioApiSelect", "selection of page-type", callback, "editPageType");
    }

    static Msg(html) {
        console.info(html);
        const elem = document.getElementById("msg");
        showMessage(elem, html);
    }

    static errorMsg(msg) {
        console.error(msg);
        const elemText = document.getElementById("errorMessageText");
        const elem = document.getElementById("errorMessage");
        elem.style.visibility = "visible";
        elemText.innerText = ` ${msg} `;
    }
}

class UIOClient {
    createPage(pageId) {

    }

    deletePage(pageId) {

    }
}