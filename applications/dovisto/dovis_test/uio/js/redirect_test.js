"use strict";

const DOVISIT_HOME = `https://home.dovis.it/dovisit/index.html`;

const redirect = function () {
    const webAdress = window.location.href;
    const shortUrlIndex = 1 + webAdress.lastIndexOf("?");
    const shortUrlParam = webAdress.substring(shortUrlIndex);
    let showUrlOnly = false;
    let shortUrl;
    if (shortUrlParam.endsWith('%2b')) {
        shortUrl = shortUrlParam.substring(0, shortUrlParam.length - 3);
        //console.log("shortUrl: " + shortUrl);
        showUrlOnly = true;
    } else {
        shortUrl = shortUrlParam;
    }
    if (!shortUrl) window.location.href = DOVISIT_HOME;
    document.getElementById("shorturl").innerText = `  dovis.it/${shortUrl}`;

    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", `${selectedServer}800/8/public/select?shortcut=${shortUrl}`, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xhttpState = -1;
    xhttp.onreadystatechange = function () {
        xhttpState = xhttp.readyState;
        if (xhttpState === 4) {
            if (xhttp.status > 0 && xhttp.status <= 204) {
                let jsonObject = JSON.parse(xhttp.responseText);
                if (jsonObject.count === 0) {
                    document.getElementById("redirection").innerHTML = `dovis.it/${shortUrl}&nbsp;&nbsp;
                    <i class="fas fa-unlink"></i><br/>
                    <img src="img/dovisit_logo.png" style="max-height: 100px; max-width: 100px;"><br/>&nbsp;&nbsp;AVAILABLE!&nbsp;&nbsp;<i class="fas fa-check"></i>`;
                    document.getElementById("redirectionUrl").setAttribute("href", `${DOVISIT_HOME}?shorturl=${shortUrl}`);
                } else {
                    const page = jsonObject.rows[0].s[2];
                    const decodedPage = decodeURIComponent(page);
                    console.log(`page/decoded page: ${page} / ${decodedPage}`);
                    document.getElementById("redirection").innerHTML = `dovis.it/${shortUrl}&nbsp;&nbsp;
                                                          <i class="fas fa-arrow-right"></i>&nbsp;&nbsp;${decodedPage}<i class="fas fa-link"></i>&nbsp;`;
                    document.getElementById("redirectionUrl").setAttribute("href", `${decodedPage}`);
                    if (!showUrlOnly)
                        window.location.href = decodedPage;
                }
            } else {
                document.getElementById("redirection").innerHTML = `<br/><i class="fas fa-exclamation-circle"></i><br/>&nbsp;&nbsp;ERROR:&nbsp;&nbsp;HTTP${xhttp.status}&nbsp;&nbsp;`;
                document.getElementById("redirectionUrl").setAttribute("href", `${DOVISIT_HOME}`);
            }
        }
    };
    try {
        xhttp.send();
    } catch (err) {
        console.error(err.toString());
        document.getElementById("redirection").innerHTML = `<br/><i class="fas fa-exclamation-triangle"></i><br/>&nbsp;&nbsp;UNKNOWN ERROR OCCURRED&nbsp;&nbsp;`;
        document.getElementById("redirectionUrl").setAttribute("href", `${DOVISIT_HOME}`);
    }
};

