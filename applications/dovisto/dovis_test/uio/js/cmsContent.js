const cmsContent = {
    contact: `<div class="border border-light rounded" style="padding:15px;">

               <p>Enpasoft GmbH<br>
               Küferstrasse 12<br>
               69168 Wiesloch, Germany<br>
               <i class="fa fa-phone"></i>+49-6222-3171090</p> 
               <ul>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:support@dovis.it?subject=Referred to dovis.it:">${TEXT_GENERAL_ENQUIRIES}</a>&nbsp;<i class="fa fa-info fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=BUG-description:">${TEXT_SUBMIT_BUG}</a>&nbsp;<i class="fa fa-bug fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=FEATURE-Request:">${TEXT_SUBMIT_FEATURE}</a>&nbsp;<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i></li>
               </ul>`,
    about: `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
            <b>${TEXT_FREE_OF_CHARGE}</b><br><br><br>
            ${TEXT_PERSONAL_USE}<br>
                <ul>
                    <li> ${getTextById(105)}</li>
                    <li> ${getTextById(106)}</li>
                    <li > ${getTextById(107)}</li>
                    <li > ${getTextById(108)}</li>
                    <li > ${getTextById(109)}</li>
                </ul>
              ${TEXT_PROFESSIONAL_USE} - ${TEXT_MAILINGS}<br>
                 <ul>
                    <li > ${getTextById(112)}</li>
                    <li > ${getTextById(113)}</li>
                    <li > ${getTextById(114)}</li> 
               </ul>
               ${TEXT_CORS} <a href="#footnote-1">&nbsp;*&nbsp;</a> <br>
                 <ul>
                    <li > ${getTextById(116)}</li>
                    <li > ${getTextById(117)}</li>
                    <li > ${getTextById(118)}</li> 
               </ul>
               <p id="footnote-1">
            *<i class="fa fa-link" aria-hidden="true"> </i>&nbsp;<a href="https://www.dovis.it/CORSmdn" target="_parent">dovis.it/CORSmdn</a> &nbsp;
            <a href="https://www.dovis.it/CORSwiki" target="_parent">dovis.it/CORSwiki</a>
               <br><br>
            </div>`,
    footer: `<div class="container">
        <div class="custom-footer">
            <div class="row">
                <div class="col-md-6 col-lg-3 about-footer">
                    <img src="img/whitelogo.png" alt="">
                    <p>

                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a urna at leo pellentesque hendrerit. Suspendisse auctor tortor ut lorem dictum facilisis.

                    </p>
                    <a href="" class="btn red-btn">Book Now</a>
                </div>
                <div class="col-md-6 col-lg-3 page-more-info">

                    <ul>
                        <div class="footer-title">
                            <h5>Page links</h5>
                        </div>
                        <li><a href="#">Home</a></li>
                        <li>  <a class="dovis_footer list-inline-item  cms btn btn-outline-light" data-page="about">
                           ${getTextById(57)}</a> 
                        </li>
                        <li>  <a class="dovis_footer list-inline-item cms btn  btn-outline-light" data-page="howto">
                            ${getTextById(140)}</a>
                         </li>
                        <li><a href="#">Pricing</a></li>
                    </ul>
                </div>

                <div class="col-md-6 col-lg-3 page-more-info">

                    <ul>
                        <div class="footer-title">
                            <h5>Support</h5>
                        </div>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li> <a class="dovis_footer list-inline-item cms btn  btn-outline-light" data-page="contact">
                            ${getTextById(58)}</a>
                        </li>

                    </ul>
                </div>
                <div class="col-md-6 col-lg-2 page-more-info">

                    <ul>
                        <div class="footer-title">
                            <h5>Social</h5>
                        </div>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Instagram</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Youtube</a></li>


                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="footer-bottom">
            <div class="row">

                <div class="col-sm-8">
                    <p>© dovis.it 2022. Version: 1.22/1.3.2 All Rights Reserved.</p>
                </div>
                <div class="col-sm-3 ">
                    <a href="#">Terms Of Use</a> |
                    <a href="#">Privacy policy</a>
                </div>
            </div>
        </div>
    </div>`,
    howto: `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
                <ul>
                    <li> <a href="https://www.dovis.it/WTTA5A" target="_blank">${getTextById(141)}</a></li>
                    <li> <a href="https://www.dovis.it/G3C79A" target="_blank">${getTextById(142)}</a></li>
                    <li> <a href="https://www.dovis.it/FSXXMA" target="_blank">${getTextById(143)}</a></li>
                    <li> <a href="https://www.dovis.it/XAOMLY" target="_blank">${getTextById(144)}</a></li> 
                </ul>
            </div>`,

}