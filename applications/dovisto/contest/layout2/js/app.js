/*
*
* Web Preloader Start
*
*/
$(document).ready(function(){

  setTimeout(function(){
  $('.loder-var , .loader').fadeToggle();
  },1500);

});
/*
*
* End
*
*/


/*
*
* Web Navbar Scrolling Script Start
*
*/
$(function() {

    var header = $(".navbar-main");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 10) {
            header.removeClass('navbar-main').addClass("navbarScroll");
        } else {
            header.removeClass("navbarScroll").addClass('navbar-main');
        }
    });

});
/*
*
* End
*
*/



/*
*
* Jump Top Button Script Start
*
*/
$(document).ready(function () {

    mybutton = document.getElementById("myBtn");

    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
      } else {
        mybutton.style.display = "none";
      }
    }

    function topFunction() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }


});
/*
*
* End
*
*/