"use strict";
const errordiv = "dovis-errors";
const TableStyle = {
    loadingIcon: `<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>`,
    pre: ``,
    classes: `table table-hover table-striped table-sm text-sm-left dataTable`,
    colOpen: `<col>`,
    colClose: `</col>`,
    tableHeadOpen: `<thead><tr>`,
    tableHeadClose: `</tr></thead><tbody>`,
    thOpen: `<th class="text-sm-left">`,
    thClose: `</th>`,
    trOpen: `<tr class="text-left">`,
    trClose: `</tr>`,
    tdOpen: `<td class="text-left">`,
    tdClose: `</td>`,
    aOpen: `<a`,
    aAttributes: `>`,
    aClose: `</a>`,
    bottom: `</table>`,
    elements: [],
};

const FormStyle = {
    pre1: '<div class="row"><div class="col-md-',
    pre2: '"><div class="box box-primary"><form role="form" id=',
    pre3: ' enctype="multipart/form-data"><div class="box-body">',
    suf: '</div></form></div></div></div>',
}

const styleLoginLayout = function (userName) {
    $("#login").after(`&nbsp;<a class="btn btn-primary custom-btn click_usersettings text-light" id ="settings" style="z-index: 2;"><i class="fa fa-user-circle"></i> &nbsp;${userName}</a>&nbsp;`);
    $("#settings").before(`<a class="btn btn-primary custom-btn doLogout" id ="logoutButton"><i class="fa fa-power-off"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary custom-btn click_usershortcuts" id ="showUserUrlsButton"><i class="fa fa-list-ul"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary custom-btn show_submit" id ="showSubmitButton"><i class="fa fa-plus-square icon-link"></i></a>&nbsp;`);
}

const styleSettingsPageLayout = function (storedData, last_login, username, email, firstNameValue, firstNamePlaceholder,
                                          lastNameValue, lastNamePlaceholder, userid, lastUrlsArray) {
    let lastUrls = ``;
    let mailings = ``;
    for (let r = 0; r < lastUrlsArray.length; r++) {
        const href = lastUrlsArray[r].href;
        const urlSubstr = lastUrlsArray[r].urlSubstr;
        lastUrls += `<a href="${href}">
                  <span class="badge badge-pill badge-primary">${href}</span>&nbsp;
                  <span class="badge badge-pill badge-outline">${urlSubstr}</span></a><br/>`;
    }
    if (USER_CATEGORY > 5)
        mailings = ` <span class="badge badge-pill badge-light">
                              <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                              <i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i>
                              ${getTextById(87)}
                          </span>`;
    const html = ` <div class="container p-5 bg-white">
                    
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="border-bottom pb-2 mb-3">
                                    <button class="btn btn-primary edituser-settings">
                                        <i class="fa fa-edit text-right"></i> <strong>${getTextById(76)}</strong>
                                    </button>
									
                                    <a id="save_edit" style="visibility:hidden;" class="btn btn-primary click_updateusersettings text-light">
                                        <i class="fa fa-save"></i>
                                    </a>
									
									<a id="close-user-settings" style="visibility:hidden" class="btn btn-primary close-user-settings text-light">
                                        <i class="fas fa-times"></i>
									</a>
                                </div>
                                        
                                <div class="form-group">
                                    <label><i class="fa fa-user"></i> Username:</label>
                                    <input id="edit-username" class="form-control" type="text" disabled value="${username}">
                                </div>

                                <div class="form-group">
                                    <label><i class="fa fa-envelope"></i> Email:</label>
                                    <input id="edit-email" class="form-control" type="text" disabled value="${email}">
                                </div>

                                <div class="form-group">
                                    <label><i class="fa fa-user"></i> First Name:</label>
                                    <input id="edit-firstname" class="form-control" type="text" disabled ${firstNameValue} placeHolder="${firstNamePlaceholder}">
                                </div>

                                <div class="form-group">
                                    <label><i class="fa fa-user"></i> Last Name:</label>
                                    <input id="edit-lastname" class="form-control" type="text" disabled ${lastNameValue} placeHolder="${lastNamePlaceholder}">
                                </div>

                                <div class="bg-white" style="border-left: 5px solid #007BFF; padding-left: 15px;">
                                    <ul class="list-inline mt-2">
                                        <li class="list-inline-item text-dark">
                                            <i class="fa fa-id-badge text-muted"></i> <span>${getTextById(79)}:</span> <span class="text-primary">${userid}</span>
                                        </li>
                                        <li class="list-inline-item text-dark">
                                            <i class="fa fa-certificate text-muted"></i> <span>${getTextById(80)}:</span> <span class="text-primary">${USER_PRODUCT}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
							<!-- Added VP -->
                            <div class="col-lg-12">
                                <div class="border p-2 mb-2">
                                    <form>
                                        <div class="form-group">
                                            <label><i class="fa fa-upload"></i> Upload</label>
                                            <input  type="file" class="form-control-file showCustomerEmails" id="emailCSV">
                                        </div>
                                    </form>
                                </div>
                            </div>
							<!-- -->
							
                            <div class="col-lg-12">
                                <div class="p-3 mb-2 border">
                                    <h6 id="lastCreated" class="text-dark mb-2"><i class="fas fa-tools"></i> <strong>${getTextById(73)}:</strong></h6>
                                    <p class="">${lastUrls}</p>
                                </div>
                            </div>

                            <div class="col-lg-12 mt-2">
                                <ul class="list-inline mt-2">
                                    <li class="list-inline-item">
                                        <button class="btn btn-primary show_submit"><i class="fa fa-link"></i> </button>
                                    </li>
                                    <li class="list-inline-item">
                                        <button class="btn btn-dark click_usershortcuts"><i class="fa fa-list-ul"></i> </button>
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-12">
                                <div class="alert alert-primary p-3">
                                    <p>${getTextById(72)}: ${last_login}</p>
                                    <p>Dovis-Version: ${DOVIS_VERSION}</p>
                                </div>
                            </div>

                        </div>  

                    </div>`;
    return html;
}

const styleBuildMailAccountsListLayout = function () {
    return ` <div class="container">
                <div class="row">
                <div class="col-lg-12">
                    <div class="bg-white p-5">
                        <a class="click_usersettings" id="close_emailupload">
                        <i class="fa fa-remove  fa-2x"></i></a><br>
                        <div class="form-check rounded dovis_small" style="padding: 1.5em;width:100%;">
                        <div id="uploadCSVPreview" class="container" style="">
                            <p style="font-size: 1.15rem;">${TEXT_EXPLAIN_UPLOAD_FILE} ${Math.floor(MAXIMUM_ALLOWED_FILESIZE_CSV / 1000)} Kb</p>
                        </div> 
                        <i id="uploadClock" class="fa fa-file fa-2x"></i> 
                            <br> 
                            <i class="fa fa-upload  fa-2x"> <input type="file" id="emailCSV" /></i>   <p id="csvFileInfo"></p> 
                            <br> 
                            <b id="columnSelection" style="font-size:1.75rem;color: #0d6efd; visibility: hidden">${TEXT_SELECT_EMAIL_FIRSTNAME_LASTNAME}</b>
                            <br><span id="showSelectedColumns" style="visibility: hidden;">
                        <i class="fa fa-at"></i><i class="fa fa-envelope-o"></i>&nbsp; &nbsp; &nbsp;<b id="email-column"></b><br>
                            ${getTextById(74)}:&nbsp; &nbsp; &nbsp;<b id="name-column"></b><br>
                            ${getTextById(75)}:&nbsp; &nbsp; &nbsp;<b id="lastname-column"></b></span><br>
                            <select id="emailColumnSelect" style="font-size:1.75rem;visibility: hidden;background: #0d6efd;color: #ffffff">
                            </select>
                            <select id="firstNameColumnSelect" style="font-size:1.75rem;visibility: hidden;background: #0d6efd;color: #ffffff">
                            </select>
                            <select id="lastNameColumnSelect" style="font-size:1.75rem;visibility: hidden;background: #0d6efd;color: #ffffff">
                            </select>
                            <p id="showHeader">  </p>  <b id="showHeaderIconInfo"></b>
                            <hr>
                            <i id="uploadFileIcon" class="fa fa-cloud-upload fa-2x start-upload" data-iduser=${currentDovisUserID} style="color: #0d6efd; visibility: hidden">${getTextById(92)}</i>
                            <div id="csvImport"></div>
                        </div>
                    </div>
                </div>
            </div>
    </div> `
        ;
}

const styleUrlListLayout = function (TDopen, TDclose, rowData) {
    let tl = ``, kw = ``, desc = ``;
    let icon, linkInfo;
    let shareButtonVisibility = "visible";
    const idshortcut = rowData.idshortcut;
    const cat = rowData.cat;
    const iconUrl = rowData.iconUrl;
    const description = rowData.description;
    const taglist = rowData.taglist;
    const keywords = rowData.keywords;
    const userDesc = rowData.userDesc;
    const shortcut = rowData.shortcut;
    const decodedUrl = rowData.decodedUrl;
    const timestamp = rowData.timestamp;
    const userHtml = rowData.userHtml;

    let shareDescription = `${description} &nbsp; ${userDesc}`;

    if (iconUrl && isImage(iconUrl)) {
        icon = `<img src="${iconUrl}" class="dovisthumb">`
    } else icon = ``;

    if (notEmpty(description)) desc = `<span class="dovis_small float-start" id="desc${idshortcut}" >${description}&nbsp;</span><br>&nbsp;&nbsp;`;
    if (notEmpty(taglist))
        tl = `<span class="dovis_small float-start" id="taglist${idshortcut}"> <i class="fa fa-tags"></i>  [...${taglist}...]  &nbsp;</span><br>&nbsp;&nbsp;`;
    if (notEmpty(keywords))
        kw = ` <span class="dovis_small float-start" id ="keywordlist${idshortcut}">  <i class="fa fa-sticky-note"></i> [...${keywords}...] &nbsp; </span><br>&nbsp;&nbsp;`;
    const metadiv = `
                       ${desc} 
                       ${tl} 
                       ${kw}`;
    if (cat === 1) {
        linkInfo = `<i class="fa fa-unlink fa-lg" id="icon${idshortcut}"></i>`;
        shareButtonVisibility = "hidden";
    } else if (cat === 2) {
        linkInfo = `<i class="fa fa-link fa-lg" id="icon${idshortcut}"></i>`;
    } else if (cat > 2) {
        linkInfo = `<i class="fa fa-forward fa-lg" id="icon${idshortcut}"></i>`;
    }
    return `${TDopen}
                        <div class="container">
                            <div class="bg-white p-5" id="shortcutrow${idshortcut}">
                                <div style="visibility:hidden;">
                                ${timestamp}
                                </div>
                                <p class="mb-2"><i class="fas fa-calendar-alt"></i> <strong>Created Date:</strong> ${timestamp}</p>
                                <div class="alert alert-success" role="alert">
                                    <a title="https://dovis.it/${shortcut}"  class="dovislink float-start" href="${decodedUrl}" target="_blank">${icon}&nbsp;${decodedUrl}</a> 
                                </div>  
                                ${metadiv} 
                                <div class="border-top mt-2">
                                    <div class="mt-3 mb-3">
                                        <span class="dbadge open-preview"  data-sc="${shortcut}">
                                            <button class="btn btn-primary"><i class="fas fa-link"></i></button>
                                        </span>

                                        <span id="sc_properties_${idshortcut}" class="dbadge90 show_shortcut_properties" data-url="${decodedUrl}"
                                        data-idshortcut="${idshortcut}"
                                        data-linkcategory="${cat}"
                                        data-keywords="${keywords}"
                                        data-description="${description}&nbsp;${userDesc}"
                                        data-tags="${taglist}">
                                            <button class="btn btn-primary"><i class="fas fa-tools"></i></button>
                                        </span>

                                        <span id="share_${idshortcut}" class="dbadge show_share"
                                            style="visibility:${shareButtonVisibility}" data-userHtml='${htmlEncode(userHtml)}'
                                            data-shortcut="${shortcut}" data-urldescriptiontext="${shareDescription}" data-idshortcut="${idshortcut}"> 
                                            <button class="btn btn-primary"><i class="fas fa-share-alt"></i></button>
                                        </span>

                                        <span class="delete-shortcut  right" id="delete_${idshortcut}" data-idsc="${idshortcut}"> 
                                            <button class="btn btn-primary"><i class="fa fa-trash"></i></button>
                                        </span> 
                                    </div>
                                </div> 

                                <div class="ad">
                                <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8975961160217250"
                                        crossorigin="anonymous">
                                </script>
                                    <ins class="adsbygoogle"
                                        style="display:block"
                                        data-ad-format="fluid"
                                        data-ad-layout-key="-f8+6l-1t-cu+tp"
                                        data-ad-client="ca-pub-8975961160217250"
                                        data-ad-slot="7442951102">
                                    </ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>
                                </div>
                                    
                            </div>
                        </div>
                       
                       
                       ${TDclose}`;
}

const styleEmailListTrashIconLayout = function (idEmail) {
    return `<i class="fa fa-trash optout" data-emailid="${idEmail}"></i> &nbsp;`;
}

const styleEmailListRowLayout = function (TDopen, TDclose, cellData) {
    return `${TDopen}<b class="dovis_small">${cellData}</b>${TDclose}`;
}

const styleCreateShortCutDialog = function (shortcut, urlString, linkData, actionName) {

    const tagList = `<div class="border p-5">
                
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Tags" id="shortCut_taglist">
                        <div class="input-group-append">
                        <button id="shortCut_taglist_icon" class="btn btn-primary"><i class="fa fa-tags fa-lg"></i></button>
                        </div>
                    </div>`;

    return `${tagList}

            <div id="scurl_linkage">
                <p class="alert alert-success">dovis.it/${shortcut} <i class="fas fa-long-arrow-alt-right"></i> ${urlString}</p>
                <p class="alert alert-info">${TEXT_NUMBER_OF_CHARACTERS} : ${urlString.length}</p>


                <ul class="nav justify-content-center mb-3">
                    <li class="nav-item ml-2 mr-2">
                        <a class="btn btn-primary text-light ${actionName}" id="save_shortcut" ${linkData} data-idcategory="1"> 
                            ${TEXT_SAVE_THIS_SHORTURL} <i class="fa fa-save"></i><i class="fa fa-unlink"></i>
                        </a>  
                    </li>
                    <li class="nav-item ml-2 mr-2">
                        <a class="btn btn-primary text-light ${actionName}" id="publish_shortcut" ${linkData} data-idcategory="2">
                        ${TEXT_PUBLISH_THIS_SHORTURL} <i class="fa fa-globe"></i><i class="fa fa-link"></i>
                        </a>
                    </li>
                </ul>

                <p class="border-top pt-2" id="newshortcut_h3">dovis.it/${shortcut}</p>

            </div>
                   
        `;
}

const styleCreateSubmit1 = function (s9) {
    return `<p>${TEXT_URL_ALREADY_SAVED}
            &nbsp;<i class="fa fa-exclamation"></i></p>
                   <h4><a href="${APPLICATION_HOMEPAGE}/${s9}">
                      ${APPLICATION_HOMEPAGE}/${s9}</a></h4>`;
}

const styleCreateSubmit2 = function (urlData) {
    return `<div id="saveScDialog" class="text-md-center" id="tableForSubmittingNewShortcut">
                     <br/> ${urlData}
                </div>`;
}

const styleEventError = function () {
    return `<i class="fa fa-exclamation-triangle"></i>`;
}

const styleUrlListDataSizeError = function () {
    return `<h4 id="datasize-warning">
              <i  class="fa fa-exclamation fa-3x"></i><br>${TEXT_MAXIMUM_DATASIZE_EXCEEDED}</h4><br>`;
}

const styleInvalidUrlMessage = function (stringOfUrl) {
    if (!stringOfUrl.startsWith("http")) {
        return `<p class="badge-danger">http://* | https://* </p>
                               <p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    } else {
        return `<p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    }
}

const styleShortCutProperties = function (radioSelectionClass, idshortcut, check1, check2) {
    return `<div id="urlstring" class="alert-info mt-3 p-3">
                       <div class="text-right"><i class="fa fa-window-close fa-2x hide_shortcut_properties"
                           id="close_properties_icon"></i></div>
                            <h5>${TEXT_SET_REDIRECTION_MODUS}</h5>

                            <a id="setRedirectModeButton" class="btn btn-primary ${radioSelectionClass}" style="visibility: hidden;" data-idshortcut="${idshortcut}"  data-enable="">
                                  <i class="fa fa-check text-light" ></i>
                            </a>

                          <div class="form-check rounded" style="padding: -0.42em;" id="categoryRadio">
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat1" ${check1}>
                                <i class = "fa fa-unlink"></i>
                                &nbsp;  ${getTextById(69)}</input><br>
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat2" ${check2}>
                               <i class = "fa fa-link"></i> &nbsp; ${getTextById(70)}</input><br>
                          </div>

                          <hr>
                          <div class="rounded">
                            <i class="fas fa-tags"></i>
                            <input type="text" id="inputUsertags" placeHolder="${TEXT_FILL_SOME_TAGS}" class="rounded">&nbsp;&nbsp;
                            <a class="dovislink" >
                                <button type="button" class="btn btn-primary btn-sm click_tagshortcut" data-idshortcut="${idshortcut}" id="addTagsToShortcut">Save</button>
                            </a>
                            <i id="infoTaglistAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          </div>
                          <hr>
                          <div class="doviseditor"> 
                             <div id="summernote"></div>
                          </div>
                           <a class="dovislink">
                                <button class="btn btn-primary mt-3 click_shortcutdesc" id="click_shortcutdesc" data-idshortcut="${idshortcut}" ><i class="fa fa-save "  ></i> Save</button>
                            </a>
                          <i id="infoUserDescAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          <hr>

                        </div>`;
}

const styleShareOptions = function (idshortcut, shortcut, descriptionText, urlString, twitterTags) {
    return `<div id="sharediv" class="border mt-3 ">

                       <div class="text-right hide_share" data-idshortcut="${idshortcut}"><i class="fa fa-window-close fa-2x"></i></div>


                       <ul class="list-inline border-top p-3 justyify-content-center">
                            <li class="list-inline-item">
                                <a href="https://www.facebook.com/sharer/sharer.php?u=${APPLICATION_HOMEPAGE}/${shortcut}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fab fa-facebook-f"></i> Facebook</button>
                                </a> 
                            </li>
                            <li class="list-inline-item">
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url=${APPLICATION_HOMEPAGE}/${shortcut}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fab fa-linkedin-in"></i> Linkdin</button>
                                </a> 
                            </li>
                            <li class="list-inline-item">
                                <a href="https://telegram.me/share/url?url=${APPLICATION_HOMEPAGE}/${shortcut}&text=${descriptionText}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fab fa-telegram"></i>Telegram</button>
                                </a> 
                            </li>
                            <li class="list-inline-item">
                                <a href="whatsapp://send?text=${APPLICATION_HOMEPAGE}/${shortcut}  ${encodeURIComponent(descriptionText)}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fab fa-whatsapp"></i> Whatsapp</button>
                                </a> 
                            </li>
                            <li class="list-inline-item">
                                <a href="https://twitter.com/intent/tweet?text=${encodeURIComponent(descriptionText)}&url=${urlString}${twitterTags}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fab fa-twitter"></i> Twitter</button>
                                </a> 
                            </li>
                            <li class="list-inline-item">
                                <a href="mailto:?subject=${descriptionText}&body=${APPLICATION_HOMEPAGE}/${shortcut}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="far fa-envelope"></i> Email</button>
                                </a> 
                            </li>

                            <li class="list-inline-item">
                                <a id="mailing_${shortcut}" class="dbadge85 mailing_action"   
                                data-shortcut="${shortcut}" data-idshortcut="${idshortcut}">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fas fa-user"></i> Link</button>
                                </a> 
                            </li>

                        </ul>

               </div>`;
}

const styleShowSubmit = function () {
    const createNewScDiv = document.getElementById("create-newshortcut-div");
    createNewScDiv.classList.add("container");
    createNewScDiv.classList.add("border-primary");
    createNewScDiv.setAttribute("style", "visibility:visible;");
    return ` <div class="container">

            <div class="bg-white p-5">
                <input class="form-control" id="longurl_input" placeholder="" type="text">
                <br/>
                <div class="dovis badge badge-info flex-md-wrap text-wrap border border-primary rounded"
                    id="longUrlText">
                </div>
            <button class="btn btn-block btn-primary click_selecturlid"
                    id="createsubmit"
                    type="submit">
            </button>
            </div>

    </div>`;
}

const styleRedirectIcon = function (icon) {
    icon.removeAttr("class");
    icon.addClass("fa fa-check fa-lg");
}

const styleEmailCampaignSend = function (campaignElement) {
    campaignElement.classList.add("dbadge85");
}

const styleEmailCampaignOpen = function (layout, idShortcut, sc) {
    return `<h3> dovis.it/${sc} </h3><br>
            <div id="mail_campaign_recipients" class="container"  style="font-size:0.8rem;max-height:200px;overflow-y:scroll;padding:25px;"></div><br>
              <i  class="fa fa-envelopes-bulk"></i><br>
             <input type="text" id="mail_campaign_subject"  placeHolder="${TEXT_TYPE_EMAIL_SUBJECT}" size="45" disabled oninput="enableMailSending()"></input><br>      
              <div id="summernote"></div>   
            <span id="sendEmailCampaign" class="subjectHint"  data-idshortcut="${idShortcut}" data-shortcut="${sc}">
                <i class="fa fa-envelope-o fa-2x" ></i>
            </span>`;
}

const styleHandleUploadFile = function (header, rows) {
    let headerSample = `<table><tr>`;
    const td = `<td style="border-style: solid;border-width: 1px;text-align:center;">`;
    for (let i = 0; i < header.length; i++) headerSample += `${td}${i + 1}</td>&nbsp;`
    headerSample += "</tr><tr>"
    for (let i = 0; i < header.length; i++) headerSample += `${td}${rows[0].s[i]}</td>&nbsp;`
    headerSample += "</tr><tr>";
    for (let i = 0; i < header.length; i++) headerSample += `${td}${rows[1].s[i]}</td>&nbsp;`
    headerSample += "</tr><tr>";
    for (let i = 0; i < header.length; i++) headerSample += `${td}...</td>&nbsp;`
    headerSample += "</tr><table>";
    document.getElementById("showHeader").innerHTML = headerSample;
    document.getElementById("showHeaderIconInfo").innerHTML = `<i class="fa fa-info fa-2x"></i>`;

}

const styleRowsInLoadFile = function (responseText, div) {
    const result = responseText.replace(/[\n]/g, '<br>');
    document.getElementById(div).innerHTML = `<p>${result}</p>`;
}

const styleContact = function () {
    return `<div class="border border-light rounded" style="padding:15px;">
               <p>Enpasoft GmbH<br>
               Küferstrasse 12<br>
               69168 Wiesloch, Germany<br>
               <i class="fa fa-phone"></i>+49-6222-3171090</p> 
               <ul>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:support@dovis.it?subject=Referred to dovis.it:">${TEXT_GENERAL_ENQUIRIES}</a>&nbsp;<i class="fa fa-info fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=BUG-description:">${TEXT_SUBMIT_BUG}</a>&nbsp;<i class="fa fa-bug fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=FEATURE-Request:">${TEXT_SUBMIT_FEATURE}</a>&nbsp;<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i></li>
               </ul>`;
}

const styleAbout = function () {
    return `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
            <b>${TEXT_FREE_OF_CHARGE}</b><br><br><br>
            ${TEXT_PERSONAL_USE}<br>
                <ul>
                    <li> ${getTextById(105)}</li>
                    <li> ${getTextById(106)}</li>
                    <li > ${getTextById(107)}</li>
                    <li > ${getTextById(108)}</li>
                    <li > ${getTextById(109)}</li>
                </ul>
              ${TEXT_PROFESSIONAL_USE} - ${TEXT_MAILINGS}<br>
                 <ul>
                    <li > ${getTextById(112)}</li>
                    <li > ${getTextById(113)}</li>
                    <li > ${getTextById(114)}</li> 
               </ul>
               ${TEXT_CORS} <a href="#footnote-1">&nbsp;*&nbsp;</a> <br>
                 <ul>
                    <li > ${getTextById(116)}</li>
                    <li > ${getTextById(117)}</li>
                    <li > ${getTextById(118)}</li> 
               </ul>
               <p id="footnote-1">
            *<i class="fa fa-link" aria-hidden="true"> </i>&nbsp;<a href="https://www.dovis.it/CORSmdn" target="_parent">dovis.it/CORSmdn</a> &nbsp;
            <a href="https://www.dovis.it/CORSwiki" target="_parent">dovis.it/CORSwiki</a>
               <br><br>
            </div>`;
}

const styleFooter = function () {
    return `<div class="web-footer-sec">
        <!-- SVG -->
        <div class="custom-shape-divider-top-1677351719">
            <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
                <path d="M1200 120L0 16.48 0 0 1200 0 1200 120z" class="shape-fill"></path>
            </svg>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright-content">
                        <p>© dovis.it 2022. Version:${backendVersion} (Backend), ${frontendVersion} (Frontend) All Rights Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="footer-link">
                        <a class="text-light" href="#" onclick="loadFile('./tos_${getCurrentLanguage()}.txt','main-content')">${getTextById(59)}</a> |
                        <a class="text-light ml-3" href="#" onclick="loadFile('./privacy_${getCurrentLanguage()}.txt','main-content')">${getTextById(60)}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>`;
}


const styleHowto = function () {
    return `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
                <ul>
                    <li> <a href="https://www.dovis.it/WTTA5A" target="_blank">${getTextById(141)}</a></li>
                    <li> <a href="https://www.dovis.it/G3C79A" target="_blank">${getTextById(142)}</a></li>
                    <li> <a href="https://www.dovis.it/FSXXMA" target="_blank">${getTextById(143)}</a></li>
                    <li> <a href="https://www.dovis.it/XAOMLY" target="_blank">${getTextById(144)}</a></li> 
                </ul>
            </div>`;
}