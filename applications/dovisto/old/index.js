"use strict";

const TEXT_YOUR_LONG_URL = getTextById(1);
const TEXT_CREATE_PERSONALIZED_URL = getTextById(4);
const TEXT_SLOGAN = getTextById(5);
const TEXT_CREATE = getTextById(6);
const TEXT_MAIL_REGISTRATION_SUBJECT = "dovis.it: your registration-request";
const TEXT_SERVER_ERROR = "Sorry! An error on the server occured.";
const TEXT_SERVER_UNREACHABLE = "Server could not be reached";
const TEXT_SENDING_EMAIL = "Sending ...";
const TEXT_REGISTER_NOW = "Click to confirm registration";
const TEXT_ENTER_VALIDATIONCODE = "Write here your validation-code received by Email";
const TEXT_WHAT_IS_THE_SUM_OF = "What is the sum of ";
const TEXT_AND = "and";

const CALLBACKPAGE_USER_REGISTRATION = "https://home.dovis.it/dovisit?";
const MAIL_BODY_START = `Clicke here to complete your registration `;
const MAIL_BODY_END = ` <BR/>Thank you!`;
const JSON_HTML_URL = `https://home.dovis.it/dovisit/email.json`;

setElemText(2);
setElemText(3);
let hasShortUrl = false;
let shortUrl;
const webAdress = window.location.href;
const instruction = webAdress.lastIndexOf("?");

function buildRegistrationConfirmationForm(data) {
    $("#registrationEmail").attr("disabled", "true");
    const challenge = `${TEXT_WHAT_IS_THE_SUM_OF}${data.challengevalue1}&nbsp;${TEXT_AND}&nbsp;${data.challengevalue2} &nbsp;?`;
    $("#registrationEmail").after(`<p>${challenge}</p><br>`)
    $("#registrationEmail").after(`<input id="challengeresponse" class="form-control form-control-lg" placeholder="${challenge}"
                                   type="number" required="true">`);
    $("#registrationEmail").after(`<input id="validationcode" class="form-control form-control-lg" placeholder="${TEXT_ENTER_VALIDATIONCODE}"
                                   type="text" required="true">`);
    $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-user-plus"></i></span>&nbsp;${TEXT_REGISTER_NOW}`);
    $("#registrationButton").attr("class", `btn btn-block btn-lg btn-primary confirmRegistration`);
    $("#registrationMessage").hide();
}

if (instruction.toString().startsWith("shorturl")) {
    const shortUrlIndex = 1 + instruction.lastIndexOf("=");
    if (shortUrlIndex > 1) {
        shortUrl = instruction.substring(shortUrlIndex);
        hasShortUrl = true;
    }
} else if (instruction.toString().startsWith("validationcode")) {

}

$("#longurl_input").prop("placeholder", TEXT_YOUR_LONG_URL);
$("#slogan").text(TEXT_SLOGAN);
$("#createsubmit").html(`<i class="fas fa-random"></i>&nbsp;${TEXT_CREATE}`);
if (hasShortUrl) {
    $("#create_personalized_shorturl").html(`<i class="fas fa-link"></i>&nbsp;${TEXT_CREATE_PERSONALIZED_URL} dovis.it/${shortUrl}`);
    showUrls();
} else {
    $("#create_personalized_shorturl").html(`<i class="fas fa-link"></i>&nbsp;${TEXT_CREATE_PERSONALIZED_URL} dovis.it/&nbsp;<i class="fas fa-tags"></i>`);
}
let responseObject = {errorMnemonic: ""};

$(document).on('click', '.doRegistration', function () {

        $("#registrationMessage").attr("style", "visibility: hidden");
        const userEmail = $("#registrationEmail").val();
        if (validateEmail(userEmail)) {

            console.log(`Start registration of user with Email ${userEmail}`);
            $("#registrationIcon").attr("style", "visibility: visible;");
            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-envelope"></i></span>&nbsp;${TEXT_SENDING_EMAIL}`);
            blink("registrationButton", 250, 5);
            blink("registrationEmail", 250, 5);
            const xhttp = new XMLHttpRequest();
            const postUrl = getAPIServerPath() + "0/0/customer/asktoregister/800/8";
            xhttp.open("POST", postUrl, true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            let xhttpState = -1;

            xhttp.onreadystatechange = function () {
                xhttpState = xhttp.readyState;
                if (xhttpState === 4) {
                    try {
                        if (xhttp.status > 0) {
                            responseObject = JSON.parse(xhttp.responseText);
                            if (xhttp.status <= 204) {
                                $("#registrationButton").html(`<span  id="registrationIcon"><i class="fas fa-check-circle"></i></span>Email sent!`);
                                buildRegistrationConfirmationForm(responseObject);
                            } else if (xhttp.status === 422) {
                                if (responseObject.errorMnemonic.toString().startsWith("asktoregister.3")) {
                                    $("#registrationButton").text(`This eMail is already registered: ${userEmail} `);
                                } else if (responseObject.errorMnemonic.toString().startsWith("asktoregister.7")) {
                                    $("#registrationButton").text(`Failed to send eMail to ${userEmail}`)
                                }
                            } else if (xhttp.status >= 400) {
                                $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                            }
                        } else {
                            $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                            $("#registrationMessage").text(`${TEXT_SERVER_UNREACHABLE}`);
                            $("#registrationMessage").attr("style", "visible");
                        }
                    } catch
                        (error) {
                        console.error(error);
                        $("#registrationButton").html(`<span  id="registrationIcon"><i class="fa fa-exclamation-circle"></i></span>&nbsp;${TEXT_SERVER_ERROR}`);
                    }
                } else {
                    $("#registrationMessage").attr("style", "visible");
                    $("#registrationMessage").text("ERROR");
                }
            }
            xhttp.send(`email=${userEmail}&mailbodymessagestart=${MAIL_BODY_START}&mailbodymessageend=${MAIL_BODY_END}&mailsubjectmessage=${TEXT_MAIL_REGISTRATION_SUBJECT}&callbackpage=${CALLBACKPAGE_USER_REGISTRATION}&jsonurl=${JSON_HTML_URL}`);
        }
    }
)
;

$(document).on('click', '.confirmRegistration', function () {
    const userEmail = $("#registrationEmail").val();
    const code = $("#validationcode").val();
    const cr = $("#challengeresponse").val();
    $.validator
    let validator =    $.validator;//$("#registrationform").data("bootstrapValidator");
    if (validator.isValid("#registrationform")) {
        console.log(`Confirm registration of user ${userEmail} with , code ${code} and challenge ${cr}`);
    } else {
        console.error(`Invalid form: ${validation.val()} Cannot start registration-confirmation for user ${userEmail}, code ${code} and challenge ${cr}`);
    }
    bootstrapValidate('#myInput', 'rule', function (isValid) {
        if (isValid) {
            alert('Element is valid');
        } else {
            alert('Element is invalid');
        }
    });
});