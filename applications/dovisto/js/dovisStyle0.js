"use strict";
const errordiv = "dovis-errors";
const TableStyle = {
    loadingIcon: `<i class="fa fa-cog fa-spin fa-2x fa-fw"></i>`,
    pre: ``,
    classes: `table table-hover table-striped table-sm text-sm-left dataTable`,
    colOpen: `<col>`,
    colClose: `</col>`,
    tableHeadOpen: `<thead><tr>`,
    tableHeadClose: `</tr></thead><tbody>`,
    thOpen: `<th class="text-sm-left">`,
    thClose: `</th>`,
    trOpen: `<tr class="text-left">`,
    trClose: `</tr>`,
    tdOpen: `<td class="text-left">`,
    tdClose: `</td>`,
    aOpen: `<a`,
    aAttributes: `>`,
    aClose: `</a>`,
    bottom: `</table>`,
    elements: [],
};

const FormStyle = {
    pre1: '<div class="row"><div class="col-md-',
    pre2: '"><div class="box box-primary"><form role="form" id=',
    pre3: ' enctype="multipart/form-data"><div class="box-body">',
    suf: '</div></form></div></div></div>',
}

const styleLoginLayout = function (userName) {
    $("#login").after(`&nbsp;<a class="btn btn-primary custom-btn click_usersettings" id ="settings" style="z-index: 2;"><i class="fa fa-user-circle">&nbsp;${userName}</i></a>&nbsp;`);
    $("#settings").before(`<a class="btn btn-primary custom-btn doLogout" id ="logoutButton"><i class="fa fa-power-off"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary custom-btn click_usershortcuts" id ="showUserUrlsButton"><i class="fa fa-list-ul"></i></a>&nbsp;`);
    $("#settings").after(`&nbsp;<a class="btn btn-primary custom-btn show_submit" id ="showSubmitButton"><i class="fa fa-plus-square icon-link"></i></a>&nbsp;`);
}

const styleSettingsPageLayout = function (storedData, last_login, username, email, firstNameValue, firstNamePlaceholder,
                                          lastNameValue, lastNamePlaceholder, userid, lastUrlsArray) {
    let lastUrls = ``;
    let mailings = ``;
    for (let r = 0; r < lastUrlsArray.length; r++) {
        const href = lastUrlsArray[r].href;
        const urlSubstr = lastUrlsArray[r].urlSubstr;
        lastUrls += `<a href="${href}">
                  <span class="badge badge-pill badge-primary">${href}</span>&nbsp;
                  <span class="badge badge-pill badge-outline">${urlSubstr}</span></a><br/>`;
    }
    if (USER_CATEGORY > 5)
        mailings = ` <span class="badge badge-pill badge-light">
                              <i class="fa fa-users fa-2x" aria-hidden="true"></i>
                              <i class="fa fa-envelope-square fa-2x" aria-hidden="true"></i>
                              ${getTextById(87)}
                          </span>`;
    const html = ` <div class="container-lg" style="padding: 25px;">
                                <i class="fa fa-remove fa-2x close-user-settings" style="visibility:hidden" id="close-user-settings"></i>&nbsp;&nbsp;&nbsp;
                                <a id="save_edit" style="visibility:hidden;" class="btn btn-primary click_updateusersettings">
                                     <i class="fa fa-save"></i>
                                </a>
                                <i class="fa fa-edit edituser-settings text-right fa-2x"> ${getTextById(76)}</i>
                                    <div class="form-row rounded">
                                        <i class="fa fa-user"></i>&nbsp;
                                        <input id="edit-username" type="text" disabled value="${username}" class="rounded">
                                    </div>
                                    <div class="form-row rounded">
                                        <i class="fa fa-envelope"></i>
                                        <input id="edit-email" type="text" disabled value="${email}" class="rounded">
                                    </div>
                                    <div class="form-row rounded">
                                        <i class="fa fa-id-card-o"></i>
                                        <input id="edit-firstname" type="text" disabled ${firstNameValue} placeHolder="${firstNamePlaceholder}" class="rounded">
                                    </div>
                                    <div class="form-row rounded">
                                        <i class="fa fa-id-card-o"></i>
                                        <input id="edit-lastname" type="text" disabled ${lastNameValue} placeHolder="${lastNamePlaceholder}" class="rounded">
                                    </div>
                                <div class="row">  
                                         <span class ="badge badge-pill badge-dark" style="color: #b1b4be;"><i class="fa fa-id-badge fa-2x"></i> &nbsp;${getTextById(79)}: &nbsp;${userid} &nbsp;
                                         <i class="fa fa-certificate fa-2x"></i>&nbsp;${getTextById(80)}:${USER_PRODUCT}  &nbsp;</span>
                                         <div id="showCustomerEmailsList" class="showCustomerEmails" style="padding: 0.5em;">
                                            ${mailings}
                                         </div>
                                </div>
                      <div class="row">
                         <div class="dovis_small  border-light" id="lastCreated">
                            ${getTextById(73)}:
                            <br>
                           ${lastUrls}  
                         </div>
                         <div class="border-light" >
                              <span class="badge badge-pill badge-outline"><i class="fa fa-link show_submit fa-2x"></i></span>
                              <span class="badge badge-pill badge-outline"><i class="fa fa-list-ul click_usershortcuts fa-2x"></i></span>
                         </div>
                      </div>
                        <HR>
                        <div class="dovis_small">
                            ${getTextById(72)}:${last_login} <br/>
                            Dovis-Version: ${DOVIS_VERSION}
                        </div>
                    </div>`;
    return html;
}

const styleBuildMailAccountsListLayout = function () {
    return `<a class="click_usersettings" id="close_emailupload">
          <i class="fa fa-remove  fa-2x"></i></a><br>
           <div class="form-check rounded dovis_small" style="padding: 1.5em;width:100%;">
           <div id="uploadCSVPreview" class="container border border-light" style="width:80%;height:200px; overflow-x:auto;overflow-y:auto;border-radius:10px;padding:10px;">
             <p style="font-size: 1.15rem;">${TEXT_EXPLAIN_UPLOAD_FILE} ${Math.floor(MAXIMUM_ALLOWED_FILESIZE_CSV / 1000)} Kb</p>
           </div> 
           <i id="uploadClock" class="fa fa-file fa-2x"></i> 
            <br> 
            <i class="fa fa-upload  fa-2x"> <input type="file" id="emailCSV" /></i>   <p id="csvFileInfo"></p> 
            <br> 
            <b id="columnSelection" style="font-size:1.75rem;color: #0d6efd; visibility: hidden">${TEXT_SELECT_EMAIL_FIRSTNAME_LASTNAME}</b>
            <br><span id="showSelectedColumns" style="visibility: hidden;">
           <i class="fa fa-at"></i><i class="fa fa-envelope-o"></i>&nbsp; &nbsp; &nbsp;<b id="email-column"></b><br>
            ${getTextById(74)}:&nbsp; &nbsp; &nbsp;<b id="name-column"></b><br>
            ${getTextById(75)}:&nbsp; &nbsp; &nbsp;<b id="lastname-column"></b></span><br>
            <select id="emailColumnSelect" style="font-size:1.75rem;visibility: hidden;background: #0d6efd;color: #ffffff">
            </select>
            <select id="firstNameColumnSelect" style="font-size:1.75rem;visibility: hidden;background: #0d6efd;color: #ffffff">
            </select>
            <select id="lastNameColumnSelect" style="font-size:1.75rem;visibility: hidden;background: #0d6efd;color: #ffffff">
            </select>
            <p id="showHeader">  </p>  <b id="showHeaderIconInfo"></b>
            <hr>
            <i id="uploadFileIcon" class="fa fa-cloud-upload fa-2x start-upload" data-iduser=${currentDovisUserID} style="color: #0d6efd; visibility: hidden">${getTextById(92)}</i>
            <div id="csvImport"></div>
          </div> `
        ;
}

const styleUrlListLayout = function (TDopen, TDclose, rowData) {
    let tl = ``, kw = ``, desc = ``;
    let icon, linkInfo;
    let shareButtonVisibility = "visible";
    const idshortcut = rowData.idshortcut;
    const cat = rowData.cat;
    const iconUrl = rowData.iconUrl;
    const description = rowData.description;
    const taglist = rowData.taglist;
    const keywords = rowData.keywords;
    const userDesc = rowData.userDesc;
    const shortcut = rowData.shortcut;
    const decodedUrl = rowData.decodedUrl;
    const timestamp = rowData.timestamp;
    const userHtml = rowData.userHtml;

    let shareDescription = `${description} &nbsp; ${userDesc}`;

    if (iconUrl && isImage(iconUrl)) {
        icon = `<img src="${iconUrl}" class="dovisthumb">`
    } else icon = ``;

    if (notEmpty(description)) desc = `<span class="dovis_small float-start" id="desc${idshortcut}" >${description}&nbsp;</span><br>&nbsp;&nbsp;`;
    if (notEmpty(taglist))
        tl = `<span class="dovis_small float-start" id="taglist${idshortcut}"> <i class="fa fa-tags"></i>  [...${taglist}...]  &nbsp;</span><br>&nbsp;&nbsp;`;
    if (notEmpty(keywords))
        kw = ` <span class="dovis_small float-start" id ="keywordlist${idshortcut}">  <i class="fa fa-sticky-note"></i> [...${keywords}...] &nbsp; </span><br>&nbsp;&nbsp;`;
    const metadiv = `
                       ${desc} 
                       ${tl} 
                       ${kw}`;
    if (cat === 1) {
        linkInfo = `<i class="fa fa-unlink fa-lg" id="icon${idshortcut}"></i>`;
        shareButtonVisibility = "hidden";
    } else if (cat === 2) {
        linkInfo = `<i class="fa fa-link fa-lg" id="icon${idshortcut}"></i>`;
    } else if (cat > 2) {
        linkInfo = `<i class="fa fa-forward fa-lg" id="icon${idshortcut}"></i>`;
    }
    return `${TDopen}
                        <div id="shortcutrow${idshortcut}">
                           <div style="visibility:hidden;height:1px;">
                              ${timestamp}
                           </div>  
                            <a title="https://dovis.it/${shortcut}"  class="dovislink float-start" href="${decodedUrl}" target="_blank">${icon}&nbsp;${decodedUrl}</a>                              
                           <br>&nbsp;
                              ${metadiv} 
                           <br>
                           <div class="float-start">
                              <span class="dbadge open-preview"  data-sc="${shortcut}">
                                 <i class="fa fa-external-link fa-sm"></i>
                              </span>
                              <span id="sc_properties_${idshortcut}" class="dbadge90 show_shortcut_properties" data-url="${decodedUrl}"
                                 data-idshortcut="${idshortcut}"
                                 data-linkcategory="${cat}"
                                 data-keywords="${keywords}"
                                 data-description="${description}&nbsp;${userDesc}"
                                 data-tags="${taglist}">
                                 <i class="fa fa-cog fa-sm">&nbsp;${linkInfo}&nbsp;</i>&nbsp;${shortcut.toUpperCase()}&nbsp;&nbsp;&nbsp;
                             </span>
                             <span id="share_${idshortcut}" class="dbadge show_share"
                                style="visibility:${shareButtonVisibility}" data-userHtml='${htmlEncode(userHtml)}'
                                data-shortcut="${shortcut}" data-urldescriptiontext="${shareDescription}" data-idshortcut="${idshortcut}"> 
                                <i class="fa fa-share-alt fa-sm" aria-hidden="true"></i>
                             </span> 
                             <small><small><br>${timestamp}</small></small>
                           </div> 
                           <div class="ad">
                           <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8975961160217250"
                                     crossorigin="anonymous">
                           </script>
                                <ins class="adsbygoogle"
                                     style="display:block"
                                     data-ad-format="fluid"
                                     data-ad-layout-key="-f8+6l-1t-cu+tp"
                                     data-ad-client="ca-pub-8975961160217250"
                                     data-ad-slot="7442951102">
                                </ins>
                                <script>
                                     (adsbygoogle = window.adsbygoogle || []).push({});
                                </script>
                           </div>
                                <span class="delete-shortcut  right" id="delete_${idshortcut}" data-idsc="${idshortcut}"> 
                                   <i class="fa fa-trash fa-2x"></i>
                                </span> 
                       </div>${TDclose}`;
}

const styleEmailListTrashIconLayout = function (idEmail) {
    return `<i class="fa fa-trash optout" data-emailid="${idEmail}"></i> &nbsp;`;
}

const styleEmailListRowLayout = function (TDopen, TDclose, cellData) {
    return `${TDopen}<b class="dovis_small">${cellData}</b>${TDclose}`;
}

const styleCreateShortCutDialog = function (shortcut, urlString, linkData, actionName) {

    const tagList = `<div class="text-md-center"><input type="text" placeholder="Tags" id="shortCut_taglist"/>
                    &nbsp;<span class="badge-outline badge-dark rounded" style ="padding: 0.65em;" id="shortCut_taglist_icon"><i class="fa fa-tags fa-lg"></i></span>`;
    return `${tagList}<p class="dovis" id="scurl_linkage">dovis.it/${shortcut}&nbsp;&nbsp;<i class="fa fa-arrow-right"></i>
                     &nbsp;${urlString}<br>${urlString.length}&nbsp;${TEXT_NUMBER_OF_CHARACTERS}</p>&nbsp;&nbsp;
                <a class="btn btn-primary ${actionName}" id="save_shortcut" ${linkData}
                data-idcategory="1">${TEXT_SAVE_THIS_SHORTURL}
                    <i class="fa fa-save"></i>&nbsp;&nbsp;<i class="fa fa-unlink"></i>&nbsp;&nbsp;</a></p>
                   <p><a class="btn btn-primary ${actionName}" id="publish_shortcut" ${linkData}
                     data-idcategory="2">${TEXT_PUBLISH_THIS_SHORTURL}&nbsp;&nbsp;
                  <i class="fa fa-globe"></i>&nbsp;&nbsp;<i class="fa fa-link"></i>
                   </a></p></a><h3 id="newshortcut_h3">dovis.it/${shortcut}</h3></div>`;
}

const styleCreateSubmit1 = function (s9) {
    return `<p>${TEXT_URL_ALREADY_SAVED}
            &nbsp;<i class="fa fa-exclamation"></i></p>
                   <h4><a href="${APPLICATION_HOMEPAGE}/${s9}">
                      ${APPLICATION_HOMEPAGE}/${s9}</a></h4>`;
}

const styleCreateSubmit2 = function (urlData) {
    return `<div id="saveScDialog" class="text-md-center" id="tableForSubmittingNewShortcut">
                     <br/> ${urlData}
                </div>`;
}

const styleEventError = function () {
    return `<i class="fa fa-exclamation-triangle"></i>`;
}

const styleUrlListDataSizeError = function () {
    return `<h4 id="datasize-warning">
              <i  class="fa fa-exclamation fa-3x"></i><br>${TEXT_MAXIMUM_DATASIZE_EXCEEDED}</h4><br>`;
}

const styleInvalidUrlMessage = function (stringOfUrl) {
    if (!stringOfUrl.startsWith("http")) {
        return `<p class="badge-danger">http://* | https://* </p>
                               <p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    } else {
        return `<p class="badge-danger">[${stringOfUrl.replace(checkUrl, '***')}]</p>`;
    }
}

const styleShortCutProperties = function (radioSelectionClass, idshortcut, check1, check2) {
    return `<div id="urlstring" class="rounded" style="padding: 0.5em;
                       align-items;  text-sm-left; background-color: #b3d7ff; color: black">
                       <div class="text-right"><i class="fa fa-window-close fa-2x hide_shortcut_properties"
                           id="close_properties_icon"></i></div>
                          <i class="fa fa-external-link fa-lg">&nbsp;<small>${TEXT_SET_REDIRECTION_MODUS}</small></i>
                               <a id="setRedirectModeButton" class="btn btn-primary ${radioSelectionClass}"
                                  style="visibility: hidden;" data-idshortcut="${idshortcut}"  data-enable="">
                                  <i class="fa fa-check" ></i></a>
                          <div class="form-check rounded" style="padding: -0.42em;" id="categoryRadio">
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat1" ${check1}>
                                <i class = "fa fa-unlink"></i>
                                &nbsp;  ${getTextById(69)}</input><br>
                               <input class="form-check-input" type="radio" name="shortUrlCategoryRadio" id="cat2" ${check2}>
                               <i class = "fa fa-link"></i> &nbsp; ${getTextById(70)}</input><br>
                          </div>
                          <hr>
                          <div   class="rounded" style="padding: -0.42em;">
                          <i class="fa fa-tags fa-lg">&nbsp;</i>
                          <input type="text" id="inputUsertags" placeHolder="${TEXT_FILL_SOME_TAGS}" class="rounded">&nbsp;&nbsp;
                           <a class="dovislink">
                           <i class="fa fa-save fa-3x click_tagshortcut" data-idshortcut="${idshortcut}" id="addTagsToShortcut"></i> </a>
                          <i id="infoTaglistAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          </div>
                          <hr>
                          <div class="doviseditor"> 
                             <div id="summernote"></div>
                          </div>
                           <a class="dovislink"> <i class="fa fa-save fa-3x click_shortcutdesc" id="click_shortcutdesc" data-idshortcut="${idshortcut}"></i>
                            </a>
                          <i id="infoUserDescAdded" class="fa fa-check-circle" style="visibility:hidden;"></i>
                          <hr>
                          </div>`;
}

const styleShareOptions = function (idshortcut, shortcut, descriptionText, urlString, twitterTags) {
    return `<div id="sharediv" class="rounded text-md-center" style="padding: 0.2em;
                       align-items; background-color: #b3d7ff; color: black">
                       <div class="text-right hide_share" data-idshortcut="${idshortcut}"><i class="fa fa-window-close fa-2x"></i></div>
                       <br>
                       <span>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=${APPLICATION_HOMEPAGE}/${shortcut}" target="_blank">
                                <i class="fa fa-facebook-f fa-2x"></i></a> &nbsp;&nbsp;&nbsp;
                     <a href="https://www.linkedin.com/shareArticle?mini=true&url=${APPLICATION_HOMEPAGE}/${shortcut}" target="_blank">
                                <i class="fa fa-linkedin fa-2x"></i></a>&nbsp;&nbsp;
                     <a href="https://telegram.me/share/url?url=${APPLICATION_HOMEPAGE}/${shortcut}&text=${descriptionText}" target="_blank">
                                <i class="fa fa-telegram fa-2x" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                     <a href="whatsapp://send?text=${APPLICATION_HOMEPAGE}/${shortcut}  ${encodeURIComponent(descriptionText)}" data-action="share/whatsapp/share"  target="_blank">
                                <i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></a>    &nbsp;&nbsp;&nbsp;
                      <a href="https://twitter.com/intent/tweet?text=${encodeURIComponent(descriptionText)}&url=${urlString}${twitterTags}" target="_blank">
                        <i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>              &nbsp;&nbsp;&nbsp;
                       <a href="mailto:?subject=${descriptionText}&body=${APPLICATION_HOMEPAGE}/${shortcut}">
                                <i class="fa fa-envelope fa-2x"></i></a>
                     </span>&nbsp;&nbsp;&nbsp;
                       <span id="mailing_${shortcut}" class="dbadge85 mailing_action"   
                             data-shortcut="${shortcut}" data-idshortcut="${idshortcut}"> 
                              <i class="fa fa-users fa-2x"></i>
                             <i class="fa fa-envelope-square fa-2x"></i>
                       </span>
               </div>`;
}

const styleShowSubmit = function () {
    const createNewScDiv = document.getElementById("create-newshortcut-div");
    createNewScDiv.classList.add("border");
    createNewScDiv.classList.add("border-primary");
    createNewScDiv.setAttribute("style", "visibility:visible;padding:25px;border-radius:10px;");
    return ` <input class="form-control form-control-sm" id="longurl_input" placeholder=""
                       type="text">
                <br/>
                <div class="dovis badge badge-info flex-md-wrap text-wrap border border-primary rounded"
                     id="longUrlText">
                </div>
                <button class="btn btn-block btn-sm btn-primary click_selecturlid"
                        id="createsubmit"
                        type="submit">
                </button>`;
}

const styleRedirectIcon = function (icon) {
    icon.removeAttr("class");
    icon.addClass("fa fa-check fa-lg");
}

const styleEmailCampaignSend = function (campaignElement) {
    campaignElement.classList.add("dbadge85");
}

const styleEmailCampaignOpen = function (layout, idShortcut, sc) {
    return `<h3> dovis.it/${sc} </h3><br>
            <div id="mail_campaign_recipients" class="container"  style="font-size:0.8rem;max-height:200px;overflow-y:scroll;padding:25px;"></div><br>
              <i  class="fa fa-envelopes-bulk"></i><br>
             <input type="text" id="mail_campaign_subject"  placeHolder="${TEXT_TYPE_EMAIL_SUBJECT}" size="45" disabled oninput="enableMailSending()"></input><br>      
              <div id="summernote"></div>   
            <span id="sendEmailCampaign" class="subjectHint"  data-idshortcut="${idShortcut}" data-shortcut="${sc}">
                <i class="fa fa-envelope-o fa-2x" ></i>
            </span>`;
}

const styleHandleUploadFile = function (header, rows) {
    let headerSample = `<table><tr>`;
    const td = `<td style="border-style: solid;border-width: 1px;text-align:center;">`;
    for (let i = 0; i < header.length; i++) headerSample += `${td}${i + 1}</td>&nbsp;`
    headerSample += "</tr><tr>"
    for (let i = 0; i < header.length; i++) headerSample += `${td}${rows[0].s[i]}</td>&nbsp;`
    headerSample += "</tr><tr>";
    for (let i = 0; i < header.length; i++) headerSample += `${td}${rows[1].s[i]}</td>&nbsp;`
    headerSample += "</tr><tr>";
    for (let i = 0; i < header.length; i++) headerSample += `${td}...</td>&nbsp;`
    headerSample += "</tr><table>";
    document.getElementById("showHeader").innerHTML = headerSample;
    document.getElementById("showHeaderIconInfo").innerHTML = `<i class="fa fa-info fa-2x"></i>`;

}

const styleRowsInLoadFile = function (responseText, div) {
    const result = responseText.replace(/[\n]/g, '<br>');
    document.getElementById(div).innerHTML = `<p>${result}</p>`;
}

const styleContact = function () {
    return `<div class="border border-light rounded" style="padding:15px;">
               <p>Enpasoft GmbH<br>
               Küferstrasse 12<br>
               69168 Wiesloch, Germany<br>
               <i class="fa fa-phone"></i>+49-6222-3171090</p> 
               <ul>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:support@dovis.it?subject=Referred to dovis.it:">${TEXT_GENERAL_ENQUIRIES}</a>&nbsp;<i class="fa fa-info fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=BUG-description:">${TEXT_SUBMIT_BUG}</a>&nbsp;<i class="fa fa-bug fa-2x" aria-hidden="true"></i></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>
                              <a href="mailto:contact-project+cenzo-dovis-42313791-issue-@incoming.gitlab.com?subject=FEATURE-Request:">${TEXT_SUBMIT_FEATURE}</a>&nbsp;<i class="fa fa-lightbulb-o fa-2x" aria-hidden="true"></i></li>
               </ul>`;
}

const styleAbout = function () {
    return `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
            <b>${TEXT_FREE_OF_CHARGE}</b><br><br><br>
            ${TEXT_PERSONAL_USE}<br>
                <ul>
                    <li> ${getTextById(105)}</li>
                    <li> ${getTextById(106)}</li>
                    <li > ${getTextById(107)}</li>
                    <li > ${getTextById(108)}</li>
                    <li > ${getTextById(109)}</li>
                </ul>
              ${TEXT_PROFESSIONAL_USE} - ${TEXT_MAILINGS}<br>
                 <ul>
                    <li > ${getTextById(112)}</li>
                    <li > ${getTextById(113)}</li>
                    <li > ${getTextById(114)}</li> 
               </ul>
               ${TEXT_CORS} <a href="#footnote-1">&nbsp;*&nbsp;</a> <br>
                 <ul>
                    <li > ${getTextById(116)}</li>
                    <li > ${getTextById(117)}</li>
                    <li > ${getTextById(118)}</li> 
               </ul>
               <p id="footnote-1">
            *<i class="fa fa-link" aria-hidden="true"> </i>&nbsp;<a href="https://www.dovis.it/CORSmdn" target="_parent">dovis.it/CORSmdn</a> &nbsp;
            <a href="https://www.dovis.it/CORSwiki" target="_parent">dovis.it/CORSwiki</a>
               <br><br>
            </div>`;
}

const styleFooter = function () {
    return `<div class="container">
        <div class="custom-footer">
            <div class="row">
                <div class="col-md-6 col-lg-3 about-footer">
                    <img src="img/whitelogo.png" alt="">
                    <p>
                    ${getTextById(53)}<br>
                    ${getTextById(55)}<br>
                    <ul>
                        <li> ${getTextById(105)}</li>
                        <li>${getTextById(106)}</li>
                    </ul>
                    </p>
                </div>
                <div class="col-md-6 col-lg-3 page-more-info">
                    <ul>
                        <div class="footer-title">
                            <h5>Page links</h5>
                        </div>
                        <li><a href="#">Home</a></li>
                        <li>  <a href="#" class="dovis_footer list-inline-item  cms" data-page="about">
                           ${getTextById(57)}</a> 
                        </li>
                        <li>  <a href="#" class="dovis_footer list-inline-item cms btn  btn-outline-light" data-page="howto">
                            ${getTextById(140)}</a>
                         </li>
                        <li><a href="#">Pricing</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-3 page-more-info">
                    <ul>
                        <div class="footer-title">
                            <h5>Support</h5>
                        </div>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li> <a href="#" class="dovis_footer list-inline-item cms btn  btn-outline-light" data-page="contact">
                            ${getTextById(58)}</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-lg-2 page-more-info">

                    <ul>
                        <div class="footer-title">
                            <h5>Social</h5>
                        </div>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Instagram</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Youtube</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <hr>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-sm-8">
                    <p>© dovis.it 2022. Version:${backendVersion} (Backend), ${frontendVersion} (Frontend) All Rights Reserved.</p>
                </div>
                <div class="col-sm-3 ">
                    <a href="#" onclick="loadFile('./tos_${getCurrentLanguage()}.txt','main-content')">${getTextById(59)}</a> |
                    <a href="#" onclick="loadFile('./privacy_${getCurrentLanguage()}.txt','main-content')">${getTextById(60)}</a>
                </div>
            </div>
        </div>
    </div>`;
}

const styleHowto = function () {
    return `<div class="border border-light rounded" style="padding:15px;"><h4>${TEXT_DOVIS_FUNCTIONS}</h4><br>
                <ul>
                    <li> <a href="https://www.dovis.it/WTTA5A" target="_blank">${getTextById(141)}</a></li>
                    <li> <a href="https://www.dovis.it/G3C79A" target="_blank">${getTextById(142)}</a></li>
                    <li> <a href="https://www.dovis.it/FSXXMA" target="_blank">${getTextById(143)}</a></li>
                    <li> <a href="https://www.dovis.it/XAOMLY" target="_blank">${getTextById(144)}</a></li> 
                </ul>
            </div>`;
}