const cmsContent = {
    contact: styleContact(),
    about: styleAbout(),
    footer: styleFooter(),
    howto: styleHowto(),
}