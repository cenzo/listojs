"use strict";



const RMLVersion = `RMLRest2-1.9.3`;
const DOVISIT_HOME = `https://home.dovis.it/dovisit`;
const NO_SUCH_PAGE = `This redirection does not exist. Click here to create a new one`;

const redirect = function () {
    const webAdress = window.location.href;
    const shortUrlIndex = 1 + webAdress.lastIndexOf("?");
    const shortUrlParam = webAdress.substring(shortUrlIndex);
    let showUrlOnly = false;
    let shortUrl;
    if (shortUrlParam.endsWith('%2b')) {
        shortUrl = shortUrlParam.substring(0, shortUrlParam.length - 3);
        console.log("shortUrl: " + shortUrl);
        showUrlOnly = true;
    } else {
        shortUrl = shortUrlParam;
    }
    if (!shortUrl) window.location.href = DOVISIT_HOME;
    document.getElementById("shorturl").innerText = shortUrl;

    const xhttp = new XMLHttpRequest();
    xhttp.open("GET", `https://backend2.dovis.it/${RMLVersion}/rml/800/8/public/select?shortcut=${shortUrl}`, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let xhttpState = -1;
    xhttp.onreadystatechange = function () {
        xhttpState = xhttp.readyState;
        if (xhttpState === 4) {
            let jsonObject = JSON.parse(xhttp.responseText);
            let page;
            if (jsonObject.count === 0) {
                page = createErrorRedirect(webAdress, xhttp.status);
                document.getElementById("redirection").innerHTML = `dovis.it/${shortUrl}&nbsp;&nbsp;<i class="fas fa-arrow-right"></i>&nbsp;&nbsp;${page}`;
            } else {
                page = jsonObject.rows[0].s[2];
                document.getElementById("redirection").innerHTML = `dovis.it/${shortUrl}&nbsp;&nbsp;<i class="fas fa-arrow-right"></i>&nbsp;&nbsp;${page}`;
                if (showUrlOnly) {
                    document.getElementById("redirectionUrl").setAttribute("href", page);
                } else {
                    window.location.href = page;
                }
            }
        }
    };
    try {
        xhttp.send();
    } catch (err) {
        console.error(err.toString());
        if (xhttpState === 0) {
            document.getElementById("redirection").innerText = createErrorRedirect(webAdress, 0);
        }
    }
};


const createErrorRedirect = function (redirection, httpStatus) {
    document.getElementById("redirectionUrl").setAttribute("href", DOVISIT_HOME);
    return NO_SUCH_PAGE;
}

