# Listojs

Listojs is an experimental single-page application (SPA) that allows api-communications, and adresses several purposes.
The current reference-version of the software implements a frontend to manage restaurant-functionalities. The application uses a (jQuery-based), customized version of the [AdminLTE](https://adminlte.io/)-software in order
to administer the customer-frontends and allow the staff-operations (waiter, cook, manager, web-administrator). <br/>
Data are retrieved via a special JSON-based API, which returns a special tabular JSON - format with headers, whose data must be accessed by zero-based arrays.
The general format of this json-data has the following generic structure:<br/>


>  {

>  "_VariableLabels": [ ` ... ` ], "_VariableTypes": [` ... `],

>  "_rc": `returncode`,"count": `number of returned rows`,

>  "rows": [ { "rn": `rownumber`, "s": [ `zero-based array with the actual data to be accessed`]},

>  `more rows:`{...},{...},],

>  "variableNames": [` ... `]
    

>  }

 <br/>
 
Data can be accessed via the  `row-index`  and the `column-index`, but Listojs provides also a convenience `get`-function which can be used to access the data by the corresponding 
`label` of the variable. Hence, there are two possibilities to read the JSON-data. Either by using the row and column indices:<br/>

>   var someVariable=data.rows[`rowIndex`].s[`columnIndex`]

or by using the get-function with the variable-label:

>   var someVariable=get(data, `rowIndex`, `variableLabel`)


The library uses the following function for Ajax-calls:

> sendAjax(`urlShort`, params, myCallback, args)


where `urlShort` is one of the calls to the documented API (See API-directory of this documentation).

Non-public calls are implemented with a bearer-token-authentification-mechanism. See below example. <br/><br/> 

**EXAMPLE**<br/>


CALL-ID listorante.Waiter.6 of the waiterAPI 
>  "/listorante/waiter/showcart/" + bill + "/" + table+ "?LOC=en"

returns following JSON:<br/><br/>
{
    "_VariableLabels": [
        `"qty",
        "prodname",
        "idproduct",
        "price",
        "pricesum",
        "tablenumber",
        "idbillnumber"`
    ],<br/>
    "_VariableTypes": [
        `"stringType",
        "stringType",
        "stringType",
        "stringType",
        "stringType",
        "integerType",
        "integerType"`
    ],<br/>
    "_rc": `0`,
    "count": `17`,<br/>
    "rows": [
        {
            "rn": `1`,
            "s": [
                `"36",
                "Hamburger",
                "9",
                "2.99",
                "107.64",
                "20",
                "27"`
            ]
        },
       {` ... `},{` ... `},
         {
            "rn": `17`,
            "s": [
                `"433",
                "TOTAL",
                " ",
                "0.00",
                "3,003.51",
                "20",
                "27"`
            ]
        }
    ],<br/>
    "variableNames": [
        `"qty",
        "prodname_en",
        "idproduct",
        "price",
        "pricesum",
        "tablenumber",
        "idbillnumber"`
    ]
}
<br/><br/>
Based on this JSON-data, getting the  product-name of the first row can be achieved in following ways. Either:<br/>
>  var productname=data.rows[`0`].s[`1`]<br/>

or:<br/>

>  var productname=get(data,`0`,`"prodname"`)

**Beware**: `rows.rn` returns the `NON-zero-based index` of the row. <br/>

As this is a secured call with prior bearer-token-authentication, below function can be used to retrieve the JSON-data. (Prior to the call, the client must login and set a cookie
with the returned token from the login-call. This is already implemented in the Login-screen of the application).



>  function showCart(bill, table) {

>   var bearerCookie = getCookie(bearerCookieName);

>  	var url = "/listorante/waiter/showcart/" + bill + "/" + table + "?LOC="
			+ locale;

>  	sendAjax(url, {header : {"Authorization" : "Bearer " + bearerCookie,"Content-Type" : "application/x-www-form-urlencoded;charset=UTF-8"}	}, 

>   function(data) {

>  var prodname1=data.rows[0].s[1];

>  var prodname2=get(data,1,"prodname");

>  // do something else ...

>   },{"met" : "post" });

>  }

**DEMO:** [(NON-SSL-Beta-Version in current development) www.listorante.com/integration](http://www.listorante.com/integration)<br/>username:demo, passwort:demo
