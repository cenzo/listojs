/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Warehouse'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Warehouse.apiCall1
	select all items currently being procured from the warehouse
	***************************************/
function apiCall_elisto_warehouse_items
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Warehouse.apiCall1","/4/warehouse/items", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Warehouse.apiCall2
	Update delivery status of an ordered item
	***************************************/
function apiCall_elisto_warehouse_setprodstatus
	(idorder,
	status,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"status" : status
	};
	return apiCall("elisto.Warehouse.apiCall2","/4/warehouse/setprodstatus", dataObject, callBack, httpErrorCallBack,"put");
}


