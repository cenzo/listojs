/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Customer'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Customer.apiCall1
	Add a product-item to the order-list.
	***************************************/
function apiCall_elisto_customer_addtoproductlist
	(departmentnumber,
	executivenumber,
	idinvoicenumber,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber,
	"executivenumber" : executivenumber,
	"idinvoicenumber" : idinvoicenumber,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("elisto.Customer.apiCall1","/4/customer/addtoproductlist", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall2
	Delete a product from the order-list
	***************************************/
function apiCall_elisto_customer_delitem
	(idorder,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder
	};
	return apiCall("elisto.Customer.apiCall2","/4/customer/delitem", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall3
	Set remark on an ordered item 
	***************************************/
function apiCall_elisto_customer_itemremark
	(idorder,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder
	};
	return apiCall("elisto.Customer.apiCall3","/4/customer/itemremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall4
	Show shopping cart related to an invoice 
	***************************************/
function apiCall_elisto_customer_showcart
	(invoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"invoicenumber" : invoicenumber
	};
	return apiCall("elisto.Customer.apiCall4","/4/customer/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall5
	Show the status of the ordered items of an invoice 
	***************************************/
function apiCall_elisto_customer_deliverystatus
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Customer.apiCall5","/4/customer/deliverystatus", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall6
	Send the list of ordered items to the warehouse
	***************************************/
function apiCall_elisto_customer_sendorder
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Customer.apiCall6","/4/customer/sendorder", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall7
	Send a message to an executive
	***************************************/
function apiCall_elisto_customer_sendnotification
	(idexecutive,
	idinvoice,
	text,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idexecutive" : idexecutive,
	"idinvoice" : idinvoice,
	"text" : text
	};
	return apiCall("elisto.Customer.apiCall7","/4/customer/sendnotification", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall8
	Get meta-information about the selected invoice
	***************************************/
function apiCall_elisto_customer_invoiceinfo
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Customer.apiCall8","/4/customer/invoiceinfo", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall9
	Initiate a new invoice; it will be assigned to the chief-executive (chief-executive has idexecutive=1)
	***************************************/
function apiCall_elisto_customer_createinvoice
	(departmentnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber
	};
	return apiCall("elisto.Customer.apiCall9","/4/customer/createinvoice", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall10
	Get order-information per user of the selected invoice
	***************************************/
function apiCall_elisto_customer_orderuserinfo
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Customer.apiCall10","/4/customer/orderuserinfo", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Customer.apiCall11
	Get the department-id of the logged-in customer
	***************************************/
function apiCall_elisto_customer_getcustomerdepartment
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Customer.apiCall11","/4/customer/getcustomerdepartment", dataObject, callBack, httpErrorCallBack,"post");
}


