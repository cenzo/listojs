/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Admin'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Admin.apiCall1
	Create a new product
	***************************************/
function apiCall_elisto_admin_newproduct
	(price,
	prod_desc,
	prod_image,
	prod_info,
	prod_label,
	prod_name,
	qty_in_stock,
	qty_threshold,
	unit,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"price" : price,
	"prod_desc" : prod_desc,
	"prod_image" : prod_image,
	"prod_info" : prod_info,
	"prod_label" : prod_label,
	"prod_name" : prod_name,
	"qty_in_stock" : qty_in_stock,
	"qty_threshold" : qty_threshold,
	"unit" : unit
	};
	return apiCall("elisto.Admin.apiCall1","/4/admin/newproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall2
	Create a new product-category underneath an existing parent category.
		('idproductcategory' here is the parent-id. The newly created category will be assigned a new id). 
		 
	***************************************/
function apiCall_elisto_admin_newcategory
	(hierarchy_1,
	hierarchy_2,
	hierarchy_3,
	hierarchy_4,
	hierarchy_5,
	idproductcategory,
	level,
	position,
	prodcatdesc,
	prodcatdesc_locale,
	prodcatimage,
	prodcatname,
	prodcatname_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"hierarchy_1" : hierarchy_1,
	"hierarchy_2" : hierarchy_2,
	"hierarchy_3" : hierarchy_3,
	"hierarchy_4" : hierarchy_4,
	"hierarchy_5" : hierarchy_5,
	"idproductcategory" : idproductcategory,
	"level" : level,
	"position" : position,
	"prodcatdesc" : prodcatdesc,
	"prodcatdesc_locale" : prodcatdesc_locale,
	"prodcatimage" : prodcatimage,
	"prodcatname" : prodcatname,
	"prodcatname_locale" : prodcatname_locale
	};
	return apiCall("elisto.Admin.apiCall2","/4/admin/newcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall3
	Create new staff-person
	***************************************/
function apiCall_elisto_admin_newstaff
	(email,
	first_name,
	is_active,
	is_superuser,
	last_name,
	password,
	username,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"is_active" : is_active,
	"is_superuser" : is_superuser,
	"last_name" : last_name,
	"password" : password,
	"username" : username
	};
	return apiCall("elisto.Admin.apiCall3","/4/admin/newstaff", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall4
	Delete a staff-person
	***************************************/
function apiCall_elisto_admin_deletestaff
	(USERIDVALUE,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"USERIDVALUE" : USERIDVALUE
	};
	return apiCall("elisto.Admin.apiCall4","/4/admin/deletestaff", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall5
	Assign role to staff person
	***************************************/
function apiCall_elisto_admin_assignrole
	(roleid,
	userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid,
	"userid" : userid
	};
	return apiCall("elisto.Admin.apiCall5","/4/admin/assignrole", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall6
	Revoke role from staff person
	***************************************/
function apiCall_elisto_admin_revokerole
	(roleid,
	userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid,
	"userid" : userid
	};
	return apiCall("elisto.Admin.apiCall6","/4/admin/revokerole", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall7
	select a layout
	***************************************/
function apiCall_elisto_admin_setlayoutid
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall7","/4/admin/setlayoutid", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall8
	select all possible structures of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutstructures
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall8","/4/admin/layoutstructures", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall9
	select all possible images of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutimages
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall9","/4/admin/layoutimages", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall10
	select all possible colors of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutcolors
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall10","/4/admin/layoutcolors", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall11
	Update a layout-text. Works currently only for texts. This call nNeeds to be amende for superadmin
	***************************************/
function apiCall_elisto_admin_updatelayout
	(layoutelementname,
	layoutid,
	value,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutelementname" : layoutelementname,
	"layoutid" : layoutid,
	"value" : value
	};
	return apiCall("elisto.Admin.apiCall11","/4/admin/updatelayout", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall13
	edit a department
	***************************************/
function apiCall_elisto_admin_editdepartment
	(cnt_participants,
	description,
	description_locale,
	iddepartment,
	isonlyone,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"cnt_participants" : cnt_participants,
	"description" : description,
	"description_locale" : description_locale,
	"iddepartment" : iddepartment,
	"isonlyone" : isonlyone
	};
	return apiCall("elisto.Admin.apiCall13","/4/admin/editdepartment", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall14
	create a new department
	***************************************/
function apiCall_elisto_admin_createdepartment
	(cnt_participants,
	description,
	description_locale,
	isonlyone,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"cnt_participants" : cnt_participants,
	"description" : description,
	"description_locale" : description_locale,
	"isonlyone" : isonlyone
	};
	return apiCall("elisto.Admin.apiCall14","/4/admin/createdepartment", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall15
	Delete a department
	***************************************/
function apiCall_elisto_admin_deletedepartment
	(iddepartment,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iddepartment" : iddepartment
	};
	return apiCall("elisto.Admin.apiCall15","/4/admin/deletedepartment", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall16
	Update profile data
	***************************************/
function apiCall_elisto_admin_editprofile
	(email,
	first_name,
	iduser,
	image,
	is_active,
	is_staff,
	last_name,
	username,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"first_name" : first_name,
	"iduser" : iduser,
	"image" : image,
	"is_active" : is_active,
	"is_staff" : is_staff,
	"last_name" : last_name,
	"username" : username
	};
	return apiCall("elisto.Admin.apiCall16","/4/admin/editprofile", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall17
	Edit a product-category
	***************************************/
function apiCall_elisto_admin_editcategory
	(hierarchy_1,
	hierarchy_2,
	hierarchy_3,
	hierarchy_4,
	hierarchy_5,
	idproductcategory,
	idstyle,
	position,
	prodcatdesc,
	prodcatdesc_locale,
	prodcatimage,
	prodcatname,
	prodcatname_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"hierarchy_1" : hierarchy_1,
	"hierarchy_2" : hierarchy_2,
	"hierarchy_3" : hierarchy_3,
	"hierarchy_4" : hierarchy_4,
	"hierarchy_5" : hierarchy_5,
	"idproductcategory" : idproductcategory,
	"idstyle" : idstyle,
	"position" : position,
	"prodcatdesc" : prodcatdesc,
	"prodcatdesc_locale" : prodcatdesc_locale,
	"prodcatimage" : prodcatimage,
	"prodcatname" : prodcatname,
	"prodcatname_locale" : prodcatname_locale
	};
	return apiCall("elisto.Admin.apiCall17","/4/admin/editcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall18
	Edit a product
	***************************************/
function apiCall_elisto_admin_editproduct
	(idlabel,
	idprodstyle,
	idproduct,
	image,
	image_thumb,
	position,
	price,
	proddesc_locale,
	prodinfo_locale,
	prodname,
	prodname_locale,
	qty_in_stock,
	qty_unit,
	threshold_qty,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlabel" : idlabel,
	"idprodstyle" : idprodstyle,
	"idproduct" : idproduct,
	"image" : image,
	"image_thumb" : image_thumb,
	"position" : position,
	"price" : price,
	"proddesc_locale" : proddesc_locale,
	"prodinfo_locale" : prodinfo_locale,
	"prodname" : prodname,
	"prodname_locale" : prodname_locale,
	"qty_in_stock" : qty_in_stock,
	"qty_unit" : qty_unit,
	"threshold_qty" : threshold_qty
	};
	return apiCall("elisto.Admin.apiCall18","/4/admin/editproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall19
	Assign a product to a category
	***************************************/
function apiCall_elisto_admin_setproductcategory
	(ididhierarchy,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ididhierarchy" : ididhierarchy,
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall19","/4/admin/setproductcategory", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall20
	Remove a product from a category
	***************************************/
function apiCall_elisto_admin_unsetproductcategory
	(ididhierarchy,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ididhierarchy" : ididhierarchy,
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall20","/4/admin/unsetproductcategory", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall21
	Admin  Dashboard 
	***************************************/
function apiCall_elisto_admin_admindash
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall21","/4/admin/admindash", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall22
	select all items currently in pipeline
	***************************************/
function apiCall_elisto_admin_orders
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall22","/4/admin/orders", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall23
	Define a new product attribute
	***************************************/
function apiCall_elisto_admin_defineattribute
	(attribute_type,
	attributemnemonic,
	productattributename_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attribute_type" : attribute_type,
	"attributemnemonic" : attributemnemonic,
	"productattributename_locale" : productattributename_locale
	};
	return apiCall("elisto.Admin.apiCall23","/4/admin/defineattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall24
	Assign a  product attribute
	***************************************/
function apiCall_elisto_admin_setattribute
	(idattribute,
	idproduct,
	productattributevalue_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct,
	"productattributevalue_locale" : productattributevalue_locale
	};
	return apiCall("elisto.Admin.apiCall24","/4/admin/setattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall25
	De-assign a  product attribute
	***************************************/
function apiCall_elisto_admin_unsetattribute
	(idattribute,
	idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall25","/4/admin/unsetattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall26
	Delete an attribute-definition
	***************************************/
function apiCall_elisto_admin_deleteattributedefinition
	(idproductattributename,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproductattributename" : idproductattributename
	};
	return apiCall("elisto.Admin.apiCall26","/4/admin/deleteattributedefinition", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall27
	Insert a new image for a product
	***************************************/
function apiCall_elisto_admin_insertproductimage
	(idproduct,
	imagepath,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct,
	"imagepath" : imagepath
	};
	return apiCall("elisto.Admin.apiCall27","/4/admin/insertproductimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall28
	Delete an image of a product
	***************************************/
function apiCall_elisto_admin_deleteproductimage
	(idlnk_product_image,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlnk_product_image" : idlnk_product_image
	};
	return apiCall("elisto.Admin.apiCall28","/4/admin/deleteproductimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall29
	Define a new localized text 
	***************************************/
function apiCall_elisto_admin_newText
	(id,
	mnemonic,
	text_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id,
	"mnemonic" : mnemonic,
	"text_locale" : text_locale
	};
	return apiCall("elisto.Admin.apiCall29","/4/admin/newText", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall30
	Update a localized text 
	***************************************/
function apiCall_elisto_admin_editText
	(id,
	mnemonic,
	text_locale,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id,
	"mnemonic" : mnemonic,
	"text_locale" : text_locale
	};
	return apiCall("elisto.Admin.apiCall30","/4/admin/editText", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall31
	select all user profiles 
	***************************************/
function apiCall_elisto_admin_userprofiles
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall31","/4/admin/userprofiles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall32
	select all roles
	***************************************/
function apiCall_elisto_admin_userroles
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall32","/4/admin/userroles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall33
	select roles of a given user
	***************************************/
function apiCall_elisto_admin_getuserroles
	(userid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"userid" : userid
	};
	return apiCall("elisto.Admin.apiCall33","/4/admin/getuserroles", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall34
	select owners of a given role
	***************************************/
function apiCall_elisto_admin_getroleowners
	(roleid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"roleid" : roleid
	};
	return apiCall("elisto.Admin.apiCall34","/4/admin/getroleowners", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall35
	select all possible fonts of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutfonts
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall35","/4/admin/layoutfonts", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall36
	select all possible texts of the selected layout
	***************************************/
function apiCall_elisto_admin_layouttexts
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall36","/4/admin/layouttexts", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall37
	delete a product
	***************************************/
function apiCall_elisto_admin_deleteproduct
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Admin.apiCall37","/4/admin/deleteproduct", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall38
	select all css-elements of the selected layout
	***************************************/
function apiCall_elisto_admin_layoutelements
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall38","/4/admin/layoutelements", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall39
	move an existing category under a new parent category
	***************************************/
function apiCall_elisto_admin_linkcategory
	(idcategory,
	idparentcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory,
	"idparentcategory" : idparentcategory
	};
	return apiCall("elisto.Admin.apiCall39","/4/admin/linkcategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall40
	delete a category
	***************************************/
function apiCall_elisto_admin_deletecategory
	(idcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory
	};
	return apiCall("elisto.Admin.apiCall40","/4/admin/deletecategory", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall41
	Edit a  product attribute value
	***************************************/
function apiCall_elisto_admin_editattribute
	(idattribute,
	idproduct,
	value,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idattribute" : idattribute,
	"idproduct" : idproduct,
	"value" : value
	};
	return apiCall("elisto.Admin.apiCall41","/4/admin/editattribute", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall42
	Edit a  product attribute name
	***************************************/
function apiCall_elisto_admin_editattributename
	(attributename,
	idproductattributename,
	mnemonic,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attributename" : attributename,
	"idproductattributename" : idproductattributename,
	"mnemonic" : mnemonic
	};
	return apiCall("elisto.Admin.apiCall42","/4/admin/editattributename", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall43
	Update a css-file with the saved element-types
	***************************************/
function apiCall_elisto_admin_updatecss
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall43","/4/admin/updatecss", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall44
	Get the css for a specific layoutid
	***************************************/
function apiCall_elisto_admin_getcss
	(layoutid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layoutid" : layoutid
	};
	return apiCall("elisto.Admin.apiCall44","/4/admin/getcss", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall45
	Show all open tickets
	***************************************/
function apiCall_elisto_admin_tickets
	(customerid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid
	};
	return apiCall("elisto.Admin.apiCall45","/4/admin/tickets", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall46
	Create a new support-ticket
	***************************************/
function apiCall_elisto_admin_createticket
	(customerid,
	ticketdefid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customerid" : customerid,
	"ticketdefid" : ticketdefid
	};
	return apiCall("elisto.Admin.apiCall46","/4/admin/createticket", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall47
	Upload text and or data for a support-ticket
	***************************************/
function apiCall_elisto_admin_addticketdata
	(attachedfile,
	customertext,
	ticketid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"attachedfile" : attachedfile,
	"customertext" : customertext,
	"ticketid" : ticketid
	};
	return apiCall("elisto.Admin.apiCall47","/4/admin/addticketdata", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall48
	Get all data of a support-ticket
	***************************************/
function apiCall_elisto_admin_ticketdata
	(ticketid,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"ticketid" : ticketid
	};
	return apiCall("elisto.Admin.apiCall48","/4/admin/ticketdata", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall49
	Show which services can be requested
	***************************************/
function apiCall_elisto_admin_ticketdef
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall49","/4/admin/ticketdef", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall50
	Show shopping cart of a customer 
	***************************************/
function apiCall_elisto_admin_showcart
	(billnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"billnumber" : billnumber
	};
	return apiCall("elisto.Admin.apiCall50","/4/admin/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall51
	get customer-ID
	***************************************/
function apiCall_elisto_admin_customerid
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall51","/4/admin/customerid", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall52
	change user password
	***************************************/
function apiCall_elisto_admin_changeuserpassword
	(iduser,
	password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iduser" : iduser,
	"password" : password
	};
	return apiCall("elisto.Admin.apiCall52","/4/admin/changeuserpassword", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall53
	store a new string for a certain context
	***************************************/
function apiCall_elisto_admin_storetext
	(idsemantic,
	stringvalue,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idsemantic" : idsemantic,
	"stringvalue" : stringvalue
	};
	return apiCall("elisto.Admin.apiCall53","/4/admin/storetext", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall54
	update a string for a certain context
	***************************************/
function apiCall_elisto_admin_updatetext
	(idsemantic,
	stringvalue,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idsemantic" : idsemantic,
	"stringvalue" : stringvalue
	};
	return apiCall("elisto.Admin.apiCall54","/4/admin/updatetext", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall55
	update a settings-value
	***************************************/
function apiCall_elisto_admin_setcustomerpassword
	(setting,
	val,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"setting" : setting,
	"val" : val
	};
	return apiCall("elisto.Admin.apiCall55","/4/admin/setcustomerpassword", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall56
	Assign an employee-id to a department 
	***************************************/
function apiCall_elisto_admin_setcustomerdepartment
	(iddepartment,
	iduser,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"iddepartment" : iddepartment,
	"iduser" : iduser
	};
	return apiCall("elisto.Admin.apiCall56","/4/admin/setcustomerdepartment", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall57
	Show the current amount of available credits
	***************************************/
function apiCall_elisto_admin_showCredits
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall57","/4/admin/showCredits", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall58
	Select the user-data of the currently logged-in Administrator
	***************************************/
function apiCall_elisto_admin_adminuserdata
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Admin.apiCall58","/4/admin/adminuserdata", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Admin.apiCall59
	Insert the stripe account id
	***************************************/
function apiCall_elisto_admin_setstripeid
	(stripe_user_account,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"stripe_user_account" : stripe_user_account
	};
	return apiCall("elisto.Admin.apiCall59","/4/admin/setstripeid", dataObject, callBack, httpErrorCallBack,"post");
}


