/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Public'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Public.apiCall1
	Show Products
	***************************************/
function apiCall_elisto_public_products
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall1","/4/public/products", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall2
	Show Categories
	***************************************/
function apiCall_elisto_public_categories
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall2","/4/public/categories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall3
	Show to which categories a given product belongs to
	***************************************/
function apiCall_elisto_public_productcategories
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Public.apiCall3","/4/public/productcategories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall4
	Show child-categories of a category
	***************************************/
function apiCall_elisto_public_category
	(category,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"category" : category
	};
	return apiCall("elisto.Public.apiCall4","/4/public/category", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall5
	Show which products belong to the subtree of a given category
	***************************************/
function apiCall_elisto_public_product
	(idcategory,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcategory" : idcategory
	};
	return apiCall("elisto.Public.apiCall5","/4/public/product", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall6
	Get a Text by id
	***************************************/
function apiCall_elisto_public_textbyid
	(id,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"id" : id
	};
	return apiCall("elisto.Public.apiCall6","/4/public/textbyid", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall7
	Get a Text by mnemonic
	***************************************/
function apiCall_elisto_public_textbymnemonic
	(mnemonic,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"mnemonic" : mnemonic
	};
	return apiCall("elisto.Public.apiCall7","/4/public/textbymnemonic", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall8
	[Under development] New client registration
	***************************************/
function apiCall_elisto_public_register
	(domainName,
	eMail,
	firstName,
	lastName,
	passWord,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName,
	"eMail" : eMail,
	"firstName" : firstName,
	"lastName" : lastName,
	"passWord" : passWord
	};
	return apiCall("elisto.Public.apiCall8","/4/public/register", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Public.apiCall9
	[This call is deprecated]: Check  a domain-name
	***************************************/
function apiCall_elisto_public_domaincheck
	(domainName,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"domainName" : domainName
	};
	return apiCall("elisto.Public.apiCall9","/4/public/domaincheck", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall10
	Show available layouts
	***************************************/
function apiCall_elisto_public_layouts
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall10","/4/public/layouts", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall11
	Show attributes of a product
	***************************************/
function apiCall_elisto_public_getproductattributes
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Public.apiCall11","/4/public/getproductattributes", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall12
	Show all attribute-definitions 
	***************************************/
function apiCall_elisto_public_allproductattributes
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall12","/4/public/allproductattributes", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall13
	Select images of a product
	***************************************/
function apiCall_elisto_public_productimages
	(idproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idproduct" : idproduct
	};
	return apiCall("elisto.Public.apiCall13","/4/public/productimages", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall14
	[This call is deprecated]: show all departments
	***************************************/
function apiCall_elisto_public_showdepartments
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall14","/4/public/showdepartments", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall15
	Show parent-categories of a category
	***************************************/
function apiCall_elisto_public_parentcategories
	(category,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"category" : category
	};
	return apiCall("elisto.Public.apiCall15","/4/public/parentcategories", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall16
	Select id of a product by its label 
	***************************************/
function apiCall_elisto_public_idproductbylabel
	(idlabel,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idlabel" : idlabel
	};
	return apiCall("elisto.Public.apiCall16","/4/public/idproductbylabel", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall17
	Select all variable texts of a layout
	***************************************/
function apiCall_elisto_public_assignedtexts
	(layout,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"layout" : layout
	};
	return apiCall("elisto.Public.apiCall17","/4/public/assignedtexts", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall18
	Show id of currently selected  layout
	***************************************/
function apiCall_elisto_public_layoutid
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Public.apiCall18","/4/public/layoutid", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall19
	Select a long paragraph as html or text via the contextid
	***************************************/
function apiCall_elisto_public_longtext
	(idcontext,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idcontext" : idcontext
	};
	return apiCall("elisto.Public.apiCall19","/4/public/longtext", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall20
	Select a long paragraph as html or text by its contextname
	***************************************/
function apiCall_elisto_public_longtextbystr
	(contextname,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"contextname" : contextname
	};
	return apiCall("elisto.Public.apiCall20","/4/public/longtextbystr", dataObject, callBack, httpErrorCallBack,"get");
}

/***************************************
	CALL-ID: elisto.Public.apiCall21
	Get the value of a setting
	***************************************/
function apiCall_elisto_public_settings
	(setting,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"setting" : setting
	};
	return apiCall("elisto.Public.apiCall21","/4/public/settings", dataObject, callBack, httpErrorCallBack,"post");
}


