// Check HTML5 File API Browser Support

let waitingForUpload = [];
let kbCount = 0;
let allowedKB = 0;
let nrOfUnAllowedFiles = 0;
const FILE_TYPE_DISPLAYED = {};
FILE_TYPE_DISPLAYED.WORD = "far fa-file-word fa-7x";
FILE_TYPE_DISPLAYED.EXCEL = "far fa-file-excel fa-7x";
FILE_TYPE_DISPLAYED.PDF = "far fa-file-pdf fa-7x";
FILE_TYPE_DISPLAYED.UNKNWON = "far fa-file fa-7x";
FILE_TYPE_DISPLAYED.POWERPOINT = "fas fa-file-powerpoint fa-7x";
FILE_TYPE_DISPLAYED.CSV = "fas fa-file-csv fa-7x";
FILE_TYPE_DISPLAYED.TXT = "far fa-file-alt fa-7x";
const maxAllowedKB = 10000;
const WWW_IMAGE_PATH = "https://enpasoft.com/listorante/applications/images";
const UPLOAD_URL = "/upload.php";
const TEXT_NUMBER_OF_UPLOADED_FILES = " Number of uploaded file(s): ";
const TEXT_UPLOAD = " Upload ";
const FILE_IDENTIFIER = "D";
const CUSTOMER_ID = 4711;//this must be changed so to be retrieved via some function
const USER_ID = 4713;//this must be changed so to be retrieved via some function
//const LOAD_IMAGE = WWW_IMAGE_PATH + "/loading_2d_blue.gif";
const LOAD_IMAGE = WWW_IMAGE_PATH + "/ajax-loader.gif";
//const LOAD_IMAGE =  "/loading_3d_balls_red.png";
const ICON_OK = "fas fa-check";
const ICON_CHECK_DOUBLE = "fas fa-check-double fa-3x";
const ICON_ERROR = "fas fa-exclamation-circle";
const ICON_UPLOADING = "fas fa-cloud-upload-alt";
const ICON_UPLOAD = "fas fa-upload";
const ICON_UPLOAD_ALL = "far fa-list-alt fa-2x";
let preLoadImage = document.getElementById("preloadImage");

let progressBar = document.getElementById("progressbar");
progressBar.hidden = true;
if (window.File && window.FileList && window.FileReader) {
    function showFile() {
        let preview = document.getElementById("preview");
        let uploadAllElement = document.getElementById("uploadAll");
        preview.innerHTML = "";
        uploadAllElement.innerHTML = "";
        allowedKB = 0;
        let progressText = document.getElementById("progress");
        progressText.innerText = "";
        let kbElement = document.getElementById("kilobytes");
        kbElement.innerText = "";
        progressBar.hidden = true;
        let countFiles = 0;
        nrOfUnAllowedFiles = 0;
        let waitingFiles = [];
        let selectedKB = 0;

        let selectedFiles = {};
        selectedFiles.files = [];

        kbElement = document.getElementById("kilobytes");
        kbElement.innerText = "";

        // let fileInput = document.querySelector("input[type=file]");
        let fileInput = document.getElementById("documents-fileSelector");

        let uploadAllButton;
        preLoadImage.hidden = false;
        for (let i = 0; i < fileInput.files.length; i++) {
            let fi = fileInput.files[i];
            selectedKB += fi.size;
            selectedFiles.files[i] = fi;
            let displayReader = new FileReader();
            let imgItem = document.createElement("img");

            let uploadButton = document.createElement("button");
            let progressMsg = document.createElement("small");
            progressMsg.setAttribute("id", "msg" + i);
            progressMsg.innerText = "";
            uploadButton.innerText = TEXT_UPLOAD;
            uploadButton.setAttribute("class", "sendToServer btn btn-secondary dummy");
            uploadButton.setAttribute("data-file" + i, fi.name);
            uploadButton.setAttribute("data-index", i + "");
            uploadButton.setAttribute("id", "uploadButton" + i);
            displayReader.onload = function (readerEvent) {

                let listItem = document.createElement("li");
                let fType = fi['type'];
                let progressIcon = document.createElement("i");
                progressIcon.setAttribute("class", ICON_UPLOAD);
                progressIcon.setAttribute("id", "progressIcon" + i);

                //progressIcon.hidden = true;

                function setShownFileType(fType) {

                    if (fType.includes("word"))
                        return FILE_TYPE_DISPLAYED.WORD;
                    else if (fType.includes("excel") || fType.includes("spreadsheet"))
                        return FILE_TYPE_DISPLAYED.EXCEL;
                    else if (fType.includes("pdf"))
                        return FILE_TYPE_DISPLAYED.PDF;
                    else if (fType.includes("presentation"))
                        return FILE_TYPE_DISPLAYED.POWERPOINT;
                    else if (fType.includes("csv"))
                        return FILE_TYPE_DISPLAYED.CSV;
                    else if (fType.includes("text"))
                        return FILE_TYPE_DISPLAYED.TXT;
                    else
                        return FILE_TYPE_DISPLAYED.UNKNWON;
                }

                listItem.setAttribute("id", "listItem" + i);
                let fileTextParagraph = document.createElement("p");
                let shownFileText;
                if (fi.name.length < 12) {
                    shownFileText = fi.name;
                } else {
                    shownFileText = "[" + fi.name.substring(0, 9) + "...]";
                }
                if (fType.startsWith("image")) {
                    imgItem.setAttribute("src", readerEvent.target.result);
                    imgItem.setAttribute("id", fi.name);
                    imgItem.setAttribute("alt", fi.name + " (" + Math.round(fi.size / 1024) + " KB)");
                    imgItem.setAttribute("title", fi.name + " (" + Math.round(fi.size / 1024) + " KB)");
                    listItem.appendChild(imgItem);
                } else {
                    let shownTypeIcon = document.createElement("i");
                    listItem.appendChild(shownTypeIcon);
                    shownTypeIcon.setAttribute("class", setShownFileType(fType));
                    shownTypeIcon.setAttribute("title", fi.name + " (" + Math.round(fi.size / 1024) + " KB)");
                }
                let fileSizeText = document.createElement("b");
                fileSizeText.innerText = Math.round(fi.size / 1024) + " KB";
                fileTextParagraph.innerText = shownFileText;
                const brLine = document.createElement("br");
                fileTextParagraph.appendChild(brLine);
                fileTextParagraph.appendChild(fileSizeText);
                listItem.appendChild(fileTextParagraph);
                uploadButton.appendChild(progressIcon);
                uploadButton.appendChild(progressMsg);
                preview.append(listItem);
                console.log("name of file: " + i + ": " + fi.name + "\tsize:" + fi.size, "\ttype:" + fi.type);
                let fSize = Math.round(fi.size / 1024);
                if (fSize <= maxAllowedKB) {
                    waitingFiles.push(i);
                    //console.log("waitingFiles:" + JSON.stringify(waitingFiles));
                    allowedKB += fi.size;

                    listItem.appendChild(uploadButton);
                } else {
                    nrOfUnAllowedFiles++;
                    let tooBigIcon = document.createElement("i");
                    tooBigIcon.setAttribute("class", ICON_ERROR);
                    listItem.appendChild(tooBigIcon);
                    let tooBigMsg = document.createElement("b");
                    tooBigMsg.innerText = "(>" + maxAllowedKB + " KB)";
                    listItem.appendChild(tooBigMsg);
                }
            }
            displayReader.onloadend = function () {

                if (i === 0) preLoadImage.hidden = true;
                // assign local to global variables
                countFiles = waitingFiles.length;
                waitingForUpload = waitingFiles;
                console.log("waitingForUpload: " + JSON.stringify(waitingForUpload));
                kbCount = 0;
                // end of assigning local to global variables
                if (waitingFiles.length > 1) {

                    if (!uploadAllButton) {
                        uploadAllButton = document.createElement("button");
                        uploadAllButton.innerText = " Upload ALL ";
                        const allFilesIcon = document.createElement("i");
                        allFilesIcon.setAttribute("class", ICON_UPLOAD_ALL);
                        allFilesIcon.setAttribute("id", "uploadAllIcon");
                        uploadAllButton.appendChild(allFilesIcon);
                        uploadAllButton.setAttribute("class", "btn btn-secondary sendToServer");
                        uploadAllButton.setAttribute("data-file", "ALL");
                        uploadAllButton.setAttribute("data-index", "-1");
                        uploadAllButton.setAttribute("id", "uploadAllButton");
                        uploadAllElement.appendChild(uploadAllButton);
                    }
                }
                if (nrOfUnAllowedFiles === 0) {
                    progressText.innerText = "Selected Size: " + Math.round(selectedKB / 1024) + " KB (" + waitingFiles.length + " files)";
                } else {
                    progressText.innerText = "Selected Size: " + Math.round(selectedKB / 1024) + " KB, allowed for upload: " + Math.round(allowedKB / 1024) + " KB (" + waitingFiles.length + " files, " + nrOfUnAllowedFiles + " file(s) is/are too big)";
                }
                document.getElementById("progressbar").value = 0;
            }
            displayReader.readAsDataURL(fi);//displays the image as preview
        }

    }

    function dragOverHandler(evt) {
        evt.preventDefault();
        let fileInput = evt.dataTransfer;
        for (let k = 0; k < fileInput.files.length; k++) alert(k + ":" + fileInput.files[k].name);

    };

    function dropHandler(evt) {
        evt.preventDefault();
        let fileInput = evt.dataTransfer;
        for (let k = 0; k < fileInput.files.length; k++) console.log(k + ":" + fileInput.files[k].name);

    }


    $(document).on('click', '.sendToServer', function () {
        const idx = $(this).data('index');
        let kbElement = document.getElementById("kilobytes");


        let progressBar = document.getElementById("progressbar");
        let progressText = document.getElementById("progress");

        function uploadTheFile(idx) {
            try {
                const fileId = document.getElementById('uploadButton' + idx).getAttribute("data-file" + idx);
                console.log("Try to upload  file " + idx + " [" + fileId + "]:");
            } catch (err) {
            }
            let loadImageElement = document.createElement("img");

            let uploadButton = document.getElementById("uploadButton" + idx);
            let uploadAllButton = document.getElementById("uploadAllButton");
            let uploadAllIcon = document.getElementById("uploadAllIcon");
            let errMsg = document.getElementById("msg" + idx);
            if (errMsg) errMsg.innerText = "";
            let progressIcon = document.getElementById("progressIcon" + idx);

            progressIcon.setAttribute("class", ICON_UPLOADING);
            loadImageElement.setAttribute("style", "max-width: 25px; max-height:25px;");
            loadImageElement.setAttribute("id", "loadImage" + idx);
            uploadButton.appendChild(loadImageElement);
            let selectedFiles = document.getElementById("documents-fileSelector");
            let fi = selectedFiles.files[idx];
            let countFiles = selectedFiles.files.length;

            loadImageElement.setAttribute("src", LOAD_IMAGE);
            let formData = new FormData();

            formData.append("fileTimestamp", "" + fi.lastModified);
            formData.append('user', "" + USER_ID);
            formData.append('customer', "" + CUSTOMER_ID);
            formData.append('fileToUpload', fi);
            let xhr = new XMLHttpRequest();

            xhr.open('POST', UPLOAD_URL, true);
            //xhr.setRequestHeader('enctype', 'multipart/form-data');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {

                    if (xhr.status === 200 || xhr.status === 204) {
                        // remove the index from array:
                        const index = waitingForUpload.indexOf(idx);
                        if (index > -1) {
                            waitingForUpload.splice(index, 1);
                        }
                        console.log("Upload " + idx + " finished: HTTP=" + xhr.status + ", still in list: " + JSON.stringify(waitingForUpload));
                        progressIcon.setAttribute("class", ICON_OK);
                        if (errMsg) uploadButton.removeChild(errMsg);
                        if (loadImageElement) uploadButton.removeChild(loadImageElement);
                        uploadButton.setAttribute("class", "btn btn-success disabled");
                        if (waitingForUpload.length === 0 && uploadAllButton) {
                            uploadAllButton.setAttribute("class", "btn btn-success disabled");
                            uploadAllIcon.setAttribute("class", ICON_CHECK_DOUBLE);
                        } else if (uploadAllButton) {
                            uploadAllButton.setAttribute("class", "sendToServer btn btn-info");
                        }
                        if (countFiles - waitingForUpload.length > 0) {
                            progressBar.hidden = false;
                        }
                        kbCount += fi.size;
                        let percentValue = kbCount / allowedKB;

                        kbElement.innerText = Math.round(kbCount / 1024) + " KBytes of " + Math.round(allowedKB / 1024) + " (" + Math.round(100 * percentValue) + "%)  ";
                        loadImageElement.setAttribute("src", "");
                        progressText.innerText = TEXT_NUMBER_OF_UPLOADED_FILES + (countFiles - nrOfUnAllowedFiles - waitingForUpload.length) + " of "
                            + countFiles;
                        progressBar.value = percentValue;
                    } else if (xhr.status > 399) {
                        progressIcon.setAttribute("class", ICON_ERROR);
                        uploadButton.setAttribute("class", "sendToServer btn btn-warning");
                        waitingForUpload.push(idx);
                        progressBar.hidden = false;
                        errMsg.innerText = " HTTP " + xhr.status;
                        uploadButton.removeChild(loadImageElement);
                    }
                }
            }
            xhr.send(formData);  // multipart/form-data
        }

        if (idx === -1) {
            console.log("Upload ALL files...!");
            let selectedFiles = document.getElementById("documents-fileSelector");
            for (let j = 0; j < selectedFiles.files.length; j++) {
                let isWaiting = waitingForUpload.includes(j);
                if (isWaiting) {
                    uploadTheFile(j);
                }
            }
        } else {
            uploadTheFile(idx);
        }
    });
} else {
    alert("Your browser is too old to support HTML5 File API");
}