/***************************************
	JAVA-Script RESTclient-calls. 
	API: 'elisto'
	Module: 'Executive'
	Version: 1.0
***************************************/

/***************************************
	CALL-ID: elisto.Executive.apiCall1
	Create a new order-list and initiate invoice
	***************************************/
function apiCall_elisto_executive_createinvoice
	(departmentnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber
	};
	return apiCall("elisto.Executive.apiCall1","/4/executive/createinvoice", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall2
	Add a product-item to the order-list
	***************************************/
function apiCall_elisto_executive_addtoproductlist
	(departmentnumber,
	idinvoicenumber,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber,
	"idinvoicenumber" : idinvoicenumber,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("elisto.Executive.apiCall2","/4/executive/addtoproductlist", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall3
	Delete a product from the order-list
	***************************************/
function apiCall_elisto_executive_delitem
	(idorder,
	idorderedproduct,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"idorderedproduct" : idorderedproduct
	};
	return apiCall("elisto.Executive.apiCall3","/4/executive/delitem", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall4
	Set notice on invoice 
	***************************************/
function apiCall_elisto_executive_invoiceremark
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall4","/4/executive/invoiceremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall5
	Set remark on an ordered item 
	***************************************/
function apiCall_elisto_executive_itemremark
	(idorder,
	remark,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idorder" : idorder,
	"remark" : remark
	};
	return apiCall("elisto.Executive.apiCall5","/4/executive/itemremark", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall6
	Show shopping cart of a department 
	***************************************/
function apiCall_elisto_executive_showcart
	(departmentnumber,
	invoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber,
	"invoicenumber" : invoicenumber
	};
	return apiCall("elisto.Executive.apiCall6","/4/executive/showcart", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall8
	executive Dashboard 
	***************************************/
function apiCall_elisto_executive_executivedash
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Executive.apiCall8","/4/executive/executivedash", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall10
	Show invoices of a department 
	***************************************/
function apiCall_elisto_executive_departmentinvoices
	(departmentnumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"departmentnumber" : departmentnumber
	};
	return apiCall("elisto.Executive.apiCall10","/4/executive/departmentinvoices", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall11
	Show status of ordered items of a invoice 
	***************************************/
function apiCall_elisto_executive_deliverystatus
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Executive.apiCall11","/4/executive/deliverystatus", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall12
	Send listed items as order to warehouse
	***************************************/
function apiCall_elisto_executive_sendorder
	(idinvoicenumber,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoicenumber" : idinvoicenumber
	};
	return apiCall("elisto.Executive.apiCall12","/4/executive/sendorder", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall13
	Get notifications of customers for the currently
			logged in executive
	***************************************/
function apiCall_elisto_executive_getnotifications
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Executive.apiCall13","/4/executive/getnotifications", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall14
	Delete a customer-notification for the currently
			logged in executive
	***************************************/
function apiCall_elisto_executive_deletenotification
	(idnotification,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idnotification" : idnotification
	};
	return apiCall("elisto.Executive.apiCall14","/4/executive/deletenotification", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall15
	Set number of customers for an invoice
	***************************************/
function apiCall_elisto_executive_setcustomercount
	(customercount,
	idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"customercount" : customercount,
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall15","/4/executive/setcustomercount", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall16
	Mark a invoice with current status
	***************************************/
function apiCall_elisto_executive_setinvoicestatus
	(idinvoice,
	invoicestatus,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice,
	"invoicestatus" : invoicestatus
	};
	return apiCall("elisto.Executive.apiCall16","/4/executive/setinvoicestatus", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall17
	Archiviation of an invoice
	***************************************/
function apiCall_elisto_executive_archiveinvoice
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall17","/4/executive/archiveinvoice", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall18
	Select executive profile data
	***************************************/
function apiCall_elisto_executive_getexecutiveprofile
	(callBack, httpErrorCallBack) {
	const dataObject = {
	};
	return apiCall("elisto.Executive.apiCall18","/4/executive/getexecutiveprofile", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall19
	Update profile data
	***************************************/
function apiCall_elisto_executive_editexecutiveprofile
	(email,
	executivename,
	first_name,
	last_name,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"email" : email,
	"executivename" : executivename,
	"first_name" : first_name,
	"last_name" : last_name
	};
	return apiCall("elisto.Executive.apiCall19","/4/executive/editexecutiveprofile", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall20
	Change executive password
	***************************************/
function apiCall_elisto_executive_changeexecutivepassword
	(password,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"password" : password
	};
	return apiCall("elisto.Executive.apiCall20","/4/executive/changeexecutivepassword", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall21
	Change executive profile Image
	***************************************/
function apiCall_elisto_executive_changeexecutiveimage
	(image,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"image" : image
	};
	return apiCall("elisto.Executive.apiCall21","/4/executive/changeexecutiveimage", dataObject, callBack, httpErrorCallBack,"put");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall22
	Show orders of all registered users for an invoice
	***************************************/
function apiCall_elisto_executive_userorders
	(idinvoice,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idinvoice" : idinvoice
	};
	return apiCall("elisto.Executive.apiCall22","/4/executive/userorders", dataObject, callBack, httpErrorCallBack,"post");
}

/***************************************
	CALL-ID: elisto.Executive.apiCall23
	Re-Assign orders of a registered user to a different
			invoice
	***************************************/
function apiCall_elisto_executive_reassignuserorders
	(idfrominvoice,
	idtoinvoice,
	iduser,
	callBack, httpErrorCallBack) {
	const dataObject = {
	"idfrominvoice" : idfrominvoice,
	"idtoinvoice" : idtoinvoice,
	"iduser" : iduser
	};
	return apiCall("elisto.Executive.apiCall23","/4/executive/reassignuserorders", dataObject, callBack, httpErrorCallBack,"put");
}


