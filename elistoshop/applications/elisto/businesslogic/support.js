module = "support.js";

/**** SUPPORT MODULE ****/





function supportButtonClicked() {
        const customerID = getCustomerId();
        const tableDivCreateTicket = formRowDiw("tableDivCreate", 12);
        const tableDivSelectTicket = formRowDiw("tableDivSelect", 12);
        const form = [tableDivCreateTicket, tableDivSelectTicket];
        listoHTML.buildForm("#main-content", 12, form, 'select_or_create_ticket_form');
        apiCall_elisto_admin_tickets(customerID, function (data) {
            const action1 = ["supportIcon", actionSupportStyle.elements[0]];
            const dataColumnNames1 = ["id", "ticketdef", "date_posted", "ticketstatus"];
            const dataColumnIndices1 = [0, 1, 2, 3];
            const ticketsTableObject = {
                IDENTIFIER: "admin-support-ticket-table",
                jsonRMLData: data,
                header: [getTextById(331), getTextById(233), getTextById(332), getTextById(333), ""],
                columnIndices: [0, 1, 2, 3],
                actions: [action1],
                dataColumnIndices: [dataColumnIndices1],
                dataColumnNames: [dataColumnNames1]
            };
            apiCall_elisto_admin_ticketdef(function (data) {
                const createTicketAction = ["createTicket", createTicketStyle.elements[0]];
                const ticketActionDataIndex = [0];
                const ticketActionDataName = ["ticketDefinition"];
                const ticketDefsTableObject = {
                    IDENTIFIER: "admin-support-ticketDef-table",
                    jsonRMLData: data,
                    header: [getTextById(233), '', '', ''],
                    columnIndices: [0, 1, 2],
                    actions: [createTicketAction],
                    dataColumnIndices: [ticketActionDataIndex],
                    dataColumnNames: [ticketActionDataName]
                };
                listoHTML.createTable.call(ticketDefsTableObject, "tableDivCreate", true);
                listoHTML.createTable.call(ticketsTableObject, "tableDivSelect", false);
            }, function (err) {
                showHttpErrorMessage((err.status));
            });
        }, function (err) { // If failed
            showHttpErrorMessage("main-content", err);
        });


}


function supportServiceClicked(ticketId) {
    const ticketText = FormTextArea(336, "ticketText", "rows='3' required ", '');
    const attachedFile = formImage(337, "file_imagefield", "required", "", "fileImage_id");
    const submitButton = FormButton(339, "submit_ticket", "", "submit-ticket");
    const cancelButton = FormButton(602, "back", "", "support-button");
    const hiddenField = formHiddenInput("hiddenticketID", ticketId, "ticketID_" + ticketId);
    const tableDiv = formRowDiw("tableDiv", 12);

    apiCall_elisto_admin_ticketdata(ticketId, function (data) {
        console.log(data);
        const action1 = ["viewMessageDetails", viewTicketStyle.elements[0]];
        const dataColumnNames1 = ["id", "ticketid", "customertext", "attachedfile", "changedate"];
        const dataColumnIndices1 = [0, 1, 2, 3, 4];
        const ticketDataTableObject = {
            IDENTIFIER: "admin-support-ticket-detail-table",
            jsonRMLData: data,
            header: [getTextById(335), getTextById(336), getTextById(337), getTextById(338), ""],
            columnIndices: [0, 2, 3, 4],
            actions: [action1],
            dataColumnIndices: [dataColumnIndices1],
            dataColumnNames: [dataColumnNames1],
            rowFormat: function (r, arr) {
                return arr;
            },
            cellFormat: function (colIdx, rowIdx, TDOpen, cell, TDClose) {
                if (colIdx == 2) {
                    debug("colIdx", 0, colIdx, cell);
                    return `${TDOpen}${formThumb(601, '', '', '', cell)}${TDClose}`;
                } else {
                    return `${TDOpen}${cell}${TDClose}`;
                }
            }
        };
        const form1 = [ticketText, attachedFile, hiddenField, submitButton, cancelButton, tableDiv];
        listoHTML.buildForm("#main-content", 10, form1, 'support-ticket-form');
        listoHTML.createTable.call(ticketDataTableObject, "tableDiv", false);
    }, function (err) { // If failed
        showHttpErrorMessage("main-content", err);
    });


}


$(document).on('click', '#submit_ticket', function () {
    if ($("#support-ticket-form").valid()) {
        const ticketText = $("#ticketText").val();
        const ticketID = $(".hiddenticketID").val();
        const customerID = getCustomerId();
        if (document.getElementById('file_imagefield').value) {
            upload(customerID, 'support-ticket-form', function (data) {
                debug("upload-data:", release, data);
                const attachedFile = "" + data.rows[0].s[2];
                //alert(getTextById(255) + document.getElementById('file_imagefield').value + getTextById(256) + imageNew + "'");
                apiCall_elisto_admin_addticketdata(attachedFile, ticketText, ticketID, function (data) {
                    if (data._rc > 0) {
                        alert(getTextById(253) + data._rc);
                    } else {
                        supportServiceClicked(ticketID);
                    }
                }, function err() {
                    showHttpErrorMessage("main-content", err);
                });
            }, function (error) {
                alert(getTextById(257) + document.getElementById('file_imagefield').value + getTextById(258) + error.status);
            });
        } else {
            const attachedFile = images.missing_image_txt;
            apiCall_elisto_admin_addticketdata(attachedFile, ticketText, ticketID, function (data) {
                if (data._rc > 0) {
                    alert(getTextById(253) + data._rc);
                } else {
                    supportServiceClicked(ticketID);
                }
            }, function err() {
                showHttpErrorMessage("main-content", err);
            });
        }
    }
});

$(document).on('click', '.createTicket', function () {

    const ticketdefid = $(this).data('ticketdefinition');
    const customerid = getCustomerId();
    apiCall_elisto_admin_createticket(customerid, ticketdefid, function (data) {
        supportButtonClicked();
    }, function (err) {
        showHttpErrorMessage("main-content", err);
    });

});

$(document).on('click', '.viewMessageDetails', function () {
    const ticketid = $(this).data("ticketid");
    //const messageId = $(this).data("id");
    const attachedfile = $(this).data("attachedfile");
    const attachedfilePath = getImagePath() + "/" + attachedfile;
    const customertext = $(this).data("customertext");
    const ticketText = FormTextArea(336, "ticketText", "rows='3' required readonly ", customertext);
    const cancelButton = FormButton(602, "backToList", "", "support-button");
    const attachedFile = formDisplayImage(337, "", "", attachedfilePath);
    const hiddenField = formHiddenInput("hiddenticketID", ticketid, "ticketID_" + ticketid);
    const form1 = [ticketText, attachedFile, hiddenField, cancelButton];
    listoHTML.buildForm("#main-content", 10, form1, 'support-ticket-form');
});

$(document).on('click', '#backToList', function () {
    const ticketid = $('.hiddenticketID').val();
    supportServiceClicked(ticketid);
});


$(document).on('click', '#support_button', function () {
    supportButtonClicked();
});

$(document).on('click', '#back', function () {
    supportButtonClicked();
});

$(document).on('click', '.supportIcon', function () {
    const ticketId = $(this).data('id');
    supportServiceClicked(ticketId);
});



$(document).on('click', '.support_button', function () {
    supportButtonClicked();
});

