"use strict";

var applicationID = 1;

var bearerCookieName;
var bearerCookie;

const STRIPE_CLID = "ca_FzRG2xy7pxFmDRDzeCL1WGdPi7bVl5gQ";
var is_stripe_registered = false;
var stripe_customer_id;
var role;
var RELEASE = "55710";
var release = "d16af_5";
let module = "server.js";
const WEBSERVER = "ws";
const backendVersion = `/RMLRest2-1.1.7/rml/`;
const serverCount = 2;
const version = "1.2.1";
const environment = "DEV";
var selectedServer=`https://backend2.listorante.com${backendVersion}`;


function getImagePath() {
    return "https://enpasoft.com/listorante/applications/images";
}

function getAPIServerPath() {
    if (isReplicatedCustomer) {
        if (!selectedServer) {
            const node = Math.floor(Math.random() * serverCount + 1);
            try {
                const xhttp0 = new XMLHttpRequest();
                const url0 = `https://backend.listorante.com${backendVersion}server/nodeid`;
                xhttp0.open("get", url0);
                xhttp0.setRequestHeader('Content-Type', 'application/json');
                xhttp0.send();
                xhttp0.onreadystatechange = function () {
                    if (xhttp0.readyState === 4) {
                        if (xhttp0.status === 200) {
                            console.log("\n\t:::::::::::\t Selected node for replicated instance is node " + node);
                            selectedServer = `https://backend${node}.listorante.com${backendVersion}`;
                        }
                    }
                };
            } catch (err) {
                console.error("Could not select server node " + node + ": " + err.message);
            }
            if (selectedServer) {
                return selectedServer;
            }
        } else {
            return selectedServer;
        }
    }
    if (!selectedServer) {
        console.log("getAPIServerPath() selectedServer not set");
        const storedServer = sessionStorage.getItem("selectedServer");
        if (!storedServer) {
            console.log("In getAPIServerPath: storedServer not set (storedServer=" + storedServer + ")");
            const url = `https://backend.listorante.com${backendVersion}server/nodeid`;
            try {
                const xhttp = new XMLHttpRequest();
                xhttp.open("get", url);
                xhttp.onreadystatechange = function () {
                    if (xhttp.readyState === 4) {
                        if (xhttp.status === 200) {
                            const serverData = JSON.parse(xhttp.responseText);
                            console.log("Server-response:" + JSON.stringify(serverData));
                            selectedServer = `https://backend${serverData.node}.listorante.com${backendVersion}`;
                            sessionStorage.setItem("selectedServer", selectedServer);
                            console.log(":::::::::::::\n\n\t Selected backend-server in xhttp-request of server.js: " + selectedServer + "\n\n ::::::::::::::::::::::::");
                        } else if (xhttp.status > 399) {
                            console.log(":::::::::::::\n\n\t Could not select backend-server. \n\n ::::::::::::::::::::::::");
                        }
                    }
                };
                if (!selectedServer) {
                    xhttp.send();
                } else {
                    return selectedServer;
                }
            } catch (err2) {
                console.error("Exception while trying to set backend server with url " + url + ":" + err2.message);
            }
        } else {
            selectedServer = storedServer;
            console.log(":::::::::::::\n\n\t Selected backend-server in server.js FROM SESSIONSTORAGE: " + selectedServer + "\n\n ::::::::::::::::::::::::");
            return selectedServer;
        }
    } else {
        return selectedServer;
    }
}

function getCustomerId() {
    if (!isNaN(customerID) && customerID > 0) {
        return customerID;
    } else {
        const urlCustomerID = parseInt(getUrlVars()['cust'], 10);
        if (urlCustomerID && !isNaN(urlCustomerID) && urlCustomerID > 0) {
            sessionStorage.setItem("theCustomerID", "" + urlCustomerID);
            return urlCustomerID;
        } else {
            const storedCustomer = parseInt(sessionStorage.theCustomerID, 10);
            if (!isNaN(storedCustomer) && storedCustomer > 0) {
                console.log("customerID " + storedCustomer + " retrieved from session");
                customerID = storedCustomer;
                return customerID;
            } else {
                console.log("No customerID is set!");
            }
        }
    }
}

function getApplication() {
    return applicationID;
}

function getUrlVars() {
    const vars = {};
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

function getUserRole() {
    const urlRoleID = parseInt(getUrlVars()['role'], 10);
    const notNeg = (urlRoleID >= 0);
    console.log("urlRoleID=" + urlRoleID + " isNaN:" + isNaN(urlRoleID) + ", is >=0" + notNeg);
    if (!isNaN(urlRoleID) && urlRoleID >= 0) {
        sessionStorage.setItem("theRoleID", "" + urlRoleID);
        role = urlRoleID;
        console.log("role=" + role);
        return urlRoleID;
    } else {
        if (!isNaN(role) && role >= 0) {
            return role;
        } else {
            const storedRole = parseInt(sessionStorage.theRoleID, 10);
            if (!isNaN(storedRole) && storedRole >= 0) {
                console.log("storedRole " + storedRole + " retrieved from session");
                role = storedRole;
                return role;
            } else {
                console.log("No user-role is set!");
            }
        }
    }
}

if (!selectedServer) {
    console.log(":::: \n\tCalling getAPIServerPath() ...\n\n");
    getAPIServerPath();
    setTimeout(function () {
        if (customerID) {
            console.log("::::: \n\nSelect initial server: currently selected server is: " + selectedServer + ". customerID is " + customerID);
        } else {
            console.log("::::: \n\nSelect initial server: currently selected server is: " + selectedServer + ". customerID is not set.");
        }
    }, 250);
}



